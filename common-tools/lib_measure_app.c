/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

// Reminder: To compile this library as a dynamic library:
// $ gcc -shared lib_measure_app.c -o lib_measure_app.so -fPIC

#include "lib_measure_app.h"
#include "rdtsc.h"
#include <stdio.h>
#include <stdlib.h>


void push(LifoStack* ls, unsigned long val)
{
    LifoValue* lv = (LifoValue *) malloc (sizeof (LifoValue));
    lv->value = val;
    lv->next = ls->start;
    ls->start = lv;
    ls->size++;
}

/*------------------------------------------------------------------------ */

unsigned long pull(LifoStack* ls)
{
    unsigned long val = ls->start->value;
    LifoValue* lv = ls->start;
    ls->start = ls->start->next;
    ls->size--;
    free(lv);
    return val;
}

/*------------------------------------------------------------------------ */

void start_global()
{
    extern struct LifoStack values_stack;
    
    //Creating file or truncating it should it exists
    FILE* fp = fopen(filename, "w");
    
    if (fp == NULL)
    {
         printf ("Can't properly open file: %s\n", filename);
         exit (1);
    }
    
    if (fclose (fp) == EOF)
    {
        printf ("Can't properly close the file\n");
        exit (1);
    }
    
    values_stack.size = 0;
    values_stack.start = NULL;
}

/*------------------------------------------------------------------------ */

void stop_global()
{
    extern struct LifoStack values_stack;
    
    //Emptying and freeing the stack
    if(values_stack.size > 0)
    {
        FILE* fp = fopen(filename, "a");
        
        if (fp == NULL)
        {
             printf ("Can't properly open file: %s\n", filename);
             exit (1);
        }
        
        fprintf(fp, "WARNING: %d values remain in stack\n", values_stack.size);
        if (fclose (fp) == EOF)
        {
            printf ("Can't properly close the file\n");
            exit (1);
        }
        
    }
    while(values_stack.size > 0)
        pull(&values_stack);
}

/*------------------------------------------------------------------------ */

void start()
{
    extern unsigned long start_val;
    extern struct LifoStack values_stack;
    
    rdtscll(start_val);
}

/*------------------------------------------------------------------------ */

void start_id()
{
    unsigned long val;
    extern struct LifoStack values_stack;
    
    rdtscll(val);
    push(&values_stack, val);
}

/*------------------------------------------------------------------------ */

void stop()
{
    unsigned long stop_val, result;
    extern unsigned long start_val;
    
    rdtscll(stop_val);
    
    result = stop_val - start_val;
    FILE* fp = fopen(filename, "a");
    
    if (fp == NULL)
    {
         printf ("Can't properly open file: %s\n", filename);
         exit (1);
    }
    
    fprintf(fp, "%lu\n", result);
    
    
    if (fclose (fp) == EOF)
    {
        printf ("Can't properly close the file\n");
        exit (1);
    }
}

/*------------------------------------------------------------------------ */

void stop_id(int loop_id)
{
    unsigned long stop_val, result;
    extern struct LifoStack values_stack;
    
    rdtscll(stop_val);
    if(values_stack.size <= 0)
    {
        FILE* fp = fopen(filename, "a");
        
        if (fp == NULL)
        {
             printf ("Can't properly open file: %s\n", filename);
        }
        
        fprintf(fp, "WARNING: no values remain in stack\n");
        if (fclose (fp) == EOF)
        {
            printf ("Can't properly close the file\n");
            exit (1);
        }
        
        exit (2);
    }
    result = stop_val - pull(&values_stack);
    FILE* fp = fopen(filename, "a");
    
    if (fp == NULL)
    {
         printf ("Can't properly open file: %s\n", filename);
         exit (1);
    }
    
    fprintf(fp, "%d,%lu\n", loop_id, result);
    
    
    if (fclose (fp) == EOF)
    {
        printf ("Can't properly close the file\n");
        exit (1);
    }
}
