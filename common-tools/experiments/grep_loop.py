#!/usr/bin/python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#This script is used to search for a regular expression in loop source files.
#The loop that match the patter will receive the tag specified in the arguments.
#Can be used with the process plugin: cti process run_command "grep.py,<UID>,reg_exp,log_filename" query repository_query

import sys, os, subprocess
from cti_hapi import entry, util_uid, plugin
import cti, ctr

prefix_tag = "pattern_matching_"
#Getting args
if len(sys.argv)!=4:
    sys.stderr.write("Wrong number of arguments.\nUsage: grep.py <loop_uid> <regular_expression> <tag>\n")
    exit(1)

loop_uid = sys.argv[1]
reg_exp = sys.argv[2]
tag = prefix_tag + sys.argv[3]

loop_uid_object = cti.CTI_UID(loop_uid)
file_name = "loop.txt"

#Searching for the expression in the file
file_path = os.path.join(
                ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, loop_uid_object), 
                cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                file_name
            )
if not os.path.isfile(file_path):
    exit(0)
sub = subprocess.Popen(['grep',reg_exp,file_path], shell=False, stdout=open(os.devnull, 'wb'))

output = sub.communicate()
rc = sub.returncode

tmp_tag = tag
if rc == 1:
    tmp_tag = "not_" + tag

#Searching for the application
json_file = \
{
    "add": 
    {
        "attributes": {cti.META_ATTRIBUTE_NAME : "add"},
        "params": 
        [
            {cti.META_ATTRIBUTE_NAME : "entry",cti.META_ATTRIBUTE_VALUE : loop_uid},
            {cti.META_ATTRIBUTE_NAME : "tag",cti.META_ATTRIBUTE_VALUE : tmp_tag}
        ]
    }
}
result = ""
try:
    result = plugin.execute_plugin_by_file(util_uid.CTI_UID('tag', cti.CTR_ENTRY_PLUGIN), json_file)
except:
    print sys.exc_info()[1]
    exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)

