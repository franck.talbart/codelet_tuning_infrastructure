#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#setting variables

#Required
application=""
binary=""
compiled_application=""
submitter_cmd="none"
regular_expression="none"
ug="none"
mode="ssh"
hostname=""
submitter_opt="none"
prefix=""
prefix_cmd="none"
partition="regular"
run_parameters="none"
prefix_execute_script=""
suffix_execute_script=""
uarch=""
k_clusters=""
submitter_user="none"
frequency_host="none"
loop_type="outermost OR single"

tag_name="tag_regexp"
tag=""
cmd_lib_counting="run"
fast_mode="false"

#Geting options
while getopts "a:b:c:C:e:f:g:k:hl:m:n:O:p:q:r:s:S:t:u:U:v" OPTION
do
    case ${OPTION} in
        a)
            application=${OPTARG}
            ;;
        b)
            binary=${OPTARG}
            ;;
        c)
            compiled_application=${OPTARG}
            ;;
        C)
            submitter_cmd=${OPTARG}
            ;;
        e)
            regular_expression=${OPTARG}
            ;;
        f)
            frequency_host=${OPTARG}
            ;;
        g)
            ug=${OPTARG}
            ;;
        h)
            echo "$0 -n <hostname> -b <binary> -c <compiled_application>
            -u <uarch> (sandy_bridge/nehalem/haswell) -f <frequency_host> [-p <prefix>] [-r <run_parameters>] [-a <application_name>] [-e <regular expression>] [-m <mode>] [-g <ug>] [-s <prefix_execute_script>] [-S <suffix_execute_script>] [-C <submitter_cmd>] [-O <submitter_opt>] [-q <prefix_cmd>] [-U <submitter_user>] [-t <partition>] [-l <loop_type>] [-k <nb_clusters>]"
            echo "Example of regular expression: [^%(. ]*%[^%(. ]*%[^%(. ]*[(]"
            exit 0
            ;;
        k)
            k_clusters=${OPTARG}
            ;;
 
        l)
            loop_type=${OPTARG}
            ;;
        m)
            mode=${OPTARG}
            ;;
        n)
            hostname=${OPTARG}
            ;;
        O)
            submitter_opt=${OPTARG}
            ;;
        p)
            prefix=${OPTARG}
            ;;
        q)
            prefix_cmd=${OPTARG}
            ;;
 
        r)
            run_parameters=${OPTARG}
            ;;
        s)
            prefix_execute_script=${OPTARG}
            ;;
        S)
            suffix_execute_script=${OPTARG}
            ;;
        t)
            partition=${OPTARG}
            ;;
        u)
            uarch=${OPTARG}
            ;;
        U)
            submitter_user=${OPTARG}
            ;;
        v)
            cmd_lib_counting="run_fast"
            ;;
        ?)
            echo "Use -h to know the list of options."
            exit 1
            ;;
    esac
done

#Checking we have the minimal amount of information
[ "${hostname}" == "" ] && echo "You must provide the hostname (-n option)!" && exit 1
[ "${binary}" == "" ] && echo "You must provide the binary (-b option)!" && exit 1
binary_name=`basename $binary`
[ "${compiled_application}" == "" ] && echo "You must provide the compiled application (-c option)!" && exit 1
[ "${uarch}" == "" ] && echo "You must provide the uarch (-u option)
(sandy_bridge/nehalem/haswell)!" && exit 1

[ "${uarch}" != "sandy_bridge" ] && [ "${uarch}" != "nehalem" ] && [ "${uarch}" != "haswell" ] && echo "uarch only supports nehalem,  sandy_bridge  and haswell in this script." && exit 1 
[ "${uarch}" == "sandy_bridge" ] && uarch_cqa="SANDY_BRIDGE" && uarch_libcounting="sandy_bridge" && counter_list="FP_COMP_OPS_EXE:SSE_FP_PACKED_DOUBLE,FP_COMP_OPS_EXE:SSE_SCALAR_DOUBLE,L1D:REPLACEMENT,L1D:M_EVICT,DRAM_DATA_READS,DRAM_DATA_WRITES,UNC_M_CAS_COUNT_0:RD,UNC_M_CAS_COUNT_0:WR,UNC_M_CAS_COUNT_1:RD,UNC_M_CAS_COUNT_1:WR,UNC_M_CAS_COUNT_2:RD,UNC_M_CAS_COUNT_2:WR,UNC_M_CAS_COUNT_3:RD,UNC_M_CAS_COUNT_3:WR" && special_permission="true"
[ "${uarch}" == "nehalem" ] && uarch_cqa="NEHALEM" && uarch_libcounting="nehalem_westmere" && counter_list="UNC_QMC_NORMAL_READS:ANY,UNC_QMC_WRITES:FULL_ANY,FP_COMP_OPS_EXE:SSE_FP_PACKED,FP_COMP_OPS_EXE:SSE_FP_SCALAR,L1D:REPL,L1D:M_EVICT,UNC_L3_MISS:ANY,INSTR_RETIRED"
[ "${uarch}" == "haswell" ] && uarch_cqa="HASWELL" && uarch_libcounting="haswell" && counter_list="UOPS_EXECUTED_PORT:PORT_0,UOPS_EXECUTED_PORT:PORT_1,L1D:REPLACEMENT,L2_TRANS:L1D_WB,ICACHE:MISSES,DRAM_DATA_READS,DRAM_DATA_WRITES" && special_permission="true"

[ "${frequency_host}" == "" ] && echo "You must provide the frequency of the host machine (-f option)!" && exit 1
[ "${application}" == "" ] && application="$binary_name"

[ ! -f "${binary}" ] && echo "Binary not found!" && exit 1
[ ! -d "${compiled_application}" ] && echo "Compiled application not found!" && exit 1
[ "$prefix" != "" ] && [ ! -f "${prefix}" ] && echo "Prefix not found!" && exit 1
[ "$prefix_execute_script" != "" ] && [ ! -f "${prefix_execute_script}" ] && echo "Prefix execute script not found!" && exit 1
[ "$suffix_execute_script" != "" ] && [ ! -f "${suffix_execute_script}" ] && echo "Suffix execute script not found!" && exit 1

echo "=========================================================================="
echo "Preparing the experiments..."
echo "=========================================================================="
mkdir -p $CTI_ROOT/local_repositories
tmp_dir=`mktemp -d -p $CTI_ROOT/local_repositories`
cp $binary $tmp_dir
cp -r $compiled_application $tmp_dir
[ -f "$prefix" ]  && cp $prefix $tmp_dir
[ -f "$prefix_execute_script" ]  && cp $prefix_execute_script $tmp_dir
[ -f "$suffix_execute_script" ]  && cp $suffix_execute_script $tmp_dir
cd $tmp_dir

binary=${binary##*/}
prefix=${prefix##*/}

date_repo=`date +"%m-%d-%Y-%H-%M-%S"`
repository="repo_${application}_${date_repo}"
echo "Creating the repository..."
set -x
cti repository init $repository | tee log_exp.log
set +x

echo "Adding the cpu entry if needed..."
cpu=""
[ "${uarch_cqa}" != "none" ] &&  cpu=`cti cpu init "cpu_$hostname" --vendor=none --codename=none --model=nont --family=none --external_model=none --external_family=none --revision=none --number_of_cores=none --number_of_threads=none --core_frequency=none --bus_frequency=none --fsb_speed=none --core_frequency_multiplier=none --instructions=none --core_vid=none --package=none --specification=none --stepping=none --technology=none --reference=none --micro_architecture=$uarch_cqa --platform=none`
cpu=`echo $cpu |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`
echo "Adding the platform entry..."
set -x
platform=`cti platform init "$hostname" --vendor=none --organization=none --processor="$cpu" | tee -a log_exp.log`
set +x

platform=`echo $platform |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`
echo "Adding the application entry..."
set -x
application_entry=`cti application init "${application}_${date_repo}" none none` 
set +x

application_entry=`echo $application_entry |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`
echo "Adding the compile entry..."
set -x
compile=`cti compile init "${application_entry}" local none none false --binary_name="$binary_name" --platform="$platform" --compiled_application="$compiled_application" --auto_run=false  | tee -a log_exp.log`
set +x

compile=`echo $compile |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`
echo "Adding the binary entry..."
set -x
binary=`cti binary init "$binary" "$compile"  | tee -a log_exp.log`
set +x

binary=`echo $binary |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`

echo "=========================================================================="
echo "Performing the experiments..."
echo "=========================================================================="

echo "Running Maqao Perf..."
set -x
maqao_perf=`cti maqao_perf init "${binary}" "${run_parameters}" --mode="$mode" --partition="$partition" --platform="$platform" --prefix_execute_script="$prefix_execute_script" --suffix_execute_script="$suffix_execute_script" --submitter_cmd="$submitter_cmd" --submitter_opt="$submitter_opt" --submitter_user="$submitter_user" --granularity=xsmall --bypass_kernel_detection=true --auto_run=false --prefix_cmd="$prefix_cmd" | tee -a log_exp.log`
maqao_perf=`echo $maqao_perf |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`
cti maqao_perf run "$maqao_perf" "$prefix" --ug="$ug"
set +x

echo "Running Maqao CQA..."
set -x
loop_group=`cti query select $repository "*" loop_group --format=txt --fields=entry_uid`
loop_group=`echo $loop_group | grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`
cti maqao_cqa init "$loop_group" | tee -a log_exp.log
set +x
echo "Running the pattern matching... (if needed)"
[ "${regular_expression}" != "none" ] && cti process run_command "${CTI_ROOT}/common-tools/experiments/grep_loop.py,<UID>,$regular_expression,${tag_name}" "*" $repository loop && tag="pattern_matching_${tag_name}"

echo "Running Libcounting..."
set -x
lib_counting_entry=`cti lib_counting init "${binary}" "${run_parameters}" "${frequency_host}" "${repository}" "loop_type:(${loop_type}) nb_sample:>=5" --mode="${mode}" --partition="${partition}" --platform="${platform}" --special_permissions="${special_permission}" --uarch="${uarch_libcounting}" --auto_run=false --counter_list="${counter_list}"`
lib_counting_entry=`echo $lib_counting_entry |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`

cti lib_counting "${cmd_lib_counting}" ${lib_counting_entry} --prefix="$prefix"

set +x

echo "Running the clustering process..."
set -x
clustering_entry=`cti clustering init "loop_type:(${loop_type}) lib_counting_results.cycles:>=1000" --repository_query="${repository}" --prefix_lib_counting="${uarch_libcounting}_" --lib_counting_entry=${lib_counting_entry} --auto_run=false`
clustering_entry=`echo $clustering_entry |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`
cti clustering run $clustering_entry $k_clusters --tag_name="clustering_intra" 
set +x

echo "=========================================================================="
echo "Preparing the results..."
echo "=========================================================================="

cd $CTI_ROOT/common-tools/XLSX/
set -x
./generate_all_exp_xlsx.sh -r "$repository" -a "$tag" -b "not_${tag}" -l "$loop_type" | tee -a log_exp.log
set +x
cd -
mkdir import
mv $CTI_ROOT/common-tools/XLSX/*$application*.xlsx import/
mv $CTI_ROOT/common-tools/XLSX/*$application*.csv import/
mv $CTI_ROOT/common-tools/XLSX/clustering.csv import/
echo ""
echo "XLSX and CSV files are stored in $PWD"
echo "Storing the files into CTI..."
set -x
file_entry=`cti file init $PWD/import $maqao_perf`
$file_entry=`echo $file_entry |grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`
cti alias data "files_${application}_${date_repo}" $file_entry
set +x
