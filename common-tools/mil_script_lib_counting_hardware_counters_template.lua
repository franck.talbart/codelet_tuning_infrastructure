
function user_defined_get_table_id(object)
    ret_val = -99;
    current_id = -99;
    loop_ids = {#__CTI_PLACEHOLDER_LOOP_LIST__#};
    
    if(object:get_id()) then
        current_id = object:get_id();
    else
        return ret_val;
    end

    for i,loop_id in ipairs(loop_ids) do
        if(current_id == loop_id) then
            return i-1
        end
    end
    return ret_val;
end


events = {
    run_dir = "./",

    main_bin = {
        properties={
            enable_callsite_instrumentation = false,
            instru_trace_log=true,
            generate_metafile = true,
            distinguish_inlined_functions = true,
            distinguish_suffix = "_omp",
        },
        path = "#__CTI_PLACEHOLDER_BIN_PATH__#",
    	at_entry={
      	  {
     	       name = "counting_add_hw_counters",
     	       lib = "#__CTI_PLACEHOLDER_LIB__#",
     	       params = { {type = "string",value = "#__CTI_PLACEHOLDER_PROBE_LIST__#"}}
    	    },
    	    {
    	        name = "counting_start_counters",
    	        lib = "#__CTI_PLACEHOLDER_LIB__#",
   	         params = { {type = "imm",value = #__CTI_PLACEHOLDER_LOOP_NUMBER__#} }
   	     }
  	},
   	 at_exit={
   	     {
   	         name="#__CTI_PLACEHOLDER_TYPE_COUNTING__#",
   	         lib="#__CTI_PLACEHOLDER_LIB__#",
   	         params = { {type = "string",value = "test_counting.result"} }
   	     }
 	},
        functions={
            {
                loops = {
                    {
                        filters = {
                            { 
                                type = "whitelist", 
                                filter = {
                                    {
                                        subtype = "numberlist",
                                        value = {#__CTI_PLACEHOLDER_LOOP_LIST__#}
                                    }
                                }
                            }
                        },
                        entries = {
                            {
                                name = "counting_start_counting",
                                lib = "#__CTI_PLACEHOLDER_LIB__#",
                                params = { {type = "function",value = user_defined_get_table_id} }
                            }
                        },
                        exits = {
                            {
                                name = "#__CTI_PLACEHOLDER_STOP_COUNTING__#",
                                lib = "#__CTI_PLACEHOLDER_LIB__#",
                                params = { {type = "function",value = user_defined_get_table_id} }
                            }
                        },
                    }
                }
            }
        }
    }
}

