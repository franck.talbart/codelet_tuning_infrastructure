#ifndef _RDTSC_H_
#define _RDTSC_H_

#define rdtscll(val) do { \
           unsigned int __a,__d; \
           asm volatile("rdtsc" : "=a" (__a), "=d" (__d)); \
           (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32); \
} while(0)


#endif
