#!/bin/bash

#************************************************************************
#    Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

if [ ! -e /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies ]; then
    echo "Set frequency is not allowed on this machine."
    exit 0
fi
echo -n "Available Speeds:"
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies

if [ -z "$*" ]; then
  echo "CPU Frequency Missing!"
  echo "Usage: $0 <CPU Frequency>"
  echo "To set governors on 'ondemand' mode: $0 ondemand"
  exit 0
fi

SPEED=$1

# check for number of CPU
cpu_min=`cut -d- -f1 /sys/devices/system/cpu/online`
cpu_max=`cut -d- -f2 /sys/devices/system/cpu/online`

if [ $1 == "ondemand" ]; then
    # set governors to ondemand
    for i in `seq $cpu_min $cpu_max`; do
        echo ondemand > /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor
    done
else
    # set governors to userspace
    for i in `seq $cpu_min $cpu_max`; do
        echo userspace > /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor
    done


    echo -n "starting speed = "
    cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
    sleep 3
    echo -n "new speed = "
    for i in `seq $cpu_min $cpu_max`; do
        echo ${SPEED} > /sys/devices/system/cpu/cpu$i/cpufreq/scaling_setspeed
    done
fi
echo "Done"
