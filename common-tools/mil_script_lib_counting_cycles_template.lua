
function user_defined_get_loop_id(object)
    if(object:get_id()) then
        return object:get_id();
    end
    return -99;
end


events = {
    run_dir = "./",

    main_bin = {
        properties={
            enable_callsite_instrumentation = false,
            enable_callsite_instrumentation = false,
            instru_trace_log=true,
            generate_metafile = true,
            distinguish_inlined_functions = true,
            distinguish_suffix = "_omp",
        },
        path = "#__CTI_PLACEHOLDER_BIN_PATH__#",
    	at_entry={
   	     {
     	       name = "start_global",
    	        lib = "#__CTI_PLACEHOLDER_LIB__#"
     	   }
   	 },
  	  at_exit={},
        functions={
            {
                loops = {
                    {
                        filters = {
                            {
                                type = "whitelist", 
                                filter = {
                                    {
                                        subtype = "numberlist",
                                        value = {#__CTI_PLACEHOLDER_LOOP_LIST__#}
                                    }
                                }
                            }
                        },
                        entries = {
                            {
                                name = "start_id",
                                lib = "#__CTI_PLACEHOLDER_LIB__#"
                            }
                        },
                        exits = {
                            {
                                name = "stop_id",
                                lib = "#__CTI_PLACEHOLDER_LIB__#",
                                params = {  {type = "function",value = user_defined_get_loop_id} }
                            }
                        },
                    }
                }
            }
        }
    }
}


