/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef LIB_MEASURE_
#define LIB_MEASURE_

#define filename "result.txt"

typedef struct LifoValue 
{
    unsigned long value;
    struct LifoValue* next;
}LifoValue;

typedef struct LifoStack 
{
    LifoValue* start;
    int size;
}LifoStack;

struct LifoStack values_stack;
unsigned long start_val;

//Lifo functions
void init_stack(LifoStack*);
void push(LifoStack*, unsigned long);
unsigned long pull(LifoStack*);

//Counter functions
void start_global();
void stop_global();
void start();
void start_id();
void stop();
void stop_id(int);

#endif /* LIB_MEASURE_ */

