#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#Required
REPOSITORY=""

#Geting options
while getopts "r:a:b:c:d:h" OPTION
do
    case ${OPTION} in
        a)
            METRIC1=${OPTARG}
            ;;
        b)
            METRIC2=${OPTARG}
            ;;
        c)
            METRIC3=${OPTARG}
            ;;
        d)
            METRIC4=${OPTARG}
            ;;
        r)
            REPOSITORY=${OPTARG}
            ;;
        h)
            echo "$0 -r <repository> -a <metric1> -b <metric2> [-c <metric3>] [-d <metric4>]"
            exit 0
            ;;
        ?)
            echo "Use -h to know the list of options."
            exit 1
            ;;
    esac
done

NUM_SPEEDUP="2"
#Checking we have the minimal amount of information
[ "${REPOSITORY}" == "" ] && echo "You must provide a repository (-r option)!" && exit 1
[ "${METRIC1}" == "" ] && echo "You must provide the 1st metric (-a option)!" && exit 1
[ "${METRIC2}" == "" ] && echo "You must provide the 2nd metric (-b option)!" && exit 1
[ "${METRIC3}" != "" ] && NUM_SPEEDUP="3"
[ "${METRIC4}" != "" ] && NUM_SPEEDUP="4"

FILENAME_CSV="speedup_metrics_${NUM_SPEEDUP}_CQA_${REPOSITORY}_${METRIC1}.csv"
FILENAME_XLSX="speedup_metrics_${NUM_SPEEDUP}_CQA_${REPOSITORY}_${METRIC1}.xlsx"
REFERENCE="matrix_loop_results.common_(L1)_Nb_cycles"

FIELDS="alias,loop.loop_id,loop.original_file,loop.source_function_name,loop.first_line,loop.end_line,loop.language,loop.nb_sample,loop.cpi_ratio,loop.coverage_maqao,loop.loop_type,${REFERENCE},${METRIC1},${METRIC2}"
[ "${METRIC3}" != "" ] && FIELDS="${FIELDS},${METRIC3}"
[ "${METRIC4}" != "" ] && FIELDS="${FIELDS},${METRIC4}"

cti entry export csv "loop.nb_sample:>=5 loop.loop_type:(innermost OR single)" "$REPOSITORY" maqao_cqa_results -10 --filename="$FILENAME_CSV" --fields=$FIELDS

[ -f $FILENAME_CSV ] && ./csv_to_xlsx.py "$FILENAME_CSV" template_xlsx/metrics_speedup_${NUM_SPEEDUP}.xlsx sheet1 "$FILENAME_XLSX" 0 A && ./insert_cell.py "$FILENAME_XLSX" 1 L "$REFERENCE" sheet1 && ./insert_cell.py "$FILENAME_XLSX" 1 M "$METRIC1" sheet1 && ./insert_cell.py "$FILENAME_XLSX" 1 N "$METRIC2" sheet1

[ -f $FILENAME_CSV ] && [ "${METRIC3}" != "" ] && ./insert_cell.py "$FILENAME_XLSX" 1 O "$METRIC3" sheet1
[ -f $FILENAME_CSV ] && [ "${METRIC4}" != "" ] && ./insert_cell.py "$FILENAME_XLSX" 1 P "$METRIC4" sheet1

[ -f $FILENAME_CSV ] && NB_LINE=`cat $FILENAME_CSV | wc -l` && ./update_chart.py "$FILENAME_XLSX" 2 "$NB_LINE" $(($NUM_SPEEDUP - 1)) chart1

echo "CSV and XLSX files have been generated in the current directory."

