#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#Required
REPOSITORY=""

#Geting options
while getopts "r:h" OPTION
do
    case ${OPTION} in
        r)
            REPOSITORY=${OPTARG}
            ;;
        h)
            echo "$0 -r<repository>"
            exit 0
            ;;
        ?)
            echo "Use -h to know the list of options."
            exit 1
            ;;
    esac
done

#Checking we have the minimal amount of information
[ "${REPOSITORY}" == "" ] && echo "You must provide a repository (-r option)!" && exit 1

FILENAME_CSV="total_loops_$REPOSITORY.csv"
cti entry export csv "nb_sample:>=5" $REPOSITORY loop -10 --filename=$FILENAME_CSV --fields="alias,loop_id,original_file,source_function_name,first_line,end_line,language,nb_sample,cpi_ratio,coverage_maqao,loop_type" 
[ -f $FILENAME_CSV ] && ./csv_to_xlsx.py $FILENAME_CSV template_xlsx/loop_repartition.xlsx sheet1 loop_repartition_$REPOSITORY.xlsx 0 A

echo "CSV and XLSX files have been generated in the current directory."
