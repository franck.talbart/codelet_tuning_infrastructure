#************************************************************************
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distripbuted in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart

import zipfile
from xml.dom import minidom
import tempfile, shutil, os, re

class XLSX():
#---------------------------------------------------------------------------
    def __init__(self, filename):
        """ prepare the XLSX metadata
        
        Args:
            self: class XLSX
            filename: the XLSX file to update
        """
        self.directory = tempfile.mkdtemp()
        self.filename = filename
        self.xmldoc = None
        self.cur_dir = os.path.abspath(os.curdir)
#---------------------------------------------------------------------------
    def __enter__(self):
        print "Loading " + self.filename
        z = zipfile.ZipFile(self.filename, "r")
        z.extractall(self.directory)
        self.select_worksheet("sheet1")
        os.chdir(os.path.join(self.directory, "xl"))
        self.xml_shared_strings = minidom.parse("sharedStrings.xml")
        self.cpt_shared_string = len(self.xml_shared_strings.getElementsByTagName('t'))
        return self
#---------------------------------------------------------------------------
    def select_worksheet(self, worksheet):
        """ Select the worksheet to update (by default, 1st worksheet is selected)
        
        Args:
            self: class XLSX
            worksheet: the worksheet name (sheet2, sheet3, etc...)
        """
        print "Selecting Worksheet " + worksheet
        self.worksheet = worksheet + ".xml"
        os.chdir(os.path.join(self.directory, "xl", "worksheets"))
        self.xmldoc = minidom.parse(self.worksheet)
        # Removing default values of the computed formula
        formula = self.xmldoc.getElementsByTagName('f')
        for f in formula:
            parent = f.parentNode
            if len(parent.childNodes) > 1:
                parent.removeChild(parent.childNodes[1])
#---------------------------------------------------------------------------
    def select_chart(self, chart):
        """ Select the chart to update (by default, 1st chart is selected)
        
        Args:
            self: class XLSX
            worksheet: the worksheet name (chart2, chart3, etc...)
        """
        print "Selecting Chart " + chart
        self.chart_filename = chart + ".xml"
        os.chdir(os.path.join(self.directory, "xl", "charts"))
        self.chart = minidom.parse(self.chart_filename)
#---------------------------------------------------------------------------
    def change_ref_chart(self, nb_num_ref, num_min, num_max):
        """ Select the chart to update (by default, 1st chart is selected)
        
        Args:
            self: class XLSX
            worksheet: the worksheet name (chart2, chart3, etc...)
        """
        num_ref = self.chart.getElementsByTagName('c:numRef')[nb_num_ref]
        for c in num_ref.childNodes:
            if hasattr(c, "tagName"):
                if c.tagName == "c:f":
                    c.childNodes[0].nodeValue = re.sub("([^!]*[!][$][A-Z]+[$])[0-9]+([:][$][A-Z]+[$])[0-9]+", "\g<1>" + str(num_min) + "\g<2>" + str(num_max), c.childNodes[0].nodeValue)
                elif c.tagName == "c:numCache":
                    cpt = 0
                    for child in c.childNodes:
                        if hasattr(child, "tagName"):
                            if child.tagName == "c:ptCount":
                                child.setAttribute("val", str(num_max -num_min + 1))
                            elif child.tagName == "c:pt":
                                parent = child.parentNode                         
                                parent.removeChild(parent.childNodes[cpt])
                            cpt += 1                        
                    for i in range(num_max - num_min + 1):
                       c_pt = self.chart.createElement('c:pt')
                       c_pt.setAttribute("idx",str(i))
                       c_v = self.chart.createElement('c:v')
                       c_pt.appendChild(c_v)
                       c_v.appendChild(self.chart.createTextNode("0"))
                       c.appendChild(c_pt)
                       
#---------------------------------------------------------------------------
    def insert_data(self, column, line, value):
        """ Insert data into the worksheet
        
        Args:
            self: class XLSX
            column: the column of the cell of update. For instance, "A".
            line: the line of the cell of update. For instance, 1.
            value: the value to insert into the selected cell
        """
        
        def set_row(doc, row, column, line, value):
            c = doc.createElement('c')
            c.setAttribute("r",column + str(line))
            value = insert_value(value, c)
            
            c_list = row.getElementsByTagName('c')
            # c must be in alphabatical order, otherwhise Excel 2010 won't open the file...
            len_c_list = len(c_list)
            if len_c_list > 0:
                max_letter = 0
                for c_l in c_list:
                    if ord(c_l.getAttribute('r')[0]) > ord(column):
                        break
                    max_letter += 1
                if max_letter >= len_c_list:
                   row.appendChild(c)
                else:  
                   row.insertBefore(c, c_list[max_letter])
            else:
                row.appendChild(c)
                    
            v = doc.createElement('v')
            c.appendChild(v)
            v.appendChild(doc.createTextNode(value))
            
        def insert_string(xml_shared_strings, value):
            si = xml_shared_strings.createElement('si')
            t = xml_shared_strings.createElement('t')
            t.appendChild(xml_shared_strings.createTextNode(value))
            si.appendChild(t)
            sst = xml_shared_strings.getElementsByTagName('sst')
            for s in sst:
                s.appendChild(si)
                
        def insert_value(value, c):
            try:
                tmp = float(value)
            except:
                # value is a string
                c.setAttribute("t","s")
                insert_string(self.xml_shared_strings, value)
                value = str(self.cpt_shared_string)
                self.cpt_shared_string += 1
            return value
           
        print "Inserting " + column + str(line)
        sheetData = self.xmldoc.getElementsByTagName('sheetData')
        try:
            for s in sheetData:
                for row in s.childNodes:
                    if str(row.getAttributeNode("r").nodeValue) == str(line):
                        for c in row.childNodes:
                            if c.attributes["r"].value == column + str(line):
                               for v in c.childNodes:
                                   v.childNodes[0].nodeValue = insert_value(value, c)
                                   raise StopIteration								
                               # val not found
                               value = insert_value(value, c)
                               v = self.xmldoc.createElement('v')
                               c.appendChild(v)
                               v.appendChild(self.xmldoc.createTextNode(value))
                               raise StopIteration
                        # c not found
                        set_row(self.xmldoc, row, column, line, value)
                        raise StopIteration
                # row not found
                row = self.xmldoc.createElement('row')
                row.setAttribute('r',str(line))
                row_list = self.xmldoc.getElementsByTagName('row')
                # row must be sorted, otherwhise Excel 2010 won't open the file...
                len_row_list = len(row_list)
                if len_row_list > 0:
                    max_line = 0
                    for row_l in row_list:
                        if int(row_l.getAttribute('r')) > line:
                            break
                        max_line += 1
                    if max_line >= len_row_list:
                       s.appendChild(row)
                    else:  
                       s.insertBefore(row, row_list[max_line])
                else:
                    s.appendChild(row)
                set_row(self.xmldoc, row, column, line, value)
        except StopIteration:
            exit            
#---------------------------------------------------------------------------
    def save_worksheet(self):
        """ Record the updated worksheet. THEN, THE XLSX FILE MUST BE GENERATED
        
        Args:
            self: class XLSX
        """
        print "Recording worksheet " + self.worksheet
        os.chdir(os.path.join(self.directory, "xl", "worksheets"))
        xml_content = self.xmldoc.toxml()
        file_worksheet = open(self.worksheet, "w")
        file_worksheet.write(xml_content)
        file_worksheet.close()
#---------------------------------------------------------------------------
    def save_chart(self):
        """ Record the updated chart.
        
        Args:
            self: class XLSX
        """
        print "Recording chart " + self.chart_filename
        os.chdir(os.path.join(self.directory, "xl", "charts"))
        chart_content = self.chart.toxml()
        file_chart = open(self.chart_filename, "w")
        file_chart.write(chart_content)
        file_chart.close()
#---------------------------------------------------------------------------
    def build(self, filename):
        """ Generate the updated XLSX file
        
        Args:
            self: class XLSX
            filename: the filename of the new XLSX file. It is generated in the current directory.
        """
        
        # Shared strings
        os.chdir(os.path.join(self.directory, "xl"))
        xml_shared_strings_content = self.xml_shared_strings.toxml()
        file_shared_string = open("sharedStrings.xml", "w")
        file_shared_string.write(xml_shared_strings_content)
        file_shared_string.close()        
        os.chdir(self.cur_dir)
        print "Building the XLSX file"
        zf = zipfile.ZipFile(filename, "w")
        abs_src = os.path.abspath(self.directory)
        for dirname, subdirs, files in os.walk(self.directory):
            for filename in files:
                absname = os.path.abspath(os.path.join(dirname, filename))
                arcname = absname[len(abs_src) + 1:]
                print 'Adding %s as %s' % (os.path.join(dirname, filename),
                                            arcname)
                zf.write(absname, arcname)
        zf.close()
#---------------------------------------------------------------------------
    def __exit__(self, exc_type, exc_value, traceback):
        print "Cleaning up the sandbox"        
        shutil.rmtree(self.directory)
#---------------------------------------------------------------------------

