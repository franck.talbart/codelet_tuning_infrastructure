#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#Required
REPOSITORY=""
METRIC1=""
REFERENCE=""

#Geting options
while getopts "r:m:e:h" OPTION
do
    case ${OPTION} in
        r)
            REPOSITORY=${OPTARG}
            ;;
        m)
            METRIC1=${OPTARG}
            ;;
        e)
            REFERENCE=${OPTARG}
            ;;
        h)
            echo "$0 -r <repository> -m <metric> -e <reference>"
            exit 0
            ;;
        ?)
            echo "Use -h to know the list of options."
            exit 1
            ;;
    esac
done

#Checking we have the minimal amount of information
[ "${REPOSITORY}" == "" ] && echo "You must provide a repository (-r option)!" && exit 1
[ "${METRIC1}" == "" ] && echo "You must provide the metric (-m option)!" && exit 1
[ "${REFERENCE}" == "" ] && echo "You must provide the metric reference (-e option)!" && exit 1

FILENAME_CSV="ratio_metrics_CQA_$REPOSITORY.csv"
FILENAME_XLSX="ratio_metrics_CQA_$REPOSITORY.xlsx"

cti entry export csv "loop.nb_sample:>=5 loop.loop_type:(innermost OR single)" $REPOSITORY maqao_cqa_results -10 --filename=$FILENAME_CSV --fields="alias,loop.loop_id,loop.original_file,loop.source_function_name,loop.first_line,loop.end_line,loop.language,loop.nb_sample,loop.cpi_ratio,loop.coverage_maqao,loop.loop_type,$REFERENCE,$METRIC1"
[ -f $FILENAME_CSV ] && ./csv_to_xlsx.py $FILENAME_CSV template_xlsx/ratio_metrics.xlsx sheet1 $FILENAME_XLSX 0 A && ./insert_cell.py $FILENAME_XLSX 1 L $REFERENCE sheet1 && ./insert_cell.py $FILENAME_XLSX 1 M $METRIC1 sheet1
[ -f $FILENAME_CSV ] && NB_LINE=`cat $FILENAME_CSV | wc -l` && ./update_chart.py $FILENAME_XLSX 2 $NB_LINE 1 chart1 && ./update_chart.py $FILENAME_XLSX 2 $NB_LINE 0 chart2

echo "CSV and XLSX files have been generated in the current directory."
