#! /usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import XLSX_lib
import csv, sys, os

file_xlsx = os.path.abspath(sys.argv[1])
min_val = int(sys.argv[2])
max_val = int(sys.argv[3])
nb_ref = int(sys.argv[4])
chart = sys.argv[5]

with XLSX_lib.XLSX(file_xlsx) as xlsx:
    xlsx.select_chart(chart)
    for i in range(nb_ref +1):
        xlsx.change_ref_chart(i, min_val, max_val)
    xlsx.save_chart()
    xlsx.build(file_xlsx)
