#! /usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import XLSX_lib
import csv, sys, os

file_xlsx = os.path.abspath(sys.argv[1])
row = int(sys.argv[2])
col = sys.argv[3]
value = sys.argv[4]
worksheet = sys.argv[5]

with XLSX_lib.XLSX(file_xlsx) as xlsx:
    xlsx.select_worksheet(worksheet)
    xlsx.insert_data(col, row, value)
    xlsx.save_worksheet()
    xlsx.build(file_xlsx)
