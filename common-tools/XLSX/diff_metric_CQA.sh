#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#Required
REPOSITORY1=""
REPOSITORY2=""
METRIC=""

#Geting options
while getopts "r:s:m:h" OPTION
do
    case ${OPTION} in
        r)
            REPOSITORY1=${OPTARG}
            ;;
        s)
            REPOSITORY2=${OPTARG}
            ;;
        m)
            METRIC=${OPTARG}
            ;;
        h)
            echo "$0 -r <repository1> -s <repository2> -m <metric>"
            exit 0
            ;;
        ?)
            echo "Use -h to know the list of options."
            exit 1
            ;;
    esac
done

#Checking we have the minimal amount of information
[ "${REPOSITORY1}" == "" ] && echo "You must provide the 1st repository (-r option)!" && exit 1
[ "${REPOSITORY2}" == "" ] && echo "You must provide the 2nd repository (-s option)!" && exit 1
[ "${METRIC}" == "" ] && echo "You must provide a metric (-m option)!" && exit 1

FILENAME_CSV="speedup_metrics_CQA_${METRIC}.csv"
FILENAME_XLSX="diff_${METRIC}_CQA_${REPOSITORY1}_${REPOSITORY2}.xlsx"
REFERENCE="matrix_loop_results.common_(L1)_Nb_cycles"

FIELDS="alias,loop.loop_id,loop.original_file,loop.source_function_name,loop.first_line,loop.end_line,loop.language,loop.nb_sample,loop.cpi_ratio,loop.coverage_maqao,loop.loop_type,${REFERENCE},${METRIC}"

> $FILENAME_CSV
cti entry export csv "loop.nb_sample:>=5 loop.loop_type:(innermost OR single)" "$REPOSITORY1" maqao_cqa_results -10 --filename="$FILENAME_CSV" --fields=$FIELDS
[ -f $FILENAME_CSV ] && ./csv_to_xlsx.py "$FILENAME_CSV" template_xlsx/diff_metrics_speedup.xlsx sheet1 tmp.xlsx 0 A && ./insert_cell.py tmp.xlsx 1 L "$REFERENCE" sheet1 && ./insert_cell.py tmp.xlsx 1 M "${METRIC}_${REPOSITORY1}" sheet1
> $FILENAME_CSV
cti entry export csv "loop.nb_sample:>=5 loop.loop_type:(innermost OR single)" "$REPOSITORY2" maqao_cqa_results -10 --filename="$FILENAME_CSV" --fields=$FIELDS
[ -f $FILENAME_CSV ] && ./csv_to_xlsx.py "$FILENAME_CSV" tmp.xlsx sheet2 "$FILENAME_XLSX" 0 A && ./insert_cell.py "$FILENAME_XLSX" 1 L "$REFERENCE" sheet2 && ./insert_cell.py "$FILENAME_XLSX" 1 M "${METRIC}_${REPOSITORY2}" sheet2

rm tmp.xlsx

[ -f $FILENAME_CSV ] && NB_LINE=`cat $FILENAME_CSV | wc -l` && ./update_chart.py "$FILENAME_XLSX" 2 "$NB_LINE" 1 chart1

echo "CSV and XLSX files have been generated in the current directory."

