#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#Required
REPOSITORY=""
LOOP_TYPE="outermost OR single"
TAG1=""
TAG2=""

#Geting options
while getopts "r:a:b:hl:" OPTION
do
    case ${OPTION} in
        r)
            REPOSITORY=${OPTARG}
            ;;
        a)
            TAG1=${OPTARG}
            ;;
        b)
            TAG2=${OPTARG}
            ;;
        h)
            echo "$0 -r <repository> [-a <tag1>] [-b <tag2>] [-l <loop_type>]"
            exit 0
            ;;
        l)
            LOOP_TYPE=${OPTARG}
            ;;
        ?)
            echo "Use -h to know the list of options."
            exit 1
            ;;
    esac
done

#Checking we have the minimal amount of information
[ "${REPOSITORY}" == "" ] && echo "You must provide a repository (-r option)!" && exit 1

./loop_repartition.sh -r $REPOSITORY

./coverage.sh -r $REPOSITORY

[ "${TAG1}" != "" ] && [ "${TAG2}" != "" ] && ./coverage_tags.sh -r $REPOSITORY -a $TAG1 -b $TAG2

./speedup_metrics_CQA.sh -r $REPOSITORY -a "matrix_loop_results.x86_64_(L1)_Nb_cycles_if_clean" -b "matrix_loop_results.x86_64_(L1)_Nb_cycles_if_only_FP" -c "matrix_loop_results.x86_64_(L1)_Nb_cycles_if_only_FP_arith"

./speedup_metrics_CQA.sh -r $REPOSITORY -a "matrix_loop_results.x86_64_(L1)_Nb_cycles_if_FP_arith_vectorized" -b "matrix_loop_results.x86_64_(L1)_Nb_cycles_if_fully_vectorized"

./ratio_metrics.sh -r $REPOSITORY -m "matrix_loop_results.x86_64_(L1)_Nb_cycles_if_clean" -e "matrix_loop_results.common_(L1)_Nb_cycles"

> clustering.csv
cti entry export csv "loop_type:($LOOP_TYPE) lib_counting_results.cycles:>=1000" $REPOSITORY "loop" --filename="clustering.csv" --fields="alias,loop_id,first_line,end_line,source_function_name,status_maqao_perf,coverage_maqao,inclusive_coverage,loop_type,inclusive_nb_sample,tag,lib_counting_results.invocations,lib_counting_results.cycles,maqao_cqa_results.matrix_loop_results.common_Nb_FLOP_div,maqao_cqa_results.matrix_loop_results.x86_64_Nb_insn_SD,maqao_cqa_results.matrix_loop_results.x86_64_Nb_uops_P1,maqao_cqa_results.matrix_loop_results.x86_64_Ratio_ADD_SUB_/_MUL,maqao_cqa_results.matrix_loop_results.x86_64_RecMII_(cycles),maqao_cqa_results.matrix_loop_results.common_Vec_ratio_(%)_mul_(FP),maqao_cqa_results.matrix_loop_results.common_Vec_ratio_(%)_other,maqao_cqa_results.matrix_loop_results.common_Vec_ratio_(%)_other_(FP),maqao_cqa_results.matrix_loop_results.common_(L1)_Bytes_stored_/_cycle,maqao_cqa_results.matrix_loop_results.common_(L1)_IPC,lib_counting_results.metrics.haswell_compute_instructions,lib_counting_results.metrics.haswell_L2_bandwidth,lib_counting_results.metrics.haswell_memory_bandwidth,lib_counting_results.metrics.sandy_bridge_double_precision,lib_counting_results.metrics.sandy_bridge_L2_bandwidth,lib_counting_results.metrics.sandy_bridge_memory_bandwidth,lib_counting_results.metrics.nehalem_westmere_double_precision,lib_counting_results.metrics.nehalem_westmere_L2_bandwidth,lib_counting_results.metrics.nehalem_westmere_L3_miss_rate,lib_counting_results.metrics.nehalem_westmere_memory_bandwidth" --sorted_query=11
# the grep removes deuplicated lines when a loop contain several tags
head -n 1 clustering.csv > tmp_clustering.csv
grep "cluster" "clustering.csv" >> tmp_clustering.csv
mv tmp_clustering.csv clustering.csv

