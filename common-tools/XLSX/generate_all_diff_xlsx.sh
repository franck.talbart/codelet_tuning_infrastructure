#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#Required
REPOSITORY1=""
REPOSITORY2=""

#Geting options
while getopts "r:s:h" OPTION
do
    case ${OPTION} in
        r)
            REPOSITORY1=${OPTARG}
            ;;
        s)
            REPOSITORY2=${OPTARG}
            ;;
        h)
            echo "$0 -r <repository1> -s <repository2>"
            exit 0
            ;;
        ?)
            echo "Use -h to know the list of options."
            exit 1
            ;;
    esac
done

#Checking we have the minimal amount of information
[ "${REPOSITORY1}" == "" ] && echo "You must provide a repository (-r option)!" && exit 1
[ "${REPOSITORY2}" == "" ] && echo "You must provide a 2nd repository (-s option)!" && exit 1

metric_list="matrix_loop_results.x86_64_(L1)_Nb_cycles_if_clean matrix_loop_results.x86_64_(L1)_Nb_cycles_if_only_FP matrix_loop_results.x86_64_(L1)_Nb_cycles_if_only_FP_arith matrix_loop_results.x86_64_(L1)_Nb_cycles_if_FP_arith_vectorized matrix_loop_results.x86_64_(L1)_Nb_cycles_if_fully_vectorized"   
for element in $metric_list    
do   
        ./diff_metric_CQA.sh -r "${REPOSITORY1}" -s "${REPOSITORY2}" -m "$element"
done

