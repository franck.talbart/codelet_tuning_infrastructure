#! /usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import XLSX_lib
import csv, sys, os

file_csv = os.path.abspath(sys.argv[1])
template = os.path.abspath(sys.argv[2])
worksheet = sys.argv[3]
output = os.path.abspath(sys.argv[4])
num_line = int(sys.argv[5])

with XLSX_lib.XLSX(template) as xlsx:
    xlsx.select_worksheet(worksheet)
    with open(file_csv, 'rb') as f:
        reader = csv.reader(f)
        row_num = 1
        for row in reader:
            if row_num > 1:
                col_num = sys.argv[6]
                for column in row:
                    xlsx.insert_data(col_num, row_num + num_line, column)
                    col_num = chr(ord(col_num) + 1) 
            row_num+=1 
        xlsx.save_worksheet()
    xlsx.build(output)
