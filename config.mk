#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

BUILD_DIR=${CTI_ROOT}/build.tmp
CFG_DIR=cfg
CTI_ALIAS_REPOSITORY=alias_repository.txt
CTI_CFG=config.txt
CTI_COMMON_TOOLS_DIR=common-tools
CTI_DEMO_DIR=demo
CTI_ENVIRONMENT_SCRIPT=set_environment.sh
CTI_FE_NAME=cti
CTI_GLOBAL_INDEX_FILE=global_index_file.txt
CTI_LOG=cti.log
CTI_MAN_DIR=man
CTI_SCRIPT=set_cti.sh
CTI_SERVICES_SCRIPT=set_services.sh
CTR_COMMON=ctr-common
CTS_DIR=cts
DATA_DIR=data
DESTINATION_BIN=${CTI_ROOT}/bin
DESTINATION_CFG=${CTI_ROOT}/${CFG_DIR}
DESTINATION_COMMON=${CTI_ROOT}/$(CTR_COMMON)
DESTINATION_COMMON_TOOLS=${CTI_ROOT}/common-tools
DESTINATION_CTS=${CTI_ROOT}/${CTS_DIR}
DESTINATION_DEMO_DIR=${CTI_ROOT}/demo
DESTINATION_INCLUDE=${CTI_ROOT}/include
DESTINATION_LIB=${CTI_ROOT}/lib
DESTINATION_MAN_DIR=${CTI_ROOT}/man
DESTINATION_PYTHON_EXT=${DESTINATION_LIB}/python
DESTINATION_TEMP=${CTI_ROOT}/ctr-temp
DESTINATION_THIRD_PARTY=${CTI_ROOT}/third-party
DOC_DIR=doc
PLATFORM_SPECIFIC=-fPIC -g -O2
PLUGINS_DIR=plugins
SOURCE_DIR=src
TEST_DIR=tests
UID_TOOL=uuidgen

