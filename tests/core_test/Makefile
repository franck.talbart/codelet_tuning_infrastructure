#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

TRUNK_DIR = $(CURDIR)/../../
SRC_DIR = $(TRUNK_DIR)/src
THIRD_PARTY = $(TRUNK_DIR)/third-party
DEFAULT_FLAGS = -Werror -Wall -std=gnu99 -I ../../src
PLATFORM_SPECIFIC = -fPIC -g -lpthread
LIB_CTICOMMON = -L$(BUILD_DIR)/lib -lcticommon  -I$(SRC_DIR)/common -I$(SRC_DIR)/common/platform -I$(SRC_DIR)/lib-cti -I$(BUILD_DIR)/
CC = gcc
LIB_CTI = -L$(BUILD_DIR)/lib -lcti -I$(SRC_DIR)/lib-cti
LIB_CTR = -L$(BUILD_DIR)/lib -lctr -I$(SRC_DIR)/lib-ctr
SANDBOX = $(CURDIR)/sandbox
BUILD_DIR = $(CURDIR)/sandbox/build.tmp

all:: clean prepare_sandbox build

prepare_sandbox:
	@echo "*****************************************************************************************************"
	@echo "Prepare Sandbox."
	@rm -rf $(SANDBOX)
	@mkdir -p $(SANDBOX)
	$(MAKE) -C $(TRUNK_DIR) CTI_ROOT=$(SANDBOX) BUILD_DIR="$(BUILD_DIR)" clean all install OVERWRITE=1
	@echo "Sandbox succefully prepared!"

build:: test.config_parser test.get_current_dir test.cti_uid test.get_path_by_cti_uid test.global_index test.alias test.concat_strings 

debug:
	$(MAKE) DEFAULT_FLAGS="-g $(DEFAULT_FLAGS)" all BUILD_DIR="$(BUILD_DIR)"

clean:
	rm -rf build.log
	rm -rf *.out
	rm -rf *.dSYM
	rm -rf logs
	rm -rf sandbox

test.global_index:: common.a
	echo "" > sandbox/cfg/global_index_file.txt
	$(CC) $(DEFAULT_FLAGS) $(PLATFORM_SPECIFIC) $(LIB_CTICOMMON) test_global_index_file.c -o global_index.out $(LIB_CTI)

test.alias:: libcti.a
	$(CC) $(DEFAULT_FLAGS) $(PLATFORM_SPECIFIC) test_alias.c -o alias.out $(LIB_CTI) $(LIB_CTICOMMON)

test.cti_uid: common.a
	$(CC) $(DEFAULT_FLAGS) $(PLATFORM_SPECIFIC) test_cti_uid.c -o cti_uid.out $(LIB_CTICOMMON) $(LIB_CTI)

test.get_current_dir: common.a
	$(CC) $(DEFAULT_FLAGS) $(PLATFORM_SPECIFIC) test_get_current_dir.c -o get_current_dir.out $(LIB_CTICOMMON)

test.config_parser: common.a
	$(CC) $(DEFAULT_FLAGS) $(PLATFORM_SPECIFIC) test_parser.c -o parser.out $(LIB_CTICOMMON)

test.get_path_by_cti_uid: libcti.a
	$(CC) $(DEFAULT_FLAGS) $(PLATFORM_SPECIFIC) test_get_path_by_cti_uid.c -o get_path_by_cti_uid.out $(LIB_CTI) $(LIB_CTICOMMON)

test.concat_strings: common.a
	$(CC) $(DEFAULT_FLAGS) $(PLATFORM_SPECIFIC) test_concat_strings.c -o concat_strings.out $(LIB_CTICOMMON)

libcti.a:
	@echo "*****************************************************************************************************"
	@echo "Building exposed libraries."
	$(MAKE) -s -C $(SRC_DIR)/lib-cti clean all BUILD_DIR="$(BUILD_DIR)" PLATFORM_SPECIFIC="$(PLATFORM_SPECIFIC)"

libctr.a:
	@echo "*****************************************************************************************************"
	@echo "Building exposed libraries."
	$(MAKE) -s -C $(SRC_DIR)/lib-ctr clean all BUILD_DIR="$(BUILD_DIR)" PLATFORM_SPECIFIC="$(PLATFORM_SPECIFIC)"

common.a:
	@echo "*****************************************************************************************************"
	@echo "Building core libraries."
	$(MAKE) -s -C $(SRC_DIR)/common clean all BUILD_DIR="$(BUILD_DIR)" PLATFORM_SPECIFIC="$(PLATFORM_SPECIFIC)"
