/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "cti_const.h"
#include "cti_uid.h"
#include "cti_plugin_api.h"

void set_cti_env_vars()
{
  setenv(CTI_ROOT, "./sandbox/", 1);
  setenv(CTI_CFG, "./sandbox/cfg", 1);
}


void test_uid() {
   CTI_UID *uid1 = cti_generate_cti_uid();
   assert(uid1 != NULL);
   CTI_UID *uid2 = cti_generate_cti_uid();
   assert(uid2 != NULL);
   char *str_uid1 = cti_uid_to_str(uid1);
   char *str_uid2 = cti_uid_to_str(uid2);
   int size1 = strlen(str_uid1);
   int size2 = strlen(str_uid2);
   assert(size1 == UID_NUMBER_OF_SYMBOL);
   assert(size2 == UID_NUMBER_OF_SYMBOL);
   printf("size:%d uid:%s\n", size1, str_uid1);
   printf("size:%d uid:%s\n", size2, str_uid2);
   int equals = strcmp(str_uid1, str_uid2);
   assert(equals);
   free(str_uid1);
   free(str_uid2);
   free(uid1);
   free(uid2);
}

int main()
{
  set_cti_env_vars();
  test_uid();
  return 0;
}
