# Implementation: Yuriy Kashnikov, Franck Talbart
#
# (C)opyright, Exascale Computing Research Center (Intel/CEA/GENCI/UVSQ), 2010-2015
# All rights reserved


import sys
from cti_hapi.main import HapiPlugin

class DescriptionPlugin(HapiPlugin):
    pass

if __name__ == "__main__":
    p = DescriptionPlugin()
    result = p.main(sys.argv)
    exit(result)
