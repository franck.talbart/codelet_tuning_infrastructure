#!/bin/bash

CTI_PLUGIN_DIR=$1
MAIN_SCRIPT="${CTI_PLUGIN_DIR}/main.py"
python "$MAIN_SCRIPT" $@
