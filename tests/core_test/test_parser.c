/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "util.h"
#include "cti_table.h"

void test_parser() {
  printf("Started paring\n");
  cti_table *table = NULL;
  table = cti_table_load_from_file("test_parse.input");
  printf("TEST: stored:%d size:%d\n", table->stored, table->size);
  for (uint32_t i = 0; i < table->stored; ++i) {
    printf("TEST: key:%s value:%s\n",
           (char*)table->data[i]->key,
           (char*)table->data[i]->value);
  }
  cti_table_destruct(table);
}

void test_xstrtrim() {
  const char *test_str = "\t\t \n.hello \t \t  \n";
  assert(!strcmp(xstrtrim(test_str), ".hello"));
}

int main()
{
  //  test_xstrtrim();
  test_parser();
  return 0;
}
