/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <unistd.h>

#include "ctr_util.h"
#include "util.h"
#include "ctr_path.h"
#include "global_index.h"
#include "cti_const.h"
#include "cti_uid.h"

void set_cti_env_vars()
{
    setenv(CTI_ROOT, "./sandbox/", 1);
    setenv(CTI_CFG, "./sandbox/cfg", 1);
}

void test_global_index(char* rep)
{
    CTI_UID *uid = cti_generate_cti_uid();
    assert(uid != NULL);

    int result = global_index_file_write(rep, uid);

    free(uid);

    assert(result == 0);
    void *gi_file = global_index_file_load();
    void *it = global_index_file_it_begin(gi_file);
    assert(it != NULL);
    while (!global_index_file_it_is_end(it))
    {
        char *value = global_index_file_it_value(it);
        assert(value != NULL);
        printf("RESULT: %s\n", value);
        free(value);
        it = (char**) global_index_file_it_next(it);
    }
    global_index_file_unload(gi_file);
}

void test_get_path()
{
    char *uid1_str = "09732395-7b19-4f51-9d6f-9b6c55d9a239";
    char *uid2_str = "120ceae9-c9b7-4bf0-8b8f-acb38ee0749c";
    CTI_UID *uid1 = str_to_cti_uid(uid1_str);
    CTI_UID *uid2 = str_to_cti_uid(uid2_str);
    // first test
    char *path1 = get_path_by_uid(CTR_ENTRY_DATA, uid1);
    assert(path1 != NULL);
    printf("path1 = %s", path1);
    // second test

    char *path2 = get_path_by_uid(CTR_ENTRY_PLUGIN, uid2);
    assert(path2 != NULL);
    printf("path2 = %s", path2);
    free(uid1);
    free(uid2);
    free(path1);
    free(path2);
}

int main()
{
    const int buf_size = 1024;
    char *cur_dir = malloc(buf_size);
    cur_dir = getcwd(cur_dir, buf_size);
    assert(cur_dir != NULL);
 
    char *rep = concat_strings(cur_dir, CTI_SLASH, "test_repository",
            CTI_SLASH, ".ctr");

    set_cti_env_vars();
    test_global_index(rep);
    test_get_path();
    global_index_file_rm(rep);
    free(cur_dir);
    free(rep);
    return 0;
}
