/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "cti_plugin_api.h"

void test_get_path_by_uid() {
  //  char *current = get_current_dir();
  //  setenv("CTI_ROOT", concat_strings(current, "/../"), 1);
  //  setenv("CTI_CFG", concat_strings(current, "/../cfg"), 1);
  //char *uid = "76246758394615263748";
  //char *path = cti_get_path_by_uid(uid);
  //  printf("TEST:%s\n", path);
  printf("UNIMPLEMENTED\n");
}

int main()
{
  test_get_path_by_uid();
  return 0;
}
