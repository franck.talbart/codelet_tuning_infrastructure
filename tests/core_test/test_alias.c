/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>

#include "ctr_util.h"
#include "util.h"
#include "ctr_path.h"
#include "cti_uid.h"
#include "alias.h"

#include "cti_const.h"
#include "cti_plugin_api.h"

void set_cti_env_vars()
{
  setenv(CTI_ROOT, "./sandbox/", 1);
  setenv(CTI_CFG, "./sandbox/cfg", 1);
}

void test_data_alias()
{
  // write data
  const char* foo_key = "foo";
  char* foo_str = "e190d332-7fb4-4504-a57e-6529dd866bae";
  CTI_UID* foo_value = str_to_cti_uid(foo_str);
  alias_data_set_value(foo_key, foo_value);
  free(foo_value);
}

int main()
{
  char* current_path = get_current_dir();
  printf("Current dir:%s\n", current_path);
 
  set_cti_env_vars();
  test_data_alias();
  return 0;
}
