#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

# define all necessary variables
TEST_DIR=$PWD
CORE_TEST=${TEST_DIR}/core_test
TRUNK_DIR=${TEST_DIR}/../
TEST_BUILD_LOG=${CORE_TEST}/build.log
SANDBOX=${CORE_TEST}/sandbox
BUILD_DIR=${SANDBOX}/build.tmp

# build and install tests
echo "******************************************************"
echo "Preparing sandbox and building Core Tests ...         "
make -C core_test SANDBOX=${SANDBOX} 1>${TEST_BUILD_LOG}

export DESTINATION_THIRD_PARTY=third-party
export force=1
export CTI_ROOT=${SANDBOX}
export CTI_BUILD_DIR=${BUILD_DIR}
export SOURCE_THIRD_PARTY=${TRUNK_DIR}/third-party

${TRUNK_DIR}/third-party/install.sh

source ${SANDBOX}/set_environment.sh
echo "******************************************************"

#run tests
cd ${CORE_TEST}
mkdir logs
echo " "
echo "======================================================"
echo "========= CTI Test System Core Tests Report =========="
echo " "

RESULT=0

for TEST_NAME in $(ls *.out) 
do
    ./$TEST_NAME  1>"logs/${TEST_NAME}.log" 2>&1
    TEST_RESULT=$?
    TEST_MSG="TEST: [${TEST_NAME}] STATUS:"
    if [ $TEST_RESULT == 0 ]; then
    echo "$TEST_MSG PASSED"
    else
    echo "$TEST_MSG FAILED"
    RESULT=1
    fi
done
echo "======================================================"
cd $TEST_DIR

exit ${RESULT}
