#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

###
### This file should be executed with nosetests --with-xunit to generate 
### xml test report.
import subprocess,os

demo_name="all.sh"
cts_test_dir = "./core_test/sandbox/cts/tests"

def test_cti_exists():
    p = subprocess.Popen("cti > /dev/null", shell=True)
    sts = os.waitpid(p.pid, 0)[1]
    assert(sts == 0)

def get_plugins_uid():
    """
    returns a list with all the plugins uids.
    """
    output = subprocess.Popen(["cti", "--user=admin", "21232f297a57a5a743894a0e4a801fc3", "self_test", "list"], 
                              shell=False, 
                              stdout=subprocess.PIPE).communicate()[0]
    #f = file("/tmp/debug", "w")
    #f.write(output+"\n")
    #f.close()

    # separate the lines
    output = output.split("\n")
    for l in output:
        if l.strip() != "": yield l.strip()

def get_demo_paths():
    p = subprocess.Popen(["find", "../demo/", "-name", demo_name], 
                         shell=False, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         cwd="%s/demo"%(os.environ["CTI_ROOT"]))
    stdout,stderr = p.communicate()
    for p in stdout.split("\n"):
        if p: yield p[:-len(demo_name)]


def test_all_core():
    for test_name in os.listdir("core_test"):
        if test_name.endswith(".out"):
            yield run_core, test_name

def test_all_plugin():
    for uid in get_plugins_uid():
        yield run_plugin, uid

def test_all_demo():
    for demo_path in get_demo_paths():
        yield run_demo, demo_path

def test_all_cts():
    for test_name in os.listdir(cts_test_dir):
        if test_name.endswith(".php"):
            yield run_cts, test_name

def run_demo(demo_path):
    p = subprocess.Popen(["./%s"%demo_name], 
                        shell=False, stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                        cwd=demo_path)
    stdout,stderr = p.communicate()
    if p.returncode != 0 :
        message = ("Got return code of %s.\n\nSTDOUT\n======\n%s\n\nSTDERR\n"
                "======\n%s" % (p.returncode, stdout, stderr))
        raise AssertionError(message)

def run_core(fname):
    p = subprocess.Popen(["./" + fname], shell=False, stdout=subprocess.PIPE,
                                                   stderr=subprocess.PIPE,
                                                   cwd="core_test/")
    stdout,stderr = p.communicate()
    if p.returncode != 0 :
        message = ("Got return code of %s.\n\nSTDOUT\n======\n%s\n\nSTDERR\n"
                "======\n%s" % (p.returncode, stdout, stderr))
        raise AssertionError(message)

def run_plugin(uid):
    p = subprocess.Popen(["cti", "--user=admin", "21232f297a57a5a743894a0e4a801fc3", "self_test", "single", str(uid), " --color=false"], 
                         shell=False, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    stdout,stderr = p.communicate()
    if p.returncode != 0 :
        message = ("Got return code of %s.\n\nSTDOUT\n======\n%s\n\nSTDERR\n"
                "======\n%s" % (p.returncode, stdout, stderr))
        raise AssertionError(message)
    
def run_cts(testfile):
    p = subprocess.Popen("cd " + cts_test_dir + " && phpunit --stderr " + testfile,
                         shell=True, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    stdout,stderr = p.communicate()

    if p.returncode != 0 :
        message = ("Got return code of %s.\n\nSTDOUT\n======\n%s\n\nSTDERR\n"
                "======\n%s" % (p.returncode, stdout, stderr))
        raise AssertionError(message)
