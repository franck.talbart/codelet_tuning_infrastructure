#!/bin/bash

#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

CTI_PATH=.
PATH_INSTALL=$CTI_PATH/install-tools
INSTALLER=install.sh

if [ ! -e $PATH_INSTALL/$INSTALLER ]; then
    echo ""
    echo "Error: Can't find installer for CTI or CTI-powered tools in $PATH_INSTALL"
    echo "       Please, update $INSTALLER script with the correct path!"
    echo ""
    exit 1
fi

#Launch installer for CTI-powered Tools
CTI_CUR_INSTALL_DIR=$PWD
pushd $CTI_PATH > /dev/null
. $PATH_INSTALL/$INSTALLER $CTI_CUR_INSTALL_DIR $@
popd > /dev/null
