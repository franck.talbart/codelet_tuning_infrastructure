#!/bin/sh
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

VERSION_FILE=$1

#Try to get git version hash tag
GITBUILDNUMBER=`git rev-parse --short HEAD`
if [ $? -eq 0 ]; then
    CTI_VERSION_BUILD_NUMBER=$GITBUILDNUMBER
else
    CTI_VERSION_BUILD_NUMBER="UNDEFINED"
fi

# Import version information in ../VERSION
. $VERSION_FILE

# Print version
echo "$CTI_VERSION_MAJOR.$CTI_VERSION_MINOR.$CTI_VERSION_HOT_FIXES.$CTI_VERSION_BUILD_NUMBER"
