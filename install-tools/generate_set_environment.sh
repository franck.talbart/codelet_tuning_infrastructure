
# Autocompletion feature

_cti()
{
    local argc cur opts;
    COMPREPLY=();
    argc=${COMP_CWORD};
    cur="${COMP_WORDS[$argc]}";
    if [ $argc -eq 1 ]; then
        result=`cti list plugins --format=raw`
        result+=" --version --help";
    elif [ $argc -eq 2 ]; then
        plugin=${COMP_WORDS[1]}; result=`cti $plugin help | awk '/cti '$plugin' / {print $4}'`
    elif [ $argc -gt 2 ]; then
        plugin=${COMP_WORDS[1]}; command=${COMP_WORDS[2]}; nb=$(( 2 + $argc ))
        result=`cti $plugin help | awk '/cti '$plugin' '$command' / {print $'$nb'}'`
    else
        result='';
    fi
    opts="$result"; COMPREPLY=( $(compgen -W "$opts" -- $cur ) )

}
complete -F _cti cti

# Aliases
cdctidata()
{
    cd `cti view data $1 | grep "Path   " | cut --delimiter=":" --fields=2`
}

export -f cdctidata

cdctiplugin()
{
    cd `cti view plugin $1 | grep "Path: " | cut --delimiter=":" --fields=2`
}

export -f cdctiplugin

cdctirepository()
{
    a="`echo $1 | awk '{print tolower($0)}'`"
    cd `cti list local_repositories  | grep $a  | cut --delimiter=" " --fields=4`/../
}

export -f cdctirepository

last_cti_entry()
{
    cti view data `cti user whoami raw` | grep "Last_Entry_Created" | awk '{print $NF}'
}

export -f last_cti_entry

