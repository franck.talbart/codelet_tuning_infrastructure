#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

#functions
function press_enter
{
    echo ""
    if [ $1 -eq 0 ] ; then
        if [ "$2" = "" ] ; then
            echo "Press <Enter> to continue."
        else
            echo $2
        fi
        read var1
        if [ -z $var1 ] ; then
            return 0
        else 
            return 1
        fi
    fi
    return 0
}

function cls
{
    if [ $1 -eq 0 ] ; then
        clear
    fi
}

#installation script
force=0
if [ "$2" = "-f" ] ; then
    force=1
fi

rm_makefile=1
if [ "$3" = "-no_rm_makefile" ] ; then
    rm_makefile=0
fi

CUR_DIR=$PWD

cls $force

#read configuration data
if [ -z "$1" ] ; then
    echo "Error: first parameter in command line should point to the directory with the configuration file."
    exit 1
fi

CTI_CUR_INSTALL_DIR=$1/install-tools
FILE_CFG="$CTI_CUR_INSTALL_DIR/install.cfg"

echo ""
echo "Reading configuration data"

if [ ! -e $FILE_CFG ] ; then
    echo ""
    echo ""
    echo "Error: can't find file '$FILE_CFG' with installation data."
    exit 1
fi

source $FILE_CFG

#checking variables
if [ -z "$SEP" ] ; then
    echo ""
    echo ""
    echo "Error: corrupted installation data: SEP variable is not defined."
    exit 1
fi
if [ -z "$CTI_LICENSE" ] ; then
    echo ""
    echo ""
    echo "Error: corrupted installation data: CTI_LICENSE variable is not defined."
    exit 1
fi
if [ -z "$CTI_TITLE1" ] && [ -z "$CTI_TITLE2" ] && [ -z "$CTI_TITLE3" ] &&
    [ -z "$CTI_TITLE4" ]; then
echo ""
echo "" 
echo "Error: corrupted installation data: CTI_TITLE variable is not defined."
exit 1
fi

cls $force

echo "$SEP"
echo "$CTI_TITLE1"
echo "$CTI_TITLE2"
echo "$CTI_TITLE3"
echo "$CTI_TITLE4"
echo "$SEP"
echo "You are going to install CTI."
press_enter $force
cls $force

#license
if [ ! -e $CTI_LICENSE ] ; then
    echo "Error: can't find file '$CTI_LICENSE' with CTI license."
    exit 1
fi

#reading license
if [ $force -eq 0 ] ; then
    less $CTI_LICENSE
else
    cat $CTI_LICENSE
fi

#checking agreement
press_enter $force "Please, press <Enter> IF YOU ACCEPT the license or anything else if you reject it." 

if [ $? -eq 1 ] ; then
    echo ""
    echo "License rejected. Installation terminated."
    exit 1
fi

echo "$SEP"
echo "License accepted. Installation will continue."

#checking if environment for CTI directory exists
EXISTS=0

if [ -n $CTI_ROOT ] ; then
    if [ -e $CTI_ROOT/$CTI_ENV ] ; then
        EXISTS=1
    fi
fi

if [ "$EXISTS" = "0" ] ; then
    #possibly clean installation

    if [ "$CTI_ROOT" = "" ] ; then
        echo ""
        echo "Environment variable CTI_ROOT is empty, hence we assume new installation."
        CTI_ROOT=$CTI_INSTALL_DIR
    fi

    echo ""
    echo "We suggest installing CTI to the following directory:"
    echo $CTI_ROOT
    echo ""
    
    if [ $force -eq 0 ] ; then
        echo "Input the path for CTI installation or leave it empty to accept to above suggestion:"
        read var
        if [ ! -z "$var" ] ; then
            CTI_ROOT=$var
        fi        
    fi
    
    #check if installation exists
    if [ -n $CTI_ROOT ] ; then
        if [ -e $CTI_ROOT/$CTI_ENV ] ; then
            EXISTS=1
        fi
    fi
fi

#perform actions depending if installation exists or not
OVERWRITE=0
if [ "$EXISTS" = "1" ] ; then
    echo ""
    echo "WARNING: existing CTI installation found."
    echo ""
    echo "Press <Enter> to keep configuration files and overwrite only the rest."    
    press_enter $force "Otherwise enter anything else to overwrite ALL files (configuration files will be backed up)." 
    
    if [ $? -eq 1 ] ; then
        echo ""
        echo "All files including configuration will be overwrited!"

        echo "Are you sure (Y/N)?"
        read ver

        if [ "$ver" = "y" ] || [ "$ver" = "Y" ]; then 
            echo "Overwriting all files including configuration ..."
            OVERWRITE=1
        fi
    fi
fi
export CTI_ROOT

if [ "$CTI_BUILD_DIR" == "" ] ; then
    CTI_BUILD_DIR=$CTI_ROOT/build.tmp
fi
export CTI_BUILD_DIR

echo ""
echo "$SEP"
echo "Ready to check CTI pre-requisites"
echo "$SEP"
echo ""

ls $HOME &> /dev/null
if test $? != 0; then
    echo "No home path detected."
    exit 1
fi

./configure
if [ "${?}" != "0" ] ; then
    exit 1
fi

#Checking uuid
uuid &> /dev/null
if [  $? -eq 0 ] ; then
	UID_TOOL="uuid"
else 
	UID_TOOL="uuidgen"
fi
echo ""
echo "Please, check the above information and install missing packages if necessary!"
press_enter $force

echo ""
echo "$SEP"
echo "Ready to compile CTI."
echo "$SEP"
echo ""

#Install CTI base
echo "Installing CTI core ..."
echo "  INSTALL DIR = $CTI_ROOT"
echo "  BUILD DIR   = $CTI_BUILD_DIR"

echo ""
echo -n "* Cleaning up the installation ...                         "
make clean BUILD_DIR=$CTI_BUILD_DIR
if [ "${?}" != "0" ] ; then
	echo -e "\\033[1;31m" "[KO]" "\\033[0;39m"
    echo ""
    echo "Error: CTI compilation failed!"
    exit 1
fi
echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
echo ""
make BUILD_DIR=$CTI_BUILD_DIR
if [ "${?}" != "0" ] ; then
	echo -e "\\033[1;31m" "[KO]" "\\033[0;39m"
    echo ""
    echo "Error: CTI compilation failed!" 
    exit 1
fi

echo "Build process finished."

echo "$SEP"
echo "Installing CTI ..."
echo "$SEP"

make install OVERWRITE=$OVERWRITE BUILD_DIR=$CTI_BUILD_DIR  UID_TOOL=$UID_TOOL 
if [ "${?}" != "0" ] ; then
	echo -e "\\033[1;31m" "[KO]" "\\033[0;39m"
    echo ""
    echo "Error: CTI installation failed!" 
    exit 1
fi
if [ $? -eq 1 ] ; then
    echo ""
    echo "Installation process terminated."
    exit 1
fi

# Install third party modules

export DESTINATION_THIRD_PARTY
export SOURCE_THIRD_PARTY="third-party"
export force
third-party/install.sh 

# Adding codes to set_environment.sh
cat $CTI_CUR_INSTALL_DIR/generate_set_environment.sh >> $CTI_ROOT/$CTI_ENV

#Sourcing CTI environment for further installation
echo "$SEP"
echo "Setting CTI environment"
echo ""
. $CTI_ROOT/set_environment.sh

echo "$SEP"
echo "Database generation"
echo "$SEP"
#Indexing repositories
cti reindex all

# Demo repositories
if [ -n $CTI_ROOT/$REPO_DEMO_DIR ] && [ `ls $CTI_CUR_INSTALL_DIR/../$REPO_DEMO_DIR/*.tar.gz &> /dev/null | wc -l` -ne 0 ]; then
    
    if [ -e $CTI_ROOT/$REPO_DEMO_DIR ]; then
        echo "Deleting previous demo repositories"
        for repo in `ls $CTI_ROOT/$REPO_DEMO_DIR/` ; do
            if [ -e $CTI_ROOT/$REPO_DEMO_DIR/$repo/.ctr ] ; then
                # Clean previous repository
                echo "* Deleting demo repository \"$repo\""
                cti --user=admin 21232f297a57a5a743894a0e4a801fc3 rm repository $repo --force=True
            fi
        done
        rm -rf $CTI_ROOT/$REPO_DEMO_DIR
    fi
    
    cp -rf $CTI_CUR_INSTALL_DIR/../$REPO_DEMO_DIR $CTI_ROOT
    cd $CTI_ROOT/$REPO_DEMO_DIR
    echo "Creating the demo repositories"
    for repo in `ls *.tar.gz` ; do
        repo_name=`basename $repo .tar.gz`
        tar xf $repo
        rm $repo
        cd $repo_name
        echo "* Import demo repository \"$repo_name\""
        cti --user=admin 21232f297a57a5a743894a0e4a801fc3 repository import $repo_name
        cd - &> /dev/null
    done
    cd $CUR_DIR
    echo "Demo repositories are created!"
fi
echo "$SEP"
#Install CTS
if [ "$INSTALL_CTS" == "1" ] ; then
    pushd cts  &> /dev/null
    make install OVERWRITE=$OVERWRITE BUILD_DIR=$CTI_BUILD_DIR CTI_SOURCES=$CUR_DIR CTS_LIGHTTPD_PORT=$CTS_LIGHTTPD_PORT
	if [ "${?}" != "0" ] ; then
		echo -e "\\033[1;31m" "[KO]" "\\033[0;39m"
	fi
    popd  &> /dev/null
fi

echo "$SEP"
echo ""
echo -n "* Cleaning up the CTI sources ...                          "
if [ "$rm_makefile" == "1" ] ; then
	rm -f $CUR_DIR/Makefile
	rm -f $CUR_DIR/cts/Makefile
	rm -f $CUR_DIR/src/Makefile
	rm -f $CUR_DIR/src/common/Makefile
	rm -f $CUR_DIR/src/common/platform/Makefile
	rm -f $CUR_DIR/src/fe/Makefile
	rm -f $CUR_DIR/src/lib-cti/Makefile
	rm -f $CUR_DIR/src/lib-cti/python/Makefile
	rm -f $CUR_DIR/src/lib-ctr/Makefile
	rm -f $CUR_DIR/src/lib-ctr/python/Makefile
	rm -f $CUR_DIR/config.status
fi
rm -f $CUR_DIR/config.log
rm -f $CUR_DIR/libtool
rm -rf $CUR_DIR/autom4te.cache 
echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"

#Sourcing CTI environment for further installation
echo "$SEP"
echo "Setting CTI services"
echo ""
. $CTI_ROOT/set_services.sh

# Finish
echo ""
echo "Installation of CTI and related tools is finished!"
echo ""
echo "$SEP"
echo "Checking CTI installation (executing 'cti --version'):"
echo ""
cti --version

echo "$SEP"
if [ "$INSTALL_CTS" == "1" ] ; then
    echo "You can access the web-user interface at the following url: http://localhost:$CTS_LIGHTTPD_PORT"
    echo ""
fi
echo "Warning: Do not forget to source $CTI_ROOT/set_cti.sh before using CTI and related tools."
echo "         You can modify $CTI_ROOT/set_environment.sh to update CTI environment variables."
echo ""
echo "Feedback about CTI and related tools is always appreciated (https://github.com/franck-talbart/codelet_tuning_infrastructure/wiki)."
echo "The default username is: 'admin'. The default password is: 'admin'. Please change it as soon as possible and create"
echo "a new user by typing 'cti user init'."

