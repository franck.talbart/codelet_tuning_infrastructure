#!/bin/bash

#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

# Install third party modules

function press_enter
{
    echo ""
    if [ $1 -eq 0 ] ; then
        if [ "$2" = "" ] ; then
            echo "Press <Enter> to continue."
        else
            echo $2
        fi
        read var1
        if [ -z $var1 ] ; then
            return 0
        else 
            return 1
        fi
    fi
    return 0
}

var_enter=0
echo ""
echo "**************************************************************************************"
echo "Installing third-party modules ..."

echo -n "* MAQAO (Exascale Computing Research)                      "
if [ -e $SOURCE_THIRD_PARTY/MAQAO ]; then 
    make DESTINATION_THIRD_PARTY="$CTI_ROOT/$DESTINATION_THIRD_PARTY"  BUILD_DIR=$CTI_BUILD_DIR \
     -C $SOURCE_THIRD_PARTY/MAQAO -f Makefile-Maqao.cti  >> $CTI_BUILD_DIR/cti_compil.log 2>&1
    echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
    echo ""
else
    echo -e "\\033[1;31m" "[KO]" "\\033[0;39m"
    echo "MAQAO not found."; 
    echo "You can find the tool here: http://www.maqao.org/" 
    echo "Install the tool in the $SOURCE_THIRD_PARTY directory ($SOURCE_THIRD_PARTY/maqao/)."
    echo ""
    var_enter=1
fi

if [ $var_enter -eq 1 ] ; then
    echo "Please, check the above information and install missing packages if necessary!"
    press_enter $force
fi
