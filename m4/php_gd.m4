# Copyright (C) 2010 Curtis C. Hovey
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright notice
# and this notice are preserved.

AC_DEFUN([AC_CHECK_PHP_GD],[
    MODULE_NAME=$1
    AC_MSG_CHECKING(for php-gd module $MODULE_NAME)
    php="php"
   	$php -q m4/php_gd.php 2>/dev/null
    if test $? -eq 0; then
        AC_MSG_RESULT(yes)
    else
        php="php5"
        $php -q m4/php_gd.php 2>/dev/null
        if test $? -eq 0; then
            AC_MSG_RESULT(yes)
        else
            AC_MSG_RESULT(no)
             AC_MSG_ERROR([php-gd was not found, but is required to install CTI (if you plan to use CTS).])
        fi
    fi
])

