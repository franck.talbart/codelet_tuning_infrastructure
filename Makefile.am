# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

# Default parameters
AUTOMAKE_OPTIONS = foreign
ACLOCAL_AMFLAGS = -I m4

include config.mk

define COPYRIGHT
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

endef

export COPYRIGHT

all: check_cti_root
	@echo -n "* Creating temporary build directory ...                   "
	@mkdir -p $(BUILD_DIR)/bin
	@mkdir -p $(BUILD_DIR)/objs
	@mkdir -p $(BUILD_DIR)/lib
	@mkdir -p $(BUILD_DIR)/python
	@mkdir -p $(BUILD_DIR)/third-party
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
	@echo ""
	@echo -n "* Compiling CTI ...                                        "
	@$(MAKE) PLATFORM_SPECIFIC="$(PLATFORM_SPECIFIC)" BUILD_DIR="$(BUILD_DIR)" -C $(SOURCE_DIR)/ >> $(BUILD_DIR)/cti_compil.log 2>&1
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
	@echo ""

check_cti_root:
if CTI_ROOT_IS_NOT_DEFINED
	@echo "Error: Environment variable CTI_ROOT is not defined!" 
	exit 1
endif

clean::
if BUILD_DIR_IS_DEFINED
	@mkdir -p $(BUILD_DIR)/ 
	@$(MAKE) clean -C $(SOURCE_DIR) BUILD_DIR="$(BUILD_DIR)" >> $(BUILD_DIR)/cti_compil.log 2>&1  
	@$(MAKE) clean -C $(TEST_DIR)/core_test BUILD_DIR="$(BUILD_DIR)" >> $(BUILD_DIR)/cti_compil.log 2>&1 
	@rm -rf "$(BUILD_DIR)"
endif

install:: check_cti_root
# create directory structure
	@echo -n "* Creating directory structure ...                         "
	@if test ! -d "$(CTI_ROOT)"; then mkdir -p $(CTI_ROOT); fi
	@if test ! -d "$(DESTINATION_BIN)"; then mkdir -p $(DESTINATION_BIN); fi
	@if test ! -d "$(DESTINATION_LIB)"; then mkdir -p $(DESTINATION_LIB); fi
	@if test ! -d "$(DESTINATION_INCLUDE)"; then mkdir -p $(DESTINATION_INCLUDE); fi
	@if test ! -d "$(DESTINATION_CTS)"; then mkdir -p $(DESTINATION_CTS); fi
	@if test ! -d "$(DESTINATION_COMMON)"; then mkdir -p $(DESTINATION_COMMON); fi
	@if test ! -d "$(DESTINATION_TEMP)/$(DATA_DIR)"; then mkdir -p $(DESTINATION_TEMP)/$(DATA_DIR); fi
	@if test ! -d "$(DESTINATION_TEMP)/$(PLUGINS_DIR)"; then mkdir -p $(DESTINATION_TEMP)/$(PLUGINS_DIR); fi
	@if test ! -d "$(DESTINATION_THIRD_PARTY)"; then mkdir -p $(DESTINATION_THIRD_PARTY); fi
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
	@echo ""
	
# copy frontend
	@echo -n "* Copying front-end ($(CTI_FE_NAME)) ...                              "
	@cp -f $(BUILD_DIR)/bin/$(CTI_FE_NAME) $(DESTINATION_BIN)
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
	@echo ""

# copy include files
	@echo -n "* Copying include files ...                                "
	@cp $(SOURCE_DIR)/lib-cti/cti_plugin_api.h $(DESTINATION_INCLUDE)
	@cp $(SOURCE_DIR)/lib-ctr/ctr_plugin_api.h $(DESTINATION_INCLUDE)
	@cp $(SOURCE_DIR)/lib-cti/cti_error.h $(DESTINATION_INCLUDE)
	@cp $(SOURCE_DIR)/lib-cti/cti_const.h $(DESTINATION_INCLUDE)
	@cp $(SOURCE_DIR)/lib-cti/cti_types.h $(DESTINATION_INCLUDE)
	@cp $(BUILD_DIR)/cti_version.h $(DESTINATION_INCLUDE)
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
	@echo ""

# copy library files
	@echo -n "* Copying library files ...                                "
	@cp $(BUILD_DIR)/lib/libcti.a $(DESTINATION_LIB)
	@cp $(BUILD_DIR)/lib/libctr.a $(DESTINATION_LIB)
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
	@echo ""
	
# copy common tools
	@echo -n "* Copying common tools ...                                 "
	@rm -rf "$(DESTINATION_COMMON_TOOLS)"
	@cp -rf "$(CTI_COMMON_TOOLS_DIR)" "$(DESTINATION_COMMON_TOOLS)"
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"

# python extension
	@echo ""
	@echo "**************************************************************************************"
	@echo  "* Preparing python extension (to connect to CTI dynamic library) ..."
	@rm -rf "$(DESTINATION_LIB)/python"
	@mkdir "$(DESTINATION_LIB)/python"
	@cp -rf $(BUILD_DIR)/python/Site-cti/* $(DESTINATION_LIB)/python/
	@cp -rf $(BUILD_DIR)/python/Site-ctr/* $(DESTINATION_LIB)/python/
	@cp -rf $(SOURCE_DIR)/cti_hapi $(DESTINATION_LIB)/python/cti_hapi
	@cp -rf $(BUILD_DIR)/python/ctr.py $(DESTINATION_LIB)/python/
	@cp -rf $(BUILD_DIR)/python/cti.py $(DESTINATION_LIB)/python/
	@echo ""

# prepare configuration files
	@$(MAKE) config_cti
	
# prepare environment script
	@echo "**************************************************************************************"
	@echo "Preparing CTI configuration ..."
	@echo ""
	$(MAKE) script_cti

# common repository
	@echo "**************************************************************************************"
	@echo "* Preparing common repository ..."
	@echo ""
	@cp -rf $(CTR_COMMON)/* $(DESTINATION_COMMON)
		
	@$(DESTINATION_COMMON)/$(PLUGINS_DIR)/compile_plugins.sh $(DESTINATION_COMMON)/$(PLUGINS_DIR)
	
	@for i in `find "$(DESTINATION_COMMON)/$(PLUGINS_DIR)" -name "*.pyc"`; do \
		rm -f $$i; \
	done

# man pages
	@echo -n "* Copying man pages ...                                    "
	@rm -rf "$(DESTINATION_MAN_DIR)"
	@cp -rf "$(CTI_MAN_DIR)" "$(DESTINATION_MAN_DIR)"
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
	@echo ""

# demo directory
	@echo -n "* Copying demo directory ...                               "
	@rm -rf "$(DESTINATION_DEMO_DIR)"
	@cp -rf "$(CTI_DEMO_DIR)" "$(DESTINATION_DEMO_DIR)"
	@echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"

script_cti::
# prepare environment script
	@echo "* Preparing $(CTI_ENVIRONMENT_SCRIPT) script ..."
	@echo ""

	@if test -e "$(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)"; then \
		echo "* Saving current script to $(CTI_ENVIRONMENT_SCRIPT).bak ..."; \
		mv $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT) $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT).bak; \
	fi

	@echo "# CTI Environment" > $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)
	@echo "#" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)
	@echo "$$COPYRIGHT" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)

	@echo "" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)
	 
	@echo "" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)
	@echo "export CTI_ROOT=$(CTI_ROOT)" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)
	@echo "export CTI_CFG=$(DESTINATION_CFG)" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)

	@echo "" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)
	@echo "export PYTHONPATH=$(CTI_ROOT)/lib/python:$$""PYTHONPATH" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)
	@echo "export PATH=$(CTI_ROOT)/bin:$$""PATH" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)
	@echo "export MANPATH=$(CTI_ROOT)/man:$$""MANPATH" >> $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)

# create (or reset if previous installation) CTI_SERVICES_SCRIPT
	@echo "# CTI Services" > $(CTI_ROOT)/$(CTI_SERVICES_SCRIPT)
	@echo "#" >> $(CTI_ROOT)/$(CTI_SERVICES_SCRIPT)
	@echo "$$COPYRIGHT" >> $(CTI_ROOT)/$(CTI_SERVICES_SCRIPT)
	@echo "" >> $(CTI_ROOT)/$(CTI_SERVICES_SCRIPT)

# create CTI_SCRIPT
	@echo "$$COPYRIGHT" > $(CTI_ROOT)/$(CTI_SCRIPT)
	@echo "" >> $(CTI_ROOT)/$(CTI_SCRIPT)
	@echo ". $(CTI_ROOT)/$(CTI_ENVIRONMENT_SCRIPT)" >> $(CTI_ROOT)/$(CTI_SCRIPT)
	@echo ". $(CTI_ROOT)/$(CTI_SERVICES_SCRIPT)" >> $(CTI_ROOT)/$(CTI_SCRIPT)

config_cti::
# prepare configuration files (INDIVIDUALLY!)
	@echo "* Preparing individual configuration files ..."
	@echo ""

	@mkdir -p ${DESTINATION_CFG}

	@if ! test -e "$(DESTINATION_CFG)/$(CTI_GLOBAL_INDEX_FILE)"; then \
		touch $(DESTINATION_CFG)/$(CTI_GLOBAL_INDEX_FILE);\
	fi
	
	@if ! test -e "$(DESTINATION_CFG)/$(CTI_ALIAS_REPOSITORY)"; then \
		touch $(DESTINATION_CFG)/$(CTI_ALIAS_REPOSITORY);\
	fi	
	
	@if ! test -e "$(DESTINATION_CFG)/$(CTI_LOG)"; then \
		touch $(DESTINATION_CFG)/$(CTI_LOG);\
	fi	
	@chmod o+xw $(DESTINATION_CFG)/$(CTI_LOG)
	
# backup and copy configuration files
	@$(MAKE) config_cti_file CFG=$(CTI_CFG)
	
	@cp AUTHORS $(CTI_ROOT)/
	@cp README $(CTI_ROOT)/
	
# updating main configuration file
# here we expect that the file is now reset (copied from the original cfg),
# so that we avoid additional appending ...
	@echo ""
	@echo "* Updating main configuration file $(CTI_CFG)..."
	@echo "" >> ${DESTINATION_CFG}/$(CTI_CFG)
	@echo "CTR_COMMON_DIR = ${DESTINATION_COMMON}" >> ${DESTINATION_CFG}/$(CTI_CFG)
	@echo "CTR_TEMP_DIR = ${DESTINATION_TEMP}" >> ${DESTINATION_CFG}/$(CTI_CFG)
	@echo "CTR_PLUGIN_DIR = ${PLUGINS_DIR}" >> ${DESTINATION_CFG}/$(CTI_CFG)
	@echo "CTR_DATA_DIR = ${DATA_DIR}" >> ${DESTINATION_CFG}/$(CTI_CFG)	
	@echo "COMMON_TOOLS_DIR = ${DESTINATION_COMMON_TOOLS}" >> ${DESTINATION_CFG}/$(CTI_CFG)	
	@echo "UID_TOOL = ${UID_TOOL}" >> ${DESTINATION_CFG}/$(CTI_CFG)
	@echo "THIRD_PARTY_DIR = ${DESTINATION_THIRD_PARTY}" >> ${DESTINATION_CFG}/$(CTI_CFG)

	@chmod o+w ${DESTINATION_CFG}/$(CTI_CFG)

config_cti_file::
# backup and copy configuration files
	@echo ""
	@echo "* Copying $(CFG) ..."
	@if test -e "$(DESTINATION_CFG)/$(CFG)"; then \
		echo "Saving current configuration file to $(DESTINATION_CFG)/$(CFG).bak ..."; \
		mv $(DESTINATION_CFG)/$(CFG) $(DESTINATION_CFG)/$(CFG).bak; \
	fi
	@cp $(CFG_DIR)/$(CFG) $(DESTINATION_CFG)/$(CFG)

tests::
	cd $(TEST_DIR); bash runtest.sh

doc::
	@cp $(DOC_DIR)/Doxyfile $(DOC_DIR)/input 
	@echo "PROJECT_NUMBER = `./install-tools/print_cti_version.sh ./VERSION`"\
		>> $(DOC_DIR)/input
	doxygen $(DOC_DIR)/input
	$(MAKE) -C $(DOC_DIR)/latex
	@rm $(DOC_DIR)/input
