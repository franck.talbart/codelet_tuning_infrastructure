#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import description, entry, alias, plugin, submitter, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, datetime, tempfile, glob, shutil
import import_maqao_perf_results

class Maqao_perfPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ prepare parameters
        Args:
            self: class of the plugin
            params: working parameters
        """
        self.work_params = description.description_write(self.command, self.work_params)
        binary = self.work_params[self.command].params["binary"][cti.META_ATTRIBUTE_VALUE]
        granularity = self.work_params[self.command].params["granularity"][cti.META_ATTRIBUTE_VALUE]
        mode = self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE]
        platform = self.work_params[self.command].params["platform"][cti.META_ATTRIBUTE_VALUE]
        format_o = self.work_params[self.command].params["format"][cti.META_ATTRIBUTE_VALUE]
        auto_run = self.work_params[self.command].params["auto_run"][cti.META_ATTRIBUTE_VALUE]

        # check parameters
        if not binary:
            util.cti_plugin_print_error("The binary entry parameter is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS

        if granularity not in ['xsmall', 'small', 'medium', 'large']:
            util.cti_plugin_print_error("Unknown granularity %s." % granularity)
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS

        if not mode:
            mode = "local"
            self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE] = mode
        elif mode not in submitter.MODE_AVAIL:
            util.cti_plugin_print_error("Unknown mode.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS

        if mode != "local" and mode != "other" and not platform:
            util.cti_plugin_print_error("You must provide a platform on non-local mode.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS

        # create the alias
        binary_alias = alias.get_data_alias(binary)
        if not binary_alias:
            binary_alias = ""
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        alias_e = "MAQAO_perf_%s_%s" % (binary_alias, date)
        
        (data_entry, output) = self.default_init_command(params, alias_e = alias_e, no_output = True)
        entry.update_entry_parameter(binary, {"maqao_perf_entry": {"value": data_entry.uid}})
        res = plugin.plugin_auto_run(self, data_entry, format_o, auto_run, output)
        return res
    
    #---------------------------------------------------------------------------
    
    @hapi_command("run")
    def run_cmd(self, params):
        """ This function gets a MAQAO Perf entry (created using maqao_perf init command) and runs MAQAO Perf on it.
        
        Args:
            self: class of the plugin
            params: working parameters
            
        Returns:
            Nothing
        """      
        maqao_dir = os.path.join(cti.cti_plugin_config_get_value(cti.THIRD_PARTY_DIR), "maqao")
        if not os.path.isdir(maqao_dir):
            util.cti_plugin_print_error("MAQAO is not installed\nYou can find the tool here: http://www.maqao.org/")
            util.cti_plugin_print_error("Install the tool in the third-party directory (third-party/maqao/).")
            return cti.CTI_PLUGIN_ERROR_TOOL_NOT_FOUND
        
        # gets parameters
        maqao_perf_uid = params["entry"]
        nb_run_maqao_perf = params["nb_run_maqao_perf"]
        ug_parameter = params["ug"]
        loop_plugin_uid = params["loop_plugin_uid"]
        loop_group_plugin_uid = params["loop_group_plugin_uid"]
        
        (input_param, output_param) = entry.load_data(maqao_perf_uid)
        
        binary_uid = output_param["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        run_parameters = output_param["init"].params["run_parameters"][cti.META_ATTRIBUTE_VALUE]
        granularity = output_param["init"].params["granularity"][cti.META_ATTRIBUTE_VALUE]
        bypass_kernel_detection = input_param["init"].params["bypass_kernel_detection"][cti.META_ATTRIBUTE_VALUE]
        stability_sanity_check = output_param["init"].params["stability_sanity_check"][cti.META_ATTRIBUTE_VALUE]
        mode = output_param["init"].params["mode"][cti.META_ATTRIBUTE_VALUE]
        partition = output_param["init"].params["partition"][cti.META_ATTRIBUTE_VALUE]
        platform = output_param["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
        prefix_execute_script = output_param["init"].params["prefix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        suffix_execute_script = output_param["init"].params["suffix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        submitter_cmd = output_param["init"].params["submitter_cmd"][cti.META_ATTRIBUTE_VALUE]
        submitter_opt = output_param["init"].params["submitter_opt"][cti.META_ATTRIBUTE_VALUE]
        submitter_user = output_param["init"].params["submitter_user"][cti.META_ATTRIBUTE_VALUE]
        prefix_cmd = output_param["init"].params["prefix_cmd"][cti.META_ATTRIBUTE_VALUE]
        
        # Check parameters               
        if not bypass_kernel_detection or bypass_kernel_detection is None:
            bypass_kernel_detection = False
        
        if not run_parameters:
            run_parameters = None
        
        if not prefix_cmd:
            prefix_cmd = None

        compile_uid = None
        output_compile = None
        (_, output_binary) = entry.load_data(binary_uid)
        compile_uid = output_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]
        if compile_uid:
            (_, output_compile) = entry.load_data(compile_uid)
        
        ## get the binary path from the binary entry
        binary = output_binary["init"].params["binary_file"][cti.META_ATTRIBUTE_VALUE]
        binary_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, binary_uid), 
                                   cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                   binary)
        
        # get compiled application path
        compiled_application = None
        if output_compile:
            compiled_application = output_compile["init"].params["compiled_application"][cti.META_ATTRIBUTE_VALUE]
        if compiled_application and compile_uid:
            compiled_application_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, compile_uid), 
                                                     cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                                     compiled_application)
        else:
            compiled_application_path = None

        common_tool_dir = cti.cti_plugin_config_get_value(cti.COMMON_TOOLS_DIR)
 
        # create the arguments
        work_dir = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)
        hierarchy_script = os.path.join(self.plugin_directory, "print_loop_child.lua")
        parent_script = os.path.join(self.plugin_directory, "get_parent.lua")

        dict_opt = {}
        dict_opt["APP"] = [binary_path]
        dict_opt["SCRIPTS"] = [hierarchy_script, parent_script]
        if params["prefix"]:
            shutil.copy(params["prefix"], "/tmp/prefix.sh")
            dict_opt["SCRIPTS"].append("/tmp/prefix.sh")
        dict_opt["TOOL"] = [maqao_dir]
        if compiled_application_path:
            dict_opt["OTHER"] = [compiled_application_path]
        dict_opt["platform"] = platform
        dict_opt["entry"] = params["entry"]
        dict_opt["plugin_execute_script"] = os.path.join(self.plugin_directory, "execute_maqao_perf.py") 
        dict_opt["partition"] = partition
        dict_opt["submitter_cmd"] = submitter_cmd
        dict_opt["submitter_opt"] = submitter_opt
        dict_opt["submitter_user"] = submitter_user
        dict_opt["prefix_execute_script"] = prefix_execute_script
        dict_opt["suffix_execute_script"] = suffix_execute_script
        dict_opt["username"] = self.username

        # create the script parameters file
        dict_opt["script_params"] = tempfile.mkstemp(dir=work_dir)[1]
        script_params_f = open(dict_opt["script_params"], "w")
        script_params_f.write(str(nb_run_maqao_perf) + "\n")
        script_params_f.write(os.path.join(common_tool_dir, "set_freq.sh") + "\n")
        script_params_f.write(str(granularity) + "\n")
        script_params_f.write(str(stability_sanity_check) + "\n")
        if compiled_application_path:
            script_params_f.write(os.path.basename(compiled_application_path) + "\n")
        else:
            script_params_f.write("\n")
        script_params_f.write(os.path.basename(hierarchy_script) + "\n")
        script_params_f.write(os.path.basename(parent_script) + "\n")
        script_params_f.write(str(bypass_kernel_detection) + "\n")
        script_params_f.write(str(ug_parameter) + "\n")
        script_params_f.write(str(run_parameters) + "\n")
        script_params_f.write(str(prefix_cmd) + "\n")
        script_params_f.close()

        (job_dir, daemon_log_file) = submitter.submitter(dict_opt, self.plugin_uid, mode)

        execution_log_file = None
        # collect execution log file
        for f in glob.glob("*.out"):
            if f != daemon_log_file:
                execution_log_file = os.path.abspath(f)

        dict_opt["result_dir"] = os.path.join(work_dir, job_dir + "_local", job_dir, "RESULTS")
        maqao_perf_version = "NA"
        maqao_perf_version_path = os.path.join(dict_opt["result_dir"], "maqao_perf_version")
        if os.path.isfile(maqao_perf_version_path):
            maqao_perf_version_file = open(maqao_perf_version_path, "r")
            maqao_perf_version = " ".join(maqao_perf_version_file.readlines())
        else:
            print "Maqao Perf version file not found!"

        message_daemon = ""
        nb_files = 0
        for f in os.walk("RESULTS"): 
             nb_files += len(f[2])

        dict_opt["maqao_perf_uid"] = self.plugin_uid
        dict_opt["maqao_perf_version"] = maqao_perf_version
        dict_opt["execution_log"] = execution_log_file
        dict_opt["daemon_log"] = daemon_log_file
        dict_opt["plugin_dir"] = self.plugin_directory
        dict_opt["nb_run_maqao_perf"] = nb_run_maqao_perf
        dict_opt["loop_plugin_uid"] = loop_plugin_uid
        dict_opt["loop_group_plugin_uid"] = loop_group_plugin_uid
        dict_opt["stability_sanity_check"] = stability_sanity_check
        dict_opt["password"] = self.password
        dict_opt["binary_path"] = binary_path
        dict_opt["compiled_application"] = compiled_application
        dict_opt["compiled_application_path"] = compiled_application_path
 
        file_daemon = open(daemon_log_file, "a")

        if nb_files == 0:
            execution_log_f = open(execution_log_file, "r")
            execution_log_message = execution_log_f.readlines()
            execution_log_f.close()
            execution_log_message = " ".join(execution_log_message)

            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] ERROR: no csv file generated\n\tThe error message is:\n\
                    ==============================\n%s\n==============================\n\
                    Temporary files are located in the directory %s" \
                    % (date, execution_log_message, os.getcwd())
            util.cti_plugin_print_error(message)
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            import_maqao_perf_results.import_results(dict_opt)
        else:
            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] Importing results into CTI..." % date
            print message
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            import_maqao_perf_results.import_results(dict_opt)

            # cleaning temporary files
            shutil.rmtree(os.path.join(work_dir, job_dir + "_local"))


#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = Maqao_perfPlugin()
    exit(p.main(sys.argv))
