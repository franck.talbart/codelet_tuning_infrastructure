local args = Utils:get_args(arg);

if (args.bin == nil or args.loop == nil) then
   print (string.format ("Usage: maqao %s [bin=]path/to/bin loop=id1,id2...\n", arg[1]))
   os.exit (-1)
end

local prj = project.new ("get_asm")
local bin = prj:load (args.bin, 4)

-- file name is loop_id.asm if only one path, loop_id_path_id otherwise

local is_requested_loop = {};
for lid in string.gmatch (args.loop, "[^,]+") do
   is_requested_loop [tonumber (lid)] = true;
end

local function get_path_insns (path)
   local path_insns = {};
   for _,block in ipairs (path) do
      for insn in block:instructions() do
         table.insert (path_insns, insn);
      end
   end

   return path_insns;
end

local function get_loop_insns (loop)
   local main_path = {};
   for block in loop:blocks() do
      table.insert (main_path, block);
   end

   return get_path_insns (main_path);
end

for f in bin:functions() do
   for l in f:loops() do
      local lid = l:get_id();

      if (is_requested_loop [lid]) then
         local nb_paths = l:get_nb_paths();

         if (nb_paths == 1) then
            local file = io.open (string.format ("loop_%d.asm", lid), "w");
            file:write (cqa:get_assembly_code (get_loop_insns (l)));
            file:close ();
            -- print (string.format ("loop %d", lid));
            -- print (cqa:get_assembly_code (get_loop_insns (l)));
         elseif (nb_paths <= 4) then
            local path_id = 1;
            for path in l:paths() do
               local file = io.open (string.format ("loop_%d_path_%d.asm", lid, path_id), "w");
               file:write (cqa:get_assembly_code (get_path_insns (path)));
               file:close ();
               -- print (string.format ("loop %d path %d", lid, path_id));
               -- print (cqa:get_assembly_code (get_path_insns (path)));
               path_id = path_id + 1;
            end
	    l:free_paths();
	 else
	    print (string.format ("too many paths (%d) for loop %d", nb_paths, lid))
         end
      end
   end
end
