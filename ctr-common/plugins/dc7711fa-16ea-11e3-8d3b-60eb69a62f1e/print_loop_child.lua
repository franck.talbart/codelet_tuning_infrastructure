--HOW TO USE IT 
-- maqao print_loop_child.lua ./BINARY

p = project.new("a");
a = p:load(arg[2],0);

for fct in a:functions() do
    for loop in fct:loops() do
      io.write(loop:get_id()..":");
      local children = loop:get_children();
      if (children ~= nil) then
         for loop_id,_ in pairs(children) do
            io.write(loop_id..",");
         end
      end
      print();
   end
end
