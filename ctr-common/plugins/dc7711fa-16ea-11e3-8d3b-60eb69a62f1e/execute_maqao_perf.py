#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import sys, os, tempfile, subprocess, stat

#constants & global
MAQAO_OUTPUT = "maqao_output"
SCRIPT_PARAMS = "script_params"

nb_run_maqao_perf = ""
set_freq_script = ""
granularity = ""
stability_sanity_check = ""
run_parameters = ""
prefix_cmd = ""

# get parameters
param_file = open(SCRIPT_PARAMS, "r")

nb_run_maqao_perf = int(param_file.readline().rstrip('\n'))
set_freq_script = param_file.readline().rstrip('\n')
granularity = param_file.readline().rstrip('\n')
stability_sanity_check = float(param_file.readline().rstrip('\n'))
compiled_application = param_file.readline().rstrip('\n')
hierarchy_script = param_file.readline().rstrip('\n')
parent_script = param_file.readline().rstrip('\n')
bypass_kernel_detection = bool(param_file.readline().rstrip('\n'))
ug_parameter = param_file.readline().rstrip('\n')
run_parameters = param_file.readline().rstrip('\n')
prefix_cmd = param_file.readline().rstrip('\n')

param_file.close()

# check parameters
if not granularity:
    print "You must provide the granularity!"
    exit(1)
if bypass_kernel_detection:
    bypass_kernel_detection = "-bkd=on"
else:
    bypass_kernel_detection = ""
if not str(nb_run_maqao_perf):
    nb_run_maqao_perf = 10
if ug_parameter.lower() == "none":
    ug_parameter = ""
else:
    ug_parameter = "ug=" + ug_parameter
if run_parameters.lower() == "none":
    run_parameters = ""
if prefix_cmd.lower() == "none":
    prefix_cmd = ""
if not str(stability_sanity_check):
    print "You must provide the stability sanity check !"
    exit(1)

work_dir = os.getcwd()

relevance_threshold_min = (nb_run_maqao_perf * 8) / 10

print "\n* Number of maqao perf run: %s" % nb_run_maqao_perf
print "* Stability_sanity_check: %s" % stability_sanity_check
print "* Relevance_threshold_min: %s" % relevance_threshold_min

# get and untar the binary
os.chdir(os.path.join(work_dir, "APP"))
binary_name = os.listdir(".")[0]
os.system("tar xf " + binary_name)
os.system("rm " + binary_name)
binary_name = binary_name.replace(".tar.gz", "")
# cd command could be done on the prefix script, so we use the absolute path
binary_name = os.path.join(os.getcwd(), binary_name)

if compiled_application:
    # get and untar the application
    application_dir = os.path.join(work_dir, "OTHER")
    os.chdir(application_dir)
    os.system("tar xf " + compiled_application)
    os.system("rm " + compiled_application)
    os.chdir(application_dir)

tmp_tool_dir = os.listdir(os.path.join(work_dir, "TOOL"))[0]
tmp_tool_dir = os.path.join(work_dir, "TOOL", tmp_tool_dir)
maqao_bin = os.path.join(tmp_tool_dir, "maqao")

print "\n##########################################################################"
print "WARNING: Please make sure Hyperthreading and Turboboost are disabled."
print "On top of that, the frequency of the CPU should be stabilized. To do so, please use cpuspeed."
print "A script to set the frequency can be found here: '%s'." % set_freq_script
print "WARNING: The application must be compiled using the -g flag."
print "##########################################################################\n"

print "Running Maqao perf.\n"

for j in range(0, nb_run_maqao_perf):
    # process output file name
    maqao_display_outputfile = "run_" + str(j) + "_" + MAQAO_OUTPUT
    maqao_display_outputfile = os.path.join(work_dir, "RESULTS", maqao_display_outputfile)
    
    # process maqao commands
    output_dir = tempfile.mkdtemp(dir=".")
    os.rmdir(output_dir)
    maqao = ("%s %s lprof -g=%s xp=%s t=SX %s %s -- %s %s" % (prefix_cmd, maqao_bin, granularity, output_dir, bypass_kernel_detection, ug_parameter, binary_name, run_parameters)).rstrip(" ")
    maqao_display = "%s lprof xp=%s d=SLX ssv=on --output-format=csv --output-path=%s" % (maqao_bin, output_dir, maqao_display_outputfile)
    
    # process prefix script
    sys.stdout.flush()
    path_prefix = os.path.join(work_dir, "SCRIPTS")
    file_cmd_path = os.path.join(work_dir, "file_cmd.sh")
    file_cmd = open(file_cmd_path, "w")
    file_cmd.write("#!/bin/bash\n")
    prefix_file = os.path.join(path_prefix, "prefix.sh")
    if os.path.isfile(prefix_file):
        file_cmd.write("source %s\n" % prefix_file)
    file_cmd.write(maqao+("\n"))
    file_cmd.write(maqao_display+("\n"))
    file_cmd.close()
    os.chmod(file_cmd_path, stat.S_IXUSR | stat.S_IRUSR)
    
    # run maqao perf on the binary
    print "Run number %s..\n" % (j+1)
    print maqao
    print maqao_display
    # Shell must be true because we need to keep the environment variables for LoadLeveler
    (std_out, std_err) = subprocess.Popen([file_cmd_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()
    
    os.remove(file_cmd_path)
    
    print "Output of Maqao perf:\n%s\n%s\n" % (std_out, std_err)

# record the Maqao Perf version
os.system("%s --version | head -n 1 > %s" % (maqao_bin, os.path.join(work_dir, "RESULTS", "maqao_perf_version")))

# get the loop hierarchy
hierarchy_script = os.path.join(path_prefix, hierarchy_script)
os.system("%s %s %s > %s" % (maqao_bin, hierarchy_script, binary_name, os.path.join(work_dir, "RESULTS", "loop_hierarchy")))

# get the loop parent
parent_script = os.path.join(path_prefix, parent_script)
os.system("%s %s %s > %s" % (maqao_bin, parent_script, binary_name, os.path.join(work_dir, "RESULTS", "parent_hierarchy")))

print "\nEnd of Maqao execution."
