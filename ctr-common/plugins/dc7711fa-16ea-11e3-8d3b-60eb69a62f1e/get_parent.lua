local args = Utils:get_args(arg);

if (args.bin == nil) then
   print (string.format ("Usage: maqao %s [bin=]path/to/bin\n", arg[1]))
   os.exit (-1)
end

local prj = project.new ("get_parent")
local bin = prj:load (args.bin, 4)

--print ("loop ID;parent ID;src line start;src line end;src file")

for f in bin:functions() do
   for l in f:loops() do
      local lid = l:get_id();
      local src_line_start, src_line_end = l:get_src_line();
      local first_insn = l:get_first_entry():get_first_insn();
      local src_file = first_insn:get_src_file_path();

      local p = l:get_parent();
      if (p ~= nil) then
         print (string.format ("%d;%d;%d;%d;%s", lid, p:get_id(), src_line_start, src_line_end, src_file))
      else
         print (string.format ("%d;NA;%d;%d;%s", lid, src_line_start, src_line_end, src_file));
      end
   end
end
