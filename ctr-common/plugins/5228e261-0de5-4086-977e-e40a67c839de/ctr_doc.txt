== Description ==

This plugin allows you to run a daemon and get its status and output in an entry.

== Usage examples ==

<pre>
ftalbart@brahms:~/cti$ cti daemon init --auto_run=false
Please enter the command to run in the daemon: 
/bin/bash/
/home/users/ftalbart/toto.sh

DATA_CTI_UID=878b3713-0446-4efa-9fe8-1e982f67ca85

ftalbart@brahms:~/cti$ cti daemon run 878b3713-0446-4efa-9fe8-1e982f67ca85
</pre>

To kill a daemon:
<pre>
ftalbart@brahms:~/cti$ cti daemon kill 878b3713-0446-4efa-9fe8-1e982f67ca85
</pre>