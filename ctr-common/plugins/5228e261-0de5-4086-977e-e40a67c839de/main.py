#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import entry, description, plugin, cti_json, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, time, subprocess, os, signal

import daemon

class DaemonPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        self.work_params = description.description_write(self.command, self.work_params)
        auto_run = self.work_params[self.command].params["auto_run"][cti.META_ATTRIBUTE_VALUE]
        format_o = self.work_params[self.command].params["format"][cti.META_ATTRIBUTE_VALUE]
        self.output_params[self.command].params["status"][cti.META_ATTRIBUTE_VALUE] = "Ready"
        (data_entry, output) = self.default_init_command(params, no_output = True)
        res = plugin.plugin_auto_run(self, data_entry, format_o, auto_run, output)
        return res
#---------------------------------------------------------------------------
    
    @hapi_command("run")
    def run_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
    
        # Parameters
        daemon_uid = params["entry"]
        format = params["format"]
       
        # Get data from the daemon entry
        (_, out) = entry.load_data(daemon_uid)
        
        cmd = out["init"].params["command"][cti.META_ATTRIBUTE_VALUE]
        f = out["init"].params["output"][cti.META_ATTRIBUTE_VALUE]
        status = out["init"].params["status"][cti.META_ATTRIBUTE_VALUE]
        
        if status == "Running":
            util.cti_plugin_print_error("The daemon is already running!")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        # Creating the files directory
        path = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, daemon_uid)
        if path:
            path_dest = os.path.join(path, 
                                     cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR))
            dest = os.path.join(path_dest, f)
            if (not os.path.exists(path_dest)):
                 os.makedirs(path_dest, mode=0777)
        else:
            util.cti_plugin_print_error("Entry not found.")

            return cti.CTI_PLUGIN_ERROR_IO

        entry.update_entry_parameter(
                                        daemon_uid,
                                        {
                                            "status": { "value": "Running"}
                                        }
                                    )

        if format == "json":
            print cti_json.cti_json_encode({'daemon_uid':daemon_uid})
        elif format == "txt":
            print "The daemon is running! "
            print "DATA_CTI_UID=" + str(daemon_uid)
        else:
            util.cti_plugin_print_error("Unknown format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        sys.stdout.flush()
        
        return_d = daemon.create_daemon()
        
        # Running the daemon
        output_file = open(dest,'a')
        t0 = time.time()
        sys.stdout.flush()
        process = subprocess.Popen(cmd,
                                   stdout=output_file,
                                   stderr=subprocess.STDOUT)
        
        entry.update_entry_parameter(
                                        daemon_uid, 
                                        {
                                         "pid": {"value": str(process.pid)}
                                        }
                                    )
        
        date_start = str(time.strftime('%Y/%m/%d %H:%M:%S',time.localtime()))
        update_ctr_info(daemon_uid, cti.DATA_INFO_DATE_TIME_START, date_start)

        process.communicate()
        return_val = process.poll()
        
        entry.update_entry_parameter(daemon_uid, {
                                                 "execution_time": { "value": str(time.time() - t0)},
                                                 "return": {"value": str(return_val)},
                                                 "status": {"value": "Finished"}
                                                }
                                    )
        
        output_file.write('[%s] Process is complete!' % time.strftime('%Y/%m/%d %H:%M:%S',time.localtime()))
        output_file.close()
        return return_d
#---------------------------------------------------------------------------
    
    @hapi_command("kill")
    def kill_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        daemon = params["entry"]
        format_o = params["format"]
        
        (_, out) = entry.load_data(daemon)

        pid = out["init"].params["pid"][cti.META_ATTRIBUTE_VALUE]
        status = out["init"].params["status"][cti.META_ATTRIBUTE_VALUE]
        
        if status == "Running" and pid != "":
            pid = int(pid)
            os.kill(pid, signal.SIGKILL)
            
            entry.update_entry_parameter(daemon, {
                                         "execution_time": { "value": ""},
                                         "return": {"value": ""},
                                         "status": {"value": "Killed"}
                                        }
                            )
            
            if format_o == "txt":
                print "The daemon has been killed!"
            elif format_o == "json":
                print cti_json.cti_json_encode({'was_running':True, 'daemon_uid':daemon})
            else:
                util.cti_plugin_print_error("Unknown format: " + format_o)
                return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        
        else:
            if format_o == "txt":
                print "The daemon is not running! "
            elif format_o =="json":
                print cti_json.cti_json_encode({'was_running':False})
            else:
                util.cti_plugin_print_error("Unknown format: " + format_o)
                return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        return 0
#---------------------------------------------------------------------------

def update_ctr_info(uid, attribute_name, value):
    data_info = ctr.ctr_plugin_info_file_load_by_uid(uid)
    ctr.ctr_plugin_info_put_value(data_info, attribute_name, value)
    ctr_info_name = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, uid), 
                                         cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME))
    ctr.ctr_plugin_info_record_in_file(data_info, ctr_info_name)

    entry.update_entry_parameter(uid, {attribute_name: { "value": value}})
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = DaemonPlugin()
    exit(p.main(sys.argv))
