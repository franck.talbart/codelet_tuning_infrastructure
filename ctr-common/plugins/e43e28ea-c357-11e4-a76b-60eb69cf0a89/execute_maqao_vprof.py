#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Romain Anty

import sys, os, tarfile, stat, subprocess

#Constants
SCRIPT_PARAMS   = "script_params"
WORK_DIR        = os.getcwd()

#-------------------------------------------------------------------------------

def untar(file_path):
    """ Extracts a tar.gz archive.

    Args:
        file_path: path of the archive
    
    Returns:
        Nothing
    """
    
    if file_path.endswith("tar.gz"):
        print("Extracting {0}.".format(file_path))
        tar = tarfile.open(file_path)
        tar.extractall()
        tar.close()
    else:
        print("{0} is not a tar.gz file. Extraction aborted.".format(file_path))

#-------------------------------------------------------------------------------

#Retrieving parameters from the parameters file.
param_file = open(SCRIPT_PARAMS, "r")

nb_run_maqao_vprof      = int(param_file.readline().rstrip('\n'))
project_name                = param_file.readline().rstrip('\n')
subsets_count           = int(param_file.readline().rstrip('\n'))

#Retrieving loops subsets.
subsets = []
for i in range (0, subsets_count):
    loop_ids = param_file.readline().rstrip('\n').strip('"')
    subsets.append(loop_ids)

instruments                 = param_file.readline().rstrip('\n')
run_cmd                     = param_file.readline().rstrip('\n')
safe_mode                   = param_file.readline().rstrip('\n')
compiled_application_path   = param_file.readline().rstrip('\n')

print("\nNumber of MAQAO-VPROF runs: {0}\n".format(nb_run_maqao_vprof))

if subsets_count > 1:
    print("{0} loops subsets.\n".format(subsets_count))

#Getting and extracting the binary to analyze.
os.chdir(os.path.join(WORK_DIR, "APP"))
binary_name = os.listdir(".")[0]

untar(os.path.join(WORK_DIR, "APP", binary_name))
binary_name = binary_name.replace(".tar.gz", "")

#Getting the MAQAO binary.
maqao_bin = os.path.join(WORK_DIR, "TOOL", "maqao", "maqao")

#Getting and extracting the application environment, if any.
if compiled_application_path:
    untar(os.path.join(WORK_DIR, "OTHER", compiled_application_path))

print("\nRunning MAQAO-VPROF.\n")

for i in range (1, nb_run_maqao_vprof + 1):
    #Generating MAQAO-VPROF command arguments common to all loops subsets.
    project_arg = "--project=" + project_name
    instruments_arg = ""
    if instruments:
        instruments_arg = "--instruments=" + instruments
    run_cmd_arg = ""
    if run_cmd:
        run_cmd_arg = "--run-cmd=" + run_cmd

    for j in range (1, subsets_count + 1):
        #Generating output file name.
        maqao_output_file = "result_vprof_{0}_subset_{1}.csv".format(i, j)
        maqao_output_file = os.path.join(WORK_DIR, "RESULTS", maqao_output_file)
        output_path_arg = "--output-path=" + maqao_output_file

        maqao_vprof_cmd = "{0} vprof {1} {2} --loop-id={3} {4} {5} --output-format=csv {6}\n".format(
            maqao_bin, os.path.join(WORK_DIR, "APP", binary_name), project_arg, subsets[j-1], instruments_arg, run_cmd_arg, output_path_arg)

        #Generating the prefix script.
        prefix_script_path = os.path.join(WORK_DIR, "SCRIPTS")
        file_cmd_path = os.path.join(WORK_DIR, "file_cmd.sh")
        
        file_cmd = open(file_cmd_path, "w")
        file_cmd.write("#!/bin/bash\n")
        prefix_file = os.path.join(prefix_script_path, "prefix.sh")

        if os.path.isfile(prefix_file):
            file_cmd.write("source {0}\n".format(prefix_file))

        file_cmd.write(maqao_vprof_cmd + '\n')
        file_cmd.close()
        os.chmod(file_cmd_path, stat.S_IXUSR | stat.S_IRUSR)

        #Running MAQAO-VPROF on the binary.
        if subsets_count > 1:
            print("Run number {0}, subset number {1}...\n".format(i, j))
        else:
            print("Run number {0}...\n".format(i))
        
        print(maqao_vprof_cmd)
        sys.stdout.flush()

        #Adding the libvprof.so library needed by the executable.
        env = os.environ.copy()
        env["LD_LIBRARY_PATH"] = os.path.join(WORK_DIR, "OTHER")
        (std_out, std_err) = subprocess.Popen([file_cmd_path], env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        
        os.remove(file_cmd_path)

        print("MAQAO-VPROF output:\n{0}\n{1}\n".format(std_out, std_err))

print("End of MAQAO-VPROF execution.")
