#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Romain Anty

import cti
import ctr

from cti_hapi import plugin, util, submitter, entry, alias
from cti_hapi.main import HapiPlugin, hapi_command
from maqao_vprof_util import get_depth

import sys, os, json, tempfile, glob, datetime, shutil

from import_maqao_vprof_results import import_results

class MaqaoVProfPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Initializes information for MAQAO-VPROF.

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
            Nothing
        """

        binary              = params["binary"]
        repository_query    = params["repository_query"]
        query               = params["query"]
        instruments         = params["instruments"]
        loop_uids           = params["loops"]
        mode                = params["mode"]
        platform            = params["platform"]
        query_uid           = params["query_uid"]
        auto_run            = params["auto_run"]

        #Checking arguments.
        if instruments:
            for instrument in instruments:
                if instrument not in ['cycles', 'iterations', 'none']:
                    util.cti_plugin_print_error("Unknown instrument {0}.\n".format(instrument))
                    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

        if mode not in ['local', 'ssh', 'slurm']:
            util.cti_plugin_print_error("Unknown mode {0}.\n".format(mode))
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

        if mode != 'local' and not platform:
            util.cti_plugin_print_error("You must provide a platform in non-local mode.\n")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

        #Retrieving the profiler used to profile the binary.
        (_, binary_output_data) = entry.load_data(binary)
        profiler = None
       
        if binary_output_data["init"].params["maqao_perf_entry"][cti.META_ATTRIBUTE_VALUE]:
            profiler = "maqao_perf"
        elif binary_output_data["init"].params["icc_profiler_entry"][cti.META_ATTRIBUTE_VALUE]:
            profiler = "icc_profiler"

        if not profiler:
            util.cti_plugin_print_error("The binary has not been profiled yet.")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

        #Preparing the user query.
        #Restricting the query to the loops relative to the given binary.
        #If no query was specified, we get all loops from the given binary.
        if query:
            query += " AND loop_group.{0}.binary.entry_uid:{1}".format(profiler, binary)
        else:
            query = "loop_group.{0}.binary.entry_uid:{1}".format(profiler, binary)

        json_file = \
        {
            "select":
            {
                "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                "params":
                [
                    {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_query},
                    {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                    {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : "loop"},
                    {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                    {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : ['entry_uid']},
                    {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : False},
                    {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : None}
                ]
            }
        }

        #Executing the user query and getting the corresponding loops to analyze.
        loops_query_result = ""
        try:
            loops_query_result = plugin.execute_plugin_by_file(query_uid, json_file, self.username, self.password)
        #We can not be more specific since the above method raises an "Exception" if something goes wrong.
        except Exception:
            print(sys.exc_info()[1])
            exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)

        loops = json.loads(loops_query_result)

        #List of the loop UIDs to analyze, retrieved from the result of the user query.
        query_loop_uids = []
        for loop_uid in loops["data"]:
            (_, loop_output_data) = entry.load_data(cti.CTI_UID(str(loop_uid["entry_uid"])))
            loop_language = loop_output_data["init"].params["language"][cti.META_ATTRIBUTE_VALUE]

            #Eliminating special loops inserted by the compiler which can not be profiled by MAQAO-VPROF.
            if loop_language:
                query_loop_uids.append(str(loop_uid["entry_uid"]))

        #Aborting the experience if no loops have been found.
        if not query_loop_uids and not loop_uids:
            util.cti_plugin_print_error("No loops to analyze. Please verify your query and make sure your binary contains at least one loop.\n")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

        #Concatenating the query results to the loops list.
        #Converting each result into a loop UID.
        array_uid = map(cti.CTI_UID, query_loop_uids)
        loop_uids += array_uid
        self.output_params["init"].params["loops"][cti.META_ATTRIBUTE_VALUE] = loop_uids

        #Creating entry alias.
        binary_alias = alias.get_data_alias(binary)
        if not binary_alias:
            binary_alias = ""

        now = datetime.datetime.now()
        date = now.strftime("%Y_%m_%d_%H:%M:%S:%f")
        entry_alias = "MAQAO_VPROF_{0}_{1}".format(binary_alias, date)

        #Calling default constructor.
        entry_data = self.default_init_command(params, alias_e=entry_alias)

        #Run plugin if auto_run is True.
        plugin.plugin_auto_run(self, entry_data, None, auto_run)

    #---------------------------------------------------------------------------

    @hapi_command("run")
    def run(self, params):
        """ Runs MAQAO VProf on a set of loops to fill their "cycles" and "invocations" fields.

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """

        #Constants
        WORK_DIR = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)
        COMMON_TOOLS_DIR = cti.cti_plugin_config_get_value(cti.COMMON_TOOLS_DIR)

        #Checking MAQAO installation.
        maqao_dir = os.path.join(cti.cti_plugin_config_get_value(cti.THIRD_PARTY_DIR), "maqao")
        if not os.path.isdir(maqao_dir):
            util.cti_plugin_print_error("MAQAO is not installed\nYou can find the tool here: http://www.maqao.org/\n")
            util.cti_plugin_print_error("Install the tool in the third-party directory (third-party/maqao/).\n")
            exit(cti.CTI_PLUGIN_ERROR_TOOL_NOT_FOUND)

        #Retrieving argument values.
        maqao_vprof_uid = params["entry"]
        (_, output_params) = entry.load_data(maqao_vprof_uid)

        nb_run                  = params["nb_run"]
        prefix                  = params["prefix"]
        
        binary                  = output_params["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        project_name            = output_params["init"].params["project_name"][cti.META_ATTRIBUTE_VALUE]
        instruments             = output_params["init"].params["instruments"][cti.META_ATTRIBUTE_VALUE]
        run_cmd                 = output_params["init"].params["run_cmd"][cti.META_ATTRIBUTE_VALUE]
        loop_uids               = output_params["init"].params["loops"][cti.META_ATTRIBUTE_VALUE]
        mode                    = output_params["init"].params["mode"][cti.META_ATTRIBUTE_VALUE]
        partition               = output_params["init"].params["partition"][cti.META_ATTRIBUTE_VALUE]
        platform                = output_params["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
        prefix_execute_script   = output_params["init"].params["prefix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        submitter_cmd           = output_params["init"].params["submitter_cmd"][cti.META_ATTRIBUTE_VALUE]
        submitter_opt           = output_params["init"].params["submitter_opt"][cti.META_ATTRIBUTE_VALUE]
        submitter_user          = output_params["init"].params["submitter_user"][cti.META_ATTRIBUTE_VALUE]

        #Since VPROF is not able to instrument two parented loops at the same time, we check if some of these loops are parented.
        #If so, we have to split this loops list into several subsets of non-parented loops and to run VPROF on these subsets.

        #These loops can be represented as a tree where each loop is a node.
        #Each loop have zero or one direct parent and zero or several children.

        #For example:
        #  3
        # /
        #1   5
        # \ /
        #  2
        #   \
        #    4
        #
        #  8
        # /
        #6
        # \
        #  7
        #
        #9

        #Suppose we want to instrment the loops 1, 2, 5, 7, 8 and 9.
        #The minimum number of VPROF calls to achieve this without conflict is equal to the depth of the innermost loop of this list. Here 5.
        #Loop 5 is three levels deep.
        #Then, we divide the original list into three subsets. We put in each subset loops of the same depth.
        
        #Here, the subsets are:
        #deep level 0: 1, 9
        #deep level 1: 2, 7, 8
        #deep level 2: 5

        #Thus, the execute script will call VPROF three times, on each of these subsets and without any conflict.

        #A dictionary where a deep level is linked to the corresponding loops list.
        subsets = {}

        #A dictionary used by the get_depth method (see its docstring for more details).
        visited_nodes = {}

        for loop_uid in loop_uids:
            (_, loop_output_data) = entry.load_data(loop_uid)
            
            loop_id = loop_output_data["init"].params["loop_id"][cti.META_ATTRIBUTE_VALUE]
            depth = get_depth(loop_uid, visited_nodes)
            
            if depth in subsets:
                subsets[depth].append(loop_id)
            else:
                subsets[depth] = [loop_id]

        subsets_count = len(subsets)

        if subsets_count > 1:
            print("Parented loops detected. MAQAO-VPROF will be run safely on independent loops subsets.\n")

        #Gathering information for the submitter call (allows to execute the plugin on a distant machine).

        #Getting the binary path from the binary entry.
        (_, output_binary) = entry.load_data(binary)
        binary_file = output_binary["init"].params["binary_file"][cti.META_ATTRIBUTE_VALUE]
        binary_path = os.path.join(
            ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, binary),
            cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR),
            binary_file)

        #Getting the compiled application path from the binary entry.
        compile_uid = output_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]
        compiled_application_path = None

        if compile_uid:
            (_, output_compile) = entry.load_data(compile_uid)
            compiled_application = output_compile["init"].params["compiled_application"][cti.META_ATTRIBUTE_VALUE]

            if compiled_application:
                compiled_application_path = os.path.join(
                    ctr.ctr_plugin_get_path_by_uid(ctr.CTR_ENTRY_DATA, compile_uid),
                    cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR),
                    compiled_application)

        #Creating arguments for the submitter call.

        #Options dictionary used by the submitter.
        dict_opt = {}
        dict_opt["APP"] = [binary_path]
        dict_opt["TOOL"] = [maqao_dir]
        if prefix:
            prefix_script_path = os.path.join(tempfile.mkdtemp(), "prefix.sh")
            shutil.copy(prefix, prefix_script_path)
            dict_opt["SCRIPTS"] = [prefix_script_path]
        dict_opt["OTHER"] = [os.path.join(COMMON_TOOLS_DIR, "libvprof.so")]
        if compiled_application_path:
            dict_opt["OTHER"].append(compiled_application_path)
        dict_opt["platform"] = platform
        dict_opt["entry"] = maqao_vprof_uid
        dict_opt["plugin_execute_script"] = os.path.join(self.plugin_directory, "execute_maqao_vprof.py")
        dict_opt["partition"] = partition
        dict_opt["submitter_cmd"] = submitter_cmd
        dict_opt["submitter_opt"] = submitter_opt
        dict_opt["submitter_user"] = submitter_user
        dict_opt["prefix_execute_script"] = prefix_execute_script
        dict_opt["username"] = self.username

        #Creating the script parameters file used by the submitter.
        dict_opt["script_params"] = tempfile.mkstemp(dir=WORK_DIR)[1]
        
        script_params_file = open(dict_opt["script_params"], "w")
        script_params_file.write(str(nb_run) + '\n')
        script_params_file.write(project_name + '\n')
        
        #Writing the number of subsets and all the loops subsets to analyze.
        script_params_file.write(str(subsets_count) + '\n')
        for depth in subsets:
            str_loop_ids = ",".join(str(loop_id) for loop_id in subsets[depth])
            script_params_file.write(str_loop_ids + '\n')
        
        if not instruments:
            instruments = ""
        script_params_file.write(instruments + '\n')
        if not run_cmd:
            run_cmd = ""
        script_params_file.write(run_cmd + '\n')
        if not compiled_application_path:
            compiled_application_path = ""
        script_params_file.write(compiled_application_path + '\n')
        script_params_file.close()

        #Executing plugin via the submitter.
        print("Calling MAQAO-VPROF. This may take a while depending on the experiment and the number of runs.\n")
        (job_dir, daemon_log_file_path) = submitter.submitter(dict_opt, self.plugin_uid, mode)

        #Collecting execution file.
        execution_log_file_path = None
        for f in glob.glob("*.out"):
            if f != daemon_log_file_path:
                execution_log_file_path = os.path.abspath(f)

        #Enriching the options dictionary with the needed arguments for the import_results script.
        dict_opt["result_dir"] = os.path.join(WORK_DIR, job_dir + "_local", job_dir, "RESULTS")
        dict_opt["loop_uids"] = loop_uids
        dict_opt["nb_run"] = nb_run
        dict_opt["execution_log"] = execution_log_file_path
        dict_opt["daemon_log"] = daemon_log_file_path

        #The resulting message to be written in the daemon log file.
        daemon_message = ""

        #Current date to be written in the log file.
        date = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_%f")

        daemon_log_file = open(daemon_log_file_path, "a")

        #If the RESULTS directory is empty, the experience has failed.
        if not os.listdir("RESULTS"):
            execution_log_file = open(execution_log_file_path, "r")
            execution_log_message = execution_log_file.readlines()
            execution_log_file.close()
            execution_log_message = "".join(execution_log_message)
            
            error_message = "[{0}] No csv files generated.\ntThe error message is:\n\
                            ==============================\n{1}\n==============================\n\
                            Temporary files are located in the directory {2}".format(date, execution_log_message, os.getcwd())
            util.cti_plugin_print_error(error_message)
            daemon_message += error_message
            daemon_log_file.write(daemon_message)
            daemon_log_file.close()
        else:
            import_message = "[{0}] Importing results into CTI...".format(date)
            print(import_message)
            daemon_message += import_message
            daemon_log_file.write(daemon_message)
            daemon_log_file.close()

            import_results(dict_opt)

            #Cleaning temporary files.
            shutil.rmtree(os.path.join(WORK_DIR, job_dir + "_local"))


#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = MaqaoVProfPlugin()
    exit(p.main(sys.argv))
