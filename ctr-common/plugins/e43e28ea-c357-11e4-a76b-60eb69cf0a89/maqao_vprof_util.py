#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Romain Anty

import cti

from cti_hapi import entry

import sys, tarfile

def get_depth(loop_uid, visited_nodes):
    """ Computes the depth of the provided loop.

    Args:
        loop_uid: UID of the loop to analyze
        visited_nodes: a dictionnary referencing already visited nodes and their depth to speed up the algorithm

    Returns:
        The loop deep level.
    """
    
    deep_level = -1
    (_, loop_output_data) = entry.load_data(loop_uid)
    loop_id = loop_output_data["init"].params["loop_id"][cti.META_ATTRIBUTE_VALUE]
    loop_type = loop_output_data["init"].params["loop_type"][cti.META_ATTRIBUTE_VALUE]

    if loop_id in visited_nodes:
        deep_level = visited_nodes[loop_id]

    elif (loop_type == "outermost") or (loop_type == "single"):
        deep_level = 0
        #Adding the visited node to the dictionary.
        visited_nodes[loop_id] = deep_level

    else:
        loop_parent = loop_output_data["init"].params["loop_parents"][cti.META_ATTRIBUTE_VALUE][0]
        deep_level = get_depth(loop_parent, visited_nodes) + 1
        visited_nodes[loop_id] = deep_level

    return deep_level
