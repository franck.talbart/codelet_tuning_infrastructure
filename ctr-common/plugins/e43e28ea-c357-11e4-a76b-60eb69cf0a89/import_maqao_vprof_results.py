#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Romain Anty

import cti
import ctr

from cti_hapi import entry, util

import os, glob, csv, numpy

def import_results(dict_opt):
    """ Reads the csv files generated by MAQAO-VPROF and fills the invocations and cycles fields of the given loop entries.

    Args:
        dict_opt: a dictionary containing all needed arguments to run the MAQAO-VPROF binary

    Returns:
        Nothing
    """

    #Constants
    RESULTS_DIR = dict_opt["result_dir"]
    CSV_LOOP_ID = "loop_id"
    CSV_INVOCATIONS = "instance_count"
    CSV_CYCLES = "cycle_mean"

    #The results directory does not necessary exists, as the experiment may have failed.
    if RESULTS_DIR:
        #Dictionary storing the results of the csv files, for each loop and formatted as follows:
        #{
        #   loop_uid_1 : {"invocations" : [<invocations_list>], "cycles" : [<cycles_list>]}
        #   .
        #   .
        #   .
        #   loop_uid_n : {"invocations" : [<invocations_list>], "cycles" : [<cycles_list>]}
        #}
        #Where <invocations_list> and <cycles_list> correspond respectively to the list of invocations
        #and the list of cycles of the loop, read in each csv result file.
        loops_results = {}

        #List of csv results files to put in the "csv_files" field of the maqao_vprof entry.
        csv_files_to_import = []

        #Processing all csv files produced by MAQAO-VPROF.
        results_files_names = glob.glob(os.path.join(RESULTS_DIR, "*.csv"))
        for result_file_name in results_files_names:
            result_file_path = os.path.join(RESULTS_DIR, result_file_name)
            result_file = open(result_file_path, "r")

            result_file_content = csv.DictReader(result_file)
            
            for row in result_file_content:
                loop_id = row[CSV_LOOP_ID]
                loop_invocations = row[CSV_INVOCATIONS]
                loop_cycles = row[CSV_CYCLES]

                #If the key does not exist in the results dictionary yet.
                if loop_id not in loops_results:
                    loops_results[loop_id] = {"invocations" : [int(loop_invocations)], "cycles" : [float(loop_cycles)]}
                else:
                    loops_results[loop_id]["invocations"].append(int(loop_invocations))
                    loops_results[loop_id]["cycles"].append(float(loop_cycles))

            #Importing the csv result file into the maqao_vprof entry.
            try:
                imported_result_file = entry.put_file_in_entry(dict_opt["entry"], result_file_path, False)
                csv_files_to_import.append(imported_result_file)
            except IOError:
                util.cti_plugin_print_error("No such file {0}\n".format(result_file_path))

        #Getting the median number of invocations and cycles for each loop and put them in the loop entry.
        for loop_uid in dict_opt["loop_uids"]:
            (_, loop_output_data) = entry.load_data(cti.CTI_UID(str(loop_uid)))
            loop_id = loop_output_data["init"].params["loop_id"][cti.META_ATTRIBUTE_VALUE]

            if loop_id in loops_results:
                loop_median_invocations = numpy.median(loops_results[loop_id]["invocations"])
                loop_median_cycles      = numpy.median(loops_results[loop_id]["cycles"])

                entry.update_entry_parameter(
                    cti.CTI_UID(str(loop_uid)),
                    {
                        "invocations" : {"value" : loop_median_invocations},
                        "cycles" : {"value": loop_median_cycles}
                    })
            else:
                util.cti_plugin_print_warning("No data found for loop {0}. It has probably not been profiled correctly by MAQAO-Perf.".format(loop_id))

        #List of csv results files to put in the "csv_files" field of the maqao_vprof entry.
        log_files_to_import = []

        #Importing log files into the maqao_vprof entry.
        try:
            imported_execution_log_file = entry.put_file_in_entry(dict_opt["entry"], dict_opt["execution_log"], False)
            log_files_to_import.append(imported_execution_log_file)
        except IOError:
            util.cti_plugin_print_error("No such file {0}\n".format(dict_opt["execution_log"]))
        try:
            imported_daemon_log_file = entry.put_file_in_entry(dict_opt["entry"], dict_opt["daemon_log"], False)
            log_files_to_import.append(imported_daemon_log_file)
        except IOError:
            util.cti_plugin_print_error("No such file {0}\n".format(dict_opt["daemon_log"]))
            
        #Updating the maqao_vprof entry.
        entry.update_entry_parameter(
            dict_opt["entry"],
            {
                "nb_run"        : {"value" : dict_opt["nb_run"]},
                "logs"          : {"value" : log_files_to_import, "append" : True},
                "csv_output"    : {"value" : csv_files_to_import}
            })

        print("Finished importation.")
