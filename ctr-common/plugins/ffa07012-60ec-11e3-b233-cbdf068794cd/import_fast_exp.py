#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit
import cti, ctr

from cti_hapi import entry, util_uid, plugin, alias, util

import sys, os, numpy, datetime, csv

#-------------------------------------------------------------------------------
def import_results(dict_opt):
    message = ""
    message_daemon = ""
    work_dir = os.getcwd()
    log_to_import = []

    if dict_opt["results_dir"]:
        print "results dir is: " + dict_opt["results_dir"] 
        # repository type
        repository = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, util_uid.CTI_UID(dict_opt["entry"]))
        # if the repository is local, we need its UID
        if repository == cti.CTR_REP_LOCAL:
            repository_path = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(util_uid.CTI_UID(dict_opt["entry"]), cti.CTR_DATA_DIR)
            repository = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(repository_path))
    
        #Preparing results data
        print "Preparing results..."
        results = {'binary_cycles_table':[],'binary_cycles_median':None}
        for loop_id in dict_opt["loop_id_list"]:
            loop_id = int(loop_id)
            results[loop_id] = \
            {
                'invocations_table': [],
                'invocations_median': None,
                'cycles_table': [],
                'cycles_median': None
            }
            for hw_c in dict_opt["counter_list"]:
                results[loop_id]['{0}_table'.format(hw_c)] = []
                results[loop_id]['{0}_median'.format(hw_c)] = None
    
        #Extracting loop cycles and instances
        current_filename = os.path.join(dict_opt["results_dir"], 'result_vprof.csv')
        if os.path.isfile(current_filename):
            #Preparing log importation
            try:
                maqao_output_f = entry.put_file_in_entry(
                                    dict_opt["entry"], 
                                    current_filename, 
                                    False
                                 )
                log_to_import.append(maqao_output_f)
            except IOError:
                message = "CTI error: No such file: '" + current_filename + "' \n"
                sys.stderr.write(message)
                message_daemon += message
            sys.stdout.flush() 
            
            #Extracting Data
            current_file = open(current_filename, 'r')
            current_open = csv.DictReader(current_file, delimiter=",")
            
            for row in current_open:
                results[int(row['loop_id'])]['cycles_table'].append(float(row['cycle_mean']))
                results[int(row['loop_id'])]['invocations_table'].append(float(row['instance_count']))
            
            current_file.close()
            
        else:
            message = "Warning: No Vprof result file found! \n"
            sys.stderr.write(message)
            message_daemon += message

        #Extracting hardware counters
        print "Extracting hardware counter (this can take a while depending on the amount of data gathered)..."
        for hw_counter in dict_opt["counter_list"]:
            print "\t"+str(hw_counter)
            sys.stdout.flush()
            current_filename = os.path.join(dict_opt["results_dir"], 'test_counting_{0}_1.result'.format(hw_counter))
                
            if os.path.isfile(current_filename):
                #Preparing log importation
                try:
                    maqao_output_f = entry.put_file_in_entry(
                                        dict_opt["entry"], 
                                        current_filename, 
                                        False
                                     )
                    log_to_import.append(maqao_output_f)
                except IOError:
                    message = "CTI error: No such file: '" + current_filename + "' \n"
                    sys.stderr.write(message)
                    message_daemon += message
                    
                #Extracting Data
                source = open(current_filename, "r")
                lines = source.readlines()
                for l in lines:
                    elem = dict([e.split(" = ") for e in l.split(", ")])
                    if elem.has_key("value"):
                        results[int(elem["loop"])]['{0}_table'.format(hw_counter)].append(int(elem["value"].strip()))

                source.close()
        
        print "Extracting binary cycles"
        current_filename = os.path.join(dict_opt["results_dir"], 'result_APP_CYCLES.txt')
        if os.path.isfile(current_filename):
            #Preparing log importation
            try:
                maqao_output_f = entry.put_file_in_entry(
                                    dict_opt["entry"], 
                                    current_filename, 
                                    False
                                 )
                log_to_import.append(maqao_output_f)
            except IOError:
                message = "CTI error: No such file: '" + current_filename + "' \n"
                sys.stderr.write(message)
                message_daemon += message
                
            #Extracting Data
            current_file = open(current_filename, 'r')
            results['binary_cycles_table'].append(float(current_file.readline().rstrip('\n')))
            current_file.close()
        else:
            message = "Warning: No binary_cycles result file found! \n"
            sys.stderr.write(message)
            message_daemon += message
        #Processing extracted data
        print "Computing median" 
        curr_result = numpy.median(results['binary_cycles_table'])
        if not numpy.isnan(curr_result):
            results['binary_cycles_median'] = numpy.median(curr_result)

        for loop_id in dict_opt["loop_id_list"]:
            loop_id = int(loop_id)
            curr_result = numpy.median(results[loop_id]['invocations_table'])
            if not numpy.isnan(curr_result):
                results[loop_id]['invocations_median'] = numpy.median(curr_result)
 
            curr_result = numpy.median(results[loop_id]['cycles_table'])
            if not numpy.isnan(curr_result):
                results[loop_id]['cycles_median'] = numpy.median(curr_result)
            for hw_c in dict_opt["counter_list"]:
               curr_result = 0
               if len(results[loop_id]['{0}_table'.format(hw_c)]) > 0:
                    curr_result = numpy.median(results[loop_id]['{0}_table'.format(hw_c)])
               if not numpy.isnan(curr_result):
                   results[loop_id]['{0}_median'.format(hw_c)] = numpy.median(curr_result)
    
        # creation of a lib_counting_group
        print "Generating lib_counting_group."
        json_input = \
        {
            "init":
            {
                "attributes" : {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
                "params": 
                [
                    {cti.META_ATTRIBUTE_NAME : "lib_counting", cti.META_ATTRIBUTE_VALUE : dict_opt["entry"]},
                    {cti.META_ATTRIBUTE_NAME : "binary_cycles", cti.META_ATTRIBUTE_VALUE : results['binary_cycles_median']},
                    {cti.META_ATTRIBUTE_NAME : "lib_counting_results", cti.META_ATTRIBUTE_VALUE : []}
                ]
            }
        }
    
        output = ""
        try:
            output = plugin.execute_plugin_by_file(dict_opt["lib_counting_group_plugin_uid"], json_input, dict_opt["username"], dict_opt["password"])
        except :
            print json_input
            print sys.exc_info()[1]
            exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
    
        counting_group_uid = plugin.get_output_data_uid(output)
    
        #Alias for lib_counting_group
        (_, output_counting) = entry.load_data(dict_opt["entry"])
        binary_uid = output_counting['init'].params['binary'][cti.META_ATTRIBUTE_VALUE]
        bin_name = alias.get_data_alias(binary_uid)
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        alias_counting_group = "lib_counting_group_{0}".format(date)
    
        if bin_name:
            alias_counting_group = "{0}_lib_counting_group_{1}".format(bin_name, date)
         
        if alias.set_data_alias(counting_group_uid, alias_counting_group) == 0:
            sys.stderr.write("Cannot set the alias %s (already used?)\n" % (alias_counting_group))
    
        #Getting the list of hardware counters to fill
        (_, counting_results_output) = entry.load_defaults(dict_opt["lib_counting_results_plugin_uid"])
        hardware_counter_entry_list = counting_results_output["init"].params['counters'][cti.META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS]
    
        #Getting the loop uids
        (_, output_counting) = entry.load_data(dict_opt["entry"])
        loop_uids = output_counting['init'].params['loops'][cti.META_ATTRIBUTE_VALUE]
    
        #Generating lib_counting_results entries for each loop
        print "Generating lib_counting_results."
        total_loops = len(loop_uids)
        curr_loop = 1
        for loop_uid in dict_opt["loop_uid_list"]:
            util.rewrite_output_line("\tProcessing result {0: >5}/{1}.".format(curr_loop, total_loops))
            curr_loop += 1
            (_, output_loop) = entry.load_data(loop_uid)
            current_id = str(output_loop['init'].params['loop_id'][cti.META_ATTRIBUTE_VALUE])
            
            matrix_contents = {}
            for hw_counter in hardware_counter_entry_list:
                counter = hw_counter.replace(' ', '_')
                if counter.startswith(dict_opt["uarch"]):
                    counter = counter[len(dict_opt["uarch"]+"_"):]
                current_index = '{0}_median'.format(counter)
                if current_index in results[int(current_id)]:
                    matrix_contents[hw_counter.replace(' ', '_').replace(':','_')] = [results[int(current_id)][current_index]]
                else:
                    matrix_contents[hw_counter.replace(' ', '_').replace(':','_')] = [None]
        
            #Create lib_counting_results entries
            json_input = \
            {
                "init":
                {
                   "attributes" : {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "lib_counting_group", cti.META_ATTRIBUTE_VALUE : str(counting_group_uid)},
                        {cti.META_ATTRIBUTE_NAME : "loop", cti.META_ATTRIBUTE_VALUE : str(loop_uid)},
                        {cti.META_ATTRIBUTE_NAME : "invocations", cti.META_ATTRIBUTE_VALUE : results[int(current_id)]['invocations_median']},
                        {cti.META_ATTRIBUTE_NAME : "cycles", cti.META_ATTRIBUTE_VALUE : results[int(current_id)]['cycles_median']},
                        {cti.META_ATTRIBUTE_NAME : "frequency", cti.META_ATTRIBUTE_VALUE : dict_opt["frequency"]},
                        {cti.META_ATTRIBUTE_NAME : "counters", cti.META_ATTRIBUTE_VALUE : matrix_contents}
                    ]
                }
            }
        
            output = ""
            try:
                output = plugin.execute_plugin_by_file(dict_opt["lib_counting_results_plugin_uid"], json_input, dict_opt["username"], dict_opt["password"])
            except :
                print sys.exc_info()[1]
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        
            counting_results_uid = plugin.get_output_data_uid(output)
        
            if counting_results_uid:
                # alias for lib_counting_results
                now = datetime.datetime.now()
                date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
                alias_counting_results = "lib_counting_results{0}_{1}".format(current_id, date)
            
                if bin_name:
                    alias_counting_results = "{0}_lib_counting_results{1}_{2}".format(bin_name, current_id, date)
            
                if alias.set_data_alias(counting_results_uid, alias_counting_results) == 0:
                    sys.stderr.write("Cannot set the alias %s (already used?)\n" % (alias_counting_results))

    os.chdir(work_dir)

    # import exectution log file into lib_counting
    execution_log_file = ""
    try:
        execution_log_file = entry.put_file_in_entry(
                                dict_opt["entry"], 
                                dict_opt["execution_log"], 
                                False
                             )
    except IOError:
        message = "CTI error: No such file: '" + dict_opt["execution_log"] + "' \n"
        sys.stderr.write(message)
        message_daemon += message

    file_daemon = open(dict_opt["daemon_log"], "a")
    file_daemon.write(message_daemon)
    file_daemon.close()

    # import daemon log file into lib_counting
    daemon_log_file = ""
    try:
        daemon_log_file = entry.put_file_in_entry(
                            dict_opt["entry"], 
                            dict_opt["daemon_log"], 
                            False
                          )
    except IOError:
        sys.stderr.write("CTI error: No such file: '" + dict_opt["daemon_log"] + "' \n")

    #Update the main entry
    entry.update_entry_parameter(dict_opt["entry"], {"csv_output": {"value": log_to_import},
                                                      "logs": {"value": [daemon_log_file, execution_log_file], "append": True}
                                                     })

    print "Finished importation."
