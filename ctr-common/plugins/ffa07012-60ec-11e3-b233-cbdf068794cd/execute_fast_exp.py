#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2013 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart

import sys, os, subprocess, stat, tarfile, csv, re


#constants
SCRIPT_PARAMS = "script_params"

#---------------------------------------------------------------------------

def run_probe(work_dir, patched_binary, run_parameters, special_permissions, ld_library_path):
    #Running the prefix script then the patched binary
    prefix_file = os.path.join(work_dir, "SCRIPTS", "prefix.sh")
    file_cmd_path = os.path.join(work_dir, "file_cmd.sh")
    file_cmd = open(file_cmd_path, "w")
    file_cmd.write("#!/bin/bash\n")

    if os.path.isfile(prefix_file):
        file_cmd.write("source %s %s\n" % (prefix_file, patched_binary))
    # sudo don't keep env so we can't use env param of subprocess.Popen
    file_cmd.write("export LD_LIBRARY_PATH=\"$LD_LIBRARY_PATH:%s\"\n" % (ld_library_path))

    file_cmd.write("%s %s\n" % (patched_binary, run_parameters))
    #file_cmd.write("chown -R cti %s\n" % (work_dir))
    file_cmd.close()
    #os.chmod(file_cmd_path, stat.S_IXUSR | stat.S_IWUSR | stat.S_IRUSR)
    
    sys.stdout.flush()
    if special_permissions:
        output,output_error = subprocess.Popen(["sudo", file_cmd_path], stdout=subprocess.PIPE).communicate()
    else:
        output,output_error = subprocess.Popen([file_cmd_path], stdout=subprocess.PIPE).communicate()
    if output:
        print output
    if output_error:
        sys.stderr.write(str(output_error)+'\n')
    sys.stdout.flush()

#---------------------------------------------------------------------------

def untar(tar_file_path):
    print "\t Untar {0}".format(tar_file_path)
    tar_file = tarfile.open(tar_file_path)
    tar_file.extractall()
    tar_file.close()

#---------------------------------------------------------------------------


# get parameters
param_file = open(SCRIPT_PARAMS, "r")

loop_ids = param_file.readline().rstrip('\n').strip('"')
hw_counters = (param_file.readline().rstrip('\n')).strip('"').split(',')
run_parameters = param_file.readline().rstrip('\n')
special_permissions = param_file.readline().rstrip('\n')
if special_permissions == "True":
    special_permissions = True
else:
    special_permissions = False

application_env = param_file.readline().rstrip('\n')
param_file.close()

work_dir = os.getcwd()
#Hawful hack, must be resolved ASAP
#os.system("chmod -R 707 %s\n" % (work_dir))
 
other_dir_absolute = os.path.join(work_dir, "OTHER")


# check parameters
if not loop_ids:
    sys.stderr.write("No loop ids provided: nothing to analyse.\n")
    exit(1)
if not hw_counters:
    sys.stderr.write("No hardware counters provided: nothing to analyse.\n")
    exit(1)


# get and untar the binary
os.chdir(os.path.join(work_dir, "APP"))
binary_name = os.listdir(".")[0]
os.chdir(other_dir_absolute)
if application_env:
    untar(os.path.join(other_dir_absolute, application_env))

untar(os.path.join(work_dir, "APP", binary_name))
binary_name = binary_name.replace(".tar.gz", "")

maqao_bin = os.path.join(work_dir, "TOOL", "maqao", "maqao")

ld_library_path = other_dir_absolute

print "\n##########################################################################"
print "WARNING: Please make sure Hyperthreading and Turboboost are disabled."
print "On top of that, the frequency of the CPU should be stabilized. To do so, please use cpuspeed."
print "A script to set the frequency can be found in the CTI common tools."
print "WARNING: The application must be compiled using the -g flag."
print "##########################################################################\n"

print "Running vprof.\n"

current_filename = os.path.abspath("result_vprof.csv")

maqao_vprof_cmd = "{0} vprof {1} --project=tmp_project lid={2} --run-cmd=\"{{MAQAO_BIN}} {3}\" of=csv op={4} --debug=1".format(maqao_bin, os.path.join(other_dir_absolute, binary_name), loop_ids, run_parameters, current_filename)
    
##Running the prefix script then the binary
prefix_file = os.path.join(work_dir, "SCRIPTS", "prefix.sh")
file_cmd_path = os.path.join(work_dir, "file_cmd.sh")
file_cmd = open(file_cmd_path, "w")
file_cmd.write("#!/bin/bash\n")
if os.path.isfile(prefix_file):
    file_cmd.write("source %s\n" % prefix_file)
file_cmd.write(maqao_vprof_cmd + ("\n"))
file_cmd.write("rm -rf tmp_project\n")
file_cmd.close()
os.chmod(file_cmd_path, stat.S_IXUSR | stat.S_IWUSR | stat.S_IRUSR)
    
sys.stdout.flush()
env=os.environ.copy()
env["LD_LIBRARY_PATH"] = ld_library_path
(output, output_error) = subprocess.Popen([file_cmd_path], env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
sys.stdout.flush()
if output:
    print output
if output_error:
    sys.stderr.write(str(output_error)+'\n')
sys.stdout.flush()

tmp_cmd = "cp {0} {1}".format(current_filename, os.path.join(work_dir, "RESULTS", os.path.basename(current_filename)))
os.system(tmp_cmd)
 
#Extracting Data
current_file = open(current_filename, 'r')
current_open = csv.DictReader(current_file, delimiter=",")

target_row = "bucket_instance_percent"
bucket_row = "bucket_instances"
loop_id_row = "loop_id"
lid = {}
for line in current_open:
    max_instance_percent = 0
    bucket = ""
    for row in line:
        if target_row in row and line[row] is not None:
            if float(line[row]) > max_instance_percent: 
                bucket = line[bucket_row + re.match(r"{0}(.+)".format(target_row), row).group(1)]
                max_instance_percent = float(line[row])
    lid[int(line[loop_id_row])] = [int(i) for i in bucket.split(";") if i ]

current_file.close()
li_decan = ""
for i in lid:
    if lid[i]:
        li_decan += "{{l={0},i=".format(i) + ",".join([str(j) for j in lid[i]]) + "}"

# run DECAN on the application for each hardware counter
for hw_counter in hw_counters:
    print "\tRunning DECAN for hardware counter {0}".format(hw_counter)
    result_name = "test_counting_{0}_1".format(hw_counter)
    sys.stdout.flush()
 
    decan_cmd = [
                    #"sudo",
                    maqao_bin,
                    "decan",
                    os.path.join(other_dir_absolute, binary_name),
                    "-p=hc",
                    "-hc=" + hw_counter,
                    "-li='" + li_decan + "'",
                    "-o=" + result_name,
                    "-xp=" + other_dir_absolute,
                    "gdb=2",
                    "--no-recovery"
                ]
    sys.stdout.flush()
    env=os.environ.copy()
    env["LD_LIBRARY_PATH"] = ld_library_path

    #os.chmod(other_dir_absolute, stat.S_IXUSR | stat.S_IWUSR | stat.S_IRUSR)
    (output, output_error) = subprocess.Popen(decan_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    sys.stdout.flush()

    sys.stdout.flush()
 
    if output:
        print output
    if output_error:
        sys.stderr.write(str(output_error)+'\n')

    print "\tRunning the binary"
    sys.stdout.flush()
    run_probe(work_dir, os.path.join(work_dir, other_dir_absolute, result_name), run_parameters, special_permissions, ld_library_path)

    #Keeping the result file
    tmp_cmd = "mv {0}_0.rslt {1}.result".format(result_name, os.path.join(work_dir, "RESULTS", result_name))
    os.system(tmp_cmd)
    
patched_binary = os.path.join(other_dir_absolute, binary_name + "_i_global_cycles")
# untar the binary
tar_binary = patched_binary + ".tar.gz"
untar(tar_binary)

#Running one last script to get the total number of cycles for the application
print "\tRunning maqao mil for application cycles"
sys.stdout.flush()
run_probe(work_dir, patched_binary, run_parameters, special_permissions, ld_library_path)
    
##Keeping the result file
result_rename = "result_APP_CYCLES.txt"
tmp_cmd = "find . -name \"result.txt\" -exec mv {} %s \\;" % (os.path.join(work_dir, "RESULTS", result_rename))
print tmp_cmd
os.system(tmp_cmd)

os.remove(patched_binary)
os.remove(tar_binary)

print "\nEnd of Maqao execution."
