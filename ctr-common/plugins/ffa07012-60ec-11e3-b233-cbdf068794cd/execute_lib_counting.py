#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2013 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import sys, os, subprocess, stat, tarfile


#constants
SCRIPT_PARAMS = "script_params"

#---------------------------------------------------------------------------

def run_probe(work_dir, patched_binary, run_parameters, special_permissions, ld_library_path):
    #Running the prefix script then the patched binary
    if run_parameters:
        run_parameters = "-- " + str(run_parameters)
    else:
        run_parameters = ""

    prefix_file = os.path.join(work_dir, "SCRIPTS", "prefix.sh")
    file_cmd_path = os.path.join(work_dir, "file_cmd.sh")
    file_cmd = open(file_cmd_path, "w")
    file_cmd.write("#!/bin/bash\n")

    if os.path.isfile(prefix_file):
        file_cmd.write("source %s %s\n" % (prefix_file, patched_binary))
    # sudo don't keep env so we can't use env param of subprocess.Popen
    file_cmd.write("export LD_LIBRARY_PATH=\"$LD_LIBRARY_PATH:%s\"\n" % (ld_library_path))

    file_cmd.write("%s %s\n" % (patched_binary, run_parameters))
    file_cmd.write("chown -R cti %s\n" % (work_dir))
    file_cmd.close()
    os.chmod(file_cmd_path, stat.S_IXUSR | stat.S_IWUSR | stat.S_IRUSR)
    
    sys.stdout.flush()
    if special_permissions:
        output,output_error = subprocess.Popen(["sudo", file_cmd_path], stdout=subprocess.PIPE).communicate()
    else:
        output,output_error = subprocess.Popen([file_cmd_path], stdout=subprocess.PIPE).communicate()
    if output:
        print output
    if output_error:
        sys.stderr.write(str(output_error)+'\n')
    sys.stdout.flush()

#---------------------------------------------------------------------------

def untar(tar_file_path):
    print "\t Untar {0}".format(tar_file_path)
    tar_file = tarfile.open(tar_file_path)
    tar_file.extractall()
    tar_file.close()

#---------------------------------------------------------------------------

nb_run_experiment = ""

# get parameters
param_file = open(SCRIPT_PARAMS, "r")

nb_run_experiment = int(param_file.readline().rstrip('\n'))
loop_ids = param_file.readline().rstrip('\n').strip('"')
hw_counters = (param_file.readline().rstrip('\n')).strip('"').split(',')
run_parameters = param_file.readline().rstrip('\n')
special_permissions = param_file.readline().rstrip('\n')
if special_permissions == "True":
    special_permissions = True
else:
    special_permissions = False

application_env = param_file.readline().rstrip('\n')
param_file.close()

work_dir = os.getcwd()
other_dir_absolute = os.path.join(work_dir, "OTHER")


# check parameters
if not loop_ids:
    sys.stderr.write("No loop ids provided: nothing to analyse.\n")
    exit(1)
if not hw_counters:
    sys.stderr.write("No hardware counters provided: nothing to analyse.\n")
    exit(1)


print "\n* Number of lib counting run: %s" % nb_run_experiment

# get and untar the binary
os.chdir(os.path.join(work_dir, "APP"))
binary_name = os.listdir(".")[0]
os.chdir(other_dir_absolute)
if application_env:
    untar(os.path.join(other_dir_absolute, application_env))

untar(os.path.join(work_dir, "APP", binary_name))
binary_name = binary_name.replace(".tar.gz", "")

maqao_bin = os.path.join(work_dir, "TOOL", "maqao", "maqao")

ld_library_path = other_dir_absolute

print "\n##########################################################################"
print "WARNING: Please make sure Hyperthreading and Turboboost are disabled."
print "On top of that, the frequency of the CPU should be stabilized. To do so, please use cpuspeed."
print "A script to set the frequency can be found in the CTI common tools."
print "WARNING: The application must be compiled using the -g flag."
print "##########################################################################\n"

print "Running lib counting.\n"

# run lib counting on the application for each hardware counter
for hw_counter in hw_counters:
    print "\tRunning maqao mil for hardware counter {0}".format(hw_counter)
    sys.stdout.flush()
    
    patched_binary = os.path.join(other_dir_absolute, binary_name + "_i_" + hw_counter)
    
    # untar the binary
    tar_binary = patched_binary + ".tar.gz"
    untar(tar_binary)
    
    for j in range(1, nb_run_experiment+1):
        print "Run number %s..\n" % (j)
        
        run_probe(work_dir, patched_binary, run_parameters, special_permissions, ld_library_path)
        
        #Keeping the result file
        result_rename = "test_counting_{0}_{1}.result".format(hw_counter, j)
        tmp_cmd = "find . -name \"test_counting.result\" -exec echo {} \\; -exec mv {} %s \\;" % (os.path.join(work_dir, "RESULTS", result_rename))
        print tmp_cmd
        os.system(tmp_cmd)
    
    # clean
    os.remove(patched_binary)
    os.remove(tar_binary)

print "\tRunning maqao vprof for loop cycles"
sys.stdout.flush()
#Get cycles with Maqao Vprof
for j in range(1, nb_run_experiment+1):
    print "Run number %s..\n" % (j)
    sys.stdout.flush()

    maqao_vprof_cmd = "{0} vprof {1} --project=tmp_project lid={2} --instrument=cycles --run-cmd=\"{{MAQAO_BIN}} {3}\" of=csv op=result_vprof_{4}.csv --debug=1".format(maqao_bin, os.path.join(other_dir_absolute, binary_name), loop_ids, run_parameters, j)
    
    ##Running the prefix script then the binary
    prefix_file = os.path.join(work_dir, "SCRIPTS", "prefix.sh")
    file_cmd_path = os.path.join(work_dir, "file_cmd.sh")
    file_cmd = open(file_cmd_path, "w")
    file_cmd.write("#!/bin/bash\n")
    if os.path.isfile(prefix_file):
        file_cmd.write("source %s\n" % prefix_file)
    file_cmd.write(maqao_vprof_cmd + ("\n"))
    file_cmd.write("rm -rf tmp_project\n")
    file_cmd.close()
    os.chmod(file_cmd_path, stat.S_IXUSR | stat.S_IWUSR | stat.S_IRUSR)
    
    print maqao_vprof_cmd
    sys.stdout.flush()
    env=os.environ.copy()
    env["LD_LIBRARY_PATH"] = ld_library_path
    (output, output_error) = subprocess.Popen([file_cmd_path], env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    sys.stdout.flush()
    if output:
        print output
    if output_error:
        sys.stderr.write(str(output_error)+'\n')
    sys.stdout.flush()
    tmp_cmd = "find . -name \"result_vprof_%i.csv\" -exec mv {} %s \\;" % (j, os.path.join(work_dir, "RESULTS"))
    print tmp_cmd
    os.system(tmp_cmd)

patched_binary = os.path.join(other_dir_absolute, binary_name + "_i_global_cycles")
# untar the binary
tar_binary = patched_binary + ".tar.gz"
untar(tar_binary)

#Running one last script to get the total number of cycles for the application
print "\tRunning maqao mil for application cycles"
sys.stdout.flush()
for j in range(1, nb_run_experiment+1):
    print "Run number %s..\n" % (j)
    run_probe(work_dir, patched_binary, run_parameters, special_permissions, ld_library_path)
    
    ##Keeping the result file
    result_rename = "result_{0}_{1}.txt".format("APP_CYCLES",j)
    tmp_cmd = "find . -name \"result.txt\" -exec mv {} %s \\;" % (os.path.join(work_dir, "RESULTS", result_rename))
    print tmp_cmd
    os.system(tmp_cmd)

os.remove(patched_binary)
os.remove(tar_binary)

print "\nEnd of Maqao execution."
