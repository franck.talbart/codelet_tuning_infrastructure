#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti
import ctr

from cti_hapi import description, entry, alias, plugin, util, submitter
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, datetime, subprocess, json, tempfile, shutil, tarfile, glob

import import_lib_counting_results, import_fast_exp

MADRAS_LOG = "madras_trace"

#---------------------------------------------------------------------------
        
def patch_binary(patch_cmd):
    """ Patch the binary using Maqao Mil
        Args:
           patch_cmd: the Maqao Mil command to patch the binary
           
        Returns:
           Nothing
    """
    sys.stdout.flush()
    print patch_cmd
    output,output_error = subprocess.Popen(patch_cmd.split(' '), stdout=subprocess.PIPE).communicate()
    if output:
        print output
    if output_error:
        sys.stderr.write(str(output_error) + '\n')
    sys.stdout.flush()

#---------------------------------------------------------------------------
        
def file_replace(input_path, tuples, output_path=None):
    if not output_path:
        output_path=input_path
            
    to_change = open(input_path, 'r')
    lines = []
    for line in to_change:
        for (expr, repl) in tuples:
            line = line.replace(expr, repl)
        lines.append(line)
    to_change.close()
            
    #Writing the result
    to_change = open(output_path, 'w')
    to_change.writelines(lines)
    to_change.close()
        
#---------------------------------------------------------------------------
 

class LibCountingPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Initialize information for lib_counting
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        self.work_params = description.description_write(self.command, self.work_params)
        auto_run = self.work_params[self.command].params["auto_run"][cti.META_ATTRIBUTE_VALUE]
        
        repository_query = params["repository_query"]
        query = params["query"]
        type_query = params["type_query"]
        query_uid = params["query_uid"]
        loop_uids = params["loops"]
        
        mode = self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE]
        platform = str(self.work_params[self.command].params["platform"][cti.META_ATTRIBUTE_VALUE])
        binary = str(self.work_params[self.command].params["binary"][cti.META_ATTRIBUTE_VALUE])
        
        # check parameters
        if mode not in ['local', 'ssh', 'slurm']:
            util.cti_plugin_print_error("Unknown mode.\n")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        if mode != "local" and not platform:
            util.cti_plugin_print_error("You must provide a platform on non-local mode.\n")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        
        if not binary:
            util.cti_plugin_print_error("The binary entry parameter is mandatory.\n")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if not query:
            query = "loop_group.maqao_perf.binary.entry_uid:{0}".format(binary)
            type_query = "loop"
        
        json_file = \
        {
            "select": 
            {
                "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                "params": 
                [
                    {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_query},
                    {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                    {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : type_query},
                    {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                    {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : ['entry_uid']},
                    {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : False},
                    {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : None}
                ]
            }
        }
            
        result = ""
        try:
            result = plugin.execute_plugin_by_file(query_uid,
                                          json_file,
                                          self.username,
                                          self.password)
        except:
            print sys.exc_info()[1]
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
         
        r = json.loads(result)
          
        result = []
        for l in r['data']:
            result.append(str(l['entry_uid']))
        
        if len(result) > 1:
            if result[len(result)-1] == "":
                del(result[len(result)-1])
        array_uid = map(cti.CTI_UID, result)
        loop_uids += array_uid
        self.output_params['init'].params['loops'][cti.META_ATTRIBUTE_VALUE] = loop_uids
       
        # create the alias
        bin_alias = alias.get_data_alias(binary)
        if not bin_alias:
            bin_alias = ""
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        alias_e = "Lib_Counting_%s_%s" % (bin_alias, date)
      
        data_entry = self.default_init_command(params, alias_e = alias_e)
       
        plugin.plugin_auto_run(self, data_entry, None, auto_run)
        return 0
    
    #---------------------------------------------------------------------------
    
    @hapi_command("run")
    def run_cmd(self, params):
        """ Run lib_counting analysis
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
       
       
        common_tool_dir = cti.cti_plugin_config_get_value(cti.COMMON_TOOLS_DIR)
        maqao_dir = os.path.join(cti.cti_plugin_config_get_value(cti.THIRD_PARTY_DIR), "maqao")
        if not os.path.isdir(maqao_dir):
            util.cti_plugin_print_error("MAQAO is not installed\nYou can find the tool here: http://www.maqao.org/\n")
            util.cti_plugin_print_error("Install the tool in the third-party directory (third-party/maqao/).\n")
            return(cti.CTI_PLUGIN_ERROR_TOOL_NOT_FOUND)
        
        #get parameters
        lib_counting_uid = params["entry"]
        lib_counting_results_plugin_uid = params["lib_counting_results_plugin_uid"]
        nb_run_experiment = params["nb_run_lib_counting"]
        lib_counting_group_plugin_uid = params["lib_counting_group_plugin_uid"]
                
        (input_param, output_param) = entry.load_data(lib_counting_uid)
        
        #Load params from the lib_counting entry
        binary_uid = output_param["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        loops_uids = output_param["init"].params["loops"][cti.META_ATTRIBUTE_VALUE]
        mode = output_param["init"].params["mode"][cti.META_ATTRIBUTE_VALUE]
        partition = output_param["init"].params["partition"][cti.META_ATTRIBUTE_VALUE]
        platform = output_param["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
        prefix_execute_script = output_param["init"].params["prefix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        suffix_execute_script = output_param["init"].params["suffix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        submitter_cmd = output_param["init"].params["submitter_cmd"][cti.META_ATTRIBUTE_VALUE]
        submitter_opt = output_param["init"].params["submitter_opt"][cti.META_ATTRIBUTE_VALUE]
        submitter_user = output_param["init"].params["submitter_user"][cti.META_ATTRIBUTE_VALUE]
        run_parameters = output_param["init"].params["run_parameters"][cti.META_ATTRIBUTE_VALUE]
        uarch = output_param["init"].params["uarch"][cti.META_ATTRIBUTE_VALUE]
        frequency = output_param["init"].params["frequency"][cti.META_ATTRIBUTE_VALUE]
        counter_list = output_param["init"].params["counter_list"][cti.META_ATTRIBUTE_VALUE]
        special_permissions = output_param["init"].params["special_permissions"][cti.META_ATTRIBUTE_VALUE]
        
        # Check parameters
        if not mode:
            mode = "local"
        elif mode not in ['local', 'ssh', 'slurm']:
            util.cti_plugin_print_error("Unknown mode '{0}'.\n".format(mode))
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        
        #Display platform warning
        util.cti_plugin_print_warning("WARNING: The job must be launched on a machine "+\
                         "with a CPU architecture corresponding to {0}\n".format(uarch))
        
        #Display permissions warning
        if special_permissions:
            util.cti_plugin_print_warning("WARNING: The job must be launched on a machine "+\
                         "where your current user has admin rights\n")
        
        # get the binary path from the binary entry
        (_, output_binary) = entry.load_data(binary_uid)
        binary = output_binary["init"].params["binary_file"][cti.META_ATTRIBUTE_VALUE]
        binary_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, binary_uid), 
                                   cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                   binary)
        
        
        # get compiled application path
        compile_uid = output_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]
        compiled_application_path = None
        if compile_uid:
            (_, output_compile) = entry.load_data(compile_uid)
            compiled_application = output_compile["init"].params["compiled_application"][cti.META_ATTRIBUTE_VALUE]
            if compiled_application:
                compiled_application_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, compile_uid), 
                                                 cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                                 compiled_application)
        
        #Get the loop ID list
        loop_id_list = []
        for loop_uid in loops_uids:
            (_, loop_output) = entry.load_data(loop_uid)
            loop_id_list.append(loop_output['init'].params['loop_id'][cti.META_ATTRIBUTE_VALUE])
        if not loop_id_list:
            util.cti_plugin_print_error("No loops have been found/given.\nAborting run.\n")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        str_loop_ids = ",".join(str(x) for x in loop_id_list)
        
        
        #Get the list of hardware counters
        (_, counting_results_output) = entry.load_defaults(lib_counting_results_plugin_uid)
        all_counters = counting_results_output["init"].params['counters'][cti.META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS]
        all_counters = [x.replace(' ','_') for x in all_counters]
        
        #Trim down the list to get only those coresponding to the given microarchitecture
        hardware_counters = []
        for counter in all_counters:
            if counter.startswith(uarch):
                hardware_counters.append(counter[len(uarch+"_"):])
        
        #Trim down the list to match the given one if any.
        final_counter_list = []
        if counter_list:
            for x in counter_list:
                if x in hardware_counters:
                    final_counter_list.append(x)
                else:
                    util.cti_plugin_print_error("Unknown counter for architecture {0}: {1}.\nAborting run.\n".format(uarch, x))
                    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        else:
            final_counter_list = hardware_counters

        if not final_counter_list:
            util.cti_plugin_print_error("No counters have been found/given.\nAborting run.\n")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        
        # Create all patched binaries
        ## Create a sandbox to store patched binaries
        tmp_dir = tempfile.mkdtemp()
        old_dir = os.getcwd()
        os.chdir(tmp_dir)
        
        ## Get mil template scripts
        mil_script_template = "mil_script_lib_counting_hardware_counters_template.lua"
        mil_measure_app = "measure_app.mil"
        mil_counting_path = os.path.join(tmp_dir, "mil_script_counting_current.lua")
        
        shutil.copy(os.path.join(common_tool_dir, mil_script_template), tmp_dir)
        shutil.copy(os.path.join(common_tool_dir, mil_measure_app), tmp_dir)
        
        ## Get the binary
        shutil.copy(binary_path, tmp_dir)
        tar_file = tarfile.open(binary)
        tar_file.extractall()
        tar_file.close()
        binary_name = binary.replace(".tar.gz", "")
        os.remove(binary)
        
        ## Fill general placeholders in the mil counters script
        file_replace(
                        mil_script_template,
                        [
                            ("#__CTI_PLACEHOLDER_LOOP_NUMBER__#", str(len(loop_id_list)) ),
                            ("#__CTI_PLACEHOLDER_BIN_PATH__#", binary_name),
                            ("#__CTI_PLACEHOLDER_LOOP_LIST__#", str_loop_ids),
                            ("#__CTI_PLACEHOLDER_TYPE_COUNTING__#", "counting_dump_file_accumulate"),
                            ("#__CTI_PLACEHOLDER_STOP_COUNTING__#", "counting_stop_counting_and_accumulate"),
                            ("#__CTI_PLACEHOLDER_LIB__#", "perfeval_c.so")
                        ]
                    )
        ## Fill general placeholders in the mil application cycles script
        file_replace(
                        mil_measure_app,
                        [
                            ("#__CTI_PLACEHOLDER_BIN_PATH__#", binary_name),
                            ("#__CTI_PLACEHOLDER_LIB__#", "lib_measure_app.so")
                        ]
                    )
        
        ## process maqao commands
        maqao_bin = os.path.join(maqao_dir, "maqao")
        maqao_counters_cmd = "{0} mil -i={1}".format(maqao_bin, mil_counting_path)
        maqao_global_cycles = "{0} instrument -i={1}".format(maqao_bin, mil_measure_app)
        
        madras_log_to_import = []
        mil_counting_scripts_to_import = []

        dict_opt = {}
        dict_opt["OTHER"] = [
                                os.path.join(common_tool_dir, "lib_measure_app.so"),
                                os.path.join(common_tool_dir, "perfeval_c.so"),
                                os.path.join(common_tool_dir, "libvprof.so")
                            ]
        bin_to_import = []
        patched_binary = os.path.basename(binary_name) + "_i"

        #Generating all mil counting scripts and import them into the lib_counting entry before execution.
        #Allows to retrieve them from the entry without having to wait for the end of plugin execution.
        mil_counting_scripts = []

        for counter in final_counter_list:
            script_name = "mil_script_counting_{0}.lua".format(counter)
            file_replace(mil_script_template, [('#__CTI_PLACEHOLDER_PROBE_LIST__#', counter)], script_name)
            mil_counting_scripts.append(script_name)
            
            #Importing the mil counters script into the lib_counting entry.
            try:
                entry.put_file_in_entry(lib_counting_uid, script_name, False)
                mil_counting_scripts_to_import.append(script_name)
            except IOError:
                util.cti_plugin_print_error("CTI error: No such file: '" + mil_counting_path + "' \n")
            
            entry.update_entry_parameter(
                lib_counting_uid,
                {
                    "mil_counting_scripts"  : {"value" : mil_counting_scripts_to_import}
                })
        
        print("Mil counting scripts importation successful.")

        ### Create binaries for hardware counter
        for counter, mil_script in zip(final_counter_list, mil_counting_scripts):
            print "Running maqao mil for hardware counter {0}".format(counter)
            sys.stdout.flush()

            #Setting the mil script.
            shutil.copyfile(mil_script, mil_counting_path)

            # patch binary
            patch_binary(maqao_counters_cmd)
            if os.path.isfile(patched_binary):
                new_binary_name = patched_binary + "_" + counter
                os.rename(patched_binary, new_binary_name)
                
                # get the madras log
                madras_log_name = "{0}_{1}.log".format(MADRAS_LOG, counter)
                os.rename(MADRAS_LOG + ".log", madras_log_name)
                try:
                    log_f = entry.put_file_in_entry(lib_counting_uid, madras_log_name, False)
                    madras_log_to_import.append(log_f)
                except IOError:
                    util.cti_plugin_print_error("CTI error: No such file: '" + madras_log_name + "' \n")
 
                # Tar the binary
                tar_binary = new_binary_name + ".tar.gz"
                tar_file = tarfile.open(tar_binary, "w:gz")
                tar_file.add(new_binary_name)
                tar_file.close()
                os.remove(new_binary_name)
                dict_opt["OTHER"].append(os.path.abspath(tar_binary))
                
                try:
                    bin_f = entry.put_file_in_entry(lib_counting_uid, tar_binary, False)
                    bin_to_import.append(bin_f)
                except IOError:
                    util.cti_plugin_print_error("CTI error: No such file: '" + tar_binary + "' \n")
            else:
                util.cti_plugin_print_error("CTI error: No such file: '" + patched_binary + "' \n")
        
        ### Create binaries for the total number of cylces for the application
        patch_binary(maqao_global_cycles)
        if os.path.isfile(patched_binary):
            new_binary_name = patched_binary + "_global_cycles"
            os.rename(patched_binary, new_binary_name)
            
            # Tar the binary
            tar_binary = new_binary_name + ".tar.gz"
            tar_file = tarfile.open(tar_binary, "w:gz")
            tar_file.add(new_binary_name)
            tar_file.close()
            os.remove(new_binary_name)
            dict_opt["OTHER"].append(os.path.abspath(tar_binary))
            
            try:
                bin_f = entry.put_file_in_entry(lib_counting_uid, tar_binary, False)
                bin_to_import.append(bin_f)
            except IOError:
                util.cti_plugin_print_error("CTI error: No such file: '" + tar_binary + "' \n")
        else:
            util.cti_plugin_print_error("CTI error: No such file: '" + patched_binary + "' \n")
        
        ## Clean the sandbox
        os.system("rm *.lua")
        os.system("rm *.meta")
        os.system("rm {0}_*.log".format(MADRAS_LOG))
        os.remove(binary_name)
        
        ## Update lib_counting entry
        entry.update_entry_parameter(
            lib_counting_uid,
            {
                "patched_binaries"      : {"value" : bin_to_import, "append" : True},
                "madras_logs"           : {"value" : madras_log_to_import, "append" : True}
            })
        os.chdir(old_dir)
       
        # create the arguments
        work_dir = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)
        dict_opt["APP"] = [binary_path]
        dict_opt["SCRIPTS"] = []
        if params["prefix"]:
            prefix_temp = os.path.join(tempfile.mkdtemp(), "prefix.sh")
            shutil.copy(params["prefix"], prefix_temp)
            dict_opt["SCRIPTS"].append(prefix_temp)
        dict_opt["TOOL"] = [maqao_dir]

        dict_opt["OTHER"].append(compiled_application_path)
        dict_opt["platform"] = platform
        dict_opt["entry"] = params["entry"]
        dict_opt["plugin_execute_script"] = os.path.join(self.plugin_directory, "execute_lib_counting.py") 
        dict_opt["partition"] = partition
        dict_opt["submitter_cmd"] = submitter_cmd
        dict_opt["submitter_opt"] = submitter_opt
        dict_opt["submitter_user"] = submitter_user
        dict_opt["prefix_execute_script"] = prefix_execute_script
        dict_opt["suffix_execute_script"] = suffix_execute_script
        dict_opt["username"] = self.username

        # create the script parameters file
        dict_opt["script_params"] = tempfile.mkstemp(dir=work_dir)[1]
        script_params_f = open(dict_opt["script_params"], "w")
        script_params_f.write(str(nb_run_experiment) + "\n")
        script_params_f.write(str_loop_ids + "\n")
        script_params_f.write('"'+",".join(str(x) for x in final_counter_list)+'"' + "\n")
        if not run_parameters:
            run_parameters = ""
        script_params_f.write(run_parameters + "\n")
        script_params_f.write(str(special_permissions) + "\n")

        if compiled_application_path:
            script_params_f.write(os.path.basename(compiled_application_path) + "\n")
        else:
            script_params_f.write("\n")

        script_params_f.close()

        (job_dir, daemon_log_file) = submitter.submitter(dict_opt, self.plugin_uid, mode)
        execution_log_file = None
        # collect execution log file
        for f in glob.glob("*.out"):
            if f != daemon_log_file:
                execution_log_file = os.path.abspath(f)

        dict_opt["results_dir"] = os.path.join(work_dir, job_dir + "_local", job_dir, "RESULTS")

        message_daemon = ""
        nb_files = 0
        for f in os.walk("RESULTS"): 
             nb_files += len(f[2])

        file_daemon = open(daemon_log_file, "a")

        dict_opt["nb_run_experiment"] = nb_run_experiment
        dict_opt["frequency"] = frequency
        dict_opt["uarch"] = uarch
        dict_opt["loop_id_list"] = loop_id_list
        dict_opt["loop_uid_list"] = loops_uids
        dict_opt["counter_list"] = final_counter_list
        dict_opt["username"] = self.username
        dict_opt["password"] = self.password
        dict_opt["execution_log"] = execution_log_file
        dict_opt["daemon_log"] = daemon_log_file
        dict_opt["lib_counting_group_plugin_uid"] = lib_counting_group_plugin_uid
        dict_opt["lib_counting_results_plugin_uid"] = lib_counting_results_plugin_uid


        if nb_files == 0:
            execution_log_f = open(execution_log_file, "r")
            execution_log_message = execution_log_f.readlines()
            execution_log_f.close()
            execution_log_message = " ".join(execution_log_message)

            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] ERROR: no result files generated\n\tThe error message is:\n\
                    =================================\n%s\n=================================\n\
                    Temporary files are located in the directory %s" \
                    % (date, execution_log_message, os.getcwd())
            util.cti_plugin_print_error(message)
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            import_lib_counting_results.import_results(dict_opt)
        else:
            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] Importing results into CTI..." % date
            print message
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            # import parameters
            import_lib_counting_results.import_results(dict_opt)

            # cleaning temporary files
            shutil.rmtree(os.path.join(work_dir, job_dir + "_local"))

    #---------------------------------------------------------------------------
    
    @hapi_command("run_fast")
    def run_fast_cmd(self, params):
        """ Run lib_counting analysis in a quicker way
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        #---------------------------------------------------------------------------
	if params["prefix"]:
            params["prefix"] = os.path.abspath(params["prefix"])
        
        common_tool_dir = cti.cti_plugin_config_get_value(cti.COMMON_TOOLS_DIR)
        maqao_dir = os.path.join(cti.cti_plugin_config_get_value(cti.THIRD_PARTY_DIR), "maqao")
        if not os.path.isdir(maqao_dir):
            util.cti_plugin_print_error("MAQAO is not installed\nYou can find the tool here: http://www.maqao.org/\n")
            util.cti_plugin_print_error("Install the tool in the third-party directory (third-party/maqao/).\n")
            return(cti.CTI_PLUGIN_ERROR_TOOL_NOT_FOUND)
        
        #get parameters
        lib_counting_uid = params["entry"]
        lib_counting_results_plugin_uid = params["lib_counting_results_plugin_uid"]
        lib_counting_group_plugin_uid = params["lib_counting_group_plugin_uid"]
                
        (input_param, output_param) = entry.load_data(lib_counting_uid)
        
        #Load params from the lib_counting entry
        binary_uid = output_param["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        loops_uids = output_param["init"].params["loops"][cti.META_ATTRIBUTE_VALUE]
        mode = output_param["init"].params["mode"][cti.META_ATTRIBUTE_VALUE]
        partition = output_param["init"].params["partition"][cti.META_ATTRIBUTE_VALUE]
        platform = output_param["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
        prefix_execute_script = output_param["init"].params["prefix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        suffix_execute_script = output_param["init"].params["suffix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        submitter_cmd = output_param["init"].params["submitter_cmd"][cti.META_ATTRIBUTE_VALUE]
        submitter_opt = output_param["init"].params["submitter_opt"][cti.META_ATTRIBUTE_VALUE]
        submitter_user = output_param["init"].params["submitter_user"][cti.META_ATTRIBUTE_VALUE]
        run_parameters = output_param["init"].params["run_parameters"][cti.META_ATTRIBUTE_VALUE]
        uarch = output_param["init"].params["uarch"][cti.META_ATTRIBUTE_VALUE]
        frequency = output_param["init"].params["frequency"][cti.META_ATTRIBUTE_VALUE]
        counter_list = output_param["init"].params["counter_list"][cti.META_ATTRIBUTE_VALUE]
        special_permissions = output_param["init"].params["special_permissions"][cti.META_ATTRIBUTE_VALUE]
        
        # Check parameters
        if not mode:
            mode = "local"
        elif mode not in ['local', 'ssh', 'slurm']:
            util.cti_plugin_print_error("Unknown mode '{0}'.\n".format(mode))
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        
        #Display platform warning
        util.cti_plugin_print_warning("WARNING: The job must be launched on a machine "+\
                         "with a CPU architecture corresponding to {0}\n".format(uarch))
        
        #Display permissions warning
        if special_permissions:
            util.cti_plugin_print_warning("WARNING: The job must be launched on a machine "+\
                         "where your current user has admin rights\n")
        
        # get the binary path from the binary entry
        (_, output_binary) = entry.load_data(binary_uid)
        binary = output_binary["init"].params["binary_file"][cti.META_ATTRIBUTE_VALUE]
        binary_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, binary_uid), 
                                   cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                   binary)
        
        
        # get compiled application path
        compile_uid = output_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]
        compiled_application_path = None
        if compile_uid:
            (_, output_compile) = entry.load_data(compile_uid)
            compiled_application = output_compile["init"].params["compiled_application"][cti.META_ATTRIBUTE_VALUE]
            if compiled_application:
                compiled_application_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, compile_uid), 
                                                 cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                                 compiled_application)
        
        #Get the loop ID list
        loop_id_list = []
        for loop_uid in loops_uids:
            (_, loop_output) = entry.load_data(loop_uid)
            loop_id_list.append(loop_output['init'].params['loop_id'][cti.META_ATTRIBUTE_VALUE])
        if not loop_id_list:
            util.cti_plugin_print_error("No loops have been found/given.\nAborting run.\n")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        str_loop_ids = ",".join(str(x) for x in loop_id_list)
        
        
        #Get the list of hardware counters
        (_, counting_results_output) = entry.load_defaults(lib_counting_results_plugin_uid)
        all_counters = counting_results_output["init"].params['counters'][cti.META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS]
        all_counters = [x.replace(' ','_') for x in all_counters]
        
        #Trim down the list to get only those coresponding to the given microarchitecture
        hardware_counters = []
        for counter in all_counters:
            if counter.startswith(uarch):
                hardware_counters.append(counter[len(uarch+"_"):])
        
        #Trim down the list to match the given one if any.
        final_counter_list = []
        if counter_list:
            for x in counter_list:
                if x in hardware_counters:
                    final_counter_list.append(x)
                else:
                    util.cti_plugin_print_error("Unknown counter for architecture {0}: {1}.\nAborting run.\n".format(uarch, x))
                    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        else:
            final_counter_list = hardware_counters

        if not final_counter_list:
            util.cti_plugin_print_error("No counters have been found/given.\nAborting run.\n")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

        dict_opt = {}
        dict_opt["OTHER"] = [
                                os.path.join(common_tool_dir, "lib_measure_app.so"),
                                os.path.join(common_tool_dir, "perfeval_c.so"),
                                os.path.join(common_tool_dir, "libvprof.so"),
                                os.path.join(common_tool_dir, "libdecanrt.so"),
                                os.path.join(common_tool_dir, "libmaqao.so"),
                                os.path.join(common_tool_dir, "libmcommon.so"),
                                os.path.join(common_tool_dir, "libmasm.so"),
                                os.path.join(common_tool_dir, "libdwarfreader.so"),
                                os.path.join(common_tool_dir, "libelf.so"),
                                os.path.join(common_tool_dir, "libmarchx86_64.so"),
                                os.path.join(common_tool_dir, "libextsx86_64.so"),
                                os.path.join(common_tool_dir, "libmarchk1om.so"),
                                os.path.join(common_tool_dir, "libextsk1om.so"),
                                os.path.join(common_tool_dir, "libmcore.so"),
                                os.path.join(common_tool_dir, "libmmaqao.so"),
                                os.path.join(common_tool_dir, "libmdbg.so"),
                                os.path.join(common_tool_dir, "libmdisass.so"),
                                os.path.join(common_tool_dir, "libmdsmblx86_64.so"),
                                os.path.join(common_tool_dir, "libmdsmblk1om.so"),
                                os.path.join(common_tool_dir, "libmtroll.so"),
                                os.path.join(common_tool_dir, "libmasmbl.so"),
                                os.path.join(common_tool_dir, "libmasmblx86_64.so"),
                                os.path.join(common_tool_dir, "libmextx86_64.so"),
                                os.path.join(common_tool_dir, "libmpatchx86_64.so"),
                                os.path.join(common_tool_dir, "libmasmblk1om.so"),
                                os.path.join(common_tool_dir, "libmpatchk1om.so"),
                                os.path.join(common_tool_dir, "libmextk1om.so"),
                                os.path.join(common_tool_dir, "libdwarf.so"),
                                os.path.join(common_tool_dir, "abstract_objects_c.so")
                            ]

        ## Create a sandbox to store patched binaries
        tmp_dir = tempfile.mkdtemp()
        old_dir = os.getcwd()
        os.chdir(tmp_dir)
 
        ## Get the binary
        shutil.copy(binary_path, tmp_dir)
        tar_file = tarfile.open(binary)
        tar_file.extractall()
        tar_file.close()
        binary_name = binary.replace(".tar.gz", "")
        os.remove(binary)
        
        # Create the patched binary
        maqao_bin = os.path.join(maqao_dir, "maqao")
        mil_measure_app = "measure_app.mil"
        shutil.copy(os.path.join(common_tool_dir, mil_measure_app), tmp_dir)
        maqao_global_cycles = "{0} instrument -i={1}".format(maqao_bin, mil_measure_app)
        file_replace(
                        mil_measure_app,
                        [
                            ("#__CTI_PLACEHOLDER_BIN_PATH__#", binary_name),
                            ("#__CTI_PLACEHOLDER_LIB__#", "lib_measure_app.so")
                        ]
                    )

        patched_binary = os.path.basename(binary_name) + "_i"
        patch_binary(maqao_global_cycles)
        if os.path.isfile(patched_binary):
            new_binary_name = patched_binary + "_global_cycles"
            os.rename(patched_binary, new_binary_name)
            
            # Tar the binary
            tar_binary = new_binary_name + ".tar.gz"
            tar_file = tarfile.open(tar_binary, "w:gz")
            tar_file.add(new_binary_name)
            tar_file.close()
            os.remove(new_binary_name)
            dict_opt["OTHER"].append(os.path.abspath(tar_binary))
            
        else:
            util.cti_plugin_print_error("CTI error: No such file: '" + patched_binary + "' \n")
 
        # create the arguments
        work_dir = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)
        dict_opt["APP"] = [binary_path]
        dict_opt["SCRIPTS"] = []
        if params["prefix"]:
            prefix_temp = os.path.join(tempfile.mkdtemp(), "prefix.sh")
            shutil.copy(params["prefix"], prefix_temp)
            dict_opt["SCRIPTS"].append(prefix_temp)
        dict_opt["TOOL"] = [maqao_dir]

        dict_opt["OTHER"].append(compiled_application_path)
        dict_opt["platform"] = platform
        dict_opt["entry"] = params["entry"]
        dict_opt["plugin_execute_script"] = os.path.join(self.plugin_directory, "execute_fast_exp.py") 
        dict_opt["partition"] = partition
        dict_opt["submitter_cmd"] = submitter_cmd
        dict_opt["submitter_opt"] = submitter_opt
        dict_opt["submitter_user"] = submitter_user
        dict_opt["prefix_execute_script"] = prefix_execute_script
        dict_opt["suffix_execute_script"] = suffix_execute_script
        dict_opt["username"] = self.username

        # create the script parameters file
        dict_opt["script_params"] = tempfile.mkstemp(dir=work_dir)[1]
        script_params_f = open(dict_opt["script_params"], "w")
        script_params_f.write(str_loop_ids + "\n")
        script_params_f.write('"'+",".join(str(x) for x in final_counter_list)+'"' + "\n")
        if not run_parameters:
            run_parameters = ""
        script_params_f.write(run_parameters + "\n")
        script_params_f.write(str(special_permissions) + "\n")

        if compiled_application_path:
            script_params_f.write(os.path.basename(compiled_application_path) + "\n")
        else:
            script_params_f.write("\n")

        script_params_f.close()

        (job_dir, daemon_log_file) = submitter.submitter(dict_opt, self.plugin_uid, mode)
        execution_log_file = None
        # collect execution log file
        for f in glob.glob("*.out"):
            if f != daemon_log_file:
                execution_log_file = os.path.abspath(f)

        dict_opt["results_dir"] = os.path.join(work_dir, job_dir + "_local", job_dir, "RESULTS")

        message_daemon = ""
        nb_files = 0
        for f in os.walk("RESULTS"): 
             nb_files += len(f[2])

        file_daemon = open(daemon_log_file, "a")

        dict_opt["frequency"] = frequency
        dict_opt["uarch"] = uarch
        dict_opt["loop_id_list"] = loop_id_list
        dict_opt["loop_uid_list"] = loops_uids
        dict_opt["counter_list"] = final_counter_list
        dict_opt["username"] = self.username
        dict_opt["password"] = self.password
        dict_opt["execution_log"] = execution_log_file
        dict_opt["daemon_log"] = daemon_log_file
        dict_opt["lib_counting_group_plugin_uid"] = lib_counting_group_plugin_uid
        dict_opt["lib_counting_results_plugin_uid"] = lib_counting_results_plugin_uid

        if nb_files == 0:
            execution_log_f = open(execution_log_file, "r")
            execution_log_message = execution_log_f.readlines()
            execution_log_f.close()
            execution_log_message = " ".join(execution_log_message)

            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] ERROR: no result files generated\n\tThe error message is:\n\
                    =================================\n%s\n=================================\n\
                    Temporary files are located in the directory %s" \
                    % (date, execution_log_message, os.getcwd())
            util.cti_plugin_print_error(message)
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            #import_lib_counting_results.import_results(dict_opt)
        else:
            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] Importing results into CTI..." % date
            print message
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            # import parameters
            import_fast_exp.import_results(dict_opt)

            # cleaning temporary files
            print os.path.join(work_dir, job_dir + "_local")
            #shutil.rmtree(os.path.join(work_dir, job_dir + "_local"))
 
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = LibCountingPlugin()
    exit(p.main(sys.argv))
