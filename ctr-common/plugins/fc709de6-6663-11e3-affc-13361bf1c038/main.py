#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti
from cti_hapi import description, entry
from cti_hapi.main import HapiPlugin, hapi_command

import sys

class LibCountingResultPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Initialize information for codelet
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        self.work_params = description.description_write(self.command, self.work_params)
                
        #Computing metrics
        params = self.compute_metrics(params)
        
        _, plugin_output_default = entry.load_defaults(self.plugin_uid)
        self.output_params[self.command].params['metrics'] = plugin_output_default['init'].params['metrics']
        self.output_params[self.command].params['metrics'][cti.META_ATTRIBUTE_VALUE] = params['metrics']
        
        self.default_init_command(params)
        return 0
    
    #---------------------------------------------------------------------------
    
    @hapi_command("update")
    def update_cmd(self, params):
        """ Prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        input,output = entry.load_data(params['entry'])
        self.compute_metrics(params, output['init'].params)
        
        result = description.update_entry(self, params)
        return result
    
    #---------------------------------------------------------------------------
    
    def compute_metrics(self, params, output_params=None):
        """ Prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        def is_available(table,index):
            return (table[index] and (table[index][0] is not None))
        
        #Merging old input
        if output_params:
            for curr_param in output_params.values():
                if not curr_param[cti.META_ATTRIBUTE_NAME] in params:
                    params[curr_param[cti.META_ATTRIBUTE_NAME]] = curr_param[cti.META_ATTRIBUTE_VALUE]
                elif params[curr_param[cti.META_ATTRIBUTE_NAME]] is None:
                    params[curr_param[cti.META_ATTRIBUTE_NAME]] = curr_param[cti.META_ATTRIBUTE_VALUE]
        
        counter_vals = params['counters']
        
        _, plugin_output_default = entry.load_defaults(self.plugin_uid)
        metrics_vals = {}
        if 'metrics' in params:
            metrics_vals = params['metrics']
        else:
            metrics_vals = plugin_output_default['init'].params['metrics'][cti.META_ATTRIBUTE_VALUE]
        
        #Setting default value for metrics if needed.
        for metric_name in plugin_output_default['init'].params['metrics'][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES]:
            if not metrics_vals[metric_name]:
                metrics_vals[metric_name] = [None]
        
        #If there is no values, just return with the empty matrix.
        if not counter_vals[counter_vals.keys()[0]]:
            params['metrics'] = metrics_vals
            return params
        
        runtime = None
        if 'cycles' in params and params['cycles'] is not None and 'frequency' in params and params['frequency']:
            runtime = params['cycles'] / params['frequency']
        
        metrics_vals['loop_runtime'][0] = runtime
        
        #Computing metrics
        
        if is_available(counter_vals,'nehalem_westmere_FP_COMP_OPS_EXE_SSE_FP_PACKED') and \
           is_available(counter_vals,'nehalem_westmere_FP_COMP_OPS_EXE_SSE_FP_SCALAR') and \
           runtime:
            metrics_vals['nehalem_westmere_double_precision'][0] = \
                ( 
                  counter_vals['nehalem_westmere_FP_COMP_OPS_EXE_SSE_FP_PACKED'][0]*2+\
                  counter_vals['nehalem_westmere_FP_COMP_OPS_EXE_SSE_FP_SCALAR'][0]
                ) / runtime
        
        if is_available(counter_vals,'nehalem_westmere_L1D_REPL') and \
           is_available(counter_vals,'nehalem_westmere_L1D_M_EVICT') and \
           runtime:
            if counter_vals['nehalem_westmere_L1D_REPL'][0] < 100:
                counter_vals['nehalem_westmere_L1D_REPL'][0] = 0
            if counter_vals['nehalem_westmere_L1D_M_EVICT'][0] < 100:
                counter_vals['nehalem_westmere_L1D_M_EVICT'][0] = 0
 
            metrics_vals['nehalem_westmere_L2_bandwidth'][0] = \
                (
                  counter_vals['nehalem_westmere_L1D_REPL'][0] +\
                  counter_vals['nehalem_westmere_L1D_M_EVICT'][0]
                ) * (10**-6) * 64 / runtime
        
        if is_available(counter_vals,'nehalem_westmere_UNC_L3_MISS_ANY') and \
            is_available(counter_vals,'nehalem_westmere_INSTR_RETIRED'):
            if counter_vals['nehalem_westmere_INSTR_RETIRED'][0] != 0:
                metrics_vals['nehalem_westmere_L3_miss_rate'][0] = \
                counter_vals['nehalem_westmere_UNC_L3_MISS_ANY'][0] / counter_vals['nehalem_westmere_INSTR_RETIRED'][0]
            else:
                metrics_vals['nehalem_westmere_L3_miss_rate'][0] = 0
            print "ERROR: nehalem_westmere_INSTR_RETIRED is 0, this should NEVER happen!!" 
        
        if is_available(counter_vals,'nehalem_westmere_UNC_QMC_NORMAL_READS_ANY') and \
           is_available(counter_vals,'nehalem_westmere_UNC_QMC_WRITES_FULL_ANY') and \
           runtime:
            metrics_vals['nehalem_westmere_memory_bandwidth'][0] = \
                (
                  counter_vals['nehalem_westmere_UNC_QMC_NORMAL_READS_ANY'][0]+\
                  counter_vals['nehalem_westmere_UNC_QMC_WRITES_FULL_ANY'][0]
                ) * (10**-6) * 64 / runtime
        
        if is_available(counter_vals,'sandy_bridge_FP_COMP_OPS_EXE_SSE_FP_PACKED_DOUBLE') and \
           is_available(counter_vals,'sandy_bridge_FP_COMP_OPS_EXE_SSE_SCALAR_DOUBLE') and \
           runtime:
            metrics_vals['sandy_bridge_double_precision'][0] = \
                ( 
                  counter_vals['sandy_bridge_FP_COMP_OPS_EXE_SSE_FP_PACKED_DOUBLE'][0]*2+\
                  counter_vals['sandy_bridge_FP_COMP_OPS_EXE_SSE_SCALAR_DOUBLE'][0]
                ) / runtime
        
        if is_available(counter_vals,'sandy_bridge_L1D_REPLACEMENT') and \
           is_available(counter_vals,'sandy_bridge_L1D_M_EVICT') and \
           runtime:
            if counter_vals['sandy_bridge_L1D_REPLACEMENT'][0] < 100:
               counter_vals['sandy_bridge_L1D_REPLACEMENT'][0] = 0
            if counter_vals['sandy_bridge_L1D_M_EVICT'][0] < 100:
               counter_vals['sandy_bridge_L1D_M_EVICT'][0] = 0
 
            metrics_vals['sandy_bridge_L2_bandwidth'][0] = \
                (
                  counter_vals['sandy_bridge_L1D_REPLACEMENT'][0]+\
                  counter_vals['sandy_bridge_L1D_M_EVICT'][0]
                ) * (10**-6) * 64 / runtime
        
        
        #Normal version
        if is_available(counter_vals,'sandy_bridge_DRAM_DATA_READS') and \
           is_available(counter_vals,'sandy_bridge_DRAM_DATA_WRITES'):
            metrics_vals['sandy_bridge_memory_data_volume'][0] = \
                (
                  counter_vals['sandy_bridge_DRAM_DATA_READS'][0]+\
                  counter_vals['sandy_bridge_DRAM_DATA_WRITES'][0]
                ) *64.0 * (10**-6)
                
        #EX version
        elif is_available(counter_vals,'sandy_bridge_UNC_M_CAS_COUNT_0_RD') and \
             is_available(counter_vals,'sandy_bridge_UNC_M_CAS_COUNT_0_WR') and \
             is_available(counter_vals,'sandy_bridge_UNC_M_CAS_COUNT_1_RD') and \
             is_available(counter_vals,'sandy_bridge_UNC_M_CAS_COUNT_1_WR') and \
             is_available(counter_vals,'sandy_bridge_UNC_M_CAS_COUNT_2_RD') and \
             is_available(counter_vals,'sandy_bridge_UNC_M_CAS_COUNT_2_WR') and \
             is_available(counter_vals,'sandy_bridge_UNC_M_CAS_COUNT_3_RD') and \
             is_available(counter_vals,'sandy_bridge_UNC_M_CAS_COUNT_3_WR'):
            
            metrics_vals['sandy_bridge_memory_data_volume'][0] = \
                (
                  counter_vals['sandy_bridge_UNC_M_CAS_COUNT_0_RD'][0]+\
                  counter_vals['sandy_bridge_UNC_M_CAS_COUNT_0_WR'][0]+\
                  counter_vals['sandy_bridge_UNC_M_CAS_COUNT_1_RD'][0]+\
                  counter_vals['sandy_bridge_UNC_M_CAS_COUNT_1_WR'][0]+\
                  counter_vals['sandy_bridge_UNC_M_CAS_COUNT_2_RD'][0]+\
                  counter_vals['sandy_bridge_UNC_M_CAS_COUNT_2_WR'][0]+\
                  counter_vals['sandy_bridge_UNC_M_CAS_COUNT_3_RD'][0]+\
                  counter_vals['sandy_bridge_UNC_M_CAS_COUNT_3_WR'][0]
                ) * 64.0 * (10**-6)
        
        if runtime and metrics_vals['sandy_bridge_memory_data_volume'][0] is not None:
            metrics_vals['sandy_bridge_memory_bandwidth'][0] = \
                metrics_vals['sandy_bridge_memory_data_volume'][0] / runtime 

        if is_available(counter_vals,'haswell_UOPS_EXECUTED_PORT_PORT_0') and \
           is_available(counter_vals,'haswell_UOPS_EXECUTED_PORT_PORT_1') and \
           runtime:
            metrics_vals['haswell_compute_instructions'][0] = \
                ( 
                  counter_vals['haswell_UOPS_EXECUTED_PORT_PORT_0'][0]+\
                  counter_vals['haswell_UOPS_EXECUTED_PORT_PORT_1'][0]
                ) / runtime
        
        if is_available(counter_vals,'haswell_L1D_REPLACEMENT') and \
           is_available(counter_vals,'haswell_L2_TRANS_L1D_WB') and \
           is_available(counter_vals,'haswell_ICACHE_MISSES') and \
           runtime:
            if counter_vals['haswell_L1D_REPLACEMENT'][0] < 100:
               counter_vals['haswell_L1D_REPLACEMENT'][0] = 0
            if counter_vals['haswell_L2_TRANS_L1D_WB'][0] < 100:
               counter_vals['haswell_L2_TRANS_L1D_WB'][0] = 0
 
            metrics_vals['haswell_L2_bandwidth'][0] = \
                (
                  counter_vals['haswell_L1D_REPLACEMENT'][0]+\
                  counter_vals['haswell_L2_TRANS_L1D_WB'][0]+\
                  counter_vals['haswell_ICACHE_MISSES'][0]
                ) * (10**-6) * 64.0 / runtime
        
        
        #Normal version
        if is_available(counter_vals,'haswell_DRAM_DATA_READS') and \
           is_available(counter_vals,'haswell_DRAM_DATA_WRITES'):
            metrics_vals['haswell_memory_data_volume'][0] = \
                (
                  counter_vals['haswell_DRAM_DATA_READS'][0]+\
                  counter_vals['haswell_DRAM_DATA_WRITES'][0]
                ) *64.0 * (10**-6)
                
        if runtime and metrics_vals['haswell_memory_data_volume'][0] is not None:
            metrics_vals['haswell_memory_bandwidth'][0] = \
                metrics_vals['haswell_memory_data_volume'][0] / runtime 


        params['metrics'] = metrics_vals
        return params

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = LibCountingResultPlugin()
    exit(p.main(sys.argv))
