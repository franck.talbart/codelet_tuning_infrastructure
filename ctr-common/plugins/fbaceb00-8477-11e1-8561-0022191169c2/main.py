#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import plugin, util_uid, entry, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, copy, json, os, StringIO, zipfile, csv

import util as util_e

class EntryPlugin(HapiPlugin):
    @hapi_command("export")
    def export_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        format_output = params["format_output"]
        query = params["query"]
        type_query = params["type_query"]
        query_uid = params["query_uid"]
        view_uid = params["view_uid"]
        repository_query = params["repository_query"]
        uid_list = params["uid_list"]
        filename = params["filename"]
        fields = params["fields"]
        sorted_query = params["sorted_query"]
        separator = params["separator"]
        
        if not filename:
            util.cti_plugin_print_error("The output filename is empty.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        #Computing fields
        if not len(fields):
            produced_by = None
            if uid_list and util_uid.is_valid_uid(str(uid_list[0])):
                info_file = ctr.ctr_plugin_info_file_load_by_uid(cti.CTI_UID(str(uid_list[0])))
                produced_by = ctr.ctr_plugin_info_get_value(info_file, cti.DATA_INFO_PLUGIN_UID)
            elif type_query:
                produced_by = type_query
                
            if produced_by:
                json_file = \
                {
                    "plugin": 
                    {
                        "attributes": {cti.META_ATTRIBUTE_NAME : "plugin"},
                        "params": 
                        [
                            {cti.META_ATTRIBUTE_NAME : "plugin_uid",cti.META_ATTRIBUTE_VALUE : produced_by},
                            {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"}
                        ]
                    }
                }
                sys.stdout.flush()
                try:
                    result = plugin.execute_plugin_by_file(view_uid,
                                                  json_file,
                                                  self.username,
                                                  self.password,
                                                  )
                    result = json.loads(result)
                except:
                    print sys.exc_info()[1]
                    return cti.CTI_PLUGIN_ERROR_UNEXPECTED
                if not result["output_file"].has_key("init"):
                    util.cti_plugin_print_error("Init command not found in the data")
                    exit(cti.CTI_PLUGIN_ERROR_INIT_NOT_FOUND)
                fields = ["alias"] + [format(param[cti.META_ATTRIBUTE_NAME]) for param in result["output_file"]["init"]["params"]]
            else:
                util.cti_plugin_print_warning('No uid_list nor type selected. Default query fields used.')
        
        #Repository
        repo = repository_query
        #Checking the argument itself.
        if repo not in [cti.COMMON_REPOSITORY, cti.TEMP_REPOSITORY, cti.ALL_REPOSITORY]:
            if util_uid.is_valid_uid(repo):
                repo = cti.cti_plugin_alias_repository_get_key(cti.CTI_UID(repo))
            repo = util_uid.CTI_UID(repo, cti.CTR_ENTRY_REPOSITORY)
            
            if repo is None:
                util.cti_plugin_print_error("Unknown repository '{0}'.".format(repository_query))
                exit(cti.CTI_ERROR_INVALID_ARGUMENT)
        
        repository_query = repo
        
        #Getting the path if needed
        if util_uid.is_valid_uid(repository_query):
            repos = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(util_uid.CTI_UID(str(repository_query), cti.CTR_ENTRY_REPOSITORY))
        elif repository_query == cti.COMMON_REPOSITORY:
            repos = ctr.ctr_plugin_get_common_dir()
        elif repository_query == cti.TEMP_REPOSITORY:
            repos = ctr.ctr_plugin_get_temp_dir()
        
        print "Retrieving the entries..."
        query_result = {}
        if(uid_list):
            overflow_limit = 80
            first_time = True
            processed_entries = 0
            while(uid_list):
                #Add the specified UIDs to the query
                query_add = "entry_uid:({0})".format(' OR '.join([str(x) for x in uid_list[:overflow_limit]]))
                processed_entries += min(overflow_limit, len(uid_list))
                uid_list = uid_list[overflow_limit:]
                if first_time:
                    if not query or query == "*":
                        query = query_add
                    else:
                        if repos:
                            query = '({0}) AND entry_info.path_repository:{1}'.format(query, repos)
                            repository_query = 'all'
                        query = '({0}) OR {1}'.format(query, query_add)
                else:
                    query = query_add
                
                #Determine the uid type
                if not type_query:
                    if params["uid_list"]:
                        type_query = entry.load_data_info(params["uid_list"][0])[cti.DATA_INFO_PLUGIN_UID]
                    else:
                        util.cti_plugin_print_error("No input data")
                        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                        
                if query and query.strip() != "":
                    json_file = \
                    {
                        "select": 
                        {
                            "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                            "params": 
                            [
                                {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_query},
                                {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                                {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : type_query},
                                {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                                {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : fields},
                                {cti.META_ATTRIBUTE_NAME : "sorted",cti.META_ATTRIBUTE_VALUE : sorted_query},
                                {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : False},
                                {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1}
                            ]
                        }
                    }
                    result = ""
                    try:
                        result = plugin.execute_plugin_by_file(query_uid, json_file, self.username,
                                                                        self.password)
                        if not query_result:
                            query_result = json.loads(result)
                        else:
                            added_result = json.loads(result)
                            query_result['data'] += added_result['data']
                            query_result['total'] += added_result['total']
                    except:
                        print sys.exc_info()[1]
                        exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
                if first_time:
                    processing_total = len(uid_list) + query_result['total']
                    processed_entries = query_result['total']
                    first_time = False
                print 'Retrieved {0}/{1} entries'.format(processed_entries, processing_total)
                
        else:
            if query and query.strip() != "":
                json_file = \
                {
                    "select": 
                    {
                        "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                        "params": 
                        [
                            {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_query},
                            {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                            {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : type_query},
                            {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                            {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : fields},
                            {cti.META_ATTRIBUTE_NAME : "sorted",cti.META_ATTRIBUTE_VALUE : sorted_query},
                            {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : False},
                            {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1}
                        ]
                    }
                }
                try:
                    result = plugin.execute_plugin_by_file(query_uid, json_file, self.username,
                                                                    self.password)
                    if not query_result:
                        query_result = json.loads(result)
                    else:
                        added_result = json.loads(result)
                        query_result['data'] += added_result['data']
                        query_result['total'] += added_result['total']
                except:
                    print sys.exc_info()[1]
                    exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            
        if not (query_result and len(query_result['data'])):
            util.cti_plugin_print_error("No data to extract")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        
        #################### CSV FORMAT #######################
        if format_output == "csv":
            output = open(filename, 'wb')
            writer_obj = csv.DictWriter(output, copy.copy(fields), delimiter=separator, lineterminator='\n', extrasaction='ignore')
            writer_obj.writer.writerow(query_result['header'])
            for row in query_result['data']:
                for cname in row:
                    if type(row[cname]) == float:
                        row[cname] = "%.3f" % row[cname]
                    if row[cname] is None:
                        row[cname] = "None"
                writer_obj.writerow(row)
            writer_obj
            output.close()
        else:
            util.cti_plugin_print_error("Unknown output format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------

    @hapi_command("archive")
    def archive_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters
        """
        uid = params["data_uid"]
        output_filename = os.path.abspath(params["output_filename"])
        
        path = os.path.join(ctr.ctr_plugin_get_path_by_uid(
                                    cti.CTR_ENTRY_DATA, 
                                    uid
                                ),
                                cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR)
                            )
        
        archive = StringIO.StringIO()
        
        zf = zipfile.ZipFile(archive,'w', zipfile.ZIP_DEFLATED)
        try:
            os.chdir(path)
            files_l = util_e.list_files('.')
            for name in files_l:
                zf.write(name)
        
        finally:
            zf.close()
            archive.seek(0)
            
            filout = open(output_filename, 'w+')
            filout.write(archive.read())
            filout.close()
            archive.close()
            
            print "Done!"
#---------------------------------------------------------------------------
    @hapi_command("archive_file")
    def archive_file_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters
        Returns:
          the list of local repositories
        """
        util_e.download_file(params)

#---------------------------------------------------------------------------
    @hapi_command("clone")
    def clone_cmd(self, params):
        """ Clone an entry

        Args:
            self: class of the plugin
            params: working parameters
        Returns:
          the new entry UID
        """
        
        format_o = self.work_params[self.command].params["format"][cti.META_ATTRIBUTE_VALUE]
        entry_uid = self.work_params[self.command].params["entry"][cti.META_ATTRIBUTE_VALUE]
        rep = self.work_params[self.command].params["repository"][cti.META_ATTRIBUTE_VALUE]
        
        if rep != cti.COMMON_REPOSITORY and rep != cti.TEMP_REPOSITORY:
            # check if it is a valid repository
            rep = util_uid.CTI_UID("repo_" + str(rep))
        if not rep or rep is None:
            rep = self.work_params[self.command].attributes[cti.META_ATTRIBUTE_REP]
        
        # get info from the entry
        entry_info = entry.load_data_info(entry_uid)
        entry_alias = entry_info[cti.DATA_INFO_ALIAS]
        if not entry_alias:
            entry_alias = "No_alias"
        entry_alias = entry_alias + "_clone"
        
        (inp, out) = entry.load_data(entry_uid)
        
        # delete references
        for json_r in (inp, out):
            if json_r.has_key("init"):
                params_list = json_r["init"].params
                for p in params_list:
                    if params_list[p].has_key(cti.META_ATTRIBUTE_TYPE) and \
                        params_list[p][cti.META_ATTRIBUTE_TYPE] == cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
                        value = None
                        if params_list[p].has_key(cti.META_ATTRIBUTE_LIST) and \
                           params_list[p][cti.META_ATTRIBUTE_LIST]:
                            value = []
                        params_list[p][cti.META_ATTRIBUTE_VALUE] = value
        
        # create the new entry
        data_entry = entry.create_data_entry(util_uid.CTI_UID(entry_info[cti.DATA_INFO_PLUGIN_UID], cti.CTR_ENTRY_PLUGIN), rep, self.username)
        
        out.record_output("init", data_entry.path)
        inp.record_input("init", data_entry.path)
        
        # copy files
        entry_files_dir_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, entry_uid), 
                                            cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR))
        if os.path.isdir(entry_files_dir_path):
            os.system("cp -r %s %s" % (entry_files_dir_path, data_entry.path))
        
        print plugin.plugin_exit(data_entry.entry, format_o, alias_e=entry_alias)
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = EntryPlugin()
    exit(p.main(sys.argv))
