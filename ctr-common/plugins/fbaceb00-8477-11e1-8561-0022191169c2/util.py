#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import ctr, cti

import os, StringIO, zipfile

#---------------------------------------------------------------------------

def list_files(path):
    files_l = []
    for i in os.listdir(path):
        new_path = os.path.join(path, i)
        if os.path.isdir(new_path):
            files_l.extend(list_files(new_path))
        else:
            files_l.append(new_path)
    return files_l

#---------------------------------------------------------------------------

def download_file(params):
    """ Create an archive of a single file taken from the entry

    Returns:
      None
    """
    output_filename = os.path.abspath(params["output_filename"])
    filename = params["filename"]
    filename = os.path.basename(filename)
    path = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, params["data_uid"])
    path = os.path.join(path, cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), filename)
    archive = StringIO.StringIO()

    zf = zipfile.ZipFile(archive,'w', zipfile.ZIP_DEFLATED)
    try:
       os.chdir(os.path.dirname(path))
       zf.write(os.path.basename(path))
    finally:
       zf.close()
       archive.seek(0)
       filout = open(output_filename, 'w+')
       filout.write(archive.read())
       filout.close()
       archive.close()
       
#---------------------------------------------------------------------------
