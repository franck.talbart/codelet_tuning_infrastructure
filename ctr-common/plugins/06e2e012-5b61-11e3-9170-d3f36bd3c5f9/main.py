#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi import entry
from cti_hapi.main import HapiPlugin, hapi_command

import sys

class LoopPlugin(HapiPlugin):
    @hapi_command("update")
    def update_cmd(self, params):
        """ Update a binary entry
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
            Nothing
        """
        data_entry = self.default_update_command(params)
        if params['maqao_cqa_results']:
            entry.update_entry_parameter(params['maqao_cqa_results'], {"loop": {"value": data_entry.uid}})
        
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = LoopPlugin()
    exit(p.main(sys.argv))
