#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi.main import HapiPlugin, hapi_command
from cti_hapi import util

import sys

class LoopGroupPlugin(HapiPlugin):
#---------------------------------------------------------------------------
    @hapi_command("init")
    def init_cmd(self, params):
        """ Initialize information for loop_group
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        if sum( bool(x) for x in [params['maqao_perf'], params['icc_profiler'], params['vtune']] ) > 1:
            util.cti_plugin_print_error('A loop_group cannot come from maqao_perf, icc profiler, or Vtune at the same time.')
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        self.default_init_command(params)
        return 0
    
#---------------------------------------------------------------------------
    @hapi_command("update")
    def update_cmd(self, params):
        """ Prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        if sum( bool(x) for x in [params['maqao_perf'], params['icc_profiler'], params['vtune']] ) > 1:
            util.cti_plugin_print_error('A loop_group cannot come from maqao_perf, icc profiler, or Vtune at the same time.')
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        return self.default_update_command(params)

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = LoopGroupPlugin()
    exit(p.main(sys.argv))
