=== Description ===

This plugin describes a set of loops.

=== Usage examples ===

Create a loop_group entry:

<pre>
$ cti loop_group init [<loops>] [<application>] [<maqao_perf>] [<icc_profiler>] [<nb_loops>] [<profiler_version>] [<format>]
</pre>

