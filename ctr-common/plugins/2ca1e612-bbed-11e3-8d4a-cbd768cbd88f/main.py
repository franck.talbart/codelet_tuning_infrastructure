#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import description, entry, plugin, util, util_uid, alias
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, datetime, copy, csv, tempfile, time, json

class DiffPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Prepare parameters

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        
        self.work_params = description.description_write(self.command, self.work_params)
        type_query = self.work_params[self.command].params["type_query"][cti.META_ATTRIBUTE_VALUE]
        column_comparison_1 = self.work_params[self.command].params["column_comparison_1"][cti.META_ATTRIBUTE_VALUE]
        column_comparison_2 = self.work_params[self.command].params["column_comparison_2"][cti.META_ATTRIBUTE_VALUE]
        link_params = self.work_params[self.command].params["link_params"][cti.META_ATTRIBUTE_VALUE]
        margins = self.work_params[self.command].params["margins"][cti.META_ATTRIBUTE_VALUE]
        format_o = self.work_params[self.command].params["format"][cti.META_ATTRIBUTE_VALUE]
        auto_run = self.work_params[self.command].params["auto_run"][cti.META_ATTRIBUTE_VALUE]
        
        # check parameters
        if link_params is None or not link_params:
            util.cti_plugin_print_error("The link_params parameter is empty!")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        # check comparison parameters
        (_, plugin_output_default) = entry.load_defaults(type_query)
        plugin_output_default_params = plugin_output_default["init"].params
        for param in link_params:
            if param not in plugin_output_default_params:
                util.cti_plugin_print_error("Comparison parameter %s not found in plugin %s." % (param, type_query))
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if column_comparison_1 is None:
            column_comparison_1 = []
            self.work_params[self.command].params["column_comparison_1"][cti.META_ATTRIBUTE_VALUE] = column_comparison_1
        if column_comparison_2 is None:
            column_comparison_2 = []
            self.work_params[self.command].params["column_comparison_2"][cti.META_ATTRIBUTE_VALUE] = column_comparison_2
        # check comparison column lists
        index = 0
        for param1 in column_comparison_1:
            param2 = column_comparison_2[index]
            if param1 not in plugin_output_default_params:
                util.cti_plugin_print_error("Comparison parameter %s not found in plugin %s." % (param1, type_query))
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            elif param2 not in plugin_output_default_params:
                util.cti_plugin_print_error("Comparison parameter %s not found in plugin %s." % (param2, type_query))
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            else:
                type1 = plugin_output_default_params[param1][cti.META_ATTRIBUTE_TYPE]
                type2 = plugin_output_default_params[param2][cti.META_ATTRIBUTE_TYPE]
                if type1 != type2:
                    util.cti_plugin_print_error("The parameters %s (%s) and %s (%s) of the column_comparison have not the same type!" % (param1, type1, param2, type2))
                    return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            index += 1
        
        if len(column_comparison_1) != len(column_comparison_2):
            util.cti_plugin_print_error("The column_comparison lists must have the same size!")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if len(margins) != 0 and len(margins) != len(link_params):
            util.cti_plugin_print_error("The margins list must have the same size as the link_params list!")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        if margins is None or not margins:
            margins = []
            index = 0
            for link_param in link_params:
                param_type = plugin_output_default_params[link_param][cti.META_ATTRIBUTE_TYPE]
                if param_type in [cti.META_CONTENT_ATTRIBUTE_TYPE_INTEGER, cti.META_CONTENT_ATTRIBUTE_TYPE_FLOAT]:
                    margins.append(0)
                else:
                    margins.append(None)
                index += 1
            self.work_params[self.command].params["margins"][cti.META_ATTRIBUTE_VALUE] = margins
        else:
            # check the margins values
            index = 0
            for margin in margins:
                param_type = plugin_output_default_params[link_params[index]][cti.META_ATTRIBUTE_TYPE]
                if param_type not in [cti.META_CONTENT_ATTRIBUTE_TYPE_INTEGER, cti.META_CONTENT_ATTRIBUTE_TYPE_FLOAT] and \
                   margin is not None:
                    util.cti_plugin_print_error("The margin for the parameter %s (type: %s) must be 'None'!" % (link_params[index], param_type))
                    return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
                index += 1
        
        # create the alias
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        alias_e = "diff_%s_%s" % (alias.get_plugin_alias(type_query), date)
        
        (data_entry, output) = self.default_init_command(params, alias_e = alias_e, no_output = True)
        res = plugin.plugin_auto_run(self, data_entry, format_o, auto_run, output)
        return res
    
    #---------------------------------------------------------------------------
    
    @hapi_command("run")
    def run_cmd(self, params):
        """ Run diff analysis
        
        Args:
            self: class of the plugin
            params: working parameters
            
        Returns:
            Nothing
        """
        
        #---------------------------------------------------------------------------
        def get_loop_time(entry_param, entry_type):
            """ Get the loop execution time
            
            Args:
                entry_param: the entry parameters
                entry_type: the entry type
            
            Returns:
                The time in second, None instead
            """
            
            if alias.get_plugin_alias(entry_type) == "loop":
                return entry_param["time_sec"][cti.META_ATTRIBUTE_VALUE]
            else:
                return None
        
        #---------------------------------------------------------------------------
        
        def process_entries_not_match(entry_type, query_params, list_entries_match, column_comparison_1, column_comparison_2):
            """ Process entries that do not match
            Args:
                entry_type: the entry type 'A' or 'B'
                query_params: query parameters
                list_entries_match: list of entries that have matched
                column_comparison_1: the column comparison 1
                column_comparison_2: the column comparison 2
            
            """
            
            query = query_params["query"]
            
            time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
            print "\n[%s] Retrieving the entries %s that do not match..." % (time_process, entry_type)
            
            query_result = {}
            if query and query.strip() != "":
                json_file = \
                {
                    "select": 
                    {
                        "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                        "params": 
                        [
                            {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : query_params["repository"]},
                            {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query_params["query"]},
                            {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : query_params["type"]},
                            {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                            {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1},
                            {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : ["entry_info.entry_uid"]}
                        ]
                    }
                }
                result = ""
                try:
                    result = plugin.execute_plugin_by_file(query_params["query_plugin_uid"], json_file, self.username,
                                                                    self.password)
                    if not query_result:
                        query_result = json.loads(result)
                    else:
                        added_result = json.loads(result)
                        query_result["data"] += added_result["data"]
                        query_result["total"] += added_result["total"]
                except:
                    print sys.exc_info()[1]
                    exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            
            if not (query_result and len(query_result["data"])):
                util.cti_plugin_print_error("No data to extract from the query %s" % type_query)
                return None
            
            list_comp = []
            nb_entries_not_match = 0
            time_not_match = 0.0
            total_time = 0.0
            
            list_entry_uid = list(res_data["entry_info.entry_uid"] for res_data in query_result["data"])
            
            # get the entries that do not match
            list_entries_not_match = set(list_entry_uid).difference(list_entries_match)
            
            total_nb_entries = float(len(list_entries_not_match))
            
            for entry_uid in list_entries_not_match:
                nb_entries_not_match += 1
                
                # load entry
                entry_uid = util_uid.CTI_UID(entry_uid)
                (_, output_entry) = entry.load_data(entry_uid)
                params_entry = output_entry["init"].params
                
                percent = int(nb_entries_not_match / total_nb_entries * 100)
                time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
                util.rewrite_output_line("[" + str(time_process) + "] [" + str(percent) +" %] Processing entries ..")
                
                # manage entries with comparison lists
                index = 0
                for comparison_param_2 in column_comparison_2:
                    if comparison_param_2 in params_entry:
                        del(params_entry[comparison_param_2])
                        param_pop = params_entry.pop(column_comparison_1[index])
                        
                        if entry_type == 'A':
                            new_param_name = column_comparison_1[index] + "_" + comparison_param_2
                        else:
                            new_param_name = comparison_param_2 + "_" + column_comparison_1[index]
                        param_pop[cti.META_ATTRIBUTE_NAME] = new_param_name
                        params_entry[new_param_name] = param_pop
                    index += 1
                
                tmp_list = []
                # Add the entry column
                entry_alias = str(alias.get_data_alias(entry_uid))
                tmp_dict = {}
                tmp_dict[cti.META_ATTRIBUTE_NAME] = "entries"
                tmp_dict[cti.META_ATTRIBUTE_DESC] = "the entries"
                tmp_dict[cti.META_ATTRIBUTE_LONG_DESC] = "The entries alias."
                tmp_dict[cti.META_ATTRIBUTE_TYPE] = cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT
                if entry_type == 'A':
                    tmp_dict[cti.META_ATTRIBUTE_VALUE] = entry_alias + ":NA"
                else:
                    tmp_dict[cti.META_ATTRIBUTE_VALUE] = "NA:" + entry_alias
                tmp_list.append(tmp_dict)
                
                # Add parameters column
                for param in params_entry:
                    current_value_param = params_entry[param][cti.META_ATTRIBUTE_VALUE]
                    if param not in black_list_params:
                        for entry_t in ['A', 'B']:
                            tmp_param = copy.copy(params_entry[param])
                            tmp_param[cti.META_ATTRIBUTE_NAME] = str(tmp_param[cti.META_ATTRIBUTE_NAME]) + "_" + entry_t
                            if entry_t == entry_type:
                                tmp_param[cti.META_ATTRIBUTE_VALUE] = str(current_value_param)
                            else:
                                tmp_param[cti.META_ATTRIBUTE_VALUE] = "NA"
                            tmp_list.append(tmp_param)
                
                list_comp.append(tmp_list)
                
                time_e = get_loop_time(params_entry, type_query)
                if time_e is not None:
                    total_time += time_e
                    time_not_match += time_e
            
            dict_return = {}
            dict_return["list_comp"] = list_comp
            dict_return["nb_entries_not_match"] = nb_entries_not_match
            dict_return["time_not_match"] = time_not_match
            dict_return["total_time"] = total_time
            
            return dict_return
        
        #---------------------------------------------------------------------------
        
        # gets parameters
        entry_uid = params["entry"]
        
        (input_param, output_param) = entry.load_data(entry_uid)
        type_query = output_param["init"].params["type_query"][cti.META_ATTRIBUTE_VALUE]
        link_params = output_param["init"].params["link_params"][cti.META_ATTRIBUTE_VALUE]
        column_comparison_1 = output_param["init"].params["column_comparison_1"][cti.META_ATTRIBUTE_VALUE]
        column_comparison_2 = output_param["init"].params["column_comparison_2"][cti.META_ATTRIBUTE_VALUE]
        margins = output_param["init"].params["margins"][cti.META_ATTRIBUTE_VALUE]
        black_list_params = output_param["init"].params["black_list_params"][cti.META_ATTRIBUTE_VALUE]
        
        
        query_1 = output_param["init"].params["query_1"][cti.META_ATTRIBUTE_VALUE]
        query_2 = output_param["init"].params["query_2"][cti.META_ATTRIBUTE_VALUE]
        type_query = output_param["init"].params["type_query"][cti.META_ATTRIBUTE_VALUE]
        repository_query_1 = output_param["init"].params["repository_query_1"][cti.META_ATTRIBUTE_VALUE]
        repository_query_2 = output_param["init"].params["repository_query_2"][cti.META_ATTRIBUTE_VALUE]
        
        time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
        print "\n[%s] Retrieving the entries..." % time_process
        
        query_result = {}
        if query_1 and query_1.strip() != "" and query_2 and query_2.strip() != "":
            json_file = \
            {
                "select_couple": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "select_couple"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "query_1",cti.META_ATTRIBUTE_VALUE : query_1},
                        {cti.META_ATTRIBUTE_NAME : "query_2",cti.META_ATTRIBUTE_VALUE : query_2},
                        {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : type_query},
                        {cti.META_ATTRIBUTE_NAME : "link_params",cti.META_ATTRIBUTE_VALUE : link_params},
                        {cti.META_ATTRIBUTE_NAME : "margins",cti.META_ATTRIBUTE_VALUE : margins},
                        {cti.META_ATTRIBUTE_NAME : "repository_1",cti.META_ATTRIBUTE_VALUE : repository_query_1},
                        {cti.META_ATTRIBUTE_NAME : "repository_2",cti.META_ATTRIBUTE_VALUE : repository_query_2},
                        {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1}
                    ]
                }
            }
            result = ""
            try:
                result = plugin.execute_plugin_by_file(params["query_plugin_uid"], json_file, self.username,
                                                                self.password)
                query_result = json.loads(result)
            except:
                print sys.exc_info()[1]
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            
        if not (query_result and len(query_result["data"])):
            util.cti_plugin_print_error("No data to extract")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        # get total number of entries that match
        total_nb_entries = float(query_result["total"])
        
        # process entries A and B that match
        nb_entries_match = 0
        nb_entries_strict_match = 0
        total_time_A = 0.0
        total_time_B = 0.0
        time_A_scrict_match = 0.0
        time_B_scrict_match = 0.0
        list_comp = []
        dict_match = {}
        list_entries_A = []
        list_entries_B = []
        for (entry_A_uid, entry_B_uid) in query_result["data"]:
            nb_entries_match += 1
            
            if entry_A_uid not in list_entries_A:
                list_entries_A.append(entry_A_uid)
            if entry_B_uid not in list_entries_B:
                list_entries_B.append(entry_B_uid)
            
            # load entry A
            entry_A_uid = util_uid.CTI_UID(entry_A_uid)
            (_, output_A) = entry.load_data(entry_A_uid)
            params_A = output_A["init"].params
            
            # load entry B
            entry_B_uid = util_uid.CTI_UID(entry_B_uid)
            (_, output_B) = entry.load_data(entry_B_uid)
            params_B = output_B["init"].params
            
            percent = int(nb_entries_match / total_nb_entries * 100)
            time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
            util.rewrite_output_line("[" + str(time_process) + "] [" + str(percent) +" %] Processing diff between entries ..")
            
            # check that the match has not been already done
            if entry_B_uid not in dict_match or \
               (entry_B_uid in dict_match and entry_A_uid not in dict_match[entry_B_uid]):
                if entry_A_uid not in dict_match:
                    dict_match[entry_A_uid] = []
                dict_match[entry_A_uid].append(entry_B_uid)
                
                # manage entries A and B with comparison lists
                index = 0
                for comparison_param_1 in column_comparison_1:
                    comparison_param_2 = column_comparison_2[index]
                    if comparison_param_1 in params_B and comparison_param_2 in params_A:
                        del(params_A[comparison_param_2])
                        param_pop_A = params_A.pop(comparison_param_1)
                        new_param_name = comparison_param_1 + "_" + comparison_param_2
                        param_pop_A[cti.META_ATTRIBUTE_NAME] = new_param_name
                        params_A[new_param_name] = param_pop_A
                        
                        del(params_B[comparison_param_1])
                        param_pop_B = params_B.pop(column_comparison_2[index])
                        new_param_name = comparison_param_1 + "_" + column_comparison_2[index]
                        param_pop_B[cti.META_ATTRIBUTE_NAME] = new_param_name
                        params_B[new_param_name] = param_pop_B
                    index += 1
                
                tmp_list = []
                entry_A_alias = str(alias.get_data_alias(entry_A_uid))
                entry_B_alias = str(alias.get_data_alias(entry_B_uid))
                # Add the entry column
                tmp_dict = {}
                tmp_dict[cti.META_ATTRIBUTE_NAME] = "entries"
                tmp_dict[cti.META_ATTRIBUTE_DESC] = "the entries"
                tmp_dict[cti.META_ATTRIBUTE_LONG_DESC] = "The entries alias."
                tmp_dict[cti.META_ATTRIBUTE_TYPE] = cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT
                tmp_dict[cti.META_ATTRIBUTE_VALUE] = entry_A_alias + ":" + entry_B_alias
                tmp_list.append(tmp_dict)
                
                # Add parameters column
                param_stric_match = 0
                for param in params_A:
                    current_value_param_A = params_A[param][cti.META_ATTRIBUTE_VALUE]
                    current_value_param_B = params_B[param][cti.META_ATTRIBUTE_VALUE]
                    if param not in black_list_params:
                        if param not in link_params and current_value_param_A != current_value_param_B:
                            param_stric_match += 1
                        tmp_param = copy.copy(params_A[param])
                        tmp_param[cti.META_ATTRIBUTE_NAME] = str(tmp_param[cti.META_ATTRIBUTE_NAME]) + "_A"
                        tmp_param[cti.META_ATTRIBUTE_VALUE] = str(current_value_param_A)
                        tmp_list.append(tmp_param)
                        
                        tmp_param = copy.copy(params_B[param])
                        tmp_param[cti.META_ATTRIBUTE_NAME] = str(tmp_param[cti.META_ATTRIBUTE_NAME]) + "_B"
                        tmp_param[cti.META_ATTRIBUTE_VALUE] = str(current_value_param_B)
                        tmp_list.append(tmp_param)
                list_comp.append(tmp_list)
                
                time_A = get_loop_time(params_A, type_query)
                if time_A is not None:
                    total_time_A += time_A
                time_B = get_loop_time(params_B, type_query)
                if time_B is not None:
                    total_time_B += time_B
                
                if param_stric_match == len(tmp_list) + 1:
                    nb_entries_strict_match += 1
                    if time_A is not None:
                        time_A_scrict_match += time_A
                    if time_B is not None:
                        time_B_scrict_match += time_B
        
        # get total number of entries A that do not match
        query_params = {"repository": repository_query_1, "query": query_1, "type": type_query, "query_plugin_uid": params["query_plugin_uid"]}
        
        dict_res_A = process_entries_not_match('A', query_params, list_entries_A, column_comparison_1, column_comparison_2)
        
        nb_entries_A_not_match = dict_res_A["nb_entries_not_match"]
        time_A_not_match = dict_res_A["time_not_match"]
        total_time_A += dict_res_A["total_time"]
        list_comp.extend(dict_res_A["list_comp"])
        
        # get total number of entries B that do not match
        query_params["repository"] = repository_query_2
        query_params["query"] = query_2
        
        dict_res_B = process_entries_not_match('B', query_params, list_entries_B, column_comparison_2, column_comparison_1)
        
        nb_entries_B_not_match = dict_res_B["nb_entries_not_match"]
        time_B_not_match = dict_res_B["time_not_match"]
        total_time_B += dict_res_B["total_time"]
        list_comp.extend(dict_res_B["list_comp"])
        
        total_nb_entries_A = nb_entries_match + nb_entries_A_not_match
        total_nb_entries_B = nb_entries_match + nb_entries_B_not_match
        
        time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
        print "\n[" + str(time_process) + "] END processing results"
        
        
        perc_nb_entries_A_strict_match = None
        perc_nb_entries_B_strict_match = None
        perc_nb_entries_A_not_match = None
        perc_nb_entries_B_not_match = None
        # get percentage of number of entries
        if total_nb_entries_A != 0:
            perc_nb_entries_A_strict_match = (nb_entries_strict_match / float(total_nb_entries_A)) * 100
            perc_nb_entries_A_not_match = (nb_entries_A_not_match / float(total_nb_entries_A)) * 100
        if total_nb_entries_B != 0:
            perc_nb_entries_B_strict_match = (nb_entries_strict_match / float(total_nb_entries_B)) * 100
            perc_nb_entries_B_not_match = (nb_entries_B_not_match / float(total_nb_entries_B)) * 100
        
        perc_coverage_A_strict_match = None
        perc_coverage_B_strict_match = None
        perc_coverage_A_not_match = None
        perc_coverage_B_not_match = None
        # get percentage of coverage
        if alias.get_plugin_alias(type_query) == "loop":
            if total_time_A != 0:
                perc_coverage_A_strict_match = (time_A_scrict_match / total_time_A) * 100
                perc_coverage_A_not_match = (time_A_not_match / total_time_A) * 100
            if total_time_B != 0:
                perc_coverage_B_strict_match = (time_B_scrict_match / total_time_B) * 100
                perc_coverage_B_not_match = (time_B_not_match / total_time_B) * 100
        
        diff_results_csv_file = None
        if list_comp:
            time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
            print "\n[" + str(time_process) + "] Write the csv file.."
            
            # write csv file
            csv_file = tempfile.NamedTemporaryFile(delete=False, suffix=".csv", prefix="diff_", mode="w")
            ## provide a list to DictWriter to keep the column order
            list_column_name = list(c[cti.META_ATTRIBUTE_NAME] for c in list_comp[0])
            csv_writer = csv.DictWriter(csv_file, fieldnames=list_column_name, delimiter=";")
            
            dict_column_name = dict((c[cti.META_ATTRIBUTE_NAME], c[cti.META_ATTRIBUTE_NAME]) for c in list_comp[0])
            ## write column name on csv file
            csv_writer.writerow(dict_column_name)
            
            for comp in list_comp:
                csv_writer.writerow(dict((c[cti.META_ATTRIBUTE_NAME], c[cti.META_ATTRIBUTE_VALUE]) for c in comp))
            csv_file.close()
            
            ## import csv file into the diff entry
            diff_results_csv_file = entry.put_file_in_entry(entry_uid, csv_file.name, False)
        
        time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
        print "\n[" + str(time_process) + "] Import results into the CTI entry .."
        entry.update_entry_parameter(entry_uid, {"csv_results": {"value": diff_results_csv_file},
                                                 "perc_nb_entries_A_strict_match": {"value": perc_nb_entries_A_strict_match},
                                                 "perc_nb_entries_B_strict_match": {"value": perc_nb_entries_B_strict_match},
                                                 "perc_nb_entries_A_not_match": {"value": perc_nb_entries_A_not_match},
                                                 "perc_nb_entries_B_not_match": {"value": perc_nb_entries_B_not_match},
                                                 "perc_coverage_A_strict_match": {"value": perc_coverage_A_strict_match},
                                                 "perc_coverage_B_strict_match": {"value": perc_coverage_B_strict_match},
                                                 "perc_coverage_A_not_match": {"value": perc_coverage_A_not_match},
                                                 "perc_coverage_B_not_match": {"value": perc_coverage_B_not_match}
                                                })
        os.remove(csv_file.name)
        time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
        print "\n[" + str(time_process) + "] End of process"

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = DiffPlugin()
    exit(p.main(sys.argv))
