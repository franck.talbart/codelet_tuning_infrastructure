build_number = 1
category = 3ee01f51-b58c-4eec-b74c-c761cf2dbdf4
description = Show differences between entries.
authors = Intel Corporation, CEA, GENCI, and UVSQ
cti_dependence_build_number = 0.3.0:
