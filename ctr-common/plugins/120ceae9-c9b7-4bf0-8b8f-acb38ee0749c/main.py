#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import description
from cti_hapi.main import HapiPlugin, hapi_command

import sys

class PlatformPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Initialize information for Platform
        
        Args:
            self : class of the plugin
            params : working parameters
        """
        self.work_params = description.description_write(self.command, self.work_params)
        alias_entry = self.work_params[self.command].params["hostname"][cti.META_ATTRIBUTE_VALUE]
        if alias_entry:
            alias_entry = alias_entry.strip()
        self.default_init_command(params, alias_e = alias_entry)
        return 0

#---------------------------------------------------------------------------

if __name__ == "__main__":
    p = PlatformPlugin()
    exit(p.main(sys.argv))
