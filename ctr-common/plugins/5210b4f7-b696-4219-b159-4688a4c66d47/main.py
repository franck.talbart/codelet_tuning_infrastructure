#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import alias, util_uid, entry, database_manager, database, plugin, util
from cti_hapi.main import HapiPlugin, hapi_command

import os, zipfile, sys, datetime, StringIO

class RepositoryPlugin(HapiPlugin):
    @hapi_command("init")
    def repository_init(self, params):
        """ prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        alias_repo = params["alias_repository"]
        
        if not alias_repo:
            util.cti_plugin_print_error("The alias is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        path = os.getcwd()
        dot_ctr = cti.cti_plugin_config_get_value(cti.DOT_CTR)
        
        if not dot_ctr:
            util.cti_plugin_print_error("Invalid DOT_CTR")
            return cti.CTI_PLUGIN_ERROR_DOT_CTR
        path_ctr = os.path.join(path, dot_ctr)
        
        if os.path.isdir(path_ctr):
            util.cti_plugin_print_error("'.ctr' folder found in directory. To import an existing repository structure, please use 'cti repository import'.")
            return cti.CTI_PLUGIN_ERROR_CRITICAL_IO
        
        os.mkdir(path_ctr)
        
        if not ctr.ctr_plugin_global_index_file_is_there_this_path(path_ctr):
            uid = create_repository_entry(self, params)
            if not uid:
                util.cti_plugin_print_error("Error while creating the repository entry.")
                return cti.CTI_PLUGIN_ERROR_CANT_CREATE_DATA_ENTRY
            if ctr.ctr_plugin_global_index_file_write(path_ctr, uid) != 0:
                util.cti_plugin_print_error("Error while writing in the global index file.")
                return cti.CTI_PLUGIN_ERROR_CRITICAL_IO
        else:
            uid = ctr.ctr_plugin_global_index_file_get_uid_by_ctr(path_ctr)
            
        os.mkdir(os.path.join(path_ctr, "plugins"))
        os.mkdir(os.path.join(path_ctr, "data"))
        
        if alias_repo:
            if alias.set_repository_alias(uid, alias_repo) == 0:
               util.cti_plugin_print_warning("Can't set the alias (already used): %s" % (alias_repo))
                
        print "The local repository is created."
        print "REPOSITORY_CTI_UID=%s" % (uid)
        return 0
    
    #---------------------------------------------------------------------------
    
    @hapi_command("archive")
    def archive_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        repository_type = params["repository"]
        output_filename = os.path.abspath(params["output_filename"])
        
        if repository_type == cti.COMMON_REPOSITORY:
            path = ctr.ctr_plugin_get_common_dir()
        elif repository_type == cti.TEMP_REPOSITORY:
            path = ctr.ctr_plugin_get_temp_dir()
        elif util_uid.is_valid_uid(repository_type):
            path = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(util_uid.CTI_UID(repository_type, cti.CTR_ENTRY_REPOSITORY))
        elif cti.cti_plugin_alias_repository_get_value(repository_type):
            rep = cti.cti_plugin_alias_repository_get_value(repository_type)
            path = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(rep)
        else:
            util.cti_plugin_print_error("Unknown repository")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if os.path.isdir(path):
            archive = StringIO.StringIO()
            zf = zipfile.ZipFile(archive,'w', zipfile.ZIP_DEFLATED)
            try:
                files_l = list_files(path)
                for name in files_l:
                    zf.write(name)
            
            finally:
                zf.close()
                archive.seek(0)
                
                filout = open(output_filename, 'w+')
                filout.write(archive.read())
                filout.close()
                archive.close()
                
            print "Done!"
    
    #---------------------------------------------------------------------------
    
    @hapi_command("import")
    def import_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        alias_repo = params["alias_repository"]
        archive = params["archive"]
        max_threads = params["max_threads"]
        
        if not alias_repo:
            util.cti_plugin_print_error("The alias is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if type(max_threads) != int or max_threads < 1:
            util.cti_plugin_print_error("The argument 'max_thread' must be a positive integer.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        dot_ctr = cti.cti_plugin_config_get_value(cti.DOT_CTR)
        if not dot_ctr:
            util.cti_plugin_print_error("Can't get DOT_CTR config value.")
            return cti.CTI_PLUGIN_ERROR_DOT_CTR
        if archive and os.path.isfile(archive):
            if os.path.isdir(dot_ctr):
                util.cti_plugin_print_error("A repository already exists here.")
                return cti.CTI_ERROR_PATH_EXISTS
            os.mkdir(dot_ctr)
            dezip(archive, dot_ctr)
        
        cur_dir = os.getcwd()
        path_to_rep = os.path.join(cur_dir, dot_ctr)
        
        if not os.path.isdir(path_to_rep):
            util.cti_plugin_print_error("Nothing to import.")
            return cti.CTI_PLUGIN_ERROR_IMPORT
        else:
            check_exist = ctr.ctr_plugin_global_index_file_is_there_this_path(path_to_rep)
            if not check_exist:
                # check plugins and data
                dir_plugins = os.path.join(path_to_rep, cti.cti_plugin_config_get_value(cti.CTR_PLUGIN_DIR))
                if not os.path.isdir(dir_plugins):
                    os.mkdir(dir_plugins)
                
                if ctr.ctr_plugin_check_repository(path_to_rep) == 0:
                    uid = create_repository_entry(self, params)
                    if not uid:
                        util.cti_plugin_print_error("Error while creating the repository entry.")
                        return cti.CTI_PLUGIN_ERROR_CANT_CREATE_DATA_ENTRY
                    
                    if ctr.ctr_plugin_global_index_file_write(path_to_rep, uid) != 0:
                        util.cti_plugin_print_error("Error while writing in the global index file.")
                        return cti.CTI_PLUGIN_ERROR_IO
                else:
                    util.cti_plugin_print_error("Current directory doesn't contain correct repository.")
                    return cti.CTI_PLUGIN_ERROR_IMPORT
            else:
                util.cti_plugin_print_error("The repository is already imported.")
                return cti.CTI_PLUGIN_ERROR_IMPORT
        
        print "Repository successfully imported!\nIndexing...\nREPOSITORY_CTI_UID=%s\n" % (uid)
        
        db = database.Database()
        plugin_uids = get_local_plugin_uids(uid)
        if len(plugin_uids) > 0:
            lp = database_manager.list_plugins()
            for p in plugin_uids:
                database_manager.create_plugin_schema(db, str(p), lp)
        
        data_uids = get_local_data_uids(uid)
        database_manager.index_group(data_uids, db, max_threads)
            
        if alias_repo:
            if alias.set_repository_alias(uid, alias_repo) == 0:
                util.cti_plugin_print_warning("Can't set the alias (already used): %s" % (alias_repo))
                
        print "Done!"
    
#---------------------------------------------------------------------------

def list_files(path):
    files_l = []
    for i in os.listdir(path):
        new_path = os.path.join(path, i)
        if os.path.isdir(new_path):
            files_l.extend(list_files(new_path))
        else:
            files_l.append(new_path)
    return files_l

#---------------------------------------------------------------------------

def create_repository_entry(self, params):
    status = params["status"]
    now = datetime.datetime.now()
    date = now.strftime('%Y/%m/%d %H:%M:%S')
    
    self.output_params["init"].params["last_use"][cti.META_ATTRIBUTE_VALUE] = date
    self.output_params["init"].params["status"][cti.META_ATTRIBUTE_VALUE] = status
    
    rep = self.work_params["init"].attributes[cti.META_ATTRIBUTE_REP]
    if cti.META_ATTRIBUTE_REP_PRODUCE in params:
        rep = params[cti.META_ATTRIBUTE_REP_PRODUCE]
    
    data_entry = entry.create_data_entry(self.plugin_uid,
                                          rep,
                                          self.username)
    
    if data_entry is None:
        util.cti_plugin_print_error("Can't create the entry.")
        return cti.CTI_PLUGIN_ERROR_IO
    
    self.output_params.record_output("init", data_entry.path)
    self.work_params.record_input("init", data_entry.path)
    
    self.output_params.update_references("init", data_entry.uid)
    plugin.plugin_exit(data_entry.entry, alias_e="repo_"+params["alias_repository"])
    return data_entry.uid

#---------------------------------------------------------------------------

def get_local_data_uids(rep):
    """
    returns a set of all the local data uids.
    """
    
    path = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(rep)
    local_ctr = os.path.join(path, cti.cti_plugin_config_get_value(cti.CTR_DATA_DIR))
    result = []
    if os.path.exists(local_ctr):
        for uid in os.listdir(local_ctr):
            try:
                u = util_uid.CTI_UID(uid) 
                if u is not None:
                    result.append(u)
            except:
                continue
                # ignore non CTI_UID
    return result

#---------------------------------------------------------------------------

def get_local_plugin_uids(rep):
    """
    returns a set of all the local plugin uids.
    """
    
    path = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(rep)
    local_ctr = os.path.join(path, cti.cti_plugin_config_get_value(cti.CTR_PLUGIN_DIR))
    result = []
    if os.path.exists(local_ctr):
        for uid in os.listdir(local_ctr):
            try:
                u = util_uid.CTI_UID(uid, cti.CTR_ENTRY_PLUGIN)
                if u is not None:
                    result.append(u)
            except:
                continue
                # ignore non CTI_UID
    return result

#---------------------------------------------------------------------------

def dezip(filezip, pathdst = ''):
    if pathdst == '':
        pathdst = os.getcwd()
    zfile = zipfile.ZipFile(filezip, 'r')
    for i in zfile.namelist():
        if os.path.isdir(i):
            try: 
                os.makedirs(pathdst + os.sep + i)
            except: 
                pass
        else:
            try: 
                os.makedirs(pathdst + os.sep + os.path.dirname(i))
            except: 
                pass
            data = zfile.read(i)
            fp = open(pathdst + os.sep + i, "wb")
            fp.write(data)
            fp.close()
    zfile.close()

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = RepositoryPlugin()
    exit(p.main(sys.argv))
