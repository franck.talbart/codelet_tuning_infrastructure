== Description ==

A repository manager that allows the user to manage a repository.

== Usage examples ==

Create a repository:
<pre>
$ cti repository init my_rep
</pre>


Archive a repository:
<pre>
$ cti repository archive my_rep archive_filename
</pre>

Imports local repository into CTI.
'''NOTE:''' User can perform any actions related to the repository only after this repository being added to the working list, i.e. imported or created.
<pre>
$ cti repository init my_rep
$ rm -rf .ctr
$ mkdir .ctr
$ cti repository import toto
The repository is already imported.
</pre>
