build_number = 1.0.0
category = 31a52e77-1595-461a-80ef-36bfeb282349
description = This plugin is used to characterize assembly loops using MAQAO-DECAN.
authors = Intel Corporation, CEA, GENCI, and UVSQ
cti_dependence_build_number = 0.3.0:
