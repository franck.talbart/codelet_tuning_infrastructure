#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2013 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import os, tarfile

def file_replace(input_path, tuples, output_path=None):
    """ Replaces patterns in a file.

    Args:
        input_path: file path
        tuples: a list of tuples representing the patterns to replace formatted as follows: [(pattern, replacement), ...]
        output_path: resulting file path
    Returns:
        Nothing
    """

    if not output_path:
        output_path=input_path
    
    to_change = open(input_path, 'r')
    lines = []
    for line in to_change:
        for (expr, repl) in tuples:
            line = line.replace(expr, repl)
        lines.append(line)
    to_change.close()
    
    #Writing the result
    to_change = open(output_path, 'w')
    to_change.writelines(lines)
    to_change.close()

#-------------------------------------------------------------------------------

def untar(file_path):
    """ Extracts a tar.gz archive.

    Args:
        file_path: path of the archive

    Returns:
        Nothing
    """

    if file_path.endswith("tar.gz"):
        print("Extracting {0}.".format(file_path))
        tar = tarfile.open(file_path)
        tar.extractall()
        tar.close()
    else:
        print("{0} is not a tar.gz file. Extraction aborted.".format(file_path))
