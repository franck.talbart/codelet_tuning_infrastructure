#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Romain Anty

import cti
import ctr

from cti_hapi import entry, util, util_uid, plugin, alias

import getopt, sys, os, time, tempfile, glob, datetime, csv

def import_results(dict_opt):
    """ Imports the results of a MAQAO-DECAN experiment into the CTI database.
        Creates a maqao_decan_group entry and its related maqao_decan_results entries.

    Args:
        dict_opt: options dictionary containing all necessary arguments for this import script

    Returns:
        Nothing
    """

    #Constants
    RESULTS_DIR = dict_opt["result_dir"]

    #Retrieving the repository of the entry.
    repository = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, util_uid.CTI_UID(dict_opt["entry"]))
    #If the repository is local, we need its UID.
    if repository == cti.CTR_REP_LOCAL:
        repository_path = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(util_uid.CTI_UID(dict_opt["entry"]), cti.CTR_DATA_DIR)
        repository = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(repository_path))

    #List of log files to put in the "logs" field of the maqao_decan entry.
    log_files_to_import = []

    #Importing log files into the maqao_decan entry.
    try:
        imported_execution_log_file = entry.put_file_in_entry(dict_opt["entry"], dict_opt["execution_log"], False)
        log_files_to_import.append(imported_execution_log_file)
    except IOError:
        util.cti_plugin_print_error("No such file {0}\n".format(dict_opt["execution_log"]))
    try:
        imported_daemon_log_file = entry.put_file_in_entry(dict_opt["entry"], dict_opt["daemon_log"], False)
        log_files_to_import.append(imported_daemon_log_file)
    except IOError:
        util.cti_plugin_print_error("No such file {0}\n".format(dict_opt["daemon_log"]))

    #Creating a maqao_decan_group entry.
    print "Generating maqao_decan_group"

    summary_file_path = os.path.join(RESULTS_DIR, "summary.csv")

    json_file = \
    {
        "init":
        {
            "attributes": {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
            "params":
            [
                {cti.META_ATTRIBUTE_NAME : "maqao_decan", cti.META_ATTRIBUTE_VALUE : dict_opt["entry"]},
                {cti.META_ATTRIBUTE_NAME : "summary", cti.META_ATTRIBUTE_VALUE : summary_file_path},
                {cti.META_ATTRIBUTE_NAME : "summary", cti.META_ATTRIBUTE_VALUE : None},
                {cti.META_ATTRIBUTE_NAME : "maqao_decan_results", cti.META_ATTRIBUTE_VALUE : []}
            ]
        }
    }
    
    try:
        maqao_decan_execution_result = plugin.execute_plugin_by_file(
            dict_opt["maqao_decan_group_uid"], json_file, dict_opt["username"], dict_opt["password"])
    #We can not be more specific since the above method raises an "Exception" if something goes wrong.
    except Exception:
        print sys.exc_info()[1]
        exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)

    maqao_decan_group_uid = plugin.get_output_data_uid(maqao_decan_execution_result)

    #Creating alias for the maqao_decan_group entry.
    (_, maqao_decan_output_data) = entry.load_data(dict_opt["entry"])
    binary_uid = maqao_decan_output_data["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
    binary_name = alias.get_data_alias(binary_uid)
    now = datetime.datetime.now()
    date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
    maqao_decan_group_alias = "maqao_decan_group_{0}".format(date)
    
    if binary_name:
        maqao_decan_group_alias = "{0}_maqao_decan_group_{1}".format(binary_name, date)
    
    if alias.set_data_alias(maqao_decan_group_uid, maqao_decan_group_alias) == 0:
        util.cti_plugin_print_error("Cannot set the alias {0} (already used?).\n".format(maqao_decan_group_alias))

    #Creating a maqao_decan_results entry for each loop.
    print "Generating maqao_decan_results"
    
    (_, maqao_decan_output_data) = entry.load_data(dict_opt["entry"])
    loops = maqao_decan_output_data["init"].params["loops"][cti.META_ATTRIBUTE_VALUE]
    frequencies = maqao_decan_output_data["init"].params["frequencies"][cti.META_ATTRIBUTE_VALUE]
    probes = maqao_decan_output_data["init"].params["probes"][cti.META_ATTRIBUTE_VALUE]
    hardware_counters = maqao_decan_output_data["init"].params["hardware_counters"][cti.META_ATTRIBUTE_VALUE]

    for loop in loops:
        (_, loop_output_data) = entry.load_data(loop)
        loop_id = loop_output_data["init"].params["loop_id"][cti.META_ATTRIBUTE_VALUE]
        
        #Retrieving results files from the results directory.

        #Report files.
        cqa_report_path =   os.path.join(RESULTS_DIR, "cqa_report_{0}.csv".format(loop_id))
        report_path     =   os.path.join(RESULTS_DIR, "report_{0}.csv".format(loop_id))

        #Metric files are formatted as follows: <metric_name>_<loop_id>_freq_<frequency_value>.csv
        #Retrieving all metric files related to the current loop.
        metric_files_pathes = [metric_file for metric_file in glob.glob("{0}/*{1}_freq*.csv".format(RESULTS_DIR, loop_id)) if os.path.isfile(metric_file)]

        result_files = [cqa_report_path, report_path] + metric_files_pathes

        json_file = \
        {
            "init":
            {
                "attributes": {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
                "params":
                [
                    {cti.META_ATTRIBUTE_NAME : "maqao_decan_group", cti.META_ATTRIBUTE_VALUE : maqao_decan_group_uid},
                    {cti.META_ATTRIBUTE_NAME : "loop", cti.META_ATTRIBUTE_VALUE : loop},
                    {cti.META_ATTRIBUTE_NAME : "results", cti.META_ATTRIBUTE_VALUE : result_files}
                ]
            }
        }

        try:
            maqao_decan_execution_result = plugin.execute_plugin_by_file(
                dict_opt["maqao_decan_results_uid"], json_file, dict_opt["username"], dict_opt["password"])
        #We can not be more specific since the above method raises an "Exception" if something goes wrong.
        except Exception:
            print sys.exc_info()[1]
            exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)

        maqao_decan_results_uid = plugin.get_output_data_uid(maqao_decan_execution_result)

        #Creating alias for the maqao_decan_results entry.
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        maqao_decan_results_alias = "maqao_decan_results_{0}".format(date)
        
        if binary_name:
            maqao_decan_results_alias = "{0}_maqao_decan_results_{1}".format(binary_name, date)
        
        if alias.set_data_alias(maqao_decan_results_uid, maqao_decan_results_alias) == 0:
            util.icti_plugin_print_error("Cannot set the alias {0} (already used?).\n".format(maqao_decan_results_alias))

    #Updating the maqao_decan entry.
    entry.update_entry_parameter(
    dict_opt["entry"],
    {
        "logs" : {"value" : log_files_to_import, "append" : True},
        "maqao_decan_group" : {"value" : maqao_decan_group_uid}
    })

    print("Finished importation.")
