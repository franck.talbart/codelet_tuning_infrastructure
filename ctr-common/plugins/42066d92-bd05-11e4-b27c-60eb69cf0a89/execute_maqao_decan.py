#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Romain Anty

import sys, os, tarfile, stat, subprocess

#Constants
SCRIPT_PARAMS = "script_params"
WORK_DIR = os.getcwd()
MAQAO_BIN = os.path.join(WORK_DIR, "TOOL", "maqao", "maqao")

#-------------------------------------------------------------------------------

def untar(file_path):
    """ Extracts a tar.gz archive.
    
        Args:
            file_path: path of the archive
            
        Returns:
            Nothing
    """
        
    if file_path.endswith("tar.gz"):
        print("Extracting {0}.".format(file_path))
        tar = tarfile.open(file_path)
        tar.extractall()
        tar.close()
    else:
        print("{0} is not a tar.gz file. Extraction aborted.".format(file_path))

#-------------------------------------------------------------------------------

#Retrieving parameters from the parameters file.
param_file = open(SCRIPT_PARAMS, "r")

compiled_application_path   = param_file.readline().rstrip('\n')
special_permissions         = param_file.readline().rstrip('\n')

#Converting special_permissions parameters from string to boolean.
if special_permissions == "True":
    special_permissions = True
else:
    special_permissions = False

#Getting and extracting the experiment directory.
experiment_directory_name = os.listdir(os.path.join(WORK_DIR, "APP"))[0]

untar(os.path.join(WORK_DIR, "APP", experiment_directory_name))
experiment_directory_name = experiment_directory_name.replace(".tar.gz", "")

#Getting and extracting the application environment, if any.
if compiled_application_path:
    untar(os.path.join(WORK_DIR, "OTHER", compiled_application_path))

print("\nRunning MAQAO-DECAN.\n")

maqao_decan_run_cmd = "{0} decan --run --experiment-path={1}".format(MAQAO_BIN, os.path.join(WORK_DIR, experiment_directory_name))

maqao_decan_results_cmd = "{0} decan --format=cti --experiment-path={1}".format(MAQAO_BIN, os.path.join(WORK_DIR, experiment_directory_name))

#Generating the command file that will be used to run the program.
prefix_script_path = os.path.join(WORK_DIR, "SCRIPTS")
file_cmd_path = os.path.join(WORK_DIR, "file_cmd.sh")

file_cmd = open(file_cmd_path, "w")
file_cmd.write("#!/bin/bash\n")
prefix_file = os.path.join(prefix_script_path, "prefix.sh")

if os.path.isfile(prefix_file):
    file_cmd.write("source {0}\n".format(prefix_file))

#Sourcing the MAQAO-DECAN required librairies in the command file.
#"sudo" does not keep environment variables, therefore we can not use the "env" parameter of subprocess.Popen().
file_cmd.write("export LD_LIBRARY_PATH=\"$LD_LIBRARY_PATH:{0}\"\n".format(os.path.join(WORK_DIR, "OTHER")))
file_cmd.write(maqao_decan_run_cmd + '\n')
file_cmd.write(maqao_decan_results_cmd + '\n')
file_cmd.close()
os.chmod(file_cmd_path, stat.S_IXUSR | stat.S_IRUSR)

#Running MAQAO-DECAN.
print(maqao_decan_run_cmd)
print(maqao_decan_results_cmd)
sys.stdout.flush()

std_out = None
std_err = None

if special_permissions:
    (std_out, std_err) = subprocess.Popen(
        ["sudo", file_cmd_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
else:
    (std_out, std_err) = subprocess.Popen([file_cmd_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

os.remove(file_cmd_path)

print("MAQAO-DECAN output:\n{0}\n{1}\n".format(std_out, std_err))
print("End of MAQAO-DECAN execution.")
