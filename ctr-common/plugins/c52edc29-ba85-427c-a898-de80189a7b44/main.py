#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import plugin, util_uid, util
from cti_hapi.main import HapiPlugin, hapi_command

import os, sys, re, shutil, subprocess, json, tempfile

# Constants
rm_plugin_uid = util_uid.CTI_UID("8b9f58b8-9d01-4af4-8884-42443337da67", cti.CTR_ENTRY_PLUGIN)
repository_plugin_uid = util_uid.CTI_UID("5210b4f7-b696-4219-b159-4688a4c66d47", cti.CTR_ENTRY_PLUGIN)

Test_directory = "tests"
Test_command = "run_test_"
Expected_input = "expected_input_"
Expected_output = "expected_output_"
OKC = '\033[93m'
KOC = '\033[91m'
WARNINGC = '\033[92m'
ENDC = '\033[0m'

class TestSandbox():
    """
    This class provides a clean test environment with its own local ctr.
    """
    def __init__(self, test_directory, username, password):
        self.test_directory = test_directory
        self.test_ctr = os.path.join(test_directory, cti.cti_plugin_config_get_value(cti.DOT_CTR))
        self.username = username
        self.password = password
        if not os.path.exists(self.test_ctr):
            self.test_ctr = None
    
    #---------------------------------------------------------------------------
    
    def __enter__(self):
        """
        Initialises a local ctr for testing in a temp directory.
        """
        
        self.devnull = open(os.devnull, 'w')
        self.temp_dir = tempfile.mkdtemp()
        # copy the test files to the sandbox
        sandbox = os.path.join(self.temp_dir, "test")
        shutil.copytree(self.test_directory, sandbox)
        os.chdir(sandbox)
        out = ""
        if self.test_ctr:
            # the plugin provides a test ctr let's import it.
            # json file
            json_i = \
            {
                "import": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "import"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "alias_repository",cti.META_ATTRIBUTE_VALUE : "self_test"}
                    ]
                }
            }
            
            # import a ctr repo for tests
            try:
                out = plugin.execute_plugin_by_file(repository_plugin_uid, 
                                                      json_i,
                                                      self.username,
                                                      self.password)
            except :
                print sys.exc_info()[1]
                return -1
            self.repository = plugin.get_output_data_uid(out, "REPOSITORY_CTI_UID=")
        else:
            # json file
            json_i = \
            {
                "init": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "init"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "alias_repository",cti.META_ATTRIBUTE_VALUE : "self_test"}
                    ]
                }
            }
            
            # initialize an empty ctr repo for tests
            
            try:
                out = plugin.execute_plugin_by_file(repository_plugin_uid, 
                                                      json_i,
                                                      self.username,
                                                      self.password)
            except :
                print sys.exc_info()[1]
                return -1
            self.repository = plugin.get_output_data_uid(out, "REPOSITORY_CTI_UID=")
        return (sandbox, self.repository)
    
    #---------------------------------------------------------------------------
    
    def __exit__(self, exc_type, exc_value, traceback):
        """
        Unlinks the ctr repository and the temp directory.
        """
            
        json_file = \
        {
            "repository": 
            {
                "attributes": {cti.META_ATTRIBUTE_NAME : "repository"},
                "params": 
                [
                    {cti.META_ATTRIBUTE_NAME : "force",cti.META_ATTRIBUTE_VALUE : True}
                ]
            }
        }
        
        try:
            plugin.execute_plugin_by_file(rm_plugin_uid,
                                          json_file,
                                          self.username,
                                          self.password)
        except:
            print sys.exc_info()[1]
            return -1
            
        shutil.rmtree(self.temp_dir)
        self.devnull.close()

#---------------------------------------------------------------------------

class Self_testPlugin(HapiPlugin):
    def __init__(self):
        self.ok_tests = 0
        self.ko_tests = 0
    
    #---------------------------------------------------------------------------
    
    def ok(self, uid, tname):
        """
        report a successful test.

        uid -- uid of the plugin
        tname -- name of the test
        """
        self.ok_tests += 1
        print(OKC + "OK\t {tname}".format(tname=tname) + ENDC)
    
    #---------------------------------------------------------------------------
    
    def ko(self, uid, tname):
        """
        report a failing test.

        uid -- uid of the plugin
        tname -- name of the test
        """
        self.ko_tests += 1
        print(KOC + "FAILED\t {tname}".format(tname=tname) + ENDC)
    
    #---------------------------------------------------------------------------
    
    def report_result(self):
        """
        Reports the result of the tests.
        Returns 0 if everything was ok, and 1 if something went wrong.
        """
        
        if self.ko_tests == 0:
            print (WARNINGC
               + "Test report [{ok}/{tot}] OK".format(ok=self.ok_tests,
                                                    tot=self.ok_tests
                                                    + self.ko_tests)
               + ENDC)
            return 0
        else:
            print (KOC
               + "Test report [{ok}/{tot}] FAILED".format(ok=self.ok_tests,
                                                    tot=self.ok_tests
                                                    + self.ko_tests)
               + ENDC)
            return 1
    
    #---------------------------------------------------------------------------
    
    def check_expected(self, uid, exp_inp, exp_out, tname):
        """
        checks the behaviour of a plugin with a pair of
        expected input, output files
        
        uid -- uid of the plugin
        exp_inp --  expected input file
        exp_out -- expected output file
        tname -- name of the test
        """
        
        expected = open(exp_out).read()
        output = ""
        try:
            file_ = open(exp_inp)
            jr = json.load(file_)
            output_p = "" 
            try:
                output_p = plugin.execute_plugin_by_file(uid,
                                                      jr,
                                                      self.username,
                                                      self.password)
            except:
                print sys.exc_info()[1]
                raise Exception("Error while running the plugin.")
            # find uid of output entry
            output_uid = str(plugin.get_output_data_uid(output_p))
            if not output_uid:
                output = ""
                raise Exception("Empty output")
            else:
                output_uid = util_uid.CTI_UID(output_uid)
                output_path = ctr.ctr_plugin_get_path_by_uid(
                                cti.CTR_ENTRY_DATA,
                                output_uid
                              )
                output_name = cti.cti_plugin_config_get_value(
                                cti.PLUGIN_OUTPUT_FILENAME
                              )
                output = open(os.path.join(output_path, output_name)).read()
            if output.strip() == expected.strip():
                self.ok(uid, tname)
            else:
                raise Exception("Different output")
        except Exception,e :
            self.ko(uid, tname)
            
            def divider():
                print "=" * 60
            
            print("EXPECTED")
            divider()
            print expected
            divider()
            print("GOT")
            print output
            divider()
            raise e
    
    #---------------------------------------------------------------------------
    
    def run_test_command(self, uid, command_path, sandbox, tname):
        """
        runs the user defined test command for a plugin
        
        uid -- uid of the plugin
        command_path -- command to run
        sandbox -- test directory
        tname -- name of the test
        """
        test_output = tempfile.NamedTemporaryFile()
        sys.stdout.flush()
        try:
            p = subprocess.Popen(command_path, cwd=sandbox, stdout=test_output)
        except:
            util.cti_plugin_print_error("Unexpected error:" + str(sys.exc_info()[0]))
            util.cti_plugin_print_error("command: " + str(command_path))
            raise
        sts = p.wait()
        
        p_output = ''
        read_test_output = open(test_output.name, 'r')
        for line in read_test_output:
            p_output += line
        read_test_output.close()
        test_output.close()
        
        if sts == 0:
            self.ok(uid, tname)
        else:
            self.ko(uid, tname)
            print p_output
    
    #---------------------------------------------------------------------------
    
    def run_test(self, uid):
        """
        run (when available) the test of plugin 'uid'

        uid -- uid of the plugin
        """
        self.username = "admin"
        self.password = "21232f297a57a5a743894a0e4a801fc3"
        
        path = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN, uid)
        
        # test if a tests directory exists
        tests_path = os.path.join(path, Test_directory)
        if not os.path.exists(tests_path):
            return 0 
        
        # Print test header
        print (WARNINGC
               + "="*13
               + "     TEST ({0})      ".format(uid)
               + "="*13
               + ENDC)
        
        # find tests
        with TestSandbox(tests_path, self.username, self.password) as (sandbox, repository_uid):
            #ordering tests by name
            test_list = os.listdir(sandbox)
            test_list.sort()
            for fname in test_list:
                # check if expected input and expected output files are
                # present
                m = re.match(Expected_input + "(.*)_([a-zA-Z0-9]*)\.json", fname)
                if m:
                    com = m.group(1)
                    name = m.group(2)
                    suffix = "{0}_{1}.json".format(com, name)
                    expected_inp = os.path.join(sandbox, fname)
                    expected_out = os.path.join(sandbox, Expected_output + suffix)
                    self.check_expected(uid, expected_inp, expected_out, name)
                    continue
                
                # check if runners are present
                m = re.match(Test_command + "(.*)", fname)
                if m:
                    com = fname
                    name = m.group(1)
                    command_path = os.path.join(sandbox, com)
                    self.run_test_command(uid, [command_path, str(repository_uid)], sandbox, name)
        # 2 empty line after test
        print "\n"
    
    #---------------------------------------------------------------------------
    
    def get_plugins_uid(self, list_plugin):
        """
        returns a list with all the plugins uids.
        """
        
        json_file = \
        {
            "plugins": 
            {
                "attributes": {cti.META_ATTRIBUTE_NAME : "plugins"},
                "params": 
                [
                    {cti.META_ATTRIBUTE_NAME : "repository_type",cti.META_ATTRIBUTE_VALUE : cti.ALL_REPOSITORY},
                    {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : "all"},
                    {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"}
                ]
            }
        }
        
        output = ""
        try:
            output = plugin.execute_plugin_by_file(list_plugin,
                                          json_file,
                                          self.username,
                                          self.password)
        except:
            print sys.exc_info()[1]
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
        
        out_js = json.loads(output)
        output = []
        for key in out_js.keys():
            output.append(str(key))
        
        # separate the lines
        uids = set()
        for l in output:
            try:
                uids.add(util_uid.CTI_UID(l.strip(), cti.CTR_ENTRY_PLUGIN))
            except KeyError:
                pass
                # ignore lines that do not start with a CTI_UID
        return uids
    
    #---------------------------------------------------------------------------
    
    def check_colors(self, params):
        global OKC,KOC,WARNINGC,ENDC
        if params["color"] != "yes":
            OKC = KOC = WARNINGC = ENDC = ''
    
    #---------------------------------------------------------------------------
    
    @hapi_command("all")
    def command_all(self, params):
        """ finds all the plugins with tests, and run them.
        Args:
            self: class of the plugin
            params: working parameters
        """
        self.check_colors(params)
        plugins = self.get_plugins_uid(params["list_plugin"])
        total = len(plugins)
        counter = 1.
        for uid in plugins:
            print("* {0} / {1} *".format(int(counter), total))
            self.run_test(uid)
            counter += 1
        
        return self.report_result()
    
    #---------------------------------------------------------------------------
    
    @hapi_command("single")
    def command_single(self, params):
        """ runs all the tests of a single plugin.
        Args:
            self: class of the plugin
            params: working parameters
        """
        self.check_colors(params)
        uid = params["plugin"]
        self.run_test(uid)
        
        return self.report_result()
    
    #---------------------------------------------------------------------------
    
    @hapi_command("list")
    def command_list(self, params):
        """ returns a list of uids of all the plugins with tests
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        for uid in self.get_plugins_uid(params["list_plugin"]):
            path = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN, uid)
            
            # test if a tests directory exists
            tests_path = os.path.join(path, Test_directory)
            if os.path.exists(tests_path):
                print uid
    
    #---------------------------------------------------------------------------
    
    # Bypass the authentification
    def check_passwd(self):
        return True

#---------------------------------------------------------------------------

# main
if __name__ == "__main__":
    plug = Self_testPlugin()
    exit(plug.main(sys.argv))
