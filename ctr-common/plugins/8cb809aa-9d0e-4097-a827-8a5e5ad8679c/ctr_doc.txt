== Description ==

Move a data or a plugin to a given repository. Data and plugins can be identified by UID or alias.

== Usage examples ==

Move a plugin to the common repository:

<pre>
$ cti move common compiler
164c2e8f-7460-4049-8ded-1f987750ab8c: Done!
</pre>

It is the same process with data.

We can use alias.
Move the "compiler" plugin to a local repository (.ctr), with UID 9949083146017345127:

<pre>
$ cti move local d7f8328f-47ad-43c2-a97c-0aa377a83c43 compiler
164c2e8f-7460-4049-8ded-1f987750ab8c: Done!
</pre>
