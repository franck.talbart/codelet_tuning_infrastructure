#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import cti_json, entry, util_uid, util
from cti_hapi.main import HapiPlugin, hapi_command

import shutil, os, sys

class MovePlugin(HapiPlugin):
    @hapi_command("local")
    def local_cmd(self, params):
        """ move the data/plugin to a local repository
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        repository = params["repository"]
        if not repository:
            util.cti_plugin_print_error("Local repository not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        path = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(repository)
        res = self.move_general(path,
                                params)
        
        tmp_uid = util_uid.CTI_UID(params["entry"])
        if not tmp_uid:
            tmp_uid = util_uid.CTI_UID(params["entry"], cti.CTR_ENTRY_PLUGIN)
            
        if res == 0:
            entry.update_entry_parameter(
                                      tmp_uid,
                                      {"repository":
                                            {"value": cti.LOCAL_REPOSITORY},
                                       "path_repository":
                                            {"value": ctr.ctr_plugin_global_index_file_get_ctr_by_uid(repository)}
                                      }
                                   )
    
    #---------------------------------------------------------------------------
    
    @hapi_command("temp")
    def temp_cmd(self, params):
        """ move the data/plugin to the temp repository
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        res = self.move_general(ctr.ctr_plugin_get_temp_dir(), params)
        if res == 0:
            tmp_uid = util_uid.CTI_UID(params["entry"])
            if not tmp_uid:
                tmp_uid = util_uid.CTI_UID(params["entry"], cti.CTR_ENTRY_PLUGIN)
            entry.update_entry_parameter(
                                          tmp_uid,
                                          {"repository":
                                                {"value": cti.TEMP_REPOSITORY},
                                                "path_repository": {"value": ctr.ctr_plugin_get_temp_dir()}
                                          }
                                        )
    
    #---------------------------------------------------------------------------
    
    @hapi_command("common")
    def common_cmd(self, params):
        """ move the data/plugin to the common repository
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        res = self.move_general(ctr.ctr_plugin_get_common_dir(), params)
        if res == 0:
            tmp_uid = util_uid.CTI_UID(params["entry"])
            if not tmp_uid:
                tmp_uid = util_uid.CTI_UID(params["entry"], cti.CTR_ENTRY_PLUGIN)
            entry.update_entry_parameter(
                                          tmp_uid,
                                          {"repository":
                                                {"value": cti.COMMON_REPOSITORY},
                                                "path_repository":
                                                {"value": ctr.ctr_plugin_get_common_dir()}
                                          }
                                        )
    
    #---------------------------------------------------------------------------
    
    def move_general(self, repository_dir, params):
        """ move a data or a plugin
        
        Args:
            repository_dir: the path to the repository destination
            params: the parameters
        
        Returns:
          None
        """
        uid = util_uid.CTI_UID(params["entry"])
        if not uid:
            uid = util_uid.CTI_UID(params["entry"], cti.CTR_ENTRY_PLUGIN)
        if not uid:
            util.cti_plugin_print_error("Data or plugin not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        format_o = params["format"]
        cti_pair = ctr.ctr_plugin_get_path_and_type_by_uid(uid)
        
        if not cti_pair or not cti_pair.value:
            util.cti_plugin_print_error("Data or plugin not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        entry_type = int(cti_pair)
        entry_path = cti_pair.key
        
        if entry_type == cti.CTR_ENTRY_DATA: # data
            destination = os.path.join(repository_dir, cti.cti_plugin_config_get_value(cti.CTR_DATA_DIR))
            type_link = cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID
        elif entry_type == cti.CTR_ENTRY_PLUGIN: # plugin
            destination = os.path.join(repository_dir, cti.cti_plugin_config_get_value(cti.CTR_PLUGIN_DIR))
            type_link = cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID
        else:
            util.cti_plugin_print_error("Unsupported entry type.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        full_destination = os.path.join(destination, str(uid))
        
        json_dict = {'type': type_link, 'skipped': [], 'moved': []}
            
        if os.path.isdir(full_destination) == True:
                
            if format_o == "json":
                json_dict['skipped'].append(uid)
                    
            elif format_o == "txt":
                print "UID=" + str(uid) + "- Skipped (This data/plugin already exists in this repository)."
                    
            else:
                util.cti_plugin_print_error("Unknown format")
                return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
            
        else:
            shutil.move(entry_path, full_destination)
                    
            if format_o == "json":
                json_dict['moved'].append(uid)
                    
            elif format_o == "txt":
                print "UID=" + str(uid) + " - Moved"
                    
            else:
                util.cti_plugin_print_error("Unknown format")
                return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        
        if format_o == 'json':
            print cti_json.cti_json_encode(json_dict)
        elif format_o == 'txt':
            print "Entry moved to the repository."
            
        return 0

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = MovePlugin()
    exit(p.main(sys.argv))
