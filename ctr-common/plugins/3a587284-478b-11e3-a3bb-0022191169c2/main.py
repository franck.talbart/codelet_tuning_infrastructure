#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import description, entry, alias, plugin, submitter, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, datetime, tempfile, glob, shutil

import import_profile_results

class IccProfilerPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ prepare parameters
        Args:
            self: class of the plugin
            params: working parameters
        """
        self.work_params = description.description_write(self.command, self.work_params)
        binary = self.work_params[self.command].params["binary"][cti.META_ATTRIBUTE_VALUE]
        mode = self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE]
        platform = self.work_params[self.command].params["platform"][cti.META_ATTRIBUTE_VALUE]
        auto_run = self.work_params[self.command].params["auto_run"][cti.META_ATTRIBUTE_VALUE]
        format_o = self.work_params[self.command].params["format"][cti.META_ATTRIBUTE_VALUE]
        
        # check parameters
        if not binary:
            util.cti_plugin_print_error("The binary entry parameter is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if not mode:
            mode = "local"
            self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE] = mode
        elif mode not in submitter.MODE_AVAIL:
            util.cti_plugin_print_error("Unknown mode.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        if mode != "local" and mode != "other" and not platform:
            util.cti_plugin_print_error("You must provide a platform on non-local mode.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        # create the alias
        binary_alias = alias.get_data_alias(binary)
        if not binary_alias:
            binary_alias = ""
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        alias_e = "Profiler_%s_%s" % (binary_alias, date)
        
        (data_entry, output) = self.default_init_command(params, alias_e = alias_e, no_output = True)
        entry.update_entry_parameter(binary, {"icc_profiler_entry": {"value": data_entry.uid}})
        res = plugin.plugin_auto_run(self, data_entry, format_o, auto_run, output)
        return res
    
    #---------------------------------------------------------------------------
    @hapi_command("run")
    def run_cmd(self, params):
        """ This function gets a Icc profiler entry (created using icc_profiler init command) and runs Icc profiler on it
        
        Args:
            self: class of the plugin
            params: working parameters
            
        Returns:
            Nothing
        """      
        maqao_dir = os.path.join(cti.cti_plugin_config_get_value(cti.THIRD_PARTY_DIR), "maqao")
        # MAQAO Mil is used by the plugin
        if not os.path.isdir(maqao_dir):
            util.cti_plugin_print_error("MAQAO is not installed\nYou can find the tool here: http://www.maqao.org/")
            util.cti_plugin_print_error("Install the tool in the third-party directory (third-party/maqao/).")
            return(cti.CTI_PLUGIN_ERROR_TOOL_NOT_FOUND)
        
        # gets parameters
        profile_uid = params["entry"]
        prefix = None
        if params["prefix"]:
            prefix = os.path.abspath(params["prefix"])
        nb_run_profiler = params["nb_run_profiler"]
        loop_plugin_uid = params["loop_plugin_uid"]
        loop_group_plugin_uid = params["loop_group_plugin_uid"]
        
        (input_param, output_param) = entry.load_data(profile_uid)
        
        binary_uid = output_param["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        mode = output_param["init"].params["mode"][cti.META_ATTRIBUTE_VALUE]
        partition = output_param["init"].params["partition"][cti.META_ATTRIBUTE_VALUE]
        platform = output_param["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
        stability_sanity_check = output_param["init"].params["stability_sanity_check"][cti.META_ATTRIBUTE_VALUE]
        run_parameters = output_param["init"].params["run_parameters"][cti.META_ATTRIBUTE_VALUE]
        prefix_execute_script = output_param["init"].params["prefix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        suffix_execute_script = output_param["init"].params["suffix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        submitter_cmd = output_param["init"].params["submitter_cmd"][cti.META_ATTRIBUTE_VALUE]
        submitter_opt = output_param["init"].params["submitter_opt"][cti.META_ATTRIBUTE_VALUE]
        submitter_user = output_param["init"].params["submitter_user"][cti.META_ATTRIBUTE_VALUE]
        mode_abs = input_param["init"].params["mode_abs"][cti.META_ATTRIBUTE_VALUE]
        
        ## get the binary path from the binary entry
        (_, output_binary) = entry.load_data(binary_uid)
        binary = output_binary["init"].params["binary_file"][cti.META_ATTRIBUTE_VALUE]
        binary_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, binary_uid), 
                                   cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                   binary)
        
        # get compiled application path
        (_, output_binary) = entry.load_data(binary_uid)
        compile_uid = output_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]

        compiled_application_path = "" 
        if compile_uid:
            (_, output_compile) = entry.load_data(compile_uid)
            compiled_application = output_compile["init"].params["compiled_application"][cti.META_ATTRIBUTE_VALUE]
            if compiled_application:
                compiled_application_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, compile_uid), 
                                                         cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                                         compiled_application)
               
        print "Running the profiler..."
        if run_parameters == []:
            run_parameters = ["\"\""]

        lib_measure_app = os.path.join(cti.cti_plugin_config_get_value(cti.COMMON_TOOLS_DIR), "lib_measure_app.so")
        mil_script = os.path.join(cti.cti_plugin_config_get_value(cti.COMMON_TOOLS_DIR), "measure_app.mil")

        # create the arguments
        work_dir = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)

        dict_opt = {}
        dict_opt["APP"] = [binary_path]
        if compiled_application_path:
            dict_opt["OTHER"] = [compiled_application_path]
        dict_opt["TOOL"] = [lib_measure_app, mil_script, maqao_dir]
        dict_opt["SCRIPTS"] = [prefix]
        dict_opt["platform"] = platform
        dict_opt["entry"] = params["entry"]
        dict_opt["plugin_execute_script"] = os.path.join(self.plugin_directory, "execute_profile.py")
        dict_opt["partition"] = partition
        dict_opt["submitter_cmd"] = submitter_cmd 
        dict_opt["submitter_opt"] = submitter_opt 
        dict_opt["submitter_user"] = submitter_user 
        dict_opt["prefix_execute_script"] = prefix_execute_script
        dict_opt["suffix_execute_script"] = suffix_execute_script
        dict_opt["username"] = self.username
        dict_opt["password"] = self.password

        dict_opt["script_params"] = tempfile.mkstemp(dir=work_dir)[1]
        script_params_f = open(dict_opt["script_params"], "w")
        script_params_f.write(str(nb_run_profiler) + "\n")
        set_freq_script = os.path.join(cti.cti_plugin_config_get_value(cti.COMMON_TOOLS_DIR), "set_freq.sh")
        script_params_f.write(set_freq_script + "\n")
        script_params_f.write(os.path.basename(lib_measure_app) + "\n")
        script_params_f.write(os.path.basename(mil_script) + "\n")
        script_params_f.write(str(stability_sanity_check) + "\n")
        script_params_f.write(os.path.basename(compiled_application_path) + "\n")
        script_params_f.write(",".join(run_parameters) + "\n")
        script_params_f.close()

        (job_dir, daemon_log_file) = submitter.submitter(dict_opt, self.plugin_uid, mode)

        execution_log_file = None

        for f in glob.glob("*.out"):
            if f != daemon_log_file:
                execution_log_file = os.path.abspath(f)

        file_daemon = open(daemon_log_file, "a")

        dict_opt["results_dir"] = os.path.join(work_dir, job_dir + "_local", job_dir, "RESULTS")
        dict_opt["nb_run_profiler"] = nb_run_profiler
        dict_opt["loop_group_plugin_uid"] = loop_group_plugin_uid
        dict_opt["mode_abs"] = mode_abs 
        dict_opt["stability_sanity_check"] = stability_sanity_check
        dict_opt["loop_plugin_uid"] = loop_plugin_uid 
        dict_opt["log_execution"] = execution_log_file 
        dict_opt["log_daemon"] = daemon_log_file 

        message_daemon = ""

        nb_files = 0
        for f in glob.glob("RESULTS/*.xml"):
            nb_files += len(f[2])

        if nb_files == 0:
            execution_log_f = open(execution_log_file, "r")
            execution_log_message = execution_log_f.readlines()
            execution_log_f.close()
            execution_log_message = " ".join(execution_log_message)

            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] ERROR: no xml file generated\n\tThe error message is:\n\
                    =================================\n%s\n=================================\n\
                    Temporary files are located in the directory %s" \
                    % (date, execution_log_message, os.getcwd())
            util.cti_plugin_print_error(message)
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            import_profile_results.import_results(dict_opt)
        else:
            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] Importing results into CTI..." % date
            print message
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            # import parameters
            import_profile_results.import_results(dict_opt)

            # cleaning temporary files
            shutil.rmtree(os.path.join(work_dir, job_dir + "_local"))

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = IccProfilerPlugin()
    exit(p.main(sys.argv))
