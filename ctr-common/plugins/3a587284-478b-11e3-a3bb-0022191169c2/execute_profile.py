#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import sys, os, subprocess, numpy, glob, stat

#constants & global
SCRIPT_PARAMS = "script_params"
MIL_SCRIPT = "measure_app.mil"
PROFILE_RESULT_FILE = "result.txt"


def file_replace(input_path, tuples, output_path=None):
    if not output_path:
        output_path=input_path
    
    to_change = open(input_path, 'r')
    lines = []
    for line in to_change:
        for expr, repl in tuples:
            line = line.replace(expr, repl)
        lines.append(line)
    to_change.close()
    
    #Writing the result
    to_change = open(output_path, 'w')
    to_change.writelines(lines)
    to_change.close()


nb_run_profiler = ""
set_freq_script = ""
stability_sanity_check = ""
run_parameters = ""

# get parameters
param_file = open(SCRIPT_PARAMS, "r")

nb_run_profiler = int(param_file.readline().rstrip('\n'))
set_freq_script = param_file.readline().rstrip('\n')
lib_measure = param_file.readline().rstrip('\n')
mil_footer = param_file.readline().rstrip('\n')
stability_sanity_check = float(param_file.readline().rstrip('\n'))
compiled_application = param_file.readline().rstrip('\n')
run_parameters = param_file.readline().rstrip('\n')

param_file.close()

# check parameters
if not str(nb_run_profiler):
    nb_run_profiler = 10
if not str(stability_sanity_check):
    print "You must provide the stability sanity check !"
    exit(1)

work_dir = os.getcwd()

relevance_threshold_min = (nb_run_profiler * 8) / 10

print "\n* Number of profiler run: %s" % nb_run_profiler
print "* Stability_sanity_check: %s" % stability_sanity_check
print "* Relevance_threshold_min: %s" % relevance_threshold_min

# get and untar the application
application_dir = os.path.join(work_dir, "OTHER")
if not os.path.isdir(application_dir):
        os.mkdir(application_dir)

os.chdir(application_dir)
if compiled_application:
    os.system("tar -xf " + compiled_application)
    os.system("rm " + compiled_application)

# get and untar the binary
os.chdir(os.path.join(work_dir, "APP"))
binary_name = os.listdir(".")[0]
os.system("tar -xf " + binary_name)
os.system("rm " + binary_name)
binary_name = binary_name.replace(".tar.gz", "")
os.system("cp %s %s" % (binary_name, application_dir))

# get the dynamic library
lib_measure_app_name = os.path.join(work_dir, "TOOL", lib_measure)
os.system("cp %s %s" % (lib_measure_app_name, application_dir))

# get the mil footer
mil_footer = os.path.join(work_dir, "TOOL", mil_footer)
os.system("cp %s %s" % (mil_footer, application_dir))

os.chdir(application_dir)

# create the mil script
file_replace(
                mil_footer,
                [
                    ("#__CTI_PLACEHOLDER_BIN_PATH__#", binary_name),
                    ("#__CTI_PLACEHOLDER_LIB__#", os.path.join(application_dir,lib_measure))
                ],
                MIL_SCRIPT)
os.system("rm %s" % mil_footer)

maqao_bin = os.path.join(work_dir, "TOOL", "maqao", "maqao")

print "\nAdding probes..."
os.system("%s instrument i=\"%s\"" % (maqao_bin, MIL_SCRIPT))
binary_name += "_i"

print "\n##########################################################################"
print "WARNING: Please make sure Hyperthreading and Turboboost are disabled."
print "On top of that, the frequency of the CPU should be stabilized. To do so, please use cpuspeed."
print "A script to set the frequency can be found here: '%s'." % set_freq_script
print "WARNING: The application must be compiled using the profiler flag of icc / ifort."
print "##########################################################################\n"

print "Running the profiler\n"

walltime = []
my_env = os.environ.copy()
if my_env.has_key("LD_LIBRARY_PATH"):
    my_env["LD_LIBRARY_PATH"] = ".:" + my_env["LD_LIBRARY_PATH"]
else:
    my_env["LD_LIBRARY_PATH"] = "."

for j in range(0, nb_run_profiler):
    icc_profile_cmd = "./%s %s" % (binary_name, run_parameters)
    
    # process prefix script
    sys.stdout.flush()
    path_prefix = os.path.join(work_dir, "SCRIPTS")
    file_cmd_path = os.path.join(work_dir, "file_cmd.sh")
    cmd = [file_cmd_path]
    file_cmd = open(file_cmd_path, "w")
    file_cmd.write("#!/bin/bash\n")
    prefix_file = os.path.join(path_prefix, "prefix.sh")
    if os.path.isfile(prefix_file):
        file_cmd.write("source %s\n" % prefix_file)
    file_cmd.write(icc_profile_cmd)
    file_cmd.close()
    os.chmod(file_cmd_path, stat.S_IXUSR | stat.S_IRUSR)
    
    # run the binary
    print "Run number %s..\n" % (j+1)
    print icc_profile_cmd
    (std_out, std_err) = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env).communicate()
    
    os.remove(file_cmd_path)
    
    print "Output of Profile:\n%s\n%s\n" % (std_out, std_err)
    
    if os.path.isfile(PROFILE_RESULT_FILE):
        result_file = open(PROFILE_RESULT_FILE, "r")
        walltime.append(long(result_file.readline()))
        result_file.close()
        os.system("rm %s" % PROFILE_RESULT_FILE)

if walltime:
    walltime_file = open(os.path.join(work_dir, "RESULTS", "walltime"), "w")
    walltime_file.write(str(long(numpy.median(walltime))))
    walltime_file.close()

# copy all csv files Profile produced
for xml_file in glob.glob("*.xml"):
    os.system("mv %s %s" % (xml_file, os.path.join(work_dir, "RESULTS", "")))
for dump_file in glob.glob("*.dump"):
    os.system("mv %s %s" % (dump_file, os.path.join(work_dir, "RESULTS", "")))

print "\nEnd of the profiler execution."
