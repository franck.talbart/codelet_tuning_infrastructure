#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit
import cti, ctr

from cti_hapi import entry, plugin, alias, util

import sys, os, numpy, glob, datetime, time
from xml.dom import minidom

def import_results(dict_opt):
    message_daemon = ""
    if dict_opt["results_dir"]:
        
        relevance_threshold_min = (dict_opt["nb_run_profiler"] * 8) / 10
        
        # repository type
        repository = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, dict_opt["entry"])
        # if the repository is local, we need its UID
        if repository == cti.CTR_REP_LOCAL:
            repository_path = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(dict_opt["entry"], cti.CTR_DATA_DIR)
            repository = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(repository_path))
        
        list_run_loops = {}
        list_loops = {}
        list_total_nb_cycle = []
        xml_to_import = []
        # process all xml files produced by the binary
        list_xml = glob.glob(os.path.join(dict_opt["results_dir"], "*.xml"))
        for xml_file in list_xml:
            run_total_cycle = 0
            xml_file = os.path.join(dict_opt["results_dir"], xml_file)
            try:
                # import the log file into profile entry
                filename = entry.put_file_in_entry(dict_opt["entry"], 
                                xml_file,
                                safe=False)
                xml_to_import.append(filename)
            except IOError:
                message = "CTI error: No such file: '" + xml_file + "' \n"
                util.cti_plugin_print_error(message)
                message_daemon += message
            
            list_loops_xml = {}
            xmldoc = minidom.parse(xml_file)
            loops = xmldoc.getElementsByTagName('loop')
            for loop in loops :
                for child in loop.childNodes:
                    if child.localName == "src_file":
                        for child_src in child.childNodes:
                            if child_src.localName == "line":
                                line = int(child_src.childNodes[0].nodeValue)
                            elif child_src.localName == "name":
                                name = str(child_src.childNodes[0].nodeValue)
                    elif child.localName == dict_opt["mode_abs"]:
                        # self_abs = the loop itself i.e. without the inner loop's coverage
                        # ticks_abs = the coverage including all the inner loops
                        nb_cycles = long(child.childNodes[0].nodeValue)
                    elif child.localName == "function":
                        function = str(child.childNodes[0].nodeValue)
                id_loop = "%s_%s_%s" % (line, function, name)
                if list_loops_xml.has_key(id_loop):
                    list_loops_xml[id_loop]["nb_cycles"] += nb_cycles
                else:
                    list_loops_xml[id_loop] = {}
                    list_loops_xml[id_loop]["nb_cycles"] = nb_cycles
                    list_loops_xml[id_loop]["function"] = function
                    list_loops_xml[id_loop]["name"] = name
                    list_loops_xml[id_loop]["first_line"] = line
            os.remove(xml_file)
            
            list_run_loops[xml_file] = {}
            for loopID in list_loops_xml:
                list_run_loops[xml_file][loopID] = list_loops_xml[loopID]["nb_cycles"]
                if list_loops.has_key(loopID):
                    list_loops[loopID]["nb_cycles"].append(list_loops_xml[loopID]["nb_cycles"])
                else:
                    list_loops[loopID] = {}
                    list_loops[loopID]["nb_cycles"] = [list_loops_xml[loopID]["nb_cycles"]]
                    list_loops[loopID]["function"] = list_loops_xml[loopID]["function"]
                    list_loops[loopID]["name"] = list_loops_xml[loopID]["name"]
                    list_loops[loopID]["first_line"] = list_loops_xml[loopID]["first_line"]
                run_total_cycle += list_loops_xml[loopID]["nb_cycles"]
            list_total_nb_cycle.append(run_total_cycle)
        
        dump_to_import = []
        # process all dump files produced by the binary
        list_dump = glob.glob(os.path.join(dict_opt["results_dir"], "*.dump"))
        for dump_file in list_dump:
            dump_file = os.path.join(dict_opt["results_dir"], dump_file)
            try:
                # import the log file into profile entry
                filename = entry.put_file_in_entry(dict_opt["entry"], 
                                dump_file,
                                safe=False)
                dump_to_import.append(filename)
            except IOError:
                message = "CTI error: No such file: '" + dump_file + "' \n"
                util.cti_plugin_print_error(message)
                message_daemon += message
        
        walltime = None
        walltime_file_path = os.path.join(dict_opt["results_dir"], "walltime")
        if os.path.isfile(walltime_file_path):
            walltime_file = open(walltime_file_path, "r")
            walltime = float(walltime_file.readline().rstrip('\n'))
            walltime_file.close()
            message = "\nWalltime: %s\n\n" % walltime
        else:
            message = "\nWalltime: No walltime\n\n"
        print message,
        message_daemon += message
        
        # creation of loop_group
        json = \
        {
            "init":
            {
                "attributes" : {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
                "params": 
                [
                    {cti.META_ATTRIBUTE_NAME : "icc_profiler",cti.META_ATTRIBUTE_VALUE : dict_opt["entry"]},
                    {cti.META_ATTRIBUTE_NAME : "nb_loops",cti.META_ATTRIBUTE_VALUE : len(list_loops)},
                    {cti.META_ATTRIBUTE_NAME : "profiler_version",cti.META_ATTRIBUTE_VALUE : "NA"},
                    {cti.META_ATTRIBUTE_NAME : "loops",cti.META_ATTRIBUTE_VALUE : []}
                ]
            }
        }
        
        output = ""
        try:
            output = plugin.execute_plugin_by_file(dict_opt["loop_group_plugin_uid"], json, dict_opt["username"], dict_opt["password"])
        except :
            print sys.exc_info()[1]
            exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        
        loop_group_uid = plugin.get_output_data_uid(output)
        
        #Alias for loop_group
        
        (_, output_param_profile) = entry.load_data(dict_opt["entry"])
        binary_entry = output_param_profile["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        bin_name = alias.get_data_alias(binary_entry)
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        if not bin_name:
            alias_loop_group = "Loop_group_{0}".format(date)
        else:
            alias_loop_group = "{0}_Loop_group_{1}".format(bin_name, date)
        
        if alias.set_data_alias(loop_group_uid, alias_loop_group) == 0:
            util.cti_plugin_print_warning("Cannot set the alias %s (already used?)" % (alias_loop_group))
        
        message = "* List of loops detected by the profiler:\n"
        print message,
        message_daemon += message
        # create loops entries
        for loopID in list_loops.keys():
            list_loops[loopID]["nb_cycles_median"] = long(numpy.median(list_loops[loopID]["nb_cycles"]))
            message = "\t* Loop: %s number of cycles (median): %s\n\n" % \
                (loopID, list_loops[loopID]["nb_cycles_median"])
            print message,
            message_daemon += message
            
            # loop language
            if ".c" in list_loops[loopID]["name"].lower():
                loop_language = "c"
            else:
                loop_language = "fortran"
            
            # stability
            if len(list_loops[loopID]["nb_cycles"]) >= relevance_threshold_min:
                mini = min(list_loops[loopID]["nb_cycles"])
                if mini == 0:
                    list_loops[loopID]["stability"] = "unstable"
                else:
                    s_check = (list_loops[loopID]["nb_cycles_median"] - mini) / mini
                    if s_check <= dict_opt["stability_sanity_check"]:
                        list_loops[loopID]["stability"] = "stable"
                    else:
                        list_loops[loopID]["stability"] = "unstable"
            else:
                list_loops[loopID]["stability"] = "unstable"
            
            if walltime is not None:
                loop_coverage_median = (float(list_loops[loopID]["nb_cycles_median"]) / walltime) * 100.0
            else:
                loop_coverage_median = None
            
            # creation of loop
            json = \
            {
                "init":
                {
                    "attributes" : {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "loop_id",cti.META_ATTRIBUTE_VALUE : loopID},
                        {cti.META_ATTRIBUTE_NAME : "first_line",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["first_line"]},
                        {cti.META_ATTRIBUTE_NAME : "language",cti.META_ATTRIBUTE_VALUE : loop_language},
                        {cti.META_ATTRIBUTE_NAME : "source_function_name",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["function"]},
                        {cti.META_ATTRIBUTE_NAME : "coverage_icc_profiler",cti.META_ATTRIBUTE_VALUE : loop_coverage_median},
                        {cti.META_ATTRIBUTE_NAME : "status_icc_profiler",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["stability"]},
                        {cti.META_ATTRIBUTE_NAME : "loop_files",cti.META_ATTRIBUTE_VALUE : None},
                        {cti.META_ATTRIBUTE_NAME : "loop_group",cti.META_ATTRIBUTE_VALUE : loop_group_uid}
                    ]
                }
            }
            
            output = ""
            try:
                output = plugin.execute_plugin_by_file(dict_opt["loop_plugin_uid"], json, dict_opt["username"], dict_opt["password"])
            except :
                print sys.exc_info()[1]
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            
            loop_uid = plugin.get_output_data_uid(output)
            
            # alias for loop
            now = datetime.datetime.now()
            date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
            if not bin_name:
                alias_loop = "Loop{0}_{1}".format(loopID, date)
            else:
                alias_loop = "{0}_Loop{1}_{2}".format(bin_name, loopID, date)
            if alias.set_data_alias(loop_uid, alias_loop) == 0:
                util.cti_plugin_print_error("Cannot set the alias %s (already used?)" % (alias_loop))
            list_loops[loopID]["loop_uid"] = loop_uid
        
        total_coverage_loop = None
        if list_total_nb_cycle and walltime is not None:
            total_coverage_loop = (float(numpy.median(list_total_nb_cycle)) / walltime) * 100.0
        
        # update the profile entry
        entry.update_entry_parameter(dict_opt["entry"], {"total_coverage_loop": {"value": total_coverage_loop},
                                                     "walltime": {"value": walltime},
                                                     "xml_output": {"value": xml_to_import, "append": True},
                                                     "dump_output": {"value": dump_to_import, "append": True}
                                                    })

    # import exectution log file into profile
    execution_log_file = ""
    try:
        execution_log_file = entry.put_file_in_entry(
                                dict_opt["entry"], 
                                dict_opt["log_execution"], 
                                False
                             )
    except IOError:
        message = "CTI error: No such file: '" + dict_opt["log_execution"] + "' \n"
        util.cti_plugin_print_error(message)
        message_daemon += message

    file_daemon = open(dict_opt["log_daemon"], "a")
    file_daemon.write(message_daemon)
    file_daemon.close()
    # import daemon log file into profile
    daemon_log_file = ""
    try:
        daemon_log_file = entry.put_file_in_entry(
                            dict_opt["entry"], 
                            dict_opt["log_daemon"], 
                            False
                          )
    except IOError:
        util.cti_plugin_print_error("No such file: '" + dict_opt["log_daemon"] + "'")

    entry.update_entry_parameter(dict_opt["entry"], {"logs": {"value": [daemon_log_file, execution_log_file], "append": True},
                                                 "nb_run_profiler": {"value": dict_opt["nb_run_profiler"]}
                                                })

    time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
    print "\n[" + str(time_process) + "] End of process"


