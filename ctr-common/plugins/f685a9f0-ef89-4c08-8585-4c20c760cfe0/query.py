#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import util_uid, database_manager, database, entry, alias, util

import time, copy


#---------------------------------------------------------------------------

def get_unique_alias(join_dict, table, source_alias, attribute):
    """
    Returns a new and unique SQL alias for the given table, using the given existing joins
    Args:
        table : the name of the table requesting a new unique SQL alias
        join_dict : a dictionnary containing the number of uses for each table
        source_alias : the alias of the source table
        attribute : the attribute that justifies the join
    Return:
        The new and unique SQL alias
    """
    
    num = ''
    
    if source_alias is None:
        source_table = ''
    else:
        source = source_alias.split('_')
        num = source[-1]
        
        #Verifying there is a number in the source alias
        try:
           int(num)
           source_table = '_'.join(source[0:-1])
        except:
            #No number
            source_table = source_alias
            num=''
    
    source_index = "{0} - {1}".format(num,attribute)
    
    curr_num = 0
    if table in join_dict:
        curr_num = 1 + join_dict[table]['last_num']
        if source_table is not None:
            if source_table in join_dict[table]['sources']:
                join_dict[table]['sources'][source_table][source_index] = curr_num
            else:
                join_dict[table]['sources'][source_table] = {source_index:curr_num}
    elif source_table is not None:
        join_dict[table] = {'sources':{source_table:{source_index:0}}}
    
    join_dict[table]['last_num'] = curr_num 
    
    return "{0}_{1}".format(table,curr_num)

#---------------------------------------------------------------------------

def get_link_info(source, attribute_name):
    
    result = {'type':None, 'data_type': None,'dest_column':None,'dest_table':None}
    
    entry_attributes = [
             "entry_uid",
             "repository",
             "path_repository",
             "plugin_uid",
             "date_time_start",
             "date_time_end",
             "user_uid",
             "plugin_exit_code",
             "alias",
             "tag", 
             "additional_files", 
             "note"
            ]
    
    #Searching for columns
    current_attributes = database_manager.get_columns(source)
    
    
    #Searching for Link Table:
    r_search = database_manager.search(
        {
         'L': {
               'NAME':["source"],
               'TYPE':"=",
               'VAL': source
              },
         'LOGIC': 'AND',
         'R': {
               'NAME':["name_source"],
               'TYPE':"=",
               'VAL': attribute_name
              }
         }, 
         database.Database(),
         "link_table",
         [cti.META_ATTRIBUTE_TARGET, 'name_target', 'attribute_type', 'type']
      )
    

    for r in r_search:
        result['dest_table'] = r[0]
        result['dest_column'] = r[1]
        result['data_type'] = r[2]
        result['type'] = r[3]
    
    #If not found, trying direct attributes
    if result['type'] is None:
        #Basic attribute
        if attribute_name in current_attributes:
            result['type']='direct'
        #Entry attributes for non-entry tables
        elif source != 'entry_info' and attribute_name in entry_attributes:
            result['type'] = 'entry'
    
    return result

#---------------------------------------------------------------------------

def generate_key_value_constraints(source, attribute_name, joins_dict, base_type):
    val_result = copy.deepcopy(source)
    val_result['VAL'] = attribute_name
    
    result = {}
    
    link_data = get_link_info(source['TABLE'], attribute_name)
    
    if link_data['type'] == 'entry':
        #Making the link to entry_info if needed
        if source['TABLE'] == base_type:
            #No need for a new entry link, using the base entry_info alias
            source['ALIAS'] = "entry_info"
        else:
            val_result['TABLE'] = "entry_info"
            val_result['ALIAS'] = get_unique_alias(joins_dict, 'entry_info', source['ALIAS'], attribute_name)
            
            result = \
            {
                'TYPE': 'LEFT INNER JOIN', 
                'TABLE': val_result['TABLE'], 
                'NAME':['id_entry_info'],
                'VAL':'id_{0}'.format(source['TABLE']),
                'TABLE_SOURCE': source['TABLE'],
                'ALIAS_SOURCE': source['ALIAS'],
                'ALIAS_TABLE': val_result['ALIAS']
            }
            #Using the new entry_info instance alias
            source['ALIAS'] = val_result['ALIAS']
        
        #Switching the source and link to use entry_info (source['ALIAS'] has already been handled).
        source['TABLE'] = "entry_info"
        link_data = get_link_info('entry_info', attribute_name)
    
    #Error case : no link is found.
    if link_data['type'] is None:
        util.cti_plugin_print_error("Unexpected column name '{0}' for table {1}.\nPlease verify your query.".format(attribute_name, source['TABLE']))
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    #Make the link if needed
    elif link_data['type'] != 'direct':
        #Prepare eventual contains
        if link_data['data_type'] == cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
            val_result['SPECIAL'] = {
                'COLUMN': 'id_{0}'.format(link_data['dest_table']),
                'NAME_SRC' : 'entry_uid',
                'JOIN': True 
            }
            val_result['ALIAS'] = get_unique_alias(joins_dict, link_data['dest_table'], source['ALIAS'], attribute_name)
            
        elif link_data['data_type'] == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
            val_result['SPECIAL'] = {
                'COLUMN': 'id_{0}'.format(source['TABLE']),
                'NAME_SRC' : link_data['dest_table'],
                'JOIN': False 
            }
            val_result['ALIAS'] = source['ALIAS']
            
        else:
            val_result['SPECIAL'] = {
                'COLUMN': link_data['dest_column'],
                'NAME_SRC' : attribute_name,
                'JOIN': False
            }
            val_result['ALIAS'] = get_unique_alias(joins_dict, link_data['dest_table'], source['ALIAS'], attribute_name)
        
        val_result['TABLE'] = link_data['dest_table']
        #Matrixes are handled in a subquery, they don't need any kind of join.
        if link_data['data_type'] != cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
            added_result = \
            {
                'TYPE': 'LEFT INNER JOIN', 
                'TABLE': val_result['TABLE'], 
                'NAME':[link_data['dest_column']],
                'VAL':'id_{0}'.format(source['TABLE']),
                'TABLE_SOURCE': source['TABLE'],
                'ALIAS_SOURCE': source['ALIAS'],
                'ALIAS_TABLE': val_result['ALIAS']
            }
            if result:
                result = {'L':result, 'LOGIC':'AND', 'R':added_result}
            else:
                result = added_result
    return (result, val_result)

#---------------------------------------------------------------------------

def generate_join(source, name_source, joins_dict):
    constraints_result = {}
    target = None
    result = database_manager.search(
            {
                'L':{'NAME':["source"], 'TYPE':"=", 'VAL':source['TABLE']}, 
                'LOGIC':'AND', 
                'R':{'NAME':["name_source"], 'TYPE':"=", 'VAL':name_source}
            }, 
            database.Database(),
            "link_table",
            fields=[
                    "type",
                    "name_target",
                    cti.META_ATTRIBUTE_TARGET,
                    "attribute_type"
                ]
        )
    
    for r in result:
        type_j = r[0]
        name_target = r[1]
        target = r[2]
        attr_type = r[3]
    if not target:
        util.cti_plugin_print_error("%s not found in %s." % (name_source, source['TABLE']))
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    target = {'TABLE': target, 'ALIAS':get_unique_alias(joins_dict, target, source['ALIAS'], name_source)}
    
    #Matrixes are handled in a subquery, thus not needing any kind of JOIN operation.
    if attr_type != cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
        if type_j == "O2M":
            constraints_result = \
            {
                'TYPE': 'LEFT INNER JOIN', 
                'TABLE_SOURCE': source['TABLE'], 
                'VAL': "id_{0}".format(source['TABLE']),
                'NAME': [name_target],
                'TABLE': target['TABLE'],
                'ALIAS_TABLE': target['ALIAS'],
                'ALIAS_SOURCE': source['ALIAS']
            }
            
        elif type_j == "M2O":
            constraints_result = \
            {
                'TYPE': 'LEFT INNER JOIN', 
                'TABLE': target['TABLE'], 
                'NAME': [name_target], 
                'VAL': name_source,
                'TABLE_SOURCE': source['TABLE'],
                'ALIAS_SOURCE': source['ALIAS'],
                'ALIAS_TABLE': target['ALIAS']
            }
        else:
            util.cti_plugin_print_error("Unrecognized join type : %s." % (type_j))
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    return (constraints_result, target)

#---------------------------------------------------------------------------

def get_constraints(constraints, source_table, current_name, joins_dict, base_type=None):
    result = {}
    name_dict = {}
    if base_type is None:
        base_type = current_name
    #If there is sub-attributes
    if constraints and isinstance(constraints, dict):
        #Process all sub-attributes
        for k in constraints:
            #Only make the join if there is a sublevel
            join_constraints = {}
            new_source_table = copy.deepcopy(source_table)
            if constraints[k]:
                join_constraints, new_source_table = generate_join(source_table, k, joins_dict)
            other_result, other_name_dict = get_constraints(constraints[k], new_source_table, k, joins_dict, base_type)
            
            if result:
                result = {'L':copy.deepcopy(other_result), 'LOGIC':'AND', 'R':copy.deepcopy(result)}
            else:
                result = copy.deepcopy(other_result)
            
            #Updating the result tree with joins if needed
            if join_constraints:
                result = {'L':copy.deepcopy(join_constraints), 'LOGIC':'AND', 'R':copy.deepcopy(result)}
            
            #Updating the name dictionnary
            if current_name != 'entry_info':
                for name_key in other_name_dict:
                    name_dict['{0}.{1}'.format(current_name,name_key)] = other_name_dict[name_key]
            else:
                name_dict = copy.deepcopy(other_name_dict)
    #No sub-attributes
    else:
        #Generating the join, if needed
        result, val_gkv = generate_key_value_constraints(source_table, current_name, joins_dict, base_type)
        name_dict[current_name] = val_gkv
    return (result, name_dict)

#---------------------------------------------------------------------------

def get_query_constraints(repository, query, type_q, sort):
    """
    query the database:
      * the repositories to search are defined by '''places'''
      * query is a dictionnary with keys and values
      * type_q is the entry type (application, loop, ...)
      * sort: field number to sort
      * start: the start number
      * end: the end number
    get the query constraints:
      * repository: the repositories to search are defined by '''places'''
      * query: a dictionary with keys and values
      * type_q: the entry type (application, loop, ...)
      * sort: the field number to sort
    """
    
    joins_dict = {}
    type_uid = None
    outfile = None
    type_attr = {}
    
    if type_q is not None:
        if util_uid.is_valid_uid(type_q):
            type_uid = util_uid.CTI_UID(type_q, cti.CTR_ENTRY_PLUGIN)
            type_q = alias.get_plugin_alias(type_uid)
        else:
            type_uid = alias.get_plugin_uid(type_q)
    else:
        type_q = 'entry_info'
        type_uid = False
    
    #TODO: Make sure the password is never displayed when doing links.
    if type_uid:
        infile, outfile = entry.load_defaults(type_uid)
        if outfile.has_key("init"):
            for att in outfile["init"].params.keys():
                type_attr[att] = {"type" : outfile["init"].params[att][cti.META_ATTRIBUTE_TYPE]}
                if cti.META_ATTRIBUTE_PASSWORD in outfile["init"].params[att]:
                    type_attr[att]["pwd"] = outfile["init"].params[att][cti.META_ATTRIBUTE_PASSWORD]
                else:
                    type_attr[att]["pwd"] = False
        else:
            util.cti_plugin_print_error("Query failed: type is not descriptive: %s." % (type_q))
            exit( cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    elif type_uid is None:
        util.cti_plugin_print_error("Query failed: invalid type: %s." % (type_q))
        exit( cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    constraint = query["constraint"]
    if repository:
        constraint = {'L':{'NAME':['"entry_info"."path_repository"'], 'TYPE':"=", 'VAL':repository}, 'LOGIC':'AND', 'R':constraint}
    
    if query["date_start"]:
        constraint = {'L':{'NAME':["entry_info","date_time_start"], 'TYPE':">=", 'VAL':query["date_start"]}, 'LOGIC':'AND', 'R':constraint}
        
    if query["date_end"]:
        constraint = {'L':{'NAME':["entry_info","date_time_end"], 'TYPE':"<=", 'VAL':query["date_end"]}, 'LOGIC':'AND', 'R':constraint}
    
    r_get_constraints = {}
    attr_dict = {}
    
    if type_q != 'entry_info' and type_q in query['attr_dict']:
        (r_get_constraints, attr_dict) = get_constraints(query["attr_dict"][type_q], {'TABLE':type_q, 'ALIAS':type_q}, type_q, joins_dict)
    
    if 'entry_info' in query['attr_dict']:
        (entry_constraints, entry_attr_dict) = get_constraints(query["attr_dict"]["entry_info"], {'TABLE':"entry_info", 'ALIAS':"entry_info"}, 'entry_info', joins_dict)
        r_get_constraints = {'L':entry_constraints, 'LOGIC':'AND', 'R':r_get_constraints} 
        for key in entry_attr_dict:
            attr_dict['entry_info.{0}'.format(key)] = entry_attr_dict[key]
    
    if r_get_constraints: 
        constraint = {'L':r_get_constraints, 'LOGIC':'AND', 'R':constraint} 
    
    # Processing "list" fields
    fields=[]
    matrix_fields = []
    is_matrix = False
    for f in query["fields"]:
        try:
            is_matrix = False
            f_s = f.split(".")
            
            #Search for the first sub_attribute in the speficied type and entry_info.
            source = type_q
            num = ''
            added_join = {}
            final_field = ''
            alias_source = source
            
            if f_s[0] == 'entry_info':
                f_s = f_s[1:]
            
            curr_index = 0
            while(curr_index < len(f_s)):
                curr_field = f_s[curr_index]
                #Searching for the alias of the current source
                alias_source = source
                if num:
                    alias_source = "{0}_{1}".format(source, num)
                
                final_field = curr_field
                link_info = get_link_info(source, curr_field)
                
                if link_info['type'] == 'entry' and type_q != 'entry_info':
                    link_info = get_link_info('entry_info', curr_field)
                    if curr_index != 0:
                        entry_alias = 'entry_info'
                        #If we have a defined number, it means we are using another table than the main one with entry and a join is needed
                        if num:
                            entry_alias = get_unique_alias(joins_dict, 'entry_info', alias_source, "id_{0}".format(source))
                            added_join = {
                                'L':copy.deepcopy(added_join),
                                'LOGIC':'AND',
                                'R':
                                    {
                                        'TYPE': 'LEFT OUTER JOIN', 
                                        'TABLE_SOURCE': source, 
                                        'VAL': "id_{0}".format(source),
                                        'NAME': ['id_entry_info'],
                                        'TABLE': 'entry_info',
                                        'ALIAS_TABLE': entry_alias,
                                        'ALIAS_SOURCE': alias_source
                                    }
                            }
                            alias_source = entry_alias
                    else:
                        alias_source = 'entry_info'
                    source = 'entry_info'
                    
                
                if link_info['type'] is None:
                    util.cti_plugin_print_error("Requested field does not exist in {0}: {1}" .format(type_q, f))
                    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                elif link_info['type'] != 'direct':
                    if curr_index == (len(f_s)-1):
                        #Data uid case
                        if link_info['data_type'] == cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
                            #If the last of the list is of type data_uid : auto-add the alias
                            f_s.append('alias')
                        #List case
                        elif link_info['data_type'] != cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
                            f_s.append(curr_field)
                        #There is no need to process the matrix case since the query syntax requires the full name by default
                    search_join = "{0} - {1}".format(num,curr_field)
                    if link_info['dest_table'] in joins_dict and source in joins_dict[link_info['dest_table']]['sources'] and search_join in joins_dict[link_info['dest_table']]['sources']:
                        num = link_info[curr_field]['sources'][search_join]
                    else:
                        alias_dest = get_unique_alias(joins_dict, link_info['dest_table'], alias_source, curr_field)
                        
                        constraint_l = {}
                        
                        if link_info['type'] == "O2M":
                            constraint_l = \
                            {
                                'TYPE': 'LEFT OUTER JOIN', 
                                'TABLE': link_info['dest_table'],
                                'ALIAS_TABLE': alias_dest,
                                'NAME': [link_info['dest_column']],
                                'TABLE_SOURCE': source, 
                                'ALIAS_SOURCE': alias_source,
                                'VAL': "id_{0}".format(source)
                            }
                            
                        elif link_info['type'] == "M2O":
                            constraint_l = \
                            {
                                'TYPE': 'LEFT OUTER JOIN', 
                                'TABLE': link_info['dest_table'], 
                                'ALIAS_TABLE': alias_dest,
                                'NAME': [link_info['dest_column']], 
                                'TABLE_SOURCE': source,
                                'ALIAS_SOURCE': alias_source,
                                'VAL': curr_field
                            }
                        
                        if constraint_l:
                            added_join = {'L':copy.deepcopy(added_join),'LOGIC':'AND','R':constraint_l}
                        
                        num = alias_dest.split('_')[-1]
                    #If the matrix itself was the requested field
                    if link_info['data_type'] == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
                        if curr_index == (len(f_s)-1):
                            alias_source = alias_dest
                            columns = database_manager.get_columns(link_info['dest_table'])
                            columns.remove('order')
                            columns.remove('id_{0}'.format(link_info['dest_table']))
                            columns.remove('id_{0}'.format(source))
                            final_field = columns
                        else:
                            is_matrix = True
                    source = link_info['dest_table']
                    
                curr_index += 1
            
            #Adding the final field name
            if isinstance(final_field, list):
                for curr_f_field in final_field:
                    fields.append('"{0}"."{1}"'.format(alias_source,curr_f_field))
                    matrix_fields.append(f)
            else:
                fields.append('"{0}"."{1}"'.format(alias_source,final_field))
                if is_matrix:
                    matrix_fields.append(f)
            
            
            
            #Adding the join constraints if any exist
            if added_join:
                constraint = {'L':added_join, 'LOGIC':'AND', 'R':copy.deepcopy(constraint)}
            
        except:
            util.cti_plugin_print_error("Error checking links for field {0}. Check that your query is valid.".format(f))
            exit( cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    #Defining sort field.
    order = ""
    
    sort_field = []
    if abs(sort) > len(fields):
        print "Ignoring sorting of column #{0}: there are only {1} columns.".format(sort, len(fields))
    elif sort != 0:
        if sort < 0:
            sort = (-sort)
            order = " DESC"
        sort_field = [fields[sort-1]+order]
    
    dict_return = {"query": query, 
                   "constraint": constraint, 
                   "type_q": type_q, 
                   "fields": fields, 
                   "matrix_fields": matrix_fields, 
                   "sort_field": sort_field, 
                   "attr_dict": attr_dict, 
                   "type_attr": type_attr}
    return dict_return

#---------------------------------------------------------------------------

def db_query(dict_param, db, start_time, start=None, end=None):
    """
    perform the query:
      * dict_param: a dictionnary with keys and values parameters
      * db: the database instance
      * start_time: the time when the query starts to be processed
      * start: the start number
      * end: the end number
    """
    
    query = dict_param["query"]
    constraint = dict_param["constraint"]
    type_q = dict_param["type_q"]
    fields = dict_param["fields"]
    matrix_fields = dict_param["matrix_fields"]
    sort_field = dict_param["sort_field"]
    attr_dict = dict_param["attr_dict"]
    type_attr = dict_param["type_attr"]
    
    try:
        result = []
        for data in database_manager.search(constraint,
                                          db,
                                          type_q,
                                          fields,
                                          start,
                                          end,
                                          sort_field,
                                          attr_dict): 
            data_set = {}
            
            for index in range(len(fields)):
                value_attr = data[index]
                att = fields[index]
                att_suffix = att
                if len(att.split(".")) > 1:
                    att_suffix = att.split(".")[1]
                    strip_att_suffix = att_suffix.strip('"')
                if type_attr.has_key(strip_att_suffix):
                    if type_attr[strip_att_suffix]["type"] == cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
                        value_attr = database_manager.id2uid(value_attr, db)
                    # Do not display the password
                    if type_attr[strip_att_suffix]["pwd"]:
                        value_attr = ""
                data_set[query["fields"][index]] = value_attr
            
            result.append(data_set)
        count = database_manager.count_query(constraint, db, type_q, attr_dict)
        return_struct = {
            'data':result,
            'total': count,
            'time': time.time() - start_time,
            'matrix_fields': matrix_fields
        }
        return return_struct
    except:
        import logging
        logging.exception("")
        util.cti_plugin_print_error("Query failed: check that your query is valid.")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

#---------------------------------------------------------------------------

def db_query_couple(dict_param, link_params, margins, db, start_time, start=None, end=None):
    """
    perform the query:
      * dict_param: a dictionnary with keys and values parameters
      * link_params: a list of link parameters
      * margins: a list of error margin for the link parameters
      * db: the database instance
      * start_time: the time when the query starts to be processed
      * start: the start number
      * end: the end number
    """
    
    constraint = list(i["constraint"] for i in dict_param)
    type_q = dict_param[0]["type_q"]
    attr_dict = list(i["attr_dict"] for i in dict_param)
    
    try:
        result = []
        for data in database_manager.search_couple_uids(constraint,
                                                        link_params,
                                                        margins,
                                                        db,
                                                        type_q,
                                                        start,
                                                        end,
                                                        attr_dict):
            
            result.append(data)
        count = database_manager.count_query_couple(constraint, link_params, margins, db, type_q, attr_dict)
        return_struct = {
            'data': result,
            'total': count,
            'time': time.time() - start_time,
        }
        return return_struct
    except:
        import logging
        logging.exception("")
        util.cti_plugin_print_error("Query failed: check that your query is valid.")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

#---------------------------------------------------------------------------
