== Description ==

This plugin enables complex queries to repositories and helps to navigate repositories in a comprehensive and user-friendly manner.
Query can perform both fuzzy and case insensitive search: 
for example, if a user searches for "exascale*", "Exascale Research Center" would match this request.

== Usage examples ==

The query language is rather simple:
 * To search in all fields just type your search query, eg. 
<pre>
$ cti query select all exascale
</pre>
 * <pre>"*"</pre> is a wildcard which means "every other characters", eg.
<pre>
$ cti query select all exascale*
</pre>
 * To restrict the search to a given field use the syntax 
<pre>
parameter:value
</pre>
, eg. 
<pre>
$ cti query select all name:ftalbart
</pre>
The default analyzed file is the output file.
 * To search in the info or input or output file, write the corresponding prefix:
<pre>
$ cti query select all output.name:ftalbart
</pre>
 * It is possible to combine multiple constraints by separating them with a space and enclosing the whole query in quotes, eg. 
<pre>
$ cti query select all "output.name:ftalbart output.email:exascale-computing.eu"
</pre>
will search for all entries containing ftalbart in the name field and exascale-computing.eu in the email field in the output file.
 * If a user wants to match fields inside the ctr_info.txt field, just prefix the field name with info, eg. 
<pre>
$ cti query select all plugin_uid:d9737b5f-b6ce-4a4a-96e5-a13ae8867e99
</pre>
will search all the entries created by user plugin. Instead of using uids, you can reference plugins and entries by alias with the following syntax: 
<pre>
$ cti query select all plugin_uid:{user}
</pre>
 * The available attributes in ctr_info are: plugin_uid, data_time_start, date_time_end, user_uid, alias, plugin_exit_code, tag, note
 * It is possible to search numeric fields by range, for example to get all the entries for which the number_of_threads in between 1 and 4, you can use 
<pre>
$ cti query select all number_of_threads:1..4
</pre>
 * Finally, it is possible to get the entry by using its UID:
<pre>
$ cti query select all _id:f1452ea1-c8a5-4724-9463-4987f1f85d05
</pre>
or
<pre>
$ cti query select all _id:{admin}
</pre>
