#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

""" Query parser
        Provides a lexer and a parser for the query language.
"""

from cti_hapi import alias, util
import cti

import copy
from ply import lex, yacc
from ply.lex import TOKEN

#Debug constants for the Lexer and Parser. Change to 1 to activate debugging.
LEX_DEBUG_MODE=0
PARSE_DEBUG_MODE=0

def replace_alias2uid(alias_q):
    r = alias.get_data_uid(alias_q)
    if not r:
        r = alias.get_plugin_uid(alias_q)
        r = [r]
    if not r:
        util.cti_plugin_print_error("%s is not a known alias"%(alias_q))
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    else:
        r = set(r)
        
    result = set()
    for uid in r:
        uid = str(uid)
        result.add(uid)
    return result

#------------------------------------------------------------------------

class CtiQueryLexer():
    keywords = \
    {
        'true': 'TRUE',
        'false': 'FALSE',
        'none': 'NONE'
    }
    
    #------------------------------------------------------------------------
    
    tokens = \
    [
        'ALIAS',
        'UID',
        'STRING',
        'AND',
        'OR',
        'NUMERIC',
        'LP',
        'RP',
        'ATTRIBUTE_SEPARATOR',
        'PARAMETER_SEPARATOR',
        'SUPERIOR',
        'INFERIOR',
        'SUPERIOR_EQ',
        'INFERIOR_EQ',
        'RANGE',
        'CONTAINS'
    ] + keywords.values()
    
    #------------------------------------------------------------------------
    
    #Regular expressions for basic elements
    space = r' '
    space_char = r'['+space+r']'
    number_range = r'0-9'
    number_char = r'['+number_range+r']'
    alias_begin = r'\{'
    alias_end = r'\}'
    
    #Regular expression rules for simple tokens
    t_PARAMETER_SEPARATOR = r'\.'
    t_ATTRIBUTE_SEPARATOR = r':'
    t_SUPERIOR = r'>'
    t_INFERIOR = r'<'
    t_SUPERIOR_EQ = r'>='
    t_INFERIOR_EQ = r'<='
    t_RANGE = r'->'
    t_LP = r'\('
    t_RP = r'\)'
    t_CONTAINS = r'~'
    
    #Complex Strings rules
    
    #All characters except spaces, numbers, and the separator between attribute names and values.
    char = r'[^\'"'+\
            space+\
            t_CONTAINS+\
            t_SUPERIOR+\
            t_INFERIOR+\
            t_PARAMETER_SEPARATOR+\
            t_ATTRIBUTE_SEPARATOR+\
            t_LP+\
            t_RP+\
            number_range+\
            alias_begin+\
            alias_end+\
            r'-]'
    
    #To be easily detectable, a string that contains spaces or starts with a number should be enclosed in quotes or double quotes.
    string = r'(' + char + r'(' + char + r'|' + number_char +r')*)' + \
        r'|("[^"]*")' + \
        r'|(\'[^\']*\')'
    
    uid_char = r'['+number_range+r'a-f]'
    alias = alias_begin + r'[^'+alias_end+space+']*' + alias_end
    
    numeric = r'(-{0,1})' + number_char + r'+'
    
    
    uid = uid_char + r'{8}-(' + uid_char + r'{4}-){3}' + uid_char + r'{12}'
    
    
    or_token = space_char + r'*' + r'[Oo][Rr]' +space_char + r'*'
    and_token = space_char + r'*' + r'[Aa][Nn][Dd]' +space_char + r'*' + \
        r'|' + space_char + r'+'
    
    #------------------------------------------------------------------------
    
    # Regular expression rules for other tokens
    @TOKEN(alias)
    def t_ALIAS(self,t):
        #Removing alias delimiters.
        t.value = replace_alias2uid((t.value[1:-1]).replace('*','%'))
        return t
    
    #------------------------------------------------------------------------
    
    # Regular expression rules for other tokens
    @TOKEN(uid)
    def t_UID(self,t):
        return t
    
    #------------------------------------------------------------------------
    
    # Regular expression rules for other tokens
    @TOKEN(string)
    def t_STRING(self,t):
        #Keywords
        if t.value.lower() in self.keywords:
            t.type = self.keywords[t.value.lower()]
            t.value = t.value.upper()
        #Removing eventual string delimiters.
        elif t.value[0] in ['\'', '"']:
                t.value = t.value[1:-1]
        return t
    
    #------------------------------------------------------------------------
    
    # Regular expression rules for other tokens
    @TOKEN(or_token)
    def t_OR(self,t):
        t.value = 'OR'
        return t
    
    #------------------------------------------------------------------------
    
    # Regular expression rules for other tokens
    @TOKEN(and_token)
    def t_AND(self,t):
        t.value = 'AND'
        return t
    
    #------------------------------------------------------------------------
    
    @TOKEN(numeric)
    def t_NUMERIC(self,t):
        if '.' in t.value:
            t.value = float(t.value)
        else:
            t.value = int(t.value)
        return t
    
    #------------------------------------------------------------------------
    
    #ignore rules
    #Ignoring spaces when not in quote/doublequote
    #t_ignore = r'[' + space + r']+'
    
    #------------------------------------------------------------------------
    
    # Error handling rule
    def t_error(self,t):
        util.cti_plugin_print_error("LEXER : Illegal character '%s'." % t.value[0])
        t.lexer.skip(1)
    
    #------------------------------------------------------------------------
    
    def token(self):
        return self.lexer.token()
    
    #------------------------------------------------------------------------
    
    def tokenize(self, data):
        self.lexer.input(data)
        while True:
            token = self.lexer.token()
            if token:
                yield token
            else:
                break
    
    #------------------------------------------------------------------------
    
    def input(self, data):
        self.lexer.input(data)
    
    #------------------------------------------------------------------------
    
    def __init__(self):
        self.lexer = lex.lex(module=self, debug=LEX_DEBUG_MODE)

#------------------------------------------------------------------------

class CtiQueryParser():
    
    precedence = (
        ('left', 'OR'),
        ('left', 'AND'),
        ('nonassoc', 'ATTRIBUTE_SEPARATOR')
    )
    
    start = 'condition_statement'
    
    entry_attributes = [
        "entry_uid",
        "repository",
        "path_repository",
        "plugin_uid",
        "date_time_start",
        "date_time_end",
        "user_uid",
        "plugin_exit_code",
        "alias",
        "tag",
        "note",
        "additional_files"
    ]
    
    def __init__(self, entry_type=None):
        self.lexer = CtiQueryLexer()
        self.tokens = self.lexer.tokens
        self.parser = yacc.yacc(module=self,write_tables=0,debug=PARSE_DEBUG_MODE)
        self.entry_type = entry_type
        self.join_dict = {}
        if not self.entry_type:
            self.entry_type = 'entry_info'
    
    #------------------------------------------------------------------------
    
    def add_attribute_to_join_dict(self, string_list):
        def put_attribute_in_join_dict(curr_attr, curr_dict):
            if not curr_attr:
                return {} 
            if not curr_attr[0] in curr_dict:
                curr_dict[curr_attr[0]] = {}
            curr_dict[curr_attr[0]] = put_attribute_in_join_dict(
                                                        curr_attr[1:], 
                                                        curr_dict[curr_attr[0]]
                                                ) 
            return curr_dict
        put_attribute_in_join_dict(string_list, self.join_dict)
    
    #------------------------------------------------------------------------
    
    @staticmethod
    def make_join_dict(query_struct):
        def deep_update(a_dict, another_dict):
            dict_one = copy.deepcopy(a_dict)
            dict_two = copy.deepcopy(another_dict)
            for key,val in dict_two.iteritems():
                if isinstance(val, dict):
                    res = deep_update(dict_one.get(key, {}), val)
                    dict_one[key] = res
                else:
                    dict_one[key] = copy.deepcopy(val)
            return copy.deepcopy(dict_one)
        def make_dict_from_list(string_list):
            if string_list:
                return {string_list[0]:make_dict_from_list(string_list[1:])}
            else:
                return {}
        
        if 'LOGIC' in query_struct:
            if 'NAME' in query_struct:
                return make_dict_from_list(query_struct['NAME'])
            else:
                return deep_update(CtiQueryParser.make_join_dict(query_struct['L']), CtiQueryParser.make_join_dict(query_struct['R']))
                
        else:
            if not 'NAME' in query_struct:
                return {}
            return make_dict_from_list(query_struct['NAME'])
    
    #------------------------------------------------------------------------
    
    def get_join_dict(self):
        return self.join_dict
    
    #------------------------------------------------------------------------
    
    def parse(self,data,*arg,**kwarg):
        if data:
            result = self.parser.parse(data,self.lexer, *arg,**kwarg)
            if result:
                self.join_dict = CtiQueryParser.make_join_dict(result)
            
            return result
        else:
            return []
    
    #------------------------------------------------------------------------
    
    def p_error(self,p):
        util.cti_plugin_print_error("PARSER : Syntax error at '{0}'".format(p))
    
    #------------------------------------------------------------------------
    
    def p_condition_statement(self, p):
        """condition_statement : condition_reduced
                               | condition_group"""
        #This rule serves to accepts a single condition as a valid query
        p[0] = p[1]
    
    #------------------------------------------------------------------------
    
    def p_condition_reduced(self, p):
        """condition_reduced : condition
                             | condition_parenthesis"""
        #This rule serves to accepts a single condition as a valid query
        p[0] = p[1]
    
    #------------------------------------------------------------------------
    
    def p_condition_group(self, p):
        """condition_group : condition_statement logic condition_reduced"""
        #Note that the only non-existing cases in condition_group are those with a group on the right side.
        #This makes sure that the language is read left to right.
        p[0] = {'L':p[1], 'LOGIC': p[2], 'R':p[3]}
    
    #------------------------------------------------------------------------
    
    def p_condition_parenthesis(self, p):
        """condition_parenthesis : LP condition_statement RP"""
        #Simple function defined to reduce cases in condition_group
        p[0] = p[2]
    
    #------------------------------------------------------------------------
    
    def p_condition_matrix(self, p):
        """condition : attribute condition_parenthesis"""
        
        def full_name_matrix_attributes(tree):
            if 'LOGIC' in tree:
                full_name_matrix_attributes(tree['L'])
                full_name_matrix_attributes(tree['R'])
            else:
                tree['NAME'] = tree['NAME'][1:]
                self.add_attribute_to_join_dict(tree['NAME'])
        
        full_name_matrix_attributes(p[2])
        p[0] = {
            'SUBQUERY': p[2],
            'SPECIAL': 'MATRIX',
            'TYPE': 'IN',
            'NAME': p[1]
        }
        
        self.add_attribute_to_join_dict(p[0]['NAME'])
    
    #------------------------------------------------------------------------
    
    def p_condition(self, p):
        """condition : attribute value_statement
                     | attribute CONTAINS value_statement"""
        
        #attribute CONTAINS value
        if p[2] == '~':
            p[0] = p[3]
            p[0]['SPECIAL'] = 'CONTAINS'
        #attribute value
        else:
            p[0] = p[2]
        
        #Adding the name
        p[0]['NAME'] = p[1]
        
        #Adding attribute to the join dict
        self.add_attribute_to_join_dict(p[1])
    
    #------------------------------------------------------------------------
    
    def p_attribute(self, p):
        """attribute : string_chain ATTRIBUTE_SEPARATOR"""
        if not isinstance(p[1], list):
            p[1] = [p[1]]
        
        #Entry direct attribute for the given type
        if len(p[1]) == 1 and p[1][0] in self.entry_attributes:
            p[1].insert(0,"entry_info")
        #Type attribute
        else:
            p[1].insert(0,self.entry_type)
        
        
        p[0] = p[1]
    
    #------------------------------------------------------------------------
    
    def p_string_chain(self, p):
        """string_chain : STRING
                        | STRING PARAMETER_SEPARATOR string_chain"""
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[3]
            p[0].insert(0,p[1])
    
    #------------------------------------------------------------------------
    
    def p_value_statement(self, p):
        """value_statement : value
                           | value_parenthesis"""
        #This rule serves to accepts a single condition as a valid query
        p[0] = p[1]
    
    #------------------------------------------------------------------------
    
    def p_value_group(self, p):
        """value_group : value_all logic value_statement"""
        p[0] = {'L':p[1], 'LOGIC': p[2], 'R': p[3], 'TYPE': 'LOGIC'}
    
    #------------------------------------------------------------------------
    
    def p_value_all(self, p):
        """value_all : value
                     | value_parenthesis
                     | value_group"""
        #This rule serves to accepts a single condition as a valid query
        p[0] = p[1]
    
    #------------------------------------------------------------------------
    
    def p_value_parenthesis(self, p):
        """value_parenthesis : LP value_all RP"""
        #Simple function defined to reduce cases in condition_group
        p[0] = p[2]
    
    #------------------------------------------------------------------------
    
    def p_value(self, p):
        """value : boolean
                 | entry
                 | string
                 | numeric"""
        p[0] = p[1]
    
    #------------------------------------------------------------------------
    
    def p_logic(self, p):
        """logic : AND
                 | OR"""
        #Simple function defined to reduce cases in condition_group
        p[0] = p[1]
    
    #------------------------------------------------------------------------
    
    def p_none(self, p):
        """value : NONE"""
        #Booleans are converted to integers because of SQLite
        p[0] = {'TYPE': 'is', 'VAL': None}
    
    #------------------------------------------------------------------------
    
    def p_boolean_true(self, p):
        """boolean : TRUE"""
        #Booleans are converted to integers because of SQLite
        p[0] = {'TYPE': '=', 'VAL': int(True)}
    
    #------------------------------------------------------------------------
    
    def p_boolean_false(self, p):
        """boolean : FALSE"""
        #Booleans are converted to integers because of SQLite
        p[0] = {'TYPE': '=', 'VAL': int(False)}
    
    #------------------------------------------------------------------------
    
    def p_entry(self, p):
        """entry : UID
                 | ALIAS"""
        
        if isinstance(p[1],str):
            p[0] = {'TYPE': '=', 'VAL': p[1]}
        else:
            p[0] = {'TYPE': 'IN', 'VAL': p[1]}
    
    #------------------------------------------------------------------------
    
    def p_str(self, p):
        """string : STRING"""
        p[0] = {'TYPE': 'LIKE', 'VAL': p[1].replace('%','\\%').replace('*','%').replace('_', '\\_')}
    
    #------------------------------------------------------------------------
    
    def p_numeric(self, p):
        """numeric : numeric_value RANGE numeric_value
                   | SUPERIOR numeric_value
                   | INFERIOR numeric_value
                   | SUPERIOR_EQ numeric_value
                   | INFERIOR_EQ numeric_value
                   | numeric_value"""
        if len(p) == 4:
            p[0] = {'TYPE': 'BETWEEN', 'VAL':p[1], 'AND': p[3]}
        elif len(p) == 2:
            p[0] = {'TYPE': '=', 'VAL':p[1]}
        else:
            p[0] = {'TYPE': p[1], 'VAL':p[2]}
    
    #------------------------------------------------------------------------
    
    def p_numeric_value(self,p):
        """numeric_value : NUMERIC
                         | NUMERIC PARAMETER_SEPARATOR NUMERIC"""
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = float(str(p[1])+p[2]+str(p[3]))
