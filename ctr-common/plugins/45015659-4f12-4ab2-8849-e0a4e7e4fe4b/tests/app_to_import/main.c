#include <stdio.h>
#include <stdlib.h>

#define DEFAULT 32



void codelet_0001 (int size, int* a, int x, int* b, int* c)
{
   int i = 0;
   
   for (i = 0; i < size; i++)
      c[i] = a[i] * x + b[i];
}

void print (int size, int* tab)
{
   int i = 0;
   
   for (i = 0; i < size; i++)
      printf("%d ", tab[i]);
   printf("\n");
}

void init (int size, int* tab)
{
   int i = 0;
   
   for (i = 0; i < size; i++)
   {
      tab[i] = ((long int) (&tab)) % (i + 11) + 1;
   }
}


int main (int argc, char** argv)
{
   int size;
   int* a, *b, *c;
   int x = ((long int) &argv[0]) % 37;

   if (argc != 2)
      size = DEFAULT;
   else
      size = atoi (argv[1]);      

   a = malloc (size * sizeof (int));
   b = malloc (size * sizeof (int));
   c = malloc (size * sizeof (int));
      
   init (size, a);
   init (size, b);
   codelet_0001 (size, a, x, b, c);
   print (size, c);
   
   free (a);
   free (b);
   free (c);
   
   return (0);
}
