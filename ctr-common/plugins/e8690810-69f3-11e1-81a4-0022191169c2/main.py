#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from __future__ import division

import cti

from cti_hapi import plugin, entry, description, alias, util
from cti_hapi.main import HapiPlugin, hapi_command

import sand_box

import sys, os, json, subprocess

class ProcessPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        self.work_params = description.description_write(self.command, self.work_params)
        prefix = self.work_params[self.command].params["prefix"][cti.META_ATTRIBUTE_VALUE]
        run = self.work_params[self.command].params["run"][cti.META_ATTRIBUTE_VALUE]
        suffix = self.work_params[self.command].params["suffix"][cti.META_ATTRIBUTE_VALUE]
        
        if not run:
            util.cti_plugin_print_error("The run script is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
                
        if not suffix:
            util.cti_plugin_print_error("The suffix script is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if not prefix:
            util.cti_plugin_print_error("The prefix script is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        # Convert relative paths to absolute paths
        self.work_params[self.command].params["run"][cti.META_ATTRIBUTE_VALUE] = os.path.abspath(run)
        if prefix != "":
            self.work_params[self.command].params["prefix"][cti.META_ATTRIBUTE_VALUE] = os.path.abspath(prefix)
        if suffix != "":
            self.work_params[self.command].params["suffix"][cti.META_ATTRIBUTE_VALUE] = os.path.abspath(suffix)

        self.default_init_command(params)
        return 0
    
    #---------------------------------------------------------------------------
    
    @hapi_command("run")
    def run_cmd(self, params):
        """ prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        keep_files = params["keep_new_files_only"]
        list_uids = params["list_uids"]
        query_uid = params["query_uid"]
        repository_query = params["repository_query"]
        query = params["query"]
        type_query = params["type_query"]
        process = params["process"]
        
        self.work_params = description.description_write(self.command, self.work_params)
        
        rep = self.work_params[self.command].attributes[cti.META_ATTRIBUTE_REP]
        if cti.META_ATTRIBUTE_REP_PRODUCE in params:
            rep = params[cti.META_ATTRIBUTE_REP_PRODUCE]
        
        data_entry = entry.create_data_entry(self.plugin_uid, rep, self.username)
        
        if data_entry is None:
            util.cti_plugin_print_error("Can't create the entry.")
            return cti.CTI_PLUGIN_ERROR_IO
        
        for def_param in self.output_params[self.command].params.keys():
            if self.work_params[self.command].params.has_key(def_param) and \
                    self.work_params[self.command].params[def_param].has_key(cti.META_ATTRIBUTE_VALUE):
                self.output_params[self.command].params[def_param][cti.META_ATTRIBUTE_VALUE] = \
                               self.work_params[self.command].params[def_param][cti.META_ATTRIBUTE_VALUE]
                               
        self.output_params.record_output(self.command, data_entry.path)
        self.work_params.record_input(self.command, data_entry.path)
        
        self.output_params.update_references(self.command, data_entry.uid)
        
        try:
            (_, output_param) = entry.load_data(process)
        except:
            util.cti_plugin_print_error("Can't open the process entry.")
            return cti.CTI_PLUGIN_ERROR_CRITICAL_IO
        
        try:
            prefix = output_param["init"].params["prefix"][cti.META_ATTRIBUTE_VALUE]
            run = output_param["init"].params["run"][cti.META_ATTRIBUTE_VALUE]
            suffix = output_param["init"].params["suffix"][cti.META_ATTRIBUTE_VALUE]
        except:
            util.cti_plugin_print_error("This entry does not seem to be a correct process entry.")
            return cti.CTI_PLUGIN_ERROR_INCOMPATIBLE_ENTRY
        print "Running the query"
        array_uid = []
        if query:
            json_file = \
            {
                "select": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_query},
                        {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                        {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : type_query},
                        {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                        {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : False},
                        {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1}
                    ]
                }
            }
            
            result = ""
            try:
                result = plugin.execute_plugin_by_file(query_uid,
                                              json_file,
                                              self.username,
                                              self.password)
            except:
                print sys.exc_info()[1]
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
            r = json.loads(result)
            
            result = []
            for l in r["data"]:
                result.append(str(l["entry_info.entry_uid"]))
            
            if len(result) > 1:
                if result[len(result)-1] == "":
                    del(result[len(result)-1])
            array_uid = map(cti.CTI_UID, result)
        array_uid += list_uids
        
        total = len(array_uid)
        counter = 1.
        
        for uid in array_uid:
            alias_p = alias.get_data_alias(uid)
            if not alias_p:
                alias_p = str(uid)
                
            util.rewrite_output_line("[" + str(int((counter * 100.) / total)) + " %] Processing " + alias_p)
            
            with sand_box.Sandbox(prefix, run, suffix, uid, keep_files) as sandbox:
                try:
                    entry.put_dir_in_entry(data_entry.uid, sandbox, dir_dest=str(uid))
                except:
                    util.cti_plugin_print_error("Error when putting entry %s directory in process entry" % uid)
                    print sys.exc_info()[1]
                    return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            counter += 1
            
        print "The process is finished!" 
        print plugin.plugin_exit(data_entry.entry)
        
        return 0
    
    #---------------------------------------------------------------------------
    
    @hapi_command("run_command")
    def run_command_cmd(self, params):
        """ prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        command = params["command"]
        query = params["query"]
        repository_query = params["repository_query"]
        type_query = params["type_query"]
        list_uids = params["list_uids"]
        query_uid = params["query_uid"]
        date_start = params["date_start"]
        date_end = params["date_end"]
        
        print "Running the query"
        array_uid = []
        if query:
            json_file = \
            {
                "select": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_query},
                        {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                        {cti.META_ATTRIBUTE_NAME : "date_start",cti.META_ATTRIBUTE_VALUE : date_start},
                        {cti.META_ATTRIBUTE_NAME : "date_end",cti.META_ATTRIBUTE_VALUE : date_end},
                        {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : type_query},
                        {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                        {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : False},
                        {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1}
                    ]
                }
            }
            
            result = ""
            try:
                result = plugin.execute_plugin_by_file(query_uid,
                                              json_file,
                                              self.username,
                                              self.password,
                                              )
            except:
                print sys.exc_info()[1]
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            r = json.loads(result)
            
            result = []
            for l in r["data"]:
                result.append(str(l["entry_info.entry_uid"]))
            
            if len(result) > 1:
                if result[len(result)-1] == "":
                    del(result[len(result)-1])
            array_uid = map(cti.CTI_UID, result)
        array_uid += list_uids
        total = len(array_uid) 
        counter = 1.
        
        for uid in array_uid:
            alias_p = alias.get_data_alias(uid)
            if not alias_p:
                alias_p = str(uid)
                
            util.rewrite_output_line("[" + str(int(counter / total * 100)) + " %] Processing " + alias_p + " ")
            
            cur_command = []
            for cmd_part in command:
                cur_command.append(str(cmd_part).replace("<UID>", str(uid)))
            
            try:
                sys.stdout.flush()
                child = subprocess.Popen(cur_command)
                output = child.communicate()
                rc = child.returncode
                
                if rc != 0:
                    raise Exception("The command " + 
                            ' '.join(cur_command) + 
                            " encountered the error #" + 
                            str(rc) +
                            " Output: " + 
                            str(output))
                counter = counter + 1
            except:
                util.cti_plugin_print_error("Failed to execute command: {0}".format(command))
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED
        print "The process is finished!" 
        return 0

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = ProcessPlugin()
    exit(p.main(sys.argv))
