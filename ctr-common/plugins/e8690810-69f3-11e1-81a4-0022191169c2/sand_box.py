#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

import tempfile, shutil, os, sys, subprocess

from cti_hapi import util

class Sandbox():
    
    def __init__(self, prefix, run, suffix, uid, keep_files):
        self.directory = tempfile.mkdtemp()
        self.prefix = prefix
        self.run = run
        self.suffix = suffix
        self.uid = uid
        self.keep_files = keep_files 
#---------------------------------------------------------------------------

    def __enter__(self):
        os.chdir(self.directory)
        
        self.run_script(self.prefix)
        
        src = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, self.uid)
        if not src:
            util.cti_plugin_print_error("Can't get the path!")
        try:
            copy(src, self.directory, src)
        except:
            print "Can't copy %s" % (self.uid)
            print sys.exc_info()[0]
        
        self.run_script(self.run)
        old_list = list_directory(self.directory)
        self.run_script(self.suffix)
        
        # We clean the sandbox. Only the files created by suffix are kept.
        new_list = list_directory(self.directory)
        new_list = new_list & old_list
        
        if self.keep_files:
            for (f, _) in new_list:
                if os.path.exists(f):
                    if os.path.isfile(f):
                        os.remove(f)
                    else:
                        shutil.rmtree(f)
        
        return self.directory
#---------------------------------------------------------------------------

    def __exit__(self, exc_type, exc_value, traceback):
        shutil.rmtree(self.directory)
#---------------------------------------------------------------------------

    def run_script(self, script_path):
        if script_path != "":
            shutil.copy(script_path, self.directory)
            dest_script = os.path.join(self.directory, os.path.basename(script_path))
            cmd = [script_path, str(self.uid)]
            sys.stdout.flush()
            child = subprocess.Popen(
                    cmd,
                    shell=False,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE
                )
            output = child.communicate()
            f = open(script_path + ".log", "w")
            f.write("==================== Standard output ====================\n" + str(output[0]))
            f.write("==================== Error output ====================\n" + str(output[1]))
            f.close()
            os.remove(dest_script)
#---------------------------------------------------------------------------

def copy(src, dst, srcbackup):
    for i in os.listdir(src):
        i = os.path.join(src, i)
        if os.path.isdir(i): 
            copy(i, dst, srcbackup)
        else:
            try: os.makedirs(os.path.dirname(dst + os.sep + i[len(srcbackup)+1:]))
            except: pass
            shutil.copyfile(i, dst + os.sep + i[len(srcbackup)+1:])
#---------------------------------------------------------------------------

def list_directory(path):
    files = set([])
    for i in os.listdir(path):
        if os.path.isdir(i):
            # We store the date because maybe it is an old file which has been updated
            files.add((i, os.stat(i).st_mtime))
            files = files.union(list_directory(i))
        else:
            (_, ext) = os.path.splitext(i)
            if ext != '.log':
                files.add((i, os.stat(i).st_mtime))
    return files
