== Description ==

=== init and run commands ===

The process plugin automatically runs scripts on a huge amount of CTI entries.
The first step consists in providing 3 scripts (prefix, run, suffix).

The second step consists in giving the query that will select the CTI entries to process.
All the CTI entries must be created by the same plugin.

The 3 scripts are run is a sandbox which contains the entry to process.
The first script is supposed to prepare the sandbox (for example: compilation, or retrieve data from CTI).
The second script is mandatory. This script is supposed to process your data.
The last script can produce files. These data will be stored into CTI. The files produced by the other scripts
will no be recorded. If you want to keep them, set "keep_new_files_only" parameter to false.

Each script is called with the UID of the current entry as the first parameter.

==== Usage examples ====

Provide the 3 scripts:
<pre>
$ cti process init prefix.py run.py suffix.sh
DATA_CTI_UID=12345....
</pre>

Run the process on all the loops:
<pre>
$ cti process run 12345... "*" all loop
</pre>

=== run_command command ===

The plugin allows you to also run a simple command on a set of CTI entries selected by a query.
The command is a table of command arguments separated by commas.
Also, by including "<UID>" in your command, it will be automatically replaced by the current CTI entry UID to process:

==== Usage examples ====

Remove all the loops:
<pre>
$ cti process run_command "cti,rm,data,<UID>" "*" all loop
</pre>

List the files of each CTI entries:
<pre>
$ cti process run_command "ls,$CTI_ROOT/ctr-common/data/<UID>" "*" common 
</pre>


