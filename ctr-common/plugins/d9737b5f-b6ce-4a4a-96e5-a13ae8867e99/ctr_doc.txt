== Description ==

This plugin itializes CTI user.

== Usage examples ==

Create a user:

<pre>
$ cti user init [<username>] [<password>] [<full_name>] [<company_name>] [<email>] [<web_page>] [<phone>] [<fax>]
</pre>

Login:
<pre>
$ cti user login <username> [<password>]
</pre>

Logout:
<pre>
$ cti user logout
</pre>

Get the logged user:
<pre>
$ cti user whoami
</pre>
