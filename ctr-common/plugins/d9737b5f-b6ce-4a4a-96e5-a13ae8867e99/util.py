#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi import database_manager, database, util

import hashlib

def check_username(name, value, avoid_name):
    """ Description

    Args:
        name: name
        value: value
        param: working parameter

    Returns:
      true or false
    """

    if name == "username":
        if value.strip() == "":
            util.cti_plugin_print_error("The username is mandatory.")
            return True
        if value != avoid_name:
           db = database.Database()
           uids = database_manager.search_uids({'NAME':["user","username"], 'TYPE':"=", 'VAL':value},
                                                  db,
                                                  "user",
                                                  name_dict={'user.username':{'ALIAS':'user', 'TABLE':'user'}}
                                              )
           
           if len(uids) > 0:
               util.cti_plugin_print_error("This username is already used.")
               return True
    if name == "password":
        if value is None or str(value.strip()) == "":
            util.cti_plugin_print_error("The password is mandatory.")
            return True
    return False
#---------------------------------------------------------------------------

def crypt_password(name, value, param):
    """ Description

    Args:
        name: name
        value: value
        param: working parameter

    Returns:
      
    """
    if name == "password" and value != "":
        m = hashlib.md5()
        m.update(value)
        return m.hexdigest()
    return value
