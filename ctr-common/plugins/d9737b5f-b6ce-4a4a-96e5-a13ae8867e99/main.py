#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import entry, user, description,\
                                     cti_json, database, database_manager
from cti_hapi import util as util_hapi
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, hashlib, getpass

import util

SESSION_NAME=".cti"

class UserPlugin(HapiPlugin):
#---------------------------------------------------------------------------
    @hapi_command("init")
    def user_init(self, params):
        """ prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        self.work_params = description.description_write(self.command,
                                                        self.work_params, 
                                                        util.check_username,
                                                        accum_process=util.crypt_password)
        self.default_init_command(params, alias_e = self.work_params[self.command].params["username"][cti.META_ATTRIBUTE_VALUE])
        return 0
    
#---------------------------------------------------------------------------
    
    @hapi_command("update")
    def user_update(self, params):
        """ prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        data_uid = params["entry"]
        (_, output_data) = entry.load_data(data_uid)
        
        # The user can only update HIS data
        username = output_data["init"].params["username"][cti.META_ATTRIBUTE_VALUE]
        if username != self.username and self.username != "admin":
            util_hapi.cti_plugin_print_error("You can't update this entry!")
            return cti.CTI_PLUGIN_ERROR_RIGHT
        
        return description.update_entry(self, 
                                 params,
                                 util.check_username, 
                                 username,
                                 util.crypt_password)
    
#---------------------------------------------------------------------------
    
    @hapi_command("check_password")
    def check_password_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        """
        username = params["username"]
        password = params["password"]
        format = params["format"]
        
        result = user.check_password(username, password)
        
        if (format == "json"):
            print cti_json.cti_json_encode({"result": bool(result)})
            return 0
        
        if result:
            print "1"
            return 0
        print "0"
        return 0
    
#---------------------------------------------------------------------------
    
    @hapi_command("login")
    def login_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        username = params["username"]
        password = params["password"]
        session_filename = SESSION_NAME
        
        home = os.environ["HOME"]
        path_session = os.path.join(home, session_filename)
        
        if os.path.isfile(path_session):
            util_hapi.cti_plugin_print_error("A session is already opened. " +\
                "Please log out first using 'cti user logout'.")
            return cti.CTI_PLUGIN_ERROR_SESSION_EXISTS
        
        if password is None:
            password = getpass.getpass('Password: ')
        
        m = hashlib.md5()
        m.update(password)
        password = m.hexdigest()
        
        result = user.check_password(username, password)
        
        if result:
            session = open(path_session, 'w')
            session.write(username + '\n')
            session.write(password + '\n')
            session.close()
            
            db = database.Database()
            result = list(database_manager.search_uids(
                                              {'NAME':["user","username"], 'TYPE':"=", 'VAL':username},
                                              db,
                                              "user",
                                              name_dict={'user.username':{'ALIAS':'user', 'TABLE':'user'}}
                                          ))
            
            if len(result)  == 1:
                user_uid = result[0]
            else:
                util_hapi.hapi_error("Error while converting username to user_uid.")
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
            if user_uid:
                (_, output_data) = entry.load_data(user_uid)
                full_name = output_data['init'].params['full_name'][cti.META_ATTRIBUTE_VALUE]
            else:
                full_name = username
            
            print "Session opened. Welcome " + str(full_name) + "!\n"
            print "You can logout by doing 'cti user logout'.\n"
            return 0
        
        util_hapi.cti_plugin_print_error("Wrong username or password. " +\
              "Maybe you should ask the administrator to create an account.")
        return cti.CTI_PLUGIN_ERROR_WRONG_PASSWORD
    
#---------------------------------------------------------------------------
    
    @hapi_command("whoami")
    def whoami_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        format = params["format"]
        if (format == "txt"):
            if self.username == cti.NO_SESSION:
                print "No session found."
            else:
                print "You are " + self.username + "."
        elif (format == "raw"):
            print self.username
        else:
            print "Unknown format."
        
        return 0
    
#---------------------------------------------------------------------------
    
    @hapi_command("logout")
    def logout_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        session_filename = SESSION_NAME
        
        home = os.environ["HOME"]
        path_session = os.path.join(home, session_filename)
        
        if os.path.isfile(path_session):
            os.remove(path_session)
            print "You have been successfully log out. Good bye!"
        else:
            print "No session found."
        return 0
    
#---------------------------------------------------------------------------
    
    def check_passwd(self):
        if self.command == "init":
            if self.username == "admin":
                return HapiPlugin.check_passwd(self)
            else:
                util_hapi.cti_plugin_print_error("You must be administrator to do this.")
                exit(cti.CTI_PLUGIN_ERROR_RIGHT)
        
        elif self.command == "update":
            return HapiPlugin.check_passwd(self)
        else:
            return True
    
#---------------------------------------------------------------------------

if __name__ == "__main__":
    p = UserPlugin()
    exit(p.main(sys.argv))
