#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import cti_json, database, database_manager, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, datetime

class NotePlugin(HapiPlugin):
    @hapi_command("add")
    def add_cmd(self, params):
        """ Add a note to an entry

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          int
        """
        entry = params["entry"]
        note = params["note"]
        
        data_info = ctr.ctr_plugin_info_file_load_by_uid(entry)
        
        if not data_info:
            util.cti_plugin_print_error("Entry not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        old_note = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_NOTE)
        
        now = datetime.datetime.now()
        date = now.strftime('%Y/%m/%d_%H:%M:%S-')
        
        note = note.replace(",", "").strip() # Used separator 
        note = note.replace("-", "_").strip() # User separator

        txt = date + self.username + "-" + note.replace("\"", "")
        if old_note:
            values_note = txt + ",%s" % (old_note)
        else:
            values_note = txt
                    
        ctr.ctr_plugin_info_put_value(data_info, cti.DATA_INFO_NOTE, values_note)
        ctr_info_name = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, 
                                                                    entry), 
                                        cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME))
        ctr.ctr_plugin_info_record_in_file(data_info, ctr_info_name)
        db = database.Database()
        
        if database_manager.insert("note", {"id_entry_info": str(entry),
                                           "note":note},
                                        db) is False:
            util.cti_plugin_print_error("Error while adding the note into the database.")
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
                
        print "Note successfully added."
        
        return 0
#---------------------------------------------------------------------------
        
    @hapi_command("rm")
    def rm_cmd(self, params):
        """ Remove a note from an entry

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          int
        """
        entry = params["entry"]
        note_number = params["note_number"]
        
        data_info = ctr.ctr_plugin_info_file_load_by_uid(entry)
        
        if not data_info:
            util.cti_plugin_print_error("Entry not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        old_note = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_NOTE)
        
        note = []
        if old_note:
            note = old_note.split(",")
        
        try:
            r = note.pop(note_number)
        except:
            util.cti_plugin_print_error("wrong index: %s." % note_number)
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        note = ",".join(note)
        
        ctr.ctr_plugin_info_put_value(data_info, cti.DATA_INFO_NOTE, note)
        ctr_info_name = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, 
                                                                    entry), 
                                        cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME))
        ctr.ctr_plugin_info_record_in_file(data_info, ctr_info_name)
        db = database.Database()
        database_manager.delete("note", 
                                        { 
                                       'L':{'NAME':["id_entry_info"], 'TYPE':"=", 'VAL': database_manager.uid2id(entry, db)},
                                       'LOGIC':'AND',
                                       'R':{'NAME':["note"], 'TYPE':"=", 'VAL': r}
                                      },
                                      db)
        print "Note successfully removed."
        
        return 0
#---------------------------------------------------------------------------

    @hapi_command("get")
    def get_cmd(self, params):
        """ Get a note from an entry

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          int
        """
        entry = params["entry"]
        output_format = params["format"]
        
        data_info = ctr.ctr_plugin_info_file_load_by_uid(entry)
        notes = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_NOTE)
        
        if not data_info:
            util.cti_plugin_print_error("Entry not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        result = []
        if notes:
            notes = notes.split(",")
            for note in notes:
                    try:
                        tab_note = note.split("-")
                        date = tab_note[0]
                        username = tab_note[1]
                        txt = tab_note[2].strip()
                    except:
                        util.cti_plugin_print_error("invalid form with the note: '%s'" % (note))
                        return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
                    result += [{"date": date, "username": username, "txt":txt}]
        
        if output_format == "json":
            print cti_json.cti_json_encode(result)
        elif output_format == "txt":
            for note in result:
                print note["date"] + ", " + note["username"] + ": " + note["txt"]
            if result == []:
                print "No note"
        else:
            util.cti_plugin_print_error("Unknown format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        
        return 0
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = NotePlugin()
    exit(p.main(sys.argv))
