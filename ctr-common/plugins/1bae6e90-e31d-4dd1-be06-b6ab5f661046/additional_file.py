#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import entry, cti_json, database, database_manager, util

import os

def add_additional_file(self, params):
        """ Add a file into an entry
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          int
        """
        entry_i = params["entry"]
        file_i = os.path.abspath(params["file"])
        
        data_info = ctr.ctr_plugin_info_file_load_by_uid(entry_i)
        
        if not data_info:
            util.cti_plugin_print_error("Entry not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        old_additional_files = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_ADDITIONAL_FILES)
        
        check_add_files = []
        if old_additional_files is not None:
            check_add_files = old_additional_files.split(",")
            
        if os.path.basename(file_i) in check_add_files:
            util.cti_plugin_print_error("This file already exist.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        try:
            file_i = entry.put_file_in_entry(
                        entry_i, 
                        file_i, 
                        True
                    )
        except IOError:
            util.cti_plugin_print_error("No such file or directory: '" + file_i + "'")
            return(cti.CTI_PLUGIN_ERROR_IO, cti.CTI_PLUGIN_ERROR_IO)
        
        if old_additional_files:
            values_file = "%s,%s" % (file_i, old_additional_files)
        else:
            values_file = file_i
        
        ctr.ctr_plugin_info_put_value(data_info, cti.DATA_INFO_ADDITIONAL_FILES, values_file)
        ctr_info_name = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, 
                                                                    entry_i), 
                                        cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME))
        ctr.ctr_plugin_info_record_in_file(data_info, ctr_info_name)
        
        db = database.Database()
        
        if database_manager.insert("additional_files", {"id_entry_info": str(entry_i),
                                                   "additional_files":file_i},
                                                db) is False:
            util.cti_plugin_print_error("Error while adding the file into the database.")
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
        print "File successfully added."
        
        return 0
#---------------------------------------------------------------------------
def get_additional_files(self, params):
        """ Get the additional filenames from an entry
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          int
        """
        entry_i = params["entry"]
        output_format = params["format"]
        
        data_info = ctr.ctr_plugin_info_file_load_by_uid(entry_i)
        files = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_ADDITIONAL_FILES)
        
        if not data_info:
            util.cti_plugin_print_error("Entry not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        result = []       
        if files:
            files = files.split(",")
            for file_i in files:
                    try:
                        result.append(file_i)
                    except:
                        util.cti_plugin_print_error("invalid form with the file: '%s'" % (file_i))
                        return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
                    
        
        if output_format == "json":
            print cti_json.cti_json_encode(result)
        elif output_format == "txt":
            for file_i in result:
                print file_i
            if result == []:
                print "No file"
        else:
            util.cti_plugin_print_error("Unknown format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        
        return 0
#---------------------------------------------------------------------------
def rm_additional_file(self, params):
        """ Remove a file from an entry
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          int
        """
        entry_i = params["entry"]
        file_number = params["file_number"]
        
        data_info = ctr.ctr_plugin_info_file_load_by_uid(entry_i)
        
        if not data_info:
            util.cti_plugin_print_error("Entry not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        old_file = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_ADDITIONAL_FILES)
        
        file_i = []
        if old_file:
            file_i = old_file.split(",")
        
        try:
            f = file_i.pop(file_number)
            entry.rm_file_from_entry(entry_i, f)
        except:
            util.cti_plugin_print_error("wrong index: %s." % file_number)
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        file_i = ",".join(file_i)
        
        ctr.ctr_plugin_info_put_value(data_info, cti.DATA_INFO_ADDITIONAL_FILES, file_i)
        ctr_info_name = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, 
                                                                    entry_i), 
                                        cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME))
        ctr.ctr_plugin_info_record_in_file(data_info, ctr_info_name)
        
        db = database.Database()
        database_manager.delete("additional_files",
                                                    { 
                                                           'L':{'NAME':["id_entry_info"], 'TYPE':"=", 'VAL': database_manager.uid2id(entry_i, db)},
                                                           'LOGIC':'AND',
                                                           'R':{'NAME':["additional_files"], 'TYPE':"=", 'VAL': f}
                                                    },
                                                    db)
        print "File successfully removed."
        
        return 0
#---------------------------------------------------------------------------
