#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import entry, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os

import additional_file

class FilePlugin(HapiPlugin):
#---------------------------------------------------------------------------
    @hapi_command("init")
    def init_cmd(self, params):
        """ Add some raw data to CTR
            Args:
                self: class of the plugin
                params: working parameters
            
            Returns:
                Nothing
        """
        if not params["files"]:
            util.cti_plugin_print_error("File is mandatory.")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
 
        alias_entry = os.path.basename(params["files"])
        alias_entry = alias_entry.strip()
        self.default_init_command(params, alias_e = alias_entry)
        return 0

#---------------------------------------------------------------------------
    @hapi_command("get")
    def get_cmd(self, params):
        """ Description
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
            Nothing
        """
        
        inp, out = entry.load_data(params["uid"])
        
        if os.path.exists(str(params["uid"])):
             util.cti_plugin_print_error("Retrieving the files would overwrite directory {0}. Aborting!\n".format(params["uid"]))
             exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        entry.get_dir_from_entry(params["uid"], str(params["uid"]))
    
#---------------------------------------------------------------------------
    @hapi_command("add_additional_file")
    def add_additional_file_cmd(self, params):
        """ Add an additional file into the specified entry
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
            Nothing
        """
        additional_file.add_additional_file(self, params)
    
#---------------------------------------------------------------------------
    @hapi_command("get_additional_file")
    def get_additional_file_cmd(self, params):
        """ Return the list of additional files of the entry
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
            Nothing
        """
        additional_file.get_additional_files(self, params)
    
#---------------------------------------------------------------------------
    @hapi_command("rm_additional_file")
    def rm_additional_file_cmd(self, params):
        """ Remove the file from the specified entry
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
            Nothing
        """
        additional_file.rm_additional_file(self, params)

#---------------------------------------------------------------------------
# main function
if __name__ == "__main__":
    p = FilePlugin()
    exit(p.main(sys.argv))
