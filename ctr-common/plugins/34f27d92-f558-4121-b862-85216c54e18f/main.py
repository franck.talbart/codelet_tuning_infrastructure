#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi import database_manager, database
from cti_hapi.main import HapiPlugin, hapi_command

import sys

class CategoryPlugin(HapiPlugin):
    @hapi_command("list")
    def list_cmd(self, params):
        """ Returns a list of category
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
       
        db = database.Database()
        result = database_manager.search({'NAME':["plugin_uid"], 'TYPE':"=", 'VAL':str(self.plugin_uid)},
                                        db,
                                        "category",
                                        fields=["entry_info.entry_uid", "category.name"]
                                        )
        
        for r in result:
                print r[0] + ":" + r[1]
    
#---------------------------------------------------------------------------
    
    # By pass the authentification system
    # Needed by the doc plugin
    def check_passwd(self):
        return True
    
#---------------------------------------------------------------------------

if __name__ == "__main__":
    p = CategoryPlugin()
    exit(p.main(sys.argv))
