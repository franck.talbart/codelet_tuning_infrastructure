#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import util

import datetime, matplotlib.pyplot as plt, numpy as np

import util as util_p
#---------------------------------------------------------------------------

def histogram(self, params):
    """ Create a PNG file containing a histogram
    
    Args:
        self: class of the plugin
        params: working parameters
    
    Returns:
      Nothing
    """
    
    filename= params["filename"]
    field = params["field"]
    title = params["title"]
    step = params["step"]
    is_string = params["is_string"]
    
    query_results = util_p.get_data(self, params)
    
    data_hist = []
    counter = 1.
    for data in query_results['data']:
        util.rewrite_output_line("[%d %%] Plotting %s" % (int(counter / query_results['total'] * 100), data['entry_info.alias']))
        counter += 1
        value = data[field]
        if not isinstance(value, list):
            value = [value]
            
        new_val = []
        for val in value:
            if val is None:
                val='None'
                util.cti_plugin_print_warning("NULL value found for field {0} in entry {1}".format(field, data['entry_info.alias']))
                
            if not is_string:
                try:
                    val = float(val)
                except:
                    util.cti_plugin_print_warning("invalid float value '{0}' found for field {1} in entry {2}".format(val, field, data['entry_info.alias']))
                    continue
            new_val.append(val)
        
        data_hist += new_val
    data_hist_f = []
    
    if not is_string:
        for i in data_hist:
            data_hist_f.append(i)
    else:
        ind = sorted(list(set(data_hist)))
        data_hist_f = [ind.index(i) for i in data_hist]
        plt.xticks(range(len(ind)), ind)
        
    try:
        n, bins, patches = plt.hist(data_hist_f,
                               facecolor='#B5031E',
                               alpha=0.75,
                               bins = np.arange(int(min(data_hist_f)),
                                            int(max(data_hist_f)) + 2, step)
                               )
    except:
        util.cti_plugin_print_error("Incorrect data type")
        return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    
    plt.xlabel(field.split('.')[-1])
    plt.ylabel('Counts')
    now = datetime.datetime.now()
    date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
    plt.title(title + ( " / produced by CTI (%s)" % (date)))
    plt.grid(True)
    plt.savefig(filename, bbox_inches='tight', pad_inches = 0.0)
    print "Done!"

#---------------------------------------------------------------------------
