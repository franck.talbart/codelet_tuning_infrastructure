#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi import util_uid, util

import cti

import re, datetime, matplotlib.pyplot as plt, numpy as np

import util as util_p

#---------------------------------------------------------------------------
def bargraph(self, params):
    """ Create a PNG file containing a bargraph
    
    Args:
        self: class of the plugin
        params: working parameters
    
    Returns:
      Nothing
    """
    
    filename = params["filename"]
    fields = params["fields"]
    title = params["title"]
    display_label = params["display_label"]
    regexp_field = params["regexp_field"]
    
    colors = ["y", "r", "b", "g", "b", "w"]
    if len(fields) > 6:
        util.cti_plugin_print_error("Too many fields, max:6")
        return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
   
   
    query_results = util_p.get_data(self, params)
   
    x_axis_name = []
    #Storing the attribute names.
    for field in fields:
        field_split = field.split('.')
        if len(field_split) > 1:
            x_axis_name.append(".".join(field_split[-2:]))
        else:
            x_axis_name.append(".".join(field_split[-1]))
    if not filename:
        util.cti_plugin_print_error("The output filename is empty.")
        return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    
    data = {}
    label = []
    cnt = 0
    for f in x_axis_name:
        data[f] = {
                      "values":[],
                      "color": colors[cnt]
                  }
        cnt += 1
        
    counter = 1.
    for query_data in query_results['data']:
        util.rewrite_output_line("[%d %%] Plotting %s" % (int(counter / query_results['total'] * 100), query_data['entry_info.alias']))
        counter += 1
        
        for index in range(len(fields)):
            value = query_data[fields[index]]
            
            if not isinstance(value, list):
                value = [value]
            if value[0] is None:
                util.cti_plugin_print_warning("NULL value found for field {0} in entry {1}".format(fields[index], query_data['entry_info.alias']))
                data[x_axis_name[index]]["values"].append(float(0))
            else:
                try:
                    data[x_axis_name[index]]["values"].append(float(value[0]))
                except ValueError:
                    util.cti_plugin_print_error("Invalid data type for bargraph: {0} in field {1}.\n".format(type(value[0],fields[index])))
                    return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        tmp_label = util_uid.uid_visualization(query_data['entry_info.entry_uid'], cti.CTR_ENTRY_DATA) 
        extxt = re.search(regexp_field, tmp_label, re.IGNORECASE)
        try:
             label.append(extxt.group(1)) 
        except:
             label.append(tmp_label)
    
    N = len(label) # len(label) != len(array_uid) if there are corrupted entries
    ind = np.arange(N * 2, step=2)  # the x locations for the groups
    width = 0.35       # the width of the bars
    
    fig = plt.figure(figsize=(16,12))
    ax = fig.add_subplot(111)
    
    ind_tmp = ind
    rects = []
    
    for f in x_axis_name:
        rects.append(ax.bar(ind_tmp, tuple(data[f]["values"]), width, color=data[f]["color"]))
        ind_tmp = ind_tmp + width + 0.05
    
    ax.set_ylabel('Counts')
    
    now = datetime.datetime.now()
    date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
    ax.set_title(str(title) + ( " / produced by CTI (%s)" % (date)))
    ax.set_xticks(ind+width)
    ax.set_xticklabels(tuple(label), rotation='vertical')
    
    try:
        ax.legend(tuple([r[0] for r in rects]), tuple(x_axis_name))
    except:
        util.cti_plugin_print_error("Error while drawing the bars.")
        return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    
    def autolabel(rects):
        # attach some text labels
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x()+rect.get_width()/2., height * 1.05 + 1, '%d'%int(height),
                    ha='left', va='bottom', rotation="vertical")
            
    if display_label:
        for r in rects:
            autolabel(r)
            
    plt.savefig(filename, bbox_inches='tight', pad_inches = 0.0)
    print "Done!"
    
#---------------------------------------------------------------------------
