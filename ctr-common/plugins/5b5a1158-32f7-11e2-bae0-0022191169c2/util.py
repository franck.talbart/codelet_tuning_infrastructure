#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi import plugin, util_uid, entry, util

import cti, ctr

import sys, json, copy, os


#---------------------------------------------------------------------------

def get_data(self, params):
    query_uid = params["query_uid"]
    repository_query = str(params["repository_query"])
    if repository_query not in [cti.COMMON_REPOSITORY, cti.TEMP_REPOSITORY, cti.ALL_REPOSITORY]:
        repository_query = util_uid.CTI_UID(repository_query, cti.CTR_ENTRY_REPOSITORY)
    if repository_query is None:
        util.cti_plugin_print_error("Unknown repository '{0}'.".format(params['repository_query']))
        exit(cti.CTI_ERROR_INVALID_ARGUMENT)
    query = params["query"]
    type_query = params["type_query"]
    uid_list = params["uid_list"]
    
    #Getting fields:
    fields=[]
    if 'fields' in params:
        fields = params['fields']
    else:
        fields = [params['field']]
    
    used_fields = copy.deepcopy(fields)
    
    if not "entry_info.entry_uid" in used_fields:
        used_fields.append("entry_info.entry_uid")
    if not "entry_info.alias" in used_fields:
        used_fields.append("entry_info.alias")
    
    #Repository
    repos = "" 
    if util_uid.is_valid_uid(repository_query):
        repos = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(util_uid.CTI_UID(str(repository_query), cti.CTR_ENTRY_REPOSITORY))
    elif repository_query == cti.COMMON_REPOSITORY:
        repos = ctr.ctr_plugin_get_common_dir()
    elif repository_query == cti.TEMP_REPOSITORY:
        repos = ctr.ctr_plugin_get_temp_dir()
    elif repository_query == cti.LOCAL_REPOSITORY:
        dot_ctr = cti.cti_plugin_config_get_value(cti.DOT_CTR)
        path = os.path.join(os.getcwd(), dot_ctr)
        if not os.path.exists(path):
           util.cti_plugin_print_error("Local repository not found.")
           exit(cti.CTI_PLUGIN_ERROR_LOCAL_REP_DOESNT_EXISTS)
        repos = path
    elif repository_query != cti.ALL_REPOSITORY:
        util.cti_plugin_print_error("The repository type is wrong!")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    query_result = {}
    print "Retrieving the entries..."
    if(uid_list):
        overflow_limit = 80
        first_time = True
        processed_entries = 0
        while(uid_list):
            #Add the specified UIDs to the query
            query_add = "entry_uid:({0})".format(' OR '.join([str(x) for x in uid_list[:overflow_limit]]))
            processed_entries += min(overflow_limit, len(uid_list))
            uid_list = uid_list[overflow_limit:]
            if first_time:
                if not query or query == "*":
                    query = query_add
                else:
                    if repos:
                        query = '({0}) AND entry_info.path_repository:{1}'.format(query, repos)
                        repository_query = 'all'
                    query = '({0}) OR {1}'.format(query, query_add)
            else:
                query = query_add
            
            #Determine the uid type
            if not type_query:
                type_query = entry.load_data_info(uid_list[0])[cti.DATA_INFO_PLUGIN_UID]
            
            if query and query.strip() != "":
                json_file = \
                {
                    "select": 
                    {
                        "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                        "params": 
                        [
                            {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_query},
                            {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                            {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : type_query},
                            {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                            {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : used_fields},
                            {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : False},
                            {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1}
                        ]
                    }
                }
                result = ""
                try:
                    result = plugin.execute_plugin_by_file(query_uid, json_file, self.username,
                                                                    self.password)
                    if not query_result:
                        query_result = json.loads(result)
                    else:
                        added_result = json.loads(result)
                        query_result['data'] += added_result['data']
                        query_result['total'] += added_result['total']
                except:
                    print sys.exc_info()[1]
                    exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            if first_time:
                processing_total = len(uid_list) + query_result['total']
                processed_entries = query_result['total']
                first_time = False
            print 'Retrieved {0}/{1} entries'.format(processed_entries, processing_total)
            
    else:
        if query and query.strip() != "":
            json_file = \
            {
                "select": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_query},
                        {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                        {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : type_query},
                        {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                        {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : used_fields},
                        {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : False},
                        {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1}
                    ]
                }
            }
            result = ""
            try:
                result = plugin.execute_plugin_by_file(query_uid, json_file, self.username,
                                                                self.password)
                if not query_result:
                    query_result = json.loads(result)
                else:
                    added_result = json.loads(result)
                    query_result['data'] += added_result['data']
                    query_result['total'] += added_result['total']
            except:
                print sys.exc_info()[1]
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        
    if not (query_result and len(query_result['data'])):
        util.cti_plugin_print_error("No data to extract")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    final_result = copy.deepcopy(dict([(x,query_result[x]) for x in query_result if x != 'data']))
    
    final_result['data'] = []
    #post-processing of matrix fields
    if 'matrix_fields' in final_result:
        existing_uids = {}
        final_result['total'] = len(query_result['data'])
        for data in query_result['data']:
            if data['entry_info.entry_uid'] in existing_uids:
                final_result['total'] -= 1
                for matrix_field in final_result['matrix_fields']:
                    final_result['data'][existing_uids[data['entry_info.entry_uid']]][matrix_field].append(data[matrix_field])
            else:
                existing_uids[data['entry_info.entry_uid']] = len(final_result['data'])
                for matrix_field in final_result['matrix_fields']:
                    data[matrix_field] = [data[matrix_field]]
                final_result['data'].append(copy.deepcopy(data))
    else:
        final_result['data'] = copy.deepcopy(query_result['data'])
        
    return final_result

#---------------------------------------------------------------------------

