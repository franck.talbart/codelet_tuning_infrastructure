#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi.main import HapiPlugin, hapi_command

import sys, matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

import bargraph, histogram, piechart

class PlotPlugin(HapiPlugin):
    @hapi_command("histogram")
    def histogram(self, params):
        """ Create a PNG file containing a histogram
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        histogram.histogram(self, params)
    
    #---------------------------------------------------------------------------
    @hapi_command("bargraph")
    def bargraph(self, params):
        """ Create a PNG file containing a histogram
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        bargraph.bargraph(self, params)
    
    #---------------------------------------------------------------------------
    @hapi_command("piechart")
    def piechart(self, params):
        """ Create a PNG file containing a histogram
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        piechart.piechart(self, params)
    
#---------------------------------------------------------------------------
# main function
if __name__ == "__main__":
    p = PlotPlugin()
    exit(p.main(sys.argv))
