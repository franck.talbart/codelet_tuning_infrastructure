#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi import util_uid, util

import cti

import datetime, pylab

import util as util_p
#---------------------------------------------------------------------------
def piechart(self, params):
    """ Create a PNG file containing a pie chart
    
    Args:
        self: class of the plugin
        params: working parameters
    
    Returns:
      Nothing
    """
    
    filename = params["filename"]
    field = params["field"]
    title_p = params["title"]
    is_number = params["is_number"]
    
    query_results = util_p.get_data(self, params)
    
    result = {}
    counter = 1.
    for data in query_results['data']:
        util.rewrite_output_line("[%d %%] Plotting %s" % (int(counter / query_results['total'] * 100), data['entry_info.alias']))
        counter +=1
        value = data[field]
        if not isinstance(value, list):
            value = [value]
            
        for v in value:
            if result.has_key(v):
                result[v] += 1
            else:
                result[v] = 1
    
    # Converting to %
    total = sum(result.values())
    for e in result:
        result[e] = result[e] / float(total) * 100
        
    # make a square figure and axes
    pylab.figure(1, figsize=(6,6))
    
    # The slices will be ordered and plotted counter-clockwise.
    labels = tuple([util_uid.uid_visualization(k, cti.CTR_ENTRY_DATA) if util_uid.is_valid_uid(k) else k for k in result.keys()])
    fracs = result.values()
    explode = tuple([0] * len(labels))
    
    # If lables are numbers: let's sort them
    if is_number:
        cpt = 0
        ordered = []
        not_ordered = []
        for l in labels:
            try:
                ordered.append((int(l),fracs[cpt]))
            except:
                not_ordered.append((str(l), fracs[cpt]))
            cpt += 1
            
        ordered.sort(cmp=lambda x,y: cmp(x[0], y[0]))
        ordered += not_ordered
        labels = tuple([str(x) for (x,y) in ordered])
        fracs = tuple([y for (x,y) in ordered])
    
    pylab.pie(fracs, explode=explode, labels=labels,
                    autopct='%1.1f%%', shadow=True, 
                    colors=('g', 'r', 'c', 'm', 'y', 'b', 'w'))
    
    now = datetime.datetime.now()
    date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
    
    pylab.title(title_p + ( " / produced by CTI (%s)" % (date)))
    pylab.savefig(filename, bbox_inches='tight')
    print "Done!"
    
#---------------------------------------------------------------------------
