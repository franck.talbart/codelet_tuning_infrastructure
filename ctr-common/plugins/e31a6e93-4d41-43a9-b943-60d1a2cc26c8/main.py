#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import entry, plugin, alias, description, submitter, util
from cti_hapi.main import HapiPlugin, hapi_command

import os, sys, tempfile, datetime, tarfile, shutil, glob

class CompilePlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ prepare parameters
        
        Args:
            self: class of the plugin
            params: working parameters
        """
        self.work_params = description.description_write(self.command, self.work_params)
        application_uid  = self.work_params[self.command].params["application"][cti.META_ATTRIBUTE_VALUE]
        mode = self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE]
        compiled_application = self.work_params[self.command].params["compiled_application"][cti.META_ATTRIBUTE_VALUE]
        binary_entry = self.work_params[self.command].params["binary"][cti.META_ATTRIBUTE_VALUE]
        auto_run = self.work_params[self.command].params["auto_run"][cti.META_ATTRIBUTE_VALUE]
        format_o = self.work_params[self.command].params["format"][cti.META_ATTRIBUTE_VALUE]
        platform = self.work_params[self.command].params["platform"][cti.META_ATTRIBUTE_VALUE]
        
        # check parameters
        if not application_uid :
            util.cti_plugin_print_error("The application entry is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if not mode:
            mode = "local"
            self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE] = mode
        elif mode not in submitter.MODE_AVAIL:
            util.cti_plugin_print_error("Unknown mode.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        if mode != "local" and mode != "other" and not platform:
            util.cti_plugin_print_error("You must provide a platform on non-local mode.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if compiled_application and not os.path.isdir(os.path.abspath(compiled_application)):
            util.cti_plugin_print_error("The compiled application is not a directory or does not exist!")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        tar_compiled_dir = None
        if compiled_application:
            tar_compiled_dir = tar_and_add_file(compiled_application, "compiled_application")
            self.work_params[self.command].params["compiled_application"][cti.META_ATTRIBUTE_VALUE] = tar_compiled_dir
        
        # create the alias
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        
        application_alias = alias.get_data_alias(application_uid)
        if not application_alias:
            application_alias = "Unamed_application"
        alias_e = "compile_%s_%s" % (application_alias, date)
        
        (data_entry, output) = self.default_init_command(params, alias_e = alias_e, no_output = True)
        if binary_entry:
            entry.update_entry_parameter(binary_entry, {"compile": {"value": data_entry.uid}})
        
        if tar_compiled_dir:
            shutil.rmtree(os.path.dirname(tar_compiled_dir))
        res = plugin.plugin_auto_run(self, data_entry, format_o, auto_run, output)
        return res
    
    #---------------------------------------------------------------------------
    
    @hapi_command("run")
    def run_cmd(self, params):
        """ Compile an entry
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
            Nothing
        """
        
        # load data from compile
        (entry_input, entry_output) = entry.load_data(params["entry"])
        
        mode = entry_output["init"].params["mode"][cti.META_ATTRIBUTE_VALUE]
        platform = entry_output["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
        prefix_execute_script = entry_output["init"].params["prefix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        suffix_execute_script = entry_output["init"].params["suffix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        submitter_cmd = entry_output["init"].params["submitter_cmd"][cti.META_ATTRIBUTE_VALUE]
        submitter_opt = entry_output["init"].params["submitter_opt"][cti.META_ATTRIBUTE_VALUE]
        submitter_user = entry_output["init"].params["submitter_user"][cti.META_ATTRIBUTE_VALUE]
        
        application_uid = entry_output["init"].params["application"][cti.META_ATTRIBUTE_VALUE]
        binary_name = entry_input["init"].params["binary_name"][cti.META_ATTRIBUTE_VALUE]
        if not binary_name:
            binary_name = "wrapper"
        
        (_, output_application) = entry.load_data(application_uid)
        path_files = output_application["init"].params["directory"][cti.META_ATTRIBUTE_VALUE]
        
        #compile_command
        compile_command = ""
        
        make = entry_output["init"].params["make"][cti.META_ATTRIBUTE_VALUE]
        compiler_uid = entry_output["init"].params["compiler"][cti.META_ATTRIBUTE_VALUE]
        if make != "":
            make_opt = entry_output["init"].params["make_opt"][cti.META_ATTRIBUTE_VALUE]
            if make_opt == None :
                compile_command = make
            else :
                compile_command = make + " " + make_opt
        elif compiler_uid != "" and compiler_uid is not None:
            (_, out_compiler) = entry.load_data(compiler_uid)
            compiler_name = out_compiler.params["compiler_name"][cti.META_ATTRIBUTE_VALUE]
            if not compiler_name:
                util.cti_plugin_print_error("The compiler name is empty.")
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            compile_opt = entry_output["init"].params["compile_opt"][cti.META_ATTRIBUTE_VALUE]
            compile_command = compiler_name + " " + compile_opt
        else:
            util.cti_plugin_print_error("In compile plugin, no compile command given.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        #partition
        partition = entry_output["init"].params["partition"][cti.META_ATTRIBUTE_VALUE]
        
        # create the arguments
        work_dir = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)
        
        dict_opt = {}
        dict_opt["APP"] = [path_files]
        dict_opt["SCRIPTS"] = []
        if params["prefix"]:
            shutil.copy(params["prefix"], "/tmp/prefix.sh")
            dict_opt["SCRIPTS"].append("/tmp/prefix.sh")
        if params["suffix"]:
            shutil.copy(params["suffix"], "/tmp/suffix.sh")
            dict_opt["SCRIPTS"].append("/tmp/suffix.sh")
        dict_opt["platform"] = platform
        dict_opt["entry"] = params["entry"]
        dict_opt["plugin_execute_script"] = os.path.join(self.plugin_directory, "execute_compile.sh")
        dict_opt["partition"] = partition
        dict_opt["submitter_cmd"] = submitter_cmd
        dict_opt["submitter_opt"] = submitter_opt
        dict_opt["submitter_user"] = submitter_user
        dict_opt["prefix_execute_script"] = prefix_execute_script
        dict_opt["suffix_execute_script"] = suffix_execute_script
        dict_opt["username"] = self.username
        
        # create the script parameters file
        dict_opt["script_params"] = tempfile.mkstemp(dir=work_dir)[1]
        script_params_f = open(dict_opt["script_params"], "w")
        script_params_f.write(binary_name + "\n")
        script_params_f.write(compile_command + "\n")
        script_params_f.close()
        
        (job_dir, daemon_log_file) = submitter.submitter(dict_opt, self.plugin_uid, mode)
        
        execution_log_file = None
        # collect execution log file
        for f in glob.glob("*.out"):
            if f != daemon_log_file:
                execution_log_file = f

        file_daemon = open(daemon_log_file, "a")
        
        # general parameters for the import
        dict_opt = {}
        dict_opt["entry"] = params["entry"]
        
        message_daemon = ""
        binary_path = os.path.join("RESULTS", os.path.basename(binary_name))
        if not os.path.isfile(binary_path):
            execution_log_f = open(execution_log_file, "r")
            execution_log_message = execution_log_f.readlines()
            execution_log_f.close()
            execution_log_message = " ".join(execution_log_message)
            
            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] No binary generated\n\tThe error message is:\n\
                        =================================\n%s\n=================================\n\
                        Temporary files are located in the directory: %s" \
                        % (date, execution_log_message, os.getcwd())
            util.cti_plugin_print_error(message)
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()
            
            self.import_results(dict_opt)
        else:
            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] Importing binary into CTI.." % date
            print message
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()
            
            # import parameters
            dict_opt["compiled_app"] = os.path.join("RESULTS", "compiled_application.tar.gz")
            dict_opt["binary"] = binary_path
            dict_opt["binary_plugin_uid"] = params["binary_plugin_uid"]
            
            self.import_results(dict_opt)
            
            # cleaning temporary files
            shutil.rmtree(os.path.join(work_dir, job_dir + "_local"))
        
        
    #---------------------------------------------------------------------------
    
    def import_results(self, dict_opt):
        """ Import results
        
        Args:
            self: class of the plugin
            dict_opt: parameters
        Returns:
            Nothing
        """
        
        binary = dict_opt["binary"]
        binary_plugin_uid = dict_opt["binary_plugin_uid"]
        compiled_app = dict_opt["compiled_app"]
        compile_uid = dict_opt["entry"]
        
        # check parameters
        if not compile_uid:
            util.cti_plugin_print_error("You must provide the compile entry!")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        
        # import log files
        log_to_import = []
        for log_file in glob.glob("*.out"):
            try:
                log_f = entry.put_file_in_entry(
                                                compile_uid, 
                                                log_file, 
                                                False
                                               )
                log_to_import.append(log_f)
            except IOError:
                util.cti_plugin_print_error("No such file: '" + log_file + "'")
                exit(cti.CTI_PLUGIN_ERROR_IO)
        
        entry.update_entry_parameter(compile_uid, {"logs": {"value": log_to_import, "append": True}})
        
        if binary:
            # repository type
            repository = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, compile_uid)
            # if the repository is local, we need its UID
            if repository == cti.CTR_REP_LOCAL:
                repository_path = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(compile_uid, cti.CTR_DATA_DIR)
                repository = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(repository_path))
            
            # create the binary entry
            json_file = \
            {
                "init": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "init",cti.META_ATTRIBUTE_REP : repository},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "binary_file",cti.META_ATTRIBUTE_VALUE : binary},
                        {cti.META_ATTRIBUTE_NAME : "compile",cti.META_ATTRIBUTE_VALUE : compile_uid}
                    ]
                }
            }
            
            output_binary = ""
            uid_binary = ""
            try:
                output_binary = plugin.execute_plugin_by_file(binary_plugin_uid, json_file, self.username, self.password)
                uid_binary = plugin.get_output_data_uid(output_binary)
            except :
                util.cti_plugin_print_error("Error on the binary entry creation")
                print sys.exc_info()[1]
                uid_binary = ""
            
            entry.update_entry_parameter(compile_uid, {"binary": {"value":uid_binary}})
            
            if compiled_app:
                # import compiled entry
                try:
                    entry_file = entry.put_file_in_entry(
                                    compile_uid, 
                                    compiled_app, 
                                    False
                                   )
                    
                    entry.update_entry_parameter(compile_uid, {"compiled_application": {"value": entry_file}})
                except IOError:
                    util.cti_plugin_print_error("No such file: '" + compiled_app + "'")
                    exit(cti.CTI_PLUGIN_ERROR_IO)
        
        date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
        print "[%s] End of process." % date
    
    #---------------------------------------------------------------------------
    
    @hapi_command("update")
    def update_cmd(self, params):
        """ Update a compile entry
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
            Nothing
        """
        
        update_params = self.work_params[self.command].params
        
        param_name = None
        compiled_dir = None
        tar_compiled_dir = None        
       
        if update_params["compiled_application"].has_key(cti.META_ATTRIBUTE_VALUE) and \
            update_params["compiled_application"][cti.META_ATTRIBUTE_VALUE]:
            compiled_dir = update_params["compiled_application"][cti.META_ATTRIBUTE_VALUE]
            param_name = "compiled_application"
        
        if param_name is not None:
            if compiled_dir is None:
                self.work_params[self.command].params[param_name][cti.META_ATTRIBUTE_VALUE] = None
            elif os.path.isdir(os.path.abspath(compiled_dir)):
                tar_compiled_dir = tar_and_add_file(compiled_dir, param_name)
                self.work_params[self.command].params[param_name][cti.META_ATTRIBUTE_VALUE] = tar_compiled_dir
            else:
                util.cti_plugin_print_error("'%s' is not a directory or does not exist!" % compiled_dir)
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        output = description.update_entry(self, params)
        
        if tar_compiled_dir:
            shutil.rmtree(os.path.dirname(tar_compiled_dir))
        
        return output

#---------------------------------------------------------------------------

def tar_and_add_file(compiled_dir, param_name):
    """ Compress and add file into the entry
        
        Args:
            compiled_dir: directory
            param_name: the parameter name for the file
        
        Returns:
            the compressed file path
    """
    if compiled_dir is not None:
        # convert relative paths to absolute paths
        compiled_dir_abspath = os.path.abspath(compiled_dir)
        
        # compress the compiled files
        tmp_dir = tempfile.mkdtemp()
        work_dir = os.getcwd()
        os.chdir(compiled_dir_abspath)
        compiled_files = os.listdir(".")
        tar_compiled_dir = os.path.join(tmp_dir, param_name) + ".tar.gz"
        tar_file = tarfile.open(tar_compiled_dir, "w:gz")
        for compiled_file in compiled_files:
            tar_file.add(compiled_file)
        tar_file.close()
        sys.stdout.flush()
        os.chdir(work_dir)
        return tar_compiled_dir
    return None

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = CompilePlugin()
    exit(p.main(sys.argv))
