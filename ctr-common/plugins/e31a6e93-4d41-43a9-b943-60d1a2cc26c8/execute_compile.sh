#!/bin/bash --norc
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

# This script compiles a source file on a given remote computer.

#Constants
script_params="script_params"
work_dir=`pwd`

echo "[`date +"%Y/%m/%d %H:%M:%S"`] Begin execution on #`uname -n`"

binary=
compile_command=
#Read script parameters
i=0
while read line; do
    if [ $i -eq 0 ]; then
        binary=$line
        let "i++"
    elif [ $i -eq 1 ]; then
        compile_command=$line
        let "i++"
    fi
done < $script_params

source /etc/profile
module &>/dev/null
if [ $? -eq 0 ]; then
    echo "Load Intel compilers."
    module load compilers/intel
    echo "Load Intel mpi libraries."
    module load libraries/impi
fi
#For icc
export LANG=C

#Go as far as you can as long as there is one element
cd $work_dir/APP

while [ `ls -1 -a | grep -v -E "(\.|(\.\.))$"|wc -l` -eq 1 ]; do
    dir=`ls -1 -a | grep -v -E "(\.|(\.\.))$"`
    cd $dir
    if [ $? -ne 0 ]; then
        echo "Error: the application is corrupted"
        exit -1
    fi
done

#process prefix script
if [ -e $work_dir/SCRIPTS/prefix.sh ]; then
    source $work_dir/SCRIPTS/prefix.sh
fi

make clean &> /dev/null
#Use the compile command to compile sources
eval $compile_command

#process suffix script
if [ -e $work_dir/SCRIPTS/suffix.sh ]; then
    source $work_dir/SCRIPTS/suffix.sh
fi

#Compress and move binary in the results directory
if [ -e $binary ]; then
    mv $binary $work_dir/RESULTS/
else
    echo "[`date +"%Y/%m/%d %H:%M:%S"`] ERROR: binary \"$binary\" does not exist!" 1>&2
fi

#Compress and move entry files in the results directory
rm -f *.o
cd $work_dir/APP/*
tar acf $work_dir/RESULTS/compiled_application.tar.gz *

echo "[`date +"%Y/%m/%d %H:%M:%S"`] End of compilation"
