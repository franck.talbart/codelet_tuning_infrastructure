#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

import cti

from cti_hapi import entry, alias, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, json

class DatabaseTemplatePlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Add some raw data to CTR
        """
        format_o = params["format"]
        
        output = {}
        if (not params["json"]) and (not params["directory"]) and (not params["file"]):
            util.cti_plugin_print_error("Will not create empty entry, please provide "
                         "json data (--json=<>) or/and a directory "
                         "files (--directory=<>) or/and a "
                         "file (--file=<>) to upload.\n")
            exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        
        # Parse json data if provided (check if it is valid)
        if params["json"]:
            try:
                f = file(params["json"])
                jd = json.loads(f.read())
                f.close()
            except Exception, e:
                util.cti_plugin_print_error("JSON data is invalid: ({0})\n".format(e))
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        else:
            jd = {}
        
        # create the new entry
        rep = self.work_params["init"].attributes[cti.META_ATTRIBUTE_REP]
        
        data_entry = entry.create_data_entry(self.plugin_uid, rep, self.username)
        if data_entry is None:
            util.cti_plugin_print_error("Can't create the entry.")
            return cti.CTI_PLUGIN_ERROR_IO
        
        output["data"] = jd
        output["files"] = []
        
        name = ""
        
        # Should we upload a directory?
        if params["directory"]:
            files = entry.put_dir_in_entry(data_entry.uid, params["directory"])
            output["files"] = files
            name = os.path.basename(params["directory"])
        
        # Should we upload a file?
        if params["file"]:
            files = entry.put_file_in_entry(
                        data_entry.uid, 
                        params["file"], 
                        True
                    )
            
            output["files"] += [files]
            name = os.path.basename(params["file"])
        
        uid = self.record_entry("init", output, data_entry=data_entry, format_o=format_o)
        
        if name != "":
            if alias.set_data_alias(uid, name) == 0:
                util.cti_plugin_print_warning("Cannot set the alias %s (already used?)" % (name))
#---------------------------------------------------------------------------

    @hapi_command("get")
    def get_cmd(self, params):
        info = entry.load_data_info(params["uid"])
        if info[cti.DATA_INFO_PLUGIN_UID] != str(self.plugin_uid):
            util.cti_plugin_print_error("This UID was not created by this plugin.")
            exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        
        (inp, out) = entry.load_data(params["uid"])
        
        # dump the json data
        print json.dumps(out["init"].params["data"][cti.META_ATTRIBUTE_VALUE], indent=2, ensure_ascii=False)
        
        # if directory provided, copy it to the cwd
        if cti.META_ATTRIBUTE_VALUE in inp["init"].params["directory"] or cti.META_ATTRIBUTE_VALUE in inp["init"].params["file"]:
            if os.path.exists(str(params["uid"])):
                util.cti_plugin_print_error("Retrieving the files would overwrite directory"
                            " ./{0}. Aborting!".format(params["uid"]))
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            entry.get_dir_from_entry(params["uid"], str(params["uid"]))
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = DatabaseTemplatePlugin()
    exit(p.main(sys.argv))
