#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import util, cti_json, alias, database, database_manager, types
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, stat, shutil, json, csv

def create_ctr_plugin_info(uid_plugin, category_uid, path_repository):
    """
    creates a new plugin directory and populates its ctr_info.txt
    file.
    """
    record = {  cti.PLUGIN_INFO_BUILD_NUMBER : "1",
                cti.PLUGIN_INFO_CATEGORY : str(category_uid),
                cti.PLUGIN_INFO_DESCRIPTION :
                "Automatic plugin generated with database",
                cti.PLUGIN_INFO_AUTHOR : "CTI",
                cti.PLUGIN_INFO_DEPENDENCY : ":"
             }
    
    path = os.path.join(path_repository,
                        cti.cti_plugin_config_get_value(cti.CTR_PLUGIN_DIR),
                        str(uid_plugin))
    os.mkdir(path)
    
    ctr_plugin_info = ctr.ctr_plugin_info_create()
    for key, value in record.iteritems():
        ctr.ctr_plugin_info_put_value(ctr_plugin_info,
                                      key,
                                      value)
    
    path_ctr_plugin_info = os.path.join(
                                path,
                                cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME)
                           )
    ctr.ctr_plugin_info_record_in_file(ctr_plugin_info, path_ctr_plugin_info)
    return path
#---------------------------------------------------------------------------

def create_plugin(alias_p, template_dir, plugin_directory, category_uid, repository):
    if alias.get_plugin_uid(alias_p):
        util.cti_plugin_print_error("This alias already exists, please choose a new one.")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    # If the provided alias is ok
    uid_plugin = cti.cti_plugin_generate_uid()
    path_repository = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(repository)
    path = create_ctr_plugin_info(uid_plugin, category_uid, path_repository)
    if alias.set_plugin_alias(uid_plugin, alias_p, path_repository) == 0:
        util.cti_plugin_print_warning("Cannot set the alias %s (already used)" % (alias_p))
    
    # copy template files
    for f in ["main.py",
              "ctr_input_default.json",
              "ctr_output_default.json",
              "run_plugin.sh"
              ]:
        ori = os.path.join(plugin_directory,
                           "template", 
                           template_dir,
                           f)
        des = os.path.join(path,f)
        shutil.copyfile(ori, des)
    
    # make run_plugin executable
    os.chmod(os.path.join(path,"run_plugin.sh"), stat.S_IEXEC | stat.S_IREAD)
    return (uid_plugin, path)
#---------------------------------------------------------------------------

class DatabasePlugin(HapiPlugin):
    @hapi_command("new_schema")
    def schema_cmd(self, params):
        alias_p = params["alias_schema"]
        category_uid = params["category_uid"]
        repository = params["repository"]
        if not repository:
            util.cti_plugin_print_error("Cannot find repository")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
                
        uid_plugin, path = create_plugin(
            alias_p, 
            "template_new_schema", 
            self.plugin_directory, 
            category_uid,
            repository
        )
        uid_plugin = str(uid_plugin)
        #Adding the new plugin to the cti database
        plugin_infos = database_manager.list_plugins()
        database_manager.create_plugin_schema(database.Database(), uid_plugin, plugin_infos)
        
        if params["format"] == "json":
            result = {
                'new_uid': uid_plugin,
                'type': cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID
            }
            print cti_json.cti_json_encode(result)
        
        elif (params["format"] == "txt"):
            print "Created new database schema <{0}>.".format(alias_p)
            print "Hint: to get instructions, type $ cti {0} help".format(alias_p)
        else:
            util.cti_plugin_print_error("Unknown format " + str(params["format"]))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------
    
    @hapi_command("from_json")
    def from_json_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        format_o = params["format"]
        alias_p = params["alias_schema"]
        json_i = params["json"]
        repository = params["repository"]
        if not repository:
            util.cti_plugin_print_error("Cannot find repository")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        
        category_uid = params["category_uid"]
        uid_plugin, path = create_plugin(alias_p,
                                         "template_from_json",
                                         self.plugin_directory, 
                                         category_uid, repository)
        uid_plugin = str(uid_plugin)
        
        # Update the input and output default files
        
        try:
           input_file = cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_INPUT_FILENAME)
           output_file = cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_OUTPUT_FILENAME)
           
           if not input_file or not output_file:
              util.hapi_fail("Can't get value from config file")
              
           input_file = os.path.join(path, input_file)
           output_file = os.path.join(path, output_file)
           
        except OSError, e:
              util.cti_fail("Failed to get the value from the config file: %s" % e)
              
        f_input = open(input_file)
        
        j_in = json.load(f_input)
        f_input.close()
        j_in["init"]["params"] = json_i
        
        with open(output_file, 'wb') as outfile:
            json.dump(j_in, outfile, indent=4)
        
        j_in["init"]["params"].insert(0,
                                        {
                                            "type": "TEXT", 
                                            "name": "alias",
                                            "desc": "Alias", 
                                            "long_desc": "The alias of the entry to create"
                                        }
                                     )
                
        j_in["init"]["params"].append(
                                      {
                                        "type": "TEXT", 
                                        "name": "format", 
                                        "value": "txt", 
                                        "desc": "Format", 
                                        "long_desc": "The format of the output. 'txt' will display the output in a human readable manner. 'json' will output a JSON string containing the same Information.",
                                        "optional": True
                                      }
                                     )
       
        with open(input_file, 'wb') as infile:
            json.dump(j_in, infile, indent=4)
        
        #Adding the new plugin to the cti database
        plugin_infos = database_manager.list_plugins();
        database_manager.create_plugin_schema(database.Database(), uid_plugin, plugin_infos)
        
        if format_o == "json":
            result = {
                'new_uid': uid_plugin,
                'type': cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID
            }
            print cti_json.cti_json_encode(result)
        
        elif (params["format"] == "txt"):
            print "Created new database schema <{0}>.".format(alias_p)
            print "Hint: to get instructions, type $ cti {0} help".format(alias_p)
            print "PLUGIN_CTI_UID=" + uid_plugin
        else:
            util.cti_plugin_print_error("Unknown format " + str(params["format"]))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------

    @hapi_command("from_csv")
    def from_csv_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        filename = params["file"]
        table_alias = params["table_alias"]
        category_uid = params["category_uid"]
        separator = params["separator"]
        repository = params["repository"]
        
        if not repository:
            util.cti_plugin_print_error("Cannot find repository")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        try:
            csv_file = open(filename, 'r')
        except:
            util.cti_plugin_print_error("File not found: " + filename)
            return cti.CTI_ERROR_CANT_OPEN_FILE
        
        csv_contents = csv.DictReader(csv_file, delimiter=separator)
        
        #Format the columns for CTI
        column_names = csv_contents.fieldnames
        for i in range(len(column_names)):
            column_names[i] = util.column_name_replace(column_names[i])
        
        #Precedence of types dict
        precedence_type = \
        {
            "None"                                          :99,
            cti.META_CONTENT_ATTRIBUTE_TYPE_DATE            :5,
            cti.META_CONTENT_ATTRIBUTE_TYPE_INTEGER         :4,
            cti.META_CONTENT_ATTRIBUTE_TYPE_FLOAT           :3,
            cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID        :2,
            cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID      :2,
            cti.META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID  :2,
            cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN         :1,
            cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT            :0,
            cti.META_CONTENT_ATTRIBUTE_TYPE_EMAIL           :0,
            cti.META_CONTENT_ATTRIBUTE_TYPE_URL             :0,
            cti.META_CONTENT_ATTRIBUTE_TYPE_FILE            :0
        }
        
        #Setting the maximum precedence for all columns.
        column_types = dict([(cname, "None") for cname in column_names]) 
        
        #Getting through the values to find the type of column by analysing the data 
        for csv_line in csv_contents:
            for cname in column_names:
                detected_type, _ = types.from_csv(csv_line[cname])
                if precedence_type[detected_type] < precedence_type[column_types[cname]]:
                    column_types[cname] = detected_type
        
        #If some values are still have None as a type, get it to text
        for cname in column_types:
            if column_types[cname] == "None":
                column_types[cname] = cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT
        #Adding columns to the schema.
        json_schema = \
        [{
            cti.META_ATTRIBUTE_TYPE: cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX,
            cti.META_ATTRIBUTE_NAME: 'contents_matrix',
            cti.META_ATTRIBUTE_DESC: 'CSV contents.',
            cti.META_ATTRIBUTE_LONG_DESC: 'The contents of the imported CSV.',
            cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES:column_names,
            cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES:[column_types[cname] for cname in column_names],
            cti.META_ATTRIBUTE_MATRIX_COLUMN_DESCS: column_names,
            cti.META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS: ['Fill long_desc for {0}'.format(cname) for cname in column_names]
        }]
        
        csv_file.close()
        
        params_json = {}
        params_json["format"] = params["format"]
        params_json["alias_schema"] = table_alias
        params_json["json"] = json_schema
        params_json["category_uid"] = category_uid
        params_json["repository"] = repository
        
        self.from_json_cmd(params_json) 
        
        return 0
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = DatabasePlugin()
    exit(p.main(sys.argv))
