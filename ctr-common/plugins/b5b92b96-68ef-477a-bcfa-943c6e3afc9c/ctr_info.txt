build_number = 1.0.0
category = 3ee01f51-b58c-4eec-b74c-c761cf2dbdf4
description = This plugin provides universal database capabilities in CTI and allows end-users to add any raw data in repositories.
authors = Intel Corporation, CEA, GENCI, and UVSQ
cti_dependence_build_number = 0.3.0:
