    { 
    "LOOP 1":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "28"
            },
        "DECODING":
            {
            "number of cycles for decoding": "28.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "28.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "43"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "43.00"
            },
        "TOTAL":
            {
            "BOUND L1": "43.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "160.66"
            },
        "DISPATCH":
            {
            "P0": "1",
            "P1": "11",
            "P2": "43",
            "P3": "11",
            "P4": "11",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "32.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.30",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.26"
        },
    "LOOP 2":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "26"
            },
        "DECODING":
            {
            "number of cycles for decoding": "26.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "54.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "30"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "30.00"
            },
        "TOTAL":
            {
            "BOUND L1": "30.00",
            "BOUND L2 MIN": "49.14",
            "BOUND L2 AVG": "50.58",
            "BOUND RAM": "147.03"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "12",
            "P2": "30",
            "P3": "6",
            "P4": "6",
            "P5": "2"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "16.50",
        "VECTORIZATION PREDICT L2": "24.48",
        "NBR INSTRUCTIONS PER CYCLE": "1.97",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 3":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "9"
            },
        "DECODING":
            {
            "number of cycles for decoding": "9.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "15.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "6"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "6.67"
            },
        "TOTAL":
            {
            "BOUND L1": "6.00",
            "BOUND L2 MIN": "21.34",
            "BOUND L2 AVG": "22.87",
            "BOUND RAM": "63.56"
            },
        "DISPATCH":
            {
            "P0": "6",
            "P1": "1",
            "P2": "6",
            "P3": "6",
            "P4": "6",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "3.00",
        "VECTORIZATION PREDICT L2": "14.39",
        "NBR INSTRUCTIONS PER CYCLE": "3.50",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 4":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "13"
            },
        "DECODING":
            {
            "number of cycles for decoding": "13.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "23.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "9"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "11.00"
            },
        "TOTAL":
            {
            "BOUND L1": "9.00",
            "BOUND L2 MIN": "30.76",
            "BOUND L2 AVG": "31.44",
            "BOUND RAM": "82.30"
            },
        "DISPATCH":
            {
            "P0": "9",
            "P1": "3",
            "P2": "9",
            "P3": "9",
            "P4": "9",
            "P5": "3"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.12",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "6.00",
        "VECTORIZATION PREDICT L2": "20.91",
        "NBR INSTRUCTIONS PER CYCLE": "3.67",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 5":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "17"
            },
        "DECODING":
            {
            "number of cycles for decoding": "17.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "29.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "21.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "20.94",
            "BOUND L2 AVG": "23.03",
            "BOUND RAM": "64.05"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "8",
            "P2": "6",
            "P3": "6",
            "P4": "6",
            "P5": "7"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.15",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "9.00",
        "VECTORIZATION PREDICT L2": "13.15",
        "NBR INSTRUCTIONS PER CYCLE": "2.56",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.33"
        },
    "LOOP 6":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "16"
            },
        "DECODING":
            {
            "number of cycles for decoding": "16.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "26.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "21.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "20.94",
            "BOUND L2 AVG": "23.03",
            "BOUND RAM": "64.05"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "8",
            "P2": "6",
            "P3": "6",
            "P4": "6",
            "P5": "7"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.15",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "9.00",
        "VECTORIZATION PREDICT L2": "13.15",
        "NBR INSTRUCTIONS PER CYCLE": "2.56",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.33"
        },
    "LOOP 7":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "19"
            },
        "DECODING":
            {
            "number of cycles for decoding": "19.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "31.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "21.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "102.27"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "8",
            "P2": "12",
            "P3": "12",
            "P4": "6",
            "P5": "7"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.83",
        "PACKED LOAD RATIO": "1.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "18.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.89",
        "NBR FP INSTRUCTIONS PER CYCLE": "2.67"
        },
    "LOOP 8":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "24"
            },
        "DECODING":
            {
            "number of cycles for decoding": "25.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "37.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "21.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "123.72"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "8",
            "P2": "17",
            "P3": "12",
            "P4": "6",
            "P5": "7"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.75",
        "PACKED LOAD RATIO": "0.09",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "18.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "3.17",
        "NBR FP INSTRUCTIONS PER CYCLE": "2.67"
        },
    "LOOP 9":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "13"
            },
        "DECODING":
            {
            "number of cycles for decoding": "13.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "25.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "9"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "11.00"
            },
        "TOTAL":
            {
            "BOUND L1": "9.00",
            "BOUND L2 MIN": "26.67",
            "BOUND L2 AVG": "28.50",
            "BOUND RAM": "82.97"
            },
        "DISPATCH":
            {
            "P0": "9",
            "P1": "3",
            "P2": "9",
            "P3": "9",
            "P4": "9",
            "P5": "3"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.12",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "6.00",
        "VECTORIZATION PREDICT L2": "21.35",
        "NBR INSTRUCTIONS PER CYCLE": "3.67",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 10":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "17"
            },
        "DECODING":
            {
            "number of cycles for decoding": "17.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "27.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "18.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "141.33"
            },
        "DISPATCH":
            {
            "P0": "9",
            "P1": "3",
            "P2": "18",
            "P3": "18",
            "P4": "9",
            "P5": "3"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.67",
        "PACKED LOAD RATIO": "0.67",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "18.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.33",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 11":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "21"
            },
        "DECODING":
            {
            "number of cycles for decoding": "22.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "36.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "23"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "23.00"
            },
        "TOTAL":
            {
            "BOUND L1": "23.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "161.81"
            },
        "DISPATCH":
            {
            "P0": "9",
            "P1": "3",
            "P2": "23",
            "P3": "18",
            "P4": "9",
            "P5": "3"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.58",
        "PACKED LOAD RATIO": "0.07",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "20.50",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.09",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.78"
        },
    "LOOP 12":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "9"
            },
        "DECODING":
            {
            "number of cycles for decoding": "9.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "15.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "6"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "6.67"
            },
        "TOTAL":
            {
            "BOUND L1": "6.00",
            "BOUND L2 MIN": "21.34",
            "BOUND L2 AVG": "22.87",
            "BOUND RAM": "63.56"
            },
        "DISPATCH":
            {
            "P0": "6",
            "P1": "1",
            "P2": "6",
            "P3": "6",
            "P4": "6",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "3.00",
        "VECTORIZATION PREDICT L2": "14.39",
        "NBR INSTRUCTIONS PER CYCLE": "3.50",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 13":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "15"
            },
        "DECODING":
            {
            "number of cycles for decoding": "17.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "29.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "17"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "17.00"
            },
        "TOTAL":
            {
            "BOUND L1": "17.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "122.19"
            },
        "DISPATCH":
            {
            "P0": "6",
            "P1": "1",
            "P2": "17",
            "P3": "12",
            "P4": "6",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.56",
        "PACKED LOAD RATIO": "0.09",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "14.50",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.88",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.71"
        },
    "LOOP 14":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "14"
            },
        "DECODING":
            {
            "number of cycles for decoding": "16.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "30.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "16"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "16.00"
            },
        "TOTAL":
            {
            "BOUND L1": "16.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "117.45"
            },
        "DISPATCH":
            {
            "P0": "6",
            "P1": "1",
            "P2": "16",
            "P3": "12",
            "P4": "6",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.58",
        "PACKED LOAD RATIO": "0.20",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "14.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.94",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.75"
        },
    "LOOP 15":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "25"
            },
        "DECODING":
            {
            "number of cycles for decoding": "25.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "49.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "30"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "30.00"
            },
        "TOTAL":
            {
            "BOUND L1": "30.00",
            "BOUND L2 MIN": "49.14",
            "BOUND L2 AVG": "50.58",
            "BOUND RAM": "147.03"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "12",
            "P2": "30",
            "P3": "6",
            "P4": "6",
            "P5": "2"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "16.50",
        "VECTORIZATION PREDICT L2": "24.48",
        "NBR INSTRUCTIONS PER CYCLE": "1.97",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 16":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "22"
            },
        "DECODING":
            {
            "number of cycles for decoding": "22.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "44.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "30"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "30.00"
            },
        "TOTAL":
            {
            "BOUND L1": "30.00",
            "BOUND L2 MIN": "30.72",
            "BOUND L2 AVG": "33.15",
            "BOUND RAM": "147.03"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "12",
            "P2": "30",
            "P3": "6",
            "P4": "6",
            "P5": "2"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.90",
        "PACKED LOAD RATIO": "0.90",
        "PACKED STORE RATIO": "1.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.00",
        "NBR FP INSTRUCTIONS PER CYCLE": "2.00"
        },
    "LOOP 17":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "44"
            },
        "DECODING":
            {
            "number of cycles for decoding": "49.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "77.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "60"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "60.00"
            },
        "TOTAL":
            {
            "BOUND L1": "60.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "287.19"
            },
        "DISPATCH":
            {
            "P0": "19",
            "P1": "12",
            "P2": "60",
            "P3": "12",
            "P4": "6",
            "P5": "2"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.62",
        "PACKED LOAD RATIO": "0.05",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "48.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.63",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 18":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "3.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "7.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "4"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "4.67"
            },
        "TOTAL":
            {
            "BOUND L1": "4.00",
            "BOUND L2 MIN": "5.55",
            "BOUND L2 AVG": "5.79",
            "BOUND RAM": "18.52"
            },
        "DISPATCH":
            {
            "P0": "4",
            "P1": "2",
            "P2": "3",
            "P3": "1",
            "P4": "1",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "2.00",
        "VECTORIZATION PREDICT L2": "3.56",
        "NBR INSTRUCTIONS PER CYCLE": "2.75",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.25"
        },
    "LOOP 19":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "3.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "7.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "4"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "4.67"
            },
        "TOTAL":
            {
            "BOUND L1": "4.00",
            "BOUND L2 MIN": "5.55",
            "BOUND L2 AVG": "5.79",
            "BOUND RAM": "18.52"
            },
        "DISPATCH":
            {
            "P0": "4",
            "P1": "2",
            "P2": "3",
            "P3": "1",
            "P4": "1",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "2.00",
        "VECTORIZATION PREDICT L2": "3.56",
        "NBR INSTRUCTIONS PER CYCLE": "2.75",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.25"
        },
    "LOOP 20":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "21"
            },
        "DECODING":
            {
            "number of cycles for decoding": "21.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "27.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "20"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "20.00"
            },
        "TOTAL":
            {
            "BOUND L1": "20.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "106.23"
            },
        "DISPATCH":
            {
            "P0": "16",
            "P1": "4",
            "P2": "20",
            "P3": "4",
            "P4": "4",
            "P5": "2"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.77",
        "PACKED LOAD RATIO": "0.20",
        "PACKED STORE RATIO": "1.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "16.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.35",
        "NBR FP INSTRUCTIONS PER CYCLE": "2.00"
        },
    "LOOP 21":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "17"
            },
        "DECODING":
            {
            "number of cycles for decoding": "17.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "19.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "16"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "16.67"
            },
        "TOTAL":
            {
            "BOUND L1": "16.00",
            "BOUND L2 MIN": "20.90",
            "BOUND L2 AVG": "21.56",
            "BOUND RAM": "90.80"
            },
        "DISPATCH":
            {
            "P0": "16",
            "P1": "4",
            "P2": "16",
            "P3": "4",
            "P4": "4",
            "P5": "2"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.82",
        "PACKED LOAD RATIO": "0.50",
        "PACKED STORE RATIO": "1.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "16.00",
        "VECTORIZATION PREDICT L2": "16.00",
        "NBR INSTRUCTIONS PER CYCLE": "2.44",
        "NBR FP INSTRUCTIONS PER CYCLE": "2.50"
        },
    "LOOP 22":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "2.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "2.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "3"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "4.00"
            },
        "TOTAL":
            {
            "BOUND L1": "3.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "3",
            "P1": "2",
            "P2": "0",
            "P3": "0",
            "P4": "0",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "NO STORE",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.67",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 23":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "8"
            },
        "DECODING":
            {
            "number of cycles for decoding": "8.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "8.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "7"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "7.00"
            },
        "TOTAL":
            {
            "BOUND L1": "7.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "4",
            "P1": "3",
            "P2": "7",
            "P3": "3",
            "P4": "3",
            "P5": "3"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "3.00",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 24":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "11"
            },
        "DECODING":
            {
            "number of cycles for decoding": "11.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "11.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "12"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "12.00"
            },
        "TOTAL":
            {
            "BOUND L1": "12.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "5",
            "P1": "5",
            "P2": "12",
            "P3": "2",
            "P4": "2",
            "P5": "5"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.00",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 25":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "2.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "4.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "1"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "1.00"
            },
        "TOTAL":
            {
            "BOUND L1": "1.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "1",
            "P1": "1",
            "P2": "1",
            "P3": "1",
            "P4": "1",
            "P5": "0"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "5.00",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 26":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "2.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "4.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "1"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "1.00"
            },
        "TOTAL":
            {
            "BOUND L1": "1.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "1",
            "P1": "1",
            "P2": "1",
            "P3": "1",
            "P4": "1",
            "P5": "0"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "5.00",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 27":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "6"
            },
        "DECODING":
            {
            "number of cycles for decoding": "6.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "6.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "8"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "8.00"
            },
        "TOTAL":
            {
            "BOUND L1": "8.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "58.04"
            },
        "DISPATCH":
            {
            "P0": "1",
            "P1": "1",
            "P2": "8",
            "P3": "4",
            "P4": "4",
            "P5": "0"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.53",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "1.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "6.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.88",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 28":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "3.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "3.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "4"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "4.00"
            },
        "TOTAL":
            {
            "BOUND L1": "4.00",
            "BOUND L2 MIN": "7.16",
            "BOUND L2 AVG": "7.76",
            "BOUND RAM": "43.24"
            },
        "DISPATCH":
            {
            "P0": "1",
            "P1": "1",
            "P2": "4",
            "P3": "4",
            "P4": "4",
            "P5": "0"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.73",
        "PACKED LOAD RATIO": "1.00",
        "PACKED STORE RATIO": "1.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.75",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 29":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "9"
            },
        "DECODING":
            {
            "number of cycles for decoding": "10.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "10.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "9"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "9.00"
            },
        "TOTAL":
            {
            "BOUND L1": "9.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "5",
            "P1": "4",
            "P2": "9",
            "P3": "5",
            "P4": "5",
            "P5": "4"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.89",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 30":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "27"
            },
        "DECODING":
            {
            "number of cycles for decoding": "28.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "38.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "42"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "42.00"
            },
        "TOTAL":
            {
            "BOUND L1": "42.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "1",
            "P1": "1",
            "P2": "42",
            "P3": "17",
            "P4": "17",
            "P5": "0"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.57",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 31":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "30"
            },
        "DECODING":
            {
            "number of cycles for decoding": "30.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "30.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "42"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "42.00"
            },
        "TOTAL":
            {
            "BOUND L1": "42.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "1",
            "P1": "1",
            "P2": "42",
            "P3": "17",
            "P4": "17",
            "P5": "0"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.50",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 32":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "22"
            },
        "DECODING":
            {
            "number of cycles for decoding": "22.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "52.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "18.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "50.55",
            "BOUND L2 AVG": "53.85",
            "BOUND RAM": "162.15"
            },
        "DISPATCH":
            {
            "P0": "15",
            "P1": "1",
            "P2": "18",
            "P3": "15",
            "P4": "15",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "10.50",
        "VECTORIZATION PREDICT L2": "26.85",
        "NBR INSTRUCTIONS PER CYCLE": "2.78",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.83"
        },
    "LOOP 33":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "44"
            },
        "DECODING":
            {
            "number of cycles for decoding": "44.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "52.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "37"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "37.00"
            },
        "TOTAL":
            {
            "BOUND L1": "37.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "169.11"
            },
        "DISPATCH":
            {
            "P0": "27",
            "P1": "18",
            "P2": "37",
            "P3": "12",
            "P4": "12",
            "P5": "10"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.08",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "26.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.81",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.22"
        },
    "LOOP 34":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "39"
            },
        "DECODING":
            {
            "number of cycles for decoding": "39.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "53.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "28"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "32.33"
            },
        "TOTAL":
            {
            "BOUND L1": "28.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "130.18"
            },
        "DISPATCH":
            {
            "P0": "27",
            "P1": "18",
            "P2": "28",
            "P3": "10",
            "P4": "10",
            "P5": "16"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.14",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "20.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "3.57",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.61"
        },
    "LOOP 35":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "67"
            },
        "DECODING":
            {
            "number of cycles for decoding": "67.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "85.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "50"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "58.33"
            },
        "TOTAL":
            {
            "BOUND L1": "50.00",
            "BOUND L2 MIN": "78.74",
            "BOUND L2 AVG": "83.12",
            "BOUND RAM": "227.89"
            },
        "DISPATCH":
            {
            "P0": "50",
            "P1": "37",
            "P2": "35",
            "P3": "18",
            "P4": "18",
            "P5": "25"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.12",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "25.00",
        "VECTORIZATION PREDICT L2": "42.64",
        "NBR INSTRUCTIONS PER CYCLE": "3.20",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.74"
        },
    "LOOP 36":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "65"
            },
        "DECODING":
            {
            "number of cycles for decoding": "65.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "95.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "51"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "59.33"
            },
        "TOTAL":
            {
            "BOUND L1": "51.00",
            "BOUND L2 MIN": "76.21",
            "BOUND L2 AVG": "80.56",
            "BOUND RAM": "221.46"
            },
        "DISPATCH":
            {
            "P0": "51",
            "P1": "37",
            "P2": "35",
            "P3": "17",
            "P4": "17",
            "P5": "25"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.13",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "26.00",
        "VECTORIZATION PREDICT L2": "40.42",
        "NBR INSTRUCTIONS PER CYCLE": "3.12",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.71"
        },
    "LOOP 37":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "15"
            },
        "DECODING":
            {
            "number of cycles for decoding": "15.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "21.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "18.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "26.40",
            "BOUND L2 AVG": "27.90",
            "BOUND RAM": "87.06"
            },
        "DISPATCH":
            {
            "P0": "4",
            "P1": "12",
            "P2": "18",
            "P3": "6",
            "P4": "6",
            "P5": "4"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "12.00",
        "VECTORIZATION PREDICT L2": "15.24",
        "NBR INSTRUCTIONS PER CYCLE": "1.78",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.67"
        },
    "LOOP 38":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "63"
            },
        "DECODING":
            {
            "number of cycles for decoding": "63.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "91.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "50"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "58.33"
            },
        "TOTAL":
            {
            "BOUND L1": "50.00",
            "BOUND L2 MIN": "76.21",
            "BOUND L2 AVG": "80.56",
            "BOUND RAM": "221.46"
            },
        "DISPATCH":
            {
            "P0": "50",
            "P1": "37",
            "P2": "35",
            "P3": "17",
            "P4": "17",
            "P5": "25"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.13",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "25.00",
        "VECTORIZATION PREDICT L2": "40.42",
        "NBR INSTRUCTIONS PER CYCLE": "3.18",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.74"
        },
    "LOOP 39":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "59"
            },
        "DECODING":
            {
            "number of cycles for decoding": "59.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "85.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "50"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "58.33"
            },
        "TOTAL":
            {
            "BOUND L1": "50.00",
            "BOUND L2 MIN": "74.17",
            "BOUND L2 AVG": "79.86",
            "BOUND RAM": "221.82"
            },
        "DISPATCH":
            {
            "P0": "50",
            "P1": "37",
            "P2": "35",
            "P3": "17",
            "P4": "17",
            "P5": "25"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.13",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "25.00",
        "VECTORIZATION PREDICT L2": "40.50",
        "NBR INSTRUCTIONS PER CYCLE": "3.14",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.74"
        },
    "LOOP 40":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "65"
            },
        "DECODING":
            {
            "number of cycles for decoding": "65.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "87.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "51"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "61.67"
            },
        "TOTAL":
            {
            "BOUND L1": "51.00",
            "BOUND L2 MIN": "77.30",
            "BOUND L2 AVG": "80.72",
            "BOUND RAM": "215.97"
            },
        "DISPATCH":
            {
            "P0": "51",
            "P1": "36",
            "P2": "34",
            "P3": "17",
            "P4": "17",
            "P5": "32"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.16",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "25.50",
        "VECTORIZATION PREDICT L2": "42.10",
        "NBR INSTRUCTIONS PER CYCLE": "3.24",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.71"
        },
    "LOOP 41":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "83"
            },
        "DECODING":
            {
            "number of cycles for decoding": "83.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "111.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "79"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "91.67"
            },
        "TOTAL":
            {
            "BOUND L1": "79.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "256.19"
            },
        "DISPATCH":
            {
            "P0": "79",
            "P1": "41",
            "P2": "48",
            "P3": "18",
            "P4": "12",
            "P5": "38"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.85",
        "PACKED LOAD RATIO": "0.19",
        "PACKED STORE RATIO": "0.33",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "79.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.78",
        "NBR FP INSTRUCTIONS PER CYCLE": "3.01"
        },
    "LOOP 42":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "67"
            },
        "DECODING":
            {
            "number of cycles for decoding": "67.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "91.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "51"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "62.00"
            },
        "TOTAL":
            {
            "BOUND L1": "51.00",
            "BOUND L2 MIN": "77.30",
            "BOUND L2 AVG": "80.72",
            "BOUND RAM": "215.97"
            },
        "DISPATCH":
            {
            "P0": "51",
            "P1": "36",
            "P2": "34",
            "P3": "17",
            "P4": "17",
            "P5": "33"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.16",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "26.00",
        "VECTORIZATION PREDICT L2": "42.10",
        "NBR INSTRUCTIONS PER CYCLE": "3.25",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.71"
        },
    "LOOP 43":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "83"
            },
        "DECODING":
            {
            "number of cycles for decoding": "83.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "107.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "79"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "91.33"
            },
        "TOTAL":
            {
            "BOUND L1": "79.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "256.19"
            },
        "DISPATCH":
            {
            "P0": "79",
            "P1": "41",
            "P2": "48",
            "P3": "18",
            "P4": "12",
            "P5": "37"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.86",
        "PACKED LOAD RATIO": "0.19",
        "PACKED STORE RATIO": "0.33",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "79.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.75",
        "NBR FP INSTRUCTIONS PER CYCLE": "3.01"
        },
    "LOOP 44":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "15"
            },
        "DECODING":
            {
            "number of cycles for decoding": "15.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "15.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "18.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "26.40",
            "BOUND L2 AVG": "27.90",
            "BOUND RAM": "87.06"
            },
        "DISPATCH":
            {
            "P0": "4",
            "P1": "12",
            "P2": "18",
            "P3": "6",
            "P4": "6",
            "P5": "4"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "12.00",
        "VECTORIZATION PREDICT L2": "15.24",
        "NBR INSTRUCTIONS PER CYCLE": "1.78",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.67"
        },
    "LOOP 45":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "17"
            },
        "DECODING":
            {
            "number of cycles for decoding": "17.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "25.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "23"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "23.00"
            },
        "TOTAL":
            {
            "BOUND L1": "23.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "124.41"
            },
        "DISPATCH":
            {
            "P0": "4",
            "P1": "12",
            "P2": "23",
            "P3": "11",
            "P4": "6",
            "P5": "4"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.49",
        "PACKED LOAD RATIO": "0.67",
        "PACKED STORE RATIO": "0.09",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "23.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.61",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.04"
        },
    "LOOP 46":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "30"
            },
        "DECODING":
            {
            "number of cycles for decoding": "30.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "36.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "35"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "35.00"
            },
        "TOTAL":
            {
            "BOUND L1": "35.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "168.83"
            },
        "DISPATCH":
            {
            "P0": "4",
            "P1": "12",
            "P2": "35",
            "P3": "11",
            "P4": "6",
            "P5": "4"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.49",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.09",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "29.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.74",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.69"
        },
    "LOOP 47":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "67"
            },
        "DECODING":
            {
            "number of cycles for decoding": "67.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "91.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "51"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "61.67"
            },
        "TOTAL":
            {
            "BOUND L1": "51.00",
            "BOUND L2 MIN": "77.11",
            "BOUND L2 AVG": "80.69",
            "BOUND RAM": "216.65"
            },
        "DISPATCH":
            {
            "P0": "51",
            "P1": "36",
            "P2": "34",
            "P3": "17",
            "P4": "17",
            "P5": "32"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.16",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "25.50",
        "VECTORIZATION PREDICT L2": "40.92",
        "NBR INSTRUCTIONS PER CYCLE": "3.24",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.71"
        },
    "LOOP 48":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "84"
            },
        "DECODING":
            {
            "number of cycles for decoding": "84.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "106.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "78"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "90.33"
            },
        "TOTAL":
            {
            "BOUND L1": "78.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "256.19"
            },
        "DISPATCH":
            {
            "P0": "78",
            "P1": "41",
            "P2": "48",
            "P3": "18",
            "P4": "12",
            "P5": "37"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.86",
        "PACKED LOAD RATIO": "0.19",
        "PACKED STORE RATIO": "0.33",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "78.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.78",
        "NBR FP INSTRUCTIONS PER CYCLE": "3.05"
        },
    "LOOP 49":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "66"
            },
        "DECODING":
            {
            "number of cycles for decoding": "66.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "94.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "51"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "61.67"
            },
        "TOTAL":
            {
            "BOUND L1": "51.00",
            "BOUND L2 MIN": "77.11",
            "BOUND L2 AVG": "80.69",
            "BOUND RAM": "216.65"
            },
        "DISPATCH":
            {
            "P0": "51",
            "P1": "36",
            "P2": "34",
            "P3": "17",
            "P4": "17",
            "P5": "32"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.16",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "25.50",
        "VECTORIZATION PREDICT L2": "40.92",
        "NBR INSTRUCTIONS PER CYCLE": "3.27",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.71"
        },
    "LOOP 50":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "84"
            },
        "DECODING":
            {
            "number of cycles for decoding": "84.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "104.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "78"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "90.33"
            },
        "TOTAL":
            {
            "BOUND L1": "78.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "256.19"
            },
        "DISPATCH":
            {
            "P0": "78",
            "P1": "41",
            "P2": "48",
            "P3": "18",
            "P4": "12",
            "P5": "37"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.84",
        "PACKED LOAD RATIO": "0.19",
        "PACKED STORE RATIO": "0.33",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "78.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.83",
        "NBR FP INSTRUCTIONS PER CYCLE": "3.05"
        },
    "LOOP 51":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "37"
            },
        "DECODING":
            {
            "number of cycles for decoding": "37.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "50.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "28"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "32.33"
            },
        "TOTAL":
            {
            "BOUND L1": "28.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "130.61"
            },
        "DISPATCH":
            {
            "P0": "27",
            "P1": "18",
            "P2": "28",
            "P3": "10",
            "P4": "10",
            "P5": "16"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.14",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "20.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "3.54",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.61"
        },
    "LOOP 52":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "48"
            },
        "DECODING":
            {
            "number of cycles for decoding": "48.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "73.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "46"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "46.00"
            },
        "TOTAL":
            {
            "BOUND L1": "46.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "224.33"
            },
        "DISPATCH":
            {
            "P0": "27",
            "P1": "18",
            "P2": "46",
            "P3": "19",
            "P4": "10",
            "P5": "17"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.72",
        "PACKED LOAD RATIO": "0.19",
        "PACKED STORE RATIO": "0.05",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "41.50",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.61",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.96"
        },
    "LOOP 53":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "51"
            },
        "DECODING":
            {
            "number of cycles for decoding": "51.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "71.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "51"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "51.00"
            },
        "TOTAL":
            {
            "BOUND L1": "51.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "244.93"
            },
        "DISPATCH":
            {
            "P0": "27",
            "P1": "18",
            "P2": "51",
            "P3": "19",
            "P4": "10",
            "P5": "16"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.69",
        "PACKED LOAD RATIO": "0.05",
        "PACKED STORE RATIO": "0.05",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "44.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.43",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.76"
        },
    "LOOP 54":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "46"
            },
        "DECODING":
            {
            "number of cycles for decoding": "46.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "58.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "37"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "37.00"
            },
        "TOTAL":
            {
            "BOUND L1": "37.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "174.70"
            },
        "DISPATCH":
            {
            "P0": "27",
            "P1": "18",
            "P2": "37",
            "P3": "13",
            "P4": "13",
            "P5": "11"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.08",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "26.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.86",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.22"
        },
    "LOOP 55":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "53"
            },
        "DECODING":
            {
            "number of cycles for decoding": "53.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "77.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "55"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "55.00"
            },
        "TOTAL":
            {
            "BOUND L1": "55.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "270.73"
            },
        "DISPATCH":
            {
            "P0": "27",
            "P1": "18",
            "P2": "55",
            "P3": "22",
            "P4": "13",
            "P5": "12"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.71",
        "PACKED LOAD RATIO": "0.28",
        "PACKED STORE RATIO": "0.18",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "50.50",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.29",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.64"
        },
    "LOOP 56":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "58"
            },
        "DECODING":
            {
            "number of cycles for decoding": "58.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "66.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "63"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "63.00"
            },
        "TOTAL":
            {
            "BOUND L1": "63.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "301.66"
            },
        "DISPATCH":
            {
            "P0": "27",
            "P1": "18",
            "P2": "63",
            "P3": "22",
            "P4": "13",
            "P5": "12"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.67",
        "PACKED LOAD RATIO": "0.09",
        "PACKED STORE RATIO": "0.18",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "54.50",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.14",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.43"
        },
    "LOOP 57":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "23"
            },
        "DECODING":
            {
            "number of cycles for decoding": "23.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "55.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "18"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "18.00"
            },
        "TOTAL":
            {
            "BOUND L1": "18.00",
            "BOUND L2 MIN": "50.55",
            "BOUND L2 AVG": "53.85",
            "BOUND RAM": "162.15"
            },
        "DISPATCH":
            {
            "P0": "15",
            "P1": "1",
            "P2": "18",
            "P3": "15",
            "P4": "15",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "10.50",
        "VECTORIZATION PREDICT L2": "26.85",
        "NBR INSTRUCTIONS PER CYCLE": "2.78",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.83"
        },
    "LOOP 58":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "21"
            },
        "DECODING":
            {
            "number of cycles for decoding": "21.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "51.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "22"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "22.00"
            },
        "TOTAL":
            {
            "BOUND L1": "22.00",
            "BOUND L2 MIN": "30.59",
            "BOUND L2 AVG": "32.94",
            "BOUND RAM": "184.49"
            },
        "DISPATCH":
            {
            "P0": "15",
            "P1": "1",
            "P2": "22",
            "P3": "17",
            "P4": "15",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.83",
        "PACKED LOAD RATIO": "0.65",
        "PACKED STORE RATIO": "0.76",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "21.00",
        "VECTORIZATION PREDICT L2": "21.00",
        "NBR INSTRUCTIONS PER CYCLE": "2.45",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.36"
        },
    "LOOP 59":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "38"
            },
        "DECODING":
            {
            "number of cycles for decoding": "38.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "70.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "46"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "46.00"
            },
        "TOTAL":
            {
            "BOUND L1": "46.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "318.53"
            },
        "DISPATCH":
            {
            "P0": "15",
            "P1": "1",
            "P2": "46",
            "P3": "29",
            "P4": "15",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.58",
        "PACKED LOAD RATIO": "0.03",
        "PACKED STORE RATIO": "0.03",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "39.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.70",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.65"
        },
    "LOOP 60":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "60"
            },
        "DECODING":
            {
            "number of cycles for decoding": "60.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "90.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "51"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "59.67"
            },
        "TOTAL":
            {
            "BOUND L1": "51.00",
            "BOUND L2 MIN": "74.17",
            "BOUND L2 AVG": "79.86",
            "BOUND RAM": "221.82"
            },
        "DISPATCH":
            {
            "P0": "51",
            "P1": "37",
            "P2": "36",
            "P3": "17",
            "P4": "17",
            "P5": "26"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.12",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "26.00",
        "VECTORIZATION PREDICT L2": "40.50",
        "NBR INSTRUCTIONS PER CYCLE": "3.14",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.71"
        },
    "LOOP 61":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "67"
            },
        "DECODING":
            {
            "number of cycles for decoding": "67.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "89.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "51"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "61.67"
            },
        "TOTAL":
            {
            "BOUND L1": "51.00",
            "BOUND L2 MIN": "76.96",
            "BOUND L2 AVG": "79.29",
            "BOUND RAM": "215.42"
            },
        "DISPATCH":
            {
            "P0": "51",
            "P1": "36",
            "P2": "34",
            "P3": "17",
            "P4": "17",
            "P5": "32"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.16",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "25.50",
        "VECTORIZATION PREDICT L2": "42.05",
        "NBR INSTRUCTIONS PER CYCLE": "3.27",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.71"
        },
    "LOOP 62":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "82"
            },
        "DECODING":
            {
            "number of cycles for decoding": "82.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "106.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "78"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "90.33"
            },
        "TOTAL":
            {
            "BOUND L1": "78.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "256.19"
            },
        "DISPATCH":
            {
            "P0": "78",
            "P1": "41",
            "P2": "48",
            "P3": "18",
            "P4": "12",
            "P5": "37"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.85",
        "PACKED LOAD RATIO": "0.19",
        "PACKED STORE RATIO": "0.33",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "78.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.82",
        "NBR FP INSTRUCTIONS PER CYCLE": "3.05"
        },
    "LOOP 63":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "10"
            },
        "DECODING":
            {
            "number of cycles for decoding": "10.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "10.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "12"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "12.00"
            },
        "TOTAL":
            {
            "BOUND L1": "12.00",
            "BOUND L2 MIN": "21.39",
            "BOUND L2 AVG": "21.63",
            "BOUND RAM": "54.43"
            },
        "DISPATCH":
            {
            "P0": "6",
            "P1": "5",
            "P2": "12",
            "P3": "1",
            "P4": "1",
            "P5": "2"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "6.00",
        "VECTORIZATION PREDICT L2": "6.68",
        "NBR INSTRUCTIONS PER CYCLE": "1.75",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.92"
        },
    "LOOP 64":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "131"
            },
        "DECODING":
            {
            "number of cycles for decoding": "131.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "195.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "124"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "147.00"
            },
        "TOTAL":
            {
            "BOUND L1": "124.00",
            "BOUND L2 MIN": "124.00",
            "BOUND L2 AVG": "124.00",
            "BOUND RAM": "411.12"
            },
        "DISPATCH":
            {
            "P0": "124",
            "P1": "81",
            "P2": "91",
            "P3": "18",
            "P4": "18",
            "P5": "69"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.91",
        "PACKED LOAD RATIO": "0.79",
        "PACKED STORE RATIO": "1.00",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.82",
        "NBR FP INSTRUCTIONS PER CYCLE": "3.29"
        },
    "LOOP 65":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "137"
            },
        "DECODING":
            {
            "number of cycles for decoding": "137.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "203.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "125"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "146.33"
            },
        "TOTAL":
            {
            "BOUND L1": "125.00",
            "BOUND L2 MIN": "148.48",
            "BOUND L2 AVG": "156.10",
            "BOUND RAM": "423.09"
            },
        "DISPATCH":
            {
            "P0": "125",
            "P1": "77",
            "P2": "92",
            "P3": "19",
            "P4": "19",
            "P5": "64"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.17",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "63.50",
        "VECTORIZATION PREDICT L2": "63.50",
        "NBR INSTRUCTIONS PER CYCLE": "2.74",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.60"
        },
    "LOOP 66":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "135"
            },
        "DECODING":
            {
            "number of cycles for decoding": "136.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "224.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "123"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "143.33"
            },
        "TOTAL":
            {
            "BOUND L1": "123.00",
            "BOUND L2 MIN": "157.37",
            "BOUND L2 AVG": "162.34",
            "BOUND RAM": "435.31"
            },
        "DISPATCH":
            {
            "P0": "123",
            "P1": "81",
            "P2": "97",
            "P3": "18",
            "P4": "18",
            "P5": "61"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.17",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "61.50",
        "VECTORIZATION PREDICT L2": "61.50",
        "NBR INSTRUCTIONS PER CYCLE": "2.85",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.66"
        },
    "LOOP 67":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "136"
            },
        "DECODING":
            {
            "number of cycles for decoding": "137.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "193.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "125"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "146.67"
            },
        "TOTAL":
            {
            "BOUND L1": "125.00",
            "BOUND L2 MIN": "157.86",
            "BOUND L2 AVG": "164.20",
            "BOUND RAM": "442.10"
            },
        "DISPATCH":
            {
            "P0": "125",
            "P1": "81",
            "P2": "97",
            "P3": "19",
            "P4": "19",
            "P5": "65"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.17",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "63.50",
        "VECTORIZATION PREDICT L2": "63.50",
        "NBR INSTRUCTIONS PER CYCLE": "2.89",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.63"
        },
    "LOOP 68":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "175"
            },
        "DECODING":
            {
            "number of cycles for decoding": "175.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "213.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "124"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "144.33"
            },
        "TOTAL":
            {
            "BOUND L1": "124.00",
            "BOUND L2 MIN": "139.79",
            "BOUND L2 AVG": "144.96",
            "BOUND RAM": "631.62"
            },
        "DISPATCH":
            {
            "P0": "124",
            "P1": "81",
            "P2": "123",
            "P3": "35",
            "P4": "29",
            "P5": "61"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.83",
        "PACKED LOAD RATIO": "0.35",
        "PACKED STORE RATIO": "0.66",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "124.00",
        "VECTORIZATION PREDICT L2": "124.00",
        "NBR INSTRUCTIONS PER CYCLE": "3.31",
        "NBR FP INSTRUCTIONS PER CYCLE": "3.29"
        },
    "LOOP 69":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "32"
            },
        "DECODING":
            {
            "number of cycles for decoding": "32.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "48.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "42"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "42.00"
            },
        "TOTAL":
            {
            "BOUND L1": "42.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "147.78"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "14",
            "P2": "42",
            "P3": "6",
            "P4": "6",
            "P5": "8"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.07",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "28.50",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.74",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.76"
        },
    "LOOP 70":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "34"
            },
        "DECODING":
            {
            "number of cycles for decoding": "34.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "48.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "42"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "42.00"
            },
        "TOTAL":
            {
            "BOUND L1": "42.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "147.53"
            },
        "DISPATCH":
            {
            "P0": "18",
            "P1": "14",
            "P2": "42",
            "P3": "6",
            "P4": "6",
            "P5": "7"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.07",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "28.50",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "1.79",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.76"
        },
    "LOOP 71":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "124"
            },
        "DECODING":
            {
            "number of cycles for decoding": "125.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "137.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "99"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "113.33"
            },
        "TOTAL":
            {
            "BOUND L1": "99.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "408.28"
            },
        "DISPATCH":
            {
            "P0": "99",
            "P1": "54",
            "P2": "91",
            "P3": "19",
            "P4": "13",
            "P5": "43"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.81",
        "PACKED LOAD RATIO": "0.20",
        "PACKED STORE RATIO": "0.37",
        "PACKED MUL RATIO": "1.00",
        "PACKED ADD SUB RATIO": "1.00",
        "VECTORIZATION PREDICT L1": "99.00",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.95",
        "NBR FP INSTRUCTIONS PER CYCLE": "3.03"
        },
    "LOOP 72":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "3.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "3.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "3"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "3.00"
            },
        "TOTAL":
            {
            "BOUND L1": "3.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "2",
            "P1": "2",
            "P2": "3",
            "P3": "1",
            "P4": "1",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "3.33",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 73":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "2.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "2.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "1"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "1.67"
            },
        "TOTAL":
            {
            "BOUND L1": "1.00",
            "BOUND L2 MIN": "3.37",
            "BOUND L2 AVG": "3.59",
            "BOUND RAM": "10.81"
            },
        "DISPATCH":
            {
            "P0": "1",
            "P1": "1",
            "P2": "1",
            "P3": "1",
            "P4": "1",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "1.00",
        "VECTORIZATION PREDICT L2": "1.79",
        "NBR INSTRUCTIONS PER CYCLE": "6.00",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 74":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "2.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "4.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "2"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "2.00"
            },
        "TOTAL":
            {
            "BOUND L1": "2.00",
            "BOUND L2 MIN": "4.40",
            "BOUND L2 AVG": "4.65",
            "BOUND RAM": "14.51"
            },
        "DISPATCH":
            {
            "P0": "2",
            "P1": "1",
            "P2": "2",
            "P3": "1",
            "P4": "1",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "0.00",
        "PACKED MUL RATIO": "0.00",
        "PACKED ADD SUB RATIO": "0.00",
        "VECTORIZATION PREDICT L1": "1.50",
        "VECTORIZATION PREDICT L2": "2.54",
        "NBR INSTRUCTIONS PER CYCLE": "4.00",
        "NBR FP INSTRUCTIONS PER CYCLE": "1.00"
        },
    "LOOP 75":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "2.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "2.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "2"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "2.33"
            },
        "TOTAL":
            {
            "BOUND L1": "2.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "2",
            "P1": "2",
            "P2": "0",
            "P3": "0",
            "P4": "0",
            "P5": "1"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "NO STORE",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "3.50",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        },
    "LOOP 76":
        { 
        "PREDECODING":
            {
            "number of cycles for predecoding:": "1"
            },
        "DECODING":
            {
            "number of cycles for decoding": "2.00"
            },
        "REORDER BUFFER":
            {
            "number of cycles due to read-stalls": "2.00"
            },
        "EXECUTION PORTS _ SUB OPTIMAL METHOD":
            {
            "number of cycles due to the issue on execution ports (sub-optimal method)": "3"
            },
        "EXECUTION PORTS _ AGNER METHOD":
            {
            "number of cycles due to the issue on execution ports (Agner method)": "3.67"
            },
        "TOTAL":
            {
            "BOUND L1": "3.00",
            "BOUND L2 MIN": "NA",
            "BOUND L2 AVG": "NA",
            "BOUND RAM": "NA"
            },
        "DISPATCH":
            {
            "P0": "3",
            "P1": "2",
            "P2": "0",
            "P3": "0",
            "P4": "0",
            "P5": "2"
            },
        "PACKED INSTRUCTIONS USE DEGREE": "0.00",
        "PACKED LOAD RATIO": "0.00",
        "PACKED STORE RATIO": "NO STORE",
        "PACKED MUL RATIO": "NO MULTIPLICATION",
        "PACKED ADD SUB RATIO": "NO ADDITION - NO SUBSTRACTION",
        "VECTORIZATION PREDICT L1": "NA",
        "VECTORIZATION PREDICT L2": "NA",
        "NBR INSTRUCTIONS PER CYCLE": "2.67",
        "NBR FP INSTRUCTIONS PER CYCLE": "0.00"
        }
    }
