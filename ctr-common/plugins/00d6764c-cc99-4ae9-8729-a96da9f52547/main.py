#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import database_manager, repository, util_uid, database, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os

class ReindexPlugin(HapiPlugin):
#---------------------------------------------------------------------------
    
    def insert_schema(self, uids, max_threads=8):
        db = database.Database()
        database_manager.index_group(uids, db, max_threads)
    
#---------------------------------------------------------------------------
    
    def create_schema(self):
        database_manager.create_schema()
    
#---------------------------------------------------------------------------
    
    @hapi_command("all")
    def cmd_all(self, params):
        """ Reindex all visible CTR repositories
        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        
        max_threads = params["max_threads"]
        if type(max_threads) != int or max_threads < 0:
            util.cti_plugin_print_error("The argument 'max_thread' must be a positive integer (or 0 for no limit of threads).")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        
        self.create_schema()
        self.insert_schema(get_all_uids(), max_threads)
    
#---------------------------------------------------------------------------
    
    # Bypass the authentification
    def check_passwd(self):
        return True
    
#---------------------------------------------------------------------------

def get_all_uids():
    """
    returns a set with all the data uids.
    """
    path = ctr.ctr_plugin_get_common_data_dir()
    for uid in os.listdir(path):
        try:
            yield util_uid.CTI_UID(uid)
        except:
            continue
    
    path = ctr.ctr_plugin_get_temp_data_dir()
    for uid in os.listdir(path):
        try:
            yield util_uid.CTI_UID(uid)
        except:
            continue
    
    for uid in repository.get_all_uid_local():
        try:
            yield uid
        except:
            continue
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
   p = ReindexPlugin()
   exit(p.main(sys.argv))
