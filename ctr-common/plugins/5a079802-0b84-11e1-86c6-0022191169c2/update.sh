#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

export PKG_CONFIG_PATH='/usr/local/lib/pkgconfig/'
TMP_DIRECTORY=/tmp/cti_update
GIT_REPOSITORY=$1

echo "************************************************************************"
echo "     ... Updating CTI in $CTI_ROOT ..."
echo "************************************************************************"
echo ""
echo "/!\ WARNING: PLEASE BE SURE TO HAVE A BACKUP OF YOUR CTI REPOSITORIES"
echo "             AND DATA BEFORE RUNNING THE UPDATE!!"
echo "/!\ PLEASE DO NOT STOP THE PROCESS WHEN THE UPDATE PART HAS BEGUN!"
echo ""
echo ""

echo "************************************************************************"
echo " Checking the version..."
echo "************************************************************************"
if [ "`cti --version |grep \`git ls-remote --tags $GIT_REPOSITORY | tail -1 | cut -d / -f 3\``" == "" ]
then
    echo "A new release is available!"
    echo "************************************************************************"
    echo " Downloading the last CTI core release..."
    echo "************************************************************************"

    rm -rf $TMP_DIRECTORY
    mkdir -p $TMP_DIRECTORY
    cd $TMP_DIRECTORY
    git clone $GIT_REPOSITORY
    if [ $? != 0 ]
    then
        echo "Error with GIT" 1>&2
        exit 1
    fi

    cd cti

    echo "************************************************************************"
    echo " Starting the full update..."
    echo "************************************************************************"
    # Finally, it is better to run the whole installation process instead of only updating ctr-common
    # if we do not need to update the CTI core since we ALSO need to reindex the entries, rewrite the plugin
    # documentation, etc....
    ./install_cti.sh -f
    if [ $? != 0 ]
    then
        echo "Error during the installation process" 1>&2
        exit 1
    fi
else
    echo "You already have the last CTI stable release." 1>&2
fi  

echo "************************************************************************"
echo " Removing the temp files..."
echo "************************************************************************"
rm -rf $TMP_DIRECTORY

echo ""
echo "Done!"
