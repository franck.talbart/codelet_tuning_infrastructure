#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi.main import HapiPlugin, hapi_command
from cti_hapi import cti_json, plugin, util

import sys, os, subprocess

class UpdatePlugin(HapiPlugin):
    @hapi_command("run")
    def run_cmd(self, params):
        """ Updates CTI

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        
        git_repository = params['git_repository']
        update_filename = params['update_filename']
        
        cmd = [os.path.join(self.plugin_directory, update_filename), 
               git_repository]
        sys.stdout.flush()
        try: 
           p = subprocess.Popen(cmd, shell=False, stderr=subprocess.STDOUT)
           p.communicate()
           
        except OSError:
            util.cti_plugin_print_error("Failed to run the shell script.")
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
        except KeyboardInterrupt:
            util.cti_plugin_print_error("Process user-interrupted.")
            return cti.CTI_PLUGIN_ERROR_USER_INTERRUPT
            
        except:
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
        
        return p.returncode
#---------------------------------------------------------------------------
    
    @hapi_command("daemon")
    def daemon_cmd(self, params):
        """ run an update daemon

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        daemon_uid = params["daemon_uid"]
        format = params["format"]
        
        json = \
        {
            "init": 
            {
                "attributes": {cti.META_ATTRIBUTE_NAME : "init"},
                "params": 
                [
                    {
                        cti.META_ATTRIBUTE_NAME : "command",
                        cti.META_ATTRIBUTE_VALUE : 
                        [
                            'cti', 
                            '--user=' + self.username,
                            self.password,
                            str(self.plugin_uid),
                            'run'
                        ]
                    },
                    {cti.META_ATTRIBUTE_NAME : "produced_by",cti.META_ATTRIBUTE_VALUE : ""}
                ]
            }
        }
        
        try:
            output = plugin.execute_plugin_by_file (daemon_uid,
                                                    json,
                                                    self.username,
                                                    self.password)
        except :
            print sys.exc_info()[1]
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
        entry_uid = plugin.get_output_data_uid(output)
        
        if output is not None:
            if format == "json":
                print cti_json.cti_json_encode({'entry':entry_uid})
            elif format == "txt":
                print "The update is being processed."
                print output
            else:
                util.cti_plugin_print_error("Unknown format")
                return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        else:
            if format == 'json':
                print cti_json.cti_json_encode({'output': output})
            elif format == 'txt':
                print "Entry can't be created. UID is None." + output
            else:
                util.cti_plugin_print_error("Unknown format")
                return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------
    
    def check_passwd(self):
        if self.username == "admin":
            return HapiPlugin.check_passwd(self)
        else:
            util.cti_plugin_print_error("You must be administrator to do this.")
            return cti.CTI_PLUGIN_ERROR_RIGHT
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = UpdatePlugin()
    exit(p.main(sys.argv))
