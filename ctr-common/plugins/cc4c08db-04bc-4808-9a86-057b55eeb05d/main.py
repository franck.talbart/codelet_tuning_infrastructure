#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import plugin, entry, util, types
from cti_hapi.main import HapiPlugin, hapi_command

import sys, csv, glob, os

class ImportCSVPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          int
        """
        csv_filename = params["file"]
        schema_uid = params["schema_uid"]
        command = params["command"]
        matrix_parameter_name = params["output_parameter"]
        separator = params["separator"]
        format_o = params["format"]
        additional_parameters = params["additional_parameters"]
        
        rep = self.work_params["init"].attributes[cti.META_ATTRIBUTE_REP]
        if cti.META_ATTRIBUTE_REP_PRODUCE in params:
            rep = params[cti.META_ATTRIBUTE_REP_PRODUCE]
            
        #Reading csv file
        csv_file = open(csv_filename, 'r')
        csv_reader = csv.DictReader(csv_file, delimiter=separator)
        csv_contents = [line for line in csv_reader]
        csv_file.close()
        
        #Parsing the CSV by column instead of line
        csv_output_parameter = {
            cti.META_ATTRIBUTE_NAME: matrix_parameter_name,
            cti.META_ATTRIBUTE_VALUE: {}
        }
        
        header_map = dict([(util.column_name_replace(elt.strip()),elt) for elt in csv_reader.fieldnames])
        
        plugin_input, _ = entry.load_defaults(schema_uid)
        
        if not matrix_parameter_name in plugin_input[command].params:
            util.cti_plugin_print_error(
                "Parameter {0} does not exist in the {1} plugin.".format(matrix_parameter_name, schema_uid))
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        matrix_output_param = plugin_input[command].params[matrix_parameter_name]
        
        #Adding values from CSV
        for curr_fieldname in csv_reader.fieldnames:
            try:
                index = matrix_output_param[cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES].index(curr_fieldname)
            except ValueError:
                util.cti_plugin_print_error(
                    "The '{0}' column from {1} does not exist in the {2} plugin.\n".format(curr_fieldname, csv_filename, schema_uid))
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS

            column_type = matrix_output_param[cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES][index]
            
            csv_output_parameter[cti.META_ATTRIBUTE_VALUE][curr_fieldname] = [types.from_csv(line[header_map[curr_fieldname]], column_type)[1] for line in csv_contents]
            
        json = \
        {
            command:
            {
                "attributes": {cti.META_ATTRIBUTE_NAME : command, cti.META_ATTRIBUTE_REP: rep},
                "params": additional_parameters + [csv_output_parameter] + [{cti.META_ATTRIBUTE_NAME: "format",cti.META_ATTRIBUTE_VALUE : format_o}]
            }
        }
        
        output = ""
        
        try:
            output = plugin.execute_plugin_by_file(schema_uid,
                                                   json,
                                                   self.username,
                                                   self.password)
        except:
            print sys.exc_info()[1]
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
        
        entry_uid = plugin.get_output_data_uid(output)
        
        if entry_uid != None:
            entry.put_file_in_entry(
                entry_uid,
                csv_filename,
                True
            )
            
        print output
        return 0
#---------------------------------------------------------------------------
    @hapi_command("directory")
    def directory_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          int
        """
        
        schema_uid = params["schema_uid"]
        directory = params["directory"]
        separator = params["separator"]
        format_o = params["format"]
        output_parameter = params["output_parameter"]
        additional_parameters = params["additional_parameters"]
        
        rep = self.work_params["init"].attributes[cti.META_ATTRIBUTE_REP]
        if cti.META_ATTRIBUTE_REP_PRODUCE in params:
            rep = params[cti.META_ATTRIBUTE_REP_PRODUCE]
        
        for csv_file in glob.glob(directory + '/*.csv'):
            print "* Importing %s" % (os.path.basename(csv_file))
            json = \
            {
                "init":
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP: rep},
                    "params": [{cti.META_ATTRIBUTE_NAME: "schema_uid",cti.META_ATTRIBUTE_VALUE : schema_uid},
                               {cti.META_ATTRIBUTE_NAME: "file",cti.META_ATTRIBUTE_VALUE : csv_file},
                               {cti.META_ATTRIBUTE_NAME: "separator",cti.META_ATTRIBUTE_VALUE : separator},
                               {cti.META_ATTRIBUTE_NAME: "format",cti.META_ATTRIBUTE_VALUE : format_o},
                               {cti.META_ATTRIBUTE_NAME: "output_parameter",cti.META_ATTRIBUTE_VALUE : output_parameter},
                               {cti.META_ATTRIBUTE_NAME: "additional_parameters",cti.META_ATTRIBUTE_VALUE : additional_parameters}]
                }
            }
            
            
            try:
                plugin.execute_plugin_by_file(self.plugin_uid,
                                                       json,
                                                       self.username,
                                                       self.password, interactive=True)
            except:
                print sys.exc_info()[1]
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
        return 0
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = ImportCSVPlugin()
    exit(p.main(sys.argv))
