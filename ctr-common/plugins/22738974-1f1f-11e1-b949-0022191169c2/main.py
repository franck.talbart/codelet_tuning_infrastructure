#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi.main import HapiPlugin, hapi_command
from cti_hapi import plugin, util_uid, database, database_manager, util

import sys, os

class CleanPlugin(HapiPlugin):
    @hapi_command("temp_repository")
    def temp_repository_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        rm_uid = params["rm_uid"]
        
        db = database.Database()
        tmp_entries = database_manager.search_uids(
                                    {'NAME':["entry_info","repository"], 'TYPE':"=", 'VAL':cti.TEMP_REPOSITORY}, 
                                    db,
                                    name_dict={'entry_info.repository':{'ALIAS':'entry_info', 'TABLE':'entry_info'}}
                            )
        tmp_entries = list(tmp_entries)
        for data in tmp_entries: 
            uid = str(util_uid.CTI_UID(str(data)))
            json_file = \
            {
                "entry": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "entry"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "entry",cti.META_ATTRIBUTE_VALUE : uid},
                        {cti.META_ATTRIBUTE_NAME : "force",cti.META_ATTRIBUTE_VALUE : True}
                    ]
                }
            }
            
            try:
                plugin.execute_plugin_by_file(rm_uid, json_file, self.username,
                                                                self.password)
            except:
                print sys.exc_info()[1]
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
            print ".",
            sys.stdout.flush()
        print "\nDone!"
    
    #---------------------------------------------------------------------------
    
    @hapi_command("entry")
    def entry_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        rm_uid = params["rm_uid"]
        
        entries_log_file_name = cti.cti_plugin_config_get_value(cti.WRONG_ENTRIES_LOG_FILE_NAME)
        cti_cfg_dir = ctr.ctr_plugin_get_cti_cfg_dir()
        
        assert entries_log_file_name is not None
        assert cti_cfg_dir is not None
        
        filename = os.path.join(cti_cfg_dir, entries_log_file_name)
        
        try:   
            file_wrong_entries = open(filename, 'r')
        except:
            sys.stdout.write("No wrong entry found.\n")
            return 0
        
        for uid in file_wrong_entries:  
            uid = uid.strip()
            
            json_file = \
            {
                "entry": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "entry"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "entry",cti.META_ATTRIBUTE_VALUE : uid},
                        {cti.META_ATTRIBUTE_NAME : "force",cti.META_ATTRIBUTE_VALUE : True}
                    ]
                }
            }
            
            try:
                plugin.execute_plugin_by_file(rm_uid, json_file, self.username, self.password)
            except:
                print sys.exc_info()[1]
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
            print ".",
            sys.stdout.flush()
        os.remove(filename)
        print "\nDone!"
    
    #---------------------------------------------------------------------------
    def check_passwd(self):
        if self.username == "admin":
            return HapiPlugin.check_passwd(self)
        else:
            util.cti_plugin_print_error("You must be administrator to do this.\n")
            exit(cti.CTI_PLUGIN_ERROR_RIGHT)
    
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = CleanPlugin()
    exit(p.main(sys.argv))
