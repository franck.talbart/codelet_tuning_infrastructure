#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi.main import HapiPlugin, hapi_command

import sys

import create_plugin

class Plugin(HapiPlugin):
    @hapi_command("create")
    def create_plugin_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        create_plugin.create_plugin(self, params)
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = Plugin()
    exit(p.main(sys.argv))
