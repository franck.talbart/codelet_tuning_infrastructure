#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import database, database_manager, cti_json, alias, description, util

import os, shutil, sys, stat

def create_plugin(self, params):
    """ Create a plugin
    Returns:
      Nothing
    """
    
    uid_plugin = cti.cti_plugin_generate_uid()
    if not uid_plugin:
        util.cti_plugin_print_error("Unexpected error while generating an UID")
        exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
    path_plugin = create_ctr_plugin_info(self, params, uid_plugin)
    copy_template_files(path_plugin, params)
    
    if(params["format"] == "json"):
        result = {
            'new_uid': uid_plugin,
            'type': cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID
        }
        print cti_json.cti_json_encode(result)
    elif (params["format"] == "txt"):
        print "Plugin template has been successfully created (UID = ", str(uid_plugin), ")"
        print "Do not forget to fill in the source codes in the following directory: ", path_plugin
        print "Then, type 'cti reindex all' if it is a descriptive plugin."
    else:
        util.cti_plugin_print_error("Unknown format " + str(params["format"]))
        return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
    
#---------------------------------------------------------------------------

def create_ctr_plugin_info(self, params, uid_plugin):
    """ Create the CTR plugin info file

    Args:
      uid_plugin: the plugin UID 

    Returns:
      The path to the plugin directory
    """
    
    def check_alias(name, value, no_param):
        """ Description
    
        Args:
            name: name
            value: value
            param: working parameter
    
        Returns:
          true or false
        """
    
        if name == "alias":
            if value != "":
                if alias.get_plugin_uid(value):
                    print "This alias already exists!"
                    return True
        return False
    
    def category_alias2uid(name, value, no_param):
        """ Description
    
        Args:
            name: name
            value: value
            param: working parameter
    
        Returns:
          
        """
        if name == "category" and value != "":
            db = database.Database()
            result = database_manager.search(
                                             {
                                              'NAME':["entry_info","alias"],
                                              'TYPE':"=",
                                              'VAL':value
                                             }, 
                                             db,
                                             "category",
                                             fields=["entry_info.entry_uid"]
                                           )
            for r in result:
                value = r[0]
        return value
    
    print "* New plugin *"
    self.work_params = description.description_write(self.command,
                                                self.work_params, 
                                                check_alias, 
                                                accum_process=category_alias2uid)
    
    alias_p = self.work_params[self.command].params["alias"][cti.META_ATTRIBUTE_VALUE]
    category_v = self.work_params[self.command].params["category"][cti.META_ATTRIBUTE_VALUE]
    build_number = "1"
    description_p = self.work_params[self.command].params["description"][cti.META_ATTRIBUTE_VALUE]
    author = self.work_params[self.command].params["author"][cti.META_ATTRIBUTE_VALUE]
    dependence_build_number = self.work_params[self.command].params["build-number"][cti.META_ATTRIBUTE_VALUE]
    
    if alias_p != "":
        if alias.set_plugin_alias(uid_plugin, alias_p) == 0:
            sys.stderr.write("Cannot set the alias %s \n" % (alias_p))
            
    common_plugin_dir = ctr.ctr_plugin_get_common_plugin_dir()
    path = os.path.join(common_plugin_dir, str(uid_plugin))
    os.mkdir(path)
    
    ctr_plugin_info = ctr.ctr_plugin_info_create()
    ctr.ctr_plugin_info_put_value(ctr_plugin_info, cti.PLUGIN_INFO_BUILD_NUMBER, build_number)
    ctr.ctr_plugin_info_put_value(ctr_plugin_info, cti.PLUGIN_INFO_CATEGORY, str(category_v))
    ctr.ctr_plugin_info_put_value(ctr_plugin_info, cti.PLUGIN_INFO_DESCRIPTION, description_p)
    ctr.ctr_plugin_info_put_value(ctr_plugin_info, cti.PLUGIN_INFO_AUTHOR, author)
    ctr.ctr_plugin_info_put_value(ctr_plugin_info, cti.PLUGIN_INFO_DEPENDENCY, dependence_build_number)
    
    path_ctr_plugin_info = os.path.join(path, cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME))
    ctr.ctr_plugin_info_record_in_file(ctr_plugin_info, path_ctr_plugin_info)
    
    return path

#---------------------------------------------------------------------------

#Default file names
main_py = "main.py"
tests_dir = "tests"
ctr_doc = "ctr_doc.txt"
run_test_cmd = "run_test_cmd"


def copy_template_files(path_plugin, params):
    """ Copy the template files
    
    Args:
      path_plugin: The path to the plugin directory
    
    Returns:
      Nothing
    """
    
    python_main_filename = main_py
    tests = tests_dir
    
    exec_script_name = cti.cti_plugin_config_get_value(cti.EXEC_SCRIPT_NAME)
    input_default_file_name = cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_INPUT_FILENAME)
    output_default_file_name = cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_OUTPUT_FILENAME)
    
    # Add input default file
    path = os.path.join(path_plugin, input_default_file_name)
    model_path = os.path.join(sys.argv[1], params["model_ctr_input_default"])
    shutil.copyfile(model_path, path)
    
    # Add output default file
    path = os.path.join(path_plugin, output_default_file_name)
    model_path = os.path.join(sys.argv[1], params["model_ctr_output_default"])
    shutil.copyfile(model_path, path)
    
    # Add ctr_doc file
    path = os.path.join(path_plugin, ctr_doc)
    model_path = os.path.join(sys.argv[1], params["model_ctr_doc"])
    shutil.copyfile(model_path, path)
    
    # Add run_plugin.sh
    path = os.path.join(path_plugin, exec_script_name)
    model_path = os.path.join(sys.argv[1], params["model_python_run_plugin"])
    shutil.copyfile(model_path, path)
    
    # Main program
    path = os.path.join(path_plugin, python_main_filename)
    model_path = os.path.join(sys.argv[1], params["model_python_main"])
    shutil.copyfile(model_path, path)
    
    path = os.path.join(path_plugin, exec_script_name)
    os.chmod(path, stat.S_IEXEC | stat.S_IREAD)
    
    path = os.path.join(path_plugin, tests)
    os.mkdir(path)
    path = os.path.join(path, run_test_cmd)
    model_run_test = os.path.join(sys.argv[1], run_test_cmd)
    shutil.copyfile(model_run_test, path)
    os.chmod(path, stat.S_IEXEC | stat.S_IREAD)
