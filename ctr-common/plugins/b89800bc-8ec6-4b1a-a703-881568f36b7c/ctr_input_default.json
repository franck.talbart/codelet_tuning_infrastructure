{
    "init": {
        "attributes": {
            "produce_data": true, 
            "name": "init", 
            "repository": "local", 
            "desc": "parametrizes processor in a computing system",
            "long_desc": "Creates a CPU entry using the given params."
        }, 
        "params": [
            {
                "type": "TEXT", 
                "name": "alias", 
                "desc": "Alias", 
                "long_desc": "The alias of the CPU", 
                "optional": true
            }, 
            {
                "produced_by": "5c952ce6-11c0-47e8-8f53-132f0515de31", 
                "type": "DATA_UID", 
                "name": "vendor", 
                "desc": "Vendor", 
                "long_desc": "The UID or Alias of the vendor that made the CPU.", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "codename", 
                "desc": "Codename", 
                "long_desc": "Code name of the CPU (i7, i5, P5142, etc).", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "model", 
                "desc": "Model", 
                "long_desc": "The model of the CPU.", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "family", 
                "desc": "Family", 
                "long_desc": "The family the CPU belongs to.", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "external_model", 
                "desc": "external model", 
                "long_desc": "The external model of the CPU", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "external_family", 
                "desc": "external family", 
                "long_desc": "The external family of the CPU", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "revision", 
                "desc": "Revision", 
                "long_desc": "The material revision of the CPU.", 
                "optional": true,
                "value": null
            },
            {
                "type": "INTEGER", 
                "name": "number_of_cores", 
                "desc": "Number of cores", 
                "long_desc": "The number of cores the CPU holds.", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "INTEGER", 
                "name": "number_of_threads", 
                "desc": "Number of threads", 
                "long_desc": "The total number of threads accessible.", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "FLOAT", 
                "name": "core_frequency", 
                "desc": "Frequency", 
                "long_desc": "The core frequency, in Hz.",
                "optional": true,
                "value": null
            }, 
            {
                "type": "FLOAT", 
                "name": "bus_frequency", 
                "desc": "Bus speed", 
                "long_desc": "The bus speed, in Hz.",
                "optional": true,
                "value": null
            }, 
            {
                "type": "FLOAT", 
                "name": "fsb_speed", 
                "desc": "FSB speed", 
                "long_desc": "The fsb speed, in Hz.",
                "optional": true,
                "value": null
            }, 
            {
                "type": "FLOAT", 
                "name": "core_frequency_multiplier", 
                "desc": "Core frequency multiplier", 
                "long_desc": "Core frequency multiplier, which allows speeding up the CPU frequency.", 
                "optional": true,
                "value": null
            },
            {
                "type": "TEXT", 
                "name": "instructions", 
                "desc": "Instruction set", 
                "long_desc": "CPU instruction set (SSE, SSE4, MMX, etc)", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "core_vid", 
                "desc": "Core VID", 
                "long_desc": "Amount of voltage used on the CPU's stock settings (in volts).", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "package", 
                "desc": "Package", 
                "long_desc": "Describes the package that wraps the core, die, and does the link between the inside and the motherboard.", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "specification", 
                "desc": "Specification", 
                "long_desc": "Other specifications of the CPU.", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "stepping", 
                "desc": "Stepping", 
                "long_desc": "The frequency change step.", 
                "optional": true,
                "value": null
            }, 
            {
                "type": "TEXT", 
                "name": "technology", 
                "desc": "Technology", 
                "long_desc": "Lithography precision, in nanometers.",  
                "optional": true,
                "value": null
            }, 
            {
                "type": "URL", 
                "name": "reference", 
                "desc": "Reference URL", 
                "long_desc": "Link to a web page describing the CPU.", 
                "optional": true,
                "value": null
            },
            {
                "type": "TEXT", 
                "name": "micro_architecture", 
                "desc": "Micro architecture",
                "long_desc": "The micro architecture of the CPU. Only for x86_64 architecture. Must be one of the following values: {CORE2_65,CORE2_45, NEHALEM, SANDY_BRIDGE}", 
                "optional": true
            },
            {
                "type": "DATA_UID", 
                "name": "platform", 
                "desc": "The platform",
                "long_desc": "The UID or Alias of the platform.",
                "produced_by": "120ceae9-c9b7-4bf0-8b8f-acb38ee0749c",
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "format", 
                "value": "txt", 
                "desc": "Format", 
                "long_desc": "The format of the output. 'txt' will display the output in a human readable manner. 'json' will output a JSON string containing the same Information.",
                "optional": true
            }
        ]
    },
    "update": {
        "attributes": {
            "produce_data": false, 
            "name": "update",
            "desc": "Updates the entry.",
            "long_desc": "Updates the given CPU entry using the provided parameters."
        }, 
        "params": [
            {
                "type": "DATA_UID",
                "name": "entry",
                "desc": "Entry to update",
                "long_desc": "The UID or Alias of the CPU entry to update.",
                "produced_by": "b89800bc-8ec6-4b1a-a703-881568f36b7c"
            },
            {
                "produced_by": "5c952ce6-11c0-47e8-8f53-132f0515de31", 
                "type": "DATA_UID", 
                "name": "vendor", 
                "desc": "Vendor", 
                "long_desc": "The UID or Alias of the vendor that made the CPU.", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "codename", 
                "desc": "Codename", 
                "long_desc": "Code name of the CPU (i7, i5, P5142, etc).", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "model", 
                "desc": "Model", 
                "long_desc": "The model of the CPU.", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "family", 
                "desc": "Family", 
                "long_desc": "The family the CPU belongs to.", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "external_model", 
                "desc": "external model", 
                "long_desc": "The external model of the CPU", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "external_family", 
                "desc": "external family", 
                "long_desc": "The external family of the CPU", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "revision", 
                "desc": "Revision", 
                "long_desc": "The material revision of the CPU.", 
                "optional": true
            },
            {
                "type": "INTEGER", 
                "name": "number_of_cores", 
                "desc": "Number of cores", 
                "long_desc": "The number of cores the CPU holds.", 
                "optional": true
            }, 
            {
                "type": "INTEGER", 
                "name": "number_of_threads", 
                "desc": "Number of threads", 
                "long_desc": "The total number of threads accessible.", 
                "optional": true
            }, 
            {
                "type": "FLOAT", 
                "name": "core_frequency", 
                "desc": "Frequency", 
                "long_desc": "The core frequency, in Hz.",
                "optional": true
            }, 
            {
                "type": "FLOAT", 
                "name": "bus_frequency", 
                "desc": "Bus speed", 
                "long_desc": "The bus speed, in Hz.",
                "optional": true
            }, 
            {
                "type": "FLOAT", 
                "name": "fsb_speed", 
                "desc": "FSB speed", 
                "long_desc": "The fsb speed, in Hz.",
                "optional": true
            }, 
            {
                "type": "INTEGER", 
                "name": "core_frequency_multiplier", 
                "desc": "Core frequency multiplier", 
                "long_desc": "Core frequency multiplier, which allows speeding up the CPU frequency.", 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "instructions", 
                "desc": "Instruction set", 
                "long_desc": "CPU instruction set (SSE, SSE4, MMX, etc)", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "core_vid", 
                "desc": "Core VID", 
                "long_desc": "Amount of voltage used on the CPU's stock settings.", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "package", 
                "desc": "Package", 
                "long_desc": "Describes the package that wraps the core, die, and does the link between the inside and the motherboard.", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "specification", 
                "desc": "Specification", 
                "long_desc": "Other specifications of the CPU.", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "stepping", 
                "desc": "Stepping", 
                "long_desc": "The frequency change step.", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "technology", 
                "desc": "Technology", 
                "long_desc": "Lithography precision, in nanometers.", 
                "optional": true
            }, 
            {
                "type": "URL", 
                "name": "reference", 
                "desc": "Reference URL", 
                "long_desc": "Link to a web page describing the CPU.", 
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "micro_architecture", 
                "desc": "Micro architecture",
                "long_desc": "The micro architecture of the CPU. Only for x86_64 architecture. Must be one of the following values: {CORE2_65,CORE2_45, NEHALEM, SANDY_BRIDGE}", 
                "optional": true
            },
            {
                "type": "DATA_UID", 
                "name": "platform", 
                "desc": "The platform",
                "long_desc": "The UID or Alias of the platform.",
                "produced_by": "120ceae9-c9b7-4bf0-8b8f-acb38ee0749c",
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "format", 
                "value": "txt", 
                "desc": "Format", 
                "long_desc": "The format of the output. 'txt' will display the output in a human readable manner. 'json' will output a JSON string containing the same Information.",
                "optional": true
            }
        ]
    }
}
