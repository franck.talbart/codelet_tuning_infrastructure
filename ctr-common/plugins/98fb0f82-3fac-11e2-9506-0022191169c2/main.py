#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import cti_json, database_manager, database, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os


class TagPlugin(HapiPlugin):
    @hapi_command("add")
    def add_cmd(self, params):
        """ Add a tag to an entry

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          int
        """
        entry = params["entry"]
        tag = params["tag"]

        tag = tag.strip()
        tag = tag.replace(" ", "_")
        tag = tag.replace("\"", "")

        if tag:
            data_info = ctr.ctr_plugin_info_file_load_by_uid(entry)
            
            if not data_info:
                util.cti_plugin_print_error("Entry not found")
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            
            old_tag = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_TAG)
            if old_tag:
                values_tag = tag + " %s" % (old_tag)
            else:
                values_tag = tag
            
            ctr.ctr_plugin_info_put_value(data_info, cti.DATA_INFO_TAG, values_tag)
            ctr_info_name = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, 
                                                                        entry), 
                                            cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME))
            ctr.ctr_plugin_info_record_in_file(data_info, ctr_info_name)
            
            db = database.Database()
            if database_manager.insert("tag", {"id_entry_info": str(entry),
                                   "tag":tag},
                                db) is False:
                util.cti_plugin_print_error("Error while adding the tag into the database.")
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED
            
            print "Tag successfully added."
        else:
            print "Tag is empty."
        
        return 0
#---------------------------------------------------------------------------
        
    @hapi_command("rm")
    def rm_cmd(self, params):
        """ Remove a tag from an entry

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          int
        """
        entry = params["entry"]
        tag_number = params["tag_number"]
        
        data_info = ctr.ctr_plugin_info_file_load_by_uid(entry)
        
        if not data_info:
            util.cti_plugin_print_error("Entry not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        old_tag = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_TAG)
        
        tag = []
        if old_tag:
            tag = old_tag.split(" ")
        
        try:
            t = tag.pop(tag_number)
        except:
            util.cti_plugin_print_error("wrong index: %s." % tag_number)
            return 0
        
        tag = " ".join(tag)
        
        ctr.ctr_plugin_info_put_value(data_info, cti.DATA_INFO_TAG, tag)
        ctr_info_name = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, 
                                                                    entry), 
                                        cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME))
        ctr.ctr_plugin_info_record_in_file(data_info, ctr_info_name)
        db = database.Database()
        database_manager.delete("tag",
                                { 
                                   'L':{'NAME':["id_entry_info"], 'TYPE':"=", 'VAL': database_manager.uid2id(entry, db)},
                                   'LOGIC':'AND',
                                   'R':{'NAME':["tag"], 'TYPE':"=", 'VAL': t}
                                },
                                db)
        
        print "Tag successfully removed."
        
        return 0
#---------------------------------------------------------------------------

    @hapi_command("get")
    def get_cmd(self, params):
        """ Get a tag from an entry

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          int
        """
        entry = params["entry"]
        output_format = params["format"]
        
        data_info = ctr.ctr_plugin_info_file_load_by_uid(entry)
        tags = ctr.ctr_plugin_info_get_value(data_info, cti.DATA_INFO_TAG)
        
        if not data_info:
            util.cti_plugin_print_error("Entry not found")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        result = []
        if tags:
            tags = tags.split(" ")
            for tag in tags:
                    result.append(tag)
        
        if output_format == "json":
            print cti_json.cti_json_encode(result)
        elif output_format == "txt":
            for tag in result:
                print tag
            if result == []:
                print "No tag"
        else:
            util.cti_plugin_print_error("Unknown format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        
        return 0
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = TagPlugin()
    exit(p.main(sys.argv))
