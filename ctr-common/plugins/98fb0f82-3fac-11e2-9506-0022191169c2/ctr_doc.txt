== Description ==

This plugin allows the user to add tags to a specified data entry.

== Usage examples ==

<pre>
$ cti tag add my_entry "my_tag"
</pre>

<pre>
$ cti tag get my_entry
my_tag
</pre>

<pre>
$ cti tag rm my_entry 0
</pre>

