#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

if [ -z "$1" ]; then
    dir=$PWD
else
    dir=$1
    cd $dir
fi
echo "**************************************************************************************"
echo "* Compiling and installing all plugins ..."
echo "**************************************************************************************"
touch /tmp/compil_plugin.log

for i in $( ls $dir ); do 
    if [ -d $i ] && [ $i != "." ] && [ $i != ".." ] && [ $i != "" ]; then
        echo -n "* Preparing plugin $i ..."
        cd $i
        if [ -f "Makefile" ]; then
            echo ""
            echo "  Compiling ..."
            echo ""
            make >> /tmp/compil_plugin.log
        fi
        echo -e "\\033[1;32m" "[OK]" "\\033[0;39m"
        echo ""
        chmod +x run_plugin.sh
        cd ..
    fi
done
chmod a+w /tmp/compil_plugin.log
echo "Finished plugins compilation and installation."
echo "**************************************************************************************"

