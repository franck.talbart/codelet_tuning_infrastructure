#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import description, entry, alias, plugin, submitter, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, datetime, tempfile, glob, shutil

import import_vtune_results

class VtunePlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ prepare parameters
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        self.work_params = description.description_write(self.command, self.work_params)
        binary_uid = self.work_params[self.command].params["binary"][cti.META_ATTRIBUTE_VALUE]
        granularity = self.work_params[self.command].params["granularity"][cti.META_ATTRIBUTE_VALUE]
        mode = self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE]
        platform = self.work_params[self.command].params["platform"][cti.META_ATTRIBUTE_VALUE]
        format_o = self.work_params[self.command].params["format"][cti.META_ATTRIBUTE_VALUE]
        auto_run = self.work_params[self.command].params["auto_run"][cti.META_ATTRIBUTE_VALUE]
        
        # check parameters
        if not binary_uid:
            util.cti_plugin_print_error("The binary entry parameter is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    
        if granularity not in ['veryshort', 'short', 'medium', 'long']:
            util.cti_plugin_print_error("Unknown granularity %s." % granularity)
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    
        if not mode:
            mode = "local"
            self.work_params[self.command].params["mode"][cti.META_ATTRIBUTE_VALUE] = mode
        elif mode not in submitter.MODE_AVAIL:
            util.cti_plugin_print_error("Unknown mode.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        if mode != "local" and mode != "other" and not platform:
            util.cti_plugin_print_error("You must provide a platform on non-local mode.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS

        # create the alias
        binary_alias = alias.get_data_alias(binary_uid)
        if not binary_alias:
            binary_alias = ""
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        alias_e = "Vtune_%s_%s" % (binary_alias, date)
        
        (data_entry, output) = self.default_init_command(params, alias_e = alias_e, no_output = True)
        entry.update_entry_parameter(binary_uid, {"vtune_entry": {"value": data_entry.uid}})
        res = plugin.plugin_auto_run(self, data_entry, format_o, auto_run, output)
        return res
    
    #---------------------------------------------------------------------------
    
    @hapi_command("run")
    def run_cmd(self, params):
        """ This function gets a vtune entry (created using vtune init command) and runs Vtune on it.
        
        Args:
            self: class of the plugin
            params: working parameters
            
        Returns:
            Nothing
        """
        
        # gets parameters
        vtune_uid = params["entry"]
        prefix = None
        if params["prefix"]:
            prefix = os.path.abspath(params["prefix"])
        nb_run_vtune = params["nb_run_vtune"]
        loop_plugin_uid = params["loop_plugin_uid"]
        loop_group_plugin_uid = params["loop_group_plugin_uid"]
        
        (input_param, output_param) = entry.load_data(vtune_uid)        
        binary_uid = output_param["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        run_parameters = output_param["init"].params["run_parameters"][cti.META_ATTRIBUTE_VALUE]
        granularity = output_param["init"].params["granularity"][cti.META_ATTRIBUTE_VALUE]
        stability_sanity_check = output_param["init"].params["stability_sanity_check"][cti.META_ATTRIBUTE_VALUE]
        mode = output_param["init"].params["mode"][cti.META_ATTRIBUTE_VALUE]
        partition = output_param["init"].params["partition"][cti.META_ATTRIBUTE_VALUE]
        platform = output_param["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
        prefix_execute_script = output_param["init"].params["prefix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        suffix_execute_script = output_param["init"].params["suffix_execute_script"][cti.META_ATTRIBUTE_VALUE]
        submitter_cmd = output_param["init"].params["submitter_cmd"][cti.META_ATTRIBUTE_VALUE]
        submitter_opt = output_param["init"].params["submitter_opt"][cti.META_ATTRIBUTE_VALUE]
        submitter_user = output_param["init"].params["submitter_user"][cti.META_ATTRIBUTE_VALUE]

        
        (_, output_binary) = entry.load_data(binary_uid)
        
        compile_uid = output_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]

        output_compile = None
        if compile_uid:
            (_, output_compile) = entry.load_data(compile_uid)

        
        ## get the binary path from the binary entry
        binary = output_binary["init"].params["binary_file"][cti.META_ATTRIBUTE_VALUE]
        binary_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, binary_uid), 
                                   cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                   binary)

        
        # get compiled application path
        compiled_application = None
        compiled_application_path = None
        if output_compile:
            compiled_application = output_compile["init"].params["compiled_application"][cti.META_ATTRIBUTE_VALUE]
            if compiled_application:
                compiled_application_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, compile_uid), 
                                                     cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                                     compiled_application)

        
        work_dir = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)

        # create the arguments
        dict_opt = {}
        dict_opt["APP"] = [binary_path]
        if compiled_application_path:
            dict_opt["OTHER"] = [compiled_application_path]
        dict_opt["SCRIPTS"] = [prefix]
        dict_opt["platform"] = platform
        dict_opt["stability_sanity_check"] = stability_sanity_check
        dict_opt["entry"] = vtune_uid
        dict_opt["plugin_execute_script"] = os.path.join(self.plugin_directory, "execute_vtune.sh")
        dict_opt["partition"] = partition
        dict_opt["submitter_cmd"] = submitter_cmd 
        dict_opt["submitter_opt"] = submitter_opt 
        dict_opt["submitter_user"] = submitter_user 
        dict_opt["prefix_execute_script"] = prefix_execute_script
        dict_opt["suffix_execute_script"] = suffix_execute_script
        dict_opt["username"] = self.username
        dict_opt["password"] = self.password

        dict_opt["script_params"] = tempfile.mkstemp(dir=work_dir)[1]
        script_params_f = open(dict_opt["script_params"], "w")
        script_params_f.write(str(nb_run_vtune) + "\n")
        set_freq_script = os.path.join(cti.cti_plugin_config_get_value(cti.COMMON_TOOLS_DIR), "set_freq.sh")
        script_params_f.write(set_freq_script + "\n")
        script_params_f.write(str(granularity) + "\n")
        script_params_f.write(str(stability_sanity_check) + "\n")
        if not compiled_application:
            compiled_application = ""
        script_params_f.write(compiled_application + "\n")
        if run_parameters is None:
            run_parameters = ""
        script_params_f.write(run_parameters + "\n")
        script_params_f.close()

        (job_dir, daemon_log_file) = submitter.submitter(dict_opt, self.plugin_uid, mode)

        execution_log_file = None
        # collect execution log file
        for f in glob.glob("*.out"):
            if f != daemon_log_file:
                execution_log_file = os.path.abspath(f)

        dict_opt["result_dir"] = os.path.join(work_dir, job_dir + "_local", job_dir, "RESULTS")
        vtune_version = "NA"
        vtune_version_path = os.path.join(dict_opt["result_dir"], "vtune_version")
        if os.path.isfile(vtune_version_path):
            vtune_version_file = open(vtune_version_path, "r")
            vtune_version = " ".join(vtune_version_file.readlines())
        else:
            print "Vtune version file not found!"

        message_daemon = ""
        file_daemon = open(daemon_log_file, "a")
        nb_files = 0
        for f in os.walk("RESULTS"): 
             nb_files += len(f[2])

        dict_opt["nb_run_vtune"] = nb_run_vtune
        dict_opt["loop_group_plugin_uid"] = loop_group_plugin_uid
        dict_opt["loop_plugin_uid"] = loop_plugin_uid
        dict_opt["vtune_version"] = vtune_version
        dict_opt["log_execution"] = execution_log_file
        dict_opt["log_daemon"] = daemon_log_file

        if nb_files == 0:
            execution_log_f = open(execution_log_file, "r")
            execution_log_message = execution_log_f.readlines()
            execution_log_f.close()
            execution_log_message = " ".join(execution_log_message)

            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] ERROR: no csv file generated\n\tThe error message is:\n\
                    ==============================\n%s\n==============================\n\
                    Temporary files are located in the directory %s" \
                    % (date, execution_log_message, os.getcwd())
            util.cti_plugin_print_error(message)
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            import_vtune_results.import_results(dict_opt)

        else:
            date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
            message = "[%s] Importing results into CTI..." % date
            print message
            message_daemon += message
            file_daemon.write(message_daemon)
            file_daemon.close()

            import_vtune_results.import_results(dict_opt)

            # cleaning temporary files
            shutil.rmtree(os.path.join(work_dir, job_dir + "_local"))

        
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = VtunePlugin()
    exit(p.main(sys.argv))
