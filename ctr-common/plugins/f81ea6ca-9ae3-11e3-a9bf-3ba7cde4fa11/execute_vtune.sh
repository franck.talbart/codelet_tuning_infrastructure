#!/bin/bash --norc
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit


#Constants & global
script_params="script_params"
vtune_bin="amplxe-cl"

work_dir=`pwd`

echo "[`date +"%Y/%m/%d %H:%M:%S"`] Begin execution on #`uname -n`"

nb_run_vtune=
set_freq_script=
granularity=
stability_sanity_check=
compiled_application=
run_parameters=
#Read script parameters
i=1
while read line; do
    if [ $i -eq 1 ]; then
        nb_run_vtune=$line
        let "i++"
    elif [ $i -eq 2 ]; then
        set_freq_script=$line
        let "i++"
    elif [ $i -eq 3 ]; then
        granularity=$line
        let "i++"
    elif [ $i -eq 4 ]; then
        stability_sanity_check=$line
        let "i++"
    elif [ $i -eq 5 ]; then
        compiled_application=$line
        let "i++"
    elif [ $i -eq 6 ]; then
        run_parameters=$line
    fi
done < $script_params


let "relevance_threshold_min=($nb_run_vtune*8)/10"

echo -e "\n* Number of Vtune run: $nb_run_vtune"
echo "* Stability_sanity_check: $stability_sanity_check"
echo "* Relevance_threshold_min: $relevance_threshold_min"

# get and untar the application
cd $work_dir/OTHER

if [ -n "$compiled_application" ]; then
    tar xf $compiled_application
    rm $compiled_application
fi
application_dir="$work_dir/OTHER"

# get and untar the binary
cd $work_dir/"APP"
binary_name=`ls`
tar xf $binary_name
rm $binary_name
binary_name=`basename $binary_name .tar.gz`
cp $binary_name $application_dir

cd $application_dir

echo -e "\n##########################################################################"
echo "WARNING: Please make sure Hyperthreading and Turboboost are disabled."
echo "On top of that, the frequency of the CPU should be stabilized. To do so, please use cpuspeed."
echo "A script to set the frequency can be found here: '$set_freq_script'."
echo "WARNING: The application must be compiled using the -g flag."
echo -e "##########################################################################\n"

echo -e "Running Vtune.\n"


if [ -e $work_dir/SCRIPTS/prefix.sh ]; then
    echo -e "Sourcing prefix.sh\n"
    source $work_dir/SCRIPTS/prefix.sh
fi

for j in `seq 1 $nb_run_vtune`; do
    res_dir="run_$j"
    
    vtune_cmd="$vtune_bin -collect advanced-hotspots -target-duration-type $granularity -loop-mode loop-only -result-dir $res_dir -- ./$binary_name $run_parameters"
    
    # run Vtune on the binary
    echo -e "\nRun number $j..\n"
    echo $vtune_cmd
    $vtune_cmd 2>&1 | tee vtune_exec
    
    if [ -e $res_dir ]; then
        # compress the results directory
        tar acf $work_dir/RESULTS/run_$j.tar.gz $res_dir
        
        res_csv="$work_dir/RESULTS/results_$j.csv"
        vtune_report_cmd="$vtune_bin -report hotspots -result-dir $res_dir -format csv -csv-delimiter semicolon -report-output $res_csv -group-by function,source-file,source-line"
        # get the results
        echo $vtune_report_cmd
        $vtune_report_cmd
        
        if [ ! -e $res_csv ]; then
            echo "ERROR: no csv file produced for run $j!" 1>&2
        fi
        
        # get the walltime
        cat vtune_exec | grep "Elapsed Time" | grep -Eo "[0-9]+\.[0-9]+" >> $work_dir/RESULTS/walltime
        
        rm -r $res_dir
    else
        echo "ERROR on run $j!" 1>&2
    fi
done

# record the Vtune version
$vtune_bin --version | head -n 1 > $work_dir/RESULTS/vtune_version

echo -e "\nEnd of Vtune execution."
