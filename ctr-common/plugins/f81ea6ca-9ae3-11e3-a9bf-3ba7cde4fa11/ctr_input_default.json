{
    "init": {
        "attributes": {
            "long_desc": "Creates a vtune entry using the given parameters.", 
            "produce_data": true, 
            "name": "init", 
            "repository": "local", 
            "desc": "Initializes data for Vtune analysis and prepare the compile process if needed."
        }, 
        "params": [
            {
                "type": "DATA_UID", 
                "name": "binary", 
                "desc": "binary entry", 
                "long_desc": "If binary entry is specified, the plugin will not compile the application.", 
                "produced_by": "4ac29bf3-dfd3-4ac5-acb4-bcfb51ddc138",
                "value": null                
            },
            {
                "type": "TEXT",
                "name": "run_parameters", 
                "desc": "Run parameters",
                "long_desc": "Binary run parameters.", 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "granularity", 
                "desc": "Vtune granularity option", 
                "long_desc": "There are 4 available granularities depending of the given application wall time (veryshort, short, medium and long).", 
                "optional": true, 
                "value": "medium"
            },
            {
                "type": "FLOAT", 
                "name": "stability_sanity_check", 
                "desc": "Stability",
                "long_desc": "the sanity_check stability number", 
                "optional": true, 
                "value": 0.05
            }, 
            {
                "type": "TEXT", 
                "name": "mode", 
                "desc": "Mode of execution",
                "long_desc": "The mode of execution for the vtune process. 'local' for an execution on the local machine. 'ssh' to execute in on a distant machine via a direct connection. 'slurm' to use the slurm system to allocate and run on a distant machine. 'other' to use an other system to allocate and run on a distant machine.",
                "optional": true,
                "value": "local"
            }, 
            {
                "type": "TEXT", 
                "name": "partition", 
                "desc": "Slurm Partition", 
                "long_desc": "The Slurm partition, used by sbatch. This parameter will decide on which partition the user send his job. See the slurm documentation for more information.",
                "optional": true,
                "value": "regular"
            },
            {
                "type": "DATA_UID",
                "name": "platform",
                "desc": "Platform",
                "long_desc": "The UID or Alias of the platform that will be used to launch Vtune on.",
                "produced_by": "120ceae9-c9b7-4bf0-8b8f-acb38ee0749c",
                "optional": true
            },
            {
                "type": "FILE",
                "name": "prefix_execute_script",
                "desc": "Prefix execute script",
                "long_desc": "The prefix of the script that will be launched on clusters.",
                "optional": true,
                "value": null
            },
            {
                "type": "FILE",
                "name": "suffix_execute_script",
                "desc": "Suffix execute script",
                "long_desc": "The suffix of the script that will be launched on clusters.",
                "optional": true,
                "value": null
            },
            {
                "type": "TEXT",
                "name": "submitter_cmd",
                "desc": "Submitter command",
                "long_desc": "The submitter command (ex: sbatch, llsubmit).",
                "optional": true,
                "value": "sbatch"
            },
            {
                "type": "TEXT",
                "name": "submitter_opt",
                "desc": "submitter options",
                "long_desc": "The submitter options.",
                "optional": true,
                "value": null
            },
            {
                "type": "TEXT",
                "name": "submitter_user",
                "desc": "submitter user",
                "long_desc": "The submitter user.",
                "optional": true,
                "value": null
            },
            {
                "type": "TEXT", 
                "name": "format", 
                "desc": "Format",
                "long_desc": "The format of the output. 'txt' will display the output in a human readable manner. 'json' will output a JSON string containing the same informations.", 
                "optional": true, 
                "value": "txt"
            }, 
            {
                "type": "BOOLEAN", 
                "name": "auto_run", 
                "desc": "Auto run?",
                "long_desc": "false: Do not call the run command after creation. true: automatically call the run command after creation.", 
                "optional": true, 
                "value": true
            }
        ]
    },
    "run": {
        "attributes": {
            "long_desc": "Launches the Vtune tool using the given vtune entry.", 
            "produce_data": false, 
            "name": "run", 
            "desc": "Runs Vtune on given data"
        }, 
        "params": [
            {
                "type": "DATA_UID", 
                "name": "entry", 
                "desc": "The vtune entry",
                "long_desc": "The UID or Alias of the vtune entry to run.", 
                "produced_by": "f81ea6ca-9ae3-11e3-a9bf-3ba7cde4fa11"
            },
            {
                "type": "FILE", 
                "name": "prefix", 
                "desc": "Prefix script",
                "long_desc": "The script to source before each iteration of the process.",
                "optional": true,
                "value": null
            },
            {
                "type": "INTEGER", 
                "name": "nb_run_vtune", 
                "desc": "repetition number",
                "long_desc": "The number of vtune run", 
                "optional": true,
                "value": 10
            },
            {
                "type": "PLUGIN_UID", 
                "name": "loop_plugin_uid", 
                "desc": "Loop plugin UID", 
                "long_desc": "The UID or Alias of the loop entry.", 
                "value": "06e2e012-5b61-11e3-9170-d3f36bd3c5f9",
                "optional": true
            }, 
            {
                "type": "PLUGIN_UID", 
                "name": "loop_group_plugin_uid", 
                "desc": "Loop_group plugin UID", 
                "long_desc": "The UID or Alias of the loop_group entry.", 
                "value": "2b54ef94-5b61-11e3-8722-7fd2ea71b72a",
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "format", 
                "desc": "Format",
                "long_desc": "The format of the output. 'txt' will display the output in a human readable manner. 'json' will output a JSON string containing the same informations.", 
                "optional": true, 
                "value": "txt"
            }
        ]
    }, 
    "update": {
        "attributes": {
            "long_desc": "Updates the given vtune entry using the provided parameters.", 
            "produce_data": false, 
            "name": "update", 
            "desc": "Updates the entry."
        }, 
        "params": [
            {
                "type": "DATA_UID", 
                "name": "entry", 
                "desc": "The vtune entry",
                "long_desc": "The UID or Alias of the vtune entry to fill.", 
                "produced_by": "f81ea6ca-9ae3-11e3-a9bf-3ba7cde4fa11"
            }, 
            {
                "type": "DATA_UID", 
                "name": "binary", 
                "desc": "Binary entry", 
                "long_desc": "The UID or Alias of the binary entry.", 
                "produced_by": "4ac29bf3-dfd3-4ac5-acb4-bcfb51ddc138", 
                "optional": true
            },
            {
                "type": "TEXT",  
                "name": "run_parameters", 
                "desc": "Run parameters",
                "long_desc": "Binary run parameters.", 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "granularity", 
                "desc": "Vtune granularity option", 
                "long_desc": "There are 4 available granularities depending of the given application wall time (veryshort, short, medium and long).", 
                "optional": true
            },
            {
                "type": "FLOAT", 
                "name": "stability_sanity_check", 
                "desc": "Stability",
                "long_desc": "The sanity_check stability number.", 
                "optional": true
            },
            {
                "type": "FLOAT", 
                "name": "total_coverage_loop", 
                "desc": "Application total coverage time", 
                "long_desc": "The application total coverage time (in percentage).",
                "optional": true
            },
            {
                "type": "INTEGER",
                "name": "walltime",
                "desc": "Walltime",
                "long_desc": "The application's loops walltime.",
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "mode", 
                "desc": "Mode of execution",
                "long_desc": "The mode of execution for the vtune process. 'local' for an execution on the local machine. 'ssh' to execute in on a distant machine via a direct connection. 'slurm' to use the slurm system to allocate and run on a distant machine. 'other' to use an other system to allocate and run on a distant machine.",
                "optional": true
            }, 
            {
                "type": "TEXT", 
                "name": "partition", 
                "desc": "Slurm Partition", 
                "long_desc": "The Slurm partition, used by sbatch. This parameter will decide on which partition the user send his job. See the slurm documentation for more information.",
                "optional": true
            },
            {
                "type": "DATA_UID",
                "name": "platform",
                "desc": "Platform",
                "long_desc": "The UID or Alias of the platform that will be used to launch Vtune on.",
                "produced_by": "120ceae9-c9b7-4bf0-8b8f-acb38ee0749c",
                "optional": true
            },
            {
                "type": "FILE",
                "name": "prefix_execute_script",
                "desc": "Prefix execute script",
                "long_desc": "The prefix of the script that will be launched on clusters.",
                "optional": true
            },
            {
                "type": "FILE",
                "name": "suffix_execute_script",
                "desc": "Suffix execute script",
                "long_desc": "The suffix of the script that will be launched on clusters.",
                "optional": true
            },
            {
                "type": "TEXT",
                "name": "submitter_cmd",
                "desc": "Submitter command",
                "long_desc": "The submitter command (ex: sbatch, llsubmit).",
                "optional": true
            },
            {
                "type": "TEXT",
                "name": "submitter_opt",
                "desc": "submitter options",
                "long_desc": "The submitter options.",
                "optional": true
            },
            {
                "type": "TEXT",
                "name": "submitter_user",
                "desc": "submitter user",
                "long_desc": "The submitter user.",
                "optional": true
            },
            {
                "type": "INTEGER", 
                "name": "nb_run_vtune", 
                "desc": "Repetition number",
                "long_desc": "The number of vtune run", 
                "optional": true
            },
            {
                "type": "FILE",
                "name": "logs",
                "desc": "Log files",
                "long_desc": "A list of paths to logs corresponding to the Vtune run.", 
                "optional": true,
                "list": true
            },
            {
                "type": "FILE",
                "name": "csv_output",
                "desc": "The CSV files produced by Vtune",
                "long_desc": "A list of paths to CSV files produced by Vtune.", 
                "optional": true,
                "list": true
            },
            {
                "type": "FILE",
                "name": "run_files",
                "desc": "The files from the Vtune run",
                "long_desc": "A list of paths to archive files produced by Vtune.",
                "optional": true,
                "list": true
            },
            {
                "type": "DATA_UID", 
                "name": "loop_groups", 
                "desc": "Loop groups",
                "long_desc": "A list of UIDs or Aliases of loop_group entries.",
                "list": true,
                "optional": true,
                "produced_by": "2b54ef94-5b61-11e3-8722-7fd2ea71b72a", 
                "target": "vtune"
            },
            {
                "type": "TEXT", 
                "name": "format", 
                "desc": "Format",
                "long_desc": "The format of the output. 'txt' will display the output in a human readable manner. 'json' will output a JSON string containing the same informations.", 
                "optional": true, 
                "value": "txt"
            }
        ]
    }
}
