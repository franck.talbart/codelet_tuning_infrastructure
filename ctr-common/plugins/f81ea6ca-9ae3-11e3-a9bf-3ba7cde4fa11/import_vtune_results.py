#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import entry, plugin, alias, util

import sys, os, numpy, glob, tempfile, datetime, csv, time

#constants & global
csv_source_infos = "Function"
csv_time_sec = "CPU Time:Self"
csv_source_file = "Source File"
csv_source_line = "Source Line"

def import_results(dict_opt):
    message = ""
    message_daemon = ""

    work_dir = os.getcwd()
    if dict_opt["result_dir"]:
        relevance_threshold_min = (dict_opt["nb_run_vtune"] * 8) / 10
        
        # repository type
        repository = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, dict_opt["entry"])
        # if the repository is local, we need its UID
        if repository == cti.CTR_REP_LOCAL:
            repository_path = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(dict_opt["entry"], cti.CTR_DATA_DIR)
            repository = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(repository_path))
        
        list_run_loops = {}
        list_loops = {}
        list_total_time = []
        csv_to_import = []
        # process all csv files vtune produced
        list_csv = glob.glob(os.path.join(dict_opt["result_dir"], "*.csv"))
        for csv_file in list_csv:
            run_total_time = 0
            csv_file = os.path.join(dict_opt["result_dir"], csv_file)
            try:
                # import the log file into vtune entry
                vtune_output_f = entry.put_file_in_entry(
                                    dict_opt["entry"], 
                                    csv_file, 
                                    False
                                 )
                csv_to_import.append(vtune_output_f)
            except IOError:
                message = "CTI error: No such file: '" + csv_file + "' \n"
                util.cti_plugin_print_error(message)
                message_daemon += message
            
            vtune_output_file = open(csv_file, "r")
            output_content = csv.DictReader(vtune_output_file, delimiter=";")
            
            list_run_loops[csv_file] = {}
            for line in output_content:
                source_infos = line[csv_source_infos].split(" ")
                fct_name = source_infos[-1].rstrip("]")
                try:
                    first_line = int(line[csv_source_line])
                except ValueError:
                    first_line = None
                time_sec = float(line[csv_time_sec])
                file_name = line[csv_source_file]
                loopID = "%s_%s" % (first_line, fct_name)
                
                if first_line != -1:
                    list_run_loops[csv_file][loopID] = None
                    if list_loops.has_key(loopID):
                        list_loops[loopID]["time_sec"].append(time_sec)
                    else:
                        list_loops[loopID] = {}
                        list_loops[loopID]["fct_name"] = fct_name
                        list_loops[loopID]["first_line"] = first_line
                        list_loops[loopID]["last_line"] = None
                        list_loops[loopID]["file_name"] = file_name
                        list_loops[loopID]["nb_sample"] = None
                        list_loops[loopID]["loop_type"] = None
                        list_loops[loopID]["time_sec"] = [time_sec]
                        list_loops[loopID]["CPI_ratio"] = None
                run_total_time += time_sec
            
            vtune_output_file.close()
            list_total_time.append(run_total_time)
        
        walltime = None
        walltime_file_path = os.path.join(dict_opt["result_dir"], "walltime")
        if os.path.isfile(walltime_file_path):
            walltime_file = open(walltime_file_path, "r")
            walltime_lines = walltime_file.readlines()
            walltime_values = []
            for walltime_line in walltime_lines:
                walltime_values.append(float(walltime_line.rstrip('\n')))
            walltime_file.close()
            walltime = float(numpy.median(walltime_values))
            if numpy.isnan(walltime):
                message = "\nWalltime: No walltime\n\n"
                walltime = None
            else:
                message = "\nWalltime: %s sec\n\n" % walltime
        else:
            message = "\nWalltime: No walltime\n\n"
        print message,
        message_daemon += message
        
        # creation of loop_group
        json = \
        {
            "init":
            {
                "attributes" : {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
                "params": 
                [
                    {cti.META_ATTRIBUTE_NAME : "vtune",cti.META_ATTRIBUTE_VALUE : dict_opt["entry"]},
                    {cti.META_ATTRIBUTE_NAME : "nb_loops",cti.META_ATTRIBUTE_VALUE : len(list_loops)},
                    {cti.META_ATTRIBUTE_NAME : "profiler_version",cti.META_ATTRIBUTE_VALUE : dict_opt["vtune_version"]},
                    {cti.META_ATTRIBUTE_NAME : "loops",cti.META_ATTRIBUTE_VALUE : []}
                ]
            }
        }
        
        output = ""
        try:
            output = plugin.execute_plugin_by_file(dict_opt["loop_group_plugin_uid"], json, dict_opt["username"], dict_opt["password"])
        except :
            import logging
            logging.exception("")
            print json
            print sys.exc_info()[1]
            exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        
        loop_group_uid = plugin.get_output_data_uid(output)
       
        # get the compiled application
        (_, output_param_vtune) = entry.load_data(dict_opt["entry"])
        binary_entry = output_param_vtune["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        (_, output_param_binary) = entry.load_data(binary_entry)
        compile_entry = output_param_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]
        
        #Alias for loop_group
        app_name = alias.get_data_alias(binary_entry)
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        if not app_name:
            alias_loop_group = "Loop_group_{0}".format(date)
        else:
            alias_loop_group = "{0}_Loop_group_{1}".format(app_name, date)
        
        if alias.set_data_alias(loop_group_uid, alias_loop_group) == 0:
            util.cti_plugin_print_warning("Cannot set the alias %s (already used?)" % (alias_loop_group))
        
        (_, output_param_compile_entry) = entry.load_data(compile_entry)
        compiled_application = output_param_compile_entry["init"].params["compiled_application"][cti.META_ATTRIBUTE_VALUE]
        if compiled_application:
            compiled_application_path = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, compile_entry),
                                                 cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                                 compiled_application)
        
        tmp_dir = tempfile.mkdtemp()
        os.chdir(tmp_dir)
       
        if compiled_application:
            os.system("cp %s ." % compiled_application_path)
            os.system("tar xf " + compiled_application)
            os.remove(compiled_application)
        
        message = "* List of loops detected by Vtune on the application:\n"
        print message,
        message_daemon += message
        # create loops entries
        for loopID in list_loops.keys():
            list_loops[loopID]["time_median"] = float(numpy.median(list_loops[loopID]["time_sec"]))
            message = "\t* Loop %s from %s (function %s), line: %s, time (median): %s\n\n" % \
                (loopID, list_loops[loopID]["file_name"], list_loops[loopID]["fct_name"], list_loops[loopID]["first_line"], list_loops[loopID]["time_median"])
            print message,
            message_daemon += message
            
            loop_language = None
            if list_loops[loopID]["file_name"]:
                # loop language
                if ".c" in list_loops[loopID]["file_name"].lower():
                    loop_language = "c"
                else:
                    loop_language = "fortran"
            
            # stability
            if len(list_loops[loopID]["time_sec"]) >= relevance_threshold_min:
                mini = min(list_loops[loopID]["time_sec"])
                if mini == 0:
                    list_loops[loopID]["stability"] = "unstable"
                else:
                    s_check = (list_loops[loopID]["time_median"] - mini) / mini
                    if s_check <= dict_opt["stability_sanity_check"]:
                        list_loops[loopID]["stability"] = "stable"
                    else:
                        list_loops[loopID]["stability"] = "unstable"
            else:
                list_loops[loopID]["stability"] = "unstable"
            
            if walltime is not None:
                loop_coverage_median = (float(list_loops[loopID]["time_median"]) / walltime) * 100.0
            else:
                loop_coverage_median = None
            
            # creation of loop
            json = \
            {
                "init":
                {
                    "attributes" : {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "loop_id",cti.META_ATTRIBUTE_VALUE : loopID},
                        {cti.META_ATTRIBUTE_NAME : "first_line",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["first_line"]},
                        {cti.META_ATTRIBUTE_NAME : "end_line",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["last_line"]},
                        {cti.META_ATTRIBUTE_NAME : "language",cti.META_ATTRIBUTE_VALUE : loop_language},
                        {cti.META_ATTRIBUTE_NAME : "source_function_name",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["fct_name"]},
                        {cti.META_ATTRIBUTE_NAME : "coverage_vtune",cti.META_ATTRIBUTE_VALUE : loop_coverage_median},
                        {cti.META_ATTRIBUTE_NAME : "status_vtune",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["stability"]},
                        {cti.META_ATTRIBUTE_NAME : "time_sec",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["time_median"]},
                        {cti.META_ATTRIBUTE_NAME : "nb_sample",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["nb_sample"]},
                        {cti.META_ATTRIBUTE_NAME : "cpi_ratio",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["CPI_ratio"]},
                        {cti.META_ATTRIBUTE_NAME : "loop_files",cti.META_ATTRIBUTE_VALUE : None},
                        {cti.META_ATTRIBUTE_NAME : "loop_type",cti.META_ATTRIBUTE_VALUE : list_loops[loopID]["loop_type"]},
                        {cti.META_ATTRIBUTE_NAME : "loop_group",cti.META_ATTRIBUTE_VALUE : loop_group_uid}
                    ]
                }
            }
            
            output = ""
            try:
                output = plugin.execute_plugin_by_file(dict_opt["loop_plugin_uid"], json, dict_opt["username"], dict_opt["password"])
            except :
                print sys.exc_info()[1]
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            
            loop_uid = plugin.get_output_data_uid(output)
            
            # alias for loop
            now = datetime.datetime.now()
            date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
            if not app_name:
                alias_loop = "Loop{0}_{1}".format(loopID, date)
            else:
                alias_loop = "{0}_Loop{1}_{2}".format(app_name, loopID, date)
            if alias.set_data_alias(loop_uid, alias_loop) == 0:
                util.cti_plugin_print_warning("Cannot set the alias %s (already used?)" % (alias_loop))
            list_loops[loopID]["loop_uid"] = loop_uid
        
        os.system("rm -rf " + tmp_dir)
        
        total_coverage_loop = None
        if list_total_time and walltime is not None:
            total_coverage_loop = (float(numpy.median(list_total_time)) / walltime) * 100.0
            
        run_files_to_import = []
        # process all run files vtune produced
        list_run = glob.glob(os.path.join(dict_opt["result_dir"], "*.tar.gz"))
        for run_file in list_run:
            run_file = os.path.join(dict_opt["result_dir"], run_file)
            try:
                # import the log file into vtune entry
                vtune_output_f = entry.put_file_in_entry(
                                    dict_opt["entry"], 
                                    run_file, 
                                    False
                                 )
                run_files_to_import.append(vtune_output_f)
            except IOError:
                message = "CTI error: No such file: '" + run_file + "' \n"
                util.cti_plugin_print_error(message)
                message_daemon += message
        
        # update the vtune entry
        entry.update_entry_parameter(dict_opt["entry"], {"total_coverage_loop": {"value": total_coverage_loop},
                                                   "walltime": {"value": walltime},
                                                   "csv_output": {"value": csv_to_import, "append": True},
                                                   "run_files": {"value": run_files_to_import, "append": True}
                                                  })

    os.chdir(work_dir)

    # import exectution log file into vtune
    execution_log_file = ""
    try:
        execution_log_file = entry.put_file_in_entry(
                                dict_opt["entry"], 
                                dict_opt["log_execution"], 
                                False
                             )
    except IOError:
        message = "CTI error: No such file: '" + dict_opt["log_execution"] + "' \n"
        util.cti_plugin_print_error(message)
        message_daemon += message

    file_daemon = open(dict_opt["log_daemon"], "a")
    file_daemon.write(message_daemon)
    file_daemon.close()
    # import daemon log file into vtune
    daemon_log_file = ""
    try:
        daemon_log_file = entry.put_file_in_entry(
                            dict_opt["entry"], 
                            dict_opt["log_daemon"], 
                            False
                          )
    except IOError:
        util.cti_plugin_print_error("No such file: '" + dict_opt["log_daemon"] + "'")

    entry.update_entry_parameter(dict_opt["entry"], {"logs": {"value": [daemon_log_file, execution_log_file], "append": True},
                                                    "nb_run_vtune": {"value": dict_opt["nb_run_vtune"]}
                                                   })

    time_process = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime())
    print "\n[" + str(time_process) + "] End of process"


