#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi.main import HapiPlugin, hapi_command
from cti_hapi import util

import util as util_l, list_plugins, list_repositories

import sys

class ListPlugin(HapiPlugin):    
    @hapi_command("data")
    def data_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        
        util_l.run_query(self, params)
#---------------------------------------------------------------------------

    @hapi_command("plugins")
    def plugin_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        
        info = list_plugins.get_list_plugins(params["repository_type"], params["type"], self)
        
        if params["format"] == "json":
           list_plugins.list_json_plugins(info, self, params)
        elif params["format"] == "txt":
           list_plugins.list_txt_plugins(info)
        elif params["format"] == "raw":
           list_plugins.list_raw_plugins(info)
        else:
            util.cti_plugin_print_error("Unknown format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------

    @hapi_command("local_repositories")
    def local_repositories_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        info = list_repositories.get_list_local_repositories()
        if params["format"] == "json":
           list_repositories.list_json_local_repositories(info, 
                                                            self, 
                                                            params)
        elif params["format"] == "txt":
           list_repositories.list_txt_local_repositories(info)
        else:
           util.cti_plugin_print_error("Unknown format")
           return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------

    def check_passwd(self):
        # Needed by self_test and cti --help
        if self.command == "plugins":
            return True
        return False
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = ListPlugin()
    exit(p.main(sys.argv))
