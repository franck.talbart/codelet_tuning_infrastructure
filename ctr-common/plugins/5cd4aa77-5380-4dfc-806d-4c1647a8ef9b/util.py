#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi import plugin

import cti

import sys
    
def run_query(self, params):
    """ Run a query (needed to list data) and display the output
    Returns:
      None
    """
    
    repository_type = params["repository_type"]
    query_uid = params["query_uid"]
    format = "txt"
    pagination = params["pagination"]
    
    json_file = \
    {
        "select": 
        {
            "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
            "params": 
            [
                {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : repository_type},
                {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : "*"},
                {cti.META_ATTRIBUTE_NAME : "pagination",cti.META_ATTRIBUTE_VALUE : pagination},
                {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : format}
            ]
        }
    }
    sys.stdout.flush()
    try:
        plugin.execute_plugin_by_file(query_uid,
                                      json_file,
                                      self.username,
                                      self.password,
                                      True)
    except:
        print sys.exc_info()[1]
        return cti.CTI_PLUGIN_ERROR_UNEXPECTED