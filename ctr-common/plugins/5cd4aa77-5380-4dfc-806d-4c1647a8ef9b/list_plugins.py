#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import cti_json, util_uid, alias, database, database_manager, util

def get_list_plugins(repository_type, type, self):
    """ return a set of plugins from a repository

    Args:
      repository_type: the type of the repository (common / local / temp / ctr uid)
      type: the plugin type

    Returns:
      the list of plugins
    """
  
    db = database.Database()

    if repository_type == cti.COMMON_REPOSITORY:
        constraints = {'NAME':["repository_type"], 'TYPE':"=", 'VAL':cti.COMMON_REPOSITORY}
    elif repository_type == cti.TEMP_REPOSITORY:
        constraints = {'NAME':["repository_type"], 'TYPE':"=", 'VAL':cti.TEMP_REPOSITORY}
    elif repository_type == cti.LOCAL_REPOSITORY:
        constraints = {'NAME':["repository_type"], 'TYPE':"=", 'VAL':cti.LOCAL_REPOSITORY}
    elif util_uid.is_valid_uid(repository_type):
        constraints = { 
                       'L':{'NAME':["repository_type"], 'TYPE':"=", 'VAL': cti.LOCAL_REPOSITORY},
                       'LOGIC':'AND',
                       'R':{'NAME':["repository_uid"], 'TYPE':"=", 'VAL': repository_type}
                      }
    elif cti.cti_plugin_alias_repository_get_value(str(repository_type)):
        rep = cti.cti_plugin_alias_repository_get_value(repository_type)
        constraints = { 
                       'L':{'NAME':["repository_type"], 'TYPE':"=", 'VAL': cti.LOCAL_REPOSITORY},
                       'LOGIC':'AND',
                       'R':{'NAME':["repository_uid"], 'TYPE':"=", 'VAL': str(rep)}
                      }
    elif repository_type == cti.ALL_REPOSITORY:
        constraints = {}
    else:
        util.cti_plugin_print_error("Unknown repository")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    if type != "all":
        constraints = {'NAME':["type"], 'TYPE':"=", 'VAL':type}
    result = database_manager.search(constraints,
                                    db,
                                    "plugin",
                                    [
                                        "plugin.plugin_uid",
                                        "plugin.alias",
                                        "plugin.category_uid",
                                        "plugin.desc"
                                    ]
                                 )
    plugin_dict = {}
    
    for r in result:
        plugin_uid = str(r[0])
        plugin_alias = str(r[1])
        category_uid = str(r[2])
        desc = str(r[3])
        
        category_alias = ""
        if util_uid.is_valid_uid(category_uid):
            category_alias = alias.get_data_alias(util_uid.CTI_UID(category_uid))
        
        plugin_dict[plugin_uid] = {
            "category_uid": category_uid,
            "category_alias": category_alias,
            "desc": desc,
            "alias": plugin_alias
        }
    
    return plugin_dict
#---------------------------------------------------------------------------

def list_txt_plugins(plugin_set):
    """ Display the list of plugins in text format

    Args:
      plugin_set: the set of plugins to display

    Returns:
      None
    """
    print "%36s %s" % ("Plugin", "Description")
    for p in plugin_set.keys():
        plugin_data = plugin_set[p]
        name = plugin_data["alias"]
        desc = plugin_data["desc"]
        print "%36s %s" % (name, desc)

    print "\nHint: for detailed info call $ cti --help <plugin UID|alias>"

#---------------------------------------------------------------------------

def list_raw_plugins(plugin_set):
    """ Display the list of plugins in raw format

    Args:
      plugin_set: the set of plugins to display

    Returns:
      None
    """
    for p in plugin_set.keys():
        plugin_data = plugin_set[p]
        name = plugin_data["alias"]
        print name

#---------------------------------------------------------------------------

def list_json_plugins(plugin_set, self, params):
    """ Display the list of plugins in JSON format

    Args:
        plugin_set: the set of plugins to display
    Returns:
      None
    """
    
    #Here we cannot pass an uid to json_encode.
    res = {}
    for key in plugin_set:
        res[str(key)] = plugin_set[key]
    
    print cti_json.cti_json_encode(res)
