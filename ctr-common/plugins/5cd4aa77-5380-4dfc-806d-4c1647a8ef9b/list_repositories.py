#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import entry, cti_json, util

import sys

def get_list_local_repositories():
    """ return a set of local repositories

    Args:
        None

    Returns:
      the list of local repositories
    """  
    gi_file = ctr.ctr_plugin_global_index_file_load()
    it = ctr.ctr_plugin_global_index_file_it_begin(gi_file)
    
    if not it:
        return []
    result = []
    while not ctr.ctr_plugin_global_index_file_it_is_end(it):
        uid = ctr.ctr_plugin_global_index_file_it_key(it)
        if not uid:
            util.cti_plugin_print_error("No uid for repository " + \
                             str(ctr.ctr_plugin_global_index_file_it_value(it)))
            sys.exit(cti.CTI_PLUGIN_ERROR_CRITICAL_IO)
            
        try:
            out = entry.load_data(uid)[1]
        except:
            out = None
        
        if out:
            status = out["init"].params["status"][cti.META_ATTRIBUTE_VALUE]
            last_use = out["init"].params["last_use"][cti.META_ATTRIBUTE_VALUE]
        else:
            status = ""
            last_use = ""
        
        result += \
            [
                {
                    "path": ctr.ctr_plugin_global_index_file_it_value(it), 
                    "uid": uid,
                    "alias": cti.cti_plugin_alias_repository_get_key(uid),
                    "status": status,
                    "last_use": last_use
                }
            ]
        it = ctr.ctr_plugin_global_index_file_it_next(it)
        
    return result
#---------------------------------------------------------------------------

def list_txt_local_repositories(repository_set):
    """ Display the list of local repository in text format

    Args:
      repository_set: the set of local repositories to display

    Returns:
      None
    """
    print "Local repositories"
    
    if len(repository_set) == 0:
        print "Nothing"
    for ctr_instance in repository_set:
        alias = cti.cti_plugin_alias_repository_get_key(ctr_instance['uid'])
        if alias is None:
            alias = str(ctr_instance['uid'])
        print alias, ": ", ctr_instance['path'],
        print "Last access", ": ", ctr_instance['last_use']
#---------------------------------------------------------------------------

def list_json_local_repositories(repository_set, self, params):
    """ Display the list of local repositories in JSON format

    Args:
        repository_set: the set of local repositories to display

    Returns:
      None
    """
    print cti_json.cti_json_encode(repository_set)
