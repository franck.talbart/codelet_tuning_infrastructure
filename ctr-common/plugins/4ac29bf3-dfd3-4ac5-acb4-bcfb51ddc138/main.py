#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import description, entry, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, tempfile, datetime, tarfile, shutil

class BinaryPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Initialize information for binary

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        
        self.work_params = description.description_write(self.command, self.work_params)
        binary_path = self.work_params[self.command].params["binary_file"][cti.META_ATTRIBUTE_VALUE]
        compile_entry = self.work_params[self.command].params["compile"][cti.META_ATTRIBUTE_VALUE]
        
        # check parameters
        if not binary_path:
            util.cti_plugin_print_error("Binary is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        if not os.path.isfile(os.path.abspath(binary_path)):
            util.cti_plugin_print_error("The binary does not exist!")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        tar_binary = None
        tar_binary = tar_and_add_file(binary_path)
        self.work_params[self.command].params["binary_file"][cti.META_ATTRIBUTE_VALUE] = tar_binary
        
        # create the alias
        bin_name = os.path.basename(binary_path)
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        alias_e = "%s_%s" % (bin_name, date)
        
        data_entry = self.default_init_command(params, alias_e = alias_e)
        if compile_entry:
            entry.update_entry_parameter(compile_entry, {"binary": {"value": data_entry.uid}})
        
        if tar_binary:
            shutil.rmtree(os.path.dirname(tar_binary))
        return 0
    
    #---------------------------------------------------------------------------
    
    @hapi_command("update")
    def update_cmd(self, params):
        """ Update a binary entry
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
            Nothing
        """
        
        update_params = self.work_params[self.command].params
        tar_binary = None
        
        if update_params["binary_file"].has_key(cti.META_ATTRIBUTE_VALUE):
            binary_path = update_params["binary_file"][cti.META_ATTRIBUTE_VALUE]
            
            if binary_path is None:
                self.work_params[self.command].params["binary_file"][cti.META_ATTRIBUTE_VALUE] = None
            elif os.path.isfile(os.path.abspath(binary_path)):
                tar_binary = tar_and_add_file(binary_path)
                self.work_params[self.command].params["binary_file"][cti.META_ATTRIBUTE_VALUE] = tar_binary
            else:
                util.cti_plugin_print_error("'%s' is not a file or does not exist!" % binary_path)
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        output = description.update_entry(self, params)
        
        if tar_binary:
            shutil.rmtree(os.path.dirname(tar_binary))
        
        return output
#---------------------------------------------------------------------------

def tar_and_add_file(binary_file):
    """ Compress and add file into the entry
        
        Args:
            binary_file: the binary file
        Returns:
            the compressed file path
    """
    if binary_file is not None:
        # convert relative paths to absolute paths
        binary_file_abspath = os.path.abspath(binary_file)
        binary_name = os.path.basename(binary_file_abspath)
        
        # compress the binary files
        tmp_dir = tempfile.mkdtemp()
        work_dir = os.getcwd()
        os.chdir(tmp_dir)
        tar_binary = os.path.join(tmp_dir, binary_name) + ".tar.gz"
        tar_file = tarfile.open(tar_binary, "w:gz")
        tar_file.add(binary_file_abspath, arcname=binary_name)
        tar_file.close()
        sys.stdout.flush()
        os.chdir(work_dir)
        
        return tar_binary
    return None
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = BinaryPlugin()
    exit(p.main(sys.argv))
