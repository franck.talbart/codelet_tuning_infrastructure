#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi.main import HapiPlugin, hapi_command
from cti_hapi import util

import plugin_viewer, data_viewer, file_viewer

import sys

class ViewPlugin(HapiPlugin):
    @hapi_command("data")
    def data_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters
        Returns:
          the list of local repositories
        """
        
        info = data_viewer.get_info_data(params["data_uid"])
        
        if params["format"] == "json":
            data_viewer.data_json_viewer(info, self, params)
        elif params["format"] == "txt":
            data_viewer.data_txt_viewer(info)
        else:
            util.cti_plugin_print_error("Unknown format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------

    @hapi_command("plugin")
    def plugin_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters
        Returns:
          the list of local repositories
        """
        info = plugin_viewer.get_info_plugin(params["plugin_uid"])
        if info == cti.CTI_ERROR_UID_NOT_FOUND:
            return cti.CTI_ERROR_UID_NOT_FOUND
        if params["format"] == "json":
            plugin_viewer.plugin_json_viewer(info, self, params)
        elif params["format"] == "txt":
            plugin_viewer.plugin_txt_viewer(info, self)
        else:
            util.cti_plugin_print_error("Unknown format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------

    @hapi_command("file")
    def file_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters
        Returns:
          the list of local repositories
        """
        file_viewer.file_viewer(self, params)
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = ViewPlugin()
    exit(p.main(sys.argv))
