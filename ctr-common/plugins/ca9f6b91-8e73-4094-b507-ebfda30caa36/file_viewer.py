#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import cti_json, util_uid, util

import os, cgi, sys, subprocess

def file_viewer(self, params):
    """ Display the content of a file from a CTR entry
    
    Args:
    Returns:
      None
    """
    info_file = ctr.ctr_plugin_info_file_load_by_uid(params["data_uid"])
    plugin_uid = util_uid.CTI_UID(ctr.ctr_plugin_info_get_value(info_file, cti.DATA_INFO_PLUGIN_UID), cti.CTR_ENTRY_PLUGIN)
    filename = params["filename"]
    filename = filename.replace("..", "")
    
    refresh = ""
    if str(plugin_uid) == str(params["daemon_plugin_uid"]):
        refresh = 2
    
    path = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, params["data_uid"])
    path = os.path.join(path, cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), filename)
    
    result = {'read_error': []}
    
    size = 0
    if path and os.path.isfile(path):
        size = os.path.getsize(path)
        
        if size > params["max_size"]:
            result['read_error'].append("The file is too big to be displayed.")
        else: 
            sys.stdout.flush()
            try:
                output = subprocess.Popen(["file", "--mime", path],
                                          shell=False,
                                          stdout=subprocess.PIPE).communicate()[0]
            except OSError:
                util.cti_plugin_print_error("Failed to check the type. {0}".format(output))
                return cti.CTI_PLUGIN_ERROR_IO
            if output.count("ASCII") == 0 and output.count("text") == 0 and \
                    output.count("empty") == 0 and output.count("fortran") == 0:
                result['read_error'].append(output)
                result['read_error'].append("Type not supported by the viewer.")
            else:
                try:
                    ofi = open(path, 'r')
                    content = ofi.read()
                    content = cgi.escape(content)
                    ofi.close()
                except IOError:
                    result['read_error'].append("File not found!")
                result['content'] = content
                
        result['filename'] = os.path.basename(path)
        result['path'] = path
        result['refresh'] = refresh
    else:
        result['read_error'].append("No file to open.")
    
    if not result['read_error']:
        del result['read_error']
    
    print cti_json.cti_json_encode(result)
#---------------------------------------------------------------------------
