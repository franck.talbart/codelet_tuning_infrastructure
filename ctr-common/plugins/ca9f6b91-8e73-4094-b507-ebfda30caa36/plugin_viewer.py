#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import entry, util, cti_json, util_uid, alias

import cgi

def escape(v):
    return cgi.escape(str(v))
#---------------------------------------------------------------------------

def get_info_plugin(id_plugin):
    """ Get information about a plugin

    Args:
        id_plugin: plugin UID or plugin alias.

    Returns:
      an array with information about this plugin
    """
    result = {}
    result["UID"] = id_plugin
    
    if not result["UID"]:
        util.cti_plugin_print_error("Plugin not found")
        return cti.CTI_ERROR_UID_NOT_FOUND
    
    # Input and output files content
    try:
        result["input_file"], result["output_file"] = entry.load_defaults(result["UID"])
    except Exception, e:
        util.cti_plugin_print_error("Plugin not found!")
        util.cti_plugin_print_error(str(e))
        return cti.CTI_ERROR_UID_NOT_FOUND
    
    info_file = ctr.ctr_plugin_info_file_load_by_uid(result["UID"])

    result["authors"] = ctr.ctr_plugin_info_get_value(info_file, 
                                                    cti.PLUGIN_INFO_AUTHOR)
    result["build_number"] = ctr.ctr_plugin_info_get_value(info_file, 
                                                    cti.PLUGIN_INFO_BUILD_NUMBER)
    result["category"] = util_uid.CTI_UID(ctr.ctr_plugin_info_get_value(info_file, 
                                                    cti.PLUGIN_INFO_CATEGORY))
    result["description"] = ctr.ctr_plugin_info_get_value(info_file, 
                                                    cti.PLUGIN_INFO_DESCRIPTION)
    result["repository_path"] = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN,
                                                                result["UID"])
    
    return result
#---------------------------------------------------------------------------

def plugin_json_viewer(info, self, params):
    """ Display the information about a plugin in JSON format
    
    Args:
        info: the array with the plugin information
    Returns:
      None
    """
    uid = params['plugin_uid']
    repository_type = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_PLUGIN, uid)
    if repository_type == cti.CTR_REP_LOCAL:
        repository_type = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN, uid)
    info['repository_type'] = repository_type
    print cti_json.cti_json_encode(info)
#---------------------------------------------------------------------------

def plugin_txt_viewer(info, self):
    """ Display the information about a plugin in text format

    Args:
        info: the array with the plugin information

    Returns:
      None
    """
    # UID and Alias
    print util_uid.uid_visualization(info["UID"], cti.CTR_ENTRY_PLUGIN)
    
    # Output file
    if info["output_file"] != {}:
        print "\n** Output default file **\n"
        print_file_txt(info["output_file"])

    # Authors
    print "Authors: ", info["authors"]
    
    # Build number
    print "Build number: ", info["build_number"]
    
    # Path
    print "Path: ", info["repository_path"]
    
    # Category
    alias_category = alias.get_data_alias(info["category"])
    line = str(info["category"])
    if alias_category is not None:
        line = alias_category + " (" + line + ")"
    print "Category: ", line
    
    # Description
    print "Description: ", info["description"]
    
    # Input file
    if info["input_file"] != {}:
        print "\n** Input default file **\n"
        print_file_txt(info["input_file"])
#---------------------------------------------------------------------------

def print_file_txt(file_txt):
    """ Display the content of an input or an output default file in JSON format

    Args:
        file_txt: The tree given by loadDefault
        uid_viewer: the view plugin uid

    Returns:
      None
    """
    for cmd in file_txt:
        print "\nCommand: ", cmd
        print "* Attributes"

        for att, att_val in file_txt[cmd].attributes.items():
            print "** " + att + " : " + str(att_val)
        print "--------------------------------------------------"
        print "%10s %30s %10s %10s %30s" % ("Parameter", 
                                            "Description", 
                                            "Type", 
                                            "List", 
                                            "Default value")
        for param in file_txt[cmd].params:
            if file_txt[cmd].params[param].has_key(cti.META_ATTRIBUTE_NAME):
                name = file_txt[cmd].params[param][cti.META_ATTRIBUTE_NAME]
            else:
                name = ""
            
            if file_txt[cmd].params[param].has_key(cti.META_ATTRIBUTE_DESC):
                desc = file_txt[cmd].params[param][cti.META_ATTRIBUTE_DESC]
            else:
                desc = ""
            
            if file_txt[cmd].params[param].has_key(cti.META_ATTRIBUTE_TYPE):
                type_p = file_txt[cmd].params[param][cti.META_ATTRIBUTE_TYPE]
            else:
                type_p = ""
            
            if file_txt[cmd].params[param].has_key(cti.META_ATTRIBUTE_LIST):
                list_p = file_txt[cmd].params[param][cti.META_ATTRIBUTE_LIST]
            else:
                list_p = ""
            
            value_p = ""
            if file_txt[cmd].params[param].has_key(cti.META_ATTRIBUTE_VALUE):
                for value in util.value_as_list(file_txt[cmd].params[param]):
                    value_p = value_p + str(value) + " "
            
            print "%10s %30s %10s %10s %30s" % (name.title(), 
                                                desc, 
                                                type_p, 
                                                list_p, 
                                                value_p)
