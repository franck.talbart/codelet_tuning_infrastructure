#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import entry, util, util_uid, cti_json, alias

import cgi

def escape(v):
    return cgi.escape(str(v))
#---------------------------------------------------------------------------

def get_info_data(id_data):
    """ Get information about a data
    
    Args:
        id_data: data UID or data alias.
    
    Returns:
        an array with information about this data
    """
    
    # UID
    result = {}
    result["UID"] = id_data
    
    # repository type
    result["repository"] = ""
    result["repository_type"] = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, result["UID"])
    if result["repository_type"] == cti.CTR_REP_LOCAL:
        result["repository_type"] = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(result["UID"], cti.CTR_DATA_DIR)
        result["repository"] = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(result["repository_type"]))
    elif result["repository_type"] == cti.CTR_REP_COMMON:
        result["repository"] = cti.COMMON_REPOSITORY
    elif result["repository_type"] == cti.CTR_REP_TEMP:
        result["repository"] = cti.TEMP_REPOSITORY
        
    # Input and output files content
    try:
        result["input_file"], result["output_file"] = entry.load_data(result["UID"])
    except Exception:
        util.cti_plugin_print_error("Could not load data entry %s"%(result["UID"]))
        exit(cti.CTI_PLUGIN_ERROR_IO)
        
    # Removing password
    for r in ['input_file','output_file']:
        for cmd in result[r]:
             for _, p in result[r][cmd].params.items():
                 if p.has_key(cti.META_ATTRIBUTE_PASSWORD):
                     if p[cti.META_ATTRIBUTE_PASSWORD]:
                         p[cti.META_ATTRIBUTE_VALUE] = ""
                         
    # Main information
    info_file = ctr.ctr_plugin_info_file_load_by_uid(result["UID"])
    result["plugin_uid"] = ctr.ctr_plugin_info_get_value(info_file, cti.DATA_INFO_PLUGIN_UID)
    result["exit_code"] = ctr.ctr_plugin_info_get_value(info_file, cti.DATA_INFO_PLUGIN_EXIT_CODE)
    result["date_start"] = ctr.ctr_plugin_info_get_value(info_file, cti.DATA_INFO_DATE_TIME_START)
    result["date_end"] = ctr.ctr_plugin_info_get_value(info_file, cti.DATA_INFO_DATE_TIME_END)
    result["user_uid"] = ctr.ctr_plugin_info_get_value(info_file, cti.DATA_INFO_USER_UID)
    (result['input_default'], result['output_default']) = entry.load_defaults(util_uid.CTI_UID(result["plugin_uid"], cti.CTR_ENTRY_PLUGIN))
    
    result['repository_path'] = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, result['UID'])
    return result
#---------------------------------------------------------------------------

def data_json_viewer(info, self, params):
    """ Display the information about a data in JSON format
    
    Args:
        info: the array with the data information
    Returns:
      None
    """
    print cti_json.cti_json_encode(info)
#---------------------------------------------------------------------------

def data_txt_viewer(info):
    """ Display the information about a data in text format
    
    Args:
        info: the array with the data information
    
    Returns:
      None
    """
    
    # UID and Alias
    print util_uid.uid_visualization(info["UID"], cti.CTR_ENTRY_DATA) + "\n"
    
    # Information
    print "\n** Information **\n"
    print "Plugin      : {0}".format(util_uid.uid_visualization(cti.CTI_UID(info["plugin_uid"]), cti.CTR_ENTRY_PLUGIN))
    print "Exit code   : {0}".format(info["exit_code"])
    print "Start date  : {0}".format(info["date_start"])
    print "End date    : {0}".format(info["date_end"])
    print "User        : {0}".format(util_uid.uid_visualization(info["user_uid"], cti.CTR_ENTRY_DATA))
    print "Path        : {0}".format(info["repository_path"])
    if util_uid.is_valid_uid(info["repository"]):
        print "Repository  : {0}\n".format(cti.cti_plugin_alias_repository_get_key(util_uid.CTI_UID(info["repository"], cti.CTR_ENTRY_REPOSITORY)))
    else:
        print "Repository  : {0}\n".format(info["repository"])
    
    # Output file
    if info["output_file"] is not None:
        print "** Output File **\n"
        print_file_txt(info["output_file"])
    
    print "\n"
    # Input file
    if info["input_file"] is not None:
        print "** Input File **\n"
        print_file_txt(info["input_file"])
#---------------------------------------------------------------------------

def print_file_txt(file_j):
    """ Display the content of an input or an output file in JSON format
    
    Args:
        file_j: The tree given by load_data
    
    Returns:
      None
    """
    
    for cmd in file_j:
        print "Command: " + cmd
        
        for attribute_cmd in file_j[cmd].attributes:
            if attribute_cmd != cti.META_ATTRIBUTE_NAME:
                print attribute_cmd, ": ", file_j[cmd].attributes[attribute_cmd]
        
        print "\n%15s %20s %15s %5s %10s" %("Parameter",
                                            "Description",
                                            "Type",
                                            "List",
                                            "Value")
        
        for param in file_j[cmd].params:
            if file_j[cmd].params[param].has_key(cti.META_ATTRIBUTE_NAME):
                name = file_j[cmd].params[param][cti.META_ATTRIBUTE_NAME]
            else:
                name = ""
            
            if file_j[cmd].params[param].has_key(cti.META_ATTRIBUTE_DESC):
                description = file_j[cmd].params[param][cti.META_ATTRIBUTE_DESC]
            else:
                description = ""
            
            if file_j[cmd].params[param].has_key(cti.META_ATTRIBUTE_TYPE):
                type_p = file_j[cmd].params[param][cti.META_ATTRIBUTE_TYPE]
            else:
                type_p = ""
            
            if file_j[cmd].params[param].has_key(cti.META_ATTRIBUTE_LIST):
                list_p = file_j[cmd].params[param][cti.META_ATTRIBUTE_LIST]
            else:
                list_p = ""
            
            value_p = ""
            
            if type_p == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
                if file_j[cmd].params[param][cti.META_ATTRIBUTE_VALUE]:
                    value_p = '\n'+util.cti_pretty_table(
                        [file_j[cmd].params[param][cti.META_ATTRIBUTE_VALUE][i] for i in file_j[cmd].params[param][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES]],
                        file_j[cmd].params[param][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES],
                        file_j[cmd].params[param][cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES]
                    )
                else:
                    print "No data."
            else:
                if file_j[cmd].params[param].has_key(cti.META_ATTRIBUTE_VALUE):
                    for value in util.value_as_list(file_j[cmd].params[param]):
                        str_value = str(value)
                        if util_uid.is_valid_uid(str_value):
                            alias_value = alias.get_data_alias(cti.CTI_UID(str_value))
                            if alias_value is not None:
                                str_value = alias_value
                            else:
                                alias_value = alias.get_plugin_alias(cti.CTI_UID(str_value))
                                if alias_value is not None:
                                    str_value = alias_value
                                    
                        value_p = value_p + str_value + " "
            
            print "%15s %20s %15s %5s %10s" %(name.title(),
                                                description,
                                                type_p,
                                                list_p,
                                                value_p)
