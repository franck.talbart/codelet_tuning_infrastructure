#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import description, entry, alias, plugin, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys, os, subprocess, datetime, tempfile

# constants
NB_LOOP_MAX=100

class MaqaoCQAPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ prepare parameters
        Args:
            self: class of the plugin
            params: working parameters
        """
        
        self.work_params = description.description_write(self.command, self.work_params)
        loop_group = self.work_params[self.command].params["loop_group"][cti.META_ATTRIBUTE_VALUE]
        auto_run = self.work_params[self.command].params["auto_run"][cti.META_ATTRIBUTE_VALUE]
        format_o = self.work_params[self.command].params["format"][cti.META_ATTRIBUTE_VALUE]
        
        # check parameters
        if not loop_group:
            util.cti_plugin_print_error("The loop_group entry parameter is mandatory.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS

        # create the alias
        loop_group_alias = alias.get_data_alias(loop_group)
        if not loop_group_alias:
            loop_group_alias = ""
        now = datetime.datetime.now()
        date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
        alias_e = "MAQAO_cqa_%s_%s" % (loop_group_alias, date)
        
        (data_entry, output) = self.default_init_command(params, alias_e = alias_e, no_output = True)
        res = plugin.plugin_auto_run(self, data_entry, format_o, auto_run, output)
        return res
#---------------------------------------------------------------------------

    @hapi_command("run")
    def run_cmd(self, params):
        """ Run MAQAO CQA analysis
        
        Args:
            self: class of the plugin
            params: working parameters
            
        Returns:
            Nothing
        """
        maqao_dir = os.path.join(cti.cti_plugin_config_get_value(cti.THIRD_PARTY_DIR), "maqao")
        if not os.path.isdir(maqao_dir):
            util.cti_plugin_print_error("MAQAO is not installed\nYou can find the tool here: http://www.maqao.org/")
            util.cti_plugin_print_error("Install the tool in the third-party directory (third-party/maqao/).")
            return(cti.CTI_PLUGIN_ERROR_TOOL_NOT_FOUND)
        
        # This command gets a MAQAO CQA entry (created using maqao_cqa init command) and runs
        # MAQAO CQA on it.
        
        # Gets parameters
        entry_uid = params["entry"]
        balancing_tolerance = params["balancing_tolerance"]
        method = params["method"]
        if method is None or not method:
            method = "default"
        main_loops = params["main_loops"]
        maqao_cqa_results_plugin_uid = params["maqao_cqa_results_plugin_uid"]
        maqao_cqa_group_plugin_uid = params["maqao_cqa_group_plugin_uid"]
        import_csv_plugin_uid = params["import_csv_plugin_uid"]
        
        # repository type
        repository = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, entry_uid)
        # if the repository is local, we need its UID
        if repository == cti.CTR_REP_LOCAL:
            repository_path = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(entry_uid, cti.CTR_DATA_DIR)
            repository = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(repository_path))
        
        # get the loop_group
        (input_param, output_param) = entry.load_data(entry_uid)
        loop_group_uid = output_param["init"].params["loop_group"][cti.META_ATTRIBUTE_VALUE]
        (_, output_loop_group) = entry.load_data(loop_group_uid)
        
        # get maqao_perf
        maqao_perf_uid = output_loop_group["init"].params["maqao_perf"][cti.META_ATTRIBUTE_VALUE]
        if not maqao_perf_uid:
            util.cti_plugin_print_warning("The loop_group has no maqao_perf entry. Please run Maqao Perf on the corresponding binary.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        (_, output_maqao_perf) = entry.load_data(maqao_perf_uid)
        
        # get the binary
        binary_uid = output_maqao_perf["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]
        (_, output_binary) = entry.load_data(binary_uid)
        
        binary = output_binary["init"].params["binary_file"][cti.META_ATTRIBUTE_VALUE]
        if binary is None:
            util.cti_plugin_print_error("The binary entry does not have a binary file.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        binary_name = os.path.join(ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, binary_uid), 
                                    cti.cti_plugin_config_get_value(cti.CTR_ENTRY_FILES_DIR), 
                                    binary)
        language = ""
        loop_file_name = None
        loop_uid_file_name = None
        
        loop_uids = output_loop_group["init"].params["loops"][cti.META_ATTRIBUTE_VALUE]
        
        loop_uid_file_name = tempfile.mkstemp()[1]
        loop_uid_file = open(loop_uid_file_name, "w")
        list_inner_loops = []

        for loop_uid in loop_uids:
            (_, output_loop) = entry.load_data(loop_uid)
            loop_type = output_loop["init"].params["loop_type"][cti.META_ATTRIBUTE_VALUE]
            if loop_type == "innermost" or loop_type == "single":
                list_inner_loops.append(str(output_loop["init"].params["loop_id"][cti.META_ATTRIBUTE_VALUE]))
            
            # write uid in file
            loop_uid_file.write(str(loop_uid) + "\n")

        loop_uid_file.close()
        if not list_inner_loops:
            util.cti_plugin_print_error("No innermost loops found.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        # cut into several list
        loop_file_name = tempfile.mkstemp()[1]
        loop_file = open(loop_file_name, "w")
        
        # write innermost ids in file
        while len(list_inner_loops) != 0:
            for loop in list_inner_loops[:NB_LOOP_MAX]:
                loop_file.write(loop + ",")
            loop_file.write("\n")
            list_inner_loops = list_inner_loops[NB_LOOP_MAX:]
        loop_file.close()
            
        # get the micro architecture
        micro_archi = ""

        platform = None
        compile_uid = output_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]
        if compile_uid:
            (_, output_compile) = entry.load_data(compile_uid)
            platform = output_compile["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
        if platform:
            (_, out_platform) = entry.load_data(platform)
            cpu = out_platform["init"].params["processor"][cti.META_ATTRIBUTE_VALUE]
            if cpu:
                (_, out_cpu) = entry.load_data(cpu[0])
                micro_archi = out_cpu["init"].params["micro_architecture"][cti.META_ATTRIBUTE_VALUE]
        
        if not micro_archi:
            util.cti_plugin_print_warning("no micro architecture given.")
            util.cti_plugin_print_warning("MAQAO CQA will use the micro architecture of the current machine.")
        
        # Calls MAQAO CQA launcher
        
        # command for the daemon
        command = \
        [
            "/bin/bash",
            os.path.join(self.plugin_directory, "maqao_cqa.sh"),
            "-b" + binary_name,
            "-B" + str(balancing_tolerance),
            "-d" + self.plugin_directory,
            "-E" + str(entry_uid),
            "-g" + str(maqao_cqa_results_plugin_uid),
            "-G" + str(maqao_cqa_group_plugin_uid),
            "-i" + str(import_csv_plugin_uid),
            "-m" + str(main_loops),
            "-R" + repository,
            "-s" + str(self.plugin_uid),
            "-t" + method,
            "-T" + maqao_dir,
            "-U" + self.username,
            "-X" + self.password,
            "-w" + cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)
        ]
        
        if micro_archi:
            command.append("-p" + micro_archi)
        if language:
            command.append("-l" + language)
        if loop_file_name:
            command.append("-L" + str(loop_file_name))
        if loop_uid_file_name:
            command.append("-u" + str(loop_uid_file_name))
        
        output = ""
        
        print "Running the process..."
        sys.stdout.flush()
        try:
            child = subprocess.Popen(command, shell=False)
            
            output = child.communicate()
            rc = child.returncode
            
            if rc != 0:
                raise Exception("The command " + 
                        str(command) + 
                        " encountered the error #" + 
                        str(rc) +
                        " Output: " + 
                        str(output))
        except:
            util.cti_plugin_print_error("Failed to execute command: {0}".format(command))
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
        print output

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = MaqaoCQAPlugin()
    exit(p.main(sys.argv))
