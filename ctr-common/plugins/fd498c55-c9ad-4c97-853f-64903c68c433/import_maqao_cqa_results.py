#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

# This script imports results from MAQAO CQA into CTI.

import cti, ctr

from cti_hapi import entry, util, util_uid, plugin, alias

import getopt, sys, os, time, tempfile, glob, datetime, csv

# constants
MATRIX_ID = "common_ID"
MATRIX_NB_CYCLES_P = "x86_64_Nb_cycles_P"
MATRIX_IS_MULT_PATH = "common_is_multiple_path"
MATRIX_IS_MAIN_UNROLLED = "common_Is_main/unrolled"


#---------------------------------------------------------------------------
def compute_balancing(parith, pstore, pload, balancing_tolerance):
    """ Compute balancing
    Args:
        self: class of the plugin
        params: working parameters
    """
    parith_tolerance = balancing_tolerance * parith
    mplps = max(pload, pstore)
    if parith - parith_tolerance <=  mplps and parith + parith_tolerance >= mplps: 
        return ("Arithmetic memory balanced")
    elif parith > mplps:
        return("Arithmetic")
    else:
        pload_tolerance = balancing_tolerance * pload
        if pload - pload_tolerance <=  pstore and pload + pload_tolerance >= pstore:
            return("LS balanced")
        elif pload > pstore:
            return("Load")
        elif pload < pstore:
            return("Store")
        else:
            return("Unknown")
#---------------------------------------------------------------------------

#---------------------------------------------------------------------------
def default_method(matrix_results, matrix_columns, _, main_loops):
    """ Process loop data
    Args:
        matrix_results : matrix results from maqao_cqa entry
        matrix_columns : matrix columns from maqao_cqa entry
        main_loops : the main_loops parameter
    Returns:
        CSV files directory
    """
    
    if matrix_results and matrix_results[matrix_columns[0]]:
        # get the loop IDs
        list_loop_ID = []
        if main_loops:
            loop_index = 0
            for loopID in matrix_results[MATRIX_ID]:
                loopID = str(loopID) # on CTI database, the loop id are string
                # only keep "main" loops
                if matrix_results[MATRIX_IS_MAIN_UNROLLED][loop_index]:
                    list_loop_ID.append(loopID)
                loop_index += 1
        
        if not main_loops or list_loop_ID:
            work_dir = tempfile.mkdtemp(dir=".")
            loops_matrix = {}
            loop_index = 0
            # process innermost loops
            for loopID in matrix_results[MATRIX_ID]:
                loopID = str(loopID) # on CTI database, the loop id are string
                if not main_loops or loopID in list_loop_ID:
                    # only get the first result line for one loop ID
                    if not loops_matrix.has_key(loopID):
                        loops_matrix[loopID] = {}
                        for column_name in matrix_columns:
                            value = matrix_results[column_name][loop_index]
                            if value is None:
                                value = "NA"
                            loops_matrix[loopID][column_name] = value
                            loops_matrix[loopID][MATRIX_IS_MULT_PATH] = False
                    else:
                        loops_matrix[loopID][MATRIX_IS_MULT_PATH] = True
                loop_index += 1
            
            # write csv file
            for loopID in loops_matrix:
                loop_csv_file = open(os.path.join(work_dir, "loop%s.csv" % loopID), "w")
                csv_writer = csv.DictWriter(loop_csv_file, fieldnames=loops_matrix[loopID], delimiter=';')
                # write column name
                csv_writer.writerow(dict((f, f) for f in loops_matrix[loopID]))
                
                # write values
                csv_writer.writerow(loops_matrix[loopID])
                
                loop_csv_file.close()
            
            return work_dir
        else:
            return None
    else:
        return None
#---------------------------------------------------------------------------

#---------------------------------------------------------------------------
def max_method(matrix_results, matrix_columns, list_outer_loop, main_loops):
    """ Process loop data
    Args:
        matrix_results : matrix results from maqao_cqa entry
        matrix_columns : matrix columns from maqao_cqa entry
        list_outer_loop : dict of outer loop
        main_loops : the main_loops parameter
    Returns:
        CSV files directory
    """
    
    #---------------------------------------------------------------------------
    def process_outer_loop(loop_uid_current, loops_matrix, list_outer_loop):
        """ Process loop data
        Args:
            loop_uid_current : loop UID to process
            loops_matrix : dict of loops
            list_outer_loop : dict of outer loop
        Returns:
            loops matrix
        """
        (_, output_loop) = entry.load_data(loop_uid_current)
        loop_id_current = output_loop["init"].params["loop_id"][cti.META_ATTRIBUTE_VALUE]
        loop_uid_children = output_loop["init"].params["loop_children"][cti.META_ATTRIBUTE_VALUE]
        
        if loop_uid_children:
            loops_matrix[loop_id_current] = {}
            # get values from each child loop
            for loop_uid_child in loop_uid_children:
                (_, output_loop_child) = entry.load_data(loop_uid_child)
                loop_id_child = output_loop_child["init"].params["loop_id"][cti.META_ATTRIBUTE_VALUE]
                # if the child is not in loops_matrix, check if he is in the outer loops list
                # and get his values
                if not loops_matrix.has_key(loop_id_child) and loop_uid_child in list_outer_loop:
                        loops_matrix = process_outer_loop(loop_uid_child, loops_matrix, list_outer_loop)
                # check the child has values
                if loops_matrix.has_key(loop_id_child):
                    # get values from child
                    for column_name in loops_matrix[loop_id_child]:
                        column_value = loops_matrix[loop_id_child][column_name]
                        if loops_matrix[loop_id_current].has_key(column_name):
                            # get the max of numeric values
                            column_value = max(loops_matrix[loop_id_current][column_name], column_value)
                        loops_matrix[loop_id_current][column_name] = column_value
            
            if len(loops_matrix[loop_id_current]) == 0:
                del(loops_matrix[loop_id_current])
        return loops_matrix
    #---------------------------------------------------------------------------
    
    if matrix_results and matrix_results[matrix_columns[0]]:
        # get the loop IDs
        list_loop_ID = []
        if main_loops:
            loop_index = 0
            for loopID in matrix_results[MATRIX_ID]:
                loopID = str(loopID) # on CTI database, the loop id are string
                # only keep "main" loops
                if matrix_results[MATRIX_IS_MAIN_UNROLLED][loop_index]:
                    list_loop_ID.append(loopID)
                loop_index += 1
        
        if not main_loops or list_loop_ID:
            work_dir = tempfile.mkdtemp(dir=".")
            loops_matrix = {}
            loop_index = 0
            # process innermost loops
            for loopID in matrix_results[MATRIX_ID]:
                loopID = str(loopID) # on CTI database, the loop id are string
                if not main_loops or loopID in list_loop_ID:
                    # only get the first result line for one loop ID
                    if not loops_matrix.has_key(loopID):
                        loops_matrix[loopID] = {}
                        for column_name in matrix_columns:
                            value = matrix_results[column_name][loop_index]
                            if value is None:
                                value = "NA"
                            loops_matrix[loopID][column_name] = value
                            loops_matrix[loopID][MATRIX_IS_MULT_PATH] = False
                    else:
                        loops_matrix[loopID][MATRIX_IS_MULT_PATH] = True
                loop_index += 1
            
            # process outermost loops
            for loop_uid_outer in list_outer_loop:
                loops_matrix = process_outer_loop(loop_uid_outer, loops_matrix, list_outer_loop)
            
            # write csv file
            for loopID in loops_matrix:
                loop_csv_file = open(os.path.join(work_dir, "loop%s.csv" % loopID), "w")
                csv_writer = csv.DictWriter(loop_csv_file, fieldnames=loops_matrix[loopID], delimiter=';')
                # write column name
                csv_writer.writerow(dict((f, f) for f in loops_matrix[loopID]))
                
                # write values
                csv_writer.writerow(loops_matrix[loopID])
                
                loop_csv_file.close()
            
            return work_dir
        else:
            return None
    else:
        return None
#---------------------------------------------------------------------------

# constants
method_dict = {"default": default_method, "max": max_method}


balancing_tolerance = None
unprocessed_maqao_csv = ""
maqao_cqa_results_plugin_uid = ""
maqao_cqa_group_plugin_uid = ""
import_csv_plugin_uid = ""
main_loops = False
maqao_cqa_output = ""
maqao_cqa_uid = None
success = ""
method = ""
loop_uid_file_name = ""
username = ""
maqao_version = ""
password = ""

# get parameters
try:
    opts, args = getopt.getopt(sys.argv[1:], "B:c:e:g:G:i:l:m:M:s:t:u:U:v:X:")
except:
    print sys.exc_info()[1]
    util.cti_plugin_print_error(sys.argv[0] + " -M <maqao_cqa_uid> -m <maqao_cqa_output> [-v <maqao_version>] [-s <success>] [-B <balancing_tolerance> -c <unprocessed_maqao_csv> -g <maqao_cqa_results_plugin_uid> -G <maqao_cqa_group_plugin_uid> -i <import_csv_plugin_uid> -l <main_loops> -t <method> -u <loop_uid_file_name> -U <username> -X <password>]")
    sys.exit(2)
for opt, arg in opts:
    if opt == "-B":
        balancing_tolerance = float(arg)
    elif opt == "-c":
        unprocessed_maqao_csv = arg
    elif opt == "-g":
        maqao_cqa_results_plugin_uid = arg
    elif opt == "-G":
        maqao_cqa_group_plugin_uid = arg
    elif opt == "-i":
        import_csv_plugin_uid = arg
    elif opt == "-l":
        main_loops = arg
    elif opt == "-m":
        maqao_cqa_output = arg
    elif opt == "-M":
        maqao_cqa_uid = util_uid.CTI_UID(arg)
    elif opt == "-s":
        success = int(arg)
    elif opt == "-t":
        method = arg
    elif opt == "-u":
        loop_uid_file_name = arg
    elif opt == "-U":
        username = arg
    elif opt == "-v":
        maqao_version = arg
    elif opt == "-X":
        password = arg
    else:
        assert False, "unhandled option"

# check parameters
if balancing_tolerance < 0 or balancing_tolerance > 1:
    util.cti_plugin_print_error("The balancing_tolerance parameter must contain a value between 0 and 1.")
    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
if not unprocessed_maqao_csv:
    util.cti_plugin_print_error("You must provide the unprocess maqao csv (-c option)!")
    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
if not maqao_cqa_uid:
    util.cti_plugin_print_error("You must provide the maqao cqa entry (-M option)!")
    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
if not maqao_cqa_output:
    util.cti_plugin_print_error("You must provide the maqao cqa output (-m option)!")
    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
if not str(success):
    success = 0
if not maqao_version:
    maqao_version = "NA"
if main_loops.lower() == "true":
    main_loops = True
else:
    main_loops = False

# import the maqao cqa output file
try:
    maqao_cqa_output_file = entry.put_file_in_entry(
                        maqao_cqa_uid, 
                        maqao_cqa_output, 
                        False
                      )
except IOError:
    util.cti_plugin_print_error("No such file: '" + maqao_cqa_output + "' ")

entry.update_entry_parameter(maqao_cqa_uid, {"maqao_version": {"value": maqao_version},
                                             "maqao_cqa_output": {"value": maqao_cqa_output_file},
                                             "balancing_tolerance": {"value": balancing_tolerance}})

if success == 1:
    (_, output_param) = entry.load_data(maqao_cqa_uid)
    # getting the micro architecture
    loop_group_uid = output_param["init"].params["loop_group"][cti.META_ATTRIBUTE_VALUE]
    (_, output_loop_group) = entry.load_data(loop_group_uid)
        
    # get maqao_perf
    maqao_perf_uid = output_loop_group["init"].params["maqao_perf"][cti.META_ATTRIBUTE_VALUE]
    if not maqao_perf_uid:
        util.cti_plugin_print_warning("The loop_group has no maqao_perf entry. Please run Maqao Perf on the corresponding binary.")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    (_, output_maqao_perf) = entry.load_data(maqao_perf_uid)
        
    # get the binary
    binary_entry = output_maqao_perf["init"].params["binary"][cti.META_ATTRIBUTE_VALUE]

    (_, output_binary) = entry.load_data(binary_entry)
    compile_entry = output_binary["init"].params["compile"][cti.META_ATTRIBUTE_VALUE]
    if not str(compile_entry):
        util.cti_plugin_print_error("Compile entry is needed to import MAQAO CQA results.")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    (_, output_compile) = entry.load_data(compile_entry)
    
    micro_archi = ""
    platform = output_compile["init"].params["platform"][cti.META_ATTRIBUTE_VALUE]
    if platform:
        (_, out_platform) = entry.load_data(platform)
        cpu = out_platform["init"].params["processor"][cti.META_ATTRIBUTE_VALUE]
        if cpu:
            (_, out_cpu) = entry.load_data(cpu[0])
            micro_archi = out_cpu["init"].params["micro_architecture"][cti.META_ATTRIBUTE_VALUE]
    
    if not micro_archi:
        util.cti_plugin_print_warning("no micro architecture given.")
        util.cti_plugin_print_error("MAQAO CQA used the micro architecture of the current machine.")
    
    matrix_results = output_param["init"].params["matrix_results"][cti.META_ATTRIBUTE_VALUE]
    matrix_columns = output_param["init"].params["matrix_results"][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES]
    
    if matrix_results is None or matrix_columns is None:
        util.cti_plugin_print_warning("results matrix is empty!")
    else:
        port_balancing_values = []
        # getting number of cycles
        if micro_archi in ["NEHALEM", "SANDY_BRIDGE"]:
            Nb_cycles_P0 = matrix_results[MATRIX_NB_CYCLES_P + "0"]
            Nb_cycles_P1 = matrix_results[MATRIX_NB_CYCLES_P + "1"]
            Nb_cycles_P2 = matrix_results[MATRIX_NB_CYCLES_P + "2"]
            Nb_cycles_P3 = matrix_results[MATRIX_NB_CYCLES_P + "3"]
            Nb_cycles_P4 = matrix_results[MATRIX_NB_CYCLES_P + "4"]
            Nb_cycles_P5 = matrix_results[MATRIX_NB_CYCLES_P + "5"]
            
            # calculate balancing
            try:
                for cnt in range(0, len(Nb_cycles_P0)):
                    value = "Unknown"
                    
                    p0 = float(Nb_cycles_P0[cnt])
                    p1 = float(Nb_cycles_P1[cnt])
                    p2 = float(Nb_cycles_P2[cnt])
                    p3 = float(Nb_cycles_P3[cnt])
                    p4 = float(Nb_cycles_P4[cnt])
                    p5 = float(Nb_cycles_P5[cnt])
                    
                    if micro_archi == "NEHALEM":
                        parith = max(p0, p1, p5)
                        pstore = max(p3, p4)
                        pload = p2
                        value = compute_balancing(parith, pstore, pload, balancing_tolerance)
                    elif micro_archi == "SANDY_BRIDGE":
                        parith = max(p0, p1, p5)
                        pstore = p4
                        pload = max(p2, p3)
                        value = compute_balancing(parith, pstore, pload, balancing_tolerance)
                    port_balancing_values.append(value)
            except:
                print "Can't compute port balancing."
                for cnt in range(0, len(matrix_results[matrix_columns[0]])):
                    port_balancing_values.append("Unknown")
        
        else:
            print "Will not compute port balancing: this micro architecture is not supported."
            for cnt in range(0, len(matrix_results[matrix_columns[0]])):
                port_balancing_values.append("Unknown")
        
        # updating balancing
        matrix_results["common_port_balancing"] = port_balancing_values
    
    # import the unprocess maqao csv
    try:
        unprocessed_maqao_csv_file = entry.put_file_in_entry(
                                        maqao_cqa_uid, 
                                        unprocessed_maqao_csv, 
                                        False
                                     )
    except IOError:
        util.cti_plugin_print_error("No such file: '" + unprocessed_maqao_csv + "' ")
    
    entry.update_entry_parameter(maqao_cqa_uid, {"matrix_results": {"value": matrix_results},
                                                 "unprocessed_maqao_csv": {"value": unprocessed_maqao_csv_file}})
    print "[%s] Successful importation" % time.strftime('%Y/%m/%d %H:%M:%S',time.localtime())

    util.rewrite_output_line("[%s] Processing loops" % time.strftime('%Y/%m/%d %H:%M:%S',time.localtime()))
    
    if method not in method_dict.keys():
        util.cti_plugin_print_error("Unknown method.")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    # repository type
    repository = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, maqao_cqa_uid)
    # if the repository is local, we need its UID
    if repository == cti.CTR_REP_LOCAL:
        repository_path = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(maqao_cqa_uid, cti.CTR_DATA_DIR)
        repository = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(repository_path))
    
    # creation of maqao_cqa_group
    json = \
    {
        "init":
        {
            "attributes" : {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
            "params": 
            [
                {cti.META_ATTRIBUTE_NAME : "maqao_cqa_results",cti.META_ATTRIBUTE_VALUE : []},
                {cti.META_ATTRIBUTE_NAME : "maqao_cqa",cti.META_ATTRIBUTE_VALUE : maqao_cqa_uid}
            ]
        }
    }
    
    output = ""
    try:
        output = plugin.execute_plugin_by_file(maqao_cqa_group_plugin_uid, json, username, password)
    except:
        print sys.exc_info()[1]
        exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
    
    maqao_cqa_group_uid = plugin.get_output_data_uid(output)
    
    # alias for maqao_cqa_group
    now = datetime.datetime.now()
    date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
    alias_maqao_cqa_group = "maqao_cqa_group_{0}".format(date)
    if alias.set_data_alias(maqao_cqa_group_uid, alias_maqao_cqa_group) == 0:
        util.cti_plugin_print_warning("Cannot set the alias %s (already used?)" % (alias_maqao_cqa_group))
    
    print "\n[%s] Preparing the link table with the loops..." % time.strftime('%Y/%m/%d %H:%M:%S',time.localtime())
    loop_uid_file = open(loop_uid_file_name, "r")
    loop_uids = loop_uid_file.readlines()
    loop_uid_file.close()
    
    tab_loop = {}
    list_outer_loop = []
    for loop_uid in loop_uids:
        loop_uid = util_uid.CTI_UID(loop_uid.strip("\n"))
        (_, output_loop) = entry.load_data(loop_uid)
        loop_id = output_loop["init"].params["loop_id"][cti.META_ATTRIBUTE_VALUE]
        loop_type = output_loop["init"].params["loop_type"][cti.META_ATTRIBUTE_VALUE]
        tab_loop[loop_id] = {"uid": loop_uid, "type": loop_type}
        
        if loop_type == "outermost" or loop_type == "inbetween":
            list_outer_loop.append(loop_uid)
    
    # process loops
    print "[%s] Processing loops with method %s.." % (time.strftime('%Y/%m/%d %H:%M:%S',time.localtime()), method)
    csv_dir = method_dict[method](matrix_results, matrix_columns, list_outer_loop, main_loops)
    print "[%s] End processing loops with method %s." % (time.strftime('%Y/%m/%d %H:%M:%S',time.localtime()), method)
    
    if csv_dir is not None:
        # creation of maqao_cqa_results entries
        list_csv = glob.glob(os.path.join(csv_dir, "*.csv"))
        list_csv = map(os.path.basename, list_csv)
        counter = 0.
        for csv_filename in list_csv:
            counter += 1
            percent = int(counter / len(list_csv) * 100)
            time_process = time.strftime('%Y/%m/%d %H:%M:%S',time.localtime())
            util.rewrite_output_line("[" + str(time_process) + "] [" + str(percent) +" %] Processing csv loop " + csv_filename)
            # get the loop id
            loop_id = csv_filename.strip(".csv").replace("loop", "")
            loop_entry_uid = tab_loop[loop_id]["uid"]
            
            json = \
            {
                "init":
                {
                    "attributes" : {cti.META_ATTRIBUTE_NAME : "init", cti.META_ATTRIBUTE_REP : repository},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "maqao_cqa_group",cti.META_ATTRIBUTE_VALUE : maqao_cqa_group_uid},
                        {cti.META_ATTRIBUTE_NAME : "loop",cti.META_ATTRIBUTE_VALUE : loop_entry_uid}
                    ]
                }
            }
            
            output = ""
            try:
                output = plugin.execute_plugin_by_file(maqao_cqa_results_plugin_uid, json, username, password)
            except:
                print sys.exc_info()[1]
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            
            maqao_cqa_results_uid = plugin.get_output_data_uid(output)
            
            if maqao_cqa_results_uid:
                # alias for maqao_cqa_results
                now = datetime.datetime.now()
                date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
                alias_maqao_cqa_results = csv_filename.strip(".csv") + "_maqao_cqa_results_{0}".format(date)
                if alias.set_data_alias(maqao_cqa_results_uid, alias_maqao_cqa_results) == 0:
                    util.cti_plugin_print_warning("Cannot set the alias %s (already used?)" % (alias_maqao_cqa_results))
                
                csv_file = os.path.join(csv_dir, csv_filename)
                if os.path.exists(csv_file):
                    # import Maqao CQA results into entry
                    json = \
                    {
                        "init":
                        {
                            "attributes" : {cti.META_ATTRIBUTE_NAME : "init"},
                            "params": 
                            [
                                {cti.META_ATTRIBUTE_NAME : "schema_uid",cti.META_ATTRIBUTE_VALUE : maqao_cqa_results_plugin_uid},
                                {cti.META_ATTRIBUTE_NAME : "file",cti.META_ATTRIBUTE_VALUE : csv_file},
                                {cti.META_ATTRIBUTE_NAME : "command",cti.META_ATTRIBUTE_VALUE : "update"},
                                {cti.META_ATTRIBUTE_NAME : "separator",cti.META_ATTRIBUTE_VALUE : ";"},
                                {cti.META_ATTRIBUTE_NAME : "output_parameter",cti.META_ATTRIBUTE_VALUE : "matrix_loop_results"},
                                {cti.META_ATTRIBUTE_NAME : "additional_parameters",cti.META_ATTRIBUTE_VALUE : 
                                                                [
                                                                    {
                                                                        cti.META_ATTRIBUTE_NAME: "entry",
                                                                        cti.META_ATTRIBUTE_VALUE: maqao_cqa_results_uid
                                                                    }
                                                                ]
                                }
                            ]
                        }
                    }
                    
                    output = ""
                    try:
                        output = plugin.execute_plugin_by_file(import_csv_plugin_uid, json, username, password)
                    except:
                        import logging
                        logging.exception("")
                        print json
                        print sys.exc_info()[1]
                        exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
                    
                    # import csv files into the maqao_cqa_results entry
                    try:
                        maqao_cqa_results_csv_file = entry.put_file_in_entry(
                                                        maqao_cqa_results_uid, 
                                                        csv_file, 
                                                        False
                                                      )
                        
                        entry.update_entry_parameter(maqao_cqa_results_uid, 
                                                     {"maqao_cqa_results_csv": {
                                                            "value":[maqao_cqa_results_csv_file], 
                                                            "append": True
                                                      }})
                    except IOError:
                        util.cti_plugin_print_error("No such file: '" + csv_file + "' ")

        print "[%s] End processing loops" % time.strftime('%Y/%m/%d %H:%M:%S',time.localtime())
