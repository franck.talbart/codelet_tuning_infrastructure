#! /bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Mathieu Tribalat, Nicolas Petit

# This script runs MAQAO CQA on a given binary

binary=
balancing_tolerance=
plugin_directory=
maqao_cqa_uid=
maqao_cqa_results_plugin_uid=
maqao_cqa_group_plugin_uid=
import_csv_plugin_uid=
language=
loop_file=
main_loops=
uarch=
repository=
schema_uid=
method=
tool_dir=
loop_uid_file_name=
_USER=
work_dir=
_PASSWORD=

########## Get parameters ##########
# b <binary>                        : Name of the binary to analyze
# B <balancing_tolerance>           : Balancing tolerance
# d <plugin_directory>              : Path to the plugin directory
# E <maqao_cqa_uid>                 : UID of the maqao cqa entry
# g <maqao_cqa_results_plugin_uid>  : maqao_cqa_results plugin UID
# G <maqao_cqa_group_plugin_uid>    : maqao_cqa_group plugin UID
# i <import_csv_plugin_uid>         : import_csv plugin UID
# l <language>                      : Programming language
# L <loop_file>                     : Loop file
# m <main_loops>                    : Main_loops parameters
# p <uarch>                         : Name of micro architecture used for analysis
# R <repository>                    : The repository
# s <schema_uid>                    : UID of the schema used for MAQAO CQA CSV
# t <method>                        : The method for loop process
# T <tool_dir>                      : Path to the third-party directory
# u <loop_uid_file_name>            : Loop UID file
# U <user>                          : CTI user
# w <work_dir>                      : Path to the working directory
# X <passwd>                        : CTI password

echo "### Running MAQAO CQA ###"

while getopts “b:B:d:E:g:G:i:l:L:m:p:R:s:t:T:u:U:w:X:” OPTION
do
    case $OPTION in
        b)
            binary=$OPTARG
            ;;
        B)
            balancing_tolerance=$OPTARG
            ;;
        d)
            plugin_directory=$OPTARG
            ;;
        E)
            maqao_cqa_uid=$OPTARG
            ;;
        g)
            maqao_cqa_results_plugin_uid=$OPTARG
            ;;
        G)
            maqao_cqa_group_plugin_uid=$OPTARG
            ;;
        i)
            import_csv_plugin_uid=$OPTARG
            ;;
        l)
            language=$OPTARG
            ;;
        L)
            loop_file=$OPTARG
            ;;
        m)
            main_loops=$OPTARG
            ;;
        p)
            uarch=$OPTARG
            ;;
        R)
            repository=$OPTARG
            ;;
        s)
            schema_uid=$OPTARG
            ;;
        t)
            method=$OPTARG
            ;;
        T)
            tool_dir=$OPTARG
            ;;
        u)
            loop_uid_file_name=$OPTARG
            ;;
        U)
            _USER=$OPTARG
            ;;
        w)
            work_dir=$OPTARG
            ;;
        X)
            _PASSWORD=$OPTARG
            ;;
        ?)
            exit
            ;;
    esac
done


#Check parameters
if [ -z "$maqao_cqa_uid" ]; then
    echo "FATAL ERROR: the entry is not given!" 1>&2
    exit 1
fi

if [ -z "$schema_uid" ]; then
    echo "FATAL ERROR: the schema is not given!" 1>&2
    exit 1
fi

if [ -z "$plugin_directory" ]; then
    echo "FATAL ERROR: the plugin directory is not given!" 1>&2
    exit 1
fi

if [ ! -e "$binary" ]; then
    echo "FATAL ERROR: the binary is not given!" 1>&2
    exit 1
fi

if [ -z "$balancing_tolerance" ]; then
    echo "FATAL ERROR: the balancing tolerance is not given!" 1>&2
    exit 1
fi

if [ -z "$_USER" ]; then
    echo "FATAL ERROR: the username is not given!" 1>&2
    exit 1
fi

if [ -z "$_PASSWORD" ]; then
    echo "FATAL ERROR: the password is not given!" 1>&2
    exit 1
fi

import_script="$plugin_directory/import_maqao_cqa_results.py"
update_script="$plugin_directory/update_maqao_cqa_csv.py"

#Making a unique identifier that will be used for files/folder names.
job_path=$(whoami)_$(date +"%Y%m%d_%H%M%S_%N")
mkdir -p $work_dir/$job_path
cd $work_dir/$job_path

#Get and untar the binary
cp $binary .
binary=`basename $binary`
tar xf $binary
rm -f $binary
binary=`basename $binary .tar.gz`

#--------------------- Runs MAQAO CQA ---------------------------
# parameters of MAQAO CQA:
#  module: select MAQAO CQA
#  bin: binary to analyze
#  fct: function to analyze
#  uarch: in CORE2_65, CORE2_45, NEHALEM, SANDY_BRIDGE. Used by the analyzer

maqao_cqa_output=`mktemp -p .`

first=0
while read line; do
    maqao_cmd="$tool_dir/maqao cqa bin=$binary loop=\"$line\" of=csv -ext"
    if [ ! -z $uarch ]; then
        maqao_cmd="$maqao_cmd uarch=$uarch"
    fi
    echo $maqao_cmd &>> $maqao_cqa_output
    eval $maqao_cmd &>> $maqao_cqa_output
    maqao_cqa_res=$?
    
    maqao_cqa_csv=`ls *.csv 2> /dev/null`
    if [ $first -eq 0 ]; then
        mv $maqao_cqa_csv tmp_maqao
    else
        nb_line=`cat $maqao_cqa_csv | wc -l`
        let "nb_line=nb_line-1"
        tail -n $nb_line $maqao_cqa_csv >> tmp_maqao
        rm $maqao_cqa_csv
    fi
    let "first=first+1"
done < $loop_file
mv tmp_maqao maqao_cqa.csv

cat $maqao_cqa_output

maqao_cqa_csv=`ls *.csv 2> /dev/null`
csv_exist=$?

#Save the unprocess MAQAO csv
tmp_dir=`mktemp -d -p .`
cp $maqao_cqa_csv $tmp_dir

#Record the MAQAO version
maqao_version=`$tool_dir/maqao --version | head -1`

if [ $maqao_cqa_res -ne 0 ] || [ $csv_exist -ne 0 ] || [ ! -e $maqao_cqa_csv ]; then
    echo -e "MAQAO CQA error"
    #Import results into CTI
    $import_script -m $maqao_cqa_output -M $maqao_cqa_uid -s 0 -v "$maqao_version"
else
    #Process results
    sed "s/;$//g" -i $maqao_cqa_csv
    
    ##Adding port balancing column
    sed -e '1 s/$/;port_balancing/' -e '1! s/$/;NA/' -i $maqao_cqa_csv
    
    ##Update Maqao CQA csv
    $update_script -f $maqao_cqa_csv -p $schema_uid -d ";"
    
    #Loads results into CTI
    input_json=`mktemp --tmpdir=$work_dir --suffix=input.json`
    echo "
    {
    \"init\": {
        \"attributes\": {
            \"name\": \"init\"
        }, 
        \"params\": [
            {
                \"name\": \"schema_uid\", 
                \"value\": \"$schema_uid\"
            }, 
            {
                \"name\": \"file\", 
                \"value\": \"$maqao_cqa_csv\"
            },
            {
                \"name\": \"command\", 
                \"value\": \"update\"
            },
            {
                \"name\": \"separator\", 
                \"value\": \";\"
            },
            {
                \"name\": \"output_parameter\", 
                \"value\": \"matrix_results\"
            },
            {
                \"name\": \"additional_parameters\", 
                \"value\": 
                         [
                            {
                                \"name\":\"entry\",
                                \"value\": \"$maqao_cqa_uid\"
                            }
                         ]
            }
        ]
      }
    }" > $input_json
    
    #Import results into CTI
    cti --user=$_USER $_PASSWORD $import_csv_plugin_uid @$input_json
    
    $import_script -B $balancing_tolerance -c $tmp_dir/$maqao_cqa_csv -g $maqao_cqa_results_plugin_uid -G $maqao_cqa_group_plugin_uid -i $import_csv_plugin_uid -l $main_loops -m $maqao_cqa_output -M $maqao_cqa_uid -s 1 -t $method -u $loop_uid_file_name -U $_USER -X $_PASSWORD -v "$maqao_version"

fi

#Cleaning temporary files
rm -rf $work_dir/$job_path
