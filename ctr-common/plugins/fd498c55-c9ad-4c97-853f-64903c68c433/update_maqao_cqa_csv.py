#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

# This script updates the CSV file produced by CQA with CTI maqao_cqa plugin parameters

import cti, ctr
from cti_hapi import util

import json, sys, os, getopt, csv

# constants
ARCHI = ["common", "x86_64", "k1om"]

#---------------------------------------------------------------------------
def short_column_name(column_name):
    """ Process column name
    Args:
        column_name: the original column name
    Returns:
        the short column name
    """
    for archi in ARCHI:
        column_name = column_name.replace(archi + "_", "")
    return str(column_name)
#---------------------------------------------------------------------------

maqao_csv_file_name = ""
plugin_uid = None
delimiter = ";"
# get parameters
try:
    opts, args = getopt.getopt(sys.argv[1:], "f:p:d:")
except:
    print sys.exc_info()[1]
    util.cti_plugin_print_error(sys.argv[0] + " -f <maqao_csv_file_name> -p <plugin_uid> -d <delimiter>")
    sys.exit(2)
for opt, arg in opts:
    if opt == "-f":
        maqao_csv_file_name = arg
    elif opt == "-p":
        plugin_uid = cti.CTI_UID(arg)
    elif opt == "-d":
        delimiter = arg
    else:
        assert False, "unhandled option"

if not os.path.isfile(maqao_csv_file_name):
    util.cti_plugin_print_error("Maqao csv file '%s' does not exist.")
    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

path_plugin = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN, plugin_uid)
output_file = os.path.join(path_plugin, cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_OUTPUT_FILENAME))

# read default maqao_cqa output json
file_js = open(output_file)
jo = json.load(file_js)
file_js.close()

metric = {}
for param in jo["init"]["params"]:
    if param.has_key(cti.META_ATTRIBUTE_MATRIX_GENERATED):
        for column in param["column_names"]:
            short_column = short_column_name(column)
            metric[short_column] = column

final_csv = []
# read from csv file produced by CQA
maqao_csv_file = open(maqao_csv_file_name, 'r')
csv_reader = csv.DictReader(maqao_csv_file, delimiter=delimiter)
for line in csv_reader:
    tmp_dict = {}
    for c_name, c_value in line.items():
        c_name = util.column_name_replace(c_name)
        if c_value is None:
            c_value = "NA"
        
        # correspond column names
        tmp_dict[metric[c_name]] = c_value
    final_csv.append(tmp_dict)
maqao_csv_file.close()
os.remove(maqao_csv_file_name)

# write into the csv file
if len(final_csv) == 0:
    util.cti_plugin_print_warning("The Maqao csv file is empty!")
else:
    # add not indicated fields
    for field in metric:
        if metric[field] not in final_csv[0]:
            for row in final_csv:
                row[metric[field]] = "NA"
    
    csv_file = open(maqao_csv_file_name, "w")
    csv_writer = csv.DictWriter(csv_file, fieldnames=final_csv[0].keys(), delimiter=';')
    # write column name
    csv_writer.writerow(dict((f, f) for f in final_csv[0].keys()))

    # write values
    for row in final_csv:
        csv_writer.writerow(row)
    csv_file.close()
