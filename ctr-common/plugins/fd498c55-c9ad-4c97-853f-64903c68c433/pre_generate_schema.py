#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

# This script generates the dynamic MAQAO CQA schema

import cti, ctr

from cti_hapi import entry, util

import json, sys, os, subprocess

filename_x86 = "x86.json"
uid = cti.CTI_UID(sys.argv[1])
path_plugin = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN, uid)

#---------------------------------------------------------------------------
def update_file(filename, cmd, result):
    infile, outfile = entry.load_defaults(uid)
    if not outfile["init"].params.has_key(result["name"]):
        file_input = open(filename)
        ji = json.load(file_input)
        with open(filename, 'w') as outfile:
            ji[cmd]["params"].insert(4, result)
            json.dump(ji, outfile, indent=4)
#---------------------------------------------------------------------------

maqao_bin = os.path.join(cti.cti_plugin_config_get_value(cti.THIRD_PARTY_DIR),
                         "maqao", "maqao")
sys.stdout.flush()
child = subprocess.Popen([maqao_bin, "cqa", "-cti", "-ext"], shell=False)
output = child.communicate()
rc = child.returncode
if rc != 0:
    raise Exception("Error with MAQAO CQA while getting the JSON file")

file_js = open(filename_x86)
jr = json.load(file_js)

result = {
            cti.META_ATTRIBUTE_NAME: "matrix_results", 
            cti.META_ATTRIBUTE_TYPE: "MATRIX", 
            cti.META_ATTRIBUTE_DESC: "MAQAO CQA results.",
            cti.META_ATTRIBUTE_LONG_DESC: "The MAQAO CQA results from the associated compilation.",
            cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES: [],
            cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES: [],
            cti.META_ATTRIBUTE_MATRIX_COLUMN_DESCS: [],
            cti.META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS: [],
            cti.META_ATTRIBUTE_OPTIONAL: True,
            cti.META_ATTRIBUTE_MATRIX_GENERATED: True
         }

for archi_k, archi_v in jr.items():
    #Moving the metrics into a list in order to sort them
    metric_list = []
    for metric_k, metric_v in archi_v.items():
        metric_list.append({
                                cti.META_ATTRIBUTE_NAME: metric_k,
                                cti.META_ATTRIBUTE_TYPE: metric_v["type"],
                                cti.META_ATTRIBUTE_DESC: metric_v["description"]
                           })
    
    sorted_metrics = sorted(metric_list, key=lambda k: k[cti.META_ATTRIBUTE_NAME])
    
    for m in sorted_metrics:
        param = util.column_name_replace(m[cti.META_ATTRIBUTE_NAME].strip())
        
        result[cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES].append("%s_%s" % (archi_k, param))
        result[cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES].append(m[cti.META_ATTRIBUTE_TYPE])
        result[cti.META_ATTRIBUTE_MATRIX_COLUMN_DESCS].append(m[cti.META_ATTRIBUTE_NAME])
        result[cti.META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS].append(m[cti.META_ATTRIBUTE_DESC])
    
    if archi_k == "common":
        #Adding port balancing column
        result[cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES].append("%s_port_balancing" % archi_k)
        result[cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES].append("TEXT")
        result[cti.META_ATTRIBUTE_MATRIX_COLUMN_DESCS].append("The port balancing")
        result[cti.META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS].append("The port balancing.")
    
update_file(os.path.join(path_plugin, cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_INPUT_FILENAME)),
            "update", result)
update_file(os.path.join(path_plugin, cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_OUTPUT_FILENAME)),
            "init", result)

os.remove(filename_x86)
