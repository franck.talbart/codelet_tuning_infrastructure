# Declare directory from where to load the experimental data sets

DATA_PATH <<-"data"

# Declare the architecture names
arch_names <- list(
                   "sandybridge"="Sandy Bridge",
                   "core2"="Core 2",
                   "atom"="Atom",
                   "knc"="KNC"
                   )

# Architecture clk frequencies (used to convert RDTSC cycles to time)
clks = list("reference" = 1860*103,
            "atom" = 1662*103,
            "core2" = 2933*103,
            "sandybridge" = 3330*103,
            "knc" = 1047*103)

# Declare well-behaved threshold
wellbehaved = 10

# Declare color palette
colours <- c("#000000","#AE1C3E", "#F5A275")
feature_set = c("Common.nb.flop.div","X86.64.nb.insn.sd","X86.64.nb.uops.p1","X86.64.ratio.add.sub...mul","X86.64.recmii..cycles.","Common.vec.ratio.....mul..fp.","Common.vec.ratio.....other","Common.vec.ratio.....other..fp.","Common..l1..bytes.stored...cycle","Common..l1..ipc")