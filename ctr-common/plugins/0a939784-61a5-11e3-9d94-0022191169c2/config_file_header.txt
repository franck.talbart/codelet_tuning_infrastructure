# Declare directory from where to load the experimental data sets

DATA_PATH <<-"data"

# Declare well-behaved threshold
wellbehaved = 10

# Declare color palette
colours <- c("#000000","#AE1C3E", "#F5A275")
