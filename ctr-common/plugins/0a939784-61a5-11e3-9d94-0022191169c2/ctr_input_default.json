{
    "init": {
        "attributes": {
            "produce_data": true, 
            "name": "init", 
            "repository": "local", 
            "desc": "Initialize the clustering experiment",
            "long_desc": "Initialize the clustering experiment by preparing the input files."
        }, 
        "params": [
            {
                "type": "TEXT", 
                "name": "query", 
                "desc": "The query used to select the codelets",
                "long_desc": "The query used to select the codelets to analyze.",
                "value": "loop_type:(outermost OR single) inclusive_nb_sample:>=5", 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "repository_query", 
                "desc": "The repository used with the query.",
                "long_desc": "The repository used with the query.",
                "value": "all", 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "prefix_lib_counting", 
                "desc": "The Lib Counting prefix",
                "long_desc": "The prefix, representing the architecture, used to get the metrics from Lib Counting.",
                "value": "sandy_bridge_",
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "features_maqao_cqa", 
                "desc": "The codelet's MAQAO CQA features",
                "long_desc": "The codelet's features needed to perform the clustering.",
                "list": true,
                "value": [
                            "common_Nb_FLOP_div",
                            "x86_64_Nb_insn_SD",
                            "x86_64_Nb_uops_P1",
                            "x86_64_Ratio_ADD_SUB_/_MUL",
                            "x86_64_RecMII_(cycles)",
                            "common_Vec_ratio_(%)_mul_(FP)",
                            "common_Vec_ratio_(%)_other",
                            "common_Vec_ratio_(%)_other_(FP)",
                            "common_(L1)_Bytes_stored_/_cycle",
                            "common_(L1)_IPC"
                         ], 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "fields_join_maqao_cqa", 
                "desc": "The MAQAO CQA fields",
                "long_desc": "The fields used to get the MAQAO CQA results",
                "value": "maqao_cqa_results.matrix_loop_results.", 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "fields_join_lib_counting", 
                "desc": "Lib Counting fields",
                "long_desc": "The fields used to get the lib_counting results",
                "value": "lib_counting_results.metrics.",
                "optional": true
            },
            {
                "type": "DATA_UID", 
                "name": "lib_counting_entry", 
                "desc": "Lib Counting entry",
                "long_desc": "The UID or Alias of the lib_counting entry from which experiments results are searched.", 
                "produced_by": "ffa07012-60ec-11e3-b233-cbdf068794cd",
                "value": [],
                "list": true,
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "type_query", 
                "desc": "The type of the query",
                "long_desc": "The type of the query",
                "value": "loop", 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "config_header_filename", 
                "desc": "The config header filename",
                "long_desc": "The config filename which contains the header of the config file",
                "value": "config_file_header.txt", 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "features_nehalem_lib_counting", 
                "desc": "The codelet's Lib Counting features needed for the clustering on nehalem",
                "long_desc": "The codelet's features needed to perform the clustering on nehalem.",
                "list": true,
                "value": [
                            "double_precision",
                            "L2_bandwidth",
                            "L3_miss_rate",
                            "memory_bandwidth"
                         ], 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "features_sandy_bridge_lib_counting", 
                "desc": "The codelet's Lib Counting features needed for the clustering on sandy_bridge",
                "long_desc": "The codelet's features needed to perform the clustering on sandy_bridge.",
                "list": true,
                "value": [
                            "double_precision",
                            "L2_bandwidth",
                            "memory_bandwidth"
                         ], 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "features_haswell_lib_counting", 
                "desc": "The codelet's Lib Counting features needed for the clustering on haswell",
                "long_desc": "The codelet's features needed to perform the clustering on haswell.",
                "list": true,
                "value": [
                            "compute_instructions",
                            "L2_bandwidth",
                            "memory_bandwidth"
                         ], 
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "format", 
                "desc": "Format",
                "long_desc": "The format of the output. 'txt' will display the output in a human readable manner. 'json' will output a JSON string containing the same informations.", 
                "value": "txt", 
                "optional": true
            }, 
            {
                "type": "BOOLEAN", 
                "name": "auto_run", 
                "desc": "Auto run?",
                "long_desc": "false: Do not call the run command after creation. true: automatically call the run command after creation.", 
                "value": true, 
                "optional": true
            },
            {
                "type": "PLUGIN_UID", 
                "name": "entry_plugin_uid", 
                "desc": "entry plugin uid", 
                "long_desc": "The entry plugin UID", 
                "optional": true, 
                "value": "fbaceb00-8477-11e1-8561-0022191169c2"
            }
        ]
    },
    "run": {
        "attributes": {
            "long_desc": "Launches the clustering using the given clustering entry.", 
            "produce_data": false, 
            "name": "run", 
            "desc": "Runs the clustering on given data"
        }, 
        "params": [
            {
                "type": "DATA_UID", 
                "name": "entry", 
                "desc": "The clustering entry",
                "long_desc": "The UID or Alias of the clustering entry to run.", 
                "produced_by": "0a939784-61a5-11e3-9d94-0022191169c2"
            },
            {
                "type": "INTEGER", 
                "name": "nb_clusters", 
                "desc": "The number of clusters to produce.",
                "long_desc": "The number of clusters to produce.",
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "tag_name", 
                "desc": "The tag name.",
                "long_desc": "The tag name used to tag the loop in the clusters.",
                "optional": true,
                "value": "cluster"
            },
            {
                "type": "FLOAT", 
                "name": "thres", 
                "desc": "The threshold.",
                "long_desc": "The threshold used to produce the clusters.",
                "optional": true,
                "value": 0.95
            },
            {
                "type": "TEXT", 
                "name": "method", 
                "desc": "The method.",
                "long_desc": "The method used to produce the clusters.(euclidean/manhattan/maximum/canberra/minkowski)",
                "optional": true,
                "value": "euclidean"
            },
 
            {
                "type": "PLUGIN_UID",
                "name": "tag_plugin_uid",
                "desc": "tag plugin uid",
                "long_desc": "The tag plugin UID",
                "optional": true,
                "value": "98fb0f82-3fac-11e2-9506-0022191169c2"
            },
            {
                "type": "PLUGIN_UID", 
                "name": "query_plugin_uid", 
                "desc": "query plugin uid", 
                "long_desc": "The query plugin UID", 
                "optional": true, 
                "value": "f685a9f0-ef89-4c08-8585-4c20c760cfe0"
            },
            {
                "type": "TEXT", 
                "name": "format", 
                "desc": "Format",
                "long_desc": "The format of the output. 'txt' will display the output in a human readable manner. 'json' will output a JSON string containing the same informations.", 
                "optional": true, 
                "value": "txt"
            }
        ]
    }, 
    "update": {
        "attributes": {
            "long_desc": "Updates the given clustering entry using the provided parameters.", 
            "produce_data": false, 
            "name": "update", 
            "desc": "Updates the entry."
        }, 
        "params": [
            {
                "type": "DATA_UID", 
                "name": "entry", 
                "desc": "The maqao PERF entry",
                "long_desc": "The UID or Alias of the maqao perf entry to fill.", 
                "produced_by": "dc7711fa-16ea-11e3-8d3b-60eb69a62f1e",
                "optional": true
            },
            {
                "type": "TEXT", 
                "name": "query", 
                "desc": "The query used to select the codelets",
                "long_desc": "The query used to select the codelets to analyze."
            },
            {
                "type": "TEXT", 
                "name": "repository_query", 
                "desc": "The repository used with the query.",
                "long_desc": "The repository used with the query."
            },
            {
                "type": "TEXT", 
                "name": "features_lib_counting", 
                "desc": "The codelet's Lib Counting features",
                "long_desc": "The codelet's features needed to perform the clustering.",
                "list": true
            },
            {
                "type": "TEXT", 
                "name": "features_maqao_cqa", 
                "desc": "The codelet's MAQAO CQA features",
                "long_desc": "The codelet's features needed to perform the clustering.",
                "list": true
            },
            {
                "type": "TEXT", 
                "name": "prefix_lib_counting", 
                "desc": "The Lib Counting prefix",
                "long_desc": "The prefix, representing the architecture, used to get the metrics from Lib Counting.",
                "value": "sandy_bridge_",
                "optional": true
            },
            {
                "type": "DATA_UID", 
                "name": "lib_counting_entry", 
                "desc": "Lib Counting entry",
                "long_desc": "The UID or Alias of the lib_counting entry from which experiments results are searched.", 
                "produced_by": "ffa07012-60ec-11e3-b233-cbdf068794cd",
                "list": true
            },
            {
                "type": "FILE", 
                "name": "config_clustering", 
                "desc": "The config file",
                "long_desc": "The config file used to perform the clustering."
            },
            {
                "type": "FILE", 
                "name": "CSV", 
                "desc": "The CSV input file",
                "long_desc": "The CSV input file used to perform the clustering."
            },
            {
                "type": "FILE", 
                "name": "result", 
                "desc": "The CSV result file",
                "long_desc": "The CSV result file used to tag the codelets.",
                "list": true    
            },
            {
                "type": "FILE", 
                "name": "log_clustering", 
                "desc": "The log file of the clustering script",
                "long_desc": "The log file containing the output of the clustering script.",
                "list": true
            }
        ]
    }
}
