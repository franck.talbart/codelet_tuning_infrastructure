#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

from cti_hapi.main import HapiPlugin, hapi_command
from cti_hapi import util_uid, util, entry, plugin, alias
import cti

import sys, shutil, os, csv, tempfile, subprocess, re, numpy, json

#---------------------------------------------------------------------------

def format_header_R(str):
    """
        This function replaces all the non alphanumeric characters to dot.
        On top of that, if the first character is a dot, it adds an "X".
        This is required by the R script
    """
    str = re.sub(r'[^a-zA-Z0-9]', ".", str)
    if str[0] == ".":
        str = "X"+str
    
    return str.capitalize()

#---------------------------------------------------------------------------

class ClusteringPlugin(HapiPlugin):
    @hapi_command("init")
    def init_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        
        config_filename = "config.R"
        output_csv_filename = "OUTPUT-features.csv"
        config_header_filename = params["config_header_filename"]
        entry_plugin_uid = params["entry_plugin_uid"]
        query = params["query"]
        repository_query = params["repository_query"]
        prefix_lib_counting = params["prefix_lib_counting"]
        if not prefix_lib_counting:
            print "Param prefix_lib_counting can't be None."
            return None
        features_lib_counting = params["features_" + prefix_lib_counting + "lib_counting"]

        auto_run = params["auto_run"]
        
        #Compute fields list
        list_fields = []
        
        if params["fields_join_maqao_cqa"]:
            list_fields += [params["fields_join_maqao_cqa"] + f for f in params["features_maqao_cqa"]]
        else:
            list_fields += params["features_maqao_cqa"]
        
        if params['lib_counting_entry']:
            query_lc = []
            for lc in params['lib_counting_entry']:
                query_lc.append(" lib_counting_results.lib_counting_group.lib_counting.entry_uid:{0} ".format(str(lc)))
            query += "OR".join(query_lc)
            if params["fields_join_lib_counting"]:
                list_fields += [params["fields_join_lib_counting"] + \
                        prefix_lib_counting + f for f in \
                        features_lib_counting]
            else:
                list_fields += features_lib_counting
        
        
        config_file = open(os.path.join(self.plugin_directory, config_header_filename), "r")
        config_file_content = config_file.readlines()
        config_file.close()
        
        config_file_content.append("feature_set = c(")
        
        # Only the feature name is kept
        features = ["\"" + format_header_R(f.split(".")[-1]) + "\"" for f in list_fields]
        config_file_content.append(",".join(features))
        config_file_content.append(")")
        
        data_entry = self.default_init_command(params)
        
        output_dir = tempfile.mkdtemp()
        os.chdir(output_dir)
        config_file = open(config_filename, "w")
        config_file.writelines(config_file_content)
        config_file.close()
        
        list_fields = ["alias"] + list_fields
        list_fields += ["lib_counting_results.invocations", "lib_counting_results.cycles"]
        
        json_file = \
        {
            "export": 
            {
                "attributes": {cti.META_ATTRIBUTE_NAME : "export"},
                "params": 
                [
                    {cti.META_ATTRIBUTE_NAME : "format_output",cti.META_ATTRIBUTE_VALUE : "csv"},
                    {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : query},
                    {cti.META_ATTRIBUTE_NAME : "repository_query",cti.META_ATTRIBUTE_VALUE : repository_query},
                    {cti.META_ATTRIBUTE_NAME : "type_query",cti.META_ATTRIBUTE_VALUE : params["type_query"]},
                    {cti.META_ATTRIBUTE_NAME : "filename",cti.META_ATTRIBUTE_VALUE : output_csv_filename + "_tmp"},
                    {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : list_fields}
                ]
            }
        }
        
        try:
            plugin.execute_plugin_by_file(entry_plugin_uid, json_file, self.username, self.password)
        except:
            print sys.exc_info()[1]
            print "Input:"
            print str(json_file)
            return None
        
        
        # Recording the feature names only
        fr = open(output_csv_filename + "_tmp", "rb")
        cr = csv.reader(fr)
        header_line = cr.next()
        header_line_updated = []
        for e in header_line:
            if e == "Alias":
                e = "CodeletName"
            else:
                e = format_header_R(e.split(".")[-1])
            header_line_updated.append(e)
        
        fw = open(output_csv_filename, "wb")
        cw = csv.writer(fw)
        cw.writerow(header_line_updated)
        
        for c in cr:
            for i in range(len(c)):
                if c[i] == "None":
                    c[i] = ""
            cw.writerow(c)
            
        fr.close()
        fw.close()
        
        output_csv = entry.put_file_in_entry(
                data_entry.uid, 
                output_csv_filename, 
                False
        )
        
        config_r = entry.put_file_in_entry(
                data_entry.uid, 
                config_filename, 
                False
        )
        entry.update_entry_parameter(data_entry.uid,
                                      {"CSV":
                                            {"value": output_csv},
                                       "config_clustering":
                                            {"value": config_r},
                                      "features_lib_counting":
                                            {"value":
                                                features_lib_counting}
                                      }
                                    )
        shutil.rmtree(output_dir)
        plugin.plugin_auto_run(self, data_entry, None, auto_run)
        return 0
    
    #---------------------------------------------------------------------------
    
    @hapi_command("run")
    def run_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        output_features_filename = "OUTPUT-features.csv"
        nb_clusters = params["nb_clusters"]
        thres = params["thres"]
        method = params["method"]
        entry_uid = params["entry"]
        tag_name = params["tag_name"]
        tag_plugin_uid = params["tag_plugin_uid"]
        query_plugin_uid = params["query_plugin_uid"]
        clustering_filename = "clustering.csv"
        driver_filename = "driver.R"
        pca_filename = "pca.R"
        (input_param, output_param) = entry.load_data(entry_uid)
        config_clustering = output_param["init"].params["config_clustering"][cti.META_ATTRIBUTE_VALUE]
        lib_counting_entry = output_param["init"].params["lib_counting_entry"][cti.META_ATTRIBUTE_VALUE]
        csv_input = output_param["init"].params["CSV"][cti.META_ATTRIBUTE_VALUE]
        prefix_lib_counting = output_param["init"].params["prefix_lib_counting"][cti.META_ATTRIBUTE_VALUE]

        if method not in ["euclidean", "manhattan","maximum","canberra", "minkowski"]:
            util.cti_plugin_print_error("Error: method must have one of the following values: euclidean, manhattan, maximum,canberra, minkowski")
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

        entry.update_entry_parameter(entry_uid,
                                      {
                                       "nb_clusters": {"value": nb_clusters},
                                       "thres": {"value": thres},
                                       "method": {"value": method}
                                      }
                                    )
        
        output_dir = tempfile.mkdtemp()
        os.chdir(output_dir)
        
        entry.get_file_from_entry(entry_uid, config_clustering)
        os.mkdir("data")
        entry.get_file_from_entry(entry_uid, csv_input, destname="data")
        
        driver_path = os.path.join(self.plugin_directory, driver_filename)
        
        shutil.copy(driver_path, os.path.basename(driver_path))
        shutil.copytree(os.path.join(self.plugin_directory, "lib"), "lib")
        driver_path = './' + os.path.basename(driver_path)
        
        cmd = [driver_path, "output"]
        
        if nb_clusters:
            cmd.append(str(nb_clusters))
        else:
            cmd.append("-1")

        cmd.append(str(thres))
        cmd.append(str(method))
            
        log_clustering_file = open("log_clustering.txt",'a')
        process = subprocess.Popen(cmd,
                                   stdout=log_clustering_file,
                                   stderr=subprocess.STDOUT)
        
        process.communicate()
        log_clustering_file.close()
        
        output_log = entry.put_file_in_entry(
                entry_uid, 
                "log_clustering.txt", 
                False
        )
        
        entry.update_entry_parameter(entry_uid,
                                      {"log_clustering":
                                            {"value": [output_log]}
                                      }
                                    )
        
        try:
            clustering_file_path = entry.put_file_in_entry(
                    entry_uid, 
                    clustering_filename, 
                    False
            )
        except:
           util.cti_plugin_print_error("Something went wrong when running the R script. Here is the log file:")
           os.system("cat " + output_log)
           exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        
        cr = csv.reader(open(clustering_filename,"rb"))
        cr.next()
        ci = list(csv.reader(open(os.path.join("data", csv_input),"rb")))
        output_csv_file = tempfile.NamedTemporaryFile(delete=False)
        output_csv = csv.writer(open(output_csv_file.name, "wb"))

        #Adding the MAQAO coverages of each codelet to the  OUTPUT-features.csv file.
        
        entry.get_file_from_entry(entry_uid, output_features_filename)
        output_features_file = open(output_features_filename, "rb")
        output_features_reader = csv.reader(output_features_file)
        output_features_header = output_features_reader.next()

        #Adding the "Coverage MAQAO" column.
        #If it exists, then we don't have to do anything since the MAQAO
        #coverages have already been calculated.
        if "Coverage MAQAO" not in output_features_header:
            
            #We can't read and write simultaneously in the same file.
            #Thus, to update the OUTPUT-features.csv file, we create a temporary file. It will replace the original in the end.
            output_features_file_tmp = open(output_features_filename + "_tmp", "wb")
            output_features_writer = csv.writer(output_features_file_tmp)
            
            output_features_header.append("Coverage MAQAO")
            output_features_writer.writerow(output_features_header)
        
            #Adding the MAQAO coverage of each codelet.
            for line in output_features_reader:
                codelet_name = line[0]
                _,output_codelet = entry.load_data(util_uid.CTI_UID(codelet_name))
                coverage_maqao = output_codelet["init"].params["coverage_maqao"][cti.META_ATTRIBUTE_VALUE]
                line.append(coverage_maqao)
                output_features_writer.writerow(line)
        
            output_features_file.close()
            output_features_file_tmp.close()

            #Overriding the current OUTPUT-features.csv to update its content.
            os.rename(output_features_file_tmp.name, output_features_filename)
            entry.put_file_in_entry(entry_uid, output_features_filename, False)

        # Header
        output_csv.writerow(["BenchName", "Cluster"] + ci[0][1:])
        cluster_feature = {}
        loops_feature = {}
        for row_cr in cr:
            codelet_name = row_cr[1]
            cluster_id = row_cr[2]
            _,output_codelet = entry.load_data(util_uid.CTI_UID(codelet_name))
            
            for row_ci in ci[1:]:
                if codelet_name in row_ci:
                    loop_group = output_codelet["init"].params["loop_group"][cti.META_ATTRIBUTE_VALUE]
                    line = [alias.get_data_alias(loop_group), cluster_id] + row_ci[1:]
                    output_csv.writerow(line)
                    break
                
            tag = "%s_%s" % (tag_name, cluster_id)
            
            #Tagging the codelet
            json_file = \
            {
                "add": 
                {
                    "attributes": {cti.META_ATTRIBUTE_NAME : "add"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "entry",cti.META_ATTRIBUTE_VALUE : codelet_name},
                        {cti.META_ATTRIBUTE_NAME : "tag",cti.META_ATTRIBUTE_VALUE : tag}
                    ]
                }
            }
            
            result = ""
            
            try:
                print "Tagging " + codelet_name
                result = plugin.execute_plugin_by_file(tag_plugin_uid, json_file, self.username, self.password)
                print result
            except:
                print result
                print sys.exc_info()[1]
                exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
            
            coverage_maqao = output_codelet["init"].params["coverage_maqao"][cti.META_ATTRIBUTE_VALUE]
            uid_maqao_result = output_codelet["init"].params["maqao_cqa_results"][cti.META_ATTRIBUTE_VALUE]
            if uid_maqao_result is not None:
                (_, output_maqao_result) = entry.load_data(cti.CTI_UID(str(uid_maqao_result)))
            else:
                output_maqao_result = None
            
            output_lib_counting_result = None
            lib_counting_result_uid = None
            
            # WARNING: this query does not allow to have several lib_counting experiments for the same app
            # Actually, the issue could occur, but this is kind of useless doing this in this plugin.
            if lib_counting_entry:
                l_c_query_list = "OR".join([' lib_counting_group.lib_counting.entry_uid:{0} '.format(str(l)) for l in lib_counting_entry])
                l_c_results_query = '{0} loop.entry_uid:{1}'.format(l_c_query_list,str(util_uid.CTI_UID(codelet_name)))
                json_file = \
                {
                    "select": 
                    {
                        "attributes": {cti.META_ATTRIBUTE_NAME : "select"},
                        "params": 
                        [
                            {cti.META_ATTRIBUTE_NAME : "repository",cti.META_ATTRIBUTE_VALUE : 'all'},
                            {cti.META_ATTRIBUTE_NAME : "query",cti.META_ATTRIBUTE_VALUE : l_c_results_query},
                            {cti.META_ATTRIBUTE_NAME : "type",cti.META_ATTRIBUTE_VALUE : 'lib_counting_results'},
                            {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "json"},
                            {cti.META_ATTRIBUTE_NAME : "size",cti.META_ATTRIBUTE_VALUE : -1},
                            {cti.META_ATTRIBUTE_NAME : "fields",cti.META_ATTRIBUTE_VALUE : ["entry_info.entry_uid"]}
                        ]
                    }
                }
                result = ""
                try:
                    result = plugin.execute_plugin_by_file(query_plugin_uid, json_file, self.username,self.password)
                    query_result = json.loads(result)
                    if query_result['data']:
                        lib_counting_result_uid = query_result['data'][0]['entry_info.entry_uid']
                    else:
                        lib_counting_result_uid = None
                except:
                    print sys.exc_info()[1]
                    exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)
                    
            if lib_counting_result_uid != None:
                (_, output_lib_counting_result) = entry.load_data(cti.CTI_UID(str(lib_counting_result_uid)))
            
            loops_feature[codelet_name] = {'cluster':cluster_id}
            
            for feature in output_param["init"].params["features_maqao_cqa"][cti.META_ATTRIBUTE_VALUE]:
                if not cluster_feature.has_key(cluster_id):
                    cluster_feature[cluster_id] = {}
                    cluster_feature[cluster_id]["coverage_maqao"] = []
                
                if not cluster_feature[cluster_id].has_key(feature):
                    cluster_feature[cluster_id][feature] = []
                if output_maqao_result is None:
                    cluster_feature[cluster_id][feature].append(None)
                    loops_feature[codelet_name][format_header_R(feature.split(".")[-1])] = ''
                else:
                    cluster_feature[cluster_id][feature].append(output_maqao_result["init"].params["matrix_loop_results"][cti.META_ATTRIBUTE_VALUE][feature][0])
                    loops_feature[codelet_name][format_header_R(feature.split(".")[-1])] = output_maqao_result["init"].params["matrix_loop_results"][cti.META_ATTRIBUTE_VALUE][feature][0]                   
                    
            cluster_feature[cluster_id]["coverage_maqao"].append(coverage_maqao)
            
            for feature in output_param["init"].params["features_lib_counting"][cti.META_ATTRIBUTE_VALUE]:
                feature = prefix_lib_counting + feature
                if not cluster_feature[cluster_id].has_key(feature):
                    cluster_feature[cluster_id][feature] = []
                    
                if output_lib_counting_result is None:
                    cluster_feature[cluster_id][feature].append(None)
                    loops_feature[codelet_name][format_header_R(feature.split(".")[-1])]= ''
                else:
                    cluster_feature[cluster_id][feature].append(output_lib_counting_result["init"].params["metrics"][cti.META_ATTRIBUTE_VALUE][feature][0])
                    loops_feature[codelet_name][format_header_R(feature.split(".")[-1])]= output_lib_counting_result["init"].params["metrics"][cti.META_ATTRIBUTE_VALUE][feature][0]
        print "Computing the median values of each clusters..."
        file_median  = open('median_cluster.csv', "wb")
        writer_median = csv.writer(file_median)
        
        header = []
        for feature in output_param["init"].params["features_maqao_cqa"][cti.META_ATTRIBUTE_VALUE]:
                header.append(feature)
        for feature in output_param["init"].params["features_lib_counting"][cti.META_ATTRIBUTE_VALUE]:
                header.append(prefix_lib_counting + feature)
        
        writer_median.writerow(["Cluster", "Coverage MAQAO"] + [format_header_R(h.split(".")[-1]) for h in header] + ["Invocations", "Cycles"])
        for c in cluster_feature:
            row = ["%s" % c, numpy.sum(cluster_feature[c]["coverage_maqao"])]
            for f in header:
                if cluster_feature[c].has_key(f):
                    if cluster_feature[c][f].count(None) > len(cluster_feature[c][f]) / 2:
                        median = ""
                    else:
                        median = numpy.median([l for l in cluster_feature[c][f] if l is not None])
                else:
                    median = ""
                row.append(median)
            row.extend(('-', '-')) #Adding blanks for the Invocations and Cycles columns.
            writer_median.writerow(row)
        file_median.close()

        print "Computing the global median values..."
        file_median_glob  = open('median_global.csv', "wb")
        writer_median = csv.writer(file_median_glob)
        
        median_dict = {}
        writer_median.writerow([format_header_R(h.split(".")[-1]) for h in header])

        row = []
        for f in [format_header_R(h.split(".")[-1]) for h in header]:
            temp_f = []
            for l in loops_feature:
                if loops_feature[l][f] != "":
                    temp_f.append(loops_feature[l][f])
                else:
                    temp_f.append(None)
            if temp_f.count(None) > len(temp_f) / 2:
                median = ""
            else:
                median = numpy.median([l for l in temp_f if l is not None])
            row.append(median)
            median_dict[f] = median
        writer_median.writerow(row)
        file_median_glob.close()

        print "Computing the deviation values of each codelet..."
        file_dev  = open('deviation.csv', "wb")
        writer_dev = csv.writer(file_dev)
        
        writer_dev.writerow(["CodeletName"] + [format_header_R(h.split(".")[-1]) for h in header])
        
        for loop in loops_feature:
            current_line = [loop]
            #Using the same header as before
            for head in [format_header_R(h.split(".")[-1]) for h in header]:
                curr_val = None
                try:
                    if loops_feature[loop][head] is not None:
                        curr_val = float(loops_feature[loop][head]) - float(median_dict[head])
                except ValueError:
                    #Do nothing. The value was void instead of a float value.
                    pass
                current_line.append(curr_val)
            writer_dev.writerow(current_line)
        file_dev.close()
        
        #Adding generated CSVs to the entry

        dev_csv = entry.put_file_in_entry(
                entry_uid, 
                file_dev.name, 
                False
        )
        
        output_csv = entry.put_file_in_entry(
                entry_uid, 
                file_median.name, 
                False
        )

        output_csv_glob = entry.put_file_in_entry(
                entry_uid, 
                file_median_glob.name, 
                False
        )
 
        entry.update_entry_parameter(entry_uid,
                                      {"result":
                                            {"value": [clustering_file_path, output_csv, output_csv_glob, dev_csv]}
                                      }
                                    )

        output_csv_file.close()

        print "Generating the PCA..." 
        pca_path = os.path.join(self.plugin_directory, pca_filename)
        
        shutil.copy(pca_path, os.path.basename(pca_path))
        pca_path = './' + os.path.basename(pca_path)
        
        cmd = [pca_path, output_csv_file.name]

        log_pca_file = open("log_pca.txt",'a')
        
        process = subprocess.Popen(cmd,
                                   stdout=log_pca_file,
                                   stderr=subprocess.STDOUT)
        
        process.communicate()
        log_pca_file.close()

        output_log = entry.put_file_in_entry(
                entry_uid, 
                "log_pca.txt", 
                False
        )
        
        entry.update_entry_parameter(entry_uid,
                                      {"log_clustering":
                                          {"value": [output_log], "append": True}
                                      }
                                    )
        try:

           pca = entry.put_file_in_entry(
                                        entry_uid, 
                                        "pca.pdf", 
                                        False
                                         )
           input_csv_pca = entry.put_file_in_entry(
                                        entry_uid, 
                                        output_csv_file.name, 
                                        False
                                         )

        except:
           util.cti_plugin_print_error("Something went wrong when running the R script. Here is the log file:")
           os.system("cat " + output_log)
           exit(cti.CTI_PLUGIN_ERROR_UNEXPECTED)

        entry.update_entry_parameter(entry_uid,
                                      {"result":
                                            {
                                                "value": [pca, input_csv_pca],
                                                "append": True
                                            }
                                      }
                                    )
        shutil.rmtree(output_dir)
        os.remove(output_csv_file.name)

#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = ClusteringPlugin()
    exit(p.main(sys.argv))
