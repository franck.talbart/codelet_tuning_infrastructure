#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import alias, cti_json, util
from cti_hapi.main import HapiPlugin, hapi_command

import sys

class AliasPlugin(HapiPlugin):
    @hapi_command("data")
    def data_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        alias_s = params["alias"]
        uid = params["uid"]
        output_format = params["format"]
        result = make_alias(cti.CTR_ENTRY_DATA, alias_s, uid)
        if output_format == "json":
            print cti_json.cti_json_encode({'result':result})
        elif output_format == "txt":
            if result == 0:
                print "Successfully added alias.\n"
                print "Alias is now %s.\n" % (alias_s)
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------
    
    @hapi_command("plugin")
    def plugin_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        alias_s = params["alias"]
        uid = params["uid"]
        output_format = params["format"]
        result = make_alias(cti.CTR_ENTRY_PLUGIN, alias_s, uid)
        if output_format == "json":
            print cti_json.cti_json_encode({'result':result})
        elif output_format == "txt":
            if result == 0:
                print "Successfully added alias.\n"
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------
    
    @hapi_command("repository")
    def repository_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        alias_s = params["alias"]
        uid = params["uid"]
        output_format = params["format"]
        result = make_alias(cti.CTR_ENTRY_REPOSITORY, alias_s, uid)
        if output_format == "json":
            print cti_json.cti_json_encode({'result':result})
        elif output_format == "txt":
            if result == 0:
                print "Successfully added alias.\n"
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------
    
    @hapi_command("get_plugin_uid")
    def get_plugin_uid_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        uid = params["uid"]
        output_format = params["format"]
        
        if output_format == "json":
            print cti_json.cti_json_encode({'uid':str(uid)})
        elif output_format == "txt":
            print "PLUGIN_CTI_UID=" + str(uid)
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        
#---------------------------------------------------------------------------
    
    @hapi_command("get_plugin_alias")
    def get_plugin_alias_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        uid = params["uid"]
        output_format = params["format"]
        if not uid:
            util.cti_plugin_print_error("there is no specified uid")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        result = alias.get_plugin_alias(uid)
        if output_format == "json":
            print cti_json.cti_json_encode({'alias':result})
        elif output_format == "txt":
            print result
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT

#---------------------------------------------------------------------------

    @hapi_command("get_data_uid")
    def get_data_uid_cmd(self, params):
        """ Converts a list of data aliases into a list of data uids.

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
            Nothing
        """
        uid_list = params["uid_list"]
        output_format = params["format"]

        if output_format == "json":
            print cti_json.cti_json_encode({'uid_list':uid_list})
        elif output_format == "txt":
            print "DATA_CTI_UID=" + str(uid_list)
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT

#---------------------------------------------------------------------------
    
    @hapi_command("get_data_alias")
    def get_data_alias_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        uid = params["uid"]
        output_format = params["format"]
        if not uid:
            util.cti_plugin_print_error("there is no specified uid")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        result = cti.cti_plugin_alias_data_get_key(uid)
        
        if output_format == "json":
            print cti_json.cti_json_encode({'alias':result})
        elif output_format == "txt":
            print result
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------
    
    @hapi_command("get_repository_uid")
    def get_repository_uid_cmd(self, params):
        """ Description

        Args:
            command: just command
            work_params: working parameters

        Returns:
          Nothing
        """
        uid = params["uid"]
        output_format = params["format"]
        
        if output_format == "json":
            print cti_json.cti_json_encode({'uid':str(uid)})
        elif output_format == "txt":
            print "REPOSITORY_CTI_UID=" + str(uid)
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
        
#---------------------------------------------------------------------------
    
    @hapi_command("get_repository_alias")
    def get_repository_alias_cmd(self, params):
        """ Description

        Args:
            self: class of the plugin
            params: working parameters

        Returns:
          Nothing
        """
        uid = params["uid"]
        output_format = params["format"]
        if not uid:
            util.cti_plugin_print_error("there is no specified uid")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        result = cti.cti_plugin_alias_repository_get_key(uid)
        if output_format == "json":
            print cti_json.cti_json_encode({'alias':result})
        elif output_format == "txt":
            print result
        else:
            util.cti_plugin_print_error("Unknown format " + str(output_format))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------

def make_alias(entry_type, alias_s, uid):
    if entry_type == cti.CTR_ENTRY_REPOSITORY:
        if alias.set_repository_alias(uid, alias_s) == 0:
            util.cti_plugin_print_error("This alias is already used!" +\
                  "Please choose another one or rename the repository which uses it.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    else:
        path = ctr.ctr_plugin_get_path_by_uid(entry_type, uid)
        if not path:
            util.cti_plugin_print_error("Can't find specified UID, please check it and try again.")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        else:
            if entry_type == cti.CTR_ENTRY_PLUGIN:
                if alias.set_plugin_alias(uid, alias_s) == 0:
                    util.cti_plugin_print_error("This alias is already used! Please choose another one or rename " +\
                          "the plugin which uses it.")
                    return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            elif entry_type == cti.CTR_ENTRY_DATA:
                if alias.set_data_alias(uid, alias_s) == 0:
                    util.cti_plugin_print_error("This alias is already used! Please choose another one or rename " +\
                          "the data which uses it.")
                    return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            else:
                util.cti_plugin_print_error("Unknown type")
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    return 0
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = AliasPlugin()
    exit(p.main(sys.argv))
