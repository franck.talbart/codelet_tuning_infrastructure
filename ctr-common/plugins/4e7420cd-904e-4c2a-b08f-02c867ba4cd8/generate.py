#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import entry, alias
import os, tempfile

import wiki2man

#---------------------------------------------------------------------------

def generate_wiki(plugin_uid):
    plugin_name = alias.get_plugin_alias(plugin_uid)

    result = \
    [
        "= %s =\n" % plugin_name,
    ]
    result.append("== Command list ==\n")

    infile, outfile = entry.load_defaults(plugin_uid)

    for cmd in infile:
        result.append(" * %s:\n" % cmd)
        for key, value in infile[cmd].attributes.items():
            result.append("     " + key + " : " + str(value) + "\n")

    path = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN, plugin_uid)
    path = os.path.join(path, "ctr_doc.txt")

    if os.path.isfile(path):
        doc_file = open(path, "r")
        line = doc_file.readline()
        while line != "":
            result.append(line)
            line = doc_file.readline()

        doc_file.close()

    for cmd in infile:
        if infile[cmd].attributes.has_key(cti.META_ATTRIBUTE_NAME):
            result.append("== %s ==\n" % infile[cmd].attributes[cti.META_ATTRIBUTE_NAME])
        else:
            result.append("== UNKNOWN ==\n")

        result.append("=== Command input ===\n")
        result += get_params(infile, cmd, plugin_name)
        if cmd in outfile:
            result.append("=== Command output ===\n")
            result += get_params(outfile, cmd, plugin_name, False)

    return result
#---------------------------------------------------------------------------

def generate_man(content):
    f = tempfile.NamedTemporaryFile(mode='w+')
    f.write(".TH CTI 1 \"2015\" Linux \"User Manuals\"")
    f.write(wiki2man.wiki2man(content))
    f.flush()
    return f
#---------------------------------------------------------------------------

def get_params(f, cmd, plugin_name, input=True):
    command = "$ cti %s %s''" % (plugin_name, cmd)
    result = []
    for p in f[cmd].params:

        if f[cmd].params[p].has_key(cti.META_ATTRIBUTE_OPTIONAL) and f[cmd].params[p][cti.META_ATTRIBUTE_OPTIONAL]:
            command += " [%s]" % (f[cmd].params[p][cti.META_ATTRIBUTE_NAME])
        else:
            command += " %s" % (f[cmd].params[p][cti.META_ATTRIBUTE_NAME])
        try:
            result.append("===%s===\n" % (f[cmd].params[p][cti.META_ATTRIBUTE_NAME]))
            result += ["'''Label'''","    %s" % (f[cmd].params[p][cti.META_ATTRIBUTE_DESC])]
            result += ["'''Type'''","    %s" % (f[cmd].params[p][cti.META_ATTRIBUTE_TYPE])]
            result += ["'''Description'''","    %s" % (f[cmd].params[p].get(cti.META_ATTRIBUTE_LONG_DESC, "No description available."))]
            result += ["\n\n"]
        except:
            print "Error while generating the documentation: command: %s param: %s plugin: %s" % (command, p, plugin_name)

    if input:
        result = ["\n\n<pre>%s''</pre>\n\n"%(command)] + result

    return result

#---------------------------------------------------------------------------
