#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi.main import HapiPlugin, hapi_command
from cti_hapi import util

import sys, os

import generate, google_code_doc

class DocPlugin(HapiPlugin):
    @hapi_command("generate")
    def generate_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        format = params["format"]
        plugin_uid = params["plugin"]
        
        if format == "wiki": 
            if not plugin_uid:
                util.cti_plugin_print_error("Wrong UID|alias.")
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            
            result = generate.generate_wiki(plugin_uid)
            
            for line in result:
                print line
        elif format == "man":
            # Man
            if not plugin_uid:
                util.cti_plugin_print_error("Wrong UID|alias.")
                return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            
            result = generate.generate_wiki(plugin_uid)
            f = generate.generate_man(result)
            os.system("man " + f.name)
            f.close()
        else:
            util.cti_plugin_print_error("Unknown format")
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
#---------------------------------------------------------------------------

    @hapi_command("googlecode")
    def googlecode_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        category = params["category"]
        google_code_doc.json_errors_all_plugins()
        google_code_doc.json_get_all_missing_long_desc()
        google_code_doc.print_all_doc_by_category(self, category)

#---------------------------------------------------------------------------

    def check_passwd(self):
        """Redefining check_passwd to avoid rights problem when making the documentation"""
        return True
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = DocPlugin()
    exit(p.main(sys.argv))
