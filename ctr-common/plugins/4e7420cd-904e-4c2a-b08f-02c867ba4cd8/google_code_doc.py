#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit


#!/usr/bin/python

import cti, ctr

import json, os, sys
from cti_hapi import database_manager, database, util_uid, alias, plugin

def get_ctr_info_name(path):
    ctr_info = open(os.path.join(path, cti.cti_plugin_config_get_value(cti.DATA_INFO_FILENAME)), 'r')
    
    pluginname = ctr_info.readline()
    while(not (cti.PLUGIN_INFO_NAME + ' = ' in pluginname or cti.PLUGIN_INFO_NAME + '=' in pluginname)):
        pluginname = ctr_info.readline()
    pluginname = pluginname.split('=')[1]
    pluginname = pluginname.rstrip('\n')
    pluginname = pluginname.strip()
    return pluginname

def open_json(path):
    try:
        return json.load(open(path,'r'))
    except ValueError, e:
        print >> sys.stderr, path + "\n" + format(e)


def print_ctr_input_default(path):
    ctr_input = open_json(os.path.join(path, cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_INPUT_FILENAME)))
    
    pluginname = get_ctr_info_name(path)
    
    print pluginname.title() + '\n\n'
    
    for cmd, cmddict in ctr_input.items():
        print '\t' + cmd.title() + '\n\n' + \
              '\t' + ctr_input[cmd]['attributes'].get(cti.META_ATTRIBUTE_DESC, 'DESC ERROR') + '\n\n' + \
              'Description: \n\t' + \
              ctr_input[cmd]['attributes'].get(cti.META_ATTRIBUTE_LONG_DESC, 'LONG_DESC ERROR') + '\n\n' + \
              'Usage:'
              
        cmdstring ='$ cti ' + pluginname + ' ' + cmd + ' '
        
        for valdict in cmddict['params']:
            opt = valdict.get('optional', False)
            cmd
            if opt:
                cmdstring += '['
            cmdstring +=  valdict.get('name', 'NAME ERROR')
            if opt:
                cmdstring +=  ']'
            cmdstring +=  ' '
            
        print '\t' + cmdstring + '\n\n' + \
              'Parameters:'
              
        for valdict in cmddict['params']:
            print '\t' + valdict.get(cti.META_ATTRIBUTE_NAME, 'NAME ERROR') + ': ' + valdict.get(cti.META_ATTRIBUTE_DESC, 'DESC ERROR') + \
                  '\n\t\t' + valdict.get(cti.META_ATTRIBUTE_LONG_DESC, 'LONG_DESC ERROR')
                  
        print '\n\n'


def find_and_print_plugin(plugin_name, root_folder):
    for directory in os.listdir(root_folder):
        current = os.path.join(root_folder, directory)
        if(os.path.isdir(current) and get_ctr_info_name(current) == plugin_name):
            print_ctr_input_default(current)
            return True
    return False


def print_all_doc_by_category(self, category, wiki=True):
    plugin_uid = self.plugin_uid
    db = database.Database()
    result = database_manager.search({},
                                    db,
                                    "plugin",
                                    [
                                        "plugin.alias",
                                        "plugin.category_uid"
                                    ]
                                 )
    plugins_by_category = {}
    
    for r in result:
        plugin_alias = str(r[0])
        category_plugin = str(r[1])

        if util_uid.is_valid_uid(category_plugin):
            category_plugin = alias.get_data_alias(util_uid.CTI_UID(category_plugin, cti.CTR_ENTRY_PLUGIN))
    
        if not plugins_by_category.has_key(category_plugin):
            plugins_by_category[category_plugin] = []
        plugins_by_category[category_plugin].append(plugin_alias)
    
    root_folder = ctr.ctr_plugin_get_common_plugin_dir()
    count = 0
    fail = []

    plugins_by_category[category].sort()
    print "################" + category.title() + '################\n\n'
    
    for p in plugins_by_category[category]:
        ret=False
        if(wiki):
            json = \
            {
                "generate":
                {
                    "attributes" : {cti.META_ATTRIBUTE_NAME : "generate"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "plugin",cti.META_ATTRIBUTE_VALUE : p},
                        {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : "wiki"}
                    ]
                }
            }
            
            try:
                output = plugin.execute_plugin_by_file(plugin_uid, json, self.username, self.password)
            except :
                print sys.exc_info()[1] 
                return cti.CTI_PLUGIN_ERROR_UNEXPECTED

            if(output):
                ret = True
                #Converting mediawiki to google wiki syntax
                output = output.replace("<pre>", "{{{")
                output = output.replace("</pre>", "}}}")
                output = output.replace("'''", "*")

                print output
        else:
            ret = find_and_print_plugin(p, root_folder)
        if ret:
            count+=1
        else:
            fail.append(p)
                
        print '\n\n\n'
        
    print 'Total processed: '+ str(count)
    
    print 'Failed plugins: ' + str(len(fail))
    for p in fail:
        print '\t'+p
        
def json_errors_all_plugins():
    root_folder = ctr.ctr_plugin_get_common_plugin_dir()
    
    for directory in os.listdir(root_folder):
        current = os.path.join(root_folder, directory)
        if(os.path.isdir(current)):
            open_json(os.path.join(current, cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_INPUT_FILENAME)))
            open_json(os.path.join(current, cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_OUTPUT_FILENAME)))
                      
def json_get_missing_long_desc(path, include_params=True, include_unknown=False):
    json_file = open_json(path)
    found = False
    
    ignorelist = []
    
    basename = os.path.basename(path)
    if 'input' in basename:
        basename = 'input'
    elif 'output' in basename:
        basename = 'output'
        
    dirname = os.path.basename(os.path.dirname(path))
    
    if dirname in ignorelist:
        return found
    
    for command, contents in json_file.items():
        if not cti.META_ATTRIBUTE_LONG_DESC in contents['attributes']:
            found = True
            print dirname + " - " + basename + ": " + "Command "+command+" has no long desc!"
        elif include_unknown:
            if "UNKNOWN" in contents['attributes']['long_desc']:
                found = True
                print dirname + " - " + basename + ": " + "Command "+command+" has UNKNOWN long desc!"
        if include_params:
            for param in contents['params']:
                if not cti.META_ATTRIBUTE_LONG_DESC in param:
                    found = True
                    print "\t" + dirname + " - " + basename + ": " + "Param "+param['name']+" of command "+command+" has no long desc!"
                elif include_unknown:
                    if "UNKNOWN" in param['long_desc']:
                        found = True
                        print "\t" + dirname + " - " + basename + ": " + "Param "+param['name']+" of command "+command+" has UNKNOWN long desc!"
    return found

def json_get_all_missing_long_desc(include_params=True, include_unknown=False):
    root_folder = ctr.ctr_plugin_get_common_plugin_dir()
    
    for directory in os.listdir(root_folder):
        current = os.path.join(root_folder, directory)
        if(os.path.isdir(current)):
            
            input_found = json_get_missing_long_desc(os.path.join(current, cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_INPUT_FILENAME)), include_params)
            output_found = json_get_missing_long_desc(os.path.join(current, cti.cti_plugin_config_get_value(cti.PLUGIN_DEFAULT_OUTPUT_FILENAME)), include_params)
            if input_found or output_found:
                print ""

