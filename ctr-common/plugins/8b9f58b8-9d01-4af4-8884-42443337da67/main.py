#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import util_uid, cti_json, util
from cti_hapi.main import HapiPlugin, hapi_command

import util as util_rm

import sys

class RmPlugin(HapiPlugin):
    @hapi_command("repository")
    def repository_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
       
        repository = params["repository"]
        remove_dependencies = params["remove_dependencies"]
        force = int(params["force"])
        format_o = params['format']
        rm_files = params['rm_files']
        
        if format_o not in ['txt', 'json']:
            util.cti_plugin_print_error("Unknown format: {0}".format(format_o))
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        
        (result, json_ret) = util_rm.cti_rm_repository(repository, force, rm_files, format_o, remove_dependencies)
        if result == 0:
            if format_o == 'txt':
                print "Done!\n"
            elif format_o == 'json':
                json_ret['output'].append('Done.')
                print cti_json.cti_json_encode(json_ret)
        else:
            util.cti_plugin_print_error("Error while removing the repository.")
            return result
        return 0

#---------------------------------------------------------------------------

    @hapi_command("entry")
    def entry_cmd(self, params):
        """ Description
        
        Args:
            self: class of the plugin
            params: working parameters
        
        Returns:
          Nothing
        """
        
        force = int(params["force"])
        format_o = params['format']
        
        entry_uid = util_uid.CTI_UID(params["entry"])
        if not entry_uid:
            entry_uid = util_uid.CTI_UID(params["entry"], cti.CTR_ENTRY_PLUGIN)
        if not entry_uid:
            util.cti_plugin_print_error("Wrong entry UID")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
            
        if format_o not in ['txt', 'json']:
            util.cti_plugin_print_error("Unknown format: {0}\n".format(format_o))
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
       
        # Delete the entry
        (result, json_ret) = util_rm.cti_rm_entry(entry_uid, force, format_o)
        if result == 0:
            if format_o == 'txt':
                print "Done!\n"
            elif format_o == 'json':
                json_ret['output'].append('Done.')
                print cti_json.cti_json_encode(json_ret)
        else:
            util.cti_plugin_print_error("Error while removing the entry.")
            return result
        return 0
    
#---------------------------------------------------------------------------

# main function
if __name__ == "__main__":
    p = RmPlugin()
    exit(p.main(sys.argv))
