#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

from cti_hapi import util_uid, database, database_manager, alias, entry, util

import os, shutil

def cti_rm_repository(repository_uid, force, rm_files, format, remove_dependencies):
    """ Remove a repository
    
    Args:
        repository_uid: The repository UID. If empty, take the current one
        force: 1: does not ask any question
        rm_files: True: remove the files from the hard drive, False otherwhise
    """
    
    answer = ""
    if not force:
        if format == "json":
            util.cti_plugin_print_error("You cannot use non-force mode with json format.\n\nAborting removal of repository.")
            return (cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS, {})
        answer = raw_input("Do you want to delete the repository from CTI index (Y/N)?\n")
        answer = answer.lower()
    
    json_result={'output':[]}
    
    if answer == "y" or force:
        if not repository_uid:
            dot_ctr = cti.cti_plugin_config_get_value(cti.DOT_CTR)
            path_ctr = os.path.join(os.getcwd(), dot_ctr)
            repository_uid = ctr.ctr_plugin_global_index_file_get_uid_by_ctr(path_ctr)
            if not repository_uid:
                util.cti_plugin_print_error("Cannot find repository")
                return (cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS, {})
        else:
            path_ctr = ctr.ctr_plugin_global_index_file_get_ctr_by_uid(repository_uid)
        
        # Remove the repository alias
        if cti.cti_plugin_alias_repository_rm_value(repository_uid) == -1:
            if format == 'json':
                json_result['output'].append("No local repository alias found, repository is simply removed.")
            else:
                print "No local repository alias found, repository is simply removed.\n"
        
        # Remove the plugins 
        rep_tmp = ctr.ctr_plugin_get_local_plugin_dir(repository_uid)
        if os.path.isdir(rep_tmp):
            for uid in os.listdir(rep_tmp):
                if util_uid.is_valid_uid(uid):
                    cti_rm_entry(cti.CTI_UID(uid), True, format, remove_dependencies)
                
        # Remove the data 
        rep_tmp = ctr.ctr_plugin_get_local_data_dir(repository_uid)
        if os.path.isdir(rep_tmp):
            for uid in os.listdir(rep_tmp):
                if util_uid.is_valid_uid(uid):
                    cti_rm_entry(cti.CTI_UID(uid), True, format, remove_dependencies)
                        
        # Remove the repository entry
        (ret_code, json_ret) = cti_rm_entry(repository_uid, True, format, remove_dependencies)
        if ret_code != 0:
            if format == 'json':
                json_result['output'].append(json_ret)
                json_result['output'].append("Cannot remove the repository entry.")
            else:
                print "Cannot remove the repository entry."
        
        if path_ctr:
            # Remove from the global index file
            ctr.ctr_plugin_global_index_file_rm(path_ctr)
        
        if not os.path.isdir(path_ctr):
            if format == 'json':
                json_result['output'].append("Local repository not found on your hard drive.\n")
            else:
                print "Local repository not found on your hard drive.\n"
        else:
            # Remove from the hard drive
            if rm_files or force:
                shutil.rmtree(path_ctr)
            else:
                if format == 'json':
                    json_result['output'].append("The files are not removed from the hard drive.")
                else:
                    print "The files are not removed from the hard drive."
    return (0, json_result)

#---------------------------------------------------------------------------

def cti_rm_entry(entry_uid, force, format, remove_dependencies=True):
    """ Remove an entry
    
    Args:
        entry_uid: The entry UID
        force: 1: does not ask any question
    """
    
    answer = ""
    if not force:
        if format == "json":
            util.cti_plugin_print_error("You cannot use non-force mode with json format.\n\nAborting removal of repository.")
            return (cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS, {})
        answer = raw_input("Are you sure you want to delete this entry? (Y/N)?\n")
        answer = answer.lower()
    
    json_result = {'output':[]}
    
    if answer == 'y' or force:
        # Remove the entry
        db = database.Database()
        path = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN, entry_uid)
        if not path:
            # Data
            path = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_DATA, entry_uid)
            if not path:
                util.cti_plugin_print_error("Cannot find the entry.")
                return (cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS, {})
            
            if remove_dependencies:
                # Delete the references
                try:
                    (_, out_entry) = entry.load_data(entry_uid)
                    if out_entry.has_key("init"):
                        params_list = out_entry["init"].params
                        for p in params_list:
                            if params_list[p].has_key(cti.META_ATTRIBUTE_TYPE) and \
                                params_list[p][cti.META_ATTRIBUTE_TYPE] == cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
                                value = None
                                if params_list[p].has_key(cti.META_ATTRIBUTE_LIST) and \
                                    params_list[p][cti.META_ATTRIBUTE_LIST]:
                                        value = []
                                entry.update_entry_parameter(entry_uid, {params_list[p][cti.META_ATTRIBUTE_NAME] :{"value": value}})
                except:
                    # Warning: this exception is important because
                    # the clean plugin use rm to remove the corrupted entries
                    util.cti_plugin_print_error("The entry is corrupted, can't delete the references.")
                
            # Remove the entry from the index
            if database_manager.delete_from_index({'NAME':["entry_info","entry_uid"], 'TYPE':"=", 'VAL':str(entry_uid)}, db) is False:
                print "Error while removing the entry from the index."
        else:
            # Plugin
            # Remove the entries produced by the plugin
            tmp_entries = database_manager.search_uids({'NAME':["entry_info","plugin_uid"], 'TYPE':"=", 'VAL':str(entry_uid)}, db)
            tmp_entries = list(tmp_entries)
            for data in tmp_entries: 
                cti_rm_entry(data, 1, format)
                
            # Remove the plugin alias
            alias_plugin = alias.get_plugin_alias(entry_uid)
            cti.cti_plugin_alias_plugin_rm_value(entry_uid)
            
            # Remove the plugin from the plugin table
            database_manager.delete("plugin", {'NAME':["plugin_uid"], 'TYPE':"=", 'VAL':str(entry_uid)}, db)
            (_, output_default) = entry.load_defaults(entry_uid)
           
            # Remove the plugin table
            if output_default.has_key("init") and \
                    output_default["init"].attributes.has_key(cti.META_ATTRIBUTE_PRODUCED_DATA):
                if output_default["init"].attributes[cti.META_ATTRIBUTE_PRODUCED_DATA]:
                    if database_manager.drop_table(alias_plugin, db) is False:
                        print "Error while removing the table" + alias_plugin
                        
            # Remove the plugin from link_table
            database_manager.delete("link_table", {'NAME':["target"], 'TYPE':"=", 'VAL': alias_plugin}, db)
            database_manager.delete("link_table", {'NAME':["source"], 'TYPE':"=", 'VAL': alias_plugin}, db)
            
        
        shutil.rmtree(path)
    
    return (0, json_result)

