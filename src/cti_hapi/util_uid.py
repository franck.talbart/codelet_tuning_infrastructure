#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

""" UID modules provides handful functions for managing the uids.
"""

import cti, ctr

import alias

def is_valid_uid(s):
    """ Returns true if UID is valid.
        Args:
            s: the UID
        Returns:
            true if UID is valid
    """
    return cti.cti_plugin_is_UID(str(s))

#------------------------------------------------------------------------

def uid_visualization(uid, data_type, alias_f=""):
    """
    Returns user friendly format for a given UID
    
    Args:
        uid the uid to visualize
        data_type: data or plugin
        alias_f: the alias if you want to improve the performances
    Return:
        The user friendly name
    """
    
    result = None
    if alias_f == "":
        if data_type == cti.CTR_ENTRY_DATA:
            result = alias.get_data_alias(uid)
        elif data_type == cti.CTR_ENTRY_PLUGIN:
            result = cti.cti_plugin_alias_plugin_get_key(uid)
        else:
            print "Type error"
    else:
        result = alias_f
    if result == None:
        result = str(uid)
    return result

#------------------------------------------------------------------------

def CTI_UID(str_uid, data_type=cti.CTR_ENTRY_DATA):
    """
    Converts a string to the CTI_UID type
    
    Args:
        str_uid: the string
        data_type: data, plugin or repository
    
    Returns:
        The CTI_UID
    """
    
    str_uid = str(str_uid)
    #------------------------------------------------------------------------
    def get_data_uid(str_uid):
        uid = alias.get_data_uid(str_uid)
        if uid:
            return uid.pop()
        else:
            return None
    #------------------------------------------------------------------------
        
    if not str_uid:
        return None
    
    if is_valid_uid(str_uid):
        uid = cti.CTI_UID(str_uid)
        if data_type == cti.CTR_ENTRY_DATA:
            if ctr.ctr_plugin_get_data_path_by_uid(uid) is not None:
                return uid
        elif data_type == cti.CTR_ENTRY_PLUGIN:
            if ctr.ctr_plugin_get_plugin_path_by_uid(uid) is not None:
                return uid
        elif data_type == cti.CTR_ENTRY_REPOSITORY:
            if ctr.ctr_plugin_global_index_file_get_ctr_by_uid(uid):
                return uid
        return None
    
    alias_fct = {
                 cti.CTR_ENTRY_DATA: get_data_uid,
                 cti.CTR_ENTRY_PLUGIN: alias.get_plugin_uid,
                 cti.CTR_ENTRY_REPOSITORY: alias.get_repository_uid,
                }
    
    uid = alias_fct[data_type](str_uid)
    if uid:
        return uid
    
    return None

#------------------------------------------------------------------------
