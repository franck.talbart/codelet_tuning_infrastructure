#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

""" Alias module provides facilities to manage 
    the aliases
"""

import cti

import database_manager, database

#------------------------------------------------------------------------

def get_data_alias(uid):
    """ get an alias from a data UID. It uses the database so
        this function is faster that core's library
        Args:
            uid: the UID
        Returns:
            the alias, None if there is no alias
    """
    if str(uid) == "":
        print "Error: no UID given in get_data_alias."
        exit(1)
        
    db = database.Database()
    result = database_manager.search({'NAME':["entry_uid"], 'TYPE':"=", 'VAL':uid},
                                    db,
                                    fields=["alias"])
    
    alias = None
    for r in result:
        alias = r[0]
    return alias

#------------------------------------------------------------------------

def get_plugin_alias(uid):
    """ get an alias from a plugin UID.
        Args:
            uid: the UID
        Returns:
            the alias, None if there is no alias
    """
    if str(uid) == "":
        print "Error: no UID given in get_plugin_alias."
        exit(1)
    
    alias = cti.cti_plugin_alias_plugin_get_key(uid)
    return alias

#------------------------------------------------------------------------

def get_data_uid(alias):
    """ get an UID from an alias.
        Args:
            alias: the alias
        Returns:
            the UID, None if it fails
    """

    alias = format_alias(alias)

    if alias == "":
        print "Error: no alias given in get_data_uid."
        exit(1)
    
    db = database.Database()
    r = database_manager.search_uids({'NAME':["alias"], 'TYPE':"LIKE", 'VAL':alias},
                                     db)
    result = set()
    for uid in r:
        uid = cti.CTI_UID(str(uid))
        result.add(uid)
        
    return result

#------------------------------------------------------------------------

def get_plugin_uid(alias):
    """ get an UID from a plugin.
        Args:
            alias: the alias
        Returns:
            the UID, None if it fails
    """

    alias = format_alias(alias)

    uid = cti.cti_plugin_alias_plugin_get_value(alias)
    if uid is None:
        return None
    return cti.CTI_UID(str(uid))

#------------------------------------------------------------------------

def get_repository_uid(alias):
    """ get an UID from a plugin.
        Args:
            alias: the alias
        Returns:
            the UID, None if it fails
    """
    
    alias = format_alias(alias)
    
    uid = cti.cti_plugin_alias_repository_get_value(alias)
    if uid is None:
        return None
    return cti.CTI_UID(str(uid))

#------------------------------------------------------------------------

def set_data_alias(uid, alias):
    """ Create an alias for data.

    Args:
      uid: CTI_UID
      alias: an alias for given uid
      
    Return 1 if it succeeds, 0 otherwise
    """
    alias = format_alias(alias)
    
    if get_data_uid(alias):
        return 0
    
    if get_plugin_uid(alias) is not None:
        return 0
        
    cti.cti_plugin_alias_data_set_value(alias, uid)
    db = database.Database()
    if database_manager.update("entry_info",
                               {"alias": alias},
                               {
                                'NAME':["entry_uid"],
                                'TYPE':"=",
                                'VAL':str(uid)
                               },
                               db) is False:
        return 0

    return 1

#------------------------------------------------------------------------

def set_plugin_alias(uid, alias, dir = None):
    """ Create an alias for data.

    Args:
      uid: CTI_UID
      alias: an alias for given uid
    """
    alias = format_alias(alias)
    
    if get_data_uid(alias):
        return 0
    
    if get_plugin_uid(alias) is not None:
        return 0
    
    cti.cti_plugin_alias_plugin_rm_value(uid)
    cti.cti_plugin_alias_plugin_set_value(alias, uid, dir)
    
    return 1

#------------------------------------------------------------------------

def set_repository_alias(uid, alias):
    """ Create an alias for a repository.

    Args:
      uid: CTI_UID
      alias: an alias for given uid
    """
    alias = format_alias(alias)
    
    if alias in ["common", "temp", "all"]:
        return 0
    
    if get_repository_uid(alias) is not None:
        return 0
    
    cti.cti_plugin_alias_repository_rm_value(uid)
    cti.cti_plugin_alias_repository_set_value(alias, uid)
    
    return 1

#------------------------------------------------------------------------

def format_alias(alias):
    return str(alias).strip().lower().replace(" ", "_")

#------------------------------------------------------------------------
