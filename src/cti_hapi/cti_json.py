#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

import entry

import json

class CTIEncoder(json.JSONEncoder):
    """ A class to encode some special objects into the json format
    """
    def default(self, obj):
        """ The function called when encoding a json object through this class
        """
        
        if isinstance(obj, cti.CTI_UID):
            return str(obj)
        elif isinstance(obj, (entry.Commands, entry.odict)):
            return dict([ (key, obj.__dict__['_data'][key]) for key in obj.__dict__['_data'] ])
        elif isinstance(obj, (entry.CmdNode)):
            return {
                'params': [obj.params[i] for i in obj.params], 
                'attributes' : obj.attributes
            }
        
        #Regular objects are to be encoded by the basic Json encoder.
        return super(CTIEncoder, self).default(obj)

#------------------------------------------------------------------------

def cti_json_encode(obj):
    """ Encodes the given object in json and returns the encoded json string.
    This functions does the same a json.dumps(obj) but calls it with the CTIEncoder class, ensuring CTI-specific
        objects are encoded without generating errors.
    """
    return json.dumps(obj, cls=CTIEncoder, allow_nan=False)

#------------------------------------------------------------------------
