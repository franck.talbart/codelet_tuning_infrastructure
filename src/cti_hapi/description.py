#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

import util, entry, cti_json, types, os, getpass, copy

def enter_data(param):
    """
    Asks user to enter data.
    Args:
    param: The parameters
    
    Returns:
    The parameters
    """
    
    # help user with the names of plugins
    # which produce the required DATA_UID
    pb = None
    if ((cti.META_ATTRIBUTE_TYPE in param) and
            (param[cti.META_ATTRIBUTE_TYPE] == cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID) and
            (cti.META_ATTRIBUTE_PRODUCED_BY in param)):
        pb = param[cti.META_ATTRIBUTE_PRODUCED_BY]
        text = "(produced by"
        s = cti.CTI_UID(str(pb))
        a = cti.cti_plugin_alias_plugin_get_key(s)
        if a:
            text = "%s %s" % (text, a)
        else:
            text = "%s %s" % (text, s)
        text = "%s)" % text
        print "Please enter %s %s:" % (param[cti.META_ATTRIBUTE_DESC], text)
    else:
        print "Please enter %s: " % (param[cti.META_ATTRIBUTE_DESC])
    
    # enter data
    try:
        
        if param.has_key(cti.META_ATTRIBUTE_PASSWORD) and param[cti.META_ATTRIBUTE_PASSWORD]:
            s  = getpass.getpass('')
            param[cti.META_ATTRIBUTE_VALUE] = s
        else:
            s = raw_input("")
            wrong_value = True
            while wrong_value:
                try:
                    if ((cti.META_ATTRIBUTE_LIST in param) and
                      str(param[cti.META_ATTRIBUTE_LIST]).lower() == cti.META_ATTRIBUTE_TRUE):
                        if not param.has_key(cti.META_ATTRIBUTE_VALUE):
                            param[cti.META_ATTRIBUTE_VALUE] = []
                        if len(s) > 0:
                            param[cti.META_ATTRIBUTE_VALUE].append(types.from_string(s, 
                                                                                    param[cti.META_ATTRIBUTE_TYPE], 
                                                                                    False, produced_by=pb))
                        wrong_value = False
                        while len(s) > 0:
                            s = raw_input("")
                            if len(s) > 0:
                                # add conversion
                                wrong_value_2 = True
                                while wrong_value_2:
                                    try:
                                        param[cti.META_ATTRIBUTE_VALUE].append(types.from_string(s, 
                                                                                                 param[cti.META_ATTRIBUTE_TYPE], 
                                                                                                 False, produced_by=pb))
                                        wrong_value_2 = False
                                    except ValueError:
                                        wrong_value_2 = True
                                        s = raw_input("Wrong value, try again: ")
                    else:
                        matrix_types = None
                        if(param[cti.META_ATTRIBUTE_TYPE] == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX):
                            matrix_types = dict([(param[cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES][i], param[cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES][i]) for i in range(len(param[cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES]))])
                            
                        param[cti.META_ATTRIBUTE_VALUE] = types.from_string(s, param[cti.META_ATTRIBUTE_TYPE],
                                                                            False,
                                                                            matrix_types,
                                                                            produced_by=pb)
                        wrong_value = False
                except ValueError:
                    wrong_value = True
                    s = raw_input("Wrong value, try again: ")
    
    except KeyboardInterrupt:
        print "The initialization is not finished. No data being stored."
        exit(cti.CTI_PLUGIN_ERROR_USER_INTERRUPT)
        
    return param

#------------------------------------------------------------------------

def description_write(command, work_params, accum_filter=None, param_filter={}, accum_process=None, param_process={}):
    """ Ask the user to write the values
    Args:
        command: the command
        work_params: the working parameters
        accum_filter: callback
                func is a accum_filter, for instance:
                foo_filter(name, value) returns bool
        param_filter = the dictionary of parameters to send to the accum_filter
        accum_process: callback
                func is a accum_process, for instance:
                foo_filter(name, value) returns value
        param_process = the dictionary of parameters to send to the accum_process
    Returns:
        work_params: updated work_params with new values
    """
    
    wpp = work_params[command].params
    for p in wpp:
        if not cti.META_ATTRIBUTE_VALUE in wpp[p]:
            if ((cti.META_ATTRIBUTE_OPTIONAL in wpp[p] and wpp[p][cti.META_ATTRIBUTE_OPTIONAL] == False)
              or (not cti.META_ATTRIBUTE_OPTIONAL in wpp[p])):
                util.cti_plugin_print_error("Not enough parameters. Try 'help' command. Missing parameter: %s" % (p))
                exit (cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)

    
    # iterate params and trigger input
    params = work_params[command].params
    for param_name in work_params[command].params:
        if cti.META_ATTRIBUTE_VALUE in params[param_name]:
            if accum_filter is not None:
                if accum_filter(param_name, params[param_name][cti.META_ATTRIBUTE_VALUE], param_filter):
                    print "Error with accum filter."
                    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
            
            if accum_process is not None:
                params[param_name][cti.META_ATTRIBUTE_VALUE] = accum_process(
                                    param_name, 
                                    params[param_name][cti.META_ATTRIBUTE_VALUE], 
                                    param_process
                                )
            pass
        
        else:
            params[param_name] = enter_data(params[param_name])
            if accum_filter is not None:
                while accum_filter(param_name, params[param_name][cti.META_ATTRIBUTE_VALUE], param_filter):
                    params[param_name] = enter_data(params[param_name])
            
            if accum_process is not None:
                params[param_name][cti.META_ATTRIBUTE_VALUE] = accum_process(
                                    param_name, 
                                    params[param_name][cti.META_ATTRIBUTE_VALUE], 
                                    param_process
                                )
    
    return work_params

#------------------------------------------------------------------------

def update_entry(self, params, accum_filter=None, 
                 param_filter={}, accum_process=None, param_process={}):
    """ Update the existing entry
    Args:
        params: the parameters
    """
    
    command = self.command
    data_uid = params["entry"]
    
    format_out = "txt"
    if params.has_key("format"):
        format_out = params["format"]
        
    if format_out not in ["txt", "json"]:
        util.cti_plugin_print_error("Unknown format.")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        
    try:
        (_, old_output) = entry.load_data(data_uid)
    except:
        util.cti_plugin_print_error("Can't load the entry.")
        exit(cti.CTI_PLUGIN_ERROR_CRITICAL_IO)
        

    for param in self.work_params[command].params:
        if cti.META_ATTRIBUTE_VALUE not in self.work_params[command].params[param]:
            if param in old_output["init"].params and cti.META_ATTRIBUTE_VALUE in old_output["init"].params[param]:
                # No value: the previous one is kept
                self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE] = old_output["init"].params[param][cti.META_ATTRIBUTE_VALUE]
        else:
            # New value updated
            if accum_filter is not None:
                if accum_filter(param,
                                self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE],
                                param_filter):
                    util.cti_plugin_print_error("Error with accum filter.")
                    exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                    
            if accum_process is not None:
                self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE] = accum_process(
                                param, 
                                self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE], 
                                param_process
                            )

            type_value = self.work_params[command].params[param][cti.META_ATTRIBUTE_TYPE]
            is_list_input = ((cti.META_ATTRIBUTE_LIST in self.work_params[command].params[param]) and\
                 str(self.work_params[command].params[param][cti.META_ATTRIBUTE_LIST]).lower() == cti.META_ATTRIBUTE_TRUE)
            is_list_output = False
            if param in self.output_params["init"].params:
                is_list_output = ((cti.META_ATTRIBUTE_LIST in self.output_params["init"].params[param]) and\
                                       str(self.output_params["init"].params[param][cti.META_ATTRIBUTE_LIST]).lower() == cti.META_ATTRIBUTE_TRUE)
            if type_value == cti.META_CONTENT_ATTRIBUTE_TYPE_FILE:
                file_update_list_to_remove = []
                file_update_list_to_add = []
                if not is_list_input:
                    file_update_list_to_add = [self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE]]
                    if param in old_output["init"].params and cti.META_ATTRIBUTE_VALUE in old_output["init"].params[param]:
                        file_update_list_to_remove = [old_output["init"].params[param][cti.META_ATTRIBUTE_VALUE]]
                        self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE] = []
                else:
                    # if value begins with "-", then we remove it from the old list
                    # if value begins with anything else, we add it to the list
                    # if no value, nothing is done
                    current_list = copy.deepcopy(old_output["init"].params[param][cti.META_ATTRIBUTE_VALUE])
                    for val in self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE]:
                        if val is not None and val[0] == "-": # remove value from list
                            if val[1:] in current_list:
                                current_list.remove(val[1:])
                                file_update_list_to_remove.append(val[1:])
                            else:
                                util.cti_plugin_print_warning("%s not found." % val[1:])
                        else:
                            file_update_list_to_add.append(val)
                    self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE] = current_list
                # Removing the previous files
                for f in file_update_list_to_remove:
                    if f is not None:
                        entry.rm_file_from_entry(data_uid, f)
                # Adding the new files
                self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE] = util.value_as_list(self.work_params[command].params[param])
                for f in file_update_list_to_add:
                    if f is not None:
                        try:
                            if os.path.isfile(f):
                                    if len(self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE]) > 0 and not is_list_output:
                                        util.fatal("%s is not a list of files. Hence, only one file is expected." % (param), cti.CTI_ERROR_UNEXPECTED)
                                        exit(1)

                                    else:
                                        tmp_filename = entry.put_file_in_entry(data_uid, f, safe=False)
                                        if not tmp_filename in self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE]:
                                            self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE].append(tmp_filename)
                            elif os.path.isdir(f):
                                if not is_list_output:
                                        util.fatal("%s can't be a directory." % (param), cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                                        exit(1)

                                else:
                                    tmp_filenames = entry.put_dir_in_entry(data_uid, f)
                                    for t in tmp_filenames:
                                        if t not in self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE]:
                                            self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE].append(t)
                            else:
                                util.fatal("%s not found." % (f), cti.CTI_ERROR_UNEXPECTED)
                                exit(1)
                        except:
                            print "Error while adding the file %s" % (f)
                            exit(1)

                if not is_list_input:
                    if len(self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE]) > 0:
                        self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE] = \
                            self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE][0]
                    else:
                        self.work_params[command].params[param][cti.META_ATTRIBUTE_VALUE] = None

    # Updating the existing entry
    new_params = {}
    for def_param in self.output_params["init"].params.keys():
        if self.work_params[command].params.has_key(def_param) and \
                self.work_params[command].params[def_param].has_key(cti.META_ATTRIBUTE_VALUE):
            new_params[def_param] = {"value": self.work_params[command].params[def_param][cti.META_ATTRIBUTE_VALUE]}
            
    if entry.update_entry_parameter(
                                      data_uid,
                                      new_params
                                      ) == 0:
        util.cti_plugin_print_error("Error while updating the entry.")
        exit(cti.CTI_PLUGIN_ERROR_CRITICAL_IO)
    
    # Output
    if format_out == "txt":
        print "Done! The entry has been updated. DATA_CTI_UID=" + str(data_uid)
    elif format_out == "json":
        result = {
            'type': cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID,
            'new_uid': data_uid,
            'output': ""
        }
        print cti_json.cti_json_encode(result)
        
    return 0

#------------------------------------------------------------------------
