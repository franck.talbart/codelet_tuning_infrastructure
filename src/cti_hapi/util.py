#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

# TODO: Consider if logging should be extracted into a separate module or not.

""" Util modules provides handful functions for:
    1. logging
    2. error handling
    3. json output
    4. threads
"""

import cti

import cti_exception

import os, sys, threading

class CtiThread(threading.Thread):
    
    def __init__(self, target=None, args=[]):
        super(CtiThread, self).__init__()
        self.target = target
        self.args = args
        self.stoprequest = threading.Event()
    
    def run(self):
        self.args.insert(0, self)
        self.target(*(self.args))
        
    def stop_requested(self):
        return self.stoprequest.isSet()
    
    def join(self, timeout=None):
        self.stoprequest.set()
        super(CtiThread, self).join(timeout)

#------------------------------------------------------------------------
    
def fatal(msg, cti_error):
    """ Display a fatal error
        Args:
            msg: the message
            cti_error: the error code
    """
    cti_plugin_print_error(msg)
    raise Exception(cti_error)

#------------------------------------------------------------------------

def hapi_log(msg):
    """ Logs message in CTI log file.
    Args:
      msg: a message
    """
    if len(sys.argv) > 2:
        try:
            puid = os.path.basename(sys.argv[2])
        except:
            puid = None
        if puid:
            if cti.cti_plugin_is_UID(puid):
                msg = "[CALLED FROM PLUGIN]:%s %s" % (puid, msg)
    cti.cti_plugin_log("[CTI HAPI]: %s" % str(msg))

#------------------------------------------------------------------------

def cti_plugin_print_error(msg):
    """Prints plugin error message.
    Args:
      msg: an error message
    """
    print >> sys.stderr, "\033[31m"
    print >> sys.stderr, cti.CTI_ERROR_HEADER + ": " + msg
    print >> sys.stderr, "\033[0m"

#------------------------------------------------------------------------

def cti_plugin_print_warning(msg):
    """Prints plugin warning message.
    Args:
      msg: a warning message
    """
    print >> sys.stderr, "\033[33m"
    print >> sys.stderr, cti.CTI_WARNING_HEADER + ": " + msg
    print >> sys.stderr, "\033[0m"

#------------------------------------------------------------------------

def hapi_error(msg):
    """Prints HAPI error message error message.

    Args:
      msg: error message
    """
    hapi_log("%s: %s" % (cti.HAPI_ERROR_HEADER, msg))

#------------------------------------------------------------------------

def hapi_fail(msg):
    """ Prints failure message and terminates.
    Args:
          msg: error message
    """
    print "\033[31m"
    print "CTI HAPI FAILURE: %s" % msg
    print "\033[0m"
    raise cti_exception.Hapi_fail()

#------------------------------------------------------------------------

def value_as_list(param_entry, default_value = cti.META_ATTRIBUTE_VALUE):
    """
    given a plugin param entry, always
    return the value as a list.
    (when list is false, it packs the value inside
    a list)
    Args:
        param_entry: the plugin parameter
    Returns:
        the list
    """
    if isinstance(param_entry[default_value], list):
        return param_entry[default_value]
    else:
        return [param_entry[default_value]]

#------------------------------------------------------------------------

def boolean_as_int(bool_value):
    """
    Given a boolean/None, returns an int/None for SQLite.
    Args:
        bool_value: the boolean
    Returns:
        an integer or None
    """
    if bool_value is None:
        return None
    else:
        return int(bool_value)

#------------------------------------------------------------------------

def column_name_replace(cname):
    """
    Given a string, returns this string formatted into a SQLite column compatible string.
    Args:
        cname: the string to format
    Returns:
        A string
    """
    replace_list = [
        (' ', '_'),
        (',',''),
        ('[','('),
        (']',')'),
        ('-', '_'),
        ('.', ''),
        (':', '_'),
        ('__','_')
    ]
    
    for (orig_char, repl_char) in replace_list:
        cname = cname.replace(orig_char, repl_char)
    return cname

#------------------------------------------------------------------------

def cti_pretty_table(table_contents, column_names=None, column_cti_types=None):
    """
    return a table containing CTI data.
    Args:
        table_contents: The contents of the table. Each list in it represents a column.
            All of these columns must be filled with the same amount of lines.
        column_names: a list of names, one for each of the columns in table_contents. 
            If left to "None", the first row of table_contents will be used instead.
        column_cti_types: a list of types, one of each of the columns in table_contents.
            If left to "None", all data inside table_contents will be considered as strings.
    """
    
    def generate_lines_with_wrap(data_list,size):
        def empty_data_list(lst):
            for i in lst:
                i = str(i)
                if len(i):
                    return False
            return True
        
        def get_wrap(contents,size, first=False):
            if len(contents) > size:
                return '{0}'.format(contents[:size])
            if first:
                return ('{0:^%d}'%size).format(contents)
            return ('{0:<%d}'%size).format(contents)
        
        first=True
        gen_lines = []
        while not empty_data_list(data_list):
            columns_chain = []
            for ind in range(len(data_list)):
                columns_chain.append(get_wrap(str(data_list[ind]),size, first))
                data_list[ind] = str(data_list[ind])[size:]
            gen_lines.append(' | '.join(columns_chain))
            first = False
        return gen_lines
    
    try:
        term_width = os.popen('stty size', 'r').read()
        display_max = int(term_width.split(' ')[1])
    except:
        display_max = 100
    
    separator_length = 3
    
    col_min_width = 10+separator_length
    #int/int division auto-converts into an integer. Left on purpose.
    max_columns_term = display_max/col_min_width
    #Column number, without the first column
    c_number = len(table_contents)-1
    
    #Calculating columns
    columns = []
    if c_number+1 < max_columns_term:
        columns.append(range(c_number+1))
    else:
        left = c_number
        curr = 0
        while left+1 > max_columns_term:
            columns.append(range(curr,curr+max_columns_term-1))
            curr += max_columns_term
            left = left - max_columns_term
    
    #Taking the first row for column names if it was not defined.
    if column_names == None:
        for c_index in range(len(table_contents)):
            column_names.append(table_contents[c_index][0])
            table_contents[c_index] = table_contents[c_index][1:]
    
    display_lines = []
    
    for sc_index in range(len(columns)):
        screen_columns = columns[sc_index]
        text_width = display_max/(len(screen_columns))-3
        display_lines += generate_lines_with_wrap([column_names[col] for col in screen_columns], text_width)
        display_lines.append('=|='.join(['='*text_width for unused_var in screen_columns]))
        for line_index in range(len(table_contents[0])):
            display_lines += generate_lines_with_wrap([table_contents[col][line_index] for col in screen_columns], text_width)
            display_lines.append('-+-'.join(['-'*text_width for unused_var in screen_columns]))
        display_lines[-1] = display_lines[-1].replace('+','-')
        if sc_index != len(columns) -1:
            display_lines.append('\n')
        
    return '\n'.join(display_lines) + '\n\n'

#------------------------------------------------------------------------

def rewrite_output_line(new_line):
    """
    return a table containing CTI data.
    Args:
        new_line: the line to output after erasing the previous one
    """
    
    sys.stdout.write('\r')
    sys.stdout.flush()
    sys.stdout.write(new_line)
    sys.stdout.flush()
