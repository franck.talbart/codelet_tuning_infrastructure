#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

import default, plugin, util, types, entry, user

import sys, logging

#------------------------------------------------------------------------

def list_mandatory_args(params):
    """ Count the number of mandatory parameters
        Args:
            params: the set of parameters
        Returns:
            the number of mandatory parameters
    """
    list_mandatory = []
    for p in params:
        if cti.META_ATTRIBUTE_OPTIONAL not in params[p]:
            list_mandatory.append(p)
    return list_mandatory

#------------------------------------------------------------------------

def parse_optional(arg):
    """
    parses an optional parameter
    Args:
        arg -- a parameter in the form --key=value
    Return:
        (key, value)
    """
    kv = arg.split("=",1)
    if len(kv) != 2:
        util.cti_plugin_print_error("missing value for <{0}>".format(arg))
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    key = kv[0][2:] # remove the -- in front of the key
    value = kv[1]
    
    #Removing enclosing quotes or doublequotes.
    if((value.startswith("'") and value.endswith("'")) or (value.startswith('"') and value.endswith('"'))):
        value = value[1:-1]
    
    return (key, value)

#------------------------------------------------------------------------

def parse_arguments(argv):
    """
    check that arguments are in the format <p1> <p2> --o1 --o2 ...
    and separate positional and optional ones
    Args:
        argv: the arguments
    
    Return:
        (positional_list, optional_list)
    """
    reached_optional_args = False
    positional_args = []
    optional_args = []
    for arg in argv:
        if arg.startswith("--"):
            # it's an optional argument
            reached_optional_args = True
            optional_args.append(parse_optional(arg))
        elif not reached_optional_args:
            #Removing enclosing quotes or doublequotes.
            if((arg.startswith("'") and arg.endswith("'")) or (arg.startswith('"') and arg.endswith('"'))):
                arg = arg[1:-1]
            positional_args.append(arg)
        else:
            
            util.cti_plugin_print_error("optional arguments should come after positional arguments: " + arg + str(argv))
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    return positional_args, optional_args

#------------------------------------------------------------------------

def auto_parser_cli(command, argv, work_params, complex_parser=None):
    
    wpp = work_params[command].params
    list_mandatory = list_mandatory_args(wpp)
    positional_args, long_args = parse_arguments(argv)
    
    #Verify that all positional args were given.
    non_positional_args_count = len(list_mandatory) - len(positional_args)
    if (non_positional_args_count > 0):
        #If not, check if they are present in the long args (--arg="value")
        list_long_args = [long_name for long_name, long_val in long_args]
        for left_mandatory_arg in list_mandatory[-non_positional_args_count:]:
            if left_mandatory_arg not in list_long_args:
                #If they are not present at all, then output an error message.
                util.cti_plugin_print_error("Not enough parameters. Missing: '"+ left_mandatory_arg +"'. Try 'help' command.")
                exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    def _parsep(pname, arg):
        produced_by=None
        if wpp[pname].has_key(cti.META_ATTRIBUTE_PRODUCED_BY):
            produced_by = wpp[pname][cti.META_ATTRIBUTE_PRODUCED_BY]
        ptype = wpp[pname][cti.META_ATTRIBUTE_TYPE]
        islist = cti.META_ATTRIBUTE_LIST in wpp[pname]
        try:
            # If the type is COMPLEX use the user provided complex parser (if available)
            if ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_COMPLEX and complex_parser:
                wpp[pname][cti.META_ATTRIBUTE_VALUE] = complex_parser(pname, arg)
            # Else use default conversion from string
            else:
                matrix_types = None
                if ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
                    matrix_types = dict([(wpp[pname][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES][i], wpp[pname][cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES][i]) for i in range(len(wpp[pname][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES]))])
                    
                wpp[pname][cti.META_ATTRIBUTE_VALUE] = types.from_string(arg, ptype, islist, matrix_types, produced_by)
        except ValueError:
            util.cti_plugin_print_error("Invalid argument <{0}> for param {1}.".format(arg, pname))
            exit(cti.CTI_ERROR_UID_NOT_FOUND)
    
    # read positional args
    for pname, arg in zip(wpp,positional_args):
        _parsep(pname, arg)
    #read optional arguments
    for pname, arg in long_args:
        if pname ==cti.META_ATTRIBUTE_REP_PRODUCE and \
            cti.META_ATTRIBUTE_PRODUCED_DATA in work_params[command].attributes and\
            work_params[command].attributes[cti.META_ATTRIBUTE_PRODUCED_DATA]:
            repos_prod = arg
            repos_prod_type = cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT
            if arg not in [cti.COMMON_REPOSITORY, cti.TEMP_REPOSITORY]:
                repos_prod_type = cti.META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID
                repos_prod = types.from_string(arg, repos_prod_type, False)
            wpp[pname] = {
                cti.META_ATTRIBUTE_NAME: pname,
                cti.META_ATTRIBUTE_VALUE: repos_prod,
                cti.META_ATTRIBUTE_TYPE: repos_prod_type
            }
        elif not (pname in wpp):
            util.cti_plugin_print_error("argument <{0}> not understood".format(pname))
            exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        else :
            _parsep(pname, arg)
    return work_params

#------------------------------------------------------------------------

def hapi_command(command_name):
    def wrapper(f):
        f.command_name = command_name
        f.command_parser = auto_parser_cli
        return f
    return wrapper

#------------------------------------------------------------------------

def easy_params(params):
    easy = {}
    for k, v in params.iteritems():
        if cti.META_ATTRIBUTE_VALUE in v:
                easy[k] = v[cti.META_ATTRIBUTE_VALUE]
        elif v.has_key(cti.META_ATTRIBUTE_LIST) and\
                v[cti.META_ATTRIBUTE_LIST]:
                    easy[k] = []
        else:
            ptype = v[cti.META_ATTRIBUTE_TYPE]
            if ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_COMPLEX]:
                easy[k] = {}
            else:
                easy[k] = None
    return easy

#------------------------------------------------------------------------
    
class HapiPlugin(default.DefaultCommands):
    
    def __init__(self):
        """Overwriting the __init__ method
        
        Adding a mechanism to catch all uncaugh exceptions
        """        
        def excepthook(type_e, value, traceback):
            logging.error("Uncaught exception", exc_info=(type_e, value, traceback))
            sys.exit(cti.CTI_PLUGIN_ERROR_EXCEPT)
        sys.excepthook = excepthook
        
    #------------------------------------------------------------------------
    
    def record_entry(self, command, output, data_entry=None, format_o="txt", alias_e=None):
        """ Records input and output inside entry.
        Args:
          command: plugin command
          output: simplified output dictionary
          data_entry: if not specified then function
                      creates a new entry and records there
        Returns:
          uid: the UID of the recorded entry
        """
        build_in, build_out = entry.load_defaults(self.plugin_uid)
        for k, v in output.iteritems():
            build_out[command].params[k][cti.META_ATTRIBUTE_VALUE] = v
        if not data_entry:
            if cti.META_ATTRIBUTE_REP in self.work_params[command].attributes:
                rep = self.work_params[command].attributes[cti.META_ATTRIBUTE_REP]
            else:
                util.cti_plugin_print_error("Cannot find the repository attribute."
                                      "Please check the default file.")
            data_entry = entry.create_data_entry(self.plugin_uid, rep, self.username)
            if not data_entry:
                util.cti_plugin_print_error("Failed to create data entry.")
        
        # record output
        build_out.record_output(command, data_entry.path)
        self.work_params.record_input(command, data_entry.path)
        
        build_out.update_references("init", data_entry.uid)
        
        if data_entry is not None:
            print plugin.plugin_exit(data_entry.entry, format_o, alias_e=alias_e)
            
        return data_entry.uid
    
    #------------------------------------------------------------------------
    
    def extract_commands(self):
        # extract the command and parser methods
        commands = {}
        
        for obj in dir(self):
            method = getattr(self, obj)
            if callable(method) and hasattr(method, "command_name"):
                commands[method.command_name] = dict(name=method.command_name,
                                                     command=method,
                                                     parser=method.command_parser)
        
        # if help has not been redefined provide a default
        # handler
        if "help" not in commands:
            commands["help"] = dict(name="help",
                                    command=self.default_help_command,
                                    parser=default.default_help_parser)
        
        # if necessary provide a default init
        if ("init" in self.work_params
            and "init" not in commands):
            commands["init"] = dict(name="init",
                                    command=self.default_init_command,
                                    parser=lambda c,a,wp: auto_parser_cli(c,a,wp))
            
        # if necessary provide an update init
        if ("update" in self.work_params
            and "update" not in commands):
            commands["update"] = dict(name="update",
                                    command=self.default_update_command,
                                    parser=lambda c,a,wp: auto_parser_cli(c,a,wp))
        
        # ensure that all commands specified in the json file
        # have been defined
        for command in self.work_params:
            if command not in command:
                raise Exception("Missing a method for command {0}"
                                .format(command))
        
        return commands
    
    #------------------------------------------------------------------------
    
    def parse_default_cl_args(self, argv):
        try:
            self.plugin_directory = argv[1]
            self.plugin_uid = cti.CTI_UID(argv[2])
            self.username = argv[3]
            self.password = argv[4]
            
            # separate the default command line arguments
            # and the plugin specific ones
            self.plugin_args = argv[5:]
            
            if not self.plugin_args:
                util.cti_plugin_print_error("Not enough parameters. Try 'help' command.")
                exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
        except IndexError:
            util.fatal(cti.CTI_ERROR_MSG_UNEXPECTED,
                  cti.CTI_ERROR_UNEXPECTED)
            
    #------------------------------------------------------------------------
    
    def load_def_parameters(self):
        wp, oa = entry.load_defaults(self.plugin_uid)
        if wp is None:
            util.fatal("Missing input/output files for {0}"
                  .format(self.plugin_uid),
                  cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        
        self.work_params, self.output_params = wp, oa
        
    #------------------------------------------------------------------------
    
    def check_passwd(self):
        if not user.check_password(self.username, self.password):
            util.cti_plugin_print_error("Wrong username or password.")
            exit(cti.CTI_ERROR_WRONG_PASSWORD)
    
    #------------------------------------------------------------------------
    
    def check_cti_build_number(self):
        def compare_build(a, b):
            """
                result: 1 -> a < b
                        2 -> b < a
                        3 -> a == b
            """
            a = a.split(".")
            b = b.split(".")
            cpt = 0
            max  = len(a)
            while cpt < max:
                if int(a[cpt]) < int(b[cpt]):
                    return 1
                elif int(a[cpt]) > int(b[cpt]):
                    return 2
                else:
                    cpt += 1
            return 3
        
        info_file = ctr.ctr_plugin_info_file_load_by_uid(self.plugin_uid)
        info_dep = ctr.ctr_plugin_info_get_value(info_file, 
                                                 cti.PLUGIN_INFO_DEPENDENCY)
        
        info_dep = info_dep.split(":")
        
        if len(info_dep) != 2:
            util.fatal("The CTI core build dependence does not follow the correct format",
                       cti.CTI_ERROR_UNEXPECTED)
        
        try:
            min_plugin = info_dep[0]
            max_plugin = info_dep[1]
            
            cti_core_build =  cti.CTI_VERSION_MAJOR + "." + \
                              cti.CTI_VERSION_MINOR + "." + \
                              cti.CTI_VERSION_HOT_FIXES
        
        except:
            util.fatal("The CTI core build dependence does not follow the correct format",
                        cti.CTI_ERROR_UNEXPECTED)
        
        if min_plugin != "" and compare_build(min_plugin, cti_core_build) == 2:
            util.fatal("Your CTI core is too old to run your plugin. Please update it. " +\
                       "Cti core: %s Plugin requires at least %s" % (cti_core_build,
                                                                     min_plugin),
                       cti.CTI_ERROR_UNEXPECTED)
        
        if max_plugin != "" and compare_build(max_plugin, cti_core_build) == 1:
            util.fatal("Your plugin is too old to run on your CTI installation. Please update it. " +\
                       "Cti core: %s Plugin is compatible with %s" % (cti_core_build,
                                                                      max_plugin),
                       cti.CTI_ERROR_UNEXPECTED)
            
    #------------------------------------------------------------------------
    
    def main(self, argv, command_opt = ""):
        """ Runs the plugin
        Args:
          argv: the list of command line arguments
          command_opt: the command to use if not given in argv
        Returns:
          The commmand return code.
        """
        # default command line arguments
        self.parse_default_cl_args(argv)
        
        # load parameters from default files
        self.load_def_parameters()
        # find the registered @commands
        reg_commands = self.extract_commands()
        
        def check_command(command):
            if not command in reg_commands:
                util.cti_plugin_print_error("Command not found: {0}".format(command))
                exit(cti.CTI_PLUGIN_ERROR_COMMAND_NOT_FOUND)
        
        # check for input from file
        if (self.plugin_args[0].startswith("@")):
            command, self.work_params = update_from_file(
                            self.plugin_args[0][1:], self.work_params,
                            self.plugin_uid)
            check_command(command)
        else:  # input is from commandline
            if command_opt != "":
                self.plugin_args.insert(0, command_opt)
            command = self.plugin_args[0]
            
            check_command(command)
            self.work_params = reg_commands[command]['parser'](
                    command, self.plugin_args[1:], self.work_params)
        
        # setup the current command
        self.command = command
        
        # check the cti dependence build number
        self.check_cti_build_number()
        
        # check the username and password
        self.check_passwd()
        
        # execute command
        final_params = easy_params(self.work_params[command].params)
        result = reg_commands[command]['command'](final_params)
        if type(result) is entry.Entry:
            result = 0
        return result

#------------------------------------------------------------------------

def update_from_file(filename, work_params, plugin_uid):
    """Update working parameters from input file.
    Args:
      filename: a string with an input filename.
      work_params: a Commands dictionary with CTI working parameters
    
    Returns:
      A tuple: command and updated work_params.
    """
    # load file specified by filename
    
    # When working with data entry, we need the creator plugin_uid
    # so we can get "list", "optional" and "type" meta attributes
    # we can get this info from the ctr_info file.
    inp, out = entry.load_defaults(plugin_uid)
    
    cmds = entry.Commands(None, None, filename, default_data=inp, no_none_value = True)
    if not cmds:
        util.fatal("Can't load file {0}".
                    format(filename),
                    cti.CTI_PLUGIN_ERROR_UNEXPECTED)
    
    
    # IMPORTANT: We assume that ONLY ONE command is specified in the input file
    # We take the first command and proceed with it
    cmd = cmds.keys()[0]
    # get working repository, use default if none provided
    if cti.META_ATTRIBUTE_REP in cmds[cmd].attributes:
        work_params[cmd].attributes[cti.META_ATTRIBUTE_REP] = (
            cmds[cmd].attributes[cti.META_ATTRIBUTE_REP])
    params = work_params[cmd].params
    for p in params.keys():
        if p in cmds[cmd].params:
          newval = cmds[cmd].params[p][cti.META_ATTRIBUTE_VALUE]
          params[p][cti.META_ATTRIBUTE_VALUE] = newval
    return cmd, work_params

