#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

"""
   This module provides CTI versioning routines.
"""


class Version(object):
    """
       Wrapper class for version routines.
    """

    def __init__(self):
        import cti
        self.major = cti.CTI_VERSION_MAJOR
        self.minor = cti.CTI_VERSION_MINOR
        self.hot_fixes = cti.CTI_VERSION_HOT_FIXES
        self.build = cti.CTI_VERSION_BUILD_NUMBER

    #------------------------------------------------------------------------
    
    def release(self):
        """ Relase version format is MAJOR.MINOR.HOT_FIXES.BUILD """
        return "%s.%s.%s.%s" % (self.major, self.minor, self.hot_fixes, self.build)

    #------------------------------------------------------------------------
    
    def dev(self):
        """ Development version format is MAJOR.MINOR.HOT_FIXES.BUILD """
        return "%s.%s.%s.%s" % (self.major, self.minor, self.hot_fixes, self.build)

version = Version()
