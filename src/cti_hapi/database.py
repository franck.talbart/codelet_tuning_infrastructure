#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

""" Database module provides facilities to manage 
    the database
"""

import cti, ctr

import sqlite3, os, tempfile
import util
 
class Database():
    
    _instance = None
    #/!\ Singleton
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Database, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance
    
    def __init__(self):
        """ Initialize a connection to the CTI database
            Args:
                nothing
            Returns:
                nothing
        """
        cfg_dir = ctr.ctr_plugin_get_cti_cfg_dir()
        self.conn = sqlite3.connect(os.path.join(cfg_dir, 
                                                 cti.cti_plugin_config_get_value(cti.DATABASE_FILENAME)),
                                    check_same_thread = False, timeout=120)
    
    #------------------------------------------------------------------------
    
    @staticmethod
    def drop_database():
        """ Drop the CTI database
            Args:
                nothing
            Returns:
                nothing
        """
        cfg_dir = ctr.ctr_plugin_get_cti_cfg_dir()
        db_file = os.path.join(cfg_dir,
                               cti.cti_plugin_config_get_value(cti.DATABASE_FILENAME))
        if os.path.isfile(db_file):
            os.remove(db_file)
    
    #------------------------------------------------------------------------
    
    def write(self, query, fk=True):
        """ Write data into the database
            Args:
                query: the query to execute
            Returns:
                last row id if success, False otherwise
        """
        # Get the cursor
        c = self.conn.cursor()
        
        if fk:
            # foreign key support is disabled by default in SQLite.
            self.conn.execute('pragma foreign_keys=ON;')
        else:
            self.conn.execute('pragma foreign_keys=OFF;')
            
        try:
            c.execute(query)
            # Save the updates
            self.conn.commit()
            result = c.lastrowid
            # Close the cursor
            c.close()
            return result
        
        except sqlite3.OperationalError, msg:
            # Close the cursor
            c.close()
            util.cti_plugin_print_error("SQL error: %s\n Query:%s" % (msg, query))
            return False
        except sqlite3.IntegrityError, msg:
             # Close the cursor
            c.close()
            util.cti_plugin_print_error("Integrity error: %s\n Queries:%s" % (msg, query))
            return False
    
    #------------------------------------------------------------------------
    
    def write_script(self, sql, fk=True):
        """ Write data into the database
            Args:
                query: the query to execute
            Returns:
                last row id if success, False otherwise
        """
        # Get the cursor
        c = self.conn.cursor()
        
        if fk:
            # foreign key support is disabled by default in SQLite.
            self.conn.execute('pragma foreign_keys=ON;')
        else:
            self.conn.execute('pragma foreign_keys=OFF;')
            
        try:
            c.executescript(sql)
            # Save the updates
            self.conn.commit()
            result = c.lastrowid
            # Close the cursor
            c.close()
            return result
        
        except sqlite3.OperationalError, msg:
            # Close the cursor
            c.close()
            util.cti_plugin_print_error("SQL error: %s\n Query:%s" % (msg, sql))
            return False
        except sqlite3.IntegrityError, msg:
             # Close the cursor
            c.close()
            util.cti_plugin_print_error("Integrity error: %s\n Queries:%s" % (msg, sql))
            return False
    
    #------------------------------------------------------------------------
    
    def write_file(self, sql_file, fk=True):
        """ Write data from the SQL file into the database
            Args:
                sql_file: the SQL file
            Returns:
                last row id if success, False otherwise
        """
        # Get the cursor
        c = self.conn.cursor()
        
        if fk:
            # foreign key support is disabled by default in SQLite
            self.conn.execute('pragma foreign_keys=ON;')
        else:
            self.conn.execute('pragma foreign_keys=OFF;')
            
        sql_file.seek(0,0)
        sql = sql_file.read()
        
        try:
            c.executescript(sql)
            
            # Save the updates
            self.conn.commit()
            result = c.lastrowid
            # Close the cursor
            c.close()
            return result
        
        except sqlite3.OperationalError, msg:
            # Close the cursor
            c.close()
            f = tempfile.NamedTemporaryFile(delete=False)
            f.write(sql)
            f.close()
            util.cti_plugin_print_error("SQL error: %s\n Query file:%s" % (msg, f.name))
            return False
        except sqlite3.IntegrityError, msg:
             # Close the cursor
            c.close()
            f = tempfile.NamedTemporaryFile(delete=False)
            f.write(sql)
            f.close()
            util.cti_plugin_print_error("Integrity error: %s\n Queries:%s" % (msg, f.name))
            return False
    
    #------------------------------------------------------------------------
    
    def read(self, query):
        """ Read data from the database
            Args:
                query: the query to execute
            Returns:
                the query's result
        """
        c = self.conn.cursor()
        try:
            c.execute(query)
        except sqlite3.OperationalError, msg:
            # Close the cursor
            c.close()
            util.cti_plugin_print_error("SQL error: %s\n Queries:%s" % (msg, query))
            exit(False)
        except sqlite3.IntegrityError, msg:
            # Close the cursor
            c.close()
            util.cti_plugin_print_error("Integrity error: %s\n Queries:%s" % (msg, query))
            exit(False)
            
        for row in c:
            yield row
        c.close()
    
    #------------------------------------------------------------------------
    
    def list_db_table(self):
        """ List all tables from the database
            
            Returns:
                the list of tables
        """
        c = self.conn.cursor()
        try:
            c.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;")
        except sqlite3.OperationalError, msg:
            # Close the cursor
            c.close()
            util.cti_plugin_print_error("SQL error: %s\n Select all table" % msg)
            exit(False)
        res = []
        for i in c:
            res.append(i[0])
        res = map(str, res)
        c.close()
        return res
    
    #------------------------------------------------------------------------
    
    def list_columns(self, table):
        """ List all columns for the given table
            
            Returns:
                the list of columns for the given table
        """
        c = self.conn.cursor()
        try:
            c.execute("SELECT * FROM {0} LIMIT 1;".format(table))
        except sqlite3.OperationalError, msg:
            # Close the cursor
            c.close()
            util.cti_plugin_print_error("SQL error: %s\n Select all table" % msg)
            exit(False)
        res = [column[0] for column in c.description]
        c.close()
        return res
    
#------------------------------------------------------------------------
