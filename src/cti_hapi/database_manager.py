#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

""" database_manager module provides all routines to index and search inside the repositories.
"""

import ctr, cti

import util, entry, util_uid, repository, alias, cti_exception
from database import Database

import os, sys, threading, copy, subprocess, Queue, signal, traceback, time

###
# Functions for indexing
###

#------------------------------------------------------------------------

def delete_from_index(constraints, db):
    """
    Delete entry with the given uid.
    Args:
        constraints: used to select the entry to remove
        db: the database instance
    """
    
    sql = "DELETE FROM entry_info "
    sql_add_from,sql_where = parse_constraints(constraints, {}, False)
    if sql_add_from:
        sql += ' {0}'.format(sql_add_from)
    if sql_where:
        sql += ' WHERE {0}'.format(sql_where)
    
    sql += ";"
    return db.write(sql)

#------------------------------------------------------------------------

def get_repository(uid):
    repository_v = ctr.ctr_plugin_get_repository_by_uid(cti.CTR_ENTRY_DATA, uid)
    path_repository = None
    if repository_v == cti.CTR_REP_LOCAL:
        repository_v = cti.LOCAL_REPOSITORY
        path_repository = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(uid,
                                                                            cti.CTR_DATA_DIR)
    elif repository_v ==  cti.CTR_REP_COMMON:
        repository_v = cti.COMMON_REPOSITORY
        path_repository = ctr.ctr_plugin_get_common_dir()
    elif repository_v ==  cti.CTR_REP_TEMP:
        repository_v = cti.TEMP_REPOSITORY
        path_repository = ctr.ctr_plugin_get_temp_dir()
    return (repository_v, path_repository)

#------------------------------------------------------------------------

def index(uid, db):
    """
    Index entry with the given uid.
    This function should be called to index new entries.
    (entries that are not yet indexed)
    Args:
        uid: the UID to index
    """
    entries_log_file_name = cti.cti_plugin_config_get_value(cti.WRONG_ENTRIES_LOG_FILE_NAME)
    cti_cfg_dir = ctr.ctr_plugin_get_cti_cfg_dir()
        
    assert entries_log_file_name is not None
    assert cti_cfg_dir is not None
    
    filename = os.path.join(cti_cfg_dir, entries_log_file_name)
    file_wrong_entries = open(filename, 'w')
    
    try:
        (_, output_param) = entry.load_data(uid)
        command = output_param.keys()[0]
        
        # Only index 'init' entries
        if command != "init":
            return True
        
        if output_param is None:
            util.hapi_error("Can't load the entry %s\n" % uid)
            
        info = entry.load_data_info(uid)        
        repository_v, path_repository = get_repository(uid)
        
        sql = "INSERT INTO entry_info (id_entry_info, repository, path_repository, entry_uid, plugin_uid, date_time_start, \
            date_time_end, user_uid, plugin_exit_code, alias)\
            VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s);" % tuple([prepare_value(e) for e in
                                                                              [\
                                                                                 repository_v,
                                                                                 path_repository,
                                                                                 uid,
                                                                                 info[cti.DATA_INFO_PLUGIN_UID],
                                                                                 info[cti.DATA_INFO_DATE_TIME_START],
                                                                                 info[cti.DATA_INFO_DATE_TIME_END],
                                                                                 info[cti.DATA_INFO_USER_UID],
                                                                                 info[cti.DATA_INFO_PLUGIN_EXIT_CODE],
                                                                                 info[cti.DATA_INFO_ALIAS]
                                                                               ]
                                                                             ]
                                                                            )
        
        primary_key = db.write(sql)
        
        if primary_key is False:
            print "Error while writting the entry into the database."
            raise cti_exception.Hapi_fail()
        
        additional_files = info[cti.DATA_INFO_ADDITIONAL_FILES]
        if additional_files is not None:
            for filename in additional_files.split(","):
                    kv = {
                            "id_additional_files": None,
                            "additional_files": filename,
                            "id_entry_info": primary_key
                          }
                    if insert("additional_files", kv, db) is False:
                        util.cti_plugin_print_error("Error while writting the additional files into the database.")
                        raise cti_exception.Hapi_fail()
                    
        note = info[cti.DATA_INFO_NOTE]
        if note is not None:
            for r in note.split(","):
                    tab_note = r.split("-")
                    txt_note = tab_note[2].strip()
                    kv = {
                            "id_note": None,
                            "note": txt_note,
                            "id_entry_info": primary_key
                          }
                    if insert("note", kv, db) is False:
                        util.cti_plugin_print_error("Error while writting the notes into the database.")
                        raise cti_exception.Hapi_fail()
        tag = info[cti.DATA_INFO_TAG]
        if tag is not None:
            for t in tag.split(" "):
                    kv = {
                            "id_tag": None,
                            "tag": t,
                            "id_entry_info": primary_key
                          }
                    if insert("tag", kv, db) is False:
                        print "Error while writting the tags into the database."
                        raise cti_exception.Hapi_fail()
        
        other_tables_insert = {}
        
        plugin_alias = alias.get_plugin_alias(cti.CTI_UID(info[cti.DATA_INFO_PLUGIN_UID]))
        column_name = []
        column_value = []
        for p in output_param[command].params:
            p_values = output_param[command].params[p][cti.META_ATTRIBUTE_VALUE]
            p_types = output_param[command].params[p][cti.META_ATTRIBUTE_TYPE]
            if cti.META_ATTRIBUTE_LIST in output_param[command].params[p] and output_param[command].params[p][cti.META_ATTRIBUTE_LIST]:
                if p_types != cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
                    #SQLite limitation forces a boolean to integer conversion
                    if p_types == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
                        p_values = [util.boolean_as_int(p_val) for p_val in p_values]
                    table_temp = "%s_%s" % (plugin_alias, p)
                    other_tables_insert[table_temp] = []
                    for p_value in p_values:
                        other_tables_insert[table_temp].append({
                                "id_{0}".format(table_temp): None,
                                p: p_value,
                                "id_{0}".format(plugin_alias): primary_key
                             })
            elif p_types == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
                matrix_columns = output_param[command].params[p][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES]
                #Verifying that the matrix isn't empty:
                if p_values and p_values[matrix_columns[0]]:
                    table_matrix = "%s_%s" % (plugin_alias, p)
                    statement = "INSERT INTO \"{0}\" (\"id_{1}\", \"order\",".format(table_matrix, plugin_alias)
                    
                    #Looping through the matrix to get the columns
                    statement_columns = ["\"{0}\"".format(cname) for cname in matrix_columns]
                    statement += ",".join(statement_columns) + ") "
                    
                    
                    #SQLite limitation forces a boolean to integer conversion
                    for m_col_index in range(len(matrix_columns)):
                        if output_param[command].params[p][cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES][m_col_index] == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
                            output_param[command].params[p][cti.META_ATTRIBUTE_VALUE][matrix_columns[m_col_index]] = [ util.boolean_as_int(a_boolean) for a_boolean in output_param[command].params[p][cti.META_ATTRIBUTE_VALUE][matrix_columns[m_col_index]] ]
                    
                    #Looping for lines
                    values_tuples = []
                    
                    for line in range(len(p_values[matrix_columns[0]])):
                        values_tuple = ["\"{0}\" as \"id_entry_info\", \"{1}\" as \"order\"".format(primary_key,line)]
                        for cname in matrix_columns:
                            values_tuple.append("\"{0}\" as \"{1}\"".format(p_values[cname][line], cname))
                        values_tuples.append('SELECT ' + ','.join(values_tuple))
                    
                    #Looping through the values to make sure we don't get more than the maximum limit for a single insert
                    maximum_insert_limit = 499
                    temp_tuples = values_tuples
                    count_batch = 1
                    while len(temp_tuples)>maximum_insert_limit:
                        other_tables_insert['{0} batch {1}'.format(table_matrix,count_batch)] = statement +\
                                                        ' UNION ALL '.join(temp_tuples[:maximum_insert_limit])
                        temp_tuples = temp_tuples[maximum_insert_limit:]
                        count_batch += 1
                    other_tables_insert['{0} batch {1}'.format(table_matrix,count_batch)] = statement + ' UNION ALL '.join(temp_tuples)
                    
            else:
                #SQLite limitation forces a boolean to integer conversion
                if p_types == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
                    p_values = util.boolean_as_int(p_values)
                elif util_uid.is_valid_uid(p_values):
                    p_values = uid2id(p_values, db)
                column_value.append(p_values)
                column_name.append(p)
        
        column_name.insert(0, "id_%s" % (plugin_alias))
        column_value.insert(0, str(primary_key))
        
        kv = {}
        for cn, cv in zip(column_name, column_value):
            kv[cn] = cv
        if insert(plugin_alias, kv, db) is False:
            print "Error while writting the entry into the database."
            raise cti_exception.Hapi_fail()
        
        #Insert lists and matrixes
        for key in other_tables_insert:
            sql_inserts = other_tables_insert[key]
            if isinstance(sql_inserts, list):
                for insert_dict in sql_inserts:
                    if insert(key, insert_dict, db) is False:
                        util.cti_plugin_print_error("Error while writting {0} contents into the database.".format(key))
            else:
                if db.write(other_tables_insert[key]) is False:
                    util.cti_plugin_print_error("Error while writting {0} into the database.\n".format(key))
    except Exception as e:
        util.cti_plugin_print_error("Failed to prepare entry {uid}\nException: {e}\n".format(uid=uid, e=e))
        file_wrong_entries.write(str(uid) + "\n")
        delete_from_index({'NAME':["entry_info.entry_uid"], 'TYPE':"=", 'VAL':str(uid)}, db)
        return False
   
    file_wrong_entries.close()
    return True

#------------------------------------------------------------------------

def prepare_value(value):
    """
    Internal function which prepares the value in order to generate a SQL command
    Args:
        value: the value to prepare
    Return:
        the prepared value
    """
    
    if value == None:
        return "NULL"
    else:
        return "\"" + str(value).replace("\"", "'") + "\""

#------------------------------------------------------------------------

def index_group(uids, db, max_threads=8):
    """
    Index a group of entries with the given uids.
    This function should be called to index new entries.
    (entries that are not yet indexed)
    Args:
        uid: the UID to index
        db: the database instance
    """
    #Setting general static variables
    index_group.lp = list_plugins()
    index_group.column_names = dict([(table, db.list_columns(table)) for table in db.list_db_table()])
    index_group.nb_threads_max = max_threads
    
    #Setting thread queues
    index_group.queues['error_messages'] = Queue.Queue()
    index_group.queues['list_uid'] = Queue.Queue()
    index_group.queues['sql_transactions'] = Queue.Queue()
    
    #Setting table information queues
    index_group.queues['entry_info'] = Queue.Queue()
    index_group.queues['general_tables'] = Queue.Queue()
    index_group.queues['other_tables'] = Queue.Queue()
    
    #Setting thread semaphores
    index_group.semaphores['aliases'] = threading.Semaphore()
    index_group.semaphores['info_entry'] = threading.Semaphore()
    
    #------------------------------------------------------------------------
    
    def signal_handler(signal, frame):
        sys.stdout.write("Stopping threads\n")
        #Starting all threads
        for index in index_group.threads:
            if index is not 'uid_process':
                sys.stdout.write("Trying to stop thread '{0}'\n".format(index))
                index_group.threads[index].join()
        
        cpt = 1
        for thread in index_group.threads['uid_process']:
            sys.stdout.write("Trying to stop thread 'uid_process_{0}'\n".format(cpt))
            thread.join()
            cpt += 1
        exit(0)
    signal.signal(signal.SIGINT, signal_handler)
    
    #------------------------------------------------------------------------
    
    def process_uid_data(cti_thread):
        
        def add_select(values, type):
            """
            """
            val = [prepare_value(values[c]) for c in index_group.column_names[type]]
            return " SELECT %s\n" % (",".join(val))
            
        #------------------------------------------------------------------------
        
        while not cti_thread.stop_requested():
            try:
                uid = index_group.queues['list_uid'].get(True, 0.05)
                
                #Checking that the output files can be loaded before doing any further work
                info = entry.load_data_info(uid)
                try:
                    (_, output_param) = entry.load_data(uid, True)
                except:
                    output_param = None
                
                if output_param is None:
                    #Removing the lock to let other thread work
                    index_group.queues['error_messages'].put((str(uid), "Can't load the entry %s\n" % str(uid)))
                    index_group.queues['list_uid'].task_done()
                    continue
                
                #Processing entry information
                repository_v, path_repository = get_repository(uid)
                
                result_dict = \
                {
                    "entry_select": None,
                    "params_selects": None,
                    "additional_files_selects": [],
                    "tag_selects": [],
                    "note_selects": [],
                    "other_tables_selects": {}
                }
                
                plugin_alias = index_group.lp[info[cti.DATA_INFO_PLUGIN_UID]]["alias"]
                if plugin_alias is None:
                    index_group.queues['error_messages'].put((str(uid), "ERROR with the entry %s.\n\tNo alias found for plugin %s\n" % (str(uid), info[cti.DATA_INFO_PLUGIN_UID])))
                    index_group.queues['list_uid'].task_done()
                    continue
                
                #Checking the alias then updating the list of aliases
                index_group.semaphores['aliases'].acquire()
                if info[cti.DATA_INFO_ALIAS] is not None and info[cti.DATA_INFO_ALIAS] in index_group.list_alias:
                    #Removing the lock to let other thread work
                    index_group.semaphores['aliases'].release()
                    index_group.queues['error_messages'].put((str(uid), "ERROR with the entry %s.\n\tAlias %s already used!\n" % (str(uid), info[cti.DATA_INFO_ALIAS])))
                    index_group.queues['list_uid'].task_done()
                    continue
                
                #Finally updating the alias now that basic verifications have been done
                index_group.list_alias.append(info[cti.DATA_INFO_ALIAS])
                index_group.semaphores['aliases'].release()
                
                #Updating info_entry for this uid
                retry = False
                if not str(uid) in index_group.info_entry:
                    index_group.semaphores['info_entry'].acquire()
                    index_group.info_entry[str(uid)] = \
                    {
                        "id":index_group.info_entry['primary_key_counter'],
                        "plugin_alias": plugin_alias
                    }
                    index_group.info_entry['primary_key_counter'] += 1
                    index_group.semaphores['info_entry'].release()
                else:
                    retry = True
                #Preparing values for entry_info
                result_dict['entry_select'] = add_select(
                        {
                            "repository": repository_v,
                            "path_repository": path_repository,
                            "entry_uid": str(uid),
                            "plugin_uid": info[cti.DATA_INFO_PLUGIN_UID],
                            "date_time_start": info[cti.DATA_INFO_DATE_TIME_START],
                            "date_time_end": info[cti.DATA_INFO_DATE_TIME_END],
                            "user_uid": info[cti.DATA_INFO_USER_UID],
                            "plugin_exit_code": info[cti.DATA_INFO_PLUGIN_EXIT_CODE],
                            "alias": info[cti.DATA_INFO_ALIAS],
                            "id_entry_info" : index_group.info_entry[str(uid)]["id"]
                        },
                        "entry_info"
                    )
                
                additional_files = info[cti.DATA_INFO_ADDITIONAL_FILES]
                if additional_files is not None:
                    for filename in additional_files.split(","):
                            result_dict['additional_files_selects'].append(
                                    add_select(
                                        {
                                            "additional_files": filename,
                                            "id_entry_info": index_group.info_entry[str(uid)]["id"],
                                            "id_additional_files": None
                                        },
                                        "additional_files"
                                    )
                                )
                
                note = info[cti.DATA_INFO_NOTE]
                if note is not None:
                    for r in note.split(","):
                            tab_note = r.split("-")
                            txt_note = tab_note[2].strip()
                            result_dict['note_selects'].append(
                                    add_select(
                                        {
                                            "note": txt_note,
                                            "id_entry_info": index_group.info_entry[str(uid)]["id"],
                                            "id_note": None
                                        },
                                        "note"
                                    )
                                )
                
                tag = info[cti.DATA_INFO_TAG]
                if tag is not None:
                    for t in tag.split(" "):
                            result_dict['tag_selects'].append(
                                    add_select(
                                        {
                                            "tag": t,
                                            "id_entry_info": index_group.info_entry[str(uid)]["id"],
                                            "id_tag": None
                                        },
                                        "tag"
                                    )
                                )
                
                
                #Processing plugin-specific information
                command = output_param.keys()[0]
                # Only index 'init' entries
                if command == "init":
                    dict_value = {"id_{0}".format(plugin_alias): index_group.info_entry[str(uid)]["id"]}
                    get_out = False
                    for p in output_param[command].params:
                        p_values = output_param[command].params[p][cti.META_ATTRIBUTE_VALUE]
                        if output_param[command].params[p].has_key(cti.META_ATTRIBUTE_TYPE):
                            type_values = output_param[command].params[p][cti.META_ATTRIBUTE_TYPE]
                        else:
                            type_values = cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT
                        if cti.META_ATTRIBUTE_LIST in output_param[command].params[p] and\
                                 output_param[command].params[p][cti.META_ATTRIBUTE_LIST]:
                            if type_values != cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
                                #SQLite limitation forces a boolean to integer conversion
                                if type_values == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
                                    p_values = [util.boolean_as_int(p) for p in p_values]
                                type_temp = "%s_%s" % (plugin_alias, p)
                                result_dict["other_tables_selects"][type_temp] = []
                                for p_value in p_values:
                                    result_dict["other_tables_selects"][type_temp].append(
                                                add_select(
                                                    {
                                                    p: p_value,
                                                    "id_{0}".format(plugin_alias): index_group.info_entry[str(uid)]["id"],
                                                    "id_{0}".format(type_temp): None
                                                    },
                                                    type_temp
                                                )
                                            )
                                    
                        elif type_values == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
                            table_matrix = "%s_%s" % (plugin_alias, p)
                            result_dict["other_tables_selects"][table_matrix] = []
                            matrix_columns = output_param[command].params[p][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES]
                            
                            #SQLite limitation forces a boolean to integer conversion
                            if output_param[command].params[p][cti.META_ATTRIBUTE_VALUE] is not None:
                                for m_col_index in range(len(matrix_columns)):
                                    if output_param[command].params[p][cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES][m_col_index] == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
                                         output_param[command].params[p][cti.META_ATTRIBUTE_VALUE][matrix_columns[m_col_index]] = [ util.boolean_as_int(a_boolean) for a_boolean in output_param[command].params[p][cti.META_ATTRIBUTE_VALUE][matrix_columns[m_col_index]] ]
                            
                            #Looping for lines
                            if p_values is not None and len(p_values[matrix_columns[0]]) > 0:
                                for line in range(len(p_values[matrix_columns[0]])):
                                    content_matrix = {
                                                        "id_{0}".format(plugin_alias): index_group.info_entry[str(uid)]["id"],
                                                        "order": line,
                                                        "id_{0}".format(table_matrix): None
                                                     }
                                    for cname in matrix_columns:
                                        content_matrix[cname] = p_values[cname][line]
                                    result_dict["other_tables_selects"][table_matrix].append(
                                                add_select(
                                                   content_matrix,
                                                   table_matrix
                                                )
                                            )
                        else:
                            #SQLite limitation forces a boolean to integer conversion
                            if type_values == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
                                p_values = util.boolean_as_int(p_values)
                            elif isinstance(p_values, cti.CTI_UID):
                                if index_group.info_entry.has_key(str(p_values)):
                                    p_values = index_group.info_entry[str(p_values)]["id"]
                                elif retry:
                                    p_values = True
                                else:
                                    #Putting the current item at the end of the queue in case of missing info_entry data.
                                    #Since the info_entry has already been updated with this ID, other threads will work with it
                                    # and not lock will occur.
                                    index_group.queues['list_uid'].task_done()
                                    index_group.queues['list_uid'].put(uid)
                                    get_out = True
                                    index_group.list_alias.remove(info[cti.DATA_INFO_ALIAS])
                                    break
                            dict_value[p] = p_values
                    if get_out:
                        continue
                    
                    result_dict["params_selects"] = add_select(
                           dict_value,
                           plugin_alias
                        )
                
                #Processing generated results
                index_group.queues['entry_info'].put(('entry_info', result_dict["entry_select"]))
                for select in result_dict["tag_selects"]:
                    index_group.queues['general_tables'].put(('tag', select))
                for select in result_dict["note_selects"]:
                    index_group.queues['general_tables'].put(('note', select))
                for select in result_dict["additional_files_selects"]:
                    index_group.queues['general_tables'].put(('additional_files', select))
                
                if result_dict["params_selects"]:
                    index_group.queues['other_tables'].put((plugin_alias, result_dict["params_selects"]))
                
                for other_table in result_dict["other_tables_selects"]:
                    for insert_line in result_dict["other_tables_selects"][other_table]:
                        index_group.queues['other_tables'].put((other_table, insert_line))
                
                index_group.queues['list_uid'].task_done()
                
            except Queue.Empty:
                #Retry getting an item if the queue was empty
                continue
            except Exception as e:
                msg_err_traceback = "Unexpected error:\n\t{0}\n\t".format(repr(e)) + "\n\t".join(traceback.format_exception(*(sys.exc_info())))
                index_group.queues['error_messages'].put((str(uid), msg_err_traceback))
                index_group.queues['list_uid'].task_done()
                continue
    
    #------------------------------------------------------------------------
    
    def generate_statement(contents, table):
        statement = "BEGIN TRANSACTION;\n" + \
                    "INSERT INTO \"{0}\" (".format(table) + \
                    ",".join(["\"{0}\"".format(cname) for cname in index_group.column_names[table]]) + \
                    ")\n" + "\nUNION ".join(contents) + \
                    ";\nCOMMIT;"
        index_group.queues['sql_transactions'].put(statement)
    
    #------------------------------------------------------------------------
    
    def generate_transactions(cti_thread, name_queue):
        """
        """
        contents = {}
        
        while(not cti_thread.stop_requested()):
            try:
                (table, item) = index_group.queues[name_queue].get(True, 0.05)
                if not table in contents:
                    contents[table] = [item]
                elif len(contents[table]) < index_group.db_limit:
                    contents[table].append(item)
                else:
                    generate_statement(contents[table], table)
                    contents[table] = [item]
                
                index_group.queues[name_queue].task_done()
            except Queue.Empty:
                continue
            except Exception as e:
                msg_err_traceback = "Unexpected error:\n\t{0}\n".format(repr(e)) + "\n\t".join(traceback.format_exception(*(sys.exc_info())))
                index_group.queues['error_messages'].put(("GENERATE TRANSACTIONS", msg_err_traceback))
                index_group.queues['list_uid'].task_done()
                continue
        try:
            for table in contents:
                if contents[table]:
                    generate_statement(contents[table], table)
        except Exception as e:
            msg_err_traceback = "Unexpected error:\n\t{0}\n".format(repr(e)) + "\n\t".join(traceback.format_exception(*(sys.exc_info())))
            index_group.queues['error_messages'].put(("GENERATE TRANSACTIONS", msg_err_traceback))
            index_group.queues['list_uid'].task_done()
    
    #------------------------------------------------------------------------
    
    def process_inserts(cti_thread, db):
        """
        """
        while(not cti_thread.stop_requested()):
            try:
                transaction = index_group.queues['sql_transactions'].get(True, 0.05)
                db.write_script(transaction, False)
                index_group.queues['sql_transactions'].task_done()
            except Queue.Empty:
                #Get to the next queue if the queue was empty
                continue
            except Exception as e:
                msg_err_traceback = "Unexpected error:\n\t{0}\n".format(repr(e)) + "\n\t".join(traceback.format_exception(*(sys.exc_info())))
                index_group.queues['error_messages'].put(("SQL INSERTION", msg_err_traceback))
                continue
    
    #------------------------------------------------------------------------
    
    entries_log_file_name = cti.cti_plugin_config_get_value(cti.WRONG_ENTRIES_LOG_FILE_NAME)
    cti_cfg_dir = ctr.ctr_plugin_get_cti_cfg_dir()
    
    assert entries_log_file_name is not None
    assert cti_cfg_dir is not None
    
    filename = os.path.join(cti_cfg_dir, entries_log_file_name)
    file_wrong_entries = open(filename, 'w')
    
    index_group.info_entry['primary_key_counter'] = 0
    sql_max_primary_key = "SELECT MAX(id_entry_info) FROM entry_info;"
    for c in db.read(sql_max_primary_key):
        index_group.info_entry['primary_key_counter'] = c[0]
        if index_group.info_entry['primary_key_counter'] == None:
            index_group.info_entry['primary_key_counter'] = 0
        else:
            index_group.info_entry['primary_key_counter'] += 1
    
    uid_ko = []
    
    print "Preparing the entries (it may take some time)..."
    
    
    sql = "SELECT id_entry_info, entry_uid, plugin_uid, alias FROM entry_info;"
    for r in db.read(sql):
        index_group.info_entry[str(r[1])] = {"id": r[0], "plugin_alias": index_group.lp[r[2]]["alias"]}
        index_group.list_alias.append(r[3])
    
    #Preparing and launching threads
    index_group.threads['insert'] = util.CtiThread(process_inserts, [db])
    index_group.threads['entry_transact'] = util.CtiThread(generate_transactions, ["entry_info"])
    index_group.threads['general_transact'] = util.CtiThread(generate_transactions, ["general_tables"])
    index_group.threads['other_transact'] = util.CtiThread(generate_transactions, ["other_tables"])
    index_group.threads['uid_process'] = []
    
    #Creating at least one thread to process the UIDs
    if index_group.nb_threads_max <= (len(index_group.threads.keys())-1+len(index_group.threads['uid_process'])):
        index_group.threads['uid_process'].append(util.CtiThread(process_uid_data, []))
    else:
        while((len(index_group.threads.keys())-1+len(index_group.threads['uid_process'])) < index_group.nb_threads_max):
            index_group.threads['uid_process'].append(util.CtiThread(process_uid_data, []))
    
    #Starting all threads
    for index in index_group.threads:
        if index is not 'uid_process':
            index_group.threads[index].start()
    
    for thread in index_group.threads['uid_process']:
        thread.start()
    
    count_uid = 0
    
    #Giving work to the threads
    for uid in uids:
        index_group.queues['list_uid'].put(uid)
        count_uid += 1
    
    #Reading messages while waiting for the initial queue to be processed.
    while not index_group.queues['list_uid'].empty():
        try:
            util.rewrite_output_line("Prepare remaining {0: >8} uid(s).".format(index_group.queues['list_uid'].qsize()))
            (title, msg) = index_group.queues['error_messages'].get(True, 0.1)
            if util_uid.is_valid_uid(title):
                uid_ko.append(copy.copy(title))
                title = "Failed to prepare entry {0}".format(title)
            sys.stdout.write("\n" + title + ":\n\t" + msg + "\n")
            index_group.queues['error_messages'].task_done()
        except Queue.Empty:
            continue
    util.rewrite_output_line("Prepare remaining {0: >8} uid(s).\n".format(0))
    
    #Making sure nothing has beed added back to the list of uids at the last second:
    time.sleep(1)
    index_group.queues['list_uid'].join()
    
    #Terminating the uid processing threads
    for thread in index_group.threads['uid_process']:
        thread.join()
    
    #Waiting for the data queues to empty
    index_group.queues['entry_info'].join()
    index_group.queues['general_tables'].join()
    index_group.queues['other_tables'].join()
    
    #Terminating transaction threads
    index_group.threads['entry_transact'].join()
    index_group.threads['general_transact'].join()
    index_group.threads['other_transact'].join()
    
    #Waiting for the SQL queue to empty
    while not index_group.queues['sql_transactions'].empty():
        try:
            util.rewrite_output_line("Inserting remaining {0: >8} line(s) in the database.".format(index_group.queues['sql_transactions'].qsize()))
            (title, msg) = index_group.queues['error_messages'].get(True, 0.1)
            if util_uid.is_valid_uid(title):
                uid_ko.append(copy.copy(title))
                title = "Failed to prepare entry {0}".format(title)
            sys.stdout.write(title + ":\n\t" + msg + "\n")
            index_group.queues['error_messages'].task_done()
        except Queue.Empty:
            continue
    util.rewrite_output_line("Inserting remaining {0: >8} line(s) in the database.\n".format(0))
    
    #Terminating insert thread
    index_group.threads['insert'].join()
    
    #Reading remaining error messages
    while not index_group.queues['error_messages'].empty():
        try:
            (title, msg) = index_group.queues['error_messages'].get(True, 0.05)
            if util_uid.is_valid_uid(title):
                uid_ko.append(copy.copy(title))
                title = "Failed to prepare entry {0}".format(title)
            sys.stdout.write(title + ":\n\t" + msg + "\n")
            index_group.queues['error_messages'].task_done()
        except:
            continue
    
    #Writing wrong entries file
    for uid in uid_ko:
        file_wrong_entries.write(str(uid) + "\n")
    file_wrong_entries.close()
    
    print "Number of entrie(s): %d" % count_uid
    print "Success: %d entrie(s)" % (count_uid - len(uid_ko))
    print "Failure: %d entrie(s)" % len(uid_ko)
    
    
    if len(uid_ko) > 0:
        print """Some entries can't be indexed.
Please check the file {filename}
or do cti clean entry to remove them.
              """.format(filename=filename)
    

#Static variables for this function
#These are declared as static so that threads declared inside index_group can use them
#MUST BE DECLARED AFTER THE FUNCTION DEFINITION: DO NOT MOVE
index_group.list_alias = []
index_group.info_entry = {}
index_group.lp = {}
index_group.queues = {}
index_group.semaphores = {}
index_group.column_names = {}
index_group.db_limit = 500
index_group.nb_threads_max = 8
index_group.threads = {}

#------------------------------------------------------------------------

def create_schema():
    """
    Automatically design the relational schema
    Args:
        db: the database instance
    """
    
    print "Dropping the database (if exists)"    
    Database.drop_database()
    db = Database()
    
    print "* Creating generic tables"
    
    print " * Creating table plugin"    
    sql = "CREATE TABLE IF NOT EXISTS plugin (id_plugin INTEGER,\
                               plugin_uid VARCHAR(36) UNIQUE,\
                               alias TEXT UNIQUE,\
                               category_uid VARCHAR(36),\
                               desc TEXT,\
                               repository_type VARCHAR(6),\
                               repository_uid VARCHAR(36),\
                               type TEXT,\
                               PRIMARY KEY(id_plugin));"
                               
    if db.write(sql, fk=False) is False:
        util.cti_plugin_print_error("Error while writting the plugin table into the database.")
        raise cti_exception.Hapi_fail()
    
    print " * Creating table link_table"    
    sql = "CREATE TABLE IF NOT EXISTS link_table (source TEXT,\
                               target TEXT,\
                               type VARCHAR(3),\
                               name_source TEXT,\
                               name_target TEXT,\
                               attribute_type TEXT,\
                               PRIMARY KEY(source, name_source));"
                               
    if db.write(sql, fk=False) is False:
        util.cti_plugin_print_error("Error while writting the link_table table into the database.")
        raise cti_exception.Hapi_fail()
    
    print " * Creating table entry_info"
    sql = "CREATE TABLE IF NOT EXISTS entry_info (id_entry_info INTEGER,\
                               repository TEXT,\
                               path_repository TEXT,\
                               entry_uid VARCHAR(36) UNIQUE,\
                               plugin_uid VARCHAR(36),\
                               date_time_start DATE,\
                               date_time_end DATE,\
                               user_uid VARCHAR(36),\
                               plugin_exit_code INTEGER,\
                               alias TEXT UNIQUE,\
                               PRIMARY KEY(id_entry_info));"
                               
    if db.write(sql, fk=False) is False:
        util.cti_plugin_print_error("Error while writting the entry table into the database.")
        raise cti_exception.Hapi_fail()
    
    print " * Creating table note"
    sql = "CREATE TABLE IF NOT EXISTS note (id_note INTEGER,\
                               note TEXT,\
                               id_entry_info INTEGER,\
                               FOREIGN KEY(id_entry_info) REFERENCES entry_info(id_entry_info) ON DELETE CASCADE,\
                               PRIMARY KEY(id_note));"
                               
    if db.write(sql, fk=False) is False:
        util.cti_plugin_print_error("Error while writting the note table into the database.")
        raise cti_exception.Hapi_fail()
    
    print " * Creating table tag"    
    sql = "CREATE TABLE IF NOT EXISTS tag (id_tag INTEGER,\
                               tag TEXT,\
                               id_entry_info INTEGER,\
                               FOREIGN KEY(id_entry_info) REFERENCES entry_info(id_entry_info) ON DELETE CASCADE\
                               PRIMARY KEY(id_tag));"
    if db.write(sql, fk=False) is False:
        util.cti_plugin_print_error("Error while writting the tag table into the database.")
        raise cti_exception.Hapi_fail()
    
    print " * Creating table additional_files"
    sql = "CREATE TABLE IF NOT EXISTS additional_files (id_additional_files INTEGER,\
                               additional_files TEXT,\
                               id_entry_info INTEGER,\
                               FOREIGN KEY(id_entry_info) REFERENCES entry_info(id_entry_info) ON DELETE CASCADE,\
                               PRIMARY KEY(id_additional_files));"
    if db.write(sql, fk=False) is False:
        util.cti_plugin_print_error("Error while writting the additional_files table from the database.")
        raise cti_exception.Hapi_fail()
    
    #Writing note, tag and additional_files links into the link_table
    sql_link = "INSERT INTO \"link_table\" (\"source\", \"target\", \"type\", \"name_source\", \"name_target\", \"attribute_type\")\
                                      SELECT \"entry_info\", \"note\", \"O2M\", \"note\", \"id_entry_info\", \"{text}\"\
                                      UNION ALL\
                                      SELECT \"entry_info\", \"tag\", \"O2M\", \"tag\", \"id_entry_info\", \"{text}\"\
                                      UNION ALL\
                                      SELECT \"entry_info\", \"additional_files\", \"O2M\", \"additional_files\", \"id_entry_info\", \"{text}\"\
                                      ;".format(text=cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT)
    if db.write(sql_link) is False:
            print "Error while writting into the link_table table" 
    
    lp = list_plugins()
    
    for p in lp.keys():
        create_plugin_schema(db, p, lp)

#------------------------------------------------------------------------

def create_plugin_schema(db, p, plugins_info):
    """
    Automatically design the relational schema for a given plugin
    Args:
        db: the database instance
        p: the UID of the plugin to create
        plugins_info: a dictionnary of misc information about each plugin.
    """
    
    
    plugin_dir = ctr.ctr_plugin_get_path_by_uid(cti.CTR_ENTRY_PLUGIN,
                                         util_uid.CTI_UID(str(p), cti.CTR_ENTRY_PLUGIN))
    
    pre_generate_schema = os.path.join(plugin_dir,
                                       cti.cti_plugin_config_get_value(cti.PLUGIN_PRE_GENERATE_SCHEMA))
    
    if os.path.isfile(pre_generate_schema):
        print "-> Generating the dynamic schema of %s" % (plugins_info[p]["alias"])
        sys.stdout.flush()
        child_schema = subprocess.Popen([pre_generate_schema, str(p)], shell=False)
        child_schema.communicate()
        rc = child_schema.returncode
        if rc != 0:
            util.cti_plugin_print_error("Error while running the pre_generate_schema script\n")
            
    type_plugin = None
    (_, output_default) = entry.load_defaults(util_uid.CTI_UID(str(p), cti.CTR_ENTRY_PLUGIN))
    table = plugins_info[p]["alias"]
    if output_default.has_key("init") and table is not None:        
        print "* Creating table for plugin %s" % (table)
        sql = "DROP TABLE IF EXISTS \"%s\";" % (table)
        if db.write(sql) is False:
            util.cti_plugin_print_error("Error while removing the %s table from the database." % (table))
            raise cti_exception.Hapi_fail()
        sql = "CREATE TABLE \"%s\" (\"id_%s\" INTEGER," % (table, table)
        
        #------------------------------------------------------------------------
        def cti_type2sql_type(cti_type):
            if cti_type in [cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT,
                            cti.META_CONTENT_ATTRIBUTE_TYPE_FILE,
                            cti.META_CONTENT_ATTRIBUTE_TYPE_URL,
                            cti.META_CONTENT_ATTRIBUTE_TYPE_EMAIL,
                            cti.META_CONTENT_ATTRIBUTE_TYPE_COMPLEX]:
                type_attr = "TEXT"
            elif cti_type in [cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID,
                              cti.META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID]:
                type_attr = "VARCHAR(36)"
            elif cti_type in [cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID]:
                type_attr = "INTEGER" # DATA_UID are converted to PRIMARY KEY ID
            elif cti_type == cti.META_CONTENT_ATTRIBUTE_TYPE_DATE:
                type_attr = "DATE"
            elif cti_type == cti.META_CONTENT_ATTRIBUTE_TYPE_FLOAT:
                type_attr = "FLOAT"
            elif cti_type == cti.META_CONTENT_ATTRIBUTE_TYPE_INTEGER:
                type_attr = "INTEGER"
            elif cti_type == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
                type_attr = "MATRIX"
            elif cti_type == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
                type_attr = "INTEGER"
            try:
                return type_attr
            except:
                raise Exception('%s'%cti_type)
        #------------------------------------------------------------------------
        linked_tables_sql = []
        for param in output_default["init"].params:
            type_attr_cti = output_default["init"].params[param][cti.META_ATTRIBUTE_TYPE]
            type_attr = cti_type2sql_type(type_attr_cti)
            
            if type_attr == "MATRIX":
                matrix_table = "%s_%s" % (table,param)
                matrix_link_id = "id_%s" % table
                
                #Generating SQL column definitions for the matrix table.
                matrix_columns = ''
                for i in range(len(output_default["init"].params[param][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES])):
                    matrix_columns += "\"%s\" %s DEFAULT NULL," % \
                        (output_default["init"].params[param][cti.META_ATTRIBUTE_MATRIX_COLUMN_NAMES][i],
                         cti_type2sql_type(output_default["init"].params[param][cti.META_ATTRIBUTE_MATRIX_COLUMN_TYPES][i]))
                linked_tables_sql.append({'sql':"DROP TABLE IF EXISTS \"{0}\";".format(matrix_table),'msg':"Dropping table {0}".format(matrix_table)})
                linked_tables_sql.append({'sql':"CREATE TABLE \"{m_table}\" (\"id_{m_table}\" INTEGER, \"id_{table}\" INTEGER, \
                           \"order\" INTEGER, {columns}\
                           FOREIGN KEY(\"id_{table}\") REFERENCES \"{table}\"(\"id_{table}\") ON DELETE CASCADE, \
                           PRIMARY KEY(\"id_{m_table}\"),\
                           UNIQUE (\"id_{table}\", \"order\"));".format(table=table,m_table=matrix_table, columns=matrix_columns),
                        'msg':"Creating table {0}".format(matrix_table)})
                
                sql_link = "INSERT INTO \"link_table\" (\"source\", \"target\", \"type\", \"name_source\", \"name_target\", \"attribute_type\")\
                                      VALUES (\"%s\", \"%s\", \"O2M\", \"%s\", \"%s\", \"%s\");" % \
                                      (table, matrix_table, param, matrix_link_id, type_attr_cti)
                if db.write(sql_link) is False:
                        util.cti_plugin_print_error("Error while writting into the link_table table")
                        raise cti_exception.Hapi_fail()
                sql_link = "INSERT INTO \"link_table\" (\"source\", \"target\", \"type\", \"name_source\", \"name_target\", \"attribute_type\")\
                                      VALUES (\"%s\", \"%s\", \"M2O\", \"%s\", \"id_%s\", \"%s\");" % \
                                      (matrix_table, table, matrix_link_id, table, type_attr_cti)
                if db.write(sql_link) is False:
                        util.cti_plugin_print_error("Error while writting into the link_table table.")
                        raise cti_exception.Hapi_fail()
            else:
                if output_default["init"].params[param].has_key(cti.META_ATTRIBUTE_TARGET):
                    target = output_default["init"].params[param][cti.META_ATTRIBUTE_TARGET]
                    produced_by = output_default["init"].params[param][cti.META_ATTRIBUTE_PRODUCED_BY]
                    plugin_target = plugins_info[produced_by]["alias"]
                    sql_link = "INSERT INTO \"link_table\" \
                                           (\"source\", \"target\", \"type\", \"name_source\", \"name_target\", \"attribute_type\")\
                                       VALUES \
                                           (\"%s\", \"%s\", \"O2M\", \"%s\", \"%s\", \"%s\");" % (
                                                                                                      table,
                                                                                                      plugin_target,
                                                                                                      param,
                                                                                                      target,
                                                                                                      type_attr_cti
                                                                                                    )
                                                                                           
                    if db.write(sql_link) is False:
                        util.cti_plugin_print_error("Error while writting into the link_table table.")
                        raise cti_exception.Hapi_fail()
                    
                elif output_default["init"].params[param].has_key(cti.META_ATTRIBUTE_PRODUCED_BY) and\
                        not output_default["init"].params[param].has_key(cti.META_ATTRIBUTE_LIST):
                    produced_by = output_default["init"].params[param][cti.META_ATTRIBUTE_PRODUCED_BY]
                    plugin_target = plugins_info[produced_by]["alias"]
                    sql_link = "INSERT INTO \"link_table\" \
                                           (\"source\", \"target\", \"type\", \"name_source\", \"name_target\", \"attribute_type\")\
                                       VALUES \
                                           (\"%s\", \"%s\", \"M2O\", \"%s\", \"id_%s\", \"%s\");"% (
                                                                                           table,
                                                                                           plugin_target,
                                                                                           param,
                                                                                           plugin_target,
                                                                                           type_attr_cti
                                                                                        )
                    if db.write(sql_link) is False:
                        util.cti_plugin_print_error("Error while writting into the link_table table.")
                        raise cti_exception.Hapi_fail()
                    
                if output_default["init"].params[param].has_key(cti.META_ATTRIBUTE_LIST) and\
                        output_default["init"].params[param][cti.META_ATTRIBUTE_LIST]:
                            if type_attr_cti != "DATA_UID":
                                table_other = "%s_%s" % (table, param)
                                linked_tables_sql.append({'sql':"DROP TABLE IF EXISTS \"%s\";" % (table_other),
                                                          'msg':"Dropping table {0}.".format(table_other)})
                                linked_tables_sql.append({'sql':"CREATE TABLE \"{table_o}\" (\"id_{table_o}\" INTEGER,\
                                                                  \"{attr}\" {type},\
                                                                  \"id_{table}\" INTEGER,\
                                                                  FOREIGN KEY(\"id_{table}\") REFERENCES \"{table}\"(\"id_{table}\") ON DELETE CASCADE,\
                                                                  PRIMARY KEY(\"id_{table_o}\"));".format(table_o=table_other,
                                                                                           attr=param,
                                                                                           type=type_attr,
                                                                                           table=table),
                                                          'msg':"Creating table {0}.".format(table_other)})
                                sql_link = "INSERT INTO \"link_table\" \
                                                  (\"source\", \"target\", \"type\", \"name_source\",\"name_target\",\"attribute_type\")\
                                              VALUES \
                                                  (\"{table}\", \"{table_o}\", \"O2M\", \"{param}\", \"id_{table}\", \"{attr_type}\");".format(
                                                                                                                      table=table,
                                                                                                                      param=param,
                                                                                                                      table_o=table_other,
                                                                                                                      attr_type=type_attr_cti
                                                                                                                    )
                                                                         
                                if db.write(sql_link) is False:
                                        print "Error while writting into the link_table table."
                                        raise cti_exception.Hapi_fail()
                else:
                   sql += "\"%s\" %s DEFAULT NULL," % (param, type_attr)
        sql += "PRIMARY KEY(\"id_%s\")," % table
        sql += "FOREIGN KEY (\"id_%s\") references entry_info(id_entry_info) ON DELETE CASCADE" % table
        sql += ");"
        if db.write(sql, fk=False) is False:
            util.cti_plugin_print_error("Error while creating the schema for plugin {0} into the database.".format(plugins_info[p]["alias"]))
            raise cti_exception.Hapi_fail()
        
        #Creating linked tables
        for to_create in linked_tables_sql:
            print "\t{0}".format(to_create['msg'])
            if db.write(to_create['sql'], fk=False) is False:
                util.cti_plugin_print_error("\t\tError while {0}\n".format(to_create['msg']))
            
        if output_default["init"].attributes.has_key(cti.META_ATTRIBUTE_PRODUCED_DATA):
             if output_default["init"].attributes[cti.META_ATTRIBUTE_PRODUCED_DATA]:
                 type_plugin = "descriptive"
    
    values_plugins = [
    #Adding the processed plugin in the database's plugin table
                        p,
                        plugins_info[p]["alias"],
                        plugins_info[p]["category_uid"],
                        plugins_info[p]["desc"],
                        plugins_info[p]["repository_type"],
                        plugins_info[p]["repository_uid"],
                        type_plugin
                     ]
    values_plugins = [ prepare_value(v) for v in values_plugins ]
    sql = "INSERT INTO \"plugin\" (\"id_plugin\", \"plugin_uid\", \"alias\", \"category_uid\", \"desc\", \"repository_type\", \"repository_uid\", \"type\")\
                                                                                        VALUES (NULL, %s);" % (",".join(values_plugins))
    if db.write(sql) is False:
        util.cti_plugin_print_error("Error while writting the plugin into the database.")
        raise cti_exception.Hapi_fail()
#------------------------------------------------------------------------    

###
# Functions for searching
###

#------------------------------------------------------------------------

def count_query(constraints, db, type_e=None, name_dict={}):
    """
    Performs a search and return the number of results
    Args:
        constraints: the constraints: {"key": [(operator, value)]}
        db: the database instance
        type_e: the entry type
    Returns:
        The number of results
    """
    (constraints_source, sql_from) = generate_source(type_e)
    if constraints_source:
        constraints = {'LOGIC': 'AND', 'L': constraints_source, 'R': constraints}
    sql_add_from,sql_where = parse_constraints(constraints, name_dict, not sql_from)
    
    sql = "SELECT COUNT(DISTINCT entry_info.entry_uid) FROM %s " % sql_from
    if sql_add_from:
        sql += ' {0}'.format(sql_add_from)
    if sql_where:
        sql += ' WHERE {0}'.format(sql_where)
    sql += ";"
    
    for r in db.read(sql):
        result = int(r[0])
    
    return result

#------------------------------------------------------------------------

def search(constraints, db, type_e=None, fields=["*"], start=None, end=None, sort=[], name_dict={}):
    """
    Finds all the entries that match the search query.
    Returns an iterator with a dictionary for each matching entry
    whose keys are the given fields

    If fiels is None, all the fields are returned.
    Args:
        constraints: the constraints
        db: the database instance
        type_e: the entry type
        fields: the fields
        start: the start number
        end: the end number
        sort: the field to sort
        name_dict: a dictionnary to translate the raw name into an SQL attribute
    Return:
        the results
    """
    (constraints_source, sql_from) = generate_source(type_e)
    if constraints_source:
        constraints = {'LOGIC': 'AND', 'L': constraints_source, 'R': constraints}
    sql_add_from,sql_where = parse_constraints(constraints, name_dict, not sql_from)
    sql = "SELECT DISTINCT %s FROM %s " % (",".join(fields), sql_from)
    if sql_add_from:
        sql += ' {0}'.format(sql_add_from)
    if sql_where:
        sql += ' WHERE {0}'.format(sql_where)
    if len(sort) > 0:
        sql += " ORDER BY %s" % ",".join(sort)
    return ranged_query(sql, db, start, end)

#------------------------------------------------------------------------

def search_uids(constraints, db, type_e=None, start=None, end=None, name_dict={}):
    """
    Generates uids that match the search query
    Args:
        constraints: the constraints used to select the entries
        db: the database instance
        type_e: the entry type (loop, application, ...)
        start: the start number
        end: the end number
        name_dict: a dictionnary to translate the raw name into an SQL attribute
    Return:
        the UIDs
    """
    
    (constraints_source, sql_from) = generate_source(type_e)
    if constraints_source:
        constraints = {'LOGIC': 'AND', 'L': constraints_source, 'R': constraints}
    (sql_add_from, sql_where) = parse_constraints(constraints, name_dict, not sql_from)
    sql = "SELECT DISTINCT entry_uid FROM %s " % sql_from
    
    if sql_add_from:
        sql += ' {0}'.format(sql_add_from)
    if sql_where:
        sql += ' WHERE {0}'.format(sql_where)
    uids = set()
    result = ranged_query(sql, db, start, end)
    for r in result:
        uids.add(cti.CTI_UID(str(r[0])))
    return uids

#------------------------------------------------------------------------

def count_query_couple(list_constraints, link_params, margins, db, type_e=None, list_name_dict=[{}, {}]):
    """
    Performs a search and return the number of results
    Args:
        list_constraints: a list of two sets of constraints: {"key": [(operator, value)]}
        link_params: a list of link parameters
        margins: a list of error margin for the link parameters
        db: the database instance
        type_e: the entry type
        list_name_dict: a list of two dictionnaries to translate the raw name into an SQL attribute
    Returns:
        The number of results
    """
    list_sql = []
    for index in range(2):
        (constraints_source, sql_from) = generate_source(type_e)
        if constraints_source:
            constraints = {'LOGIC': 'AND', 'L': constraints_source, 'R': list_constraints[index]}
        (sql_add_from, sql_where) = parse_constraints(constraints, list_name_dict[index], not sql_from)
        sql = "SELECT DISTINCT entry_uid, %s FROM %s " % (", ".join(get_columns(type_e)), sql_from)
        
        if sql_add_from:
            sql += ' {0}'.format(sql_add_from)
        if sql_where:
            sql += ' WHERE {0}'.format(sql_where)
        list_sql.append(sql)
    
    sql_where = []
    index = 0
    for link_param in link_params:
        if margins[index] is None:
            sql_where.append("'{0}1'.'{1}' == '{0}2'.'{1}'".format(type_e, link_param))
        else:
            sql_where.append("'{0}1'.'{1}' <= ('{0}2'.'{1}' + {2}) AND '{0}1'.'{1}' >= ('{0}2'.'{1}' - {2})".format(type_e, link_param, margins[index]))
        index += 1
    if sql_where:
        sql_where = "WHERE %s" % " AND ".join(sql_where)
    else:
        sql_where = ""
    
    sql_from = "({1}) '{0}1', ({2}) '{0}2'".format(type_e, list_sql[0], list_sql[1])
    sql = "SELECT COUNT('{0}1'.'entry_uid') FROM {1} {2};".format(type_e, sql_from, sql_where)
    
    for r in db.read(sql):
        result = int(r[0])
    
    return result

#------------------------------------------------------------------------

def search_couple_uids(list_constraints, link_params, margins, db, type_e=None, start=None, end=None, list_name_dict=[{}, {}]):
    """
    Generates a couple of uids that match the search query
    Args:
        list_constraints: a list of two sets of constraints used to select the entries
        link_params: a list of link parameters
        margins: a list of error margin for the link parameters
        db: the database instance
        type_e: the entry type (codelet, application, ...)
        start: the start number
        end: the end number
        list_name_dict: a list of two dictionnaries to translate the raw name into an SQL attribute
    Return:
        the UIDs
    """
    
    list_sql = []
    for index in range(2):
        (constraints_source, sql_from) = generate_source(type_e)
        if constraints_source:
            constraints = {'LOGIC': 'AND', 'L': constraints_source, 'R': list_constraints[index]}
        (sql_add_from, sql_where) = parse_constraints(constraints, list_name_dict[index], not sql_from)
        sql = "SELECT DISTINCT entry_uid, %s FROM %s " % (", ".join(get_columns(type_e)), sql_from)
        
        if sql_add_from:
            sql += ' {0}'.format(sql_add_from)
        if sql_where:
            sql += ' WHERE {0}'.format(sql_where)
        list_sql.append(sql)
    
    sql_where = []
    index = 0
    for link_param in link_params:
        if margins[index] is None:
            sql_where.append("'{0}1'.'{1}' == '{0}2'.'{1}'".format(type_e, link_param))
        else:
            sql_where.append("'{0}1'.'{1}' <= ('{0}2'.'{1}' + {2}) AND '{0}1'.'{1}' >= ('{0}2'.'{1}' - {2})".format(type_e, link_param, margins[index]))
        index += 1
    if sql_where:
        sql_where = "WHERE %s" % " AND ".join(sql_where)
    else:
        sql_where = ""
    
    sql_from = "({1}) '{0}1', ({2}) '{0}2'".format(type_e, list_sql[0], list_sql[1])
    sql = "SELECT '{0}1'.'entry_uid', '{0}2'.'entry_uid' FROM {1} {2}".format(type_e, sql_from, sql_where)
    
    uids = set()
    result = ranged_query(sql, db, start, end)
    for r in result:
        uids.add((cti.CTI_UID(str(r[0])), cti.CTI_UID(str(r[1]))))
    return uids

#------------------------------------------------------------------------

#------------------------------------------------------------------------
# Useful functions
#------------------------------------------------------------------------

#------------------------------------------------------------------------

def get_columns(table):
    """Returns the list of columns for the given table.
    
        Returns:
            the list of columns for the given table
    """
    return Database().list_columns(table)

#------------------------------------------------------------------------

def update(table, values, constraints, db):
    def parse_values(values, db):
        table_values = []
        for k in values.keys():
            if util_uid.is_valid_uid(values[k]):
                values[k] = uid2id(values[k], db)
            table_values.append(" \"%s\" = \"%s\" " % (k, values[k]))
        return ",".join(table_values)
    sql = "UPDATE \"%s\" SET " % table
    sql += parse_values(values, db)
    sql_add_from,sql_where = parse_constraints(constraints)
    if sql_where:
        sql += ' WHERE {0}'.format(sql_where)
    sql += ';'
    return db.write(sql)

#------------------------------------------------------------------------

def delete(table, constraints, db):
    sql = "DELETE FROM \"%s\" " % table
    sql_add_from,sql_where = parse_constraints(constraints, {}, False)
    if sql_add_from:
        sql += ' {0}'.format(sql_add_from)
    if sql_where:
        sql += ' WHERE {0}'.format(sql_where)
    sql += ';'
    return db.write(sql)
    
#------------------------------------------------------------------------

def drop_table(table, db):
    sql = "DROP TABLE \"%s\" " % table
    return db.write(sql)
#------------------------------------------------------------------------

def insert(table, values, db):
    """Inserts a single row into the database
    Args:
        table: the table of the database to be updated
        values: the row to be inserted in the table
        db: the database
    Returns: last row id if success, False otherwise
    """

    sql = "INSERT INTO \"%s\" (" % table
    
    column = []
    v = []
    for k in values.keys():
        column.append(k)
        v.append(values[k])
        
    sql += ",".join(["\"%s\"" % x for x in column])
    sql += ") VALUES ("

    val = []
    for x in v:
        if util_uid.is_valid_uid(x):
            x_uid = uid2id(x, db)
            if x_uid is None:
                x_uid = x
            val.append("\"%s\"" % x_uid)
        else:
            val.append(prepare_value(x))
    sql += ",".join(val)
    sql += ")"

    return db.write(sql)
    
#------------------------------------------------------------------------

def insert_rows(table, rows, db):
    """Inserts several rows in the database
    Args:
        table: the table of the database to be updated
        rows: the rows to be inserted in the table. This is a list of dictionaries wherein 
        db: the database
    Returns: True if success, False otherwise

    The rows argument is a list of dictionaries formatted as follows:
        [
            (column1:row1_data1, column2:row1_data2, ..., columnN:row1_dataN),
            (column1:row2_data1, column2:row2_data2, ..., columnN:row2_dataN),
            .
            .
            .
            (column1:rowN_data1, column2:rowN_data2, ..., columnN:rowN_dataN)
        ]
    """

    #The number of rows to insert
    rows_count = len(rows)

    #SQLite can insert up to 500 rows per request.
    maximum_insert_limit = 500
    block_lower_bound = 0
    block_upper_bound = 0

    #Therefore, if there is more than 500 tuples to insert, we divide them into several blocks.
    if rows_count > maximum_insert_limit:
        block_upper_bound = maximum_insert_limit
    else:
        block_upper_bound = rows_count

    #Here is the syntax for a multiple insert in SQLite:
    #    INSERT INTO "table_name" ("column1", "column2", ..., "columnN")
    #              SELECT "row1_data1" AS "column1", "row1_data2" AS "column2", ..., "row1_dataN" AS "columnN"
    #    UNION ALL SELECT "row2_data1", "row2_data2", ..., "row2_dataN"
    #    .
    #    .
    #    .
    #    UNION ALL SELECT "rowN_data1", "rowN_data2", ..., "rowN_dataN"

    #Creating an "INSERT INTO" query for each block of tuples.
    while block_lower_bound < rows_count:
        sql = "INSERT INTO \"{0}\" (".format(table)
        sql += ", ".join("\"{0}\"".format(key) for key in rows[0].keys())
        sql += ") SELECT "
        sql += ", ".join("\"{0}\" AS \"{1}\"".format(value, key) for key, value in rows[block_lower_bound].items())

        #The row located at the block_lower_bound index has already been processed into the first loop.
        for row in rows[block_lower_bound+1:block_upper_bound]:
            sql += " UNION ALL SELECT "
            sql += ", ".join("\"{0}\"".format(value) for value in row.values())

        #Shifting the bounds to process the next block.
        if block_lower_bound + maximum_insert_limit > rows_count:
            block_lower_bound = rows_count
        else:
            block_lower_bound += maximum_insert_limit

        if block_upper_bound + maximum_insert_limit > rows_count:
            block_upper_bound = rows_count
        else:
            block_upper_bound += maximum_insert_limit

        #Aborting the operation if the query fails
        if db.write(sql) is False:
            return False

    return True

#------------------------------------------------------------------------

def list_plugins():
    """List all the plugins by using the CTI system files. Should be only used when the database IS NOT available!

    Args:
        None
    Returns:
        dictionary of plugin's data
    """
    
    path = ctr.ctr_plugin_get_common_plugin_dir()
    list_uid = [ (util_uid.CTI_UID(x, cti.CTR_ENTRY_PLUGIN), cti.COMMON_REPOSITORY) for x in os.listdir(path) if cti.cti_plugin_is_UID(x)]
    path = ctr.ctr_plugin_get_temp_plugin_dir()
    list_uid += [ (util_uid.CTI_UID(x, cti.CTR_ENTRY_PLUGIN), cti.TEMP_REPOSITORY) for x in os.listdir(path) if cti.cti_plugin_is_UID(x)]
    list_uid += [ (x, cti.LOCAL_REPOSITORY) for x in repository.get_all_uid_local(cti.CTR_PLUGIN_DIR)]
    
    plugin_dict = {}
    
    for (uid, repository_type) in list_uid:
        try:
            x = ctr.ctr_plugin_info_file_load_by_uid(uid)
        except:
            util.cti_plugin_print_error("Error while reading info file from %s\n" % (uid))
            exit(cti.CTI_PLUGIN_ERROR_CRITICAL_IO)
            
        if not x:
            cat = ''
            desc = ''
        else:
            cat = ctr.ctr_plugin_info_get_value(x, cti.PLUGIN_INFO_CATEGORY)
            desc = ctr.ctr_plugin_info_get_value(x, cti.PLUGIN_INFO_DESCRIPTION)
            ctr.ctr_plugin_info_destruct(x)
            
        repository_uid = None
        
        if repository_type == cti.LOCAL_REPOSITORY:
            repository_path = ctr.ctr_plugin_global_index_file_ctr_by_entry_uid(uid, cti.CTR_PLUGIN_DIR)
            repository_uid = str(ctr.ctr_plugin_global_index_file_get_uid_by_ctr(repository_path))
        plugin_dict[str(uid)] = {
            "category_uid": cat,
            "desc": desc,
            "alias": cti.cti_plugin_alias_plugin_get_key(uid),
            "repository_type": repository_type,
            "repository_uid": repository_uid
        }
        
    return plugin_dict

#------------------------------------------------------------------------

def uid2id(uid, db):
   sql = "SELECT id_entry_info FROM entry_info WHERE entry_uid=\"%s\";" % str(uid)
   result = None
   for r in db.read(sql):
      result = r[0]
   return result

#------------------------------------------------------------------------

def id2uid(id, db):
   sql = "SELECT entry_uid FROM entry_info WHERE id_entry_info=\"%s\";" % id
   result = None
   for r in db.read(sql):
      result = r[0]
   return result

#------------------------------------------------------------------------

def ranged_query(query, db, start=None, end=None):
    """
    Returns the SQL command for <query> that returns matches between
    <start> and <end>. If <start> or <end> is None, all matches are returned.
    Args:
        query: the query
        db: the database instance
        start: the start number
        end: the end number
    Return:
        the result
    """
    
    if start is not None and end is not None:
        end = end - start
        query += " LIMIT %s, %s" % (start, end)
        
    query += ";"
    return db.read(query)

#------------------------------------------------------------------------

def generate_source(entry_type):
    """
    Internal function which prepares the constraints and joins used for the given entry type
    Args:
        entry_type: the entry type
    Return:
        (dictionary of constraints, SQL "FROM" command
    """
    
    constraints = {}
    tables = {}
    if entry_type:
        if entry_type not in ["entry_info", "plugin", "link_table"]:
            constraints = \
            {
                "TYPE": "LEFT INNER JOIN", 
                "VAL": "id_entry_info", 
                "NAME": ["id_{0}".format(entry_type)], 
                "TABLE": entry_type,
                "TABLE_SOURCE": "entry_info",
                "ALIAS_SOURCE": "entry_info",
                "ALIAS_TABLE": entry_type
            }
        else:
            tables[entry_type] = entry_type
    else:
        tables["entry_info"] = "entry_info"
    table_list = []
    for k in tables:
        table_list.append("%s %s" % (tables[k], k))
    from_sql = ",".join(table_list)
    return (constraints, from_sql)

#------------------------------------------------------------------------

def parse_constraints(constraints, name_dict={}, empty_from=True):
    """
    Internal function which prepares the constraints in order to generate a SQL command
    Args:
        constraints: dictionary of constraints
        name_dict: a dictionnary to translate the raw name into an SQL attribute
    Return:
        the corresponding SQL 'FROM' and 'WHERE' parts of the query
    """
    
    parse_constraints.first_join = empty_from
    parse_constraints.dict_contains_operators = {'AND':'INTERSECT', 'OR':'UNION','INTERSECT':'INTERSECT', 'UNION':'UNION'}
    
    #------------------------------------------------------------------------
    
    def prepare_constraint(leaf, name, sql, name_dict, special=''):
        """
        Process a value leaf to make joins and where constraints
        Args:
            leaf : the value leaf
            name : the attribute name for the value leaf
            sql : the current joins SQL
            name_dict : a dictionnary to translate the raw name into an SQL attribute
            special : Set to the type of special subquery if needed, an empty string is given is there is nothing special.
        Return:
            A tuple: (where_constraints, updated_sql)
                where_constraint: the where constraints generated by the tree
                updated_sql: the sql parameter updated with the joins generated by the tree
        """
        
        if not leaf:
            return('',sql)
        attribute_name = name[-1]
        
        #Processing joins
        if 'JOIN' in leaf['TYPE']:
            if leaf['TYPE'] == 'LEFT INNER JOIN':
                leaf['TYPE'] = 'INNER JOIN'
            if parse_constraints.first_join:
                parse_constraints.first_join = False
                sql += " '{t_source}' '{a_source}' {join} '{t}' '{a_table}' ON '{a_table}'.'{t_attr}' = '{a_source}'.'{source_attr}'".format(
                                                                 t_source=leaf['TABLE_SOURCE'],
                                                                 join=leaf['TYPE'],
                                                                 t=leaf['TABLE'],
                                                                 a_table=leaf['ALIAS_TABLE'],
                                                                 t_attr=attribute_name,
                                                                 a_source=leaf['ALIAS_SOURCE'],
                                                                 source_attr=leaf['VAL']
                                                             )
            else:
                sql += " {join} '{t}' '{a_table}' ON '{a_table}'.'{t_attr}' = '{a_source}'.'{source_attr}'".format(
                                                                     join=leaf['TYPE'],
                                                                     t=leaf['TABLE'],
                                                                     a_table=leaf['ALIAS_TABLE'],
                                                                     t_attr=attribute_name,
                                                                     a_source=leaf['ALIAS_SOURCE'],
                                                                     source_attr=leaf['VAL']
                                                                 )
            return ('',sql)
        
        #Processing other constraints
        attribute_name = copy.copy(name_dict.get('.'.join(name), attribute_name))
        if not isinstance(attribute_name, dict): 
            attribute_name = {'ALIAS':None}
            if len(name)>1:
                attribute_name['TABLE']= name[-2]
                
        if not 'VAL' in attribute_name:
            attribute_name['VAL'] = name[-1]
        val = prepare_value(leaf['VAL'])
        #Making the contains constraint
        special_sql = ''
        if special == 'CONTAINS':
            contains_join = ''
            if attribute_name['SPECIAL']['JOIN']:
                contains_join = 'INNER JOIN "entry_info" ON "entry_info"."id_entry_info" = "{0}"'.format(attribute_name['SPECIAL']['COLUMN'])
            special_sql = 'SELECT "{column}" FROM "{table}" {join} WHERE '.format(
                                                                                table=attribute_name['TABLE'],
                                                                                column=attribute_name['SPECIAL']['COLUMN'],
                                                                                join = contains_join
                                                                            )
            attribute_name['VAL'] = attribute_name['SPECIAL']['NAME_SRC']
            attribute_name['ALIAS'] = None
        elif special == 'MATRIX':
            #TODO
            attribute_name['VAL'] = leaf['NAME'][-1]
            attribute_name['ALIAS'] = None
        
        if attribute_name['ALIAS'] is not None:
            attribute_name['VAL'] = '"{0}"."{1}"'.format(attribute_name['ALIAS'],attribute_name['VAL'])
        elif attribute_name['VAL'] is None:
            attribute_name['VAL'] = 'NULL'
        elif not (attribute_name['VAL'].startswith('"') and attribute_name['VAL'].endswith('"')):
            attribute_name['VAL'] = '"{0}"'.format(attribute_name['VAL'])
        
        #Range of values
        if leaf['TYPE'] == 'BETWEEN':
            return (special_sql + "{0} BETWEEN {1} AND {2}".format(
                            attribute_name['VAL'], 
                            val, 
                            leaf['AND']
                        ), 
                    sql)
        #List of values
        elif leaf['TYPE'] == 'IN':
            return (special_sql + "{0} IN ({1})".format(
                            attribute_name['VAL'],
                            str(list(val)).strip('[]')
                        ), 
                    sql)
        elif leaf['TYPE'] == 'LIKE':
            return (special_sql + "{0} {1} {2} ESCAPE '\\'".format(
                            attribute_name['VAL'], 
                            leaf['TYPE'], 
                            val
                        ), 
                    sql)
        
        #Common case: column comparator value
        return (special_sql + "{0} {1} {2}".format(
                        attribute_name['VAL'], 
                        leaf['TYPE'], 
                        val
                    ),
                sql)
    
    #------------------------------------------------------------------------
    
    def prepare_value(value):
        """
        Prepares a raw value for SQL
        Args:
            value: the value
        Return:
            A SQL-compatible string for the value.
        """
        if value == None:
            return "NULL"
        if isinstance(value, (str, unicode, cti.CTI_UID)):
            return "'" + str(value) + "'"
        return value
    
    #------------------------------------------------------------------------
    
    def process_value_tree(tree, name, sql, name_dict, special=''):
        """
        Process a tree of values
        Args:
            tree : the tree of values
            name : the attribute name ofr the tree of values
            sql : the current joins SQL
            name_dict : a dictionnary to translate the raw name into an SQL attribute
            special : Set to the type of special subquery if needed, an empty string is given is there is nothing special.
        Return:
            A tuple: (where_constraints, updated_sql)
                where_constraint: the where constraints generated by the leaf
                updated_sql: the sql parameter updated with the joins generated by the leaf
        """
        current = copy.copy(tree)
        
        #Defining the SQL version of the contains tag
        special_where = ''
        if 'SPECIAL' in current:
            special = current['SPECIAL']
            contains_table = name_dict.get('.'.join(name), name[-1])
            if not isinstance(contains_table, dict):
                contains_table = {'SPECIAL':{'COLUMN':'id_{0}'.format(contains_table)}, 'ALIAS': contains_table, 'TABLE':contains_table}
            special_where = '{0}.{1} IN ('.format(contains_table['ALIAS'],contains_table['SPECIAL']['COLUMN'])
            if special == 'MATRIX':
                special_where += "SELECT {0} FROM {1} WHERE ".format(contains_table['SPECIAL']['COLUMN'], contains_table['TABLE'])
                current = copy.copy(tree['SUBQUERY'])
         
        #Case where there is a subtree.
        if 'LOGIC' in current:
            #Changing AND to INTERSECT and OR to UNION in case of contains tag
            if special == 'CONTAINS':
                current['LOGIC'] = parse_constraints.dict_contains_operators[current['LOGIC']]
            
            #Processing the subtrees
            left_where, sql = process_value_tree(current['L'], name, sql, name_dict, special)
            right_where, sql = process_value_tree(current['R'], name, sql, name_dict, special)
            
            #Checking if the leaves gave any 'where' results
            if not left_where:
                final_where = right_where
            elif not right_where:
                final_where = left_where
            #If we have both, we join them using parenthesis accordingly
            else:
                where_table = [left_where, current['LOGIC'], right_where]
                if special != "CONTAINS":
                    where_table = ['('] + where_table + [')']
                final_where = ' '.join(where_table)
        #Direct value
        else:
            final_where, sql = prepare_constraint(current, name, sql, name_dict, special)
        
        #Adding the contains syntax if needed
        if special_where:
            final_where = special_where + final_where + ')'
        
        return (final_where, sql)
    
    #------------------------------------------------------------------------
    
    def process_tree(tree, sql, name_dict):
        """
        Process a tree of conditions
        Args:
            tree: the tree of conditions
            sql: the current joins SQL
            name_dict: a dictionnary to translate the raw name into an SQL attribute
        Return:
            A tuple: (where_constraints, updated_sql)
                where_constraint: the where constraints generated by the tree
                updated_sql: the sql parameter updated with the joins generated by the tree
        """
        #Empty tree for empty query ("*"):
        if not tree:
            return ('', sql)
        
        #Subtrees case
        if 'NAME' not in tree:
            left = tree['L']
            right = tree['R']
            left_where, sql = process_tree(left, sql, name_dict)
            right_where, sql = process_tree(right, sql, name_dict)
            if not left_where:
                return (right_where, sql)
            if not right_where:
                return (left_where, sql)
            return (' '.join(['(', left_where, tree['LOGIC'], right_where, ')']), sql)
        #Leaf case
        else:
            return process_value_tree(tree, tree['NAME'], sql, name_dict)
    
    #------------------------------------------------------------------------
    
    if not ('R' in constraints and 'NAME' in constraints['R'] and 'name_source' in constraints['R']['NAME']):
            parse_constraints.debug = True
    
    sql = ""
    sql_where = ""
    if constraints:
        sql_where, sql = process_tree(constraints, sql, name_dict)
    
    return (sql, sql_where)
