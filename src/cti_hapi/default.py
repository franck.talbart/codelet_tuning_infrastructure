#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

import util, entry, description, plugin, repository, util_uid, alias

import sys, os, datetime


class DefaultCommands():
    def default_help_command(self, params):
        """Default help command implementation.
        """
        
        # WARNING: IF YOU WANT TO UPDATE THE HELP COMMAND
        # PLEASE DO NOT FORGET TO UPDATE THE AUTOCOMPLETION AS WELL
        # (written in set_environement by the main Makefile)
        plugin_uid = util_uid.CTI_UID(sys.argv[2], cti.CTR_ENTRY_PLUGIN)
        alias_entry = cti.cti_plugin_alias_plugin_get_key(plugin_uid)
        if not alias_entry:
            alias_entry = plugin_uid
        try:
            cmd_name = self.work_params["help"].params["COMMAND_NAME"][cti.META_ATTRIBUTE_VALUE]
            if not cmd_name:
                print "Commands available for this plugin:"
                for cmd in self.work_params.keys():
                    params = ""
                    for p in self.work_params[cmd].params.keys():
                        if self.work_params[cmd].params[p].has_key(cti.META_ATTRIBUTE_OPTIONAL) and\
                          self.work_params[cmd].params[p][cti.META_ATTRIBUTE_OPTIONAL]:
                            p ="[%s]" % (p)
                        else:
                            p ="%s" % (p)
                            
                        params = "%s %s" % (params, p)
                    print "    $ cti %s %s%s"  % (alias_entry, cmd, params)
            else:
                # show help for particular command
                description_c = "NO DESCRIPTION"
                if cti.META_ATTRIBUTE_DESC in self.work_params[cmd_name].attributes:
                    description_c = self.work_params[cmd_name].attributes[cti.META_ATTRIBUTE_DESC]
                    description_l = self.work_params[cmd_name].attributes[cti.META_ATTRIBUTE_LONG_DESC]
                print "Description: %s\n%s\n\n" % (description_c, description_l)
                params = ""
                for p in self.work_params[cmd_name].params.keys():
                    if self.work_params[cmd_name].params[p].has_key(cti.META_ATTRIBUTE_OPTIONAL) and\
                      self.work_params[cmd_name].params[p][cti.META_ATTRIBUTE_OPTIONAL]:
                        p ="[%s]" % (p)
                    params = "%s %s" % (params, p)
                print "    $ cti %s %s%s"  % (alias_entry, cmd_name, params)
                print "Parameters:"
                for p in self.work_params[cmd_name].params.keys():
                    description_c = "NO DESCRIPTION"
                    if cti.META_ATTRIBUTE_DESC in self.work_params[cmd_name].params[p]:
                        description_c = self.work_params[cmd_name].params[p][cti.META_ATTRIBUTE_DESC]
                    p_type = "UNKNOWN TYPE"
                    if cti.META_ATTRIBUTE_TYPE in self.work_params[cmd_name].params[p]:
                        p_type = self.work_params[cmd_name].params[p][cti.META_ATTRIBUTE_TYPE]
                    if self.work_params[cmd_name].params[p].has_key(cti.META_ATTRIBUTE_OPTIONAL) and\
                      self.work_params[cmd_name].params[p][cti.META_ATTRIBUTE_OPTIONAL]:
                        p ="[%s]" % (p)
                    print "    %25s%15s %40s" % (p, p_type, description_c)
        
        except KeyError:
            print "Unknown command"
            util.hapi_error(cti.CTI_ERROR_MSG_UNEXPECTED)
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
        return 0
    
    #------------------------------------------------------------------------
    
    def default_init_command(self, params, alias_e = None, no_output = False):
        """Runs init command to initialize data.
        
        Args:
          Params -- init parameters
        Returns:
          An error code, 0 if succeeded.
        """
        
        format_o = "txt"
        if params.has_key("format"):
            format_o = params["format"]
            
        # check for local repository
        rep = self.work_params[self.command].attributes[cti.META_ATTRIBUTE_REP]
        if cti.META_ATTRIBUTE_REP_PRODUCE in params:
            rep = params[cti.META_ATTRIBUTE_REP_PRODUCE]
        elif rep == "0" or rep == cti.LOCAL_REPOSITORY:
            if not repository.local_exist():
                util.fatal(cti.CTI_ERROR_MSG_REP_DOESNT_EXISTS, cti.CTI_PLUGIN_ERROR_LOCAL_REP_DOESNT_EXISTS)
        
        self.work_params = description.description_write(self.command, self.work_params)
        data_entry = entry.create_data_entry(self.plugin_uid, rep, self.username)
        
        if data_entry is None:
            util.cti_plugin_print_error("Can't create the entry.")
            return cti.CTI_PLUGIN_ERROR_IO
        
        for def_param in self.output_params[self.command].params.keys():
            if self.work_params[self.command].params.has_key(def_param) and \
                    self.work_params[self.command].params[def_param].has_key(cti.META_ATTRIBUTE_VALUE):
                type_param = None
                if cti.META_ATTRIBUTE_TYPE in self.work_params[self.command].params[def_param]:
                    type_param = self.work_params[self.command].params[def_param][cti.META_ATTRIBUTE_TYPE]
                is_list_output = ((cti.META_ATTRIBUTE_LIST in self.output_params[self.command].params[def_param]) and\
                 str(self.output_params[self.command].params[def_param][cti.META_ATTRIBUTE_LIST]).lower() == cti.META_ATTRIBUTE_TRUE)
                if type_param == cti.META_CONTENT_ATTRIBUTE_TYPE_FILE:
                    tmp_filename = None
                    if is_list_output:
                        tmp_filename = []
                    for f in util.value_as_list(self.work_params[self.command].params[def_param]):
                        if f is not None and f != "":
                            try:
                                if os.path.isfile(f):
                                    tmp = entry.put_file_in_entry(data_entry.uid, f)
                                    if is_list_output:
                                        tmp_filename.append(tmp)
                                    else:
                                        tmp_filename = tmp
                                elif os.path.isdir(f):
                                    if is_list_output:
                                        tmp_filename += entry.put_dir_in_entry(data_entry.uid, f)
                                    else:
                                        util.fatal("Directory is not allowed for %s." % (def_param), cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                                        exit(1)
                                else:
                                    util.fatal("%s not found." % (f), cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                                    exit(1)
                            except:
                                print "Error while adding the file %s" % (f)
                                exit(1)
                    self.output_params[self.command].params[def_param][cti.META_ATTRIBUTE_VALUE] = tmp_filename
                else:
                    self.output_params[self.command].params[def_param][cti.META_ATTRIBUTE_VALUE] = \
                        self.work_params[self.command].params[def_param][cti.META_ATTRIBUTE_VALUE]
        if alias_e is None:
            if self.work_params[self.command].params.has_key("alias"):
                alias_e = self.work_params[self.command].params["alias"][cti.META_ATTRIBUTE_VALUE]
            else:
                # create the alias
                type_plugin = self.plugin_uid
                alias_type_plugin = alias.get_plugin_alias(type_plugin)
                if alias_type_plugin is not None:
                    type_plugin = alias_type_plugin
                now = datetime.datetime.now()
                date = now.strftime('%Y_%m_%d_%H:%M:%S:%f')
                alias_e = "%s_%s" % (type_plugin, date)
        
        self.output_params.record_output(self.command, data_entry.path)
        self.work_params.record_input(self.command, data_entry.path)
        
        output = plugin.plugin_exit(data_entry.entry, format_o, alias_e=alias_e)
        try:
            self.output_params.update_references(self.command, data_entry.uid)
        except:
            util.cti_plugin_print_error("Can't update the references.")
            util.cti_plugin_print_error("Unexpected error: " +  str(sys.exc_info()[0]))
        
        if no_output:
            return (data_entry, output)
        else:
            print output
            return data_entry
    
    #------------------------------------------------------------------------
    
    def default_update_command(self, params):
        """Default update command implementation
        
        Args:
          Params -- update parameters
        Returns:
          An error code, 0 if succeeded.
        """
        return description.update_entry(self, params)
#------------------------------------------------------------------------

def default_help_parser(command, argv, work_params):
    """Parser for default help command.
    
    Args:
      command: A string with current command name.
      argv: The list with command line arguments.
      work_params: A Commands dictionary with CTI working parameters.
    
    Returns:
      Updated work_params dictionary.
    """
    work_params[command] = entry.CmdNode({ cti.META_ATTRIBUTE_NAME:command, \
                                     cti.META_ATTRIBUTE_DESC:"default help command"\
                                     },\
                                   { "COMMAND_NAME":{ \
                                       cti.META_ATTRIBUTE_NAME:"COMMAND_NAME", \
                                       cti.META_ATTRIBUTE_VALUE:None, \
                                       cti.META_ATTRIBUTE_DESC:"command name" \
                                       } },)
    try:
        if len(argv) > 0:
            work_params[command].params["COMMAND_NAME"][cti.META_ATTRIBUTE_VALUE] = argv[0]
    except Exception, e:
        util.hapi_log("%s: %s" % (cti.CTI_ERROR_MSG_UNEXPECTED, str(e)))
    return work_params

#------------------------------------------------------------------------
