#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

""" User module provides functions to check user identity """

import cti

import database_manager, database

def check_password(username, password):
    """ Return true if the password is correct, false otherwise
    
    Args:
        username: the username
        password: the password
    
    Returns:
      true or false
    """
    
    if username == cti.NO_SESSION:
        print "\nIt seems that you are using CTI for the first time.\n" +\
            "Please log in by doing 'cti user login <username>' or " +\
            "ask the CTI administrator to create an account.\n\n"
        return False
    
    db = database.Database()
    uids = database_manager.search_uids(
                                      { 
                                       'L':{'NAME':["username"], 'TYPE':"=", 'VAL': username},
                                       'LOGIC':'AND',
                                       'R':{'NAME':["password"], 'TYPE':"=", 'VAL': password}
                                      },
                                      db,
                                      "user"
                                  )
    if len(uids) > 0 :
        return True
    return False

#------------------------------------------------------------------------
