#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

""" Repository module provides facilities to work with the repository hierarchy.
"""

import cti, ctr

import os

def get_repository(cmd):
    """ This function terminates the whole process if the repository type is not recognized.
    Args:
        cmd: the command array
    """
    if cti.META_ATTRIBUTE_REP in cmd.attributes.keys():
        return cmd.attributes[cti.META_ATTRIBUTE_REP]
    return None

#------------------------------------------------------------------------

def get_local_rep():
    """ This function returns the local repository directory
    """
    dot_ctr = cti.cti_plugin_config_get_value(cti.DOT_CTR)
    path_to_rep = os.path.join(os.path.abspath(os.curdir), dot_ctr)
    return path_to_rep

#------------------------------------------------------------------------

def local_exist():
    """ Checks that local repository exists.

    Returns:
      True if exists, False otherwise.
    """
    path_to_rep = get_local_rep()
    return (not ctr.ctr_plugin_check_repository(path_to_rep))
   
#------------------------------------------------------------------------

def get_all_uid_local(data_type = cti.CTR_DATA_DIR):
    """ return a list of uids from local repositories
    
    Args:
      data_type: cti.CTR_DATA_DIR, cti.CTR_PLUGIN_DIR
    
    Returns:
      the list of uids
    """
    gi_file = ctr.ctr_plugin_global_index_file_load()
    it = ctr.ctr_plugin_global_index_file_it_begin(gi_file)
    dir_data = cti.cti_plugin_config_get_value(data_type)
    while ctr.ctr_plugin_global_index_file_it_is_end(it) == False:
        dtw = ctr.ctr_plugin_global_index_file_it_value(it)
        dtw = os.path.join(dtw, dir_data)
        
        if os.path.isdir(dtw):
            try:
                for uid in os.listdir(dtw):
                    if cti.cti_plugin_is_UID(uid):
                        yield cti.CTI_UID(uid)
            except:
                # ignore directories which are not valid uid entries
                continue
        it = ctr.ctr_plugin_global_index_file_it_next(it)
        
#------------------------------------------------------------------------

def get_list_repositories():
    """ return a list of repositories
    
    Args:
      None
    
    Returns:
      the list of repositories (name, path, repository_type)
    """
    
    yield ("Common", ctr.ctr_plugin_get_common_dir(), cti.CTR_REP_COMMON)
    yield ("Temp", ctr.ctr_plugin_get_temp_dir(), cti.CTR_REP_TEMP)
    
    gi_file = ctr.ctr_plugin_global_index_file_load()
    it = ctr.ctr_plugin_global_index_file_it_begin(gi_file)
    
    while ctr.ctr_plugin_global_index_file_it_is_end(it) == False:
        path = ctr.ctr_plugin_global_index_file_it_value(it)
        uid = ctr.ctr_plugin_global_index_file_it_key(it)
        yield (uid, path, cti.CTR_REP_LOCAL)
        it = ctr.ctr_plugin_global_index_file_it_next(it)

#------------------------------------------------------------------------
