#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

"""
types module enables conversion between:
 1) string command line data
 2) json ctr_input and ctr_output data
 3) python native objects
"""

import cti, ctr

import util, util_uid

import json, os, math, datetime

def parse_float(float_data):
    """Tries to convert some data into a json-compatible float
    Args:
        float_data: the data to convert

    Returns:
        a float value, None if it wasn't compatible.
    """
    val = float(float_data)
    if math.isnan(val):
        val = None
        util.cti_plugin_print_warning("NaN float value replaced by None.\n\tOriginal value: {0}".format(float_data))
    if math.isinf(val):
        val = None
        util.cti_plugin_print_warning("INF float value replaced by None.\n\tOriginal value: {0}".format(float_data))

    return val



def from_string(arg, ptype, islist, matrix_types=None, produced_by=None):
    """
    Converts a string to a native python type
    Args:
        arg: string
        ptype: CTI input/output type
        islist: boolean
    
    Returns:
        python object
    
    """
    if islist:
        values = arg.split(",")
        if len(values) == 1 and values[0].strip() == "":
            return []
        
        return [from_string(v, ptype, False, produced_by=produced_by) for v in values]
    
    if arg.lower() == "none":
        return None
    
    if ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_DATE,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_URL,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_EMAIL]:
        return str(arg)
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_FILE:
        if arg == "":
            return None
        if not (os.path.isfile(arg) or os.path.isdir(arg)):
            #TODO: handling file/directory deletion with the "-" argument prefix. To be refactored.
            if arg[0] == '-'  and not (os.path.isfile(arg[1:]) or os.path.isdir(arg[1:])):
                raise ValueError, "INTERNAL ERROR: file or directory not found: {0}".format(arg)
        return str(arg)
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_FLOAT:
        try:
            if arg == "":
                arg = None
            else:
                arg = parse_float(arg)
        except:
            raise ValueError, "INTERNAL ERROR: invalid FLOAT value: {0}".format(arg)
        return arg
    
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
        if arg == "":
            return None
        else:
            arg = arg.lower()
            if arg == cti.META_ATTRIBUTE_TRUE:
                return True
            elif arg == cti.META_ATTRIBUTE_FALSE:
                return False
            else:
                raise ValueError, "INTERNAL ERROR: invalid BOOLEAN value: {0}".format(arg)
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_INTEGER:
        try:
            if arg == "":
                arg = None
            else:
                arg = int(arg)
        except:
            raise ValueError, "INTERNAL ERROR: invalid INTEGER value: {0}".format(arg)
        return arg
    
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_COMPLEX:
        return json.loads(arg)
    
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX:
        if(matrix_types == None):
            raise ValueError, "INTERNAL ERROR: MATRIX content types unknown for value {0}".format(arg)
        pos=0
        old_pos=0
        in_quote=False
        tab_matrix = {}
        while(pos < len(arg)):
            if(arg[pos]=='"' and (pos==0 or arg[pos-1] != '\\')):
                in_quote = not in_quote
            elif(arg[pos] == ';' and not in_quote):
                #Getting the string of lines for this column.
                arg_line_tab = arg[old_pos:pos].split(',')
                #Getting the column name
                try:
                    current_cname = arg_line_tab[0].split(':')
                    arg_line_tab[0] = current_cname[1]
                    current_cname = current_cname[0]
                except:
                    raise ValueError, "INTERNAL ERROR: NO COLUMN NAME SPECIFIED IN MATRIX COLUMN VALUES '{0}'".format(arg[old_pos:pos])
                
                #Verify column name
                if not current_cname in matrix_types:
                    raise ValueError, "INTERNAL ERROR: INVALID COLUMN NAME '{0}' IN COLUMN VALUES '{1}'".format(current_cname,arg[old_pos:pos])
                
                #Putting the values in the matrix dictionnary.
                tab_matrix[current_cname] = [from_string(v, matrix_types[current_cname], False) for v in arg_line_tab]
                
                old_pos = pos + 1
            
            pos += 1
        #Filling the blanks if needed
        value_column_size = 0
        if tab_matrix:
            value_column_size = max([len(tab_matrix[col]) for col in tab_matrix])
        for cname in matrix_types.keys():
            if cname in tab_matrix:
                if len(tab_matrix[cname]) < value_column_size:
                    tab_matrix[cname] = tab_matrix[cname] + [None] * (value_column_size - len(tab_matrix[cname]))
            else:
                tab_matrix[cname] = [None] * value_column_size
        return tab_matrix
    
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID:
        if str(arg) != "":
            result = util_uid.CTI_UID(str(arg), cti.CTR_ENTRY_PLUGIN)
            if result is None:
                raise ValueError, "UID NOT FOUND: {0}".format(arg)
            return result
        return None
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID:
        if str(arg) != "":
            result = util_uid.CTI_UID(str(arg), cti.CTR_ENTRY_REPOSITORY)
            if result is None:
                raise ValueError, "UID NOT FOUND: {0}".format(arg)
            return result
        return None
    elif ptype == cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
        if str(arg) != "":
            result = util_uid.CTI_UID(str(arg))
            if result is None:
                raise ValueError, "DATA_UID NOT FOUND: {0}".format(arg)
            
            info_file = ctr.ctr_plugin_info_file_load_by_uid(result)
            if info_file is None:
                raise ValueError, "INTERNAL ERROR: DATA_UID NOT FOUND: {0}".format(arg)
            
            plugin_uid = ctr.ctr_plugin_info_get_value(info_file, cti.DATA_INFO_PLUGIN_UID)
            if plugin_uid is None or (produced_by is not None and str(plugin_uid) != str(produced_by)):
                raise ValueError, "Wrong plugin type for parameter {0}.\n".format(arg)
            return result
        return None
    else:
        raise Exception("INTERNAL ERROR: Unknown type {0}".format(ptype))

#------------------------------------------------------------------------

def from_json(arg, ptype, islist, matrix_types=None):
    """
    Converts a json object to a native python type
    Args:
        arg: the json object to convert
        ptype: CTI input/output type
        islist: boolean
    
    Returns:
        python object
    
    """
    if arg is None:
        return None
    if islist:
        if type(arg) != type([]) and arg is not None:
            util.fatal("Provided value '{0}' is not of the type list but of the '{1}'.".
                  format(arg, type(arg)),
                  cti.CTI_PLUGIN_ERROR_UNEXPECTED)
        return [from_json(v, ptype, False) for v in arg]
    
    if ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_DATE,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_URL,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_FILE,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_EMAIL]:
        return str(arg)
    elif ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_FLOAT]:
        if (type(arg) in [str, unicode]):
            try:
                # necessary if the field is empty within CTS
                if arg == "":
                    arg = None
                else:
                    arg = parse_float(arg)
            except:
                raise Exception("INTERNAL ERROR: invalid FLOAT value: {0}".format(arg))
        return arg
    
    elif ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN]:
        if not isinstance(arg, bool):
            raise Exception("INTERNAL ERROR: invalid BOOLEAN value: %s, type:%s" %(arg, type(arg)))
        return arg
    
    elif ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_INTEGER]:
        if (type(arg) in [str, unicode]):
            try:
                # necessary if the field is empty within CTS
                if arg == "":
                    arg = None
                else:
                    arg = int(arg)
            except:
                raise Exception("INTERNAL ERROR: invalid INTEGER value: {0}".format(arg))
        return arg
    
    elif ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_COMPLEX]:
        return arg
    
    elif ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX]:
        if(matrix_types == None):
            raise Exception("INTERNAL ERROR: MATRIX content types unknown for value '{0}'".format(arg))
        if type(arg) != dict:
            raise Exception("INTERNAL ERROR: invalid MATRIX value '{0}'".format(arg))
        
        tab_matrix = {}
        
        for column_name in arg:
            if column_name not in matrix_types:
                raise ValueError, "INTERNAL ERROR: INVALID COLUMN NAME '{0}'. Accepted columns are '{1}'".format(column_name,matrix_types.keys())
            tab_matrix[column_name] = [from_json(elt, matrix_types[column_name], False) for elt in arg[column_name]]
            
        #Filling the blanks if needed
        value_column_size = 0
        if tab_matrix:
            value_column_size = max([len(tab_matrix[col]) for col in tab_matrix])
        for cname in matrix_types.keys():
            if cname in tab_matrix:
                if len(tab_matrix[cname]) < value_column_size:
                    tab_matrix[cname] = tab_matrix[cname] + [None] * (value_column_size - len(tab_matrix[cname]))
            else:
                tab_matrix[cname] = [None] * value_column_size
        return tab_matrix
    
    elif ptype in [cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID,
                   cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID,
                   cti.META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID]:
        try:
            if str(arg) != "":
                type_struct = {
                 cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID: cti.CTR_ENTRY_DATA,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID: cti.CTR_ENTRY_PLUGIN,
                 cti.META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID: cti.CTR_ENTRY_REPOSITORY,
                }
                return util_uid.CTI_UID(str(arg), type_struct[ptype])
            return None
        except KeyError:
            return arg
        
    else:
        raise Exception("INTERNAL ERROR: Type <{0}>not found"
                        .format(ptype))

#------------------------------------------------------------------------

def from_csv(val, type=None):
    """Description
        Checks the type of the given string value from a csv, and returns a tuple containing the type and the converted value.
        Greatly inspired by: http://stackoverflow.com/questions/2103071/determine-the-type-of-a-value-which-is-represented-as-string-in-python
    Args:
        val: a string value to check
    
    Returns:
        A tuple (type, value), type being the CTI type, and value being the val argument converted into said type.
    """
    
    #Defining functions for special CTI types, and the default string types (TEXT, URL, EMAIL, etc)
    
    #-----------------------------------------------------------
    def date_parse(val):
        datetime.datetime.strptime(val, "%Y/%m/%d %H:%M:%S")
        return val
    #-----------------------------------------------------------
    def default_parse_text(val):
        return val
    #-----------------------------------------------------------
    def bool_parse(val):
        val = val.lower()
        if val == cti.META_ATTRIBUTE_FALSE:
            return False
        elif val == cti.META_ATTRIBUTE_TRUE:
            return True
        raise ValueError("%s is not a BOOLEAN" % val)
    #-----------------------------------------------------------
    def uid_parse(val):
        if cti.is_UID(val):
            return val
        raise ValueError("Value %s is not a valid UID.\n" %(val))
    #-----------------------------------------------------------
    def unsupported_parse(val):
        raise TypeError("Unsuported type for csv importation.\n")
    #-----------------------------------------------------------
    
    #Creating a dictionnary of CTI types that can be found in CSVs and the functions used for conversion of str->type
    types_convert = \
    [
        (cti.META_CONTENT_ATTRIBUTE_TYPE_DATE, date_parse),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_INTEGER, int),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_FLOAT, parse_float),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID, uid_parse),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID, uid_parse),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID, uid_parse),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN, bool_parse),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT, default_parse_text),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_EMAIL, default_parse_text),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_URL, default_parse_text),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_FILE, default_parse_text),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_MATRIX, unsupported_parse),
        (cti.META_CONTENT_ATTRIBUTE_TYPE_COMPLEX, unsupported_parse),
    ]
    
    #Stripping eventual leading/trailing spaces from the csv-extracted value
    if val is not None:
        val = val.strip()
    if val in ['n/a', 'N/A', 'NA', 'na', None]:
        return ("None", None)
    if type is not None:
        #TODO : add all the other cti types in this case.
        for cti_type, funct in types_convert:
            if type == cti_type:
                try:
                    convert = funct(val)
                    return (type, convert)
                except ValueError:
                    util.fatal("Wrong type for value %s: %s.\n" %(val, type), cti.CTI_ERROR_INVALID_ARGUMENT)
                except TypeError as t:
                    util.fatal("Exception occured using type {0} and value {1}:\n\t{2}".format(type, repr(val), repr(t)), cti.CTI_ERROR_INVALID_ARGUMENT)
        #The type was not found.
        util.fatal("Unsupported type for csv importation: %s.\n" %(type), cti.CTI_ERROR_INVALID_ARGUMENT)
        
    for cti_type, funct in types_convert:
        try:
            convert = funct(val)
            return (cti_type, convert)
        except:
            continue
    # No match, should never happen.
    return (cti.META_CONTENT_ATTRIBUTE_TYPE_TEXT, val)

#------------------------------------------------------------------------

def marshall(obj):
    out = obj
    if isinstance(obj, list):
        out = map(marshall, obj)
    elif isinstance(obj, dict):
        out = {}
        for k, v in obj.iteritems():
            out[marshall(k)] = marshall(v)
    elif isinstance(obj, cti.CTI_UID):
        out = str(obj)
    return out

#------------------------------------------------------------------------

def dump_to_json(obj):
    return json.dumps(marshall(obj))

#------------------------------------------------------------------------
