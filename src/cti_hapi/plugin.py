#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti, ctr

import util, types, util_uid, cti_json, database, database_manager, alias

import re, subprocess, os, sys, tempfile

# Start dirty hack.http://reinout.vanrees.org/weblog/2009/08/14/readline-invisible-character-hack.html
os.environ['TERM'] = 'linux'

""" Plugin module provides facilities to manipulate with plugins:
    1. Execute plugin with/without getting output.
    2. Parse output to get UID.
"""

#------------------------------------------------------------------------

def execute_plugin_by_file(plugin_uid,
                           json_c,
                           username=None,
                           password=None,
                           interactive=False):
    """Execute CTI plugin with an input file.  It raises an exception if the plugin fails.
    
    Args:
      plugin_uid: CTI_UID.
      json: the content of the input file
    
    Returns:
      The result of command function.
    """
    
    #------------------------------------------------------------------------
    def convert_uid_to_str(v):
        """ Convert UID to string
            Args:
                v: the uid
            Returns:
                the string
        """
        if (isinstance(v, cti.CTI_UID)):
            return str(v)
        else:
            return v
    #------------------------------------------------------------------------
        
    plugin_uid = str(plugin_uid).strip()
    
    for c in json_c:
        for p in json_c[c]["params"]:
            if (isinstance(p[cti.META_ATTRIBUTE_VALUE], list)):
                p[cti.META_ATTRIBUTE_VALUE] = [convert_uid_to_str(v) for v in p[cti.META_ATTRIBUTE_VALUE]]
            else:
                p[cti.META_ATTRIBUTE_VALUE] = convert_uid_to_str(p[cti.META_ATTRIBUTE_VALUE])
    f = tempfile.NamedTemporaryFile(mode='w+', delete=False)
    f.write(types.dump_to_json(json_c))
    f.flush()
    rc = None
    try:
        cmd_login = []
        if username is not None:
            cmd_login = ["--user=%s" % username, password]
            
        cmd = ["cti"] + cmd_login + [plugin_uid, "@%s"%f.name]
        sys.stdout.flush()
        if not interactive:
            child = subprocess.Popen(cmd,
                                      shell=False,
                                      stdout=subprocess.PIPE)
        else:
            child = subprocess.Popen(cmd,
                                     shell=False)
            
        output = child.communicate()
        
        rc = child.returncode
        if rc != 0 and rc != None:
            raise Exception("The plugin " + 
                            plugin_uid + 
                            " encountered the error #" + 
                            str(rc) +
                            " Output: " + 
                            str(output))
        
        os.remove(f.name)
        
    except OSError:
        util.hapi_log("Failed to execute plugin.")
        return rc
    except KeyboardInterrupt:
        util.hapi_log("Process being interrupted by the user.")
        return None
    return output[0]

#------------------------------------------------------------------------

def get_output_data_uid(output, data_uid_prefix = "DATA_CTI_UID="):
    """ Return UID from plugin output.
    Args:
      output: string -- plugins output
    Returns:
      CTI_UID -- extracted uid, None if failed.
    """
    
    uid_re = ("([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-"
              "[0-9a-f]{12})")
    
    uid = None
    m = re.search(data_uid_prefix+uid_re, output)
    if m: uid = m.group(1)
    
    if not uid:
        util.hapi_log("Can't parse output: #%s#" % output)
        return None
    
    try:
        uid = util_uid.CTI_UID(uid)
    except KeyError:
        util.hapi_log("Wrong UID: #%s#" % uid)
        return None
    
    return uid

#------------------------------------------------------------------------

def plugin_exit(data_entry, format_o="txt", code=0, note=" ", alias_e=None):
    """Writes the information on plugins exit.
    
    Args:
      data_entry: cti_pair data structure, where
                           data_entry.key   -- UID
                           data_entry.value -- path
      code: return code
      note: just a note
    """
    
    if data_entry:
        ctr.ctr_plugin_info_append_data_info_file(code, str(data_entry))
        db = database.Database()
        if not database_manager.index(cti.CTI_UID(data_entry.key), db):
            util.cti_plugin_print_error("\n\nSomething wrong happened :-( Exiting...\n")
            exit(1)
            
        
        if alias_e:
            res_alias = alias.set_data_alias(cti.CTI_UID(data_entry.key), alias_e)
        if format_o == "json":
                uid = data_entry.key
                result = {
                    'new_uid': uid,
                    'type': cti.META_CONTENT_ATTRIBUTE_TYPE_DATA_UID,
                    'note': note
                }
                return cti_json.cti_json_encode(result)
        elif format_o == "txt":
            result =  "DATA_CTI_UID=%s\n" % data_entry.key
            if alias_e:
                if res_alias == 0:
                    result += "Can't use alias %s (already used?)" % alias_e
                else:
                    result += "Alias is %s\n" % alias_e.strip().lower().replace(" ", "_")
            result += "%s\n" % note
            return result
        else:
            util.cti_plugin_print_error("Unknown format " + str(format_o))
            return cti.CTI_PLUGIN_ERROR_UNKNOWN_FORMAT
    else:
        util.hapi_log("Passed None as a data_entry. "
                      "Possibly an error occurred during "
                      "data entry creation.")

#------------------------------------------------------------------------
def plugin_auto_run(self, data_entry, format_o, auto_run, output = ""):
    """Auto run the plugin
    
    Args:
        self: self class of the plugin
        data_entry: the data entry
        format_o: the format: txt/json/etc...
        auto_run: true: run the experiment, false otherwhise
        output: output of plugin_exit
      code: return code
    """
    if auto_run:
        json = \
            {
                "run":
                {
                    "attributes" : {cti.META_ATTRIBUTE_NAME : "run"},
                    "params": 
                    [
                        {cti.META_ATTRIBUTE_NAME : "entry",cti.META_ATTRIBUTE_VALUE : data_entry.uid},
                        {cti.META_ATTRIBUTE_NAME : "format",cti.META_ATTRIBUTE_VALUE : format_o}
                    ]
                }
            }
        if format_o == "txt":
            print output
        try:
            result = execute_plugin_by_file(self.plugin_uid, json, self.username, self.password, interactive=True)
        except :
            print sys.exc_info()[1]
            return cti.CTI_PLUGIN_ERROR_UNEXPECTED
    else:
        print output
        return 0
        
    return result

#------------------------------------------------------------------------

