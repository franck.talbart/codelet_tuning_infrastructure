#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

""" Modules provide handfull functions for managing the submit modes.
"""

import cti

import alias, util, entry

import os, subprocess, datetime, tarfile, shutil, re, time, stat, getpass

# constants
MODE_AVAIL = ["local", "ssh", "slurm", "other"]     # the first element must be the 'local' mode
EXECUTE_SCRIPT_NAME = "execute_script.sh"
ALLOC_SCRIPT_NAME = "alloc_script.sh"
CHECK_FILE = "cti_check_file"
CENTRAL_NODE = cti.cti_plugin_config_get_value(cti.CTI_CENTRAL_NODE)
WORKING_DIR = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_WORKING_DIR)
DISTANT_WORKING_DIR = cti.cti_plugin_config_get_value(cti.CTI_PLUGIN_DISTANT_WORKING_DIR)
DAEMON_LOG = "daemon_log.out"

#------------------------------------------------------------------------

def print_and_launch_cmd(cmd, message=""):
    """ Launch the command.
        Args:
            cmd: the command
            message: the message to print
        
        Returns:
            a tuple containing the printed message, the subprocess outputs (stdou and stderr) and the subprocess return code
    """
    if message:
        date = datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        message = "[%s] %s" % (date, message)
        print message
    
    child = subprocess.Popen(cmd.split(" "), shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = child.communicate()
    rc = child.returncode
    if rc != 0:
        util.cti_plugin_print_error("ERROR: The command %s encountered the error #%s\nOutput: %s" % (cmd, rc, output))
    return (message, output, rc)

#------------------------------------------------------------------------

def local_mode(job_dir, dict_opt):
    """ TODO
        Args:
            
        Returns:
            message daemon
    """
    
    # copy the plugin execute script
    shutil.copyfile(dict_opt["plugin_execute_script"], os.path.basename(dict_opt["plugin_execute_script"]))
    
    plugin_execute_script_path = os.path.basename(dict_opt["plugin_execute_script"])
    os.chmod(plugin_execute_script_path, stat.S_IXUSR | stat.S_IRUSR)
    
    os.chdir(job_dir)
    cmd = "../%s" % plugin_execute_script_path
    (_, output, _) = print_and_launch_cmd(cmd, "Launching job")
    (std_out, std_err) = output
    print std_out
    print std_err
    execute_log = open("execute.out", "w")
    execute_log.write("%s\n%s" % (std_out, std_err))
    execute_log.close()
    os.chdir(os.pardir)
    return ""

#------------------------------------------------------------------------

def ssh_mode(job_dir, dict_opt):
    """ TODO
        Args:
            
        Returns:
            message daemon
    """
    
    message_daemon = ""
    
    # compress the job directory
    job_tar_name = job_dir + ".tar.gz"
    tar_file = tarfile.open(job_tar_name, "w:gz")
    tar_file.add(job_dir)
    tar_file.close()
    cluster_ssh = "%s@%s" % (dict_opt["submitter_user"], dict_opt["cluster"])
    plugin_execute_script_path = os.path.basename(dict_opt["plugin_execute_script"])
    
    cmd = "ssh -n %s mkdir -p %s/%s_cluster" % (cluster_ssh, DISTANT_WORKING_DIR, job_dir)
    (message, _ , _) = print_and_launch_cmd(cmd, "Create working directory")
    message_daemon += message + "\n"
    
    cmd = "scp {0} {1}:{2}/{3}_cluster".format(dict_opt["plugin_execute_script"], cluster_ssh, DISTANT_WORKING_DIR, job_dir)
    (message, _ , _) = print_and_launch_cmd(cmd, "Copying script onto the cluster")
    message_daemon += message + "\n"
    
    cmd = "scp {0} {1}:{2}/{3}_cluster".format(job_tar_name, 
                                                cluster_ssh, 
                                                DISTANT_WORKING_DIR,
                                                job_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Sending tar")
    message_daemon += message + "\n"
    
    cmd = "ssh -n {1} tar -zxf {2}/{0}_cluster/{3} -C {2}/{0}_cluster/".format(job_dir, 
                                                                               cluster_ssh, 
                                                                               DISTANT_WORKING_DIR,
                                                                               job_tar_name)
    (message, _, _) = print_and_launch_cmd(cmd, "Untar the archive")
    message_daemon += message + "\n"
    
    cmd = "ssh -n {1} rm -f {2}/{0}_cluster/{3}".format(job_dir, 
                                                       cluster_ssh, 
                                                       DISTANT_WORKING_DIR, 
                                                       job_tar_name)
    (message, _, _) = print_and_launch_cmd(cmd, "Removing tar (local and distant)")
    message_daemon += message + "\n"
    os.remove(job_tar_name)
    
    cmd = "ssh -n {1} source /etc/profile && cd {2}/{0}_cluster/{0} && ../{3} 2>&1 | tee -a execution.out".format(job_dir, 
                                                                           cluster_ssh, 
                                                                           DISTANT_WORKING_DIR, 
                                                                           plugin_execute_script_path)
    (message, output, _) = print_and_launch_cmd(cmd, "Launching execution")
    message_daemon += message + "\n"
    (std_out, std_err) = output
    print std_out
    print std_err
    execution_log = open("execution.out", "w")
    execution_log.write("%s\n%s" % (std_out, std_err))
    execution_log.close()
    
    cmd = "ssh -n {1} cd {2}/{0}_cluster && tar -acf {3} {0}".format(job_dir, 
                                                       cluster_ssh, 
                                                       DISTANT_WORKING_DIR, 
                                                       job_tar_name)
    (message, _, _) = print_and_launch_cmd(cmd, "Zipping and getting files...")
    message_daemon += message + "\n"
    
    cmd = "scp {0}:{1}/{2}_cluster/{3} .".format(cluster_ssh, DISTANT_WORKING_DIR, job_dir, job_tar_name)
    (_, _, _) = print_and_launch_cmd(cmd)
    
    cmd = "ssh -n {0} rm -rf {1}/{2}_cluster".format(cluster_ssh, DISTANT_WORKING_DIR, job_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Cleaning cluster")
    message_daemon += message + "\n"
    return message_daemon

#------------------------------------------------------------------------

def slurm_mode(job_dir, dict_opt):
    """ TODO
        Args:
            
        Returns:
            message daemon
    """
    
    message_daemon = ""
    
    # compress the job directory
    job_tar_name = job_dir + ".tar.gz"
    tar_file = tarfile.open(job_tar_name, "w:gz")
    tar_file.add(job_dir)
    tar_file.close()
    
    central_node_ssh = "%s@%s" % (dict_opt["submitter_user"], CENTRAL_NODE)
    cluster_ssh = "%s@%s" % (dict_opt["submitter_user"], dict_opt["cluster"])
    plugin_execute_script_path = os.path.basename(dict_opt["plugin_execute_script"])
    
    cluster_state_cmd = "ssh -n {0} sinfo -h -n {1} -o \"\%t\" |grep \"down\" |wc -l".format(central_node_ssh, dict_opt["cluster"])
    (_, output, rc) = print_and_launch_cmd(cluster_state_cmd)
    if rc != 0:
        util.cti_plugin_print_error("The central node '%s' is not kwnow. Please update the 'CTI_CENTRAL_NODE' field in the configuration file %s/cfg/config.txt" % (CENTRAL_NODE, os.environ["CTI_ROOT"]))
        return cti.CTI_ERROR_UNEXPECTED
    cluster_state = output[0]   # get the input
    if int(cluster_state) == 1:
        util.cti_plugin_print_error("The cluster \"%s\" is currently down." % dict_opt["cluster"])
        return cti.CTI_ERROR_UNEXPECTED
    
    # create allocation script
    f_alloc = open(ALLOC_SCRIPT_NAME, "w")
    f_alloc.write("#!/bin/sh \nwhile [ 1 ]; do \n\tsleep 1000 \ndone")
    f_alloc.close()
    os.chmod(ALLOC_SCRIPT_NAME, stat.S_IXUSR | stat.S_IRUSR)

    cmd = "ssh -n %s mkdir -p /tmp/%s_central" % (central_node_ssh, job_dir)
    (message, _ , _) = print_and_launch_cmd(cmd, "Create working directory")
    message_daemon += message + "\n"
    
    cmd = "scp %s %s:/tmp/%s_central" % (dict_opt["plugin_execute_script"], central_node_ssh, job_dir)
    (message, _ , _) = print_and_launch_cmd(cmd, "Copying script onto the central node")
    message_daemon += message + "\n"
    
    cmd = "scp %s %s:/tmp/%s_central" % (ALLOC_SCRIPT_NAME, central_node_ssh, job_dir)
    (message, _ , _) = print_and_launch_cmd(cmd, "Copying allocation script onto the central node")
    message_daemon += message + "\n"
    os.remove(ALLOC_SCRIPT_NAME)
    
    # allocating access to a distant machine
    alloc_command = "sbatch -w %s -e /dev/null -o /dev/null" % dict_opt["cluster"]
    if dict_opt["partition"] and dict_opt["partition"] != "":
        alloc_command = alloc_command + " -p " + dict_opt["partition"]
    alloc_command = "ssh -n %s %s /tmp/%s_central/%s" % (central_node_ssh, alloc_command, job_dir, ALLOC_SCRIPT_NAME)
    (message, output_alloc, _) = print_and_launch_cmd(alloc_command, "First alloc")
    message_daemon += message + "\n"
    
    
    # waiting for job allocation
    date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
    message = "[%s] Waiting for job number to be assigned..." % date
    print message
    message_daemon += message + "\n"
    
    job_id = ""
    while job_id == "":
        search_alloc = re.search("[0-9]+", output_alloc[0])
        if search_alloc is not None:
            job_id = search_alloc.group()
        time.sleep(3)
    
    cmd = "ssh -n %s while [[ `squeue -a --jobs=%s --noheader |grep \"%s\" | wc -l` -eq 0 ]]; do sleep 3; done" % (central_node_ssh, job_id, dict_opt["cluster"])
    (message, _, _) = print_and_launch_cmd(cmd, "Waiting for allocation...")
    message_daemon += message + "\n"
    
    # sending data
    cmd = "ssh -n %s mkdir -p %s/%s_cluster" % (cluster_ssh, DISTANT_WORKING_DIR, job_dir)
    (_, output, rc) = print_and_launch_cmd(cmd)
    
    while rc != 0:
        print output[0]
        print output[1]
        message_daemon += output[0]
        message_daemon += output[1]
        time.sleep(5)
        
        (message, output, rc) = print_and_launch_cmd(cmd, "The cluster is currently not available, waiting...")
        message_daemon += message + "\n"
    
    cmd = "scp {0} {1}:{2}/{3}_cluster".format(job_tar_name, cluster_ssh, DISTANT_WORKING_DIR, job_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Sending tar")
    message_daemon += message + "\n"
    
    
    cmd = "ssh -n {0} tar -zxf {1}/{2}_cluster/{3} -C {1}/{2}_cluster".format(cluster_ssh, DISTANT_WORKING_DIR, job_dir, job_tar_name)
    (message, _, _) = print_and_launch_cmd(cmd, "Untar the archive")
    message_daemon += message + "\n"
    
    cmd = "ssh -n {0} rm -f {1}/{2}_cluster/{3}".format(cluster_ssh, DISTANT_WORKING_DIR, job_dir, job_tar_name)
    (message, _, _) = print_and_launch_cmd(cmd, "Removing tar (local and distant)")
    message_daemon += message + "\n"
    os.remove(job_tar_name)
    
    cmd = "ssh -n %s scancel %s" % (central_node_ssh, job_id)
    (message, _, _) = print_and_launch_cmd(cmd, "Closing first allocation")
    message_daemon += message + "\n"
    
    # preparations complete. Launching job.
    cmd = "sbatch -D {0}/{1}_cluster/{1} -w {2}".format(DISTANT_WORKING_DIR, job_dir, dict_opt["cluster"])
    if dict_opt["partition"] and dict_opt["partition"] != "":
        cmd += " -p " + dict_opt["partition"]
    cmd += " /tmp/%s_central/%s" % (job_dir, plugin_execute_script_path)
    print cmd
    cmd = "ssh -n %s %s" % (central_node_ssh, cmd)
    (message, output_exec, _) = print_and_launch_cmd(cmd, "Launching job")
    message_daemon += message + "\n"
    
    # waiting for job allocation
    date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
    message = "[%s] Waiting for job number to be assigned..." % date
    print message
    message_daemon += message + "\n"
    
    job_id = ""
    while job_id == "":
        search_alloc = re.search("[0-9]+", output_exec[0])
        if search_alloc is not None:
            job_id = search_alloc.group()
        time.sleep(3)
    
    # waiting for job to terminate
    cmd = "ssh -n %s while [[ `squeue -a --noheader | grep \"%s\" | wc -l` -ne 0 ]]; do sleep 10; done" % (central_node_ssh, job_id)
    (message, _, _) = print_and_launch_cmd(cmd, "Waiting for termination...")
    message_daemon += message + "\n"
    
    
    # job is done, we need to reopen a connection so that we can get the results and clean the copied files.
    cmd = "sbatch -w %s -e /dev/null -o /dev/null" % dict_opt["cluster"]
    if dict_opt["partition"] and dict_opt["partition"] != "":
        cmd += " -p " + dict_opt["partition"]
    cmd += " /tmp/%s_central/alloc_script.sh" % job_dir
    print cmd
    cmd = "ssh -n %s %s" % (central_node_ssh, cmd)
    (message, output_alloc, _) = print_and_launch_cmd(cmd, "Final alloc")
    message_daemon += message + "\n"
    
    # waiting for job allocation
    date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
    message = "[%s] Waiting for job number to be assigned..." % date
    print message
    message_daemon += message + "\n"
    
    job_id = ""
    while job_id == "":
        search_alloc = re.search("[0-9]+", output_alloc[0])
        if search_alloc is not None:
            job_id = search_alloc.group()
        time.sleep(3) #we don't want to keep prodding the file
    
    cmd = "ssh -n %s while [[ `squeue -a --jobs=%s --noheader |grep \"%s\" | wc -l` -eq 0 ]]; do sleep 3; done" % (central_node_ssh, job_id, dict_opt["cluster"])
    (message, _, _) = print_and_launch_cmd(cmd, "Waiting for allocation...")
    message_daemon += message + "\n"
    
    cmd = "ssh -n {0} cd {1}/{2}_cluster && tar -acf {3} {2}".format(cluster_ssh, DISTANT_WORKING_DIR, job_dir, job_tar_name)
    (message, _, _) = print_and_launch_cmd(cmd, "Zipping and getting files...")
    message_daemon += message + "\n"
    
    cmd = "scp %s:%s/%s_cluster/%s ." % (cluster_ssh, DISTANT_WORKING_DIR, job_dir, job_tar_name)
    (_, _, _) = print_and_launch_cmd(cmd)
    
    cmd = "ssh -n %s rm -rf %s/%s_cluster" % (cluster_ssh, DISTANT_WORKING_DIR, job_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Cleaning cluster")
    message_daemon += message + "\n"
    
    cmd = "ssh -n %s scancel %s" % (central_node_ssh, job_id)
    (message, _, _) = print_and_launch_cmd(cmd, "Closing allocation.")
    message_daemon += message + "\n"
    
    cmd = "ssh -n %s rm -rf /tmp/%s_central" % (central_node_ssh, job_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Cleaning central node.")
    message_daemon += message + "\n"
    return message_daemon

#------------------------------------------------------------------------

def other_mode(job_dir, dict_opt):
    """ TODO
        Args:
            
        Returns:
            message daemon
    """
    
    message_daemon = ""
    shared_working_dir = os.path.join(DISTANT_WORKING_DIR, job_dir + "_shared")
    
    # compress the job directory
    job_tar_name = job_dir + ".tar.gz"
    tar_file = tarfile.open(job_tar_name, "w:gz")
    tar_file.add(job_dir)
    tar_file.close()
    
    # create the execute script
    execute_script_path = os.path.join(os.curdir, EXECUTE_SCRIPT_NAME)
    execute_script_file = open(execute_script_path, "w")
    execute_script_file.write("#!/bin/bash\n cd %s\n" % (os.path.join(shared_working_dir, job_dir)))
    
    ## add the prefix script
    if dict_opt["prefix_execute_script"]:
        entry.get_file_from_entry(dict_opt["entry"], dict_opt["prefix_execute_script"])
        if os.path.isfile(dict_opt["prefix_execute_script"]):
            prefix_file = open(dict_opt["prefix_execute_script"])
            execute_script_file.write(prefix_file.read())
            prefix_file.close()
        else:
            util.cti_plugin_print_error("ERROR: The prefix script file '%s' does not exist!\n" % dict_opt["prefix_execute_script"])
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    
    ## add the plugin execute script
    if dict_opt["plugin_execute_script"]:
        execute_script_file.write("../%s\n" % os.path.basename(dict_opt["plugin_execute_script"]))
    else:
        util.cti_plugin_print_error("The plugin execute script is missing!\n")
        exit(cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
    
    ## add the suffix script
    if dict_opt["suffix_execute_script"]:
        entry.get_file_from_entry(dict_opt["entry"], dict_opt["suffix_execute_script"])
        if os.path.isfile(dict_opt["suffix_execute_script"]):
            suffix_file = open(dict_opt["suffix_execute_script"])
            execute_script_file.write(suffix_file.read())
            suffix_file.close()
        else:
            util.cti_plugin_print_error("ERROR: The suffix script file '%s' does not exist!\n" % dict_opt["suffix_execute_script"])
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    
    ## add the check file
    execute_script_file.write("touch ../%s" % CHECK_FILE)
    execute_script_file.close()
    os.chmod(execute_script_path, stat.S_IXUSR | stat.S_IRUSR)
    
    central_node_ssh = "%s@%s" % (dict_opt["submitter_user"], CENTRAL_NODE)
    
    # create working directory on the central node and copy scripts
    cmd = "ssh %s mkdir -p %s" % (central_node_ssh, shared_working_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Create working directory")
    message_daemon += message + "\n"
    
    cmd = "scp %s %s:%s" % (dict_opt["plugin_execute_script"], central_node_ssh, shared_working_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Copying execute script onto the central node")
    message_daemon += message + "\n"

    cmd = "scp %s %s:%s" % (execute_script_path, central_node_ssh, shared_working_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Copying plugin execute script onto the central node")
    message_daemon += message + "\n"
    
    # copy data on the central node
    cmd = "scp %s %s:%s" % (job_tar_name, central_node_ssh, shared_working_dir)
    (message, _, _) = print_and_launch_cmd(cmd, "Sending tar")
    message_daemon += message + "\n"
    os.remove(job_tar_name)
    
    # untar and remove the archive on the central node
    cluster_job_tar_name = os.path.join(shared_working_dir, job_tar_name)
    cmd = "ssh %s tar -zxf %s -C %s" % (central_node_ssh, cluster_job_tar_name, shared_working_dir)
    (_, _, _) = print_and_launch_cmd(cmd)
    cmd = "ssh %s rm -f %s" % (central_node_ssh, cluster_job_tar_name)
    (_, _, _) = print_and_launch_cmd(cmd)
    
    # launch the experiment
    cmd = "ssh -n %s %s %s %s" % (central_node_ssh, dict_opt["submitter_cmd"], dict_opt["submitter_opt"], os.path.join(shared_working_dir, EXECUTE_SCRIPT_NAME))
    (_, _, _) = print_and_launch_cmd(cmd)
    os.remove(execute_script_path)
    
    # waiting for job to terminate
    cmd = "ssh -n %s while [ ! -e %s ]; do sleep 10; done" % (central_node_ssh, os.path.join(shared_working_dir, CHECK_FILE))
    (_, _, _) = print_and_launch_cmd(cmd)
    
    # zipping results
    cmd = "ssh -n %s cd %s; tar -acf %s %s" % (central_node_ssh, shared_working_dir, job_tar_name, job_dir)
    (_, _, _) = print_and_launch_cmd(cmd)
    
    # get back results
    cmd = "scp %s:%s %s" % (central_node_ssh, cluster_job_tar_name, os.curdir)
    (_, _, _) = print_and_launch_cmd(cmd)
    
    # clean cluster
    cmd = "ssh -n %s rm -rf %s" % (central_node_ssh, shared_working_dir)
    
    return message_daemon

#------------------------------------------------------------------------

SUBMITTER_MODE = {MODE_AVAIL[0]: local_mode, MODE_AVAIL[1]: ssh_mode, MODE_AVAIL[2]: slurm_mode, MODE_AVAIL[3]: other_mode}
def submitter(dict_opt, plugin_uid, mode=MODE_AVAIL[0]):
    """ TODO
        Args:
            
        Returns:
            
    """
    
    message_daemon = ""
    plugin_alias = alias.get_plugin_alias(plugin_uid)

    #platform
    dict_opt["cluster"] = None
    if mode not in ["local", "other"]:
        # load data from platform
        if not dict_opt["platform"]:
            util.cti_plugin_print_error("Platform not provided!\n")
            return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS

        (_, output_data) = entry.load_data(dict_opt["platform"])
        dict_opt["cluster"] = output_data["init"].params["hostname"][cti.META_ATTRIBUTE_VALUE]
        if not dict_opt["partition"]:
            dict_opt["partition"] = "regular"
    #check parameters
    if not dict_opt["username"]:
        util.cti_plugin_print_error("Username not provided!\n")
        return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    if mode not in MODE_AVAIL:
        util.cti_plugin_print_error("Unknown mode.\n")
        return cti.CTI_PLUGIN_ERROR_INVALID_ARGUMENTS
    if dict_opt["submitter_user"] is None:
        dict_opt["submitter_user"] = getpass.getuser()
    
    date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
    message = "[%s] Initialization - plugin %s\n\tMode: %s" % (date, plugin_alias, mode)
    print message
    message_daemon += message + "\n"
    
    # create the job directory
    os.chdir(WORKING_DIR)
    date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
    job_dir = "%s_%s" % (dict_opt["username"], date)
    os.mkdir(job_dir + "_local")
    os.mkdir(os.path.join(job_dir + "_local", job_dir))
    os.chdir(job_dir + "_local")
    
    # create job subdirectories and copy data
    for job_dir_i in ["APP", "TOOL", "OTHER", "SCRIPTS"]:
        if job_dir_i in dict_opt:
            for data in dict_opt[job_dir_i]:
                dest = os.path.join(job_dir, job_dir_i)
                if not os.path.isdir(dest):
                    os.mkdir(dest)
                if data is not None:
                    dest = os.path.join(dest, os.path.basename(data))
                    if os.path.isdir(data):
                        shutil.copytree(data, dest)
                    else:
                        shutil.copy(data, dest)
    os.mkdir(os.path.join(job_dir, "RESULTS"))
    
    shutil.move(dict_opt["script_params"], os.path.join(job_dir, "script_params"))
    
    # call the 'mode' specific fonction
    try:
        message_daemon += SUBMITTER_MODE[mode](job_dir, dict_opt)
    except Exception as e:
        util.cti_plugin_print_error("%s Error on the submitter: %s" % (cti.HAPI_ERROR_HEADER, e))
        return cti.CTI_ERROR_UNEXPECTED
    
    date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
    message = "[%s] Got the results back" % date
    print message
    message_daemon += message + "\n"
    
    if mode != MODE_AVAIL[0]:
        date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
        message = "[%s] Unpackaging %s.tar.gz" % (date, job_dir)
        print message
        message_daemon += message + "\n"
        job_tar_name = job_dir + ".tar.gz"
        tar_file = tarfile.open(job_tar_name)
        tar_file.extractall()
        tar_file.close()
        os.remove(job_tar_name)
        
        date = datetime.datetime.now().strftime('%Y%m%d_%H%M%S_%f')
        message = "[%s] Unpackaged" % date
        print message
        message_daemon += message + "\n"
    
    os.chdir(job_dir)
    
    daemon_log = open(DAEMON_LOG, "w")
    daemon_log.write(message_daemon)
    daemon_log.close()
    return (job_dir, DAEMON_LOG)
