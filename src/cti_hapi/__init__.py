#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

"""
   High-level API library provides 'pythonic' CTI/CTR bindings, and high-level functions.
"""

__author__ = "Exascale Computing Research Center"

import sys
major, minor = sys.version_info[0:2] 
if not hasattr(sys, "version_info") or (major < 2) or (major >=2 and minor < 6):
    raise RuntimeError("CTI requires Python >=2.6")
del sys

if __name__ == "__main__":
    print """ This library is not intended to be use as an executable.
              Please on usage see the documentation page.
          """
    exit(1)

import version as v
__version__ = v.version.release()
