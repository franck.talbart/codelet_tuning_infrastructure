/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module manages the CTI internal command and
 * plugin commands.
 *
 */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "cti_const.h"
#include "ctr_path.h"
#include "alias.h"
#include "global_index.h"
#include "cti_error.h"
#include "util.h"
#include "config.h"
#include "data_info.h"
#include "commands.h"
#include "platform/platform.h"

/**
 * @brief Print the usage
 * 
 * @return int value 0 if it succeeds
 */
int cti_cmd_usage(void)
{
    char *cti_usage_text = concat_strings(
                              "Type 'cti ", 
                              help_str, 
                              "' for usage, or \n'cti ", 
                              help_str, 
                              " <plugin UID|alias>' for a specific plugin.\n"
                           );
    assert(cti_usage_text != NULL);
    printf("%s", cti_usage_text);
    free(cti_usage_text), cti_usage_text = NULL;
    return 0;
}

/*------------------------------------------------------------------------ */

/**
 * @brief Print error msg
 * 
 * @param int argc number of argument in <argv>
 * @param char **argv arguments
 * @return int value CTI_ERROR_INVALID_COMMAND
 */
int cti_cmd_not_found(int argc, char **argv)
{
    const char cti_cmd_not_found_text[] =
                    "Error: plugin not found. Please do 'cti list plugins' to see the plugins you can run.\n";
    printf("%s", cti_cmd_not_found_text);
    return CTI_ERROR_INVALID_ARGUMENT;
}

/*------------------------------------------------------------------------ */

/**
 * @brief show usage
 * 
 * @param int argc number of argument in <argv>
 * @param char **argv arguments
 * @param char *username the username
 * @param char *password the password
 * @return int value 0 if it succeeds, 1 otherwise
 */
int cti_cmd_help(int argc, char **argv, char *username, char *password)
{
    char *cti_help_text =
                    concat_strings(
                            "Usage:\ncti [",
                            verbose_str,
                            "=1]",
                            " [--debug] <plugin UID|alias> command <parameters> \ncti <plugin UID|alias> @input.json \ncti ",
                            version_str, "\n");
    
    assert(cti_help_text != NULL);
    printf("%s", cti_help_text);
    free(cti_help_text), cti_help_text = NULL;
    
    if (argc < 3)
    {
        printf("List of plugins:\n");
        char *list_plugin_uid = config_get_value(LIST_PLUGIN_UID);
        assert(list_plugin_uid != NULL);
        
        char *argv_p[] = { "", list_plugin_uid, "plugins" };
        cti_cmd_execute_plugin_by_cmd_line(3, argv_p, username, password);
        free(list_plugin_uid), list_plugin_uid = NULL;
    }
    else
    {
        char *doc_plugin_uid = config_get_value(DOC_PLUGIN_UID);
        assert(doc_plugin_uid != NULL);
        
        char *argv_p[] = {"", doc_plugin_uid, "generate", argv[2]};
        cti_cmd_execute_plugin_by_cmd_line(4, argv_p, username, password);
        free(doc_plugin_uid), doc_plugin_uid = NULL;
    }
    return 0;
}

/*------------------------------------------------------------------------ */

/**
 * @brief show the version
 * 
 * @param int argc number of argument in <argv>
 * @param char **argv arguments
 * @param char *username the username
 * @param char *password the password
 * @return 0 if it succeeds, 1 otherwise
 */
int cti_cmd_version(int argc, char **argv, char *username, char *password)
{
    char* version = get_cti_version();
    printf("CTI version %s\n", version);
    free(version), version = NULL;
    return 0;
}

/*------------------------------------------------------------------------ */

/**
 * @brief Create the command CTI will execute
 * 
 * @param int argc number of argument in <argv>
 * @param char **argv arguments
 * @param char *username the username
 * @param char *password the password
 * @return char *value the command
 */
char* create_cmd_to_exec(int argc, char **argv, char *username, char *password)
{
    char *str_uid_plugin = argv[1];
    CTI_UID *uid_plugin = NULL;
    
    if (!is_UID(str_uid_plugin))
        uid_plugin = alias_plugin_get_value(str_uid_plugin);
    
    if (uid_plugin == NULL)
        uid_plugin = str_to_cti_uid(str_uid_plugin);
    
    /* We are sure that str_uid_plugin is an UID, not an alias. */
    str_uid_plugin = cti_uid_to_str(uid_plugin);
    
    char *exec_script_name = config_get_value(EXEC_SCRIPT_NAME);
    if (exec_script_name == NULL)
    {
        printf("Can't get %s from the config file\n", EXEC_SCRIPT_NAME);
        exit(CTI_ERROR_UNEXPECTED);
    }
    
    char *cmd_to_exec = NULL;
    char *path_plugin = get_path_by_uid(CTR_ENTRY_PLUGIN, uid_plugin);
    
    if (path_plugin != NULL)
    {
        cmd_to_exec = concat_strings(path_plugin, CTI_SLASH, exec_script_name);
        
        /* We check if the file exists */
        FILE * fp = fopen(cmd_to_exec, "rb");
        if (fp == NULL)
        {
            printf("%s not found!\n", cmd_to_exec);
            exit(CTI_ERROR_SCRIPT_NOT_EXISTS);
        }
        else
            fclose(fp);
    }
    else
    {
        printf("Plugin not found.\n");
        exit(CTI_ERROR_PLUGIN_NOT_FOUND);
    }
    
    char *old_cmd_to_exec = cmd_to_exec;
    
    cmd_to_exec = concat_strings(cmd_to_exec, 
                                 " ", 
                                 path_plugin, 
                                 " ", 
                                 str_uid_plugin, 
                                 " ", 
                                 username, 
                                 " ", 
                                 password);
    free(old_cmd_to_exec), old_cmd_to_exec = NULL;
    free(str_uid_plugin), str_uid_plugin = NULL;
    
    int cpt_args = 2;
    
    while (cpt_args < argc)
    {
        old_cmd_to_exec = cmd_to_exec;
        /*
         * Some plugin arguments may contain quotes. This especially involves
         * complex types.
         * These quotes are escaped by the user in the command line but bash
         * un-escapes them before they reach the front-end.
         * Thus, in order to be correctly interpreted by the system, these
         * quotes must be re-escaped in the resulting "cmd_to_exec".
         */
        char *escaped_plugin_arg = escape_quotes(argv[cpt_args]);
        char *plugin_arg = concat_strings("\"", escaped_plugin_arg, "\"");
        cmd_to_exec = concat_strings(cmd_to_exec, " ", plugin_arg);
        free(escaped_plugin_arg), escaped_plugin_arg = NULL;
        free(plugin_arg), plugin_arg = NULL;
        free(old_cmd_to_exec), old_cmd_to_exec = NULL;
        cpt_args++;
    }
    
    free(exec_script_name), exec_script_name = NULL;
    free(path_plugin), path_plugin = NULL;
    free(uid_plugin), uid_plugin = NULL;
    
    return cmd_to_exec;
}

/*------------------------------------------------------------------------ */

/**
 * @brief function executes plugin using system()
 * 
 * @param int argc number of argument in <argv>
 * @param char **argv arguments
 * @param char *username the username
 * @param char *password the password
 * @return plugin execution code, or CTI_ERROR if it fails
 */
int cti_cmd_execute_plugin_by_cmd_line(int argc, char **argv, char *username,
                                        char *password)
{
    char *cmd_to_exec = create_cmd_to_exec(argc, argv, username, password);
    int command_result = system(cmd_to_exec);
    free(cmd_to_exec), cmd_to_exec = NULL;
    command_result = WEXITSTATUS(command_result);
    return command_result;
}

/*------------------------------------------------------------------------ */
