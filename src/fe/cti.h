/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef CTI_H_
#define CTI_H_
#define POSIXLY_CORRECT

/* Constants */
#define HIDE_GETOPT_ERRORS  0
#define opt_verbose         'v'
#define opt_debug           'd'
#define opt_user            'u'
#define opt_no_session      'n'
#define cmd_line            "+v:du:n"

int handle_cti_options(int argc, char **argv);

#endif /* _CTI_H_ */
