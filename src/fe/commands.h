/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef CTI_COMMANDS_H_
#define CTI_COMMANDS_H_

#include <stdint.h>

#include "cti_table.h"

#define verbose_str "--verbose"
#define version_str "--version"
#define debug_str "--debug"
#define user_str "--user"
#define no_session_str "--no-session"
#define help_str "--help"

/* COMMANDS */
int cti_cmd_usage(void);
int cti_cmd_not_found(int argc, char **argv);
int cti_cmd_help(int argc, char **argv, char *username, char *password);
int cti_cmd_version(int argc, char **argv, char *username, char *password);
char* create_cmd_to_exec(int argc, char **argv, char *username, char *password);
int cti_cmd_execute_plugin_by_cmd_line(int argc, char **argv, char *username, char *password);

#endif /* _CTI_COMMANDS_H_ */
