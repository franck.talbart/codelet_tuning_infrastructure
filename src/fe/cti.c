/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module manages the command line arguments.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <assert.h>
#include <getopt.h>
#include "commands.h"
#include "ctr_path.h"
#include "util.h"
#include "alias.h"
#include "get_directory.h"
#include "config.h"
#include "cti_const.h"
#include "cti.h"

extern int START_DEBUG;

/**
 * @brief verbosity level initialization
 * 
 * @return long value verbosity level
 */
long init_verbosity()
{
    long vlevel = CTI_VERBOSITY_DEFAULT;
    char *verbosity = config_get_value(CTI_VERBOSITY);
    if (verbosity == NULL)
        config_set_value(CTI_VERBOSITY, CTI_VERBOSITY_DEFAULT_STR);
    else
    {
        vlevel = strtol(verbosity, NULL, 10);
        CHECK_VERBOSITY(vlevel, verbosity);
        free(verbosity), verbosity = NULL;
    }
    return vlevel;
}

//------------------------------------------------------------------------

/**
 * @brief set cti user and password
 * 
 * @param char *user the user
 * @param char *password the password
 * @param int no_session 0 if session is activated, 1 if not
 * @return 1 if it succeeds, 0 if it fails
 */
int set_cti_user(char* user, char* password, int no_session)
{
    char *tmp = NULL;
    if (user == NULL || password == NULL)
        return 0;
    if (no_session == 1) 
    {
        snprintf(user, CTI_MAX_STRING_SIZE, "%s", NO_SESSION);
        snprintf(password, CTI_MAX_STRING_SIZE, "%s", NO_SESSION);
    }
    else if (xstrnlen(user, CTI_MAX_STRING_SIZE) <= 0)
    {
        /* --user not found in cmd line: Check if session already opened */
        char *session_filename = config_get_value(SESSION_FILENAME);
        assert(session_filename != NULL);
        char *home = getenv("HOME");
        assert(home != NULL);
        char *session_path = concat_strings(home, CTI_SLASH, session_filename);
        FILE *session = fopen(session_path, "r");
        
        if (session == NULL)
        {
            /* session opening failed: set up no session mode */
            snprintf(user, CTI_MAX_STRING_SIZE, "%s", NO_SESSION);
            snprintf(password, CTI_MAX_STRING_SIZE, "%s", NO_SESSION);
            no_session = 1;
        }
        else
        {
            char buffer[CTI_MAX_STRING_SIZE];
            /* session is already opened */
            if (fgets(buffer, CTI_MAX_STRING_SIZE, session) != NULL)
            {
                tmp = xstrtrim(buffer);
                snprintf(user, CTI_MAX_STRING_SIZE, "%s", tmp);
                free(tmp), tmp = NULL;
            }
            if (fgets(buffer, CTI_MAX_STRING_SIZE, session) != NULL)
            {
                tmp = xstrtrim(buffer);
                snprintf(password, CTI_MAX_STRING_SIZE, "%s", tmp);
                free(tmp), tmp = NULL;
            }
            
            if (xstrnlen(user, CTI_MAX_STRING_SIZE) <= 0
                    || xstrnlen(password, CTI_MAX_STRING_SIZE) <= 0)
            {
                printf("Error while reading user/password");
            }
            
            fclose(session);
        }
        free(session_filename), session_filename = NULL;
        free(session_path), session_path = NULL;
    }

    return (xstrnlen(user, CTI_MAX_STRING_SIZE) > 0)
            && (xstrnlen(password, CTI_MAX_STRING_SIZE) > 0);
}

//------------------------------------------------------------------------

/**
 * @brief analyze if <str> contains a plugin to execute
 * 
 * @param char* str the string to analyze
 * @return 1 if there is a plugin to execute, 0 if not
 */
int is_plugin(char* str)
{
    CTI_UID *command = NULL;
    char *plugin_path = NULL;
    
    if (str == NULL || xstrnlen(str, CTI_MAX_STRING_SIZE) <= 0)
        return 0;
    else if (is_UID(str))
        command = str_to_cti_uid(str);
    else
        command = alias_plugin_get_value(str);
    
    if (command == NULL)
        return 0;
    
    plugin_path = get_path_by_uid(CTR_ENTRY_PLUGIN, command);
    free(command), command = NULL;
    
    if (plugin_path == NULL)
        return 0;
    
    free(plugin_path), plugin_path = NULL;
    return 1;
}

//------------------------------------------------------------------------

/**
 * @brief Analyze if <param> is a normal command to execute
 * 
 * @param char* param the string to analyze
 * @param cti_command commands[] the array of cti commands (--help, --version)
 * @return int value -1 if it fails, positive integer if it succeeds
 */
int is_normal_command(char* param, cti_command commands[])
{
    if (param == NULL || commands == NULL)
        return -1;
    
    int len = xstrnlen(param, CTI_MAX_STRING_SIZE);
    int i = 0;
    
    if (len <= 0)
        return -1;
    
    while (commands[i].name != NULL
            && strncmp(param, commands[i].name, len) != 0)
        i = i + 1;
    
    if (commands[i].name == NULL)
    {
        /* argv[1] has not been found in the cti_command array */
        i = -1;
    }
    
    return i;
}

//------------------------------------------------------------------------

/**
 * @param int argc number of argument in <argv>
 * @param char **argv arguments
 * @return int value non-zero if it fails
 */
int handle_cti_options(int argc, char** argv)
{
    if (argc < 2)
    {
        cti_cmd_usage();
        return EXIT_SUCCESS;
    }
    
    /* command line to execute */
    char* new_argv[argc];
    int new_argc = 0;
    
    int i = 0; /* browse the initial argv array */
    int j = 1; /* browse the new command line new_argv */
    
    /* cti option values */
    char user[CTI_MAX_STRING_SIZE];
    char password[CTI_MAX_STRING_SIZE];
    int no_session = 0;
    long vlevel = init_verbosity();
    
    user[0] = '\0';
    password[0] = '\0';
    
    /* cti normal commands */
    cti_command commands[] =
    { { help_str, cti_cmd_help },
      { version_str, cti_cmd_version },
      { NULL, NULL } };
    
    /* getopt */
    int c;
    int option_index = 0;
    opterr = HIDE_GETOPT_ERRORS;
    
    struct option cti_options[] =
    { { "verbose", required_argument, NULL, opt_verbose },
      { "debug", no_argument, NULL, opt_debug },
      { "user", required_argument, NULL, opt_user },
      { "no-session", no_argument, NULL, opt_no_session },
      { 0, 0, 0, 0 } };
    
    /* getopt will stop as it will detect a non cti option */
    while ((c = getopt_long(argc, argv, cmd_line, cti_options, &option_index))
            != -1)
    {
        switch (c)
        {
        case opt_verbose:
            if (optarg != NULL)
            {
                vlevel = strtol(optarg, NULL, 10);
                CHECK_VERBOSITY(vlevel, optarg);
                config_set_value(CTI_VERBOSITY, optarg);
            }
            break;
            
        case opt_debug:
            START_DEBUG = 1;
            break;
            
        case opt_user:
            if (optarg != NULL)
                snprintf(user, CTI_MAX_STRING_SIZE, "%s", optarg);
            break;
            
        case opt_no_session:
            no_session = 1;
            break;
            
        case '?':
            /* if --help or --version are found, save current position */
            if (i == 0)
                i = optind - 1;
            break;
            
        default:
            break;
        }
    }
    
    /* if no commands were found, optind is the index of the first non cti option */
    if (i == 0)
        i = optind;
    
    if (xstrnlen(user, CTI_MAX_STRING_SIZE) > 0)
    {
        /*
         * Detect the '--user password' syntax:
         * if --user has been read, the first nonoption is the password
         */
        snprintf(password, CTI_MAX_STRING_SIZE, "%s", argv[i]);
        i = i + 1;
    }
    
    /* Copy the non-cti options to the new command line */
    while (i < argc && j < argc)
    {
        new_argv[j] = argv[i];
        j = j + 1;
        i = i + 1;
    }
    
    new_argc = j;
    new_argv[0] = argv[0]; /* program name */
    
    if(new_argc < 2)
        return EXIT_FAILURE;
    
    /* cmd is ready to run, set up cti user */
    if (set_cti_user(user, password, no_session) == 0)
        return EXIT_FAILURE;
    
    if (is_plugin(new_argv[1]) == 1)
        return cti_cmd_execute_plugin_by_cmd_line(new_argc, new_argv, user,
                password);
    else if ((i = is_normal_command(new_argv[1], commands)) != -1)
        return commands[i].command_function(new_argc, new_argv, user, password);
    else
        return cti_cmd_not_found(new_argc, new_argv);
}

//------------------------------------------------------------------------
