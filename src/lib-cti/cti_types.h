/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */


#ifndef CTI_TYPES_H_
#define CTI_TYPES_H_

#include "cti_const.h"

typedef struct
{
    char data[UID_NUMBER_OF_SYMBOL];
} CTI_UID;

enum CTR_REP_T
{
    CTR_REP_LOCAL,
    CTR_REP_COMMON,
    CTR_REP_TEMP,
};

enum CTR_DATA_T
{
    CTR_ENTRY_DATA,
    CTR_ENTRY_PLUGIN,
    CTR_ENTRY_REPOSITORY,
};

typedef struct
{
    char *name;
    int (*command_function)(int argc, char **argv, char *username, char *password);
} cti_command;

typedef struct
{
    char *key;
    void *value;
    int size;
} cti_pair;

typedef struct
{
    /* total amount of cells in the table */
    int size;
    /* amount of stored (non-null) cells */
    int stored;
    cti_pair **data;
    void (*ptr_function_free_memory)(void*);
} cti_table;

typedef struct
{
    int cpt; /* counter */
    cti_table *table;
} generic_table_iterator;

typedef generic_table_iterator global_index_iterator;

#endif /* _CTI_TYPES_H_ */
