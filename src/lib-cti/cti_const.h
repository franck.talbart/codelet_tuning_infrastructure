/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * CTI consts
 */

#ifndef CTI_CONST_H_
#define CTI_CONST_H_

/* ALIAS */
#define ALIAS_PLUGIN_NAME "ALIAS_PLUGIN_NAME"
#define ALIAS_REPOSITORY_NAME "ALIAS_REPOSITORY_NAME"

/* CONFIG */
#define CFG_FILE_NAME "config.txt"
#define CONFIG_ROOT "CONFIG_ROOT"

/* CTR */
#define COMMON_TOOLS_DIR "COMMON_TOOLS_DIR"
#define CTR_COMMON_DIR "CTR_COMMON_DIR"
#define CTR_DATA_DIR  "CTR_DATA_DIR"
#define CTR_ENTRY_FILES_DIR "CTR_ENTRY_FILES_DIR"
#define CTR_PLUGIN_DIR "CTR_PLUGIN_DIR"
#define CTR_TEMP_DIR "CTR_TEMP_DIR"
#define THIRD_PARTY_DIR "THIRD_PARTY_DIR"

/* DATA_INFO */
#define DATA_INFO_ADDITIONAL_FILES "additional_files"
#define DATA_INFO_ALIAS "alias"
#define DATA_INFO_DATE_TIME_END "date_time_end"
#define DATA_INFO_DATE_TIME_START "date_time_start"
#define DATA_INFO_FILENAME  "DATA_INFO_FILENAME"
#define DATA_INFO_PLUGIN_EXIT_CODE "plugin_exit_code"
#define DATA_INFO_PLUGIN_UID "plugin_uid"
#define DATA_INFO_NOTE "note"
#define DATA_INFO_TAG "tag"
#define DATA_INFO_USER_UID "user_uid"

#define DATA_INFO_INITIAL_SIZE 2048

/* PLUGIN_INFO */
#define PLUGIN_INFO_NAME "name"

/* INPUT AND OUPUT PLUGIN FILES */
#define PLUGIN_DEFAULT_INPUT_FILENAME "PLUGIN_DEFAULT_INPUT_FILENAME"
#define PLUGIN_DEFAULT_OUTPUT_FILENAME "PLUGIN_DEFAULT_OUTPUT_FILENAME"
#define PLUGIN_INPUT_FILENAME "PLUGIN_INPUT_FILENAME"
#define PLUGIN_OUTPUT_FILENAME "PLUGIN_OUTPUT_FILENAME"

/* META DATA */
#define META_ATTRIBUTE_DESC "desc"
#define META_ATTRIBUTE_DIRECTORY "directory"
#define META_ATTRIBUTE_FALSE "false"
#define META_ATTRIBUTE_LIST "list"
#define META_ATTRIBUTE_LONG_DESC "long_desc"
#define META_ATTRIBUTE_NAME "name"
#define META_ATTRIBUTE_OPTIONAL "optional"
#define META_ATTRIBUTE_PASSWORD "password"
#define META_ATTRIBUTE_PRODUCED_BY "produced_by"
#define META_ATTRIBUTE_PRODUCED_DATA "produce_data"
#define META_ATTRIBUTE_REP "repository"
#define META_ATTRIBUTE_REP_PRODUCE "repository_produce"
#define META_ATTRIBUTE_TARGET "target"
#define META_ATTRIBUTE_TRUE "true"
#define META_ATTRIBUTE_TYPE "type"
#define META_ATTRIBUTE_VALUE "value"
#define META_ATTRIBUTE_MATRIX_COLUMN_NAMES "column_names"
#define META_ATTRIBUTE_MATRIX_COLUMN_TYPES "column_types"
#define META_ATTRIBUTE_MATRIX_COLUMN_DESCS "column_descs"
#define META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS "column_long_descs"
#define META_ATTRIBUTE_MATRIX_GENERATED "generated_matrix"
#define META_CONTENT_ATTRIBUTE_TYPE_DATA_UID "DATA_UID"
#define META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID "PLUGIN_UID"
#define META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID "REPOSITORY_UID"
#define META_CONTENT_ATTRIBUTE_TYPE_DATE "DATE"
#define META_CONTENT_ATTRIBUTE_TYPE_INTEGER "INTEGER"
#define META_CONTENT_ATTRIBUTE_TYPE_FLOAT "FLOAT"
#define META_CONTENT_ATTRIBUTE_TYPE_COMPLEX "COMPLEX"
#define META_CONTENT_ATTRIBUTE_TYPE_TEXT "TEXT"
#define META_CONTENT_ATTRIBUTE_TYPE_FILE "FILE"
#define META_CONTENT_ATTRIBUTE_TYPE_URL "URL"
#define META_CONTENT_ATTRIBUTE_TYPE_EMAIL "EMAIL"
#define META_CONTENT_ATTRIBUTE_TYPE_MATRIX "MATRIX"
#define META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN "BOOLEAN"

/* OTHER */
#define CTI_CFG "CTI_CFG"
#define CTI_PLUGIN_WORKING_DIR "CTI_PLUGIN_WORKING_DIR"
#define CTI_PLUGIN_DISTANT_WORKING_DIR "CTI_PLUGIN_DISTANT_WORKING_DIR"
#define CTI_ROOT "CTI_ROOT"
#define CTI_CENTRAL_NODE "CTI_CENTRAL_NODE"
#define DATABASE_FILENAME "DATABASE_FILENAME"
#define DOC_PLUGIN_UID "DOC_PLUGIN_UID"
#define DOT_CTR "DOT_CTR"
#define EXEC_SCRIPT_NAME "EXEC_SCRIPT_NAME"
#define GLOBAL_INDEX_FILE "GLOBAL_INDEX_FILE"
#define LIST_PLUGIN_UID "LIST_PLUGIN_UID"
#define LOG_FILE_NAME "LOG_FILE_NAME"
#define NO_SESSION "NO_SESSION"
#define PLUGIN_PRE_GENERATE_SCHEMA "PLUGIN_PRE_GENERATE_SCHEMA"
#define SESSION_FILENAME "SESSION_FILENAME"
#define UID_NUMBER_OF_MINUS 4
#define UID_NUMBER_OF_SYMBOL 36
#define UID_TOOL "UID_TOOL"
#define USER_UID "USER_UID"
#define WRONG_ENTRIES_LOG_FILE_NAME "WRONG_ENTRIES_LOG_FILE_NAME"

/* LOGS */
#define ALIAS_CURRENT_LOG "ALIAS_CURRENT_LOG"
#define LOGS_TIME_LIMIT "LOGS_TIME_LIMIT"

/* PLUGIN_INFO */
#define PLUGIN_INFO_BUILD_NUMBER "build_number"
#define PLUGIN_INFO_CATEGORY "category"
#define PLUGIN_INFO_DESCRIPTION "description"
#define PLUGIN_INFO_AUTHOR "authors"
#define PLUGIN_INFO_DEPENDENCY "cti_dependence_build_number"

/* REPOSITORIES */
#define COMMON_REPOSITORY "common"
#define TEMP_REPOSITORY "temp"
#define LOCAL_REPOSITORY "local"
#define ALL_REPOSITORY "all"

/* UTIL */
#define CTI_SLASH "/"
#define CTI_MAX_STRING_SIZE 2048
#define END_OF_LINE "\n"

/* VERBOSITY */
#define CTI_VERBOSITY "CTI_VERBOSITY"
#define CTI_VERBOSITY_DEFAULT 1
#define CTI_VERBOSITY_DEFAULT_STR "1"
#define CTI_VERBOSITY_MAX 2
#define CTI_VERBOSITY_MIN 0

/* VERSION (CTI) */
#include "cti_version.h"
#endif /* _CTI_CONST_H_ */
