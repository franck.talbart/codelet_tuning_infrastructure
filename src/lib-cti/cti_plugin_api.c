/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * CTI libraries used by the plugins. It provides the low-level functions
 *  of the CTI core.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "cti_const.h"
#include "cti_types.h"
#include "util.h"
#include "log.h"
#include "ctr_path.h"
#include "config.h"
#include "alias.h"
#include "cti_table.h"
#include "cti_plugin_api.h"

/**
 * @brief Set <key, value> in the config file.
 * 
 * @param char *key the key
 * @param char *value the value
 */
void cti_plugin_config_set_value(const char *key, char *value)
{
    cti_log(2, concat_strings("in ", __func__, " key = ", key, " value = ", value));
    return config_set_value(key, value);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Get value from config file with the given key.
 * 
 * @param const char *key the key
 * @return char* value a given key, NULL if it fails
 */
char* cti_plugin_config_get_value(const char *key)
{
    return config_get_value(key);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Generate an unique UID.
 * 
 * @return CTI_UID *value null-terminated array of UID_NUMBER_OF_SYMBOL chars, NULL if it fails
 */
CTI_UID* cti_plugin_generate_uid(void)
{
    return cti_generate_cti_uid();
}

/*------------------------------------------------------------------------ */

/**
 * @brief Check that a given string is an array of 19th characters.
 * 
 * @param char *uid the uid to check
 * @return bool value true if it uses the good format, false otherwise
 */
bool cti_plugin_is_UID(char *uid)
{
    return is_UID(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Convert a CTI_UID into string.
 * 
 * @param CTI_UID *uid the UID to convert
 * @return char *value the string, or NULL in case of failure, writing the error reason to the CTI standard log
 */
char* cti_plugin_cti_uid_to_str(CTI_UID *uid)
{
    return cti_uid_to_str(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Convert a string into a CTI_UID.
 * 
 * @param char *m_str the string to convert
 * @return CTI_UID *value the UID, or NULL in case of failure, writing the error reason to the CTI standard log
 */
CTI_UID* cti_plugin_str_to_cti_uid(char* m_str)
{
    return str_to_cti_uid(m_str);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Return the data alias with the given UID.
 * 
 * @param CTI_UID *value the uid
 * @return char *value the alias, NULL if not found
 */
char* cti_plugin_alias_data_get_key(CTI_UID *value)
{
    return alias_data_get_key(value);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Set an alias to the data entry.
 * 
 * @param const char *key the alias name
 * @param CTI_UID* value the value
 * @return UID, NULL if not found
 */
void cti_plugin_alias_data_set_value(const char *key, CTI_UID* value)
{
    alias_data_set_value(key, value);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Return the plugin alias with the given UID.
 * 
 * @param const CTI_UID* value the value
 * @return char *value the alias, NULL if not found
 */
char* cti_plugin_alias_plugin_get_key(CTI_UID* value)
{
    return alias_plugin_get_key(value);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Return the plugin UID with the given alias.
 * 
 * @param const char *name the alias name
 * @return CTI_UID *value UID, NULL if not found
 */
CTI_UID* cti_plugin_alias_plugin_get_value(const char *name)
{
    return alias_plugin_get_value(name);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Set an alias to the plugin entry.
 * 
 * @param const char *key the key
 * @param CTI_UID *value is plugins UID
 * @param const char *dir the directory that contains the alias file. If NULL, then it is ctr-common
 */
void cti_plugin_alias_plugin_set_value(const char *key, CTI_UID *value, char *dir)
{
    alias_plugin_set_value(key, value, dir);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Remove the entry.
 * 
 * @param const char *value the value to remove
 * @return int value error code
 */
int cti_plugin_alias_plugin_rm_value(CTI_UID *value)
{
    return alias_plugin_rm_value(value);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Return the repository alias.
 * 
 * @param CTI_UID *value the uid
 * @return char *value the alias, NULL if not found
 */
char* cti_plugin_alias_repository_get_key(CTI_UID *value)
{
    return alias_repository_get_key(value);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Return the repository UID.
 * 
 * @param const char *name the alias name
 * @return CTI_UID *value the UID, NULL if not found
 */
CTI_UID* cti_plugin_alias_repository_get_value(const char *name)
{
    return alias_repository_get_value(name);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Remove the repository.
 * 
 * @param CTI_UID *value the value
 * @return int value 0 if it succeeds, -1 otherwise
 */
int cti_plugin_alias_repository_rm_value(CTI_UID *value)
{
    return alias_repository_rm_value(value);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Set an alias to a repository.
 * 
 * @param const char *key alias name
 * @param CTI_UID* value the value
 */
void cti_plugin_alias_repository_set_value(const char *key, CTI_UID* value)
{
    alias_repository_set_value(key, value);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Print debug message to CTI log.
 * 
 * @param const char *format the log text
 */
void cti_plugin_log(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    cti_log_va(CTI_VERBOSITY_DEFAULT, format, ap);
    va_end(ap);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Return the current CTI version.
 * 
 * @return char *value the cti version X.Y.Z
 *  X -- major version
 *  Y -- minor version
 *  Z -- build number
 */
char* cti_plugin_get_cti_version(void)
{
    return get_cti_version();
}

/*------------------------------------------------------------------------ */

