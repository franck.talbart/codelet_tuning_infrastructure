/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * CTI errors
 */

#ifndef CTI_ERROR_H_
#define CTI_ERROR_H_

#define HAPI_ERROR_HEADER "[CTI HAPI ERROR]"
#define CTI_ERROR_HEADER "[CTI ERROR]"
#define CTI_WARNING_HEADER "[CTI WARNING]"
#define CTI_ERROR_MSG_UNEXPECTED "Unexpected error"
#define CTI_ERROR_MSG_COMMAND_NOT_FOUND "Command not found. Please use the 'help' command for more information."
#define CTI_ERROR_MSG_CREATE_REP "To create local repository call: $ cti repository init"
#define CTI_ERROR_MSG_IMPORT_REP "If repository exists, but not imported call: $ cti repository import"
#define CTI_ERROR_MSG_REP_DOESNT_EXISTS "Local repository doesn't exist or not tracked."
#define CTI_ERROR_MSG_GET_CONFIG_VALUE "Can't get config value"
#define CTI_ERROR_MSG_CANT_CREATE_ENTRY "[CTI ERROR]: Can't create data entry"

/* Core Errors */
enum CtiError
{
    CTI_ERROR_FAILURE = 1,
    CTI_ERROR_UNEXPECTED,
    CTI_ERROR_INVALID_ARGUMENT,
    CTI_ERROR_PATH_EXISTS,
    CTI_ERROR_ALIAS_EXISTS,
    CTI_ERROR_CANT_CREATE_DIR,
    CTI_ERROR_INVALID_DOT_CTR,
    CTI_ERROR_PLUGIN_NOT_FOUND,
    CTI_ERROR_UID_NOT_FOUND,
    CTI_ERROR_SCRIPT_NOT_EXISTS,
    CTI_ERROR_PATH_NULL,
    CTI_ERROR_CANT_OPEN_FILE,
    CTI_ERROR_MALLOC,
    CTI_ERROR_GET_CONFIG_VALUE,
    CTI_ERROR_WRONG_PASSWORD,
};



/* Plugins Errors */

/* Critical */
enum CtiPluginCriticalError
{
    CTI_PLUGIN_ERROR_SYNTAX=1,
    CTI_PLUGIN_ERROR_EXCEPT=2,
    CTI_PLUGIN_ERROR_UNEXPECTED=3,
    CTI_PLUGIN_ERROR_CRITICAL_IO=4,
    CTI_PLUGIN_ERROR_RUNSCRIPT=5,
    CTI_PLUGIN_ERROR_CANT_CREATE_DATA_ENTRY=6,
    CTI_PLUGIN_ERROR_UNKNOWN_ENTRY=7,
    CTI_PLUGIN_ERROR_INCOMPATIBLE_ENTRY=8,
    CTI_PLUGIN_ERROR_INIT_NOT_FOUND=9,
    CTI_PLUGIN_ERROR_DEFAULT_REPOSITORY=10,
    CTI_PLUGIN_ERROR_DOT_CTR=11,
    CTI_PLUGIN_ERROR_COMMAND_NOT_FOUND=12,
    CTI_PLUGIN_ERROR_LOCAL_REP_DOESNT_EXISTS=13,
};

/* Non-critical */
enum CtiPluginError
{
    CTI_PLUGIN_ERROR_UNKNOWN_FORMAT=255,
    CTI_PLUGIN_ERROR_IO=254,
    CTI_PLUGIN_ERROR_INVALID_ARGUMENTS=253,
    CTI_PLUGIN_ERROR_WRONG_PASSWORD=252,
    CTI_PLUGIN_ERROR_SESSION_EXISTS=251,
    CTI_PLUGIN_ERROR_USER_INTERRUPT=250,
    CTI_PLUGIN_ERROR_RIGHT=249,
    CTI_PLUGIN_ERROR_IMPORT=248,
    CTI_PLUGIN_QUERY_NO_RESULT=247,
    CTI_PLUGIN_ERROR_TOOL_NOT_FOUND=246,
};

#endif /* _CTI_ERROR_H_ */
