#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit
AUTOMAKE_OPTIONS = foreign
DEFAULT_FLAGS = -Werror -Wall -std=c99
CC = gcc
OPTS = -std=c99
INCLUDE_COMMON = -I../common -I../common/platform/ -I$(BUILD_DIR)
PLATFORM_SPECIFIC=-fPIC
LIB_CTICOMMON = ../common/*.c ../common/platform/*.c
INCLUDE_LIB_CTI = -I.
CTI_LIB_NAME = libcti.a
BUILD_DIR = /tmp
OBJS = log.o alias.o config.o cti_table.o cti_uid.o ctr_path.o data_info.o get_directory.o global_index.o util.o platform.o

OUT_OBJS=$(addprefix $(BUILD_DIR)/objs/,$(OBJS))


all: common cti.lib swig_python

cti.lib::
	@echo "==================================================="
	@echo "Compiling CTI library for SWIG ..."
	@echo ""
	$(CC) $(OPTS) $(PLATFORM_SPECIFIC) $(DEFAULT_FLAGS) $(INCLUDE_COMMON) $(INCLUDE_LIB_CTI) -c cti_plugin_api.c -o $(BUILD_DIR)/objs/cti_plugin_api.o $(CPPFLAGS) $(LDFLAGS)
	ar rcs $(BUILD_DIR)/lib/$(CTI_LIB_NAME) $(OUT_OBJS) $(BUILD_DIR)/objs/cti_plugin_api.o $(CPPFLAGS) $(LDFLAGS)

common:
	$(MAKE) -C ../common clean all PLATFORM_SPECIFIC="$(PLATFORM_SPECIFIC)" BUILD_DIR="$(BUILD_DIR)"

swig_python:: cti.lib
	@echo "==================================================="
	@echo "Compiling SWIG (Python support) ..."
	@echo ""
	$(MAKE) -C python BUILD_DIR="$(BUILD_DIR)" OBJS="$(OUT_OBJS)" clean all

clean::
	rm -f $(OUT_OBJS)
	rm -f "$(BUILD_DIR)"/objs/cti_plugin_api.o
	rm -f "$(BUILD_DIR)"/lib/"$(CTI_LIB_NAME)"
	$(MAKE) -C python clean
