/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */


#ifndef CTI_PLUGIN_API_H_
#define CTI_PLUGIN_API_H_

#include "cti_types.h"

#include <stdbool.h>

/* Config */
char* cti_plugin_config_get_value(const char *key);
void cti_plugin_config_set_value(const char *key, char* value);

/* UID */
CTI_UID* cti_plugin_generate_uid(void);
bool cti_plugin_is_UID(char *uid);
char* cti_plugin_cti_uid_to_str(CTI_UID *uid);
CTI_UID* cti_plugin_str_to_cti_uid(char* m_str);

/* Alias */
char* cti_plugin_alias_data_get_key(CTI_UID *value);
void cti_plugin_alias_data_set_value(const char *key, CTI_UID* value);
char* cti_plugin_alias_plugin_get_key(CTI_UID* value);
CTI_UID* cti_plugin_alias_plugin_get_value(const char *name);
void cti_plugin_alias_plugin_set_value(const char *key, CTI_UID *value, char *dir);
int cti_plugin_alias_plugin_rm_value(CTI_UID *value);
char* cti_plugin_alias_repository_get_key(CTI_UID *value);
CTI_UID* cti_plugin_alias_repository_get_value(const char *name);
int cti_plugin_alias_repository_rm_value(CTI_UID *value);
void cti_plugin_alias_repository_set_value(const char *key, CTI_UID* value);

/* Log */
void cti_plugin_log(const char *format, ...);

/* CTI version */
char* cti_plugin_get_cti_version(void);

#endif /*_CTI_PLUGIN_API_H*/
