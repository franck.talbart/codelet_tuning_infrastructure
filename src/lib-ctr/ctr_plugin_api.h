/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef CTR_PLUGIN_API_H_
#define CTR_PLUGIN_API_H_

#include "cti_types.h"

#include <stdbool.h>

/* General */
int ctr_plugin_check_repository(char *path_to_rep);
cti_pair* ctr_plugin_create_entry(CTI_UID *plugin_uid, enum CTR_REP_T crt,
        enum CTR_DATA_T cdt, CTI_UID *local_repository, CTI_UID *user_uid);

/* Data info */
cti_table* ctr_plugin_info_file_load_by_uid(CTI_UID *uid);
char* ctr_plugin_info_get_value(cti_table *data_info, char *key);
void ctr_plugin_info_destruct(cti_table *data_info);
void ctr_plugin_info_put_value(cti_table *data_info, char* key, char* value);
void ctr_plugin_info_record_in_file(cti_table *data_info, char *filename);
cti_table* ctr_plugin_info_create(void);
void ctr_plugin_info_append_data_info_file(int code, char *cti_output_dir);

/* Directory */
char* ctr_plugin_get_cti_cfg_dir(void);
char* ctr_plugin_get_data_common_path_by_uid(CTI_UID *uid);
char* ctr_plugin_get_plugin_common_path_by_uid(CTI_UID *uid);
char* ctr_plugin_get_data_temp_path_by_uid(CTI_UID *uid);
char* ctr_plugin_get_plugin_temp_path_by_uid(CTI_UID *uid);
char* ctr_plugin_get_common_plugin_dir(void);
char* ctr_plugin_get_common_data_dir(void);
char* ctr_plugin_get_common_dir(void);
char* ctr_plugin_get_temp_plugin_dir(void);
char* ctr_plugin_get_temp_data_dir(void);
char* ctr_plugin_get_temp_dir(void);
char* ctr_plugin_get_local_plugin_dir(CTI_UID* ctr_uid);
char* ctr_plugin_get_local_data_dir(CTI_UID* ctr_uid);
char* ctr_plugin_get_path_by_uid(enum CTR_DATA_T datatype, CTI_UID *uid);
enum CTR_REP_T ctr_plugin_get_repository_by_uid(enum CTR_DATA_T datatype, CTI_UID *uid);
cti_pair* ctr_plugin_get_path_and_type_by_uid(CTI_UID *uid);
char* ctr_plugin_get_plugin_path_by_uid(CTI_UID *uid);
char* ctr_plugin_get_data_path_by_uid(CTI_UID *uid);

/* Global index file */
void* ctr_plugin_global_index_file_load(void);
void ctr_plugin_global_index_file_unload(void *gindex_file);
void* ctr_plugin_global_index_file_it_begin(void *it);
bool ctr_plugin_global_index_file_it_is_end(void *it);
char* ctr_plugin_global_index_file_get_ctr_by_uid(CTI_UID *uid);
CTI_UID* ctr_plugin_global_index_file_get_uid_by_ctr(char *path);
void* ctr_plugin_global_index_file_it_next(void *it);
char* ctr_plugin_global_index_file_it_value(void *it);
CTI_UID* ctr_plugin_global_index_file_it_key(void *it);
void ctr_plugin_global_index_file_rm(char *path_to_rep);
char* ctr_plugin_global_index_file_path_by_uid(CTI_UID *uid, char *ctr_dir_type);
char* ctr_plugin_global_index_file_ctr_by_entry_uid(CTI_UID *uid, char *ctr_dir_type);
bool ctr_plugin_global_index_file_is_there_this_path(char *path);
int ctr_plugin_global_index_file_write(char *path_to_rep, CTI_UID *uid);

#endif /* _CTR_PLUGIN_API_H_ */
