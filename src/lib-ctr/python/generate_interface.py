#!/usr/bin/env python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import os
import sys

def generate_setup():
    
    sources = \
    [
        os.path.join("..", "ctr_plugin_api.c"), 
        os.path.join(sys.argv[1], "python", "ctr_wrap.c")
    ]
    
    ext_objects = sys.argv[2].split(" ")
    
    common_dir = os.path.join("..", "..", "common")
    lib_dir = os.path.join("..", "..", "lib-cti", "")
    
    includes = \
    [ 
        common_dir ,
        lib_dir,
        sys.argv[1]
    ]
    
    content_lines = \
    [
        "\t" + "libraries=" + str([]) + ",",
        "\t" + "sources=" + str(sources) + ",",
        "\t" + "extra_objects=" + str(ext_objects) + ",",
        "\t" + "include_dirs=" + str(includes),
    ]
    
    setup_name = os.path.join(sys.argv[1], "python", "setup.py")
    setup_file = open(setup_name, "w")
    
    setup_file.writelines(open("setup_header.txt", "r").readlines())
    setup_file.writelines(content_lines)
    setup_file.writelines(open("setup_footer.txt", "r").readlines())
    
    setup_file.close()

#------------------------------------------------------------------------

if __name__ == "__main__":
    outfile = os.path.join(sys.argv[1], "python", "ctr.i")
    path_lib = os.path.abspath(os.path.join("..", ""))
    
    headers = [os.path.join(path_lib, h)
               for h in os.listdir(path_lib)
               if h.endswith(".h")]
    
    interface_header = open("interface_header.txt", "r").readlines()
    
    interface_header += \
        [ "%{\n", "#define SWIG_FILE_WITH_INIT\n" ] + \
        [ "#include \""+str(h)+"\"\n" for h in headers ] + \
        [ "%}\n" ]
    
    f = open(outfile, 'w')
    f.writelines(interface_header)
    
    for header_name in headers:
        cpa_header = open(header_name, 'r')
        block_comment = False
        
        for line in cpa_header.readlines():
            towrite = True
            
            if line.startswith("/*"):
                block_comment = True
            towrite = towrite & (~block_comment)
            
            if line.startswith("//"):
                towrite = False
                
            if towrite and len(line) and not line.isspace():
                f.write(line)
                
            if line.find("*/") != -1:
                block_comment = False
                
        cpa_header.close()
    f.close()
    generate_setup()
    
