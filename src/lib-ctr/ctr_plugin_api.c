/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * CTR libraries used by the plugins. It provides the functions
 * to manage the CTI repositories.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "cti_types.h"
#include "ctr_path.h"
#include "ctr_plugin_api.h"
#include "ctr_util.h"
#include "data_info.h"
#include "get_directory.h"
#include "global_index.h"

/**
 * @brief checks that a given directory is cti repository
 * 
 * @param char *path_to_rep an absolute path to the repository
 * @return int value 0 if it succeeds, 1 if it fails
 */
int ctr_plugin_check_repository(char *path_to_rep)
{
  return ctr_check_repository(path_to_rep);
}

/*------------------------------------------------------------------------ */

/**
 * @brief This functions creates a data entry in the CTR repository.
 * 
 * @param CTI_UID *plugin_uid the uid of the plugin, which called this function
 * @param enum CTR_REP_T crt a type of a repository
 * @param CTR_DATA_T cdt a type of an entry
 * @param CTI_UID *local_repository the local repository UID
 * @param CTI_UID *user_uid the user UID
 * @return cti_pair *value, key = UID, value = path, NULL if it fails
 */
cti_pair* ctr_plugin_create_entry(
      CTI_UID *plugin_uid,
      enum CTR_REP_T crt,
      enum CTR_DATA_T cdt,
      CTI_UID *local_repository,
      CTI_UID *user_uid
    )
{
    return create_ctr_entry(plugin_uid, crt, cdt, local_repository, user_uid);
}
/*------------------------------------------------------------------------ */

/**
 * @brief load the data info file according to the UID
 * 
 * @param CTI_UID *uid the uid of the data
 * @return cti_table *value a pointer to the file
 */
cti_table* ctr_plugin_info_file_load_by_uid(CTI_UID *uid)
{
    return info_file_load_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @param cti_table *data_info pointer to the data info file
 * @param char *key the key to get
 * @return char *value the value
 */
char* ctr_plugin_info_get_value(cti_table *data_info, char *key)
{
    return info_get_value(data_info, key);
}

/*------------------------------------------------------------------------ */

/**
 * @brief free the memory
 * 
 * @param cti_table *data_info pointer to the data info file
 */
void ctr_plugin_info_destruct(cti_table *data_info)
{
    info_destruct(data_info);
}

/*------------------------------------------------------------------------ */

/**
 * @param cti_table *data_info pointer to the data info file
 * @param char *value the value
 * @param char *key the key
 */
void ctr_plugin_info_put_value(cti_table *data_info, char* key, char* value)
{
    info_put_value(data_info, key, value);
}

/*------------------------------------------------------------------------ */

/**
 * @param cti_table *data_info pointer to the data info file
 * @param char *filename the filename of the file where to record
 */
void ctr_plugin_info_record_in_file(cti_table *data_info, char *filename)
{
    info_record_in_file(data_info, filename);
}

/*------------------------------------------------------------------------ */

/**
 * @return cti_table *value the new data info
 */
cti_table* ctr_plugin_info_create(void)
{
    return info_create();
}

/*------------------------------------------------------------------------ */

/**
 * @brief Add some information in the data info file
 *
 * @param int code the command result
 * @param char *cti_output_dir the output dir
 */
void ctr_plugin_info_append_data_info_file(int code, char *cti_output_dir)
{
    append_data_info_file(code, cti_output_dir);
}

/*------------------------------------------------------------------------ */

/**
 * @return char *value the CTI cfg directory, NULL if it fails
 */
char* ctr_plugin_get_cti_cfg_dir(void)
{
    return get_cti_cfg_dir();
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* uid the uid to check
 * @return char *value the absolute path to the common data, NULL if the data does not exist
 */
char* ctr_plugin_get_data_common_path_by_uid(CTI_UID *uid)
{
    return get_data_common_path_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* uid the uid to check
 * @return char *value the absolute path to the common plugin, NULL if the plugin does not exist
 */
char* ctr_plugin_get_plugin_common_path_by_uid(CTI_UID *uid)
{
    return get_plugin_common_path_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID uid the uid to check
 * @return char *value the absolute path to the temp data, NULL if the data does not exist
 */
char* ctr_plugin_get_data_temp_path_by_uid(CTI_UID *uid)
{
    return get_data_temp_path_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID uid the uid to check
 * @return char *value the absolute path to the temp plugin, NULL if the plugin does not exist
 */
char* ctr_plugin_get_plugin_temp_path_by_uid(CTI_UID *uid)
{
    return get_plugin_temp_path_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @return char * value the pointer to common plugin directory name
 */
char* ctr_plugin_get_common_plugin_dir(void)
{
    return get_plugin_common_dir();
}

/*------------------------------------------------------------------------ */

/**
 * @return char *value the pointer to common data directory name
 */
char* ctr_plugin_get_common_data_dir(void)
{
    return get_data_common_dir();
}

/*------------------------------------------------------------------------ */

/**
 * @return char *value the pointer to common directory name
 */
char* ctr_plugin_get_common_dir(void)
{
    return get_common_dir();
}

/*------------------------------------------------------------------------ */

/**
 * @return char *value the pointer to temp plugin directory name
 */
char* ctr_plugin_get_temp_plugin_dir(void)
{
    return get_plugin_temp_dir();
}

/*------------------------------------------------------------------------ */

/**
 * @return char *value the pointer to temp data directory name
 */
char* ctr_plugin_get_temp_data_dir(void)
{
    return get_data_temp_dir();
}

/*------------------------------------------------------------------------ */

/**
 * @return char *value the pointer to temp directory name
 */
char* ctr_plugin_get_temp_dir(void)
{
    return get_temp_dir();
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* ctr_uid the .ctr uid
 * @return char *value the pointer to local plugin directory name
 */
char* ctr_plugin_get_local_plugin_dir(CTI_UID* ctr_uid)
{
    return get_plugin_local_dir(ctr_uid);
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* ctr_uid the .ctr uid
 * @return char *value the pointer to local plugin directory name
 */
char* ctr_plugin_get_local_data_dir(CTI_UID* ctr_uid)
{
    return get_data_local_dir(ctr_uid);
}

/*------------------------------------------------------------------------ */

/**
 * @param enum CTR_DATA_T datatype the datatype (data / plugin)
 * @param CTI_UID* uid the uid to check
 * @return char *value the absolute path to the data or plugin, NULL if the data / plugin does not exist
 */
char* ctr_plugin_get_path_by_uid(enum CTR_DATA_T datatype, CTI_UID *uid)
{
    return get_path_by_uid(datatype, uid);
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* uid the uid to check
 * @return enum CTR_REP_T value the type of the repository
 */
enum CTR_REP_T ctr_plugin_get_repository_by_uid(enum CTR_DATA_T datatype, CTI_UID *uid)
{
    return get_repository_by_uid(datatype, uid);
}

/*------------------------------------------------------------------------ */

/**
 * @brief return the full path and the datatype (data or plugin) from an UID
 * 
 * @param CTI_UID *uid the uid to check
 * @return cti_pair* key ->full path value -> datatype
 */
cti_pair* ctr_plugin_get_path_and_type_by_uid(CTI_UID *uid)
{
    return get_path_and_type_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @brief return the full path from a plugin UID
 * 
 * @param CTI_UID *uid the uid to check
 * @return the absolute path to the plugin, NULL if the plugin does not exist
 */
char* ctr_plugin_get_plugin_path_by_uid(CTI_UID *uid)
{
    return get_plugin_path_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @brief return the full path from a data UID
 * 
 * @param CTI_UID *uid the uid to check
 * @return the absolute path to the data, NULL if the data does not exist
 */
char* ctr_plugin_get_data_path_by_uid(CTI_UID *uid)
{
    return get_data_path_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Global index file contains all the repositories which being create by the user
 * 
 * @return char ** an array of absolute paths, NULL if failed
 */
void* ctr_plugin_global_index_file_load(void)
{
    return global_index_file_load();
}

/*------------------------------------------------------------------------ */

/**
 * @brief free memory allocated for a char ** dynamic table
 * 
 * @param void *gindex_file pointer to the table
 */
void ctr_plugin_global_index_file_unload(void *gindex_file)
{
    global_index_file_unload(gindex_file);
}

/*------------------------------------------------------------------------ */

/**
 * @param void* it the iterator
 * @return void *value the iterator pointing to the first element
 */
void* ctr_plugin_global_index_file_it_begin(void *it)
{
    return global_index_file_it_begin(it);
}

/*------------------------------------------------------------------------ */

/**
 * @param void *it iterator
 * @return bool value true if it reaches the end, false otherwise
 */
bool ctr_plugin_global_index_file_it_is_end(void *it)
{
    return global_index_file_it_is_end(it);
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID *uid ctr uid
 * @return char *value the path specified by uid
 */
char* ctr_plugin_global_index_file_get_ctr_by_uid(CTI_UID *uid)
{
    return global_index_file_get_ctr_by_uid(uid);
}

/*------------------------------------------------------------------------ */

/**
 * @param char *path the path
 * @return CTI_UID *value the UID specified by path
 */
CTI_UID* ctr_plugin_global_index_file_get_uid_by_ctr(char *path)
{
    return global_index_file_get_uid_by_ctr(path);
}

/*------------------------------------------------------------------------ */

/**
 * @param void *it iterator
 * @return void *value the next element
 */
void* ctr_plugin_global_index_file_it_next(void *it)
{
    return global_index_file_it_next(it);
}

/*------------------------------------------------------------------------ */

/**
 * @param void *it iterator
 * @return char *value the absolute path to the repository
 */
char* ctr_plugin_global_index_file_it_value(void *it)
{
    return global_index_file_it_value(it);
}

/*------------------------------------------------------------------------ */

/**
 * @param void *it iterator
 * @return CTI_UID *value The UID of the repository
 */
CTI_UID* ctr_plugin_global_index_file_it_key(void *it)
{
     return global_index_file_it_key(it);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Add a new line in the global index file
 * 
 * @param char *path_to_rep path to repository
 */
void ctr_plugin_global_index_file_rm(char *path_to_rep)
{
    global_index_file_rm(path_to_rep);
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID *uid the uid
 * @param char *ctr_dir_type the direcory type
 * @return char *value the path specified by uid, NULL if it fails
 */
char* ctr_plugin_global_index_file_path_by_uid(CTI_UID *uid, char *ctr_dir_type)
{
    return global_index_file_path_by_uid(uid, ctr_dir_type);
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID *uid the uid
 * @param char *ctr_dir_type the directory type
 * @return char *value the path specified by uid, NULL if it fails
 */
char* ctr_plugin_global_index_file_ctr_by_entry_uid(CTI_UID *uid, char *ctr_dir_type)
{
    return global_index_file_ctr_by_entry_uid(uid, ctr_dir_type);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Check if a path exists or not in the global index file
 * 
 * @param char *path the path
 * @return bool value true if the path already exists, false otherwise
 */
bool ctr_plugin_global_index_file_is_there_this_path(char *path)
{
    return global_index_file_is_there_this_path(path);
}

/*------------------------------------------------------------------------ */

/**
 * @brief Add a new line in the global index file
 * 
 * @param char *path_to_rep a path to repository
 * @param CTI_UID *uid the CTR UID (UID of the repository)
 * @return int value 0 if it succeeds, error otherwise
 */
int ctr_plugin_global_index_file_write(char *path_to_rep, CTI_UID *uid)
{
    return global_index_file_write(path_to_rep, uid);
}

/*------------------------------------------------------------------------ */
