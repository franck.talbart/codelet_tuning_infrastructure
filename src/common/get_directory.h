/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef GET_DIRECTORY_H_
#define GET_DIRECTORY_H_

#include "cti_types.h"

char* get_cti_cfg_dir(void);

/* Common */
char* get_common_dir(void);
char* get_plugin_common_dir(void);
char* get_data_common_dir(void);

/* Temp */
char* get_temp_dir(void);
char* get_plugin_temp_dir(void);
char* get_data_temp_dir(void);

/* Local */
char* get_plugin_local_dir(CTI_UID* ctr_uid);
char* get_data_local_dir(CTI_UID* ctr_uid);

#endif /* GET_DIRECTORY_H_ */
