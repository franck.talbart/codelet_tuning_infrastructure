/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module provides functions that
 * return the main paths of the CTI file system
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "get_directory.h"

#include "util.h"
#include "config.h"
#include "global_index.h"


/**
 * @return CTI cfg directory, NULL if it failed
 */
char* get_cti_cfg_dir(void)
{
    char *cti_cfg_dir = getenv(CTI_CFG);
    if (cti_cfg_dir == NULL)
    {
        printf("CTI_CFG is not set or exported correctly.\n");
        exit(1);
    }
    char *result = concat_strings(cti_cfg_dir, CTI_SLASH);
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @return pointer to the common directory name
 */
char* get_common_dir(void)
{
    return config_get_value(CTR_COMMON_DIR);
}

/*------------------------------------------------------------------------*/

/**
 * @return pointer to common plugin directory name
 */
char* get_plugin_common_dir(void)
{
    char *ctr_common_dir = get_common_dir();
    char *ctr_plugin_dir = config_get_value(CTR_PLUGIN_DIR);
    char *plugin_dir = concat_strings(ctr_common_dir, 
                                      CTI_SLASH, 
                                      ctr_plugin_dir, 
                                      CTI_SLASH);
    free(ctr_common_dir), ctr_common_dir = NULL;
    free(ctr_plugin_dir), ctr_plugin_dir = NULL;
    assert(plugin_dir != NULL);
    return plugin_dir;
}

/*------------------------------------------------------------------------*/

/**
 * @return pointer to common data directory name
 */
char* get_data_common_dir(void)
{
    char *ctr_common_dir = get_common_dir();
    char *ctr_data_dir = config_get_value(CTR_DATA_DIR);
    char *data_dir = concat_strings(ctr_common_dir, 
                                    CTI_SLASH, 
                                    ctr_data_dir, 
                                    CTI_SLASH);
    assert(data_dir != NULL);
    free(ctr_common_dir), ctr_common_dir = NULL;
    free(ctr_data_dir), ctr_data_dir = NULL;
    return data_dir;
}

/*------------------------------------------------------------------------*/

/**
 * @return pointer to temp directory name
 */
char* get_temp_dir(void)
{
    return config_get_value(CTR_TEMP_DIR);
}

/*------------------------------------------------------------------------*/

/**
 * @return pointer to temp plugin directory name
 */
char* get_plugin_temp_dir(void)
{
    char *ctr_temp_dir = get_temp_dir();
    char *ctr_plugin_dir = config_get_value(CTR_PLUGIN_DIR);
    char *plugin_dir = concat_strings(ctr_temp_dir, 
                                      CTI_SLASH, 
                                      ctr_plugin_dir, 
                                      CTI_SLASH);
    assert(plugin_dir != NULL);
    free(ctr_temp_dir), ctr_temp_dir = NULL;
    free(ctr_plugin_dir), ctr_plugin_dir = NULL;
    return plugin_dir;
}

/*------------------------------------------------------------------------*/

/**
 * @return pointer to temp data directory name
 */
char* get_data_temp_dir(void)
{
    char *ctr_temp_dir = get_temp_dir();
    char *ctr_data_dir = config_get_value(CTR_DATA_DIR);
    char *data_dir = concat_strings(ctr_temp_dir, 
                                    CTI_SLASH, 
                                    ctr_data_dir, 
                                    CTI_SLASH);
    assert(data_dir != NULL);
    free(ctr_temp_dir), ctr_temp_dir = NULL;
    free(ctr_data_dir), ctr_data_dir = NULL;
    return data_dir;
}

/*------------------------------------------------------------------------*/

/**
 * @param CTI_UID* ctr_uid the .ctr uid
 * @return pointer to local plugin directory name
 */
char* get_plugin_local_dir(CTI_UID* ctr_uid)
{
    char *ctr_local_dir = global_index_file_get_ctr_by_uid(ctr_uid);
    char *ctr_plugin_dir = config_get_value(CTR_PLUGIN_DIR);
    char *plugin_dir = concat_strings(ctr_local_dir, 
                                      CTI_SLASH, 
                                      ctr_plugin_dir, 
                                      CTI_SLASH);
    assert(plugin_dir != NULL);
    free(ctr_local_dir), ctr_local_dir = NULL;
    free(ctr_plugin_dir), ctr_plugin_dir = NULL;
    return plugin_dir;
}

/*------------------------------------------------------------------------*/

/**
 * @param CTI_UID* ctr_uid the .ctr uid
 * @return pointer to local plugin directory name
 */
char* get_data_local_dir(CTI_UID* ctr_uid)
{
    char *ctr_local_dir = global_index_file_get_ctr_by_uid(ctr_uid);
    assert (ctr_local_dir != NULL);
    char *ctr_data_dir = config_get_value(CTR_DATA_DIR);
    assert (ctr_local_dir != NULL);
    char *data_dir = concat_strings(ctr_local_dir, 
                                    CTI_SLASH, 
                                    ctr_data_dir, 
                                    CTI_SLASH);
    assert(data_dir != NULL);
    free(ctr_local_dir), ctr_local_dir = NULL;
    free(ctr_data_dir), ctr_data_dir = NULL;
    return data_dir;
}

/*------------------------------------------------------------------------*/
