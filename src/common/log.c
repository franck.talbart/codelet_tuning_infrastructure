/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Antoine Darnay, Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module manages the log mechanism.
 */

/* strptime */
#define _XOPEN_SOURCE

#include <string.h>
#include <errno.h>
#include <dirent.h>

#include "log.h"
#include "data_info.h"
#include "platform.h"
#include "config.h"
#include "get_directory.h"
#include "util.h"

/**
 * @brief update last check time to time_reference
 * 
 * @param time_t time_reference the time reference
 */
static void update_last_check(time_t time_reference)
{
    char current_log[CTI_MAX_STRING_SIZE];

    /* strftime: convert time_reference to string */
    strftime(current_log, 
             CTI_MAX_STRING_SIZE, 
             "cti_%Y_%m_%d.log", 
             localtime(&time_reference));

    config_set_value(ALIAS_CURRENT_LOG, current_log);
}

/*------------------------------------------------------------------------*/

/**
 * @brief Calculate the number of days between two dates
 * 
 * @param const char* file_name the file name with format 'cti_yyyy_mm_dd.log'
 * @param time_t time_reference the time reference
 * @return int value the number of days between time_reference and file_name, 
 *          -1 if it fails
 */
static int number_of_days(char * file_name, time_t time_reference)
{
    int file_str_len = xstrnlen(file_name, CTI_MAX_STRING_SIZE);

    /* quick check on file name */
    if (file_str_len != strlen("cti_YYYY_MM_DD.log")
            || strstr(file_name, "cti_") == NULL
            || strstr(file_name, ".log") == NULL)
    {
        return -1;
    }

    struct tm date_file;

    /* convert string file_name to struct tm */
    if(strptime(file_name, "cti_%Y_%m_%d.log", &date_file) == NULL)
    {
        /* convert file name to date failed */
        return -1;
    }

    date_file.tm_sec = 0;
    date_file.tm_min = 0;
    date_file.tm_hour = 0;

    /* difftime returns elapsed time (in seconds) between two dates
     * divide it by 3600 * 24 to get the number of days */
    return (int) difftime(time_reference, mktime(&date_file)) / (3600 * 24);
}

/*------------------------------------------------------------------------*/

/*
 * @brief Checking logs only once per day
 * 
 * @param time_t time_reference the time reference
 * @return int value -1 if it fails, 1 if a check is needed for the current day (0 if not)
 */
static int time_to_check(time_t time_reference)
{
    int need_to_check = 0;
    char *current_log_str = config_get_value(ALIAS_CURRENT_LOG);

    if (current_log_str == NULL)
        return -1;
    else if (strncmp("none", current_log_str, CTI_MAX_STRING_SIZE) == 0)
        need_to_check = 1;
    else
    {
        struct tm current_log_date;
        struct tm * today;

        /* convert current_log_str to type date */
        if(strptime(current_log_str, "cti_%Y_%m_%d.log", &current_log_date) == NULL)
        {
            /* unable to convert ALIAS_CURRENT_LOG */
            free(current_log_str), current_log_str = NULL;
            return -1;
        }

        /* convert time in seconds to type date */
        today = localtime(&time_reference);

        if ((current_log_date.tm_mday != today->tm_mday)
                || (current_log_date.tm_mon != today->tm_mon)
                || (current_log_date.tm_year != today->tm_year))
        {
            /* current cti.log is now too old and needs to be replaced */
            need_to_check = 1;
        }
    }

    free(current_log_str), current_log_str = NULL;
    return need_to_check;
}

/*------------------------------------------------------------------------*/

/**
 * @brief delete olds logs
 * 
 * @param time_t time_reference the time reference
 * @return int value the number of deleted files, -1 if it fails
 */
static int delete_old_logs(time_t time_reference)
{
    int number_of_logs_deleted = 0;
    char file_to_delete[CTI_MAX_STRING_SIZE];
    char *max_time_str = config_get_value(LOGS_TIME_LIMIT);
    char *cti_cfg_dir = get_cti_cfg_dir();

    if (max_time_str == NULL || cti_cfg_dir == NULL)
    {
        /* Fail while reading cti cfg */
        if (max_time_str != NULL)
            free(max_time_str), max_time_str = NULL;

        if (cti_cfg_dir != NULL)
            free(cti_cfg_dir), cti_cfg_dir = NULL;

        return -1;
    }


    long int max_time_logs = strtol(max_time_str, NULL, 10);
    if(max_time_logs <= 0)
    {
        /* if LOGS_TIME_LIMIT value is not correct */
        free(max_time_str), max_time_str = NULL;
        free(cti_cfg_dir), cti_cfg_dir = NULL;
        return -1;
    }

    DIR * rep = opendir(cti_cfg_dir);

    if (rep == NULL)
    {
        free(max_time_str), max_time_str = NULL;
        free(cti_cfg_dir), cti_cfg_dir = NULL;
        return -1;
    }

    /* browse files in the cfg directory */
    struct dirent * ent;

    /* if max_time <= 0: return 0 */
    while ((ent = readdir(rep)) != NULL)
    {
        if (number_of_days(ent->d_name, time_reference) >= max_time_logs)
        {
            snprintf(file_to_delete, CTI_MAX_STRING_SIZE, "%s", ent->d_name);

            if (remove(file_to_delete) == 0)
                number_of_logs_deleted++;
        }
    }

    closedir(rep);
    free(max_time_str), max_time_str = NULL;
    free(cti_cfg_dir), cti_cfg_dir = NULL;
    return number_of_logs_deleted;
}

/*------------------------------------------------------------------------*/

/**
 * @brief print debug message to stdout if DEBUG compile time variable is set up.
 *
 * @param int vlevel
 * @param const char *format the message format
 */
void cti_log(int vlevel, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    cti_log_va(vlevel, format, ap);
    va_end(ap);
}

/*------------------------------------------------------------------------*/

/**
 * @brief cti_log core
 *
 * @param int vlevel
 * @param const char *format is a format just like for printf
 * @param va_list ap arguments
 */
void cti_log_va(int vlevel, const char *format, va_list ap)
{
    char *v = config_get_value(CTI_VERBOSITY);
    int verbosity = CTI_VERBOSITY_DEFAULT;

    if (v != NULL)
    {
        /* TODO: Make this code thread safe with mutex */
        errno = 0;
        verbosity = strtol(v, NULL, 10);
        if (errno || (verbosity < CTI_VERBOSITY_MIN)
                || (verbosity > CTI_VERBOSITY_MAX))
        {
            char* msg = concat_strings("Incorrect verbosity:\"", v, "\"\n");
            cti_fail(msg);
            free(msg), msg = NULL;
        }
        free(v), v = NULL;
    }

    if (vlevel <= verbosity)
    {
        char *log_file_name = config_get_value(LOG_FILE_NAME);
        char *cti_cfg_dir = get_cti_cfg_dir();

        if (!cti_cfg_dir || !log_file_name)
        {
            if (cti_cfg_dir != NULL)
                free(cti_cfg_dir), cti_cfg_dir = NULL;

            if (log_file_name != NULL)
                free(log_file_name), log_file_name = NULL;

            printf("CTI INTERNAL ERROR: CTI can't get CTI configuration directory.\n");
            exit(1);
        }

        /* path to cti.log */
        char *log_file = concat_strings(cti_cfg_dir, log_file_name);
        free(log_file_name), log_file_name = NULL;

        time_t time_reference = time(NULL);

        if (time_to_check(time_reference) == 1)
        {
            /*
             * A check is needed, cti.log is now too old for the current day
             * cti.log needs to be renamed to cti_yyyy_mm_dd.log.
             *
             * At this time, ALIAS_CURRENT_LOG is valid.
             * If it was not, time_to_check would have returned -1
             */
            char* saved_log = config_get_value(ALIAS_CURRENT_LOG);

            if (saved_log != NULL && strncmp(saved_log, "none", strlen("none")) != 0)
            {
                /*
                 * rename cti.log only if saved_log != "none".
                 * (Ex: when installing CTI, there is no previous log)
                 */
                char* saved_log_path = concat_strings(cti_cfg_dir,saved_log);
                rename(log_file, saved_log_path); /* rename(old, new) */
                delete_old_logs(time_reference);
                free(saved_log), saved_log = NULL;
                free(saved_log_path), saved_log_path = NULL;
            }
            update_last_check(time_reference);
        }
        free(cti_cfg_dir), cti_cfg_dir = NULL;

        char *vstr = xmalloc(20);

        if (vstr == NULL)
        {
            free(log_file), log_file = NULL;
            printf("CTI INTERNAL ERROR: CTI can't open log file: %s\n Errno:%d\n",
                   log_file,
                   errno);
            exit(1);
        }

        snprintf(vstr, 20, "%d", vlevel);
        char *at_time = get_current_date();
        char *debug_format = concat_strings("[",
                                            at_time,
                                            "]  ",
                                            "[VERBOSITY LEVEL: ",
                                            vstr,
                                            " ]  ",
                                            format,
                                            "\n");
        free(at_time), at_time = NULL;
        free(vstr), vstr = NULL;
        errno = 0;
        FILE *log = fopen(log_file, "a+");
        if (log == NULL)
        {
            printf("CTI INTERNAL ERROR: CTI can't open log file: %s\n Errno:%d\n",
                   log_file,
                   errno);
            exit(1);
        }
        vfprintf(log, debug_format, ap);

        if (fclose(log) == EOF)
        {
            printf("CTI INTERNAL ERROR: CTI can't close log file.\n");
            exit(1);
        }
        free(debug_format), debug_format = NULL;
        free(log_file), log_file = NULL;
    }
}

/*------------------------------------------------------------------------*/
