/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef DATA_INFO_
#define DATA_INFO_

#include <stdlib.h>

#include "cti_types.h"
#include "cti_table.h"

/* General info file functions */
cti_table* info_file_load_by_uid(CTI_UID *uid);
char* info_get_value(cti_table *data_info, char *key);
void info_destruct(cti_table *data_info);
void info_put_value(cti_table *data_info, char *key, const char *value);
void info_record_in_file(cti_table *data_info, char *filename);
cti_table* info_create(void);

/* Specific data entry functions */
void create_data_info_file(char *cti_output_dir, CTI_UID *uid_plugin, CTI_UID *user_uid);
void append_data_info_file(int code, char *cti_output_dir);

#endif /* DATA_INFO_ */
