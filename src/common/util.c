/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module provides low-level C functions
 * and is mainly used by the CTI core.
 */

#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>
#include <sys/stat.h>

#include "util.h"

#include "log.h"
#include "cti_error.h"
#include "data_info.h"
#include "platform.h"

int START_DEBUG = 0;

/**
 * @brief Get current working directory with pwd command.
 * 
 * @return char* value current directory, NULL if it fails
 */
char* get_current_dir(void)
{
    /** * @todo: path should be UTF8 string */
    FILE *current_dir = xpopen("pwd -P", "r");
    if (current_dir == NULL)
    {
        printf("Can't get current directory.\n");
        exit(CTI_ERROR_UNEXPECTED);
    }
    char *path = xmalloc(CTI_MAX_STRING_SIZE);
    /** * @todo: cover case if the current path size is bigger than allocated one. */
    path = fgets(path, CTI_MAX_STRING_SIZE - 1, current_dir);
    if (path == NULL)
    {
        printf("Can't get current directory.\n");
        exit(1);
    }
    /** * @todo add check for ferror error, if any occured.*/

    char* path_trim = xstrtrim(path);
    free(path), path = NULL;

    if (xpclose(current_dir) == -1)
    {
        printf("Can't get current directory.\n");
        exit(1);
    }
    return path_trim;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Counts character occurrences in a string.
 * 
 * @param const char *str the string
 * @param const char c the character to count
 * @return int value the number of c occurrences in str, -1 is str is NULL.
 */
int count_char(const char *str, const char c)
{
    if (str == NULL)
        return -1;

    int char_occurrences = 0;
    char *p_char = NULL;

    p_char = strchr(str, c);

    while (p_char != NULL)
    {
        char_occurrences++;
        p_char = strchr(p_char + 1, c);
    }

    return char_occurrences;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Deletes all the whitespace occurences in a given string.
 * 
 * @param const char *totrim the string
 * @return char *value NEW trimmed string, NULL if str is NULL or there is nothing but whitespace
 * @todo function leads to the memory leak, free the memory from the beggining and to the end
 */
/*
 INPUT : "hello\t  \t\n\0"
 OUTPUT: "hello\0"
 */
char* xstrtrim(const char *totrim)
{
    if (totrim == NULL)
        return NULL;

    char *str = xstrdup(totrim);

    char *start = (char*) str;
    char *end = start + xstrnlen(str, CTI_MAX_STRING_SIZE);
    char *result = NULL;

    while (!isgraph(*start) && (start < end))
        start++;
    while (!isgraph(*end) && (end > start))
        end--;
    int size = end - start + 1;
    /* str="   1  \0" */
    if (size > 0)
    {
        /* we should definitely not survive in case of a negative size */
        result = xmalloc(size + 1);
        result = strncpy(result, start, size);
        result[size] = '\0';
    }
    free(str), str = NULL;
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Escapes all quotes in a given string.
 * 
 * @param const char *str the string
 * @return char *value NEW escaped string, NULL if str is NULL.
 */
char* escape_quotes(const char *str)
{
    if (str == NULL)
        return NULL;

    char *result = NULL;
    int result_length;
    
    /* The number of quotes in the given string. */
    int number_of_simple_quotes = 0;
    int number_of_double_quotes = 0;
    
    /* Used to iterate trough the given and resulting strings. */
    int str_cursor = 0;
    int result_cursor = 0;
    char c = 0;

    number_of_simple_quotes = count_char(str, '\'');
    number_of_double_quotes = count_char(str, '\"');

    /*
     * An antislash must be added before each quote. That is why we need a
     * larger array to store the resulting string.
     */
    result_length = xstrnlen(str, CTI_MAX_STRING_SIZE)
        + number_of_simple_quotes
        + number_of_double_quotes;

    result = xmalloc((result_length + 1) * sizeof(char));
    result[result_length] = '\0';
   
    while (str_cursor < xstrnlen(str, CTI_MAX_STRING_SIZE))
    {
        c = str[str_cursor];

        /* Escaping each quote encoutered . */
        if ((c == '\'') || (c == '\"'))
        {
            result[result_cursor] = '\\';
            result[result_cursor + 1] = c;
            result_cursor += 2;
        }
        else
        {
            result[result_cursor] = c;
            result_cursor++;
        }

        str_cursor++;
    }

    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Home-made strdup.
 * 
 * @param const char *str the string to duplicate
 * @return char *value the string, NULL if it fails
 */
char* xstrdup(const char *str)
{
    if (str == NULL)
        return NULL;
    int str_size = xstrnlen(str, CTI_MAX_STRING_SIZE);
    char *result = xmalloc(str_size + 1);
    result = strncpy(result, str, str_size);
    result[str_size] = '\0';
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Home-made strndup.
 * 
 * @param const char *str the string to duplicate
 * @param const int len the string length
 * @return char *value the string, NULL if it fails
 */
char* xstrndup(const char *str, const int len)
{
    if (str == NULL)
        return NULL;
    char *result = xmalloc(len + 1);
    result = strncpy(result, str, len);
    result[len] = '\0';
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Duplicate the memory.
 * 
 * @param void *src the space to duplicate
 * @param int size the size of the memory to duplicate
 * @return void *value the new space
 */
void* memdup(void *src, int size)
{
    void* result = xmalloc(size);
    result = memcpy(result, src, size);
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Safe strlen implementation.
 *
 * @param char *str the string, the size of which we should return
 * @param int limit the limit of the string, string should be LESS than the limit.
 * @return int value the size of the string, -1 if it fails
 */
int xstrnlen(const char *str, int limit)
{
    if (str == NULL)
    {
        cti_log(1, "In %s: passed string is NULL\n", __func__);
        return -1;
    }
    int i = 0;
    while ((*(str + i) != '\0') && (i < limit))
        ++i;
    /* check if exceeded the limit */
    if (i >= limit)
    {
        cti_log(1, "String length exceeded the limit. Terminating the execution\n");
        return -1;
    }

    return i;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Create a NEW string which is result of concatenation, the last should be ALWAYS NULL.
 * 
 * @param const char* str1 the string
 * @return char *value the NEW concatenated string if succeed, NULL if it fails
 */
char* xconcat_strings(const char *str1, ...)
{
    va_list args;
    int len = 0;
    const char *str;

    va_start(args, str1);
    for (str = str1; str != NULL; str = va_arg(args, const char*))
        len += xstrnlen(str, CTI_MAX_STRING_SIZE);

    va_end(args);

    len++; /*\0 */
    if (len > CTI_MAX_STRING_SIZE)
        return NULL;

    char *result = NULL;
    char *start = NULL;

    va_start(args, str1);
    int iteration_limit = 512; /* no more than this number of iterations */
    int i = 0;
    for (str = str1; str != NULL && i < iteration_limit; str = va_arg(args, const char*), i++)
    {
        if (result != NULL)
            result = mystrncat(result, str, CTI_MAX_STRING_SIZE);
        else
        {
            result = xmalloc(len*sizeof(char));
            if (result == NULL)
                 return NULL;
            start = result;
            strncpy(result, str, len);
            result += strlen(str);
        }
    }
    va_end(args);
    return start;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Concatenate two strings.
 * 
 * @param char *dest the destination string
 * @param const char *src the source string
 * @param int limit the string limit
 * @return char *value the end of the concatenated string
 */
char* mystrncat(char *dest, const char *src, int limit)
{
    assert(limit > 0);
    int i = 0;
    while (*dest != '\0')
    {
        i++;
        if (i > limit)
        {
            printf("Exceeded the limit for string in %s.\n", __func__);
            exit(1);
        }
        dest++;
    }

    while (*src != '\0')
    {
        i++;
        if (i > limit)
        {
            printf("Exceeded the limit for string in %s\n", __func__);
            exit(1);
        }
        *dest = *src;
        dest++;
        src++;
    }
    *dest = '\0';
    return dest;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Create directory.
 * 
 * @param const char *name the full path and name of the directory to create
 * @return int value the mjdir code
 */
int xcreate_dir(const char *name)
{
    return mkdir(name, 0777);
}

/*------------------------------------------------------------------------*/

/**
 * @brief Check if string <str> is a number.
 * 
 * @param char *str the string to test
 * @return bool value true if the string is numeric, false otherwise
 */
bool is_numeric(char *str)
{
    while (*str)
    {
        if (!isdigit(*str))
            return false;
        str++;
    }
    return true;
}

/*------------------------------------------------------------------------*/

/**
 * @param const char *s the string we have to study
 * @param unsigned int start
 * @param unsigned int end
 * @return char *value the new string
 */
char *str_sub(const char *s, unsigned int start, unsigned int end)
{
    char *new_s = NULL;
    if (s != NULL && start < end)
    {
        new_s = xmalloc(end - start + 2);
        assert(new_s != NULL);
        int i;
        for (i = start; i <= end; i++)
            new_s[i - start] = s[i];
        new_s[i - start] = '\0';
    }
    return new_s;
}

/*------------------------------------------------------------------------*/

/**
 * @param char *msg failure message
 */
void cti_fail(char *msg)
{
    assert(msg != NULL);
    printf("%s", msg);
    exit(EXIT_FAILURE);
}

/*------------------------------------------------------------------------*/

/**
 * @brief Malloc wrapper.
 * 
 * @param size_t size the size
 * @return void *value the malloc result
 */
void* xmalloc(size_t size)
{
    void* ptr = malloc(size);
    if (ptr == NULL)
    {
        cti_fail(strerror(errno));
    }
    return ptr;
}

/*------------------------------------------------------------------------*/

/**
 *   @param const char *source the source file name
 *   @param const char *destination the destination file name
 *   @return int value 0 if it succeeds, error code otherwise
 */
int copy_file(const char *source, const char *destination)
{
    FILE* f_src;
    FILE* f_dest;
    char buffer[512];
    int nb_lues;
    if (source == NULL)
    {
        cti_log(1, 
                "CTI ERROR: passed source file name is NULL in function: %s",
                __func__);
        return CTI_ERROR_PATH_NULL;
    }
    if (destination == NULL)
    {
        cti_log(1,
                "CTI ERROR: passed destination file name is NULL in function: %s",
                __func__);
        return CTI_ERROR_PATH_NULL;
    }

    if ((f_src = fopen(source, "rb")) == NULL)
    {
        cti_log(1, "CTI ERROR: Can't open source file\n");
        return CTI_ERROR_CANT_OPEN_FILE;
    }

    if ((f_dest = fopen(destination, "wb")) == NULL)
    {
        cti_log(1, "CTI ERROR: Can't open destination file\n");
        fclose(f_src);
        return CTI_ERROR_CANT_OPEN_FILE;
    }

    while ((nb_lues = fread(buffer, 1, 512, f_src)) != 0)
        fwrite(buffer, 1, nb_lues, f_dest);

    fclose(f_dest);
    fclose(f_src);

    return 0;
}

/*------------------------------------------------------------------------*/

/**
 * @return char *value the current date
 */
char* get_current_date(void)
{
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    int size_date = 100;
    char *s = xmalloc(size_date);
    strftime(s, 30, "%Y/%m/%d %H:%M:%S", timeinfo);
    return s;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Return the current CTI version.
 * 
 * @return char *value the cti version X.Y.Z
 *  X -- major version
 *  Y -- minor version
 *  Z -- build number
 */
char* get_cti_version(void)
{
    return concat_strings(CTI_VERSION_MAJOR, 
                          ".", 
                          CTI_VERSION_MINOR, 
                          ".", 
                          CTI_VERSION_HOT_FIXES,
                          ".",
                          CTI_VERSION_BUILD_NUMBER);
}

/*------------------------------------------------------------------------*/

/**
 * @brief Print debug message to stdout if verbosity = 2.
 * 
 * @param const char *msg the message
 */
void debug_msg(const char *msg, ...)
{
    if (START_DEBUG)
    {
        va_list ap;
        va_start(ap, msg);
        vprintf(msg, ap);
        printf("\n");
        va_end(ap);
    }
}

/*------------------------------------------------------------------------*/
