/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef CTI_ALIAS_H_
#define CTI_ALIAS_H_

#include "cti_uid.h"

/* Data alias */
char* alias_data_get_key(CTI_UID *value);
void alias_data_set_value(const char *key, CTI_UID *value);

/* Plugin alias */
char* alias_plugin_get_key(CTI_UID *value);
CTI_UID* alias_plugin_get_value(const char *name);
void alias_plugin_set_value(const char *key, CTI_UID *value, char *dir);
int alias_plugin_rm_value(CTI_UID *value);

/* Repository alias */
char* alias_repository_get_key(CTI_UID *value);
CTI_UID* alias_repository_get_value(const char *name);
int alias_repository_rm_value(CTI_UID *value);
void alias_repository_set_value(const char *key, CTI_UID *value);

#endif /* CTI_ALIAS_H_ */
