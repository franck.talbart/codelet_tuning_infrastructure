/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module provides functions to set and get
 * values in the ctr_info file
 *
 */
#include <stdio.h>
#include <assert.h>

#include "data_info.h"

#include "cti_error.h"
#include "ctr_path.h"
#include "log.h"
#include "util.h"
#include "config.h"
#include "cti_uid.h"

/**
 * @brief load the data info file according to the UID
 * 
 * @param CTI_UID *uid the uid of the data
 * @return a pointer to the file
 */
cti_table* info_file_load_by_uid(CTI_UID *uid)
{
    cti_pair *path_type = get_path_and_type_by_uid (uid);
    if (path_type == NULL)
    {
        cti_log(1, "[CTI INTERNAL ERROR] UID is wrong.\n");
        return NULL;
    }

    char *dir = path_type->key;
    if (dir == NULL)
        return NULL;

    char *data_info_name = config_get_value(DATA_INFO_FILENAME);
    if (data_info_name == NULL)
        return NULL;
    char *dir_info = concat_strings(dir, CTI_SLASH, data_info_name);
    free(dir), dir = NULL;
    free(data_info_name), data_info_name = NULL;
    void *result = cti_table_load_from_file(dir_info);
    free(dir_info), dir_info = NULL;
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @param cti_table *data_info pointer to the data info file
 * @param char *key the key to get
 * @return the value
 */
char* info_get_value(cti_table *data_info, char *key)
{
    return cti_table_get_value(data_info, key);
}

/*------------------------------------------------------------------------*/

/**
 * @brief free the memory
 * 
 * @param cti_table *data_info pointer to the data info file
 */
void info_destruct(cti_table *data_info)
{
    assert(data_info != NULL);
    cti_table_destruct(data_info);
}

/*------------------------------------------------------------------------*/

/**
 * @brief warning: user should be very careful with this function, as it should be only
 *      used in specific cases
 * 
 * @param cti_table *data_info pointer to the data info file
 * @param char *value the value
 * @param char *key the key
 */
void info_put_value(cti_table *data_info, char *key, const char *value)
{
    int v_size = xstrnlen(value, CTI_MAX_STRING_SIZE) + 1;
    cti_table_put_value(data_info, key, (void*)value, v_size);
}

/*------------------------------------------------------------------------*/

/**
 * @param cti_table *data_info pointer to the data info file
 * @param char *filename the filename of the file where to record
 */
void info_record_in_file(cti_table *data_info, char *filename)
{
    cti_table_record_in_file(data_info, filename);
}

/*------------------------------------------------------------------------*/

/**
 * @return the new data info
 */
cti_table* info_create(void)
{
    return cti_table_create(DATA_INFO_INITIAL_SIZE, NULL);
}

/*------------------------------------------------------------------------*/

/**
 * @brief create the data info file
 * 
 * @param char *cti_output_dir the CTR output dir
 * @param CTI_UID* uid_plugin the plugin UID
 * @param CTI_UID *user_uid the user UID
 */
void create_data_info_file(char *cti_output_dir, CTI_UID *uid_plugin, CTI_UID *user_uid)
{
    char *ctr_info_name = config_get_value(DATA_INFO_FILENAME);
    if (ctr_info_name == NULL)
    {
        printf("Can't get %s from the config file\n", DATA_INFO_FILENAME);
        exit(CTI_ERROR_UNEXPECTED);
    }
    ctr_info_name = concat_strings(cti_output_dir, CTI_SLASH, ctr_info_name);
    DEBUG_MSG("CTR info filename: %s\n", ctr_info_name);

    void *data_info = info_create();

    char *str_uid_plugin = cti_uid_to_str(uid_plugin);
    info_put_value(data_info, DATA_INFO_PLUGIN_UID, str_uid_plugin);
    free(str_uid_plugin), str_uid_plugin = NULL;
    char *current_date = get_current_date();
    info_put_value(data_info, DATA_INFO_DATE_TIME_START, current_date);

    /* user uid */
    char *str_user_uid = cti_uid_to_str(user_uid);
    cti_table_put_value(data_info, 
                        DATA_INFO_USER_UID, 
                        str_user_uid,
                        xstrnlen(str_user_uid, CTI_MAX_STRING_SIZE) + 1);

    info_record_in_file(data_info, ctr_info_name);

    free(str_user_uid), str_user_uid = NULL;
    free(current_date), current_date = NULL;
    info_destruct(data_info);
}

/*------------------------------------------------------------------------*/

/**
 * @brief Add some information in the data info file
 * 
 * @param int code the command result
 * @param char *cti_output_dir the output dir
 */
void append_data_info_file(int code, char *cti_output_dir)
{
    char *ctr_info_name = config_get_value(DATA_INFO_FILENAME);
    if (ctr_info_name == NULL)
    {
        printf("Can't get %s from the config file\n", DATA_INFO_FILENAME);
        exit(CTI_ERROR_UNEXPECTED);
    }

    char *old_ctr_info_name = ctr_info_name;
    ctr_info_name = concat_strings(cti_output_dir, CTI_SLASH, ctr_info_name);
    free(old_ctr_info_name), old_ctr_info_name = NULL;
    cti_table* ctr_info = cti_table_load_from_file(ctr_info_name);

    /* End date */
    char *current_date = get_current_date();
    cti_table_put_value(ctr_info, 
                        DATA_INFO_DATE_TIME_END, 
                        current_date,
                        xstrnlen(current_date, CTI_MAX_STRING_SIZE) + 1);
    free(current_date), current_date = NULL;

    /* command result */
    char *command_result_char = xmalloc(12);
    assert(command_result_char != NULL);
    sprintf(command_result_char, "%d", code);
    cti_table_put_value(ctr_info, 
                        DATA_INFO_PLUGIN_EXIT_CODE, 
                        command_result_char,
                        xstrnlen(command_result_char, CTI_MAX_STRING_SIZE) + 1);
    free(command_result_char), command_result_char = NULL;

    cti_table_record_in_file(ctr_info, ctr_info_name);
    free(ctr_info_name), ctr_info_name = NULL;
    cti_table_destruct(ctr_info);
}

/*------------------------------------------------------------------------*/
