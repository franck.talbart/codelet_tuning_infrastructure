/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * The config module provides functions
 * to set and get values in the config file.
 *
 */

#include <stdlib.h>

#include "config.h"

#include "cti_table.h"
#include "util.h"
#include "get_directory.h"
#include "log.h"

/**
 * @return string with full path to the config file
 */
static char *config_get_full_path(void)
{
    /* Config filename */
    char *cfg_dir = get_cti_cfg_dir();
    /* Full path */
    char *cfg_file = concat_strings(cfg_dir, CFG_FILE_NAME);
    free(cfg_dir), cfg_dir = NULL;
    return cfg_file;
}

/*------------------------------------------------------------------------ */

/**
 * @return pointer to the cti_table structure, NULL if failed
 */
static cti_table* config_load_table(void)
{
    /* Config file full path */
    char *cfg_file = config_get_full_path();
    cti_table *ct = cti_table_load_from_file(cfg_file);
    free(cfg_file), cfg_file = NULL;
    return ct;
}

/*------------------------------------------------------------------------ */

/**
 * @param const char *key   -- a key to set
 * @param const char *value -- a value
 */
void config_set_value(const char *key, char *value)
{
    cti_log(2, concat_strings("in ", __func__, " key = ", key, " value = ", value));

    if (key == NULL)
    {
        cti_log(1, "Can't get config value, because key is NULL\n");
        exit(1);
    }

    DEBUG_MSG("Get value from the configuration file by key:#%s#\n", key);

    /* Load the config file */
    cti_table *config_table = config_load_table();

    if (config_table != NULL)
    {
        int v_size = xstrnlen(value, CTI_MAX_STRING_SIZE) + 1;

        /* Add the value */
        cti_table_put_value(config_table, key, value, v_size);
        char *cfg_file = config_get_full_path();

        /* Record the file */
        cti_table_record_in_file(config_table, cfg_file);
        
        /* Free */
        free(cfg_file), cfg_file = NULL;
        cti_table_destruct(config_table);
    }
}

/*------------------------------------------------------------------------ */

/**
 * @param const char *key
 * @return char* value a given key, NULL if failed
 */
char* config_get_value(const char *key)
{
    if (key == NULL)
    {
        cti_log(1, "Can't get config value, because key is NULL\n");
        return NULL;
    }
    DEBUG_MSG("Get value from the configuration file by key:#%s#\n", key);

    /* Load the config file */
    cti_table *config_table = config_load_table();

    char *result = NULL;
    if (config_table != NULL)
    {
        result = cti_table_get_value(config_table, key);
        cti_table_destruct(config_table);
    }
    return result;
}

/*------------------------------------------------------------------------ */
