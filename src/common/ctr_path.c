/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module provides functions to get paths
 * of internal CTI directories *
 */

#include <assert.h>
#include <string.h>
#include <sys/stat.h>

#include "ctr_path.h"

#include "global_index.h"
#include "get_directory.h"
#include "data_info.h"
#include "log.h"
#include "util.h"
#include "cti_uid.h"

/**
 * @brief cleans up a path by removing redundant slashes
 * @param char* path
 * @returns char*, canonical path
 */
static char* canonical_path(const char * path)
{
    int n = strlen(path);
    char * clean_path = xmalloc(sizeof(char)*n + 1);
    int j = 0;
    int last_was_slash = 0;
    for(int i=0; i<n; i++)
    {
        if (path[i] == CTI_SLASH[0])
        {
            /* Only output one slash among a group of
               consecutive ones */
            if (!last_was_slash) 
                clean_path[j++] = CTI_SLASH[0];
            last_was_slash = 1;
        }
        else
        {
            clean_path[j++] = path[i];
            last_was_slash = 0;
        }
    }
    /* remove last slash if present */
    if (clean_path[j-1] == CTI_SLASH[0]) j--;
    clean_path[j] = '\0';
    return clean_path;
}

/*------------------------------------------------------------------------ */

/**
 * @brief returns the full path and the datatype (data or plugin) from an UID
 * @param CTI_UID *uid the uid to check
 * @return cti_pair* key ->full path value -> datatype, NULL if not found
 */
cti_pair* get_path_and_type_by_uid(CTI_UID *uid)
{
    cti_pair* result = xmalloc(sizeof(cti_pair));
    result->value = NULL;

    /* We check if it's a plugin */
    char* path = get_plugin_path_by_uid(uid);
    if(path != NULL)
    {
        int tmp = CTR_ENTRY_PLUGIN;
        result->value = memdup(&tmp, sizeof(enum CTR_DATA_T));
    }
    else
    {
        path = get_data_path_by_uid(uid);
        if(path != NULL)
        {
            /* We check if it's a data */
            int tmp = CTR_ENTRY_DATA;
            result->value = memdup(&tmp, sizeof(enum CTR_DATA_T));
        }
        else
            return NULL;
    }
    result->key = path;

    return result;
}

/*------------------------------------------------------------------------ */

/**
 * @param enum CTR_DATA_T datatype the datatype (data / plugin)
 * @param CTI_UID* uid the uid to check
 * @return the absolute path to the data or plugin, NULL if the data / plugin does not exist
 */
char* get_path_by_uid(enum CTR_DATA_T datatype, CTI_UID *uid)
{
    if (datatype == CTR_ENTRY_PLUGIN)
        return get_plugin_path_by_uid(uid);
    else if (datatype == CTR_ENTRY_DATA)
        return get_data_path_by_uid(uid);
    else
        return NULL;
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* uid the uid to check
 * @return type of the repository
 */
enum CTR_REP_T get_repository_by_uid(enum CTR_DATA_T datatype, CTI_UID *uid)
{
    char *dir = NULL;
    enum CTR_REP_T result;
    /* If the entry is a plugin */
    if (datatype == CTR_ENTRY_PLUGIN)
    {
        /* Is it in a local repository? */
        dir = global_index_file_path_by_uid(uid, CTR_PLUGIN_DIR);
        if (dir == NULL)
        {
            /* Is it in the common repository */
            dir = get_plugin_common_path_by_uid(uid);
            if (dir == NULL)
            {
                /* Is it in the temp repository */
                dir = get_plugin_temp_path_by_uid(uid);
                if (dir == NULL)
                {
                    cti_log(1, "can't find UID");
                    return -1;
                }
                result = CTR_REP_TEMP;
            }
            else
                result = CTR_REP_COMMON;
        }
        else
            result = CTR_REP_LOCAL;
    }
    /* If the entry is a data */
    else if (datatype == CTR_ENTRY_DATA)
    {
        /* Is it in a local repository? */
        dir = global_index_file_path_by_uid(uid, CTR_DATA_DIR);
        if (dir == NULL)
        {
            /* Is it in a common repository */
            dir = get_data_common_path_by_uid(uid);
            if (dir == NULL)
            {
                /* Is it in a temp repository */
                dir = get_data_temp_path_by_uid(uid);
                if (dir == NULL)
                {
                    cti_log(1, "can't find UID");
                    return -1;
                }
                result = CTR_REP_TEMP;
            }
            else
                result = CTR_REP_COMMON;
        }
        else
            result = CTR_REP_LOCAL;
    }
    else
    {
        if (dir == NULL) 
            cti_log(1, "datatype unknown");
        return -1;
    }
    return result;
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* uid the uid to check
 * @return the absolute path to the plugin, NULL if the plugin does not exist
 */
char* get_plugin_path_by_uid(CTI_UID *uid)
{
    /* Local repository */
    char *dir = global_index_file_path_by_uid(uid, CTR_PLUGIN_DIR);
    if (dir == NULL)
    {
        /* Common repository */
        dir = get_plugin_common_path_by_uid(uid);
        if (dir == NULL)
        {
            /* Temp repository */
            dir = get_plugin_temp_path_by_uid(uid);
        }
    }

    if (dir == NULL)
        cti_log(1, "can't find UID");
    return dir;
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* uid the uid to check
 * @return the absolute path to the data, NULL if the data does not exist
 */
char* get_data_path_by_uid(CTI_UID *uid)
{
    /* Local repository */
    char *dir = global_index_file_path_by_uid(uid, CTR_DATA_DIR);
    if (dir == NULL)
    {
        /* Common repository */
        dir = get_data_common_path_by_uid(uid);
        if (dir == NULL)
        {
            /* Temp repository */
            dir = get_data_temp_path_by_uid(uid);
        }
    }

    if (dir == NULL)
        cti_log(1, "can't find UID");
    return dir;
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID* uid the uid to check
 * @return the absolute path to the common plugin, NULL if the plugin does not exist
 */
char* get_plugin_common_path_by_uid(CTI_UID *uid)
{
    /* Plugin common repository */
    char *plugin_common_dir = get_plugin_common_dir();
    char* str_uid = cti_uid_to_str(uid);
    char *path = concat_strings(plugin_common_dir, str_uid);
    free(str_uid), str_uid = NULL;
    free(plugin_common_dir), plugin_common_dir = NULL;
    assert(path != NULL);

    struct stat st;
    /* Check that the path does exist */
    if (stat(path, &st) == 0)
    {
        char * canonical = canonical_path(path);
        free(path), path = NULL;
        return canonical;
    }

    free(path), path = NULL;
    return NULL;
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID *uid the uid to check
 * @return the absolute path to the common data, NULL if the data does not exist
 */
char* get_data_common_path_by_uid(CTI_UID *uid)
{
    /* Data common repository */
    char *data_common_dir = get_data_common_dir();
    char* str_uid = cti_uid_to_str(uid);
    char *path = concat_strings(data_common_dir, str_uid);
    free(str_uid), str_uid = NULL;
    free(data_common_dir), data_common_dir = NULL;
    assert(path != NULL);

    struct stat st;

    /* Check that the path does exist */
    if (stat(path, &st) == 0)
    {
        char * canonical = canonical_path(path);
        free(path), path = NULL;
        return canonical;
    }

    free(path), path = NULL;
    return NULL;
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID uid the uid to check
 * @return the absolute path to the temp plugin, NULL if the plugin does not exist
 */
char* get_plugin_temp_path_by_uid(CTI_UID *uid)
{
    /* Plugin temp repository */
    char *plugin_temp_dir = get_plugin_temp_dir();
    char* str_uid = cti_uid_to_str(uid);
    char *path = concat_strings(plugin_temp_dir, str_uid);
    free(str_uid), str_uid = NULL;
    free(plugin_temp_dir), plugin_temp_dir = NULL;
    assert(path != NULL);

    struct stat st;
    /* Check that the full path does exist */
    if (stat(path, &st) == 0)
    {
        char * canonical = canonical_path(path);
        free(path), path = NULL;
        return canonical;
    }

    free(path), path = NULL;
    return NULL;
}

/*------------------------------------------------------------------------ */

/**
 * @param CTI_UID uid the uid to check
 * @return the absolute path to the temp data, NULL if the data does not exist
 */
char* get_data_temp_path_by_uid(CTI_UID *uid)
{
    /* Data temp repository */
    char *data_temp_dir = get_data_temp_dir();
    char* str_uid = cti_uid_to_str(uid);
    char *path = concat_strings(data_temp_dir, str_uid);
    free(str_uid), str_uid = NULL;
    free(data_temp_dir), data_temp_dir = NULL;
    assert(path != NULL);

    struct stat st;
    /* Check that the path does exist */
    if (stat(path, &st) == 0)
    {
        char * canonical = canonical_path(path);
        free(path), path = NULL;
        return canonical;
    }
    free(path), path = NULL;
    return NULL;
}

/*------------------------------------------------------------------------ */
