/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef CTR_UTIL_
#define CTR_UTIL_

#include "cti_types.h"

int ctr_check_repository(char *path_to_rep);
cti_pair* create_ctr_entry(CTI_UID *plugin_uid, 
                            enum CTR_REP_T crt,
                            enum CTR_DATA_T cdt, 
                            CTI_UID *local_repository, 
                            CTI_UID *user_uid);
int ctr_is_tracked(char *path_to_rep);

#endif /* CTR_UTIL_ */
