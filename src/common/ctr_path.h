/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef CTR_PATH_
#define CTR_PATH_

#include "cti_types.h"

cti_pair* get_path_and_type_by_uid(CTI_UID *uid);
char* get_path_by_uid(enum CTR_DATA_T datatype, CTI_UID *uid);
enum CTR_REP_T get_repository_by_uid(enum CTR_DATA_T datatype, CTI_UID *uid);
char* get_plugin_path_by_uid(CTI_UID *uid);
char* get_data_path_by_uid(CTI_UID *uid);

/* These functions can be used if you want to improve the performance and you are sure
   about the type of the repository */
char* get_plugin_common_path_by_uid(CTI_UID *uid);
char* get_data_common_path_by_uid(CTI_UID *uid);

char* get_plugin_temp_path_by_uid(CTI_UID *uid);
char* get_data_temp_path_by_uid(CTI_UID *uid);

#endif /* CTR_PATH_ */
