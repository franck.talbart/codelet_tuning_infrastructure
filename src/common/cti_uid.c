/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module provides functions to generate UID,
 * convert it to a string, check it, etc...
 */

#include <stdlib.h>
#include <string.h>

#include "cti_uid.h"

#include "log.h"
#include "util.h"
#include "platform.h"
#include "config.h"

/**
 * @return uuid, null-terminated array of UID_NUMBER_OF_SYMBOL chars, NULL if failed.
 */
CTI_UID* cti_generate_cti_uid(void)
{
    /* Get the uuid binary name */
    char *uuid_tool_filename = config_get_value(UID_TOOL);
    
    if (uuid_tool_filename == NULL)
    {
        cti_log(1, "UUID tool filename not found in the config file.\n");
        return NULL;
    }
    
    /* Run the UUID tool */
    FILE *uuid_tool = xpopen(uuid_tool_filename, "r");
    
    if (uuid_tool == NULL)
    {
        cti_log(1, "xpopen UUID failed\n");
        return NULL;
    }
    
    char *result_xpopen = xmalloc(UID_NUMBER_OF_SYMBOL + 1);
    
    /* Get the output */
    result_xpopen = fgets(result_xpopen, UID_NUMBER_OF_SYMBOL + 1 , uuid_tool);
    
    if (result_xpopen == NULL)
    {
        cti_log(1, "fgets xpopen failed\n");
        printf("CTI ERROR: uuid tool not found or broken: %s. Please update the config file\n", uuid_tool_filename);
        return NULL;
    }
    
    result_xpopen[UID_NUMBER_OF_SYMBOL] = '\0';
    
    /* If xpopen fails */
    if (result_xpopen == NULL)
    {
        cti_log(1, "CTI_UID generation failed. xpopen file error\n");
        return NULL;
    }
    
    int res;
    if ((res = xpclose(uuid_tool)) == -1)
    {
        printf("CTI_UID generation failed. Can't close file, xpclose failed\n");
        exit(1);
    }
    
    free(uuid_tool_filename), uuid_tool_filename;
    
    CTI_UID* result = str_to_cti_uid(result_xpopen);
    free(result_xpopen), result_xpopen = NULL;
    return result;
}

/*------------------------------------------------------------------------ */

/**
 * @brief Check that a given string is an array of 36th characters.
 * @param char *uid the uid
 * @return true if it uses the good format, false otherwise
 */
bool is_UID(char *uid)
{
    if (uid == NULL)
    {
        cti_log(1, "uid is NULL");
        return false;
    }
    
    /* check the number of symbol */
    if(xstrnlen(uid, UID_NUMBER_OF_SYMBOL + 1) != UID_NUMBER_OF_SYMBOL)
    {
        cti_log(1, "UID has not %d symbols: %s", UID_NUMBER_OF_SYMBOL, uid);
        return false;
    }
    
    /* Check if "-" at the following positions: 9 14 19 24 */
    int position_minus[UID_NUMBER_OF_MINUS] = {8, 13, 18, 23};
    
    for (int cpt = 0; cpt < UID_NUMBER_OF_MINUS; cpt++)
    {
        if (uid[position_minus[cpt]] != '-')
            return false;
    }

    return true;
}

/*------------------------------------------------------------------------ */

/**
 * @brief convert a CTI_UID into string
 * @param CTI_UID *uid the UID to convert
 * @return the string, or NULL in case of failure, writing the error reason to the CTI standard log.
 */
char* cti_uid_to_str(CTI_UID *uid)
{
    if(uid == NULL) return NULL;
    char* result = xmalloc(UID_NUMBER_OF_SYMBOL + 1);
    strncpy(result, uid->data, UID_NUMBER_OF_SYMBOL);
    result[UID_NUMBER_OF_SYMBOL] = '\0';
    return result;
}

/*------------------------------------------------------------------------ */

/**
 * @brief convert a string into a CTI_UID
 * @param char *m_str the string to convert
 * @return the UID, or NULL in case of failure, writing the error reason to the CTI standard log.
 */
CTI_UID* str_to_cti_uid(char *m_str)
{
    if(!is_UID(m_str))
    {
        cti_log(1, "[CTI MESSAGE] %s: m_str is not an UID", __func__);
        return NULL;
    }
    
    CTI_UID* result = xmalloc(sizeof(CTI_UID));
    char *sr = strncpy(result->data, m_str, UID_NUMBER_OF_SYMBOL);
    
    if (sr == NULL)
    {
        cti_log(1, "[CTI INTERNAL MESSAGE] %s: strncpy failed to copy the m_str to result->data", __func__);
        return NULL;
    }
    return result;
}

/*------------------------------------------------------------------------ */
