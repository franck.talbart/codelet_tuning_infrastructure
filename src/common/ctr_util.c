/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module contains functions that manages repositories
 * and entries
 */


#include <string.h>
#include <assert.h>
#include <sys/stat.h>

#include "ctr_util.h"

#include "log.h"
#include "cti_types.h"
#include "global_index.h"
#include "util.h"
#include "config.h"
#include "get_directory.h"
#include "platform.h"
#include "data_info.h"
#include "cti_uid.h"


/**
 * @brief Predicates: common? or in global index file?
 * 
 * @param char *path_to_rep a path to check
 * @return int value 1 if it succeeds, 0 if it fails
 */
int ctr_is_tracked(char *path_to_rep)
{
    if (path_to_rep == NULL)
    {
        cti_log(1, "[CTI INTERNAL ERROR] attempted to check repository for NULL variable.\n");
        return 0;
    }
    int path_size = xstrnlen(path_to_rep, CTI_MAX_STRING_SIZE);
    if (path_size < 0)
    {
        cti_log(1, "[CTI ERROR] Specified path is incorrect.\n");
        return 0;
    }
    int code_ret = 1;
    /* check repository is in the list of repositories */
    if (!global_index_file_is_there_this_path(path_to_rep))
    {
        /* is it a common repository? */
        char *common_dir = get_common_dir();
        if (strcmp(common_dir, path_to_rep))
        {
            char *temp_dir = get_temp_dir();
            if (strcmp(temp_dir, path_to_rep))
            {
                cti_log(1, "[CTI INTERNAL ERROR] Can't find repository in the tracked list of repositories.\n"
                        "Check global_index_file. \n");
                code_ret = 0;
            }
            free(temp_dir), temp_dir = NULL;
        }
        free(common_dir), common_dir = NULL;
    }
    return code_ret;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Checks if specified directory is a CTR rep or not.
 * 
 * @param char* path_to_rep -- a path to check
 * @return int value 0 if it succeeds, 1 if it fails
 */
int ctr_check_repository(char *path_to_rep)
{
    /* check parameter */
    if (path_to_rep == NULL)
    {
        cti_log(1, "[CTI INTERNAL ERROR] attempted to check repository for NULL variable.\n");
        return 1;
    }
    int path_size = xstrnlen(path_to_rep, CTI_MAX_STRING_SIZE);
    if (path_size < 0)
    {
        cti_log(1, "[CTI ERROR] Specified path is incorrect.\n");
        return 1;
    }
    
    /* check plugins directory */
    char *ctr_plugin_dir = config_get_value(CTR_PLUGIN_DIR);
    if (ctr_plugin_dir == NULL)
    {
        cti_log(1, "[CTI INTERNAL ERROR] Can't get CTR_PLUGIN_DIR from config file.\n");
        return 1;
    }
    int pstr_size = xstrnlen(ctr_plugin_dir, CTI_MAX_STRING_SIZE);
    if (pstr_size < 0)
    {
        cti_log(1, "[CTI INTERNAL ERROR] CTR_PLUGIN_DIR is incorrect.\n");
        free(ctr_plugin_dir), ctr_plugin_dir = NULL;
        return 1;
    }
    char *path_to_plugin_dir = xmalloc(path_size + 1 + pstr_size + 1);
    sprintf(path_to_plugin_dir, "%s%s%s", path_to_rep, CTI_SLASH, ctr_plugin_dir);
    free(ctr_plugin_dir), ctr_plugin_dir = NULL;
    struct stat pstatbuf;
    if (stat(path_to_plugin_dir, &pstatbuf) == -1)
    {
        cti_log(1, "[CTI INTERNAL ERROR] Plugin dir doesn't exists.\n");
        free(path_to_plugin_dir), path_to_plugin_dir = NULL;
        return 1;
    }
    else
    {
        if (!S_ISDIR(pstatbuf.st_mode))
        {
            cti_log(1, "[CTI INTERNAL ERROR] Plugins is not a directory.\n");
            free(path_to_plugin_dir), path_to_plugin_dir = NULL;
            return 1;
        }
    }
    free(path_to_plugin_dir), path_to_plugin_dir = NULL;
  
    /* check data directory */
    char *ctr_data_dir = config_get_value(CTR_DATA_DIR);
    if (ctr_data_dir == NULL)
    {
        cti_log(1, "[CTI INTERNAL ERROR] Can't get CTR_DATA_DIR from config file.\n");
        return 1;
    }
    int dstr_size = xstrnlen(ctr_data_dir, CTI_MAX_STRING_SIZE + 1);
    if (dstr_size < 0)
    {
        cti_log(1, "[CTI INTERNAL ERROR] Data directory is incorrect.\n");
        return 1;
    }
    char *path_to_data_dir = xmalloc(path_size + 1 + dstr_size + 1);
    sprintf(path_to_data_dir, "%s%s%s", path_to_rep, CTI_SLASH, ctr_data_dir);
    free(ctr_data_dir), ctr_data_dir = NULL;
    struct stat dstatbuf;
    
    int code_ret = 0;
    if (stat(path_to_data_dir, &dstatbuf) == -1)
    {
        cti_log(1, "[CTI INTERNAL ERROR] Data dir doesn't exists.\n");
        code_ret = 1;
    }
    else
    {
        if (!S_ISDIR(dstatbuf.st_mode))
        {
            cti_log(1, "[CTI INTERNAL ERROR] Data is not a directory.\n");
            code_ret = 1;
        }
    }
    free(path_to_data_dir), path_to_data_dir = NULL;
    return code_ret;
}

/*------------------------------------------------------------------------*/

/**
 * @brief This functions creates a data entry in the CTR repository.
 * 
 * @param CTI_UID plugin_uid A the uid of the plugin, which called this function
 * @param CTR_DATA_T cdt A type of an entry.
 * @param enum CTR_REP_T crt A type of a repository.
 * @param CTI_UID *local_repository the local repository UID
 * @param CTI_UID *user_uid the user UID
 * @return cti_pair, key = UID, value = path, NULL if it fails
 */
cti_pair* create_ctr_entry(CTI_UID *plugin_uid,
                           enum CTR_REP_T crt,
                           enum CTR_DATA_T cdt,
                           CTI_UID *local_repository,
                           CTI_UID *user_uid
                           )
{
    char *result = NULL;

    switch (crt)
    {
        case CTR_REP_LOCAL:
        {
            if (local_repository != NULL)
                result = global_index_file_get_ctr_by_uid(local_repository);
            else
            {
                char *cur_dir = get_current_dir();
                char *local_rep = config_get_value(DOT_CTR);
                result = concat_strings(cur_dir, CTI_SLASH, local_rep);
                free(cur_dir), cur_dir = NULL;
                free(local_rep), local_rep = NULL;
            }
            break;
        }
        case CTR_REP_COMMON:
        {
            result = config_get_value(CTR_COMMON_DIR);
            break;
        }
        case CTR_REP_TEMP:
        {
            result = config_get_value(CTR_TEMP_DIR);
            break;
        }
        default:
        {
            cti_log(1, "Can't recognize enum CTR_REP_T in %s", __func__);
            return NULL;
            break;
        }
    }

    /* check repository */
    if (ctr_check_repository(result) || !ctr_is_tracked(result))
    {
        printf("CTI ERROR: Repository validation failed. \n "
                "Hint: try to import the repository with \"cti repository import\"\n");
        return NULL;
    }

    int data_recognize = 1;
    char *entry_dir;
    /* TODO: here we should normalize path stored in result variable */
    switch (cdt)
    {
        case CTR_ENTRY_DATA:
        {
            entry_dir = config_get_value(CTR_DATA_DIR);
            break;
        }
        case CTR_ENTRY_PLUGIN:
        {
            entry_dir = config_get_value(CTR_PLUGIN_DIR);
            break;
        }
        default:
        {
            cti_log(1, "Can't recognize CTR_DATA_T in %s", __func__);
            data_recognize = 0;
            break;
        }
    }
    if (data_recognize == 0)
        return NULL;
    else
    {
        result = concat_strings(result, CTI_SLASH, entry_dir);
        free(entry_dir), entry_dir = NULL;
    }
    
    /* generate the UID */
    CTI_UID *uid = cti_generate_cti_uid();
    if (uid == NULL)
    {
        cti_log(1, "[CTI INTERNAL ERROR] Can't generate UID.\n");
        return NULL;
    }
    char *str_uid = cti_uid_to_str(uid);
    result = concat_strings(result, CTI_SLASH, str_uid);
    int xcd = xcreate_dir(result);
    if (xcd != 0)
    {
        cti_log(1, "Can't create CTR entry: %s", result);
        free(str_uid), str_uid = NULL;
        free(uid), uid = NULL;
        return NULL;
    }
    cti_pair *pair = xmalloc(sizeof(cti_pair));
    pair->key = str_uid;
    pair->value = result;
    
    /* Let's write the ctr info file */
    create_data_info_file(result, plugin_uid, user_uid);
    assert(uid != NULL);
    free(uid), uid = NULL;
    return pair;
}

/*------------------------------------------------------------------------*/
