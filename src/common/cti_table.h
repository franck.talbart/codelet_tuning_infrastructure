/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/* General table implementation. */

#ifndef CTI_TABLE_H_
#define CTI_TABLE_H_

#include "cti_types.h"

int cti_table_find_index(cti_table *table, const char *key);
void cti_table_put_value(cti_table *table, const char *key, void *value,
                                                                   int size);
void cti_table_destruct(cti_table *table);
void *cti_table_get_value(cti_table *table, const char *key);
cti_table* cti_table_create(int initial_size, void *ptr_function_free_memory);
cti_table* cti_table_load_from_file(const char *filename);
void cti_table_record_in_file(cti_table *table, const char* filename);
int cti_table_find_index_value(cti_table *table, const char *value, int start_index);
char* cti_table_get_key_by_index(cti_table *table, int cpt);
int cti_table_rm_value(cti_table *table, const char *value);

#endif /* CTI_TABLE_H_ */
