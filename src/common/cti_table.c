/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * The cti_table provides a dictionary (key = value)
 * and a set of functions to manage it (add, remove, find, etc...)
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <errno.h>

#include "cti_table.h"

#include "util.h"

/**
 * @param cti_table *table the table
 * @param const char *key
 * @return index if succeed, -1 if fails
 */
int cti_table_find_index (cti_table * table, const char *key)
{
    assert (table != NULL);
    assert (key != NULL);
    DEBUG_MSG ("\nSEARCHING: key:#%s#, stored:#%d#\n", key, table->stored);

    /* Browse the table */
    for (int idx = 0; idx < table->stored; ++idx)
    {
        DEBUG_MSG ("Going to compare: #%s#  \t with #%s#", 
                    key, 
                    table->data[idx]->key);
        if (strcmp (table->data[idx]->key, key) == 0)
        { /* Returns the index */
            return idx;
        }
    }
    DEBUG_MSG ("Key #%s# is not found", key);
    return -1;
}

/*------------------------------------------------------------------------ */

/**
 * @param cti_table *table
 * @param const char *key the key
 * @param const char *value the value
 * @param int size the size
 * @todo: [B] improve return parameter, i.e. non-zero if filed to put.
 * @todo: [C] the complexity is O(stored), replace with the hashtable
 */
void cti_table_put_value (cti_table * table, 
                          const char *key, 
                          void *value, 
                          int size)
{
    assert (table != NULL);
    assert (key != NULL);
    assert (value != NULL);

    char *trimmed_key = xstrdup (key);
    char *trimmed_value = memdup (value, size);
    DEBUG_MSG ("Putting key:#%s# value:#%s# into the table", 
                trimmed_key,
                trimmed_value);

    int32_t idx = cti_table_find_index (table, trimmed_key);
    if (idx != -1)
    {
        /* If the key already exist, */
        /* update the value */

        char *old_value = table->data[idx]->value;
        table->data[idx]->value = trimmed_value;
        table->data[idx]->size = size;
        free (old_value), old_value = NULL;
        free (trimmed_key), trimmed_key = NULL;
    }
    else
    {
        /* The key does not exist */
        assert (table->stored <= table->size);
        if (table->stored == table->size)
        {
            /* If table is full, we add spaces */
            table->size = table->size * 2;
            table->data = realloc (table->data, table->size * sizeof (cti_pair *));
        }
        /* Add the new value */
        table->data[table->stored] = xmalloc (sizeof (cti_pair));
        table->data[table->stored]->key = trimmed_key;
        table->data[table->stored]->value = trimmed_value;
        table->data[table->stored]->size = size;
        DEBUG_MSG ("ATTENTION: stored before:%d", table->stored);
        table->stored++;  /* = table->stored + 1; */
        DEBUG_MSG ("\nStored:%d key:#%s# \t value:#%s#\n", 
                    table->stored,
                    trimmed_key, trimmed_value);
    }
}

/*------------------------------------------------------------------------ */

/**
 * @brief free memory allocated for table
 * @param cti_table *table -- pointer to the table
 */
void cti_table_destruct (cti_table * table)
{
    assert (table != NULL);
    assert (table->data != NULL);

    for (int i = 0; i < table->stored; i++)
    {
        cti_pair *ptr = table->data[i];
        assert (ptr != NULL);

        if (table->ptr_function_free_memory != NULL)
        {   /* Call the function that free the memory for each element */
            /* because the type of the element is unknown */
            (*table->ptr_function_free_memory) (ptr->value);
        }
        else
        {
            if (ptr->value != NULL)
                free (ptr->value), ptr->value = NULL;
        }

        if (ptr->key != NULL)
            free (ptr->key), ptr->key = NULL;
        free (ptr), ptr = NULL;
    }

    free (table->data), table->data = NULL;
    free (table), table = NULL;
}

/*------------------------------------------------------------------------ */

/**
 * @param cti_table *table
 * @param const char *key
 * @return pointer to the NEWLY CREATED value if found, NULL otherwise.
 */
void *cti_table_get_value (cti_table * table, const char *key)
{
    assert (table != NULL);
    assert (key != NULL);

    char *result = NULL;
    int idx = cti_table_find_index (table, key);
    if (idx != -1)
    {
        /* Get the value with the index */
        result = memdup (table->data[idx]->value, table->data[idx]->size);
    }
    else
        DEBUG_MSG ("Can't find the key:%s \n", key);

    return result;
}

/*------------------------------------------------------------------------ */

/**
 * @param int initial_size
 * @param void *ptr_function_free_memory
 * @return pointer to the created table
 */
cti_table *cti_table_create (int initial_size, void *ptr_function_free_memory)
{
    assert (initial_size > 0);

    /* Initialize a CTI table */
    cti_table *table = xmalloc (sizeof (cti_table));
    table->size = initial_size;
    table->stored = 0;
    table->data = xmalloc (initial_size * sizeof (cti_pair *));
    table->ptr_function_free_memory = ptr_function_free_memory;
    return table;
}

/*------------------------------------------------------------------------ */

/**
 * @param char *filename -- file to load from
 * @return pointer to cti_table or NULL if failed to load file, reason written to the log file.
 */
cti_table *cti_table_load_from_file (const char *filename)
{
    /* This function should not use cti_log since cti_log uses
       cti_table_load_from_file.
     */
    assert (filename != NULL);

    /* Initialize the CTI table */
    cti_table *table = cti_table_create (2048, NULL);
    errno = 0;
    FILE *fp = fopen (filename, "r");
    if (fp == NULL)
    {
        fprintf (stderr, "Can't open the file '%s', %s\n", filename, strerror(errno));
        return NULL;
    }

    char line[CTI_MAX_STRING_SIZE + 1];

    /* Get each config line */
    while (fgets (line, CTI_MAX_STRING_SIZE, fp) != NULL)
    {
        int err = ferror (fp);
        if (err)
        {
            fprintf (stderr, "Error message:%s \n", strerror (err));
            return NULL;
        }

        char *trimmed_line = xstrtrim (line);
        /* extract (key,value) cti_pair from the trimmed_line */
        int tline_size = xstrnlen (trimmed_line, CTI_MAX_STRING_SIZE);

        if (tline_size > 0)
        {
            char *key = trimmed_line;
            char *value = strchr (trimmed_line, '=');
            if (value == NULL)
            {
                fprintf (stderr, "Table file %s has wrong "
                        "format.\n", filename);
                fprintf (stderr, "Line: %s ", line);
                return NULL;
            }

            /* Separate key / value */
            *value = '\0';
            ++value;
            key = xstrtrim (key);
            value = xstrtrim (value);
            if (key != NULL)
                DEBUG_MSG ("DEBUG_KEY:%s\n", key);
            if (value != NULL)
                DEBUG_MSG ("DEBUG_VALUE:%s\n", value);
            int v_size = xstrnlen (value, CTI_MAX_STRING_SIZE) + 1;

            /* Fill the table */
            cti_table_put_value (table, key, value, v_size);
            free (key), key = NULL;
            free (value), value = NULL;
        }
        else
            DEBUG_MSG ("Line after trimming is empty for file %s", filename);
        free (trimmed_line), trimmed_line = NULL;
    }
    /* parsing finished */
    if (fclose (fp) == EOF)
    {
        fprintf (stderr, "Error while closing the file %s", filename);
        return NULL;
    }
    return table;
}

/*------------------------------------------------------------------------ */

/**
 * @param cti_table *table: table to write into file
 * @param const char *filename: the filename of the file to write, WARNING: file will be rewritten by this function
 */
void cti_table_record_in_file (cti_table * table, const char *filename)
{
    FILE *fp = fopen (filename, "w");
    if (fp == NULL)
    {
        printf ("Can't properly open file: %s\n", filename);
        exit (1);
    }

    for (int i = 0; i < table->stored; ++i)
    {
        char *key = table->data[i]->key;
        char *value = table->data[i]->value;
        /* Create the line */
        char *line = concat_strings (key, " = ", value, END_OF_LINE);
        int line_size = xstrnlen (line, CTI_MAX_STRING_SIZE);
        /* Write the line */
        fwrite (line, line_size, 1, fp);
        free (line), line = NULL;
    }
    if (fclose (fp) == EOF)
    {
        printf ("Can't properly close the file\n");
        exit (1);
    }
}

/*------------------------------------------------------------------------ */

/**
 * @param const char *value the value
 * @param cti_table *table the table
 * @param start_index the index for which the search starts
 * @return index if succeed, -1 if fails
 */
int cti_table_find_index_value (cti_table * table, 
                                const char *value, 
                                int start_index)
{
    if (start_index >= 0)
    {
        for (int i = start_index; i < table->stored; ++i)
            if (strcmp (table->data[i]->value, value) == 0)
                return i;

    }
    return -1;
}

/*------------------------------------------------------------------------ */

/**
 * @param cti_table *table
 * @param cpt the index
 * @return the key from the index, NULL if failed
 */
char *cti_table_get_key_by_index (cti_table * table, int cpt)
{
    char *result = NULL;
    if (table == NULL)
        return NULL;
    if (cpt < table->stored)
        result = table->data[cpt]->key;
    return result;
}

/*------------------------------------------------------------------------ */

/**
 * @param cti_table *table the table
 * @param const char *value the value
 * @return 0 if success, -1 otherwise
 */
int cti_table_rm_value (cti_table * table, const char *value)
{
    assert (table != NULL);
    assert (value != NULL);

    int idx = cti_table_find_index_value (table, value, 0);

    if (idx == -1) return -1;

    /* While the value does exist */
    while (idx != -1)
    {
        free (table->data[idx]->key), table->data[idx]->key = NULL;

        /* Call the free function */
        if (table->ptr_function_free_memory != NULL)
            (*table->ptr_function_free_memory) (table->data[idx]->value);
        else
            free (table->data[idx]->value), table->data[idx]->value = NULL;

        table->data[idx]->size = 0;

        /* Switch the removed value with the last value of the CTI table */
        if (table->stored - 1 != idx)
        {
            table->data[idx]->size = table->data[table->stored - 1]->size;
            table->data[idx]->key = xstrdup (table->data[table->stored - 1]->key);
            table->data[idx]->value = 
                    memdup (table->data[table->stored - 1]->value, table->data[idx]->size);

            free (table->data[table->stored - 1]->key), table->data[table->stored -1]->key= NULL;

            if (table->ptr_function_free_memory != NULL)
            {
                (*table-> ptr_function_free_memory) (table->data[table->stored - 1]->value);
            }
            else
                free (table->data[table->stored - 1]->value), table->data[table->stored -1]->value = NULL;

            table->data[table->stored - 1]->size = 0;
        }
        table->stored--;
        idx = cti_table_find_index_value (table, value, idx);
    }
    return 0;
}

/*------------------------------------------------------------------------ */
