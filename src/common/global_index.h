/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef GLOBAL_INDEX_
#define GLOBAL_INDEX_

#include <stdbool.h>

#include "cti_types.h"

void* global_index_file_it_begin(void *it);
bool global_index_file_it_is_end(void *it);
void* global_index_file_it_next(void *it);
char* global_index_file_it_value(void *it);
CTI_UID* global_index_file_it_key(void *it);
void* global_index_file_load(void);
void global_index_file_unload(void *gindex_file);
int global_index_file_write(char *path_to_rep, CTI_UID *uid);
char* global_index_file_path_by_uid(CTI_UID *uid, char *ctr_dir_type);
char* global_index_file_ctr_by_entry_uid(CTI_UID *uid, char *ctr_dir_type);
char* global_index_file_get_ctr_by_uid(CTI_UID *uid);
CTI_UID* global_index_file_get_uid_by_ctr(char *path);
bool global_index_file_is_there_this_path(char *path);
void global_index_file_rm(char *line);

#endif /* GLOBAL_INDEX_ */
