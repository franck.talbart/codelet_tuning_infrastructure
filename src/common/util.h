/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

#ifndef CTI_UTIL_H_
#define CTI_UTIL_H_

#include <stdbool.h>

void cti_fail(char *msg);
void* xmalloc(size_t size);
char* get_current_dir(void);
int count_char(const char *str, const char c);
void die_stdout(const char *where, const char *reason);
char* xstrtrim(const char *totrim);
char* escape_quotes(const char *str);
char* xstrdup(const char *str);
char* xstrndup(const char *str, const int len);
void* memdup(void *src, int size);
int xstrnlen(const char *str, int limit);
char* xconcat_strings(const char *str1, ...);
char* mystrncat(char *dest, const char *src, int limit);
int xcreate_dir(const char *name);
char *str_sub(const char *s, unsigned int start, unsigned int end);
int copy_file(const char *source, const char *destination);
char* get_current_date(void);
char* get_cti_version(void);
void debug_msg(const char *msg, ...);

#define concat_strings(str1, ...) \
  xconcat_strings(str1, __VA_ARGS__ , NULL)

#define CHECK_VERBOSITY(X, Y) \
  if (errno || (X < CTI_VERBOSITY_MIN) || (X > CTI_VERBOSITY_MAX))  \
    cti_fail(concat_strings("Incorrect verbosity: \"", Y, "\"\n"));

#define DEBUG_MSG(msg, ...) \
  debug_msg(msg, __VA_ARGS__);

#define UNEXPECTED_ERROR(MSG, ...)              \
  printf(MSG, __VA_ARGS__);                     \
return CTI_ERROR_UNEXPECTED;

#define XARRAY_SIZE(X) sizeof(X)/sizeof(X[0])

#define LOG(MSG)                                \
  log_to_file(MSG);

#endif /* CTI_UTIL_H */
