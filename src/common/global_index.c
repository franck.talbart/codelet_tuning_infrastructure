/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * This module manages the global index file.
 * The global index file contains the list of
 * the local repositories.
 */
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h>

#include "global_index.h"

#include "log.h"
#include "get_directory.h"
#include "config.h"
#include "util.h"
#include "cti_error.h"
#include "cti_table.h"
#include "cti_uid.h"

/**
 * @param void* it the iterator
 * @return the iterator pointing to the first element
 */
void* global_index_file_it_begin(void *it)
{
    if (it == NULL)
        return NULL;
    global_index_iterator* iterator = it;
    iterator->cpt = 0;
    return iterator;
}

/*------------------------------------------------------------------------*/

/**
 * @param void *it iterator
 * @return true if reached the end, false otherwise
 */
bool global_index_file_it_is_end(void *it)
{
    if (it == NULL)
        return true;
    global_index_iterator* iterator = it;
    assert(iterator->table != NULL);
    return (iterator->cpt >= iterator->table->stored);
}

/*------------------------------------------------------------------------*/

/**
 * @param void *it iterator
 * @return next element
 */
void* global_index_file_it_next(void *it)
{
    global_index_iterator* iterator = it;
    assert(iterator != NULL);
    iterator->cpt++;
    return iterator;
}

/*------------------------------------------------------------------------*/

/**
 * @param void *it iterator
 * @return absolute path to the repository
 */
char* global_index_file_it_value(void *it)
{
    assert(it != NULL);
    global_index_iterator* iterator = it;
    int index = iterator->cpt;
    char *result = xstrdup(iterator->table->data[index]->value);
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @param void *it iterator
 * @return The UID of the repository
 */
CTI_UID* global_index_file_it_key(void *it)
{
    assert(it != NULL);
    global_index_iterator* iterator = it;
    char* str_uid = xstrdup(iterator->table->data[iterator->cpt]->key);
    CTI_UID* result = str_to_cti_uid(str_uid);
    free(str_uid), str_uid = NULL;
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Global index file contains all the repositories which being create by the user
 * 
 * @return iterator (char ** an array of absolute paths, NULL if it fails)
 */
void* global_index_file_load(void)
{
    global_index_iterator* iterator = xmalloc(sizeof(global_index_iterator));
    char *cti_cfg_dir = get_cti_cfg_dir();
    char *gif_name = config_get_value(GLOBAL_INDEX_FILE);
    char *path_global_index_file = concat_strings(cti_cfg_dir, gif_name);
    free(cti_cfg_dir), cti_cfg_dir = NULL;
    free(gif_name), gif_name = NULL;
    if (path_global_index_file == NULL)
    {
        cti_log(1, "The path to global index file is NULL.");
        return NULL;
    }
    iterator->table = cti_table_load_from_file(path_global_index_file);
    iterator->cpt = 0;
    free(path_global_index_file), path_global_index_file = NULL;
    return (iterator);
}

/*------------------------------------------------------------------------*/

/**
 * @brief free memory allocated for a char ** dynamic table
 * 
 * @param void *gindex_file pointer to the table
 */
void global_index_file_unload(void *gindex_file)
{
    global_index_iterator* iterator = (global_index_iterator*) gindex_file;
    if (iterator == NULL)
    {
        cti_log(1, "CTI WARNING: Global index file iterator is NULL.");
        return;
    }
    cti_table *table = iterator->table;
    if (table != NULL)
        cti_table_destruct(table);
    free(iterator), iterator = NULL;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Add a new line in the global index file
 * 
 * @param char *path_to_rep a path to repository
 * @param CTI_UID *uid the CTR UID (UID of the repository)
 * @return 0 if it succeeds, error otherwise
 */
int global_index_file_write(char *path_to_rep, CTI_UID *uid)
{
    char *cfg_dir = get_cti_cfg_dir();
    char *gif_name = config_get_value(GLOBAL_INDEX_FILE);
    char *path_global_index_file = concat_strings(cfg_dir, gif_name);
    free(cfg_dir), cfg_dir = NULL;
    free(gif_name), gif_name = NULL;

    cti_table *table = NULL;
    struct stat st;
    if (stat(path_global_index_file, &st) == 0)
        table = cti_table_load_from_file(path_global_index_file);
    else
        table = cti_table_create(2048, NULL);
    if (table == NULL)
    {
        cti_log(1, "[CTI ERROR]: Cant' neither load global_index_file, nor create a table for it!");
        return CTI_ERROR_UNEXPECTED;
    }

    char *str_uid = cti_uid_to_str(uid);
    int path_size = xstrnlen(path_to_rep, CTI_MAX_STRING_SIZE) + 1;
    if (path_size <= 0)
    {
        cti_log(1, "[CTI INTERNAL ERROR] Incorrect path. \n");
        return CTI_ERROR_UNEXPECTED;
    }
    /* put value on the table */
    cti_table_put_value(table, str_uid, path_to_rep, path_size + 1);
    free(str_uid), str_uid = NULL;
    cti_table_record_in_file(table, path_global_index_file);
    cti_table_destruct(table);
    free(path_global_index_file), path_global_index_file = NULL;
    return 0;
}

/*------------------------------------------------------------------------*/

/**
 * @param CTI_UID *uid the uid
 * @param ctr_dir_type
 * @return path specified by uid, NULL if it fails
 */
char* global_index_file_path_by_uid(CTI_UID *uid, char *ctr_dir_type)
{
    char *str_uid = cti_uid_to_str(uid);
    if (str_uid == NULL)
    {
        cti_log(1, "[CTI MSG]: cti_uid_to_str returned NULL, can't continue.\n");
        return NULL;
    }

    char *ctr_dir_name = config_get_value(ctr_dir_type);
    if (ctr_dir_name == NULL)
    {
        cti_log(1, "Can't get value for %s from the config file.", ctr_dir_type);
        free(str_uid), str_uid = NULL;
        return NULL;
    }

    char* value = global_index_file_ctr_by_entry_uid(uid, ctr_dir_type);

    if (value != NULL)
    {
        char *old_value = value;
        value = concat_strings(value, CTI_SLASH, ctr_dir_name, CTI_SLASH, str_uid);
        DEBUG_MSG("IN: %s value = %s", ctr_dir_name, value);
        free(old_value), old_value = NULL;
    }

    free(str_uid), str_uid = NULL;
    free(ctr_dir_name), ctr_dir_name = NULL;
    return value;
}

/*------------------------------------------------------------------------*/

/**
 * @param CTI_UID *uid the entry uid
 * @param ctr_dir_type
 * @return CTR path specified by uid, NULL if it fails
 */
char* global_index_file_ctr_by_entry_uid(CTI_UID *uid, char *ctr_dir_type)
{
    char *str_uid = cti_uid_to_str(uid);
    if (str_uid == NULL)
    {
        cti_log(1, "[CTI MSG]: cti_uid_to_str returned NULL, can't continue.\n");
        return NULL;
    }
    cti_log(1, 
            "CTI MSG: Started searching for %s in directory type %s",
            str_uid, 
            ctr_dir_type);

    global_index_iterator* gi_file = global_index_file_load();
    if (gi_file == NULL)
    {
        cti_log(1, "CTI ERROR: Can't load global index file found.");
        free(str_uid), str_uid = NULL;
        return NULL;
    }

    char *ctr_dir_name = config_get_value(ctr_dir_type);
    if (ctr_dir_name == NULL)
    {
        cti_log(1, "Can't get value for %s from the config file.", ctr_dir_type);
        free(str_uid), str_uid = NULL;
        return NULL;
    }

    for (void *it = global_index_file_it_begin(gi_file); !global_index_file_it_is_end(
            it); it = global_index_file_it_next(it))
    {
        char *value = global_index_file_it_value(it);
        char *old_value = value;
        value = concat_strings(value, CTI_SLASH, ctr_dir_name, CTI_SLASH, str_uid);
        DEBUG_MSG("IN: %s value = %s", ctr_dir_name, value);

        struct stat st;
        if (stat(value, &st) == 0)
        {
            global_index_file_unload(gi_file);
            free(ctr_dir_name), ctr_dir_name = NULL;
            free(str_uid), str_uid = NULL;
            free(value), value = NULL;
            return old_value;
        }
        DEBUG_MSG("%s DOESNT EXIST", value);
        free(value), value = NULL;
        free(old_value), old_value = NULL;
    }
    free(str_uid), str_uid = NULL;
    free(ctr_dir_name), ctr_dir_name = NULL;
    global_index_file_unload(gi_file);
    return NULL;
}

/*------------------------------------------------------------------------*/

/**
 * @param CTI_UID *uid ctr uid
 * @return path specified by uid
 */
char* global_index_file_get_ctr_by_uid(CTI_UID *uid)
{
    global_index_iterator* gi_file = global_index_file_load();
    if (gi_file == NULL)
        return NULL;
    char *str_uid = cti_uid_to_str(uid);
    char *result = cti_table_get_value(gi_file->table, str_uid);
    free(str_uid), str_uid = NULL;
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @param char *path
 * @return UID specified by path
 */
CTI_UID* global_index_file_get_uid_by_ctr(char *path)
{
    global_index_iterator* gi_file = global_index_file_load();
    if (gi_file == NULL)
        return NULL;

    CTI_UID *result = NULL;
    int index = cti_table_find_index_value(gi_file->table, path, 0);
    if (index == -1)
        return NULL;
    
    char* str_uid = cti_table_get_key_by_index(gi_file->table, index);
    result = str_to_cti_uid(str_uid);
    free(str_uid), str_uid = NULL;
    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Check if a path exists or not in the global index file
 * 
 * @param char *path path
 * @return true if the path already exists, false otherwise
 */
bool global_index_file_is_there_this_path(char *path)
{
    bool result = false;
    global_index_iterator* gi_file = global_index_file_load();
    if (gi_file == NULL)
        return NULL;

    if (cti_table_find_index_value(gi_file->table, path, 0) != -1)
        result = true;

    return result;
}

/*------------------------------------------------------------------------*/

/**
 * @brief Add a new line in the global index file
 * 
 * @param char *line the new line
 */
void global_index_file_rm(char *line)
{
    char *cfg_dir = get_cti_cfg_dir();
    char *gif_name = config_get_value(GLOBAL_INDEX_FILE);
    char *path_global_index_file = concat_strings(cfg_dir, gif_name);
    free(cfg_dir), cfg_dir = NULL;
    free(gif_name), gif_name = NULL;

    cti_table *table = NULL;
    table = cti_table_load_from_file(path_global_index_file);

    if (table == NULL)
    {
        cti_log(1, "CTI ERROR: Cant' neither load global_index_file, nor create a table for it!");
        return;
    }
    cti_table_rm_value(table, line);
    cti_table_record_in_file(table, path_global_index_file);
    cti_table_destruct(table);
    free(path_global_index_file), path_global_index_file = NULL;
}

/*------------------------------------------------------------------------*/
