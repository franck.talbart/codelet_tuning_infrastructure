/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 *  Wrappers depend on the platform
 *
 */

#include "platform.h"

/**
 * @fn xpopen
 * @brief popen
 * 
 * @param const char *commande the commande
 * @param const char *type r for read or w for write
 * @return FILE *value popen result
 */
FILE* xpopen (const char *commande, const char *type)
{
    return popen(commande, type);
}

/*------------------------------------------------------------------------ */

/**
 * @fn xpclose
 * @brief pclose
 * 
 * @param FILE *stream the stream to close
 * @return int value pclose result
 */
int xpclose(FILE *stream)
{
    return pclose(stream);
}

/*------------------------------------------------------------------------ */
