/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit */

/*
 * The alias module provides functions to set, get and
 * remove data aliases and plugins aliases.
 *
 */

#define _XOPEN_SOURCE 600

#include <stdio.h>
#include <assert.h>

#include "alias.h"

#include "config.h"
#include "util.h"
#include "get_directory.h"
#include "ctr_path.h"
#include "data_info.h"
#include "global_index.h"

/**
 * @param char *value the value
 * @param const char *filename the alias filename
 * @return alias, NULL if not found
 */
static char* alias_general_get_key(char *value, const char *filename)
{
    assert(filename != NULL);
    assert(value != NULL);

    char *result = NULL;

    /* Load the alias file to the CTI table */
    cti_table *table = cti_table_load_from_file(filename);
    if (table != NULL)
    {
        /* We decided to have only one alias / UID */
        int idx = cti_table_find_index_value(table, value, 0);

        /* Alias found */
        if (idx != -1)
        {
            result = xstrdup(cti_table_get_key_by_index(table, idx));
            DEBUG_MSG("Aliased value for file:#%s# \t value:#%s# \t result:#%s#",
                    filename, value, result);
        }

        /* Free the memory */
        cti_table_destruct(table);
    }
    return result;
}

//------------------------------------------------------------------------

/**
 * @param const char *key the key
 * @param const char *filename the alias filename
 * @return alias value, NULL if not found
 */
static char* alias_general_get_value(const char *key, const char *filename)
{
    assert(key != NULL);
    assert(filename != NULL);

    char *result = NULL;

    /* Load the alias file into the CTI table */
    cti_table *table = cti_table_load_from_file(filename);

    if (table != NULL)
    {
        /* Get the UID */
        result = cti_table_get_value(table, key);
        DEBUG_MSG("Aliased value for file:#%s# \t key:#%s# \t result:#%s#",
                filename, key, result);

        /* Free the memory */
        cti_table_destruct(table);
    }

    return result;
}

//------------------------------------------------------------------------

/**
 * @param const char *value the value
 * @param const char *filename the alias filename
 * @return 0 if success, -1 otherwhise
 */
static int alias_general_rm_value(CTI_UID *value, const char *filename)
{
    assert(value != NULL);
    assert(filename != NULL);

    int result = -1;

    /* Load the alias file into the CTI table */
    cti_table *table = cti_table_load_from_file(filename);

    if (table != NULL)
    {
        /* Converts UID to string */
        char* str_value = cti_uid_to_str(value);

        /* Remove the alias */
        result = cti_table_rm_value(table, str_value);
        DEBUG_MSG("Delete value for file:#%s# \t value:#%s# \t result:#%d#",
                    filename, 
                    str_value, 
                    result);

        free(str_value), str_value = NULL;

        /* Record the file */
        if (result != -1)
            cti_table_record_in_file(table, filename);

        /* free the memory */
        cti_table_destruct(table);
    }

    return result;
}

//------------------------------------------------------------------------

/**
 * @param const char *key the key
 * @param const char *value the value
 * @param const char *filename the alias filename
 */
static void alias_general_set_value(const char *key, char *value, const char *filename)
{
    assert(filename != NULL);
    assert(key != NULL);
    assert(value != NULL);
    assert(*key != '\0');

    cti_table *table = NULL;

    FILE* f = NULL; 

    // Check that the alias file exists
    f = fopen(filename,"r"); 
    if(f == NULL) 
    { 
        DEBUG_MSG("File %s doesn't exists. CTI is going to create this file.",
                filename);
        // Create an empty table
        table = cti_table_create(2048, NULL);
    }
    else
    {
        fclose(f);

        // Load the table from the alias file
        table = cti_table_load_from_file(filename);
        assert(table != NULL);
    }

    int e_size = xstrnlen(value, CTI_MAX_STRING_SIZE) + 1;

    /* Add the value */
    cti_table_put_value(table, key, value, e_size);

    /* Record the file */
    cti_table_record_in_file(table, filename);

    /* Free the memory */
    cti_table_destruct(table);
    DEBUG_MSG("GENERAL_SET: aliased value for file:#%s# \t key:#%s# \t value:#%s#",
                filename, 
                key, 
                value);
}

//------------------------------------------------------------------------

/**
 * @param const CTI_UID *value the uid
 * @return alias, NULL if not found
 */
char* alias_data_get_key(CTI_UID *value)
{
    assert(value != NULL);

    /* Loads ctr_info */
    cti_table* data_info = info_file_load_by_uid(value);
    if (data_info == NULL)
        return NULL;

    /* Get the value from ctr_info */
    char* alias = info_get_value(data_info, DATA_INFO_ALIAS);

    /* Free the memory */
    info_destruct(data_info);
    return alias;
}

//------------------------------------------------------------------------

/**
 * @param const char *key alias name
 * @param CTI_UID* value the value
 * @return UID, NULL if not found
 */
void alias_data_set_value(const char *key, CTI_UID* value)
{
    assert(key != NULL);
    assert(value != NULL);

    /* Load the ctr_info file */
    cti_table* data_info = info_file_load_by_uid(value);
    assert(data_info != NULL);

    /* Add alias to ctr_info */
    info_put_value(data_info, DATA_INFO_ALIAS, key);
    char* data_info_filename = config_get_value(DATA_INFO_FILENAME);

    /* Get the full path to ctr_info */
    char* ctr_info_name = concat_strings(get_path_by_uid(CTR_ENTRY_DATA, value),
                                        CTI_SLASH,
                                        data_info_filename);

    /* Record ctr_info */
    info_record_in_file(data_info, ctr_info_name);

    /* Free the memory */
    free(data_info_filename), data_info_filename = NULL;
    free(ctr_info_name), ctr_info_name = NULL;

    info_destruct(data_info);
}

//------------------------------------------------------------------------

/**
 * @param const CTI_UID* value the value
 * @return alias, NULL if not found
 */
char* alias_plugin_get_key(CTI_UID* value)
{
    assert(value != NULL);

    char* str_uid = cti_uid_to_str(value);

    /* Alias plugin filename */
    char* plugin_alias = config_get_value(ALIAS_PLUGIN_NAME);
    assert(plugin_alias != NULL);

    char *common_dir = get_common_dir();

    /* Full path to the alias plugin file */
    char *alias_file = concat_strings(common_dir, CTI_SLASH, plugin_alias);
    free(common_dir), common_dir = NULL;


    /* Get the alias */
    char* result = alias_general_get_key(str_uid, alias_file);

    /* If no alias found, check in the local repositories */
    if (result == NULL)
    {
      char *ctr_dir = global_index_file_ctr_by_entry_uid(value, CTR_PLUGIN_DIR);
      free(alias_file), alias_file = NULL;
      alias_file = concat_strings(ctr_dir, CTI_SLASH, plugin_alias);

      // If file exists
      if (fopen(alias_file,"r")!=NULL)
        result = alias_general_get_key(str_uid, alias_file);
        free(ctr_dir), ctr_dir = NULL;
    }

    /* Free the memory */
    free(plugin_alias), plugin_alias = NULL;
    free(alias_file), alias_file = NULL;
    DEBUG_MSG("GETTING alias for plugin name: #%s# alias:#%s#\n", str_uid, result);

    free(str_uid), str_uid = NULL;
    return result;
}

//------------------------------------------------------------------------

/**
 * @param const char *name the alias name
 * @return UID, NULL if not found
 */
CTI_UID* alias_plugin_get_value(const char *name)
{
    assert(name != NULL);

    /* Get the alias plugin filename */
    char* plugin_alias = config_get_value(ALIAS_PLUGIN_NAME);
    assert(plugin_alias != NULL);

    char *common_dir = get_common_dir();
    assert(common_dir != NULL);

    /* Get the full path to the alias plugin file */
    char *alias_file = concat_strings(common_dir, CTI_SLASH, plugin_alias);
    free(common_dir), common_dir = NULL;

    /* Get the UID */
    char* alias_value = alias_general_get_value(name, alias_file);

    /* If no UID found, check in the local repositories */
    if (alias_value == NULL)
    {
        void *gi_file = global_index_file_load();
        void *it = global_index_file_it_begin(gi_file);
        char *dtw = NULL;
        while (!global_index_file_it_is_end(it) && alias_value == NULL)
        {
            dtw = global_index_file_it_value(it);
            free(alias_file), alias_file = NULL;
            alias_file = concat_strings(dtw, CTI_SLASH, plugin_alias);

            // If file exists
            if (fopen(alias_file,"r")!=NULL)
                alias_value = alias_general_get_value(name, alias_file);
            free(dtw), dtw = NULL;
            it = global_index_file_it_next(it);
        }
        global_index_file_unload(gi_file);
    }

    free(alias_file), alias_file = NULL;
    if (alias_value == NULL)
        return NULL;

    /* Convert string to UID */
    CTI_UID* result = str_to_cti_uid(alias_value);
    DEBUG_MSG("GETTING alias for plugin name: #%s# alias:#%s#\n", name, alias_value);
    free(alias_value), alias_value = NULL;
    free(plugin_alias), plugin_alias = NULL;

    return result;
}

//------------------------------------------------------------------------

/**
 * @param const char *key the key
 * @param const CTI_UID *value is plugins UID
 * @param const char *dir the directory that contains the alias file. If NULL, then it is ctr-common
 */
void alias_plugin_set_value(const char *key, CTI_UID *value, char *dir)
{
    assert(key != NULL);
    assert(value != NULL);

    /* Get the alias plugin filename */
    char* plugin_alias = config_get_value(ALIAS_PLUGIN_NAME);
    assert(plugin_alias != NULL);

    char *dir_rep = dir;
    if (dir_rep == NULL)
    {
        dir_rep = get_common_dir();
        assert(dir_rep != NULL);
    }

    /* Get the full path to the alias plugin file */
    char *alias_file = concat_strings(dir_rep, CTI_SLASH, plugin_alias);

    if (dir == NULL)
        free(dir_rep), dir_rep = NULL;

    free(plugin_alias), plugin_alias = NULL;

    /* Convert UID to string */
    char* str_value = cti_uid_to_str(value);
    alias_general_set_value(key,str_value, alias_file);

    /* Free the memory */
    free(alias_file), alias_file = NULL;
    DEBUG_MSG("SETTING alias for plugin name: #%s# alias:#%s#\n", key, str_value);
    free(str_value), str_value = NULL;
}

//------------------------------------------------------------------------

/**
 * @param const char *value the value
 * @return 0 if success, -1 otherwhise
 */
int alias_plugin_rm_value(CTI_UID *value)
{
    assert(value != NULL);

    /* Get the alias plugin filename */
    char *plugin_alias = config_get_value(ALIAS_PLUGIN_NAME);
    assert(plugin_alias != NULL);

    char *common_dir = get_common_dir();

    /* Get the fullpath to the alias plugin file */
    char *alias_file = concat_strings(common_dir, CTI_SLASH, plugin_alias);
    free(common_dir), common_dir = NULL;

    /* Remove the UID */
    int result = alias_general_rm_value(value, alias_file);

    /* If plugin not found, try to search it on the other repositories...*/
    if (result == -1)
    {
        void *gi_file = global_index_file_load();
        void *it = global_index_file_it_begin(gi_file);
        char *dtw = NULL;
        while (!global_index_file_it_is_end(it) && result == -1)
        {
            dtw = global_index_file_it_value(it);
            free(alias_file), alias_file = NULL;
            alias_file = concat_strings(dtw, CTI_SLASH, plugin_alias);

            // If file exists
            if (fopen(alias_file,"r")!=NULL)
                result = alias_general_rm_value(value, alias_file);
            free(dtw), dtw = NULL;
            it = global_index_file_it_next(it);
        }
        global_index_file_unload(gi_file);
    }

    /* Free the memory */
    free(alias_file), alias_file = NULL;
    free(plugin_alias), plugin_alias = NULL;
    return result;
}

//------------------------------------------------------------------------

/**
 * @param const CTI_UID *value the uid
 * @return alias, NULL if not found
 */
char* alias_repository_get_key(CTI_UID *value)
{
    assert(value != NULL);

    /* Convert UID to string */
    char* str_uid = cti_uid_to_str(value);

    /* Get the repository alias filename */
    char *repository_alias = config_get_value(ALIAS_REPOSITORY_NAME);
    assert(repository_alias != NULL);

    char *cfg_dir = get_cti_cfg_dir();
    assert(cfg_dir != NULL);

    /* Get the fullpath to the repository alias file */
    char *alias_file = concat_strings(cfg_dir, repository_alias);
    free(cfg_dir), cfg_dir = NULL;
    free(repository_alias), repository_alias = NULL;

    /* Get the alias */
    char *result = alias_general_get_key(str_uid, alias_file);

    /* Free the memory */
    free(alias_file), alias_file = NULL;
    DEBUG_MSG("GETTING alias for repository name: #%s# alias:#%s#\n", str_uid, result);
    free(str_uid), str_uid = NULL;
    return result;
}

//------------------------------------------------------------------------

/**
 * @param const char *name the alias name
 * @return UID, NULL if not found
 */
CTI_UID* alias_repository_get_value(const char *name)
{
    assert(name != NULL);

    /* Get the alias repository filename */
    char *repository_alias = config_get_value(ALIAS_REPOSITORY_NAME);
    assert(repository_alias != NULL);

    char *cfg_dir = get_cti_cfg_dir();
    assert(cfg_dir != NULL);

    /* Get the fullpath to the alias repository file */
    char *alias_file = concat_strings(cfg_dir, repository_alias);
    free(cfg_dir), cfg_dir = NULL;
    free(repository_alias), repository_alias = NULL;

    /* Get the UID */
    char* alias_value = alias_general_get_value(name, alias_file);
    free(alias_file), alias_file = NULL;

    if (alias_value == NULL)
        return NULL;

    /* Convert string to UID */
    CTI_UID* result = str_to_cti_uid(alias_value);
    DEBUG_MSG("GETTING alias for repository name: #%s# alias:#%s#\n", name, alias_value);
    free(alias_value), alias_value = NULL;
    return result;
}

//------------------------------------------------------------------------

/**
 * @param const char *value the value
 * @return 0 if success, -1 otherwhise
 */
int alias_repository_rm_value(CTI_UID* value)
{
    assert(value != NULL);

    /* Get the alias repository filename */
    char *repository_alias = config_get_value(ALIAS_REPOSITORY_NAME);
    assert(repository_alias != NULL);

    char *cfg_dir = get_cti_cfg_dir();
    assert(cfg_dir != NULL);

    /* Get the fullpath to the alias repository file */
    char *alias_file = concat_strings(cfg_dir, repository_alias);
    free(cfg_dir), cfg_dir = NULL;
    free(repository_alias), repository_alias = NULL;

    /* Remove UID */
    int result = alias_general_rm_value(value, alias_file);
    free(alias_file), alias_file = NULL;

    return result;
}

//------------------------------------------------------------------------

/**
 * @param const char *key alias name
 * @param CTI_UID* value the value
 */
void alias_repository_set_value(const char *key, CTI_UID* value)
{
    assert(key != NULL);
    assert(value != NULL);

    /* Get the alias repository filename */
    char *repository_alias = config_get_value(ALIAS_REPOSITORY_NAME);
    assert(repository_alias != NULL);

    /* Convert UID to string */
    char* str_value = cti_uid_to_str(value);

    char *cfg_dir = get_cti_cfg_dir();
    assert(cfg_dir != NULL);

    /* Get the fullpath to the alias repository file */
    char *alias_file = concat_strings(cfg_dir, repository_alias);
    free(cfg_dir), cfg_dir = NULL;
    free(repository_alias), repository_alias = NULL;

    /* Set the alias */
    alias_general_set_value(key, str_value, alias_file);

    DEBUG_MSG("Alias for repository key: #%s# value:#%s#\n", key, str_value);

    /* Free the memory */
    free(str_value), str_value = NULL;
    free(alias_file), alias_file = NULL;
}

//------------------------------------------------------------------------
