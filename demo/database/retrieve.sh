#!/bin/sh
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

cti user login admin admin
echo "Find the created entry by querying the json data, we search a field anumber with a value containing 3"
echo "cti query select all \"data:*anumber*3*\" newdbplugin --format=json"
entry=`cti query select all "data:*anumber*3*" newdbplugin --format=txt --fields=entry_uid`
echo $entry
entry=`echo $entry | grep -o -E "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"`


echo "Show the content of the entry and retrieve the files kept inside"
echo "cti newdbplugin get $entry" 
cti newdbplugin get $entry 
ls $entry/


echo "Clean up..."
rm -rf $entry/
cti rm entry newdbplugin --force=True
cti rm repository --force=True
cti user logout
