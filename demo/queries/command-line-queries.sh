#!/bin/sh
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

cti user login admin admin
echo "This script queries all the repositories, to query other repositories just call:"
echo " * cti query all, for all the repositories"
echo " * cti query common, for common repository"
echo " * etc ..."

echo

echo "Find all COMPILER entries that contains ICC"
echo cti query select all \"compiler_name:*icc*\" compiler --pagination=false
cti query select all "compiler_name:*icc*" compiler --pagination=false

echo

echo "Find all USERS entries with email containing exascale-computing.eu"
echo cti query select all \"email:*exascale-computing.eu\" user --pagination=false
cti query select all "email:*exascale-computing.eu" user  --pagination=false

echo

echo "Find all application entries that contain LUDCMP"
echo cti query select all \"alias:*ludcmp*\" application  --pagination=false
cti query select all "alias:*ludcmp*" application --pagination=false

echo

echo "Find all CPU entries that contain between 1 and 4 cores"
echo cti query select all \"number_of_cores:1->4\" cpu  --pagination=false
cti query select all "number_of_cores:1->4" cpu  --pagination=false

echo
cti user logout
