#!/usr/bin/python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import database_manager, database

import os

# prepare the query

db = database.Database()
uids = database_manager.search_uids(
                                      { 
                                       'L':{
                                            'NAME':["entry_info.repository"],
                                            'TYPE':"=",
                                            'VAL': cti.LOCAL_REPOSITORY
                                           },
                                       'LOGIC':'AND',
                                       'R':{
                                            'L':{
                                                 'NAME':["entry_info.plugin_uid"],
                                                 'TYPE':"=",
                                                 'VAL': "45015659-4f12-4ab2-8849-e0a4e7e4fe4b"
                                                },
                                           'LOGIC':'AND',
                                           'R':{
                                                'NAME':["entry_info.alias"],
                                                'TYPE':"LIKE",
                                                'VAL': "%ludcmp%"}
                                               }
                                      },
                                      db
                                  )

print "Find all the local application that contain ludcmp"
print uids
