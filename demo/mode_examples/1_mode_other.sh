#!/bin/bash
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

cti user login admin admin
echo "Import local repository"
cti repository import mode_example
echo "Launch Maqao Perf process"
cti maqao_perf init easy_loop --binary=fc420fd8-c4a7-11e3-a92c-0b07db7b0e9e --mode=other --platform=None --partition=None --run_parameters=2000 --binary_name=None --prefix_cmd=None --prefix_execute_script=prefix_script.sh --suffix_execute_script=None --submitter_cmd="sbatch" --submitter_opt="-w britten" --submitter_user=None

cti user logout
