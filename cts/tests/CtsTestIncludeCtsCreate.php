<?php 
/************************************************************************
Codelet Tuning Infrastructure
Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once('PHPUnit/Framework.php');

require_once('../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
class CtsTestIncludeCtsCreate extends PHPUnit_Framework_TestCase
{
    /**
     * @brief Called before each and every test. 
     * Usefull to declare data before each test if needed.
     */
    protected function setUp()
    {
        if(!session_id())
        {
            session_start();
        }
        if ( !isset( $_SESSION ))
        {
            $_SESSION = array();
            $_SESSION['username'] = 'admin';
            $_SESSION['pwd'] = '21232f297a57a5a743894a0e4a801fc3';
        }
        else if((!array_key_exists('username', $_SESSION)) || (!array_key_exists('pwd', $_SESSION)))
        {
            $_SESSION['username'] = 'admin';
            $_SESSION['pwd'] = '21232f297a57a5a743894a0e4a801fc3';
        }
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Called after each and every test.
    * Usefull to clean up data after each test if needed.
    */
    protected function tearDown()
    {
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Called after the end of all tests
    * Usefull to clean up on a global scale
    */
    public static function tearDownAfterClass()
    {
        session_unset();
        session_destroy();
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
     * @brief Tests the function cts_create_link()
     */
    public function test_cts_create_link()
    {
        $this->assertEquals(
            cts_create_link('a', 'b'),
            '<a title="a" href="b">a</a>',
            'The given parameters are not reflected in the html.'
        );
        $this->assertEquals(
            cts_create_link('a', 'b', true),
            '<a title="a" href="b" target="_blank">a</a>',
            'The open in new window function is not working.'
        );
        $this->assertEquals(
            cts_create_link('"', '"'),
            '<a title="&quot;" href="&quot;">&quot;</a>',
            'The parameters are not checked for html special chars.'
        );
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_create_link_img()
    */
    public function test_cts_create_link_img()
    {
        $this->assertEquals(
            cts_create_link_img('a', 'b'),
            '<a href="b"><img border="0" src="a"/></a>',
            'The given parameters are not reflected in the html.'
        );
        $this->assertEquals(
            cts_create_link_img('"', '"'),
            '<a href="&quot;"><img border="0" src="&quot;"/></a>',
            'The parameters are not checked for html special chars.'
        );
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_create_missing_link()
    */
    public function test_cts_create_missing_link()
    {
        $this->assertEquals(
            cts_create_missing_link('a'), 
            '<font color="#FE0101">a</font>', 
            'The given parameters are not reflected in the html.'
        );
        $this->assertEquals(
            cts_create_missing_link('"', 'b'), 
            '<font color="#FE0101">&quot;</font>', 
            'The parameters are not checked for html special chars.'
        );
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_create_uid_visualization()
    */
    public function test_cts_create_uid_visualization()
    {
        //INVALID TYPE
        $this->assertEquals(
            cts_create_uid_visualization('','INVALID_TYPE'),
            'Invalid Entry Type',
            'The default error message is not present.'
        );
        
        //INVALID UID - Repository
        $this->assertEquals(
            cts_create_uid_visualization('invalid',CTR_ENTRY_REPOSITORY),
            'WARNING: UID DOES NOT EXIST',
            'An invalid repository uid with no values should return a warning.'
        );

        $this->assertEquals(
            cts_create_uid_visualization('invalid',CTR_ENTRY_REPOSITORY, 'alias'),
            'alias',
            'An invalid repository uid with just an alias should return said alias.'
        );
        
        //INVALID UID - Data
        $this->assertEquals(
            cts_create_uid_visualization('invalid',CTR_ENTRY_DATA),
            'WARNING: UID DOES NOT EXIST',
            'An invalid data uid with no values should return a warning.'
        );

        $this->assertEquals(
            cts_create_uid_visualization('invalid',CTR_ENTRY_DATA, 'alias'),
            'alias',
            'An invalid data uid with just an alias should return said alias.'
        );
        
        //INVALID UID - Plugin
        $this->assertEquals(
            cts_create_uid_visualization('invalid',CTR_ENTRY_PLUGIN),
            'WARNING: UID DOES NOT EXIST',
            'An invalid plugin uid with no values should return a warning'
        );

        $this->assertEquals(
            cts_create_uid_visualization('invalid',CTR_ENTRY_PLUGIN, 'alias'),
            'alias',
            'A plugin repository uid with just an alias should return said alias.'
        );
        
        
        //This category doesn't exist for repositories
        //VALID UIDS WITH ALIASES - Data
        $this->assertEquals(
            cts_create_uid_visualization('5af86082-3ede-11e2-a8ce-73de4b15570a',CTR_ENTRY_DATA),
            'codelet-management',
            'An aliased data uid with no values should return the alias of the entry.'
        );

        $this->assertEquals(
            cts_create_uid_visualization('5af86082-3ede-11e2-a8ce-73de4b15570a',CTR_ENTRY_DATA, 'alias'),
            'alias',
            'An aliased data uid with an alias should return said alias.'
        );
        
        
        //This category doesn't exist for repositories
        //VALID UIDS WITH NAMES AND ALIASES - Plugins
        $this->assertEquals(
        cts_create_uid_visualization('d9737b5f-b6ce-4a4a-96e5-a13ae8867e99',CTR_ENTRY_PLUGIN),
                    'user',
                    'An aliased plugin uid with no values should return the alias of the entry.'
        );

        
        //This category does not relate to plugins
        //VALID UID WITH ALIASES AND NO NAMES
        $this->markTestIncomplete('We need have local repositories for the full test.');
        $this->markTestIncomplete('We need to find data with aliases and no name, and with names but no aliases.');
        "
        VALID UID WITH ALIASES AND NO NAMES (does not exist for plugins)
        repos, /, / => 'No Alias'
        repos, 'name', / => 'No Alias'
        repos, /, 'alias' => 'alias'
        repos, 'name', 'alias' => 'alias'
        
        data, /, / => check data and give (alias)
        data, 'name', / => 'name'
        data, /, 'alias' => '(alias)'
        data, 'name', 'alias' => 'name'
        
        
        
        
        
        VALID UID WITH NAMES AND NO ALIASES (does not exist for repositories)
        data, /, / => check data and give name
        data, 'name', / => 'name'
        data, /, 'alias' => check data and give name
        data, 'name', 'alias' => 'name'
        ";
    }
}


?>
