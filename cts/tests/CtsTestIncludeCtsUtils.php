<?php 
/************************************************************************
Codelet Tuning Infrastructure
Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once('PHPUnit/Framework.php');

require_once('../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');

class CtsTestCtsUtils extends PHPUnit_Framework_TestCase
{
    /**
     * @brief Called before each and every test. 
     * Usefull to declare data before each test if needed.
     */
    protected function setUp()
    {
        if(!session_id())
        {
            session_start();
        }
        if ( !isset( $_SESSION ))
        {
            $_SESSION = array();
            $_SESSION['username'] = 'admin';
            $_SESSION['pwd'] = '21232f297a57a5a743894a0e4a801fc3';
        }
        else if((!array_key_exists('username', $_SESSION)) || (!array_key_exists('pwd', $_SESSION)))
        {
            $_SESSION['username'] = 'admin';
            $_SESSION['pwd'] = '21232f297a57a5a743894a0e4a801fc3';
        }
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Called after each and every test.
    * Usefull to clean up data after each test if needed.
    */
    protected function tearDown()
    {
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Called after the end of all tests
    * Usefull to clean up on a global scale
    */
    public static function tearDownAfterClass()
    {
        session_unset();
        session_destroy();
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_is_UID()
     */
    public function test_cts_is_UID()
    {
            $this->assertFalse(cts_is_UID(''), 'An empty UID must not pass.');
            $this->assertFalse(cts_is_UID('this is clearly not an uid'), 'A incoherent UID must not pass.');
            //36 chars but no "-"
            $this->assertFalse(cts_is_UID('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'), 'A invalid format of UID must not pass.');
            
            //A string respecting the format
            $this->assertTrue(cts_is_UID('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa'), 'A valid UID was rejected.');
            //An existing plugin
            $this->assertTrue(cts_is_UID('06e2e012-5b61-11e3-9170-d3f36bd3c5f9'), 'An existing plugin UID was rejected.');
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_url_from_array()
    */
    public function test_cts_url_from_array()
    {
        $res = cts_url_from_array(array());
        $this->assertEquals($res, '?', 'Unexpected output from cts_url_from_array.');
        
        $res = cts_url_from_array(array('id'=>8));
        $this->assertEquals($res, '?id=8', 'The array parameter is not reflected on the output.');
        
        $res = cts_url_from_array(array('id'=>8, 'thing'=>'test'));
        $this->assertContains('id=8' ,$res, 'The output fails to contain a parameter.');
        $this->assertContains('thing=test' ,$res, 'The output fails to contain a second parameter.');
        $this->assertContains('&' ,$res, 'The output does not have the "&" separator between parameters.');
        $this->assertContains('?' ,$res, 'The output lacks the starting "?".');
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_make_path()
    */
    public function test_cts_make_path()
    {
        $this->assertEquals(cts_make_path(array()), 'Invalid Path', 'The empty argument failsafe is not working.');
        $this->assertEquals(cts_make_path(array('a', 'b')), '/a/b', 'Slashes are not added to the result.');
        $this->assertEquals(cts_make_path(array('a','', 'b')), '/a/b', 'Empty strings from parameters are not ignored.');
        $this->assertEquals(cts_make_path(array('a/', 'b/')), '/a/b', 'End slashes from parameters are not correctly removed.');
        $this->assertEquals(cts_make_path(array('/a', '/b')), '/a/b', 'Begin slashes from parameters are not correctly removed.');
    }
}


?>
