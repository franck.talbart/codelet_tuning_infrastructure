<?php 
/************************************************************************
Codelet Tuning Infrastructure
Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once('PHPUnit/Framework.php');
require_once('/usr/lib/phpquery/phpQuery.php');

require_once('../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');

require_once($DIRECTORY['PLUGINS'].'AliasPlugin.php');


class CtsTestTEMPLATE extends PHPUnit_Framework_TestCase
{
    /**
     * @brief Called before each and every test. 
     * Usefull to declare data before each test if needed.
     */
    protected function setUp()
    {
        if(!session_id())
        {
            session_start();
        }
        if ( !isset( $_SESSION ))
        {
            $_SESSION = array();
            $_SESSION['username'] = 'admin';
            $_SESSION['pwd'] = '21232f297a57a5a743894a0e4a801fc3';
        }
        else if((!array_key_exists('username', $_SESSION)) || (!array_key_exists('pwd', $_SESSION)))
        {
            $_SESSION['username'] = 'admin';
            $_SESSION['pwd'] = '21232f297a57a5a743894a0e4a801fc3';
        }
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Called after each and every test.
    * Usefull to clean up data after each test if needed.
    */
    protected function tearDown()
    {
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Called after the end of all tests
    * Usefull to clean up on a global scale
    */
    public static function tearDownAfterClass()
    {
        session_unset();
        session_destroy();
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_form_delete_button()
    */
    public function test_cts_form_delete_button()
    {
        $func_output = cts_form_delete_button('a', 'entry');
        $doc = phpQuery::newDocumentHTML($func_output);
        phpQuery::selectDocument($doc);
        
        $this->assertEquals(
            count(pq('a[href*="entry=a"]')), 
            1, 
            'The uid is not in the resulting form.'
        );
        $this->assertEquals(
            count(pq('a[href*="main=delete"]')),
            1, 
            'The delete main link is not present.'
        );
        $this->assertEquals(
            count(pq('a[href*="type=entry"]')), 
            1, 
            'The type is not present.'
        );
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_form_add_button()
    */
    public function test_cts_form_add_button()
    {
        $func_output = cts_form_add_button('d9737b5f-b6ce-4a4a-96e5-a13ae8867e99');
        $doc = phpQuery::newDocumentHTML($func_output);
        phpQuery::selectDocument($doc);
        
        $this->assertEquals(
            count(pq('input[name="produced_by"][type="hidden"][value="d9737b5f-b6ce-4a4a-96e5-a13ae8867e99"]')), 
            1, 
            'The uid is not in the resulting form or not hidden.'
        );
        $this->assertEquals(
            count(pq('input[name="main"][type="hidden"][value="cts_input_form"]')), 
            1, 
            'The add main link is not present or not hidden.'
        );
        $this->assertEquals(
            count(pq('input[name="command"][type="hidden"][value="init"]')), 
            1,
            'The command is not present, not hidden, or the default command does not work.'
        );
        
        
        $func_output = cts_form_add_button('d9737b5f-b6ce-4a4a-96e5-a13ae8867e99', 'update');
        $doc = phpQuery::newDocumentHTML($func_output);
        phpQuery::selectDocument($doc);
        
        $this->assertEquals(
            count(pq('input[name="command"][type="hidden"][value="update"]')), 
            1, 
            'The command parameter does not work.'
        );
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_form_move_to_repository()
     */
    public function test_cts_form_move_to_repository()
    {
        ob_start();
        cts_form_move_to_repository('thisisatestnotevenanuid', CTR_REP_COMMON);
        $result = ob_get_flush();
        
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        $this->assertEquals(
            count(pq('option[value="'.CTR_REP_COMMON.'"]')), 
            0, 
            'The common directory appears where it should not.'
        );
        $this->assertEquals(
            count(pq('input[name="entry"][type="hidden"][value="thisisatestnotevenanuid"]')), 
            1, 
            'The uid is not in the resulting form or not hidden.'
        );
        $this->assertEquals(
            count(pq('input[name="main"][type="hidden"][value="move"]')), 
            1, 
            'The move main link is not present or not hidden.'
        );
        
        
        ob_start();
        cts_form_move_to_repository('thisisatestnotevenanuid', CTR_REP_TEMP);
        $result = ob_get_flush();
        
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        $this->assertEquals(
            count(pq('option[value="'.CTR_REP_TEMP.'"]')), 
            0, 
            'The temp directory appears where it should not.'
        );
        
        $this->markTestIncomplete('We should find a way to implement local directory tests.');
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_form_change_alias()
     */
    public function test_cts_form_change_alias()
    {
        $alias_plugin = new AliasPlugin();
        $_SERVER['QUERY_STRING'] = 'a';
        ob_start();
        $uid = '5af86082-3ede-11e2-a8ce-73de4b15570a';
        cts_form_change_alias($uid, CTR_ENTRY_DATA);
        $result = ob_get_flush();
        
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        $expected_output = "form[method=\"POST\"][action*=\"?page=repositories_summary&main=view_data&data_uid=$uid\"]";
        $this->assertEquals(
            count(pq($expected_output)), 
            1, 
            'The action linked to the form is missing. Expecting: '.$expected_output.' Got: '.$result
        );
        $this->assertEquals(
            count(pq('input[name="alias"][value="codelet-management"]')), 
            1, 
            'The alias input is missing or does not display the right value.'
        );
        $this->assertEquals(
            count(pq('input[type="submit"][value="Update"]')), 
            1, 
            'The form is missing the submit button.'
        );
    }
    //---------------------------------------------------------------------------
        
    /**
    * @brief Tests the function cts_form_enter_data()
    */
    /*
    public function test_cts_form_enter_data()
    {
        $json = json_decode('
            {
                "type":"thishasnothingtodowithanything",
                "name":"name",
                "value":"test",
                "desc":"The name of the application"
            }
        ');
        
        
        ob_start();
        $form_return = cts_form_enter_data($json);
        $result = ob_get_flush();
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        $this->assertFalse(
            $form_return, 
            'The function should return false when the data is not a list.'
        );
        $this->assertEquals(
            count(pq('td:contains("The name of the application: ")')), 
            1, 
            'The description should appear when it is defined.'
        );
        $this->assertEquals(
            count(pq('input[name="name"]')), 
            1, 
            'The value should have its input.'
        );
        $this->assertEquals(
            count(pq('input[name="name"][value="test"]')), 
            1, 
            'The value should be displayed when there is one.'
        );
        $this->assertEquals(
            count(pq('input[name="name"][type="text"][value="test"]')), 
            1, 
            'The type of the value should be text by default.'
        );
        
        
        
        
        
        $json = json_decode('
            {
                "type":"FILE",
                "name":"anothername",
                "value":""
            }
        ');
        
        
        ob_start();
        $form_return = cts_form_enter_data($json);
        $result = ob_get_flush();
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        $this->assertEquals(
            count(pq('td:contains("anothername: ")')), 
            1, 
            'The name should be used when there is no description.'
        );
        $this->assertEquals(
            count(pq('input[name="anothername"][type="file"]')), 
            1, 
            'The file type should be used when needed.'
        );
        
        
        
        
        
        $json = json_decode('
            {
                "type":"sdklmfsmdlf",
                "name":"a",
                "password": true,
                "value":"",
                "produced_by":"something"
            }
        ');
        
        
        ob_start();
        $form_return = cts_form_enter_data($json);
        $result = ob_get_flush();
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        $this->assertEquals(
            count(pq('input[name="a"][type="password"]')), 
            1, 
            'The password type should be used when needed.'
        );
        $this->assertEquals(
            count(pq('input[type="button"][value="Find"]')), 
            1, 
            'The Find button should be present when produced by is provided.'
        );
        
        
        
        
        
        
        $json = json_decode('
            {
                "type":"cat",
                "name":"a",
                "list": true,
                "value":"",
                "produced_by":"this"
            }
        ');
        
        
        ob_start();
        $form_return = cts_form_enter_data($json);
        $result = ob_get_flush();
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        $this->assertTrue(
            $form_return, 
            'The function should return true when the data is a list.'
        );
        $this->assertEquals(
            count(pq('textarea[name="a"]')), 
            1, 
            'The textarea should be present when in list mode.'
        );
        $this->assertEquals(
            count(pq('input[type="button"][value="Find"]')), 
            1, 
            'The Find button should be present when produced by is provided in list mode.'
        );
    }*/
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_form_generate()
    */
    /*
    public function test_cts_form_generate()
    {
        $this->markTestIncomplete('The whole mechanism has changed and needs to be reworked.');
        return;
        $_GET['page'] = 'a';
        $json = json_decode('
            {
                "list_repositories":
                [
                    {
                        "path":"\/ctr-common",
                        "type":1,
                        "name":"Common",
                        "alias":"Common"
                    },
                    
                    {
                        "path":"\/ctr-temp",
                        "type":2,
                        "name":"Temp",
                        "alias":"Temp"
                    },
                    
                    {
                        "path":"\/random_dir\/.ctr",
                        "type":0,
                        "name":"anameishere",
                        "alias":"TEST"
                    }
                ],
                "data_uid":
                {
                    "uid":"thisisnotanuid",
                    "value":"TESTAPPPROBLEM"
                },
                "data_id":"sample_id",
                "plugin_uid":"45015659-4f12-4ab2-8849-e0a4e7e4fe4b",
                "command":"init",
                "params":
                {
                    "init":
                    {
                        "attributes":{},
                        "params":[]
                    },
                    "update":
                    {
                        "attributes":{},
                        "params":[]
                    }
                    
                },
                "repository_type":"0",
                "no_update_by_plugin":"0",
                "local_repository_path":"\/random_dir\/.ctr"
            }
        ');
        
        
        ob_start();
        $form_return = cts_form_generate('update', $json);
        $result = ob_get_flush();
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        //Testing update mode
        $this->assertEquals(
            count(pq('h1:contains("Init data for")')), 
            1, 
            'The page title is not present in update mode.'
        );
        $this->assertEquals(
            count(pq('h2:contains("Current data: sample_id")')), 
            1, 
            'The name of the current data is not present.'
        );
        $this->assertEquals(
            count(pq('form[action*="command=update"]')), 
            1, 
            'The command argument in the form link is wrong.'
        );
        $this->assertEquals(
            count(pq('form[action*="page=a"]')), 
            1, 
            'The page argument in the form link is wrong.'
        );
        $this->assertEquals(
            count(pq('form[action*="main=cts_input_execute_form"]')), 
            1, 
            'The main argument in the form link is wrong.'
        );
        $this->assertEquals(
            count(pq('form[action*="data_uid=thisisnotanuid"]')), 
            1, 
            'The data_uid argument in the form link is wrong.'
        );
        $this->assertEquals(
            count(pq('form[action*="update_from_plugin=1"]')), 
            1, 
            'The update_from_plugin argument in the form link is missing.'
        );
        
        //General repository list testing
        $this->assertEquals(
            count(pq('option[value="1"]:contains("Common")')), 
            1, 
            'The common folder is not present in the repository options.'
        );
        $this->assertEquals(
            count(pq('option[value="2"]:contains("Temp")')), 
            1, 
            'The temp folder is not present in the repository options.'
        );
        $this->assertEquals(
            count(pq('option[value="anameishere"]:contains("TEST")')), 
            1, 
            'The local folder is not present in the repository options.'
        );
        $this->assertEquals(
            count(pq('option[value="anameishere"]:selected:contains("TEST")')), 
            1, 
            'The local folder is not selected, but should be.'
        );
        
        
        
        
        
        $json = json_decode('
            {
                "list_repositories":
                [
                    {
                        "path":"\/ctr-common",
                        "type":1,
                        "name":"Common",
                        "alias":"Common"
                    },
                    
                    {
                        "path":"\/ctr-temp",
                        "type":2,
                        "name":"Temp",
                        "alias":"Temp"
                    },
                    
                    {
                        "path":"\/random_dir\/.ctr",
                        "type":0,
                        "name":"anameishere",
                        "alias":"TEST"
                    }
                ],
                "data_uid":
                {
                    "uid":"thisisnotanuid",
                    "value":"TESTAPPPROBLEM"
                },
                "data_id":"sample_id",
                "plugin_uid":"45015659-4f12-4ab2-8849-e0a4e7e4fe4b",
                "command":"init",
                "params":
                {
                    "init":
                    {
                        "attributes":{},
                        "params":[]
                    },
                    "update":
                    {
                        "attributes":{},
                        "params":[]
                    }
                    
                },
                "repository_type":"1",
                "no_update_by_plugin":"0"
            }
        ');
        
        
        ob_start();
        $form_return = cts_form_generate('update', $json, true);
        $result = ob_get_flush();
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        //General tests
        $this->assertEquals(
            count(pq('option[value="anameishere"]:selected:contains("TEST")')), 
            0, 
            'The local folder is selected, but should not be.'
        );
        $this->assertEquals(
            count(pq('option[value="1"]:selected:contains("Common")')), 
            1, 
            'The common folder is not selected.'
        );
        
        //Testing the changes with true as the third param
        $this->assertEquals(
            count(pq('form[action*="command=init"]')), 
            1, 
            'The command argument in the form link is wrong when $new is set to false.'
        );
        $this->assertEquals(
            count(pq('form[action*="page=cts_input_execute_form"]')), 
            1, 
            'The page argument in the form link is wrong.'
        );
        $this->assertEquals(
            count(pq('form[action*="update_from_plugin=1"]')), 
            0, 
            'The update_from_plugin argument in the form link is present where it should not.'
        );
        
        
        $json = json_decode('
                    {
                        "list_repositories":
                        [
                            {
                                "path":"\/ctr-common",
                                "type":1,
                                "name":"Common",
                                "alias":"Common"
                            },
                            
                            {
                                "path":"\/ctr-temp",
                                "type":2,
                                "name":"Temp",
                                "alias":"Temp"
                            },
                            
                            {
                                "path":"\/random_dir\/.ctr",
                                "type":0,
                                "name":"anameishere",
                                "alias":"TEST"
                            }
                        ],
                        "data_uid":
                        {
                            "uid":"thisisnotanuid",
                            "value":"TESTAPPPROBLEM"
                        },
                        "data_id":"sample_id",
                        "plugin_uid":"45015659-4f12-4ab2-8849-e0a4e7e4fe4b",
                        "command":"init",
                        "params":
                        {
                            "init":
                            {
                                "attributes":{},
                                "params":[]
                            }
                        },
                        "repository_type":"0",
                        "no_update_by_plugin":"0"
                    }
                ');
        
        
        ob_start();
        $form_return = cts_form_generate('update', $json, true);
        $result = ob_get_flush();
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        //Testing the changes with update mode when no update function exist
        $this->assertEquals(
            count(pq('form[action*="command=init"]')), 
            1, 
            'The command argument in the form link is wrong when $new is set to false.'
        );
        $this->assertEquals(
            count(pq('form[action*="page=cts_input_execute_form"]')),
            1, 
            'The page argument in the form link is wrong.'
        );
        $this->assertEquals(
            count(pq('form[action*="update_from_plugin=1"]')), 
            0, 
            'The update_from_plugin argument in the form link is present where it should not.'
        );
        
        
        
        $json = json_decode('
                    {
                        "list_repositories":
                        [
                            {
                                "path":"\/ctr-common",
                                "type":1,
                                "name":"Common",
                                "alias":"Common"
                            },
                            
                            {
                                "path":"\/ctr-temp",
                                "type":2,
                                "name":"Temp",
                                "alias":"Temp"
                            },
                            
                            {
                                "path":"\/random_dir\/.ctr",
                                "type":0,
                                "name":"anameishere",
                                "alias":"TEST"
                            }
                        ],
                        "data_uid":
                        {
                            "uid":"thisisnotanuid",
                            "value":"TESTAPPPROBLEM"
                        },
                        "data_id":"sample_id",
                        "plugin_uid":"45015659-4f12-4ab2-8849-e0a4e7e4fe4b",
                        "command":"init",
                        "params":
                        {
                            "init":
                            {
                                "attributes":{},
                                "params":[]
                            },
                            "update":
                            {
                                "attributes":{},
                                "params":[]
                            }
                            
                        },
                        "repository_type":"0",
                        "no_update_by_plugin":"0"
                    }
                ');
        
        
        ob_start();
        $form_return = cts_form_generate('add', $json);
        $result = ob_get_flush();
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        
        //Testing add mode
        $this->assertEquals(
            count(pq('h1:contains("Add data for")')), 
            1, 
            'The page title is not present in add mode.'
        );
        $this->assertEquals(
            count(pq('h2:contains("Current data: sample_id")')), 
            0, 
            'The name of the current data is present when it should not.'
        );
        $this->assertEquals(
            count(pq('form[action*="command=init"]')), 
            1, 
            'The command argument in the form link is wrong in add mode.'
        );
        $this->assertEquals(
            count(pq('form[action*="page=cts_input_execute_form"]')), 
            1, 
            'The page argument in the form link is wrong.'
        );
        $this->assertEquals(
            count(pq('form[action*="data_uid=thisisnotanuid"]')), 
            1, 
            'The data_uid argument in the form link is wrong.'
        );
        $this->assertEquals(
            count(pq('form[action*="update_from_plugin=1"]')), 
            0, 
            'The update_from_plugin argument is present where it should not.'
        );
    }*/
}

?>
