<?php 
/************************************************************************
Codelet Tuning Infrastructure
Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once('PHPUnit/Framework.php');
require_once('/usr/lib/phpquery/phpQuery.php');


require_once('../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');


class CtsTestCtsTable extends PHPUnit_Framework_TestCase
{
    /**
     * @brief Called before each and every test. 
     * Usefull to declare data before each test if needed.
     */
    protected function setUp()
    {
        if(!session_id())
        {
            session_start();
        }
        if ( !isset( $_SESSION ))
        {
            $_SESSION = array();
            $_SESSION['username'] = 'admin';
            $_SESSION['pwd'] = '21232f297a57a5a743894a0e4a801fc3';
        }
        else if((!array_key_exists('username', $_SESSION)) || (!array_key_exists('pwd', $_SESSION)))
        {
            $_SESSION['username'] = 'admin';
            $_SESSION['pwd'] = '21232f297a57a5a743894a0e4a801fc3';
        }
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Called after each and every test.
    * Usefull to clean up data after each test if needed.
    */
    protected function tearDown()
    {
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Called after the end of all tests
    * Usefull to clean up on a global scale
    */
    public static function tearDownAfterClass()
    {
        session_unset();
        session_destroy();
        return;
    }
    //---------------------------------------------------------------------------
    
    /**
     * @brief Tests the function cts_table_parameter_to_array()
     */
    public function test_cts_table_parameter_to_array()
    {
        $res = cts_table_parameter_to_array(0,0,0);
        
        $this->assertArrayHasKey('current', $res, 'The resulting array lacks values.');
        $this->assertArrayHasKey('size', $res, 'The resulting array lacks values.');
        $this->assertArrayHasKey('sorted', $res, 'The resulting array lacks values.');
        $this->assertArrayHasKey('pages', $res, 'The resulting array lacks values.');
        $this->assertArrayHasKey('total', $res, 'The resulting array lacks values.');
        
        $this->assertEquals(
            $res,
            array('current'=> 1, 'sorted' => 0, 'pages' => 1, 'total' => 0, 'size' => 0), 
            'The result does not have the right values.'
        );
        
        //Normal test
        $res = cts_table_parameter_to_array(0, 81, 20);
        $this->assertEquals(
            $res,
            array('current'=> 1, 'sorted' => 0, 'pages' => 5, 'total' => 81, 'size' => 20), 
            'The fields calculation has failed.'
        );
        
        //Testing $_GET surdefinition of parameters
        $_GET = array(
            'size_0' => 10,
            'current_0' => 5,
            'sorted_0' => 10
        );
        $res = cts_table_parameter_to_array(0, 81, 20);
        $this->assertEquals(
            $res,
            array('current'=> 5, 'sorted' => 10, 'pages' => 9, 'total' => 81, 'size' => 10), 
            'The $_GET variable is not taken into account.'
        );
        
        
        //Safety for current page
        $_GET = array('current_0' => 5);
        $res = cts_table_parameter_to_array(0,10,100);
        $this->assertEquals($res['current'],1, 'The safety for the current page value is not working.');
        
        
        //Testing that $_GET of another table isn't conflicting
        $_GET = array(
            'size_0' => 5,
            'current_0' => 5,
            'sorted_0' => 10
        );
        $res = cts_table_parameter_to_array(1,100,10);
        $this->assertEquals(
            $res,
            array('current'=> 1, 'sorted' => 0, 'pages' => 10, 'total' => 100, 'size' => 10),
            'The $_GET values concerning another table are conflicting.'
        );
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_table_array_to_parameter()
    */
    public function test_cts_table_array_to_parameter()
    {
        //Checking for empty behavior
        $changes = array();
        $this->assertEquals(cts_table_array_to_parameter($changes,0), '?', 'Unexpected output from cts_table_array_to_parameter.');
        
        //This should not change anything
        $changes = array('pages'=> 9999);
        $this->assertEquals(cts_table_array_to_parameter($changes,0), '?', 'The pages parameter should not be visible.');
        
        //This should change the URL
        $changes = array('whatever'=> 9999);
        $this->assertEquals(cts_table_array_to_parameter($changes,0), '?whatever_0=9999', 'Other changes through the parameters should be visible.');
        
        //This should put a "&" between two parameters of the URL
        $changes = array('whatever'=> 9999, 'unrelated'=>'thisisacat');
        $result = cts_table_array_to_parameter($changes,0);
        $this->assertContains('whatever_0=9999', $result, 'The result should contain the value.');
        $this->assertContains('unrelated_0=thisisacat', $result, 'The result should contain the second value.');
        $this->assertContains('?', $result, 'The result should contain the starting "?".');
        $this->assertContains('&', $result, 'The result should contain the "&" separator.') ;
        
        //Checking that $_GET is used.
        $_GET = array('test'=>5);
        $changes = array();
        $this->assertEquals(cts_table_array_to_parameter($changes,0), '?test=5', 'The result should change with $_GET.');
        
        //Checking that $_GET and args cohabit
        $changes = array('test'=>8);
        $result = cts_table_array_to_parameter($changes,0);
        $this->assertContains('test=5', $result, 'Parameter value is missing because of overrides.');
        $this->assertContains('test_0=8', $result, '$_GET value is missing because of overrides.');
        
        //Checking that args overwrite $_GET
        $_GET = array('test_0'=>5);
        $this->assertContains('test_0=8', cts_table_array_to_parameter($changes,0), 'Parameters with the exact same name should ovewrite $_GET values.');
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_table_sort_array_by_field()
    */
    public function test_cts_table_sort_array_by_field()
    {
        return;
        $table = array(
            array('test'=>'ba'),
            array('test'=>'az'),
            array('test'=>'aa'),
        );
        $sorted = cts_table_sort_array_by_field($table, 'test');
        
        
        //Does it conserve table size?
        $this->assertEquals(count($table), count($sorted), 'The sorted table has a different size than the sorted one.');
        
        //Does it sort?
        $previous = $sorted[0]['test'];
        for($i = 1; $i<count($sorted); $i++)
        {
            $this->assertGreaterThan(strnatcmp($previous, $sorted[$i]['test']), 0, 'The values are not sorted correctly.');
            $previous = $sorted[$i]['test'];
        }
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_table_sort_table()
    */
    public function test_cts_table_sort_table()
    {
        return;
        $table = array(
            array('test','ba', 'header'=>true),
            array('test','az'),
            array('test','aa'),
        );
        $sorted = cts_table_sort_table($table, 2);
        
        //We already know that the sort part work (with another test case).
        //All we need to check is that the header is still the first one.
        $this->assertArrayHasKey('header', $sorted[0], 'The header is not in the first case anymore.');
    }
    //---------------------------------------------------------------------------
    
    /**
    * @brief Tests the function cts_table_create()
    */
    public function test_cts_table_create()
    {
        return;
        $table = array(
            array('I', 'am', 'header'),
            array('test','ba', 'nothing'),
            array('test','az', 'thisisatest'),
            array('test','aa', 'whatever'),
        );
        
        ob_start();
        cts_table_create(0, $table);
        $result = ob_get_flush();
        
        $doc = phpQuery::newDocumentHTML($result);
        phpQuery::selectDocument($doc);
        
        //There should be one table in the output.
        $this->assertEquals(1, count(pq('table')), 'The table does not exist.');
        
        //The resulting table should have one header.
        $this->assertEquals(1, count(pq('table > thead')), 'The table does not have one and only one header.');
        
        //The header should have three rows.
        $this->assertEquals(3, count(pq('table > thead > tr > th')), 'The header does not have exactly three columns.');
        
        //The resulting table should have three columns.
        foreach(pq('table > tbody > tr') as $tr)
        {
            $this->assertEquals(3,count(pq($tr)->find('td')), 'The table does not have exactly three columns in all lines.');
        }
        $tbody = pq('table > tbody');
        $this->assertEquals(3, count(pq($tbody)->find('tr')), 'The table does not have exactly three rows.');
    }
}


?>
