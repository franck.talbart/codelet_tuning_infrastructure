#!/usr/bin/python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

import cti

from cti_hapi import database_manager, database, entry
from plugin_config import config 

import os

db = database.Database()
result = database_manager.search({},
                                db,
                                "plugin",
                                [
                                    "plugin.plugin_uid",
                                    "plugin.alias",
                                ]
                             )
                             
plugin_class_file = open(os.path.join("plugins", "Plugin.php"), "w")
plugin_class_file.writelines(open(os.path.join("plugins", "header.txt"), "r").readlines())
plugin_class_file.writelines("""
/**
 *
 * @brief Mother class of all plugins
 * This class is defined to hold all common method and attributes of the plugin classes.
 */
abstract class Plugin
{
    var $uid;
    var $daemon_commands;
    var $daemon_produced_by;
    var $no_repository;

    static $plugin_uids = array(
""")
for r in result:
    plugin_uid = cti.CTI_UID(str(r[0]))
    plugin_alias = str(r[1])
    class_name = plugin_alias.split("_")
    class_name = "".join([x.capitalize() for x in class_name])
    plugin_class_file.write("        '%s' => '%s',\n" % (plugin_uid, class_name))
    class_file = open(os.path.join("plugins", class_name + "Plugin.php"), "w")
    class_file.writelines(open(os.path.join("plugins", "header.txt"), "r").readlines())
    class_file.writelines("""require_once('Plugin.php');
 
class %sPlugin extends Plugin
{
    /**
     * Constructor. Initialises the uid.
     */
    function __construct()
    {
        parent::__construct();
        $this->uid = '%s';
""" % (class_name, plugin_uid))
    if config.has_key(class_name):
        if config[class_name].has_key("daemon_commands"):
            class_file.write("        $this->daemon_commands = array('")
            class_file.write("','".join(config[class_name]["daemon_commands"]))
            class_file.write("');\n")
        if config[class_name].has_key("daemon_produced_by"):
            class_file.write("        $this->daemon_produced_by = array(")
            daemon_produced_by = []
            for k, v in config[class_name]["daemon_produced_by"].items():
                daemon_produced_by.append("'%s' => '%s'" % (k, v))
            class_file.write(",".join(daemon_produced_by))
            class_file.write(");\n")
    class_file.writelines("""    }
    //---------------------------------------------------------------------------
    """)
    if config.has_key(class_name):
        if config[class_name].has_key("actions"):
            class_file.writelines("""
    /**
    *
    * @brief Returns the cts action links related to entries created by that plugin.
    * @return an array containing the needed info for displaying action links.
    */
    public function get_actions(){
        return array(""")
            for action in sorted(config[class_name]["actions"]):
                name = config[class_name]["actions"][action]["name"]
                url = config[class_name]["actions"][action]["url"]

                class_file.writelines("""
            '%s' => array(
                'name' => '%s',
                'url' => '%s',
            ),""" % (action.replace("'", "\\'"), name.replace("'", "\\'"), url.replace("'", "\\'")))

            class_file.writelines("""
        );
    }
    //---------------------------------------------------------------------------
            """)

    infile, outfile = entry.load_defaults(plugin_uid)

    for cmd in infile:
        class_file.writelines("""
    /**
     * @brief A parser for parameters of the %s command.
     * @param params: the parameters
     * @return An array representing the json with command parameters.
     */
    public function %s_parse($params)
    {
        $json = array(
            '%s' => array(
                'attributes' => array('name' => '%s')
                ),
        );
        """ % (cmd, cmd, cmd, cmd))
        for param in infile[cmd].params:
            if infile[cmd].params[param][cti.META_ATTRIBUTE_TYPE] == cti.META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN:
                class_file.writelines("""
        if(array_key_exists('%s', $params))
        {
            if ($params['%s'] === "1")
                $params['%s'] = True;
                    
            if ($params['%s'] != True)
                $params['%s'] = False;
                    
            $json['%s']['params'][] = array(
                        'name' => '%s',
                        'value' =>  $params['%s'],
            );
        }
        else
        {
            $json['%s']['params'][] = array(
                        'name' => '%s',
                        'value' => False,
            );
        }
        """ % (param, param, param, param, param, cmd, param, param, cmd, param))
            else:
                if param != 'format':
                    class_file.writelines("""
        if(array_key_exists('%s', $params))
        {
            $json['%s']['params'][] = array(
                        'name' => '%s',
                        'value' => $params['%s'],
            );
        }
            """ % (param, cmd, param, param))
                else:
                    class_file.writelines("""
        if(array_key_exists('format', $params))
        {
                $json['%s']['params'][] = array(
                            'name' => 'format',
                            'value' => $params['format'],
                );
        }
        else
        {
            $json['%s']['params'][] = array(
                        'name' => 'format',
                        'value' => 'json',
            );
        }""" % (cmd, cmd))

        if cmd == "init":
            class_file.writelines("""
        if(array_key_exists('repository_attr', $params))
            $json['init']['attributes']['repository'] = $params['repository_attr'];
            """)

        class_file.writelines("""
        return $json;
    }
    //---------------------------------------------------------------------------""") 
    class_file.writelines("""
}
?>
""")
    class_file.close()

plugin_class_file.writelines(open(os.path.join("plugins", "plugin_footer.txt"), "r").readlines())
plugin_class_file.close()

