#!/usr/bin/python
#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

config =\
                { 
                    "Application": 
                                    {
                                        "actions":
                                                {
                                                    "application_create_compile_entry":
                                                                                {
                                                                                    "name": "Compile",
                                                                                    "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                                                }
                                                },
                                    },
                    "Binary": 
                                    {
                                        "actions":
                                                {
                                                    "binary_profile_maqao_perf":
                                                                                {
                                                                                    "name": "Profile with MAQAO perf",
                                                                                    "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                                                },
                                                    "binary_profile_icc":
                                                                                {
                                                                                    "name": "Profile with ICC",
                                                                                    "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                                                },
                                                    "binary_profile_vtune":
                                                                                {
                                                                                    "name": "Profile with Vtune",
                                                                                    "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                                                },
                                                    "binary_create_decan_entry":
                                                                                    {
                                                                                        "name": "DECAN",
                                                                                        "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                                                    }
                                                }
                                    },
                    "Compile":
                                    {
                                        "daemon_commands": ["run"],
                                        "daemon_produced_by":
                                        {
                                            "run": "entry",
                                            "init": "application"
                                        },
                                        "actions":
                                                {
                                                    "compile_run":
                                                         {
                                                              "name": "Run Compilation",
                                                              "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                         },
                                                }
                                    },
                    "Daemon":
                                    {
                                        "daemon_produced_by":
                                        {
                                            "init": "produced_by"
                                        },
                                        "actions":
                                                {
                                                    "kill":
                                                         {
                                                              "name": "Kill",
                                                              "url": "?page=repositories_summary&main=action_run"
                                                         },
                                                    "run":
                                                         {
                                                              "name": "Run",
                                                              "url": "?page=repositories_summary&main=action_run"
                                                         },
                                                }
                                    },
 
                    "Decan":
                                    {
                                        "daemon_commands": ["run"],
                                        "daemon_produced_by":
                                        {
                                            "run": "entry",
                                            "init": "original_binary"
                                        },
                                        "actions":
                                                {
                                                    "run":
                                                         {
                                                              "name": "Run DECAN",
                                                              "url": "?page=repositories_summary&main=action_run"
                                                         },
                                                }
 
                                    },
                    "IccProfiler":
                                    {
                                        "daemon_commands": ["run"],
                                        "daemon_produced_by":
                                        {
                                            "init": "binary",
                                            "run": "entry"
                                        },
                                        "actions":
                                                {
                                                    "icc_profiler_run":
                                                         {
                                                              "name": "Run the Icc profiler",
                                                              "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                         },
                                                }
                                    },
                    "LoopGroup":
                                    {
                                        "actions":
                                                {
                                                    "loop_group_create_maqao_cqa_entry":
                                                        {
                                                            "name": "MAQAO CQA",
                                                            "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                        }
                                                }
                                    },                                   
                    "MaqaoCqa":
                                    {
                                        "daemon_commands": ["run"],
                                        "daemon_produced_by":
                                        {
                                            "run": "entry",
                                            "init": "loop_group"
                                        },
                                        "actions":
                                                {
                                                    "maqao_cqa_run":
                                                         {
                                                              "name": "Run MAQAO CQA",
                                                              "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                         },
                                                }
                                    },
                    "MaqaoPerf":
                                    {
                                        "daemon_commands": ["run"],
                                        "daemon_produced_by":
                                        {
                                            "run": "entry",
                                            "init": "binary"
                                        },
                                        "actions":
                                                {
                                                    "maqao_perf_run":
                                                         {
                                                              "name": "Run MAQAO Perf",
                                                              "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                         },
                                                }
                                    },
                    "Process":
                                    {
                                        "daemon_commands": [
                                                                "run_command",
                                                                "run"
                                                           ]
                                    },
                    "Rm":
                                    {
                                        "daemon_commands": ["repository"]
                                    },
                    "Vtune":
                                    {
                                        "daemon_commands": ["run"],
                                        "daemon_produced_by":
                                        {
                                            "run": "entry",
                                            "init": "binary",
                                        },
                                        "actions":
                                                {
                                                    "vtune_run":
                                                         {
                                                              "name": "Run Vtune",
                                                              "url": "?page=repositories_summary&main=action_cts_input_form&mode=add"
                                                         },
                                                }
                                    }
                }
