/*
************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

/**
*@brief Shows a layer
*@param object: the layer
*/
function sb_show(object)
{
    if (!$(object).is(':visible'))
    {
        $(object).show( 'slide', {direction: "up"}, 500);
    }
}
//---------------------------------------------------------------------------

/**
*@brief Hide a layer
*@param object: the layer
*/
function sb_hide(object)
{
    if (typeof sb_hide.ignore == 'undefined') sb_hide.ignore = false;
    
    // To correct a display problem when clicking many times on the "Search" button.
    if (sb_hide.ignore) return;
    
    if ($(object).is(':visible'))
    {
        sb_hide.ignore = true;
        $(object).hide(
            'slide',
            {direction: "up"},
            500,
            function()
            {
                sb_hide.ignore = false;
            }
        );
    }
}
//---------------------------------------------------------------------------

$(document).ready( function()
{
    form = $('#search_box form');
    form.on('submit', 
        function()
        {
            sb_hide('#advanced_search_box');
            load_main_frame(form.serialize()); // Serialized the form and reload the page.
            return false;
        }
    );
});
