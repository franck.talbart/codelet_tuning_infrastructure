/*
************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

// Authors: Florent Hemmi, Franck Talbart, Mathieu Bordet, Nicolas Petit

/**
 * @brief Dashboard namespace.
 * Provide a set of functions to manage the dashboard.
 */
var cts_dashboard =
{
    /**
     * @brief The settings
     */
    settings:
    {
        div: '#dashboard', // The dashboard div
        timeBeforeRetry: 3, // Time before resend a request
        /**
         * @brief Draggable options
         */
        dragOptions:
        {
            containment: '#main_frame',
            cursor: 'move',
            grid: [20, 20],
            scroll: false,
            snap: true,
            
            /**
             * @brief Function called when the user has finished
             * to drag the widget.
             * When it is called, this function update the dashboard
             * with the new widget settings and save it
             */
            stop: function(event)
            {
                var target_id = event.target.id; // => 'widget_#'
                id = cts_dashboard.utils.get_id_of_attr(target_id);
                
                var data = $(this).position();
                var json = JSON.stringify(data);
                
                var params = 'action=update&widget='+id+'&json='+json;
                cts_dashboard.query(null, params);
            }
        },
        /**
         * @brief Resizable options
         */
        resizeOptions:
        {
            containment: '#main_frame',
            grid: 20,
            
            /**
             * @brief Function called when the user has finished
             * to drag the widget.
             * When it is called, this function update the dashboard
             * with the new widget settings and save it
             */
            stop: function(event, ui)
            {
                var target_id = event.target.id; // => 'widget_#'
                id = cts_dashboard.utils.get_id_of_attr(target_id);
                
                var data = ui.size;
                var json = JSON.stringify(data);
                
                var params = 'action=update&widget='+id+'&json='+json;
                cts_dashboard.query(null, params);
            }
        }
    },
    
    /**
     * Provide a set of useful functions for different actions.
     */
    utils:
    {
        /**
         * @param str the widget name (i.e. : widget_#)
         * @return the ID from the widget name
         */
        get_id_of_attr: function(str)
        {
            var pos = str.indexOf('_')+1;
            var id = parseInt(str.substring(pos));
            return id;
        },
        
        /**
         * @brief Add the javascript event to allow the user
         * to modify the title directly by clicking
         * @param the widget
         */
        add_title_event: function(element)
        {
            var id_attr = '#'+element.attr('id');
            var id = cts_dashboard.utils.get_id_of_attr(id_attr);
            
            var content = id_attr+' .content';
            var input_field = id_attr+' .title input';
            var title = id_attr+' .title';
            
            var save_title = function()
            {
                if ($(input_field).length != 0)
                {
                    var title_value = $(input_field).val();
                    $(title).html(title_value);
                    
                    var array = { 'title': title_value };
                    var json = JSON.stringify(array);
                    var url = 'action=update&widget='+id+'&json='+json;
                    cts_dashboard.query(null, url);
                }
            };
            
            $(title).click( function() 
            {
                if ($(input_field).length == 0)
                {
                    $(this).html('<input name="title" type="text" value="'+$(this).html()+'" />');
                    $(input_field).focus();
                    
                    $(input_field).bind(
                    {
                        keypress: function(event)
                        {
                            if (event.keyCode == 13)
                                save_title();
                        },
                        
                        focusout: function()
                        {
                            save_title();
                        }
                    });
                }
            });
                    
            $(content).click( function()
            {
                if ($(input_field).length != 0)
                    save_title();
            });
        },
        
        /**
         * @brief Add the javascript event to allow the user
         * to modify the color directly by clicking
         * @param the widget
         */
        add_color_event: function(element)
        {
            var id_attr = '#'+element.attr('id');
            var id = cts_dashboard.utils.get_id_of_attr(id_attr);
            
            var color = $(id_attr+' .color input');
            color.colorPicker();
            color.change(function()
            {
                var array = { 'color': color.val() };
                var json = JSON.stringify(array);
                var url = 'action=update&widget='+id+'&json='+json;
                cts_dashboard.query(function(data)
                {
                    cts_dashboard.refresh_widget(id, data);
                }, url);
            });
        }
    },
    
    /**
     * @brief Initialize a widget and add the event for the widget
     * id the widget id
     */
    init_widget: function(id)
    {
        $('#widget_'+id+' .title').css('cursor', 'text');
        $('#widget_'+id+' .close a').css('cursor', 'pointer');
        
        element = $('#widget_'+id);
        
        element.draggable(cts_dashboard.settings.dragOptions);
        element.resizable(cts_dashboard.settings.resizeOptions);
        cts_dashboard.utils.add_title_event(element);
        
        if ($('#widget_'+id+' .color').length != 0)
        {
            cts_dashboard.utils.add_color_event(element);
        }
        
        var cssProperties =
        {
            'position': 'absolute',
            'cursor': 'move'
        };
        element.css(cssProperties);
    },
    
    /**
     * @brief Initialize the dashboard and all the widget.
     * A query is done for all widgets to position them correctly.
     */
    init: function()
    {
        $('.widget').each( function()
        {
            var id_attr = $(this).attr('id');
            var id = cts_dashboard.utils.get_id_of_attr(id_attr);
            
            cts_dashboard.init_widget(id);
            
            var element = $(this);
            var url = 'action=get&widget='+id;
            cts_dashboard.query(function(data)
            {
                w = JSON.parse(data);
                var cssProperties =
                {
                    'top': w.top+'px',
                    'left': w.left+'px',
                    'width': w.width+'px',
                    'height': w.height+'px'
                };
                element.css(cssProperties);
            }, url);
        });
    },
    
    /**
     * @brief Delete a widget from the dashboard.
     */
    delete_widget: function(element)
    {
        widget = $(element).closest('.widget');
        
        if (!confirm('Are you sure?')) return;
        
        id_attr = '#'+widget.attr('id');
        id = cts_dashboard.utils.get_id_of_attr(id_attr);
        
        widget.remove();
        
        var url = 'action=delete&widget='+id;
        cts_dashboard.query(null, url);
    },
    
    /**
     * @brief Refresh a specific widget with the following data.
     * Before doing the refresh, this function save the widget
     * properties and restore it after refreshing.
     * @param the widget id
     * @param the new widget data
     */
    refresh_widget: function(id, data)
    {
        id_attr = '#widget_'+id;
        var cssProperties =
        {
            'top': $(id_attr).css('top'),
            'left': $(id_attr).css('left'),
            'width': $(id_attr).css('width'),
            'height': $(id_attr).css('height')
        };
        $(id_attr).replaceWith(data);
        $(id_attr).css(cssProperties);
        cts_dashboard.init_widget(id);
    },
    
    /**
     * @brief Execute a query to the dashboard
     * with POST data
     * @param callback the function to execute
     * if the query ended successfully
     * @param params the parameters to send
     * @param retry true if the function must retry after error
     */
    query: function(callback, params, retry)
    {
        func = this.query;
        if (typeof func.prevParams == 'undefined') func.prevParams == '';
        
        if (typeof params == 'undefined') params = '';
        if (typeof retry == 'undefined') retry = true;
        
        if (callback == null) callback = function() {};
        
        $.ajax(
        {
            type: 'POST',
            data: params,
            url: '../ajax/dashboard.php',
            error: function(msg)
            {
                if (msg.status == 403)
                {
                    alert('The session has expired.');
                    location.reload();
                    return;
                }
                
                console.log('An error has occured during the request.');
                console.log(msg.responseText);
                
                if (retry)
                {
                    time = cts_dashboard.settings.timeBeforeRetry;
                    func = function()
                    {
                        cts_dashboard.query(callback, params, false);
                    };
                    console.log('Try again in '+time+' seconds...');
                    setTimeout(func, time*1000);
                }
                else
                {
                    console.log('Request has been aborted after two attempts.');
                }
            },
            success: function(data)
            {
                callback(data);
            }
        });
    },
    
    /**
     * @brief Hide the dashboard.
     */
    hide: function()
    {
        $(this.settings.div).hide();
    },
    
    /**
     * @brief Show the dashboard.
     * If the user is not on the dashboard,
     * the dashboard page is reload.
     */
    show: function()
    {
        if ($(cts_dashboard.settings.div).length == 0)
        {
            window.location.replace('?page=dashboard');
            return;
        }
        
        $('#main_frame').css('height', $('#main_frame').height()-500+'px');
        
        var form = '#form_create_widget';
        if ($(form).length != 0)
            $(form).remove();
        
        display_loading_animation(false);
        $(this.settings.div).show();
    },
    
    /**
     * @brief Display the form for creating widget.
     */
    form_create_widget: function()
    {
        if ($(cts_dashboard.settings.div).length == 0)
        {
            window.location.replace('?page=dashboard');
            return;
        }
        
        $('#main_frame').css('height', $('#main_frame').height()+500+'px');
        
        var form_div = '#form_create_widget';
        
        if ($(form_div).length != 0) return;
        
        $(cts_dashboard.settings.div).hide();
        display_loading_animation(true);
        
        var url = 'action=form';
        cts_dashboard.query(function(data)
        {
            display_loading_animation(false);
            $(cts_dashboard.settings.div).after(data);
            
            $('.chart_options').hide();
            $('#form_create_widget input[name=type]').change(function ()
            {
                $('[class$=_options]').hide();
                var val = $("#form_create_widget input[name=type]:checked").val();
                
                if (val == 0) $('.text_options').show();
                else if (val == 1) $('.chart_options').show();
            });
            
            $(form_div+' form').submit(function()
            {
                display_loading_animation(true);
                
                var data = $(this).serializeArray();
                data.push({name: 'dashboard', value: '1'});
                
                var url = $.param(data);
                cts_dashboard.query(function(data)
                {
                    $(cts_dashboard.settings.div).append(data);
                    var id_attr = $('.widget').last().attr('id');
                    var id = cts_dashboard.utils.get_id_of_attr(id_attr);
                    
                    cts_dashboard.init_widget(id);
                    cts_dashboard.show();
                    
                    display_loading_animation(false);
                },
                url);
                
                return false;
            });
        }, url);
    },
    
    /**
     * @brief Sort a widget
     * @param id the widget id
     * @param n_sorted the column to sort
     */
    sort_widget: function(id, n_sorted)
    {
        var array = { 'params': { 'n_column_sort': n_sorted } };
        var json = JSON.stringify(array);
        var url = 'action=update&widget='+id+'&json='+json;
        cts_dashboard.query(function(data)
        {
            cts_dashboard.refresh_widget(id, data);
        }, url);
    }
};

// Start the function to initialize the dashboard.
$(document).ready(function()
{
    $('#left_menu .navigation a').css('cursor', 'pointer');
    $('#main_frame').css('width', $('#main_frame').width()+50+'px');
    $('#main_frame').css('height', $('#main_frame').height()+200+'px');
    
    cts_dashboard.init();
});
