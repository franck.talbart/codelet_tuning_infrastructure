/*
************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

// Author: Florent Hemmi

/**
 * @brief refresh the content of the textarea for the ViewPlugin
 * @param params serialize PHP array
 * @param refresh frequency in ms
 * @return setInterval ID to give to stop_refresh() for destruction
 */
function start_refresh(params, refresh)
{
    callback = function()
    {
        message = document.getElementById('txt');
        
        // Fix: stopping the refresh if the page is changed with AJAX.
        // In fact, in the other page, we hope that the textarea 'txt' does not exist.
        if (message == null)
        {
            update_refresh_status('', 0, null); // To keep a consistency with the parameters.
            return false; // Cancel the current request.
        }
        
        $.ajax(
        {
            type: 'GET',
            url: '../ajax/file_viewer.php?params='+params,
            error:function(msg)
            {
                $('#main_frame').html(msg.responseText);
            },
            success:function(data)
            {
                $('#txt').val(data);
                message.scrollTop = message.scrollHeight;
            }
        });
    };
    return setInterval(callback, refresh);
}
//---------------------------------------------------------------------------

/**
 * @brief stop the refresh of the content
 * @param id setInterval ID for destruction
 */
function stop_refresh(id)
{
    clearInterval(id);
}
//---------------------------------------------------------------------------

/**
 * @brief enable or disable the refresh
 * @param params serialize PHP array
 * @param refresh frecency in ms
 * @param obj the link to change
 * @return the interval id
 */
function update_refresh_status(params, refresh, obj)
{
    if (typeof this.refreshStatus == 'undefined') this.refreshStatus = false;
    if (typeof this.intervalId == 'undefined') this.intervalId = 0;
    
    if (typeof params != 'undefined')
    {
        if (this.refreshStatus) // Stop refresh
        {
            if (this.intervalId) stop_refresh(this.intervalId);
            if (obj != null) obj.innerHTML = 'Start refresh';
        }
        else // Start refresh
        {
            this.intervalId = start_refresh(params, refresh);
            if (obj != null) obj.innerHTML = 'Stop refresh';
        }
        this.refreshStatus = !this.refreshStatus;
    }
    
    return this.intervalId;
}
