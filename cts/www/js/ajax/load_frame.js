/*
************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

// Author: Florent Hemmi

/**
 * @brief Object for the AJAX request
 * @param params string with the parameters
 * @parmam currentParams refresh the page with the parameters even if there is a request before
 * @param method GET or POST
 * @param postParams POST parameters to send
 */
function AjaxLoaderWithHistory(params, currentParams, method, postParams)
{
    func = AjaxLoaderWithHistory;
    
    if (typeof func.disableHistory == 'undefined') func.disableHistory = false;
    
    this.type = method;
    this.url = '../ajax/get_page.php?'+params;
    if (postParams) this.data = postParams;
    
    this.error = function(msg)
    {
        display_loading_animation(false);
    };
    this.success = function(data)
    {
        // Loading the next page, because we don't need to refresh the current page.
        if (currentParams === '')
        {
            $('#main_frame').html(data);
            display_loading_animation(false);
            
            if (!func.disableHistory && params != '') save_to_history('?'+params);
            else func.disableHistory = false;
        }
        // Loading the good page.
        else
        {
            load_main_frame(currentParams); // Without additional parameters (infinite loop otherwise).
            func.disableHistory = true;
        }
        
        highlight_current_left_menu(params);
    };
}
//---------------------------------------------------------------------------

/**
 * @brief load the page in the main frame div
 * @param obj link (or string with the parameters)
 * @param currentParams refresh the page with the parameters even if there is a request before
 * @param ignoreSameRequests if set to true, same requests will not be executed
 * @param method GET or POST
 * @param postParams POST parameters to send
 */
function load_main_frame(obj, currentParams, ignoreSameRequests, method, postParams)
{
    func = load_main_frame;
    if (typeof func.prevParams == 'undefined') func.prevParams == '';
    
    if (typeof currentParams == 'undefined') currentParams = '';
    if (typeof ignoreSameRequests == 'undefined') ignoreSameRequests = false;
    if (typeof method == 'undefined') method = 'GET'; // By default, run GET ajax request.
    if (typeof postParams == 'undefined') postParams = '';
    
    if (typeof obj != 'string') // Link.
    {
        pos = obj.href.indexOf('?')+1; // Skip the '?'.
        params = obj.href.substring(pos, obj.href.length);
    }
    else params = obj; // Only the needed parameters.
    
    if (ignoreSameRequests)
    {
        // Don't execute the query if the previous was the same (GET only).
        if (params == func.prevParams && method == 'GET') return;
    }
    func.prevParams = params;
    
    sb_hide('#advanced_search_box');
    display_loading_animation(true);
    
    var options = new AjaxLoaderWithHistory(params, currentParams, method, postParams);
    start_ajax_request(options);
}
//---------------------------------------------------------------------------

/**
 * Save the url to the navigation history
 * @param url to save
 */
function save_to_history(url)
{
    window.History.pushState(null, null, url);
}
//---------------------------------------------------------------------------

// Manage the history (previous and next button of the browser).
// The good page is automatically reloaded.
$(document).ready( function() {
    var History = window.History;
    History.Adapter.bind(window, 'statechange', function() {
        var url = History.getState().url;
        var pos = url.indexOf('?')+1;
        // Url empty if there is no '?' or nothing after the '?'.
        var empty_url = pos == 0 || url.length-pos == 0;
        
        if (!empty_url)
        {
            params = url.substring(pos, url.length);
            load_main_frame(params, '', true);
        }
        else load_main_frame('', '', true); // Loading the default page.
    });
});
//---------------------------------------------------------------------------

/**
 * Selects and highlight the current selected main frame in the left menu if applicable
 * @param get_string: a string representing the GET part of the url.
 */
function highlight_current_left_menu(get_string)
{
    var get = get_array_from_url('?' + get_string);
    if(!get['page'] || !get['main'])
    {
        return;
    }
    var selector = '#left_menu a';
    $(selector).removeClass('highlit');
    for(attribute in get)
    {
        if(attribute != 'search_query')
        {
            selector = selector + '[href*="'+attribute+'='+get[attribute]+'"]';
        }
    }
    if(get['search_query'])
    {
        result = get['search_query'].match(/plugin_uid\:\{[^}]*\}/g);
        if(!result)
        {
            selector = selector + '[href*="search_query='+get['search_query']+'"]';
        }
        else
        {
            selector = selector + '[href*="search_query="][href*="'+result+'"]';
        }
    }
    
    $(selector).addClass('highlit');
}
//---------------------------------------------------------------------------

/**
 * Parses the given url string into an array
 * @returns an associative array containing the parameters
 */
function get_array_from_url(url) {
    var get_params = {};
    (url).replace(
        /[?&]+([^=&]+)=([^&]*)/gi, 
        function(m,key,value) 
        {
           get_params[key] = value;
        }
    );
    return get_params;
}