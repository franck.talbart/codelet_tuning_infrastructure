/*
************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

/**
 * @brief Use this to check if a checkbox is checked are not
 * @param obj: the checkbox to verify
 * @return 1 if the checkbox is checked, 0 otherwise
 */
function is_checked(obj)
{
    if (obj.is(':checked')) return 1;
    else return 0;
}
//---------------------------------------------------------------------------

/**
 * @brief Initialize, move and hide all forms of the pagem
 */
function init_all_forms()
{
    var forms = $('div.full-view');
    var toolbar = $('#toolbar');
    var cssProperties =
    {
        'position': 'absolute',
        'top': toolbar.position().top+toolbar.height()+15+'px',
        'left': '38%'
    };
    
    forms.hide();
    forms.css(cssProperties); // Apply the CSS
}
//---------------------------------------------------------------------------

/**
 * @brief Display a form with the correct animation
 * @param element: the form to display
 */
function display_form(element)
{
    func = display_form;
    
    // Animation time to hide the previous form
    // and show the new one.
    var time = 300;
    // Animation style
    var animOptions = {direction: 'up'};
    
    // Variable to store the last displayed element.
    if (typeof func.last_element == 'undefined') func.last_element = '';
    
    if (element != func.last_element)
    {
        if (func.last_element != '')
        {
            $(func.last_element).hide('slide', animOptions, time, function()
            {
                $(element).show('slide', animOptions, time);
            });
        }
        else $(element).show('slide', animOptions, time);
        func.last_element = element;
    }
    else
    {
        $(element).hide('slide', animOptions, time);
        func.last_element = '';
    }
}
//---------------------------------------------------------------------------

/**
 * @brief Display the content of a file (text or image)
 * @param link: the link in a string
 * @param sideBySide: for the side by side display
 */
function display_file(link, sideBySide)
{
    func = display_file;
    
    if (typeof func.prevParams == 'undefined') func.prevParams == '';
    if (typeof func.first == 'undefined') func.first = true;
    if (typeof sideBySide == 'undefined') sideBySide = false;
    
    file_block = $('#file-content');
    
    // Stopping the AJAX refresh if displaying an other file.
    if (func.first)
    {
        var id = update_refresh_status();
        if (typeof id != 'undefined' && id != 0)
        {
            stop_refresh(id);
        }
        func.first = false;
    }
    link_obj = $('a[href$="'+link+'"]');
    
    if (link == func.prevParams) 
    {
        return;
    }
    else 
    {
        func.prevParams = link;
    }
    
    if (sideBySide)
    {
        link += '&side_by_side=1';
    }
    
    $.ajax(
    {
        type: 'GET',
        url: '../ajax/full_view.php?'+link,
        error:function(msg)
        {
            file_block.html(msg.responseText);
        },
        success:function(data)
        {
            file_block.html(data);
        }
    });
    
    highlight_selected_file(link_obj);
}
//---------------------------------------------------------------------------

/**
 * @brief Hilghlight the currently displayed file, among those in the list
 * @param obj: the file link (html <a> element)
 */
function highlight_selected_file(obj)
{
    files_tbody = obj.parent().parent().parent();
    files_tbody.find('a[href*="page=files"]').css('font-weight', 'normal').css('color', 'grey');
    obj.css('font-weight', 'bold').css('color', 'black');
}
//---------------------------------------------------------------------------

$(document).ready( function()
{
    if ($('div.full-view').length != 0)
    {
        init_all_forms();
        $('#toolbar a').css('cursor', 'pointer');
    }
});