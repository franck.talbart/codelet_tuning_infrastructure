/*
************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

/*
*@brief display the content of the update output
*@param uid the entry uid
*@param refresh frequency in ms
*/
function update_file_viewer(uid, refresh)
{
    $.ajax(
        {
          type: "GET",
         url: "ajax/update_viewer.php?uid="+uid,
            error:function(msg)
            {
                alert( "Error!: " + msg.responseText );
            },
            success:function(data)
            {
                $('#txt').text(data);
                $('#txt').scrollTop($('#txt')[0].scrollHeight);
            }
        }
    );
    return 0;
}




/**
*@brief check the update status
*@param uid the entry uid
*@param refresh frequency in ms
*/
function get_update_status(uid, refresh)
{
    $.ajax(
        {
            type: "GET",
            url: "ajax/status_update.php?uid="+uid,
            error:function(msg)
            {
                alert( "Error!: " + msg.responseText );
            },
            success:function(data)
            {
                data = trim(data);
                if (data == '')
                {
                    content = '<br/><strong>...Updating...</strong><br/><img src="img/ajax-loader2.gif"/>';
                    setTimeout(
                        function()
                        {
                            get_update_status(uid, refresh);
                        }, 
                        refresh
                    ); 
                }
                else if (data == '0')
                {
                    content = '<br/><strong>Successfully updated!</strong><br/><img src="img/success.png" />';
                }
                else
                {
                    content = '<br/><strong>Something wrong happened.Please check build.tmp/cti_compil.log!</strong><br/><img src="img/failure.png" />';
                }
                
                $('#update_status').html(content);
                update_file_viewer(uid, refresh);
            }
        }
    );
}
