/*
************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

/**
*@brief Open a popup. The user can choose an UID from a list (cts_input plugin)
*@param produced_by_uid The plugin UID which produce the data
*@param field the name of the field
*@param is_multiple 1 if the field can take several values, 0 otherwise 
*/
function open_popup(produced_by_uid, field, is_multiple)
{
    new_window = window.open(
        'index_popup.php?main=cts_input_multiple_choices' +
            '&produced_by=' + produced_by_uid + '&field=' + field + 
            '&is_mult=' + is_multiple,
        'name',
        'height=400,width=300,scrollbars=yes'
    );
    
    if (window.focus) 
    {
        new_window.focus();
    }
    
    return false;
}
