/*
************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

//---------------------------------------------------------------------------

/**
*@brief Remove spaces at the beginning and at the end of the string
*@param myString: the string to trim
*/
function trim (myString)
{
    return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
}
//---------------------------------------------------------------------------

/**
 * @brief Display a loading animation for AJAX query
 * @param bool: true to display the ajax animation, false to stop it
 */
function display_loading_animation(bool)
{
    var main_frame = $('#main_frame');
    var animation = $('.ajax-loading');
    
    // CSS for the transparent div.
    // This div is useful to hide the content below and prevent clicking
    // on the main frame.
    var divName = 'transparent-div';
    var cssProperties = {
        'position': 'absolute',
        'top': main_frame.offset().top+'px',
        'left': main_frame.offset().left+'px',
        'width': main_frame.width()+30+'px',
        'height': '100%',
        'z-index': '2000'
    };
    
    if (bool)
    {
        main_frame.css({ opacity: 0.3 });
        $('body').css('overflow', 'hidden');
        $('body').append('<div id="'+divName+'"></div>');
        $('#'+divName).css(cssProperties);
        animation.show();
    }
    else
    {
        main_frame.css({ opacity: 1 });
        $('body').css('overflow', 'auto');
        $('#'+divName).remove();
        animation.hide();
    }
}
//---------------------------------------------------------------------------

/**
 * @brief Start a unique AJAX request and ignore all other request at the same time
 */
function start_ajax_request(options)
{
    func = start_ajax_request; // More convenient for static variables.
    if (typeof func.lock == 'undefined') func.lock = false;
    
    if (!func.lock)
    {
        var e = options.error;
        var s = options.success;
        options.error = function(msg){
            func.lock = false;
            e(msg);
        };
        options.success = function(data){
            func.lock = false;
            s(data);
        };
        
        func.lock = true;
        func.ajaxRequest = $.ajax(options);
    }
}

//---------------------------------------------------------------------------

/**
 * @brief Save a query
 * @param url: the URL to save
 */
function save_query(url)
{
    var query_label = prompt ("What is the name of the query?","");
    $.ajax({
          type: "GET",
          url: "ajax/save_query.php?label=" + query_label +"&query=" + encodeURIComponent(url),
          success:function(data)
          {
            location.reload();
          }
        });
}

//---------------------------------------------------------------------------

/**
 * @brief Delete a query
 * @param id: the id of the query 
 */
function delete_query(id)
{
    $.ajax({
          type: "GET",
          url: "ajax/delete_query.php?id=" + id,
          success:function(data)
          {
            location.reload();
          }
        });
}

