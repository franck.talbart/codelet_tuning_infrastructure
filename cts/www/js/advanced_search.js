/*
************************************************************************
 Codelet Tuning Infratructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************
*/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

//---------------------------------------------------------------------------
//
//Changing dates when loading the page.
$(document).ready( 
    function()
    {
        $( "#date_start_p" ).datetimepicker({timeFormat: 'hh:mm:ss', dateFormat: 'yy/mm/dd'});
        $( "#date_end_p" ).datetimepicker({timeFormat: 'hh:mm:ss', dateFormat: 'yy/mm/dd'});
        update_list_fields($('#type_p').val());
    }
);

