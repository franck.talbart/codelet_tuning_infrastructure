<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Florent Hemmi, Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['VIEW'].'Dashboard.php');
require_once($DIRECTORY['PLUGINS'].'QueryPlugin.php');

require_once($DIRECTORY['INCLUDE'].'pchart/class/pData.class.php');
require_once($DIRECTORY['INCLUDE'].'pchart/class/pDraw.class.php');
require_once($DIRECTORY['INCLUDE'].'pchart/class/pImage.class.php');

if (!isset($_SESSION))
    session_start();


/**
 * 
 * @brief Convert a hexadecimal color to a RGB color
 * @param the hexadecimal color
 * @return an array with the RGB color
 */
function hex2rgb($hex)
{
    $hex = str_replace("#", "", $hex);
    
    if(strlen($hex) == 3)
    {
        $r = hexdec(substr($hex,0,1).substr($hex,0,1));
        $g = hexdec(substr($hex,1,1).substr($hex,1,1));
        $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    }
    else
    {
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
    }
    $rgb = array(
        'R' => $r,
        'G' => $g,
        'B' => $b
    );
    return $rgb;
}
//---------------------------------------------------------------------------

/**
 * 
 * @brief Display a chart with the following parameters
 * @param $data the RAW data to display
 * @param $color the chart color
 * @param $params the widget parameters
 */
function print_chart($data, $color, $params)
{
    global $DIRECTORY;
    
    $time = array();
    $value = array();
    
    foreach ($data as $ckey => $cvalue)
    {
        $time[] = $cvalue['time'];
        $value[] = $cvalue['value'];
    }
    
    $pdata = new pData();
    $pdata->addPoints($time, 'time');
    $pdata->addPoints($value, 'value');
    $pdata->setAxisName(0, 'Value');
    $pdata->setAbscissa('time');
    $pdata->setAbscissaName('Time');
    
    if ($params['time_limit'] < 24*3600)
        $pdata->setXAxisDisplay(AXIS_FORMAT_TIME, 'H:i');
    else
        $pdata->setXAxisDisplay(AXIS_FORMAT_DATE, 'd/m');
    
    $pdata->setPalette('value', hex2rgb($color));
    
    $picture = new pImage(380, 150, $pdata, true);
    $picture->Antialias = false;
    $picture->setGraphArea(30, 30, 360, 120);
    
    $max = max($value);
    if (count($value) < 2)
    {
        $max += 3;
    }
    
    $axisBoundaries = array(0 => array('Min' => 0, 'Max' => $max));
    $picture->setFontProperties(array('FontName' => $DIRECTORY['INCLUDE'].'pchart/fonts/GeosansLight.ttf', 'FontSize' => 10));
    $scaleSettings = array('XMargin' => 0, 'YMargin' => 0, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $axisBoundaries);
    $picture->drawScale($scaleSettings);
    
    $picture->drawLineChart();
    
    $picture->Stroke();
}
//---------------------------------------------------------------------------


// Check if the user is already connected.
if (!isset($_SESSION['login_uid']))
{
    header('Status: 403');
    return;
}

// Retrieve a dashboard instance.
if (!isset($_SESSION['dashboard']))
{
    $dashboard = new Dashboard($_SESSION['login_uid']);
    $_SESSION['dashboard'] = serialize($dashboard);
}
else
{
    $dashboard = unserialize($_SESSION['dashboard']);
}


if (isset($_GET['action']))
{
    switch ($_GET['action'])
    {
        // Display a chart widget
        case 'view':
            if (!isset($_GET['widget']))
                break;
            
            $id = intval($_GET['widget']);
            
            $w = $dashboard->get_widget($id);
            if ($w && Widget::$types[$w->get_type()] == 'chart')
            {
                $data = $w->get_data();
                $color = $w->get_color();
                $params = $w->get_params();
                print_chart($data, $color, $params);
            }
            break;
    }
    return;
}


if (isset($_POST['action']))
{
    switch ($_POST['action'])
    {
        // Add a widget
        // When adding, the HTML code of the widget is printed
        // and the user is redirected automatically to the dashboard
        // if there is no javascript
        case 'add':
            $ajax = false;
            if (isset($_POST['dashboard']) && $_POST['dashboard'] == 1)
                $ajax = true;
            
            $params = array();
            
            $params['query'] = array();
            $params['params'] = array();
            foreach($_POST as $key => $value)
            {
                if (preg_match('/_p$/', $key))
                {
                    $key = str_replace('_p', '', $key);
                    $params['query'][$key] = $value;
                }
                else if (preg_match('/_o$/', $key))
                {
                    $key = str_replace('_o', '', $key);
                    $params['params'][$key] = $value;
                }
                else
                {
                    $params[$key] = $value;
                }
            }
            $params['query']['fields'] = $_POST['fields'];
            $array_parse_int = array('time_limit', 'record_limit', 'number_results', 'n_column_sort');
            foreach ($array_parse_int as $key)
            {
                $params['params'][$key] = intval($params['params'][$key]);
            }
            
            $id = $dashboard->add_widget($params);
            $w = $dashboard->get_widget($id);
            $w->html();
            
            if (!$ajax)
                header('Location: ../?page=dashboard');
            
            break;
        
        // For deleting a widget
        case 'delete':
            if (!isset($_POST['widget']))
                break;
            
            $id = intval($_POST['widget']);
            $dashboard->delete_widget($id);
            
            if (isset($_POST['dashboard']) && $_POST['dashboard'] == 0)
            {
                header('Location: ../?page=dashboard');
            }
            break;
        
        // Display the form for creating a widget
        case 'form':
            $dashboard->form_create_widget();
            break;
        
        // Print the JSON widget code
        case 'get':
            if (!isset($_POST['widget']))
                break;
            
            $id = intval($_POST['widget']);
            
            $w = $dashboard->get_widget($id);
            if ($w)
            {
                echo json_encode($w->to_array());
            }
            break;
        
        // Update a widget with the JSON given in parameters
        case 'update':
            if (!isset($_POST['widget']) || !isset($_POST['json']))
                break;
            
            $post_params = object_to_array(json_decode($_POST['json']));
            $id = intval($_POST['widget']);
            
            $w = $dashboard->get_widget($id);
            if ($w)
            {
                $new_params = $post_params;
                if (isset($post_params['params']))
                {
                    $new_params = $w->get_params();
                    foreach ($post_params as $key => $value)
                    {
                        foreach ($value as $ckey => $cvalue)
                        {
                            $new_params[$ckey] = $cvalue;
                        }
                    }
                    $new_params = array('params' => $new_params);
                }
                $w->set($new_params);
                $dashboard->set_widget($id, $w);
                $dashboard->save();
                $w->html();
            }
            break;
    }
}


// Save the new dashboard.
$_SESSION['dashboard'] = serialize($dashboard);
?>
