<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['VIEW'].'SavedQueries.php');

if (!isset($_SESSION))
    session_start();


// Check if the user is already connected.
if (!isset($_SESSION['login_uid']))
{
    header('Status: 403');
    return;
}

if (isset($_GET['id']))
{
    $id = $_GET['id'];
    $login_uid = $_SESSION['login_uid'];

    $saved_queries = new SavedQueries($login_uid);
    return $saved_queries->delete_query($id);
}

?>
