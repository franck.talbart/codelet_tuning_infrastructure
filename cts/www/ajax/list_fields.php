<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['PLUGINS'].'ViewPlugin.php');

if (!isset($_SESSION))
    session_start();
if ($_GET['produced_by'] != 'None' && $_GET['produced_by'] != 'undefined')
{
    $view_plugin = new ViewPlugin();

    $json = $view_plugin->get_data('plugin', array('plugin_uid' => $_GET['produced_by']));

    if (!@$json->CTI_PLUGIN_CALL_ERROR && @$json->output_file->init)
    {
        ?>
            <br/>
        <?php
        foreach ($json->output_file->init->params as $param)
        {
            ?>
                <input type="checkbox" name="fields[]" value="<?php echo $param->name;?>"><?php echo $param->name;?>
                <br/>
            <?php
        }
    }
    else
    {
        echo "Cannot get the field list for this type.";
    }

}
else
{
    echo 'Please specify the entry type to get more fields.';
}
?>

