<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

?>
    <div id="header">
        <div id="logo_left">
            <a href="?">
                <img src="img/cts_logo.png" border="0" height="100"/>
            </a>
        </div>
        <div id="login_head">
<?php 

if (isset($_SESSION['login_uid']))
{
    $login_uid = $_SESSION['login_uid'];
}
else
{
    $login_uid = '';
}

if($is_login == False)
{
    ?>
        <li>
            <a href="?page=login">Sign In</a>
        </li>
    <?php 
}
else
{
    echo 'Welcome '.$_SESSION['username'].' | ';
    ?>
        <a href="?page=logout&destroy_cookie=1">Log out</a>
    <?php 
}

?>
    </div>
    <div id="top-right-menu">
        <ul>
            <li>
                <a href="<?php echo $LINK['redmine'];?>" target="_blank">
                    <img border=0 height=40 src="img/feedback.png"/>
                    <br/>Feedback
                </a>
            </li>
            <li>
                <a href="<?php echo $LINK['wiki'];?>" target="_blank">
                    <img border=0 height=40 src="img/doc.png"/>
                    <br/>Help
                </a>
            </li>
            <li>
                <a href="?page=repositories_summary&main=view_data&data_uid=<?php echo $login_uid;?>" onclick="load_main_frame(this); return false;">
                    <img border=0 height=40 src="img/settings.png"/>
                    <br/>Settings
                </a>
            </li>
        </ul>
    </div>
</div>