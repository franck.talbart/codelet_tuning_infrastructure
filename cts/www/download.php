<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

ob_start(); // Redirect the outputs, so that we avoid blank lines before the headers...
require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['PLUGINS'].'RepositoryPlugin.php');
require_once($DIRECTORY['PLUGINS'].'EntryPlugin.php');


$plugin = $_GET["plugin"];
$uid = $_GET['uid'];

if (!isset($_GET['name']))
{
    $name = cts_create_uid_visualization($uid, CTR_ENTRY_DATA).'.zip';
}
else
{
    $name = $_GET['name'];
}

$cmd = 'archive';
if (isset($_GET['cmd']))
    $cmd = $_GET['cmd'];

// The session may be already opened
if (!isset($_SESSION))
    session_start();

$temp = tempnam(sys_get_temp_dir(), 'cts_');

$repository_plugin = new RepositoryPlugin();
$entry_plugin = new EntryPlugin();

switch ($plugin)
{
    case $repository_plugin->uid:
        $info = $repository_plugin->get_data(
            'archive',
            array(
                "repository" => $uid,
                "output_filename" => $temp,
            )
        );
        break;
    case $entry_plugin->uid:
        if ($cmd == 'export')
        {
            $info = $entry_plugin->get_data(
                'export',
                array(
                    "format_output" => "csv",
                    "uid_list" => array($uid),
                    "filename" => $temp,
                )
            );
        }
        else
        {
            $params = array(
                "data_uid" => $uid,
                "output_filename" => $temp,
            );
            if (isset($_GET['filename']))
            {
                $params['filename'] = $_GET['filename'];
            }
            $info = $entry_plugin->get_data($cmd,$params);
        }
        break;
    default:
        echo 'Unknown plugin';
        exit(0);
}

ob_end_clean();
// for example:  application/zip
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment; filename=$name");
header('Pragma: no-cache');
header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
readfile($temp);
// Remove the temp file
unlink($temp);
?>
