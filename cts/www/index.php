<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

session_start();

/*
 * Include
 */
require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');

// Cookie
manage_cookies();

// We first check that ctr-common exists
if (!is_dir($CFG['ctr_common'].'/plugins'))
{
    die("Error: the common repository does not exist. CTS can't work.");
}

// Login
$is_login = False;
if (isset($_SESSION['username']) && isset($_SESSION['pwd']) && login_user($_SESSION['username'], $_SESSION['pwd']))
{
    $is_login = True;
}
else
{
    if(!isset($_SESSION['previous_page']) || (isset($_SESSION['previous_page']) && $_SESSION['previous_page'] == ''))
    {
        $url = parse_url($_SERVER['REQUEST_URI']);
        if (isset($url['query']) && $url['query'] != '' && $url['query'] != 'page=login')
            $_SESSION['previous_page'] = $_SERVER['REQUEST_URI'];
    }
    $_GET['page'] = 'login';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<!-- Franck Talbart, Mathieu Bordet, Nicolas Petit (Intel / CEA / GENCI / UVSQ) -->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Codelet Tuning Infrastructure</title>
        <meta name="description" content="CTI" />

        <link href="<?php echo $DIRECTORY['THEMES'].$CFG['theme'];?>/theme.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo $DIRECTORY['THEMES'].$CFG['theme'];?>/colorPicker.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />

        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>jquery-1.5.min.js" ></script>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>jquery.history.js" ></script>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>jquery-ui-1.8.17.custom.min.js" ></script>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>jquery-ui-timepicker-addon.js" ></script>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>cts_input.js" ></script>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>general.js" ></script>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>search_box.js" ></script>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>left_menu.js" ></script>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>ajax/load_frame.js" ></script>
    </head>
    <body>
    <div id="header">
        <?php include("header.php"); ?>
    </div>
<?php
if ($is_login)
{
    ?>
        <div id="top-menu">
    <?php
    include("menu.php");
    ?>
        </div>
    <?php
}

?>
    <div id="main">
        <div class="ajax-loading" style="display:none">
            <img border="0" src="<?php echo $DIRECTORY['IMG'];?>ajax-loader.gif" alt="Loading" />
        </div>
<?php
    // getting the default page if the page isn't set.
    if (!isset($_GET['page']))
    {
        $_GET['page'] = $CFG['default_page'];
    }

    if (security_include($_GET['page']))
    {
        require_once($DIRECTORY['PAGES'].$_GET['page'].'.php');
    }
    else
    {
        echo "This page is not allowed!";
    }
?>
        <div id="bgimg"></div>
    </div>
    <div id="footer">
<?php

include("footer.php");

?>
        </div>
    </body>
</html>
