<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php'); 

// The session is maybe already opened
if (!isset($_SESSION))
    session_start();
    
if (isset($_GET['main']))
{
    $page = $_GET['main'];
}
else
{

    if (isset($_GET['action'])) // Used by cts table (links)
    {
        if ($_GET['action'] == 'cts_input')
        {
            $page = 'cts_input_multiple_choices';
        }
    }
    else
    {
        $page = '';
    }
}

if (security_include($page))
{
    require_once($DIRECTORY['PAGES']."$page.php");
}
else
{
    echo "This page is not allowed!";
}
?>
