<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['VIEW'].'html_page_list_plugins_choices.php');

$current = '';
if(isset($_GET['page']))
{
    $current = $_GET['page'];
}

?>
    <div id="top-menu-list">
        <ul>
            <li><a href="?"><img src="img/home.png" border=0 width=15 height=15/></a></li>
            <?php if($CFG['enable_dashboard']){?><li><a <?php if($current == 'dashboard'){ echo 'class="highlit"';}?> href="?page=dashboard">Dashboard</a></li><?php }?>
            <li><a <?php if($current == 'repositories_summary'){ echo 'class="highlit"';}?> href="?page=repositories_summary">Repositories</a></li>
            <li><a <?php if($current == 'codelets'){ echo 'class="highlit"';}?> href="?page=codelets">Codelet management</a></li>
            <li><a <?php if($current == 'architecture'){ echo 'class="highlit"';}?> href="?page=architecture">Architectures</a></li>
            <li><a <?php if($current == 'experiments'){ echo 'class="highlit"';}?> href="?page=experiments">Experiments</a></li>
            <li><a <?php if($current == 'files'){ echo 'class="highlit"';}?> href="?page=files">Files</a></li>
<?php 

if ($_SESSION['username'] == 'admin')
{
    ?>
        <li><a <?php if($current == 'administration'){ echo 'class="highlit"';}?> href="?page=administration">Administration</a></li>
    <?php 
}

?>
        </ul>
    </div>

<span id="search_box">

       <form method="GET" name="search_box"> 
           <input type="hidden" name="page" value="repositories_summary"/>
           <input type="hidden" name="main" value="query"/>
           <span id="advanced_search_box">
               <div class="block" style="background-color: white;">
                   <img border="0" src="<?php echo $DIRECTORY['IMG'];?>close_button.png" style="position: relative; top: 1px; right: -330px;" onClick="sb_hide('#advanced_search_box')"/>
                   <div align="center">
                       <table>
                           <tr>
                               <td>Repository</td>
                               <td>
<?php 

cts_form_list_repositories();

?>
                        </td>
                    </tr>
                </table>
            </div>
            <table>
               <tr>
                    <td>Alias</td>
                    <td>
                    <?php // alias_s since alias already use in the view page  ?>
                        <input type="text" name="alias_s" id="alias_s" value="<?php 
                            if (isset($_GET['alias_s'])) echo htmlentities($_GET['alias_s']);
                        ?>"/>
                    </td>
                </tr>
                <tr>
                    <td>Type (plugin)</td>
                        <td>
                          <?php
                             $type_default = '';
                             if (isset($_GET['produced_by']))
                                 $type_default = $_GET['produced_by'];
                             list_plugins_choices_display("produced_by", $type_default, "");
                          ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Date start</td>
                        <td>
                            <script>
                                $(
                                    function()
                                    {
                                        $( "#date_start" ).datetimepicker({timeFormat: 'hh:mm:ss', dateFormat: 'yy/mm/dd'});
                                    }
                                );
                            </script>
                            <input type="text" id="date_start" name="date_start" value="<?php 
                                if (isset($_GET['date_start'])) echo htmlentities($_GET['date_start']);
                            ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Date end</td>
                        <td>
                            <script>
                                $(
                                    function()
                                    {
                                        $( "#date_end" ).datetimepicker({timeFormat: 'hh:mm:ss', dateFormat: 'yy/mm/dd'});
                                    }
                                );
                            </script>
                            <input type="text" id="date_end" name="date_end" value="<?php 
                                if (isset($_GET['date_end'])) echo htmlentities($_GET['date_end']);
                            ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Additional files</td>
                        <td>
                        <?php // file_s since file already use in the view page  ?>
                            <input type="text" name="file_s" id="file_s" value="<?php 
                                if (isset($_GET['file_s'])) echo htmlentities($_GET['file_s']);
                            ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Tag</td>
                        <td>
                        <?php // tag_s since tag already used in the view page  ?>
                            <input type="text" name="tag_s" id="tag_s" value="<?php 
                                if (isset($_GET['tag_s'])) echo htmlentities($_GET['tag_s']);
                            ?>"/>
                        </td>
                    </tr>
                </table>
                <br/>
                <div align="center">
                    <a href="?page=repositories_summary&main=advanced_search" onclick="sb_hide('#advanced_search_box'); load_main_frame(this); return false;">
                        < Advanced search >
                    </a>
                </div>
            </div>
        </span> 
        <input type="text" onClick="sb_show('#advanced_search_box')" name="search_query" id="search_query" value="<?php 
            if (isset($_GET['search_query'])) echo htmlentities($_GET['search_query']);
        ?>"/>
        <input type="submit" value="Search" id="search_button" />
    </form>
</span> 
<span style="float:right;margin-right:7px;"><a href="<?php echo $LINK['query_doc'];?>" target="_blank"><img border="0" src="img/question_mark.png"/></a></span>
<script type="text/javascript">
    $("#advanced_search_box").hide('', {}, 0);
</script>
