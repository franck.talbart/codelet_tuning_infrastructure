<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

/* Set to true to disable debug logging */
$DEBUG_PROD = false;

/* Directories */
$DIRECTORY['IMG']          = 'img/';
$DIRECTORY['INCLUDE']      = $_SERVER['DOCUMENT_ROOT'].'../include/';
$DIRECTORY['CFG']          = $_SERVER['DOCUMENT_ROOT'].'../cfg/';
$DIRECTORY['JS']           = 'js/';
$DIRECTORY['PAGES']        = $_SERVER['DOCUMENT_ROOT'].'../pages/';
$DIRECTORY['THEMES']       = 'themes/';
$DIRECTORY['PLUGINS']      = $_SERVER['DOCUMENT_ROOT'].'../plugins/';
$DIRECTORY['VIEW']         = $_SERVER['DOCUMENT_ROOT'].'../view/';
$DIRECTORY['USER_DATA']    = $_SERVER['DOCUMENT_ROOT'].'../user-data/';

/* General Configuration */
$CFG['allowed_pages'] = array(
    'action_cts_input_form',
    'action_run',
    'administration',
    'advanced_search',
    'architecture',
    'codelets',
    'cts_input_form',
    'cts_input_execute_form',
    'cts_input_multiple_choices',
    'dashboard',
    'delete',
    'experiments',
    'files',
    'list_local_repositories',
    'list_plugin',
    'login',
    'logout',
    'move',
    'query',
    'repositories_summary',
    'update',
    'view_data',
    'view_file',
    'view_plugin',
);

$CFG['enable_dashboard'] = false;
$CFG['accepted_errors'] = array(23);
$CFG['default_page'] = 'repositories_summary';
$CFG['theme'] = 'default';
$CFG['admin_email'] = 'cti@list.exascale-computing.eu';

/* Others */ 
