if [ -f ${PIDFILE_LP} ]; 
then
    echo "Stopping old Lighttpd instance ..."
    kill -2 `cat ${PIDFILE_LP}`
    sleep 1
    rm -rf ${PIDFILE_LP}
fi

if [ "$1" != "stop" ];
then
    echo "Waiting for Lighttpd to start (several seconds) ..."
    ${DESTINATION_LP}/sbin/lighttpd -D -f ${DESTINATION_LP}/lighttpd_config & > ${LOG_LP} 2>&1
    echo "You can access CTS at the following URL: http://localhost:"${PORT_LP}
fi

