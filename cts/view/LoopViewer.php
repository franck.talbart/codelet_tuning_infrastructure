<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit, Florent Hemmi

require_once('Viewer.php');
require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['PLUGINS'].'LoopPlugin.php');

class LoopViewer extends Viewer
{
    /**
     *
     * @brief Override parent constructor. Initialises all the parameters.
     * @param params: the array of parameters
     * @param id_table: the next free table index
     * @param info: view plugin data
     */
    function __construct($params, $id_table, $info)
    {
        parent::__construct($params, $id_table, $info);
        $loop = new LoopPlugin();
        $this->uid = $loop->uid;
    }
    //---------------------------------------------------------------------------

    public function script()
    {
        parent::script();
        global $DIRECTORY;
        ?>
            <link href="<?php echo $DIRECTORY['THEMES'];?>default/codemirror.css" media="screen" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>codemirror/codemirror.js"></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>codemirror/clike/clike.js"></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>codemirror/fortran/fortran.js"></script>
        <?php
    }

    //---------------------------------------------------------------------------
    
    public function button_bar($current_tab)
    {
        $url_tab = '?page=repositories_summary&main=view_data&data_uid='.$this->info->UID;
        ?>
            <ul id="tabs">
                <li <?php if ($current_tab=='output') echo 'class="active"';?>><?php echo cts_create_link(' Output ', $url_tab, False, 'onclick="load_main_frame(this); return false;"');?></li>
                <li <?php if ($current_tab=='source') echo 'class="active"';?>><?php echo cts_create_link(' Source ', "$url_tab&current_tab=source", False, 'onclick="load_main_frame(this); return false;"');?></li>
            </ul>
        <?php
    }
    
    //---------------------------------------------------------------------------
    
    public function output()
    {
        $this->output_file();
        $this->file_content();
        $this->additional_files();
        $this->note();
        $this->tag();
    }

    //---------------------------------------------------------------------------
    
    public function source()
    {
        $view_plugin = new ViewPlugin();
        $info = $view_plugin->get_data('data', array('data_uid' => $this->info->UID));

        foreach($info->output_file->init->params as $param)
        {
            if($param->name == 'original_file')
            {
                 if(is_null($param->value))
                 {
                     $content = 'No file';
                     $first_line=1;
                 }
                 else
                 {
                     $content= $this->load_file($param->value);
                 }
            }
            if($param->name == 'language')
                $language= strtolower($param->value);
            if($param->name == 'first_line')
                $first_line= strtolower($param->value);
        }
        switch ($language)
        {
            case "c":
                $mode="text/x-csrc";
                break;
            case "fortran":
                $mode="text/x-Fortran";
                break;
default:

                $mode="text/x-csrc";


break;
        }
?>

<form>
<textarea id="code" name="code">
<?php echo $content; ?>
</textarea>
</form>

<script>
function jumpToLine(i) { 
          var t = editor.charCoords({line: i, ch: 0}, "local").top; 
              editor.scrollTo(null, t); 
}
var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
  mode: "<?php echo $mode;?>",
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true
});
var line = editor.getLineHandle(<?php echo $first_line;?>);
editor.addLineClass(line, 'background', 'line-error');
jumpToLine(<?php echo $first_line;?>);
</script>
    <?php
    }
    
    //---------------------------------------------------------------------------
    
    private function load_file($filename)
    {
        $view_plugin = new ViewPlugin();
        $info = $view_plugin->get_data('file', array('filename' => $filename, 'data_uid' => $this->info->UID));
        return $info->content;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Override. Print the HTML code for all the page
     */
    public function html()
    {
        $current_tab = 'output'; 
        if (isset($_GET['current_tab']))
            $current_tab = $_GET['current_tab'];
        
        $this->script();
        $this->toolbar();
        $this->actions();
        $this->header();
        $this->button_bar($current_tab);
        
        switch ($current_tab)
        {
            case "output":
                $this->output();
                break;
            case "source":
                $this->source();
                break;
        }
        $this->information();
    }
}
