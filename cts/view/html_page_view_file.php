<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['PLUGINS'].'ViewPlugin.php');
require_once($DIRECTORY['PLUGINS'].'EntryPlugin.php');

/*
 * @brief Display the content of a file
 * @param params serialize PHP array
 */
function ajax_file_viewer($params)
{
    $params = unserialize(urldecode($params));

    $view_plugin = new ViewPlugin();
    $info = $view_plugin->get_data('file', $params);

    if(@!$info->CTI_PLUGIN_CALL_ERROR)
    {
        //The file can still be empty, which means the contents variable can be unset.
        echo @$info->content;
    }
}
//---------------------------------------------------------------------------

/**
 * @brief Resize an image
 * @param img picture path
 * @param max the max width or height allowed
 * @return style attributes to apply to the img tag
 */
function scale_proportional($img, $max=650)
{
    list($img_w, $img_h, $type, $attr) = getimagesize($img);

    $w = 0;
    $h = 0;

    if ($img_w > $max || $img_h > $max)
    {
        $scaleDown = $max / ($img_h > $img_w ? $img_h: $img_w);
        $img_w = $img_w * $scaleDown;
        $img_h = $img_h * $scaleDown;
    }
    return 'style="width: ' . $img_w . 'px; height: ' . $img_h . 'px;"';
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Displays the plugin view using the given parameters
 * @param params: the array of parameters
 * @param standalone: set to true to display additional links (back and download)
 * @param side_by_side: true if the viewer is next to the table
 */
function view_file_display($params, $standalone=True, $side_by_side=False)
{
    global $DIRECTORY;

    $view_plugin = new ViewPlugin();
    $entry_plugin = new EntryPlugin();
    $info = $view_plugin->get_data('file', $params);

    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }

    if ($standalone)
    {
        ?><script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>ajax/view_file.js"></script><?php
    }

    $filename = @$params['filename'];
    $data_uid = @$params['data_uid'];

    $stop_refresh = false;
    if (array_key_exists('refresh', $params))
    {
        $stop_refresh = $params['refresh'] == '0';
    }

    $width = '';
    if (!$side_by_side)
    {
        ?>
            <h2>Content of the file</h2>
        <?php
        $width = 'width: 75%; ';
    }
    ?>
        <div align="center" class="block" style="<?php echo $width;?>padding: 5px 0 5px 0;">
    <?php

    if ($standalone)
    {
        echo cts_create_visualization_type(
                META_CONTENT_ATTRIBUTE_TYPE_DATA_UID,
                $data_uid,
                'Go back to the entry'
            ) .
            ' | ';
    }

    ?>
        <?php
            if (@$info->filename != "")
            {
        ?>
        <a href="download.php?plugin=<?php echo $entry_plugin->uid;?>&uid=<?php echo $data_uid;?>&name=<?php echo @$info->filename;?>.zip&filename=<?php echo $filename;?>&cmd=archive_file" target="_blank">Download</a>
        <?php
            }
        ?>
    <?php

    if (isset($info->refresh) && $info->refresh != '')
    {
        echo ' | ';

        if (!$stop_refresh)
        {
            echo cts_create_link(
                    'Stop refresh',
                    '?main=view_file&page=files&data_uid=' . $data_uid . '&filename=' . $filename . '&refresh=0',
                    False,
                    'onclick="update_refresh_status(\''.
                        urlencode(serialize($params)).'\', '.$info->refresh.'*1000, this); return false;"'
                );


            ?>
                <script type="text/javascript">
                    $(document).ready(
                        function()
                        {
                            var message = document.getElementById('txt');
                            message.scrollTop = message.scrollHeight;
                        }
                    );
                </script>
                <noscript>
                    <META HTTP-EQUIV="Refresh" CONTENT="<?php echo $info->refresh;?>">
                </noscript>

                <script type="text/javascript">
                    update_refresh_status('<?php echo urlencode(serialize($params));?>', <?php echo $info->refresh;?>*1000, null);
                </script>
            <?php
        }
        else
        {
            echo cts_create_link(
                'Start refresh',
                '?main=view_file&page=files&data_uid=' . $data_uid . '&filename=' . $filename . '&refresh=1',
                False,
                'onclick="update_refresh_status(\''.
                    urlencode(serialize($params)).'\', '.$info->refresh.'*1000, this); return false;"'
            );
        }
    }
    ?>
        <br/><br/>
    <?php

    if (@exif_imagetype(@$info->path))
    {
        ?>
                <img border="0" src="data:<?php image_type_to_mime_type(exif_imagetype($info->path));?>;base64,<?php echo base64_encode(file_get_contents($info->path));?>" <?php echo scale_proportional($info->path);?>/>
                <br/>
            </div>
            <br/>
        <?php
    }
    else
    {
        if (!$side_by_side)
        {
            $cols = 80;
            $rows = 20;
        }
        else
        {
            $cols = 60;
            $rows = 15;
        }

        ?><textarea name="txt" id="txt" cols="<?php echo $cols;?>" rows="<?php echo $rows;?>"><?php

        if (isset($info->read_error))
        {
            foreach($info->read_error as $line)
            {
                echo $line;
            }
        }
        else
        {
            echo $info->content;
        }

        ?></textarea>
            </div>
        <?php
    }
}
?>
