<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['PLUGINS'].'ViewPlugin.php');
require_once($DIRECTORY['PLUGINS'].'AliasPlugin.php');

function get_type($val)
{
    $type = gettype($val);

    switch ($type) 
    {
        case "boolean":
            $type = 'BOOLEAN';
            break;
        case "integer":
            $type = 'INTEGER';
            break;
        case "double":
            $type = 'DOUBLE';
            break;
        case "string":
            $type = 'TEXT';
            break;
        default:
            $type = 'TEXT';
    }
    return $type;
}

function echo_command_table($contents, $id_table=0)
{
    foreach ($contents as $cmd => $attrs)
    {
        ?>
       <h2>Command: <i><?php echo $cmd;?></i></h2>
       <h3>Attributes</h3>

    <?php

        $table_attr = array();

        $table_attr[] = array('Name', 'Value');
        foreach(@$attrs->attributes as $key=>$attr)
        {
            $table_attr[] = array(array("value"=> $key, "type"=>"TEXT"),array("value"=>$attr, "type"=>get_type($attr)));
        }
        ?>
 
            <div class="block" style="width: 75%;">
                <?php cts_table_create($id_table,$table_attr);?>
            </div>
    <?php

        $table_file = array();
       $table_file[] = array('Parameter','Description','Type','List','Default value');

        foreach(@$attrs->params as $param)
        {
            $table_append = array();

            if (isset($param->{META_ATTRIBUTE_NAME}))
            {
                $table_append[] = array('value' => $param->{META_ATTRIBUTE_NAME}, 'type' => 'TEXT');
            }
            else
            {
                $table_append[] = array('value' => '', 'type' => 'TEXT');
            }

            if (isset($param->{META_ATTRIBUTE_DESC}))
            {
                $table_append[] = array('value' => $param->{META_ATTRIBUTE_DESC}, 'type' => 'TEXT');
            }
            else
            {
                $table_append[] = array('value' => '', 'type' => 'TEXT');
            }

            if (isset($param->{META_ATTRIBUTE_TYPE}))
            {
                $table_append[] = array('value' => $param->{META_ATTRIBUTE_TYPE}, 'type' => 'TEXT');
            }
            else
            {
                $table_append[] = array('value' => '', 'type' => 'TEXT');
            }

            if (@$param->{META_ATTRIBUTE_LIST})
            {
                $table_append[] = array('value' => $param->{META_ATTRIBUTE_LIST}, 'type' => 'TEXT');
            }
            else
            {
                $table_append[] = array('value' => '', 'type' => 'TEXT');
            }

            $column = '';
            if (isset($param->{META_ATTRIBUTE_VALUE}))
            {
                $data_value = array($param->{META_ATTRIBUTE_VALUE});
                $sep = '';
                if (@$param->{META_ATTRIBUTE_LIST})
                {
                    $sep = ' ';
                    $data_value = $param->{META_ATTRIBUTE_VALUE};
                }
                foreach ($data_value as $value)
                {
                    if ($param->{META_ATTRIBUTE_TYPE} != META_CONTENT_ATTRIBUTE_TYPE_FILE)
                    {
                        if ($value != '')
                        {
                            $column .= cts_create_visualization_type(
                                        $param->{META_ATTRIBUTE_TYPE},
                                        $value
                            );
                        }
                        $column .= $sep;
                    }
                    else
                    {
                        $column .= $value . $sep;
                    }
                }
            }
            else
            {
                $column = NULL;

            }
            $table_type = $param->{META_ATTRIBUTE_TYPE};
            if (@$param->{META_ATTRIBUTE_LIST})
            {
                $table_type = 'TEXT';
            }
            $table_append[] = array('value' => $column, 'type' => $table_type);
            $table_file[] = $table_append;
        }
        ?>
            <br/> 
            <div class="block" style="width: 75%;">
                <?php $id_table +=1; cts_table_create($id_table,$table_file);?>
            </div>
        <?php
        $id_table += 1;
    }
    return $id_table;
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Displays the data view using the given parameters
 * @param params: the array of parameters
 * @param id_table: the next free table index
 * @return the next free table index.
 */
function view_plugin_display($params, $id_table)
{
    global $DIRECTORY;
    ?>
        <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>ajax/full_view.js"></script>
    <?php

    //Case of page refresh using the alias update button.
    if(array_key_exists('alias', $_POST))
    {
        $alias_plugin = new AliasPlugin();
        $alias_plugin->get_data('plugin', array('alias' => $_POST['alias'], 'uid' => $_GET['plugin_uid']));
    }

    $view_plugin = new ViewPlugin();
    $info = $view_plugin->get_data('plugin', $params);

    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }
    $get_params = $params; //Copying $_GET array for links modification.

    //Used when the entry is deleted.
    //When it is deleted, the user is redirected to the plugin list corresponding
    //to the entry repository.
    $repository_id = $info->repository_type;
    if ($repository_id != CTR_REP_COMMON && $repository_id != CTR_REP_TEMP)
    {
        $repository_id = $info->repository_path;
    }

    //Toolbar
    ?>
        <div id="toolbar" style="width: 290px;">
            <ul>
                <li>
    <?php
    if (!cts_form_check_add_button_disabled($info->UID))
    {
        ?>
            <a href="?page=<?php echo $_GET['page'];?>&main=cts_input_form&mode=add&produced_by=<?php echo $info->UID;?>&command=init" onclick="load_main_frame(this); return false;">
                <img border="0" src="img/add.png"/>
                <br/>Add data
            </a>
        <?php
    }
    else
    {
        ?>
            <span style="opacity: 0.4">
                <img border="0" src="img/add.png"/>
                <br/>Add data
            </span>
        <?php
    }
    ?>
                </li>
                <li>
                    <a onclick="display_form('.change_alias'); return false;"><img border="0" src="img/edit.png"/>
                        <br/>Change alias
                    </a>
                </li>
                <li>
                    <a onclick="display_form('.move_repository'); return false;">
                        <img border="0" src="img/move.png"/><br/>
                        Move to repository
                    </a>
                </li>
                <li>
                    <a href="?page=<?php echo $_GET['page'];?>&main=delete&type=entry&entry=<?php echo $info->UID;?>" onclick="if (confirm('Are you sure?')) load_main_frame(this, 'page=repositories_summary&main=list_plugin&repository_type=all'); return false;">
                        <img border="0" src="img/delete.png"/>
                        <br/>Delete plugin
                    </a>
                </li>
            </ul>
        </div>
        <div class="block full-view change_alias" style="margin: 5px 0 0 0; width: 250px;">
            <?php cts_form_change_alias($info->UID, CTR_ENTRY_PLUGIN);?>
        </div>
        <div class="block full-view move_repository" style="margin: 5px 0 0 0; width: 250px;">
            <table><?php cts_form_move_to_repository($info->UID, $info->repository_type);?></table>
        </div>
        <div class="full-view-space"></div>
        <h1 style="border-bottom: 0;">
            <?php echo htmlspecialchars(
                    cts_create_uid_visualization($info->UID,CTR_ENTRY_PLUGIN)
                );
            ?>
        </h1>
    <?php

    //Output file
    if (@$info->output_file)
    {
        ?>
        <h2>Output default file</h2>
        <?php
        $id_table = echo_command_table(
                $info->output_file,
                $id_table
        );
    }

    //Table Preparation.
    $table_info_plugin = array(array('Property', 'Value'));
    $table_info_plugin[] = array(array('value' => 'Author(s)', 'type' => 'TEXT'), array('value' => htmlspecialchars($info->authors), 'type' => 'TEXT'));
    $table_info_plugin[] = array(array('value' => 'Build number', 'type' => 'TEXT'), array('value' => $info->build_number, 'type' => 'TEXT'));

    $alias_plugin = new AliasPlugin();
    $name_res = $alias_plugin->get_data('get_data_alias', array('uid'=>$info->category));

    $alias_category = @$name_res->alias;
    $line = $info->category;
    if ($alias_category)
    {
        $line = $alias_category;
    }
    $table_info_plugin[] = array(array('value' => 'Category', 'type' => 'TEXT'), array('value' => htmlspecialchars($line), 'type' => 'TEXT'));
    $table_info_plugin[] = array(array('value' => 'Description', 'type' => 'TEXT'), array('value' => htmlspecialchars($info->description), 'type' => 'TEXT'));

    ?>
        <div id="full-view-property" class="full-view-widget full-view-widget-right">
            <div class="block">
    <?php
        cts_table_create($id_table, $table_info_plugin);
        $id_table += 1;
    ?>
            </div>
        </div>
    <?php
    // Input file
    if (@$info->input_file)
    {
        ?>
            <div>
                <h2>Input default file</h2>
        <?php
            $id_table = echo_command_table(
                    $info->input_file,
                    $id_table
            );
    }

    ?>
        </div>
        <br/>
    <?php

    return $id_table+1;
}

?>
