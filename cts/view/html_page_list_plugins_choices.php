<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['PLUGINS'].'ListPlugin.php');


/**
* @brief Displays the plugins list using the given parameters
* @param params: the array of parameters
* @param id_table: the next free table index
* @return the next free table index.
*/
function list_plugins_choices_display($name, $type_default='all', $additional_attr)
{
    $list_plugin = new ListPlugin();
    $params = array("type" => "descriptive");
    $cmd_result = $list_plugin->get_data('plugins', $params);
    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }
    echo "<select name='$name' id='$name' $additional_attr>";
    $def = "";
    if ($type_default == 'all')
        $def = "selected";
    echo "<option $def value='all'>ALL</option>";
 
    $list = array();
    //Looping through plugin-generated data
    foreach($cmd_result as $uid_plugin => $plugin_data)
       $list[$uid_plugin] = $plugin_data;
    
    usort($list, "cmp_plugin");
    
    foreach($list as $uid_plugin => $plugin_data)
    {
        $result = $plugin_data->alias;
        $def = "";
        if ($type_default == $result)
            $def = "selected";
        echo "<option $def>$result</option>";
    }

    echo '</select>';
}

function cmp_plugin($a, $b)
{
    if ($a->alias == $b->alias) {
        return 0;
    }
    return ($a->alias < $b->alias) ? -1 : 1;
}


?>
