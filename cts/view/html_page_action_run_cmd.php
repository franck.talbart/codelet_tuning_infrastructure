<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_input.inc.php');
require_once($DIRECTORY['PLUGINS'].'Plugin.php');


/**
 * 
 * @brief Displays the results using the given parameters
 * @param params: plugin and command to call
 */
function action_run_display($params)
{
    $plugin_name = Plugin::get_child($params['plugin']);
    if(!$plugin_name)
    {
        echo 'The given plugin uid is not compatible with CTS: '.$params['plugin'];
        return;
    }
    
    $DIRECTORY = $GLOBALS['DIRECTORY'];
    $plugin_name .= 'Plugin';
    require_once($DIRECTORY['PLUGINS'].$plugin_name.'.php');
    $plugin = new $plugin_name();
    
    // Daemon mode
    if(in_array($params['cmd'], $plugin->daemon_commands))
    {
        $cts_daemon_produced_by = NULL;
        if(@$plugin->daemon_produced_by[$params['cmd']])
            $cts_daemon_produced_by = $plugin->daemon_produced_by[$params['cmd']];

        $doc = array();
        $doc[$params['cmd']]['attributes'] = array
        (
            'name'=>$params['cmd']
        );
        $doc[$params['cmd']]['params'] = $params;
        // Run the plugin
        $param = array('input' => $doc, 'uid' => $plugin);
        $command_data = cts_input_form_parse($param);
        $command_data = cts_input_wrap_daemon($command_data, $plugin, $cts_daemon_produced_by);
        $command = $command_data->command;
        $plugin = new DaemonPlugin();
        @$info = $plugin->get_data($command_data->command, $command_data->input[$command]['params']);
        //Failing gracefully on error.
        if(isset($info->CTI_PLUGIN_CALL_ERROR))
        {
            echo 'Error calling the command '. $command_data->command . ' of plugin '. Plugin::get_child($command_data->plugin_uid) . '.';
            //Returning the error anyway.
            return $info;
        }
    }
    else
    {
        $info = $plugin->get_data($params['cmd'], $params);
    }

    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }
    $result = view_entry($info);
    if ($result !== 1)
        echo $result;
}
//---------------------------------------------------------------------------

?>
