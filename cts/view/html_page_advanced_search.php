<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['PLUGINS'].'UserPlugin.php');
require_once($DIRECTORY['VIEW'].'html_page_list_plugins_choices.php');
require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');

function advanced_search_display()
{
    global $DIRECTORY;
    $user_plugin = new UserPlugin();
    $exit = '';
    if (isset($_GET['exit']))
    {
         $exit = htmlentities($_GET['exit']);
    }
?>
    <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>advanced_search.js" ></script>
    <tr>
        <td>
        Search
        </td>
        <td>
            <input type="text" name="search_query_p" id="search_query_p" value="<?php if (isset($_GET['search_query'])) echo htmlentities($_GET['search_query']); ?>"/>
        </td>
    </tr>
    <tr>
        <td>
            Repository
        </td>
        <td>
        <?php
            cts_form_list_repositories('repository_type_p');
        ?>
        </td>
    </tr>
    <tr>
         <td>Alias</td>
         <td>
         <?php // alias_s since alias already use in the view page ?>
             <input type="text" name="alias_s_p" id="alias_s_p" value="<?php 
                 if (isset($_GET['alias_s'])) echo htmlentities($_GET['alias_s']);
             ?>"/>
         </td>
     </tr>
    <tr>
        <td>
            Type
        </td>
        <td>
        <?php
               $type_default = '';
               if (isset($_GET['produced_by']))
                     $type_default = $_GET['produced_by'];
               list_plugins_choices_display("produced_by_p", $type_default, "onchange=\"update_list_fields($('#produced_by_p').val());\"");
        ?>
        </td>
    </tr>
    <tr>
        <td>
            Date start
        </td>
        <td>
             <input type="text" id="date_start_p" name="date_start_p" value="<?php if (isset($_GET['date_start'])) echo htmlentities($_GET['date_start']); ?>"/>
        </td>
    </tr>
    <tr>
        <td>
            Date end
        </td>
        <td>
            <input type="text" id="date_end_p" name="date_end_p" value="<?php if (isset($_GET['date_end'])) echo htmlentities($_GET['date_end']); ?>">
        </td>
    </tr>
    <tr>
        <td>
           Additional files 
        </td>
        <td>
            <input type="text" name="file_s_p" id="file_s_p" value="<?php if (isset($_GET['file_s'])) echo htmlentities($_GET['file_s']); ?>"/>
        </td>
    </tr>
    <tr>
        <td>
            Tag
        </td>
        <td>
            <input type="text" name="tag_s_p" id="tag_s_p" value="<?php if (isset($_GET['tag_s'])) echo htmlentities($_GET['tag_s']); ?>"/>
        </td>
    </tr>
    <tr>
        <td>
            Note
        </td>
        <td>
            <input type="text" name="note_p" id="note_p" value="<?php if (isset($_GET['note'])) echo htmlentities($_GET['note']); ?>"/>
        </td>
    </tr>
    <tr>
        <td>
            User
        </td>
        <td>
            <input type="text" name="user" id="user" value="<?php if (isset($_GET['user'])) echo htmlentities($_GET['user']); ?>"/>
            <a href="#" onClick="open_popup('<?php echo $user_plugin->uid;?>', 'user', '0') "><small>Get the list</small></a>
        </td>
    </tr>
    <tr>
        <td>
            Exit code
        </td>
        <td>
            <input type="text" name="exit" value="<?php echo $exit;?>"/>
        </td>
    </tr>
    <tr>
        <td>
            Fields
        </td>
        <td>
            <br/>
            Common fields
            <small>
                <br/>
                <br/>
                <input type="checkbox" name="fields[]" value="entry_uid" checked>Entry UID<br/>
                <input type="checkbox" name="fields[]" value="date_time_start">Date start<br/>
                <input type="checkbox" name="fields[]" value="date_time_end">Date end<br/>
                <input type="checkbox" name="fields[]" value="repository">Repository<br/>
                <input type="checkbox" name="fields[]" value="user_uid">User<br/>
                <input type="checkbox" name="fields[]" value="plugin_exit_code">Plugin exit code<br/>
                <input type="checkbox" name="fields[]" value="plugin_uid">Plugin<br/>
                <input type="checkbox" name="fields[]" value="tag">Tag<br/>
                <input type="checkbox" name="fields[]" value="note">Note<br/>
            </small>
            <br/>
            Plugin fields
            <small>
                <br/>
                <span id="list_fields">
                     Please specify the entry type to get more fields.
                </span>
            </small>
       </td>
    </tr>
<?php
}
?>

