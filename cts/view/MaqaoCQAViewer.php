<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit, Florent Hemmi

require_once('Viewer.php');
require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['PLUGINS'].'MaqaoCqaPlugin.php');

class MaqaoCQAViewer extends Viewer
{
    /**
     *
     * @brief Override parent constructor. Initialises all the parameters.
     * @param params: the array of parameters
     * @param id_table: the next free table index
     * @param info: view plugin data
     */
    function __construct($params, $id_table, $info)
    {
        parent::__construct($params, $id_table, $info);
        $maqao_cqa = new MaqaoCqaPlugin();
        $this->uid = $maqao_cqa->uid;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Override. Page functions, for better readability
     * @param file: file
     * @param id_table: the next free table index
     * @param uid_data: data uid
     * @param type_file: file type
     * @return the next free table index
     */
    public function echo_tables($file, $id_table, $uid_data, $type_file)
    {
        foreach ($file as $cmd => $content)
        {
            ?>
                <h2>
                    Plugin command =
                    <i><?php echo htmlspecialchars($cmd);?></i>
            <?php
            
            if (isset($content->attributes->{META_ATTRIBUTE_DESC}))
            {
                ?>
                    ; Description =
                    <i><?php echo $content->attributes->{META_ATTRIBUTE_DESC};?></i>
                <?php
            }
            ?>
                </h2>
            <?php
            
            $contents_common = array();
            $contents_file = array();
            
            foreach($content->params as $index => $param)
            {
                if($param->{META_ATTRIBUTE_TYPE} == META_CONTENT_ATTRIBUTE_TYPE_FILE)
                {
                    $contents_file[] = $param;
                }
                else if($param->{META_ATTRIBUTE_TYPE} == META_CONTENT_ATTRIBUTE_TYPE_MATRIX)
                {
                    $content_matrixes = $param;
                }
                else
                {
                    $contents_common[] = $param;
                }
            }
            
            $matrix_tables = array();
            $matrix_names = array();
            
            if (isset($content_matrixes))
            {
                //Defining default values for the matrix.
                $default_vals_matrix = (object) array(
                    META_ATTRIBUTE_MATRIX_COLUMN_DESCS => array_fill(0,count($content_matrixes->{META_ATTRIBUTE_MATRIX_COLUMN_NAMES}),'No desc.'),
                    META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS => array_fill(0,count($content_matrixes->{META_ATTRIBUTE_MATRIX_COLUMN_NAMES}),'No long desc.'),
                );
                //Changing the matrix data into several acceptable tables of params.
                foreach($content_matrixes->{META_ATTRIBUTE_MATRIX_COLUMN_NAMES} as $index => $cname)
                {
                    $current_params = (object) array(
                        META_ATTRIBUTE_NAME => $cname,
                        META_ATTRIBUTE_LIST => True,
                        META_ATTRIBUTE_DESC => $default_vals_matrix->{META_ATTRIBUTE_MATRIX_COLUMN_DESCS}[$index],
                        META_ATTRIBUTE_LONG_DESC => $default_vals_matrix->{META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS}[$index],
                        META_ATTRIBUTE_TYPE => $content_matrixes->{META_ATTRIBUTE_MATRIX_COLUMN_TYPES}[$index],
                        META_ATTRIBUTE_VALUE => $content_matrixes->{META_ATTRIBUTE_VALUE}->$cname
                    );
                    if (strpos($cname, "x86_64") !== False)
                    {
                        $tmp_table = "x86_64";
                    }
                    elseif (strpos($cname, "k1om") !== False)
                    {
                        $tmp_table = "k1om";
                    }
                    else
                    {
                        $tmp_table = "common";
                    }
                    $matrix_tables[$tmp_table][] = $current_params;
                }
            }
            
            $table_common = $this->generate_table_data($contents_common, $uid_data);
            $common_matrix_table = $this->generate_table_data($matrix_tables["common"], $uid_data);
            $table_files = $this->generate_table_data($contents_file, $uid_data);
            
            ?>
                <div class="full-view-space"></div>
                <div class="block" style="width: 75%;">
                    <?php cts_table_create($this->id_table, $table_common);?>
                </div>
            <?php
            $this->id_table += 1;
            ?>
                <div class="full-view-space"></div>
                <div class="block" style="width: 75%;">
                    <?php cts_table_create($this->id_table, $common_matrix_table); ?>
                </div>
            <?php
            $this->id_table += 1;
            ?>
                <div class="full-view-space"></div>
                <div style="width: 75%;"><button style="position:relative; left:50%" id="toggle_table_other_button">More</button></div>
                
                <div id="table_maqao_cqa_other">
                <?php
                $this->id_table += 1;
                
                //Displaying matrix tables contents
                foreach($matrix_tables as $indice => $matrix_table)
                {
                    if ($indice != "common")
                    {
                        $matrix_table_gen = $this->generate_table_data($matrix_table, $uid_data);
                        ?>
                            <h1 style="border-bottom: 0;">
                                <?php echo $indice; ?>
                            </h1>
                            <div class="block" style="width: 75%;">
                                <?php cts_table_create($this->id_table, $matrix_table_gen);?>
                            </div>
                        <?php
                        $this->id_table += 1;
                    }
                }
                ?>
                </div>
                
                <div class="full-view-space"></div>
                <div class="block" style="width: 75%;">
                    <?php cts_table_create($this->id_table, $table_files);?>
                </div>
                
                <script type="text/javascript">
                   $(document).ready(function() {
                       $('#toggle_table_other_button').click(function () {
                           toggle_object($('div#table_maqao_cqa_other'));
                       });
                       $('div#table_maqao_cqa_other').hide();
                    });
               </script>
                
        <?php
        }
        return $this->id_table;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Override. Print the HTML code for all the page
     */
    public function html()
    {
        $this->script();
        $this->toolbar();
        $this->actions();
        $this->header();
        $this->output_file();
        $this->file_content();
        $this->additional_files();
        $this->note();
        $this->tag();
        $this->information();
    }
}
