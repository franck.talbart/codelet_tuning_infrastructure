<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['VIEW'].'html_page_cts_input_form.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['PLUGINS'].'ApplicationGroupPlugin.php');
require_once($DIRECTORY['PLUGINS'].'CompilePlugin.php');
require_once($DIRECTORY['PLUGINS'].'DecanPlugin.php');
require_once($DIRECTORY['PLUGINS'].'MaqaoCqaPlugin.php');
require_once($DIRECTORY['PLUGINS'].'MaqaoPerfPlugin.php');
require_once($DIRECTORY['PLUGINS'].'PlatformPlugin.php');
require_once($DIRECTORY['PLUGINS'].'IccProfilerPlugin.php');
require_once($DIRECTORY['PLUGINS'].'ViewPlugin.php');
require_once($DIRECTORY['PLUGINS'].'VtunePlugin.php');


/**
 *
 * @brief Displays the add data form using the given parameters
 * @param params: plugin and command to call
 */
function action_cts_input_form_display($params)
{
    $call_params = call_user_func($params['cmd'].'_prepare_params', $params);
    if(!$call_params)
    {
        return;
    }
    $call_params = array_merge($params, $call_params);
    cts_input_form_display($call_params);
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the application compile action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function application_create_compile_entry_prepare_params($params)
{
    $plugin = new CompilePlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'init',
        'default' => array(
            'application' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the binary maqao action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function binary_profile_maqao_perf_prepare_params($params)
{
    $plugin = new MaqaoPerfPlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'init',
        'default' => array(
            'binary' => $params['entry']
        )
    );
    return $result;
}
//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the binary icc action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function binary_profile_icc_prepare_params($params)
{
    $plugin = new IccProfilerPlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'init',
        'default' => array(
            'binary' => $params['entry']
        )
    );
    return $result;
}
//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the binary vtune action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function binary_profile_vtune_prepare_params($params)
{
    $plugin = new VtunePlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'init',
        'default' => array(
            'binary' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the binary decan action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function binary_create_decan_entry_prepare_params($params)
{
    $plugin = new DecanPlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'init',
        'default' => array(
            'original_binary' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the compile run action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function compile_run_prepare_params($params)
{
    // Don't use the generic run function since optional parameters could be changed in CTS (need to display the form)
    $plugin = new CompilePlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'run',
        'default' => array(
            'entry' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the binary maqao action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function loop_group_create_maqao_cqa_entry_prepare_params($params)
{
    $plugin = new MaqaoCqaPlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'init',
        'default' => array(
            'loop_group' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the maqao CQA run action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function maqao_cqa_run_prepare_params($params)
{
    // Don't use the generic run function since optional parameters could be changed in CTS (need to display the form)
    $plugin = new MaqaoCqaPlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'run',
        'default' => array(
            'entry' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the maqao Perf run action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function maqao_perf_run_prepare_params($params)
{
    // Don't use the generic run function since optional parameters could be changed in CTS (need to display the form)
    $plugin = new MaqaoPerfPlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'run',
        'default' => array(
            'entry' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the Icc profiler run action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function icc_profiler_run_prepare_params($params)
{
    // Don't use the generic run function since optional parameters could be changed in CTS (need to display the form)
    $plugin = new IccProfilerPlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'run',
        'default' => array(
            'entry' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------

/**
*
* @brief prepare params for the VTune run action.
* @param params: an array containing the necessary values to prepare the parameters.
*/
function vtune_run_prepare_params($params)
{
    // Don't use the generic run function since optional parameters could be changed in CTS (need to display the form)
    $plugin = new VTunePlugin();
    $result = array(
        'produced_by' => $plugin->uid,
        'command' => 'run',
        'default' => array(
            'entry' => $params['entry']
        )
    );
    return $result;
}

//---------------------------------------------------------------------------
?>
