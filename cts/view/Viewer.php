<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['PLUGINS'].'Plugin.php');
require_once($DIRECTORY['PLUGINS'].'EntryPlugin.php');
require_once($DIRECTORY['PLUGINS'].'NotePlugin.php');
require_once($DIRECTORY['PLUGINS'].'TagPlugin.php');
require_once($DIRECTORY['PLUGINS'].'FilePlugin.php');
require_once($DIRECTORY['VIEW'].'html_page_view_file.php');


/**
 *
 * @brief Mother class for all views
 * This class is defined to create specific views for certain plugins.
 *
 */
class Viewer
{
    var $params;
    var $id_table;
    var $info;

    static $plugin_uids = array(
        'fd498c55-c9ad-4c97-853f-64903c68c433' => 'MaqaoCQA',
        '85224814-81bd-11e3-ab2a-5bb78461e8a9' => 'MaqaoCQAResults',
        'fc709de6-6663-11e3-affc-13361bf1c038' => 'LibCountingResults',
        '2ca1e612-bbed-11e3-8d4a-cbd768cbd88f' => 'Diff',
        '0a939784-61a5-11e3-9d94-0022191169c2' => 'Clustering',
        '06e2e012-5b61-11e3-9170-d3f36bd3c5f9' => 'Loop'
    );
    
    /**
     *
     * @brief Constructor. Initialises all the parameters.
     * @param params: the array of parameters
     * @param id_table: the next free table index
     * @param info: view plugin data
     */
    function __construct($params, $id_table, $info)
    {
        $this->params = $params;
        $this->id_table = $id_table;
        $this->info = $info;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @return the next free table index
     */
    public function get_id_table()
    {
        return $this->id_table;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Generates CTS_Table-compatible data from ctr_input-formatted data.
     * @param content: ctr_input-formatted data
     * @param uid_data: the uid of the entry
     * @param file_side: 'true' if the files should be displayed side by side with the data table, 'false' or leave to default otherwise
     */
    public function generate_table_data($content, $uid_data, $file_side='false')
    {
        $table_ret = array(array());
        
        $first_file = true;
        $num_column = 0;
        foreach ($content as $order => $param)
        {
            if (isset($param->{META_ATTRIBUTE_NAME}))
            {
                $table_ret[0][] = htmlspecialchars($param->name);
            }
            else
            {
                $table_ret[0][] = 'Unnamed';
            }
            
            // Franck: Here, isset is evil since it returns False
            // if value is NULL! AHAHAHAHAHAHAHAH
            if (property_exists($param, META_ATTRIBUTE_VALUE))
            {
                $line = 1;
                //Type
                $type_value = null;
                $password = null;
                
                $type_value = $param->{META_ATTRIBUTE_TYPE};
                $password = @$param->{META_ATTRIBUTE_PASSWORD};
                
                $list = array();
                if (is_array($param->{META_ATTRIBUTE_VALUE}))
                {
                    $list = $param->{META_ATTRIBUTE_VALUE};
                }
                else
                {
                    $list[] = $param->{META_ATTRIBUTE_VALUE};
                }
                
                $cpt_cplx=0;
                foreach ($list as $value)
                {
                    // We do not display encrypted password
                    if ($password)
                        $value = '';
                    $field = array('value' => '', 'type' => 'TEXT');
                    if ($type_value == META_CONTENT_ATTRIBUTE_TYPE_COMPLEX)
                    {
                        $field = array('value' => '<div id="json2html' . $cpt_cplx . '">
                            <script type="text/javascript">
                                    $(document).ready(
                                        function()
                                        {
                                            complex_data_display(\'' . json_encode($value) . '\', \'json2html' . $cpt_cplx . '\');
                                        }
                                    );
                                </script>
                                <noscript><pre>' . print_r($value, true) . '</pre></noscript>
                            </div>', 'type' => 'TEXT');
                        $cpt_cplx++;
                    }
                    else
                    {
                        if ($type_value == META_CONTENT_ATTRIBUTE_TYPE_FILE)
                        {
                            $field = cts_create_visualization_type(
                                META_CONTENT_ATTRIBUTE_TYPE_FILE,
                                $uid_data,
                                $value
                            );
                            
                            if ($first_file)
                            {
                                // The first file is directly displayed.
                                ?>
                                    <script type="text/javascript">
                                        $(document).ready(
                                            function()
                                            {
                                                display_file('page=files&main=view_file&data_uid=<?php echo $uid_data;?>&filename=<?php echo $value;?>', <?php echo $file_side; ?>);
                                            }
                                        );
                                    </script>
                           <?php
                                $first_file = false;
                           }
                           $field = array('value' => add_attributes_to_link($field, 'onclick="display_file(\'page=files&main=view_file&data_uid='.$uid_data.'&filename='.$value.'\','.$file_side.'); return false;"'), 'type' =>'TEXT');
                        }
                        else
                        {
                            // Franck: Thank you PHP for making my life so beautiful
                            if ($value === false)
                            {
                                $value="False"; // Woow
                            }
                            elseif ($value === true)
                            {
                                $value="True"; // Amazing
                            }
                            if (! is_null($value))
                            {
                                $value = htmlspecialchars($value);
                            }
                            
                             $field = array('value' => $value, 'type' => $type_value);
                        }
                    }
                    
                    if (count($table_ret) <= $line)
                    {
                        $table_ret[] = array();
                        foreach ($content as $undefined)
                        {
                            $table_ret[$line][] = array('value' => '', 'type' => 'TEXT');
                        }
                    }
                    $table_ret[$line][$num_column] = $field;
                    $line += 1;
                }
            }
            else
            {
                $table_ret[] = array();
                while(count($table_ret[1]) <= $num_column)
                {
                    $table_ret[1][] = array('value' => '', 'type' => 'TEXT');
                }
            }
            
            $num_column += 1;
        }
        
        return $table_ret;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Page functions, for better readability
     * @param file: file
     * @param id_table: the next free table index
     * @param uid_data: data uid
     * @param type_file: file type
     * @return the next free table index
     */
    public function echo_tables($file, $id_table, $uid_data, $type_file)
    {
        foreach ($file as $cmd => $content)
        {
            ?>
                <h2>Plugin command = <i><?php echo htmlspecialchars($cmd);?></i>
            <?php
            if (isset($content->attributes->{META_ATTRIBUTE_DESC}))
            {
                ?>
                    ; Description =
                    <i>
                        <?php echo $content->attributes->{META_ATTRIBUTE_DESC};?>
                    </i>
                <?php
            }
            ?>
                </h2>
            <?php
            
            //Extracting MATRIX data
            $matrixes = array();
            foreach($content->params as $indice => $param_contents)
            {
                if($param_contents->{META_ATTRIBUTE_TYPE} == META_CONTENT_ATTRIBUTE_TYPE_MATRIX)
                {
                    $matrixes[] = $param_contents;
                    //Removing it so it is not processed in the common table.
                    unset($content->params[$indice]);
                }
            }
            
            $matrix_tables = array();
            
            foreach($matrixes as $current_matrix)
            {
                $current_params = array();
                
                //Changing the matrix data into an acceptable table of params.
                foreach($current_matrix->{META_ATTRIBUTE_MATRIX_COLUMN_NAMES} as $index => $cname)
                {
                    $current_params[] = (object) array(
                        META_ATTRIBUTE_NAME => $cname,
                        META_ATTRIBUTE_LIST => True,
                        META_ATTRIBUTE_DESC => $current_matrix->{META_ATTRIBUTE_MATRIX_COLUMN_DESCS}[$index],
                        META_ATTRIBUTE_LONG_DESC => $current_matrix->{META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS}[$index],
                        META_ATTRIBUTE_TYPE => $current_matrix->{META_ATTRIBUTE_MATRIX_COLUMN_TYPES}[$index],
                        META_ATTRIBUTE_VALUE => $current_matrix->{META_ATTRIBUTE_VALUE}->$cname
                    );
                }
                $matrix_tables[$current_matrix->{META_ATTRIBUTE_NAME}] = $this->generate_table_data($current_params, $uid_data);
            }
            
            //Displaying common table contents
            // Don't display the table if the array is empty;
            // Otherwhise we get a "no data" message when displaying a CSV file (one MATRIX parameter only)
            if (count($content->params) != 0)
            {
                $table_common = $this->generate_table_data($content->params, $uid_data);
                
           ?>
                <div class="block" style="width: 75%;">
                    <?php cts_table_create($this->id_table, $table_common);?>
                </div>
            <?php
               $this->id_table += 1;
            }
            
            //Displaying matrix tables contents
            foreach($matrix_tables as $indice => $matrix_table)
            {
                ?>
                    <h1 style="border-bottom: 0;">
                        <?php echo $indice; ?>
                    </h1>
                    <div class="block" style="width: 75%;">
                        <?php cts_table_create($this->id_table, $matrix_table);?>
                    </div>
                <?php
                $this->id_table += 1;
            }
        }
        
        //Additional script for complex data
        ?>
            <script type="text/javascript">
                $(document).ready(
                    function()
                    {
                        $("img[id^='hideable_json']").each(
                            function(index, elt)
                            {
                                hideChild(elt);
                            }
                        );
                    }
                );
            </script>
        <?php
        return $this->id_table;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the Javascript
     */
    public function script()
    {
        global $DIRECTORY;
        ?>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>ajax/full_view.js"></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>ajax/view_file.js"></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>toggle_table.js"></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>json2html/BubbleTooltips.js"></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>json2html/parse.js"></script>
        <?php
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the toolbar
     */
    public function toolbar()
    {
        $entry_plugin = new EntryPlugin();
        
        $uid_archive = null;
        if (isset($this->info->input_default->archive))
        {
            $uid_archive = $this->info->plugin_uid;
        }
        else
        {
            //Display the link only if the files directory exists
            $path_dl = cts_make_path(array($this->info->repository_path, 'files'));
            if (is_dir($path_dl))
            {
                if ($point = opendir($path_dl)) 
                {
                    while (($file = readdir($point)) !== false)
                        if ($file!="." && $file!=".." ) $uid_archive = $entry_plugin->uid;
                    closedir($point);
                }
            }
        }
        
        $alias_dl = cts_create_uid_visualization($this->info->UID, CTR_ENTRY_DATA);
        $alias_dl = str_replace(' ', '_', $alias_dl);
        
        //Used when the entry is deleted.
        //When it is deleted, the user is redirected to the data list corresponding
        //to the entry repository.
        $repository_id = $this->info->repository_type;
        if ($repository_id != CTR_REP_COMMON && $repository_id != CTR_REP_TEMP)
        {
            $repository_id = $this->info->repository;
        }
        
        if (!isset($_GET['page']))
            $_GET['page'] = 'repositories_summary';
        
        ?>
            <div id="toolbar"<?php if ($uid_archive) echo ' style="width: 540px;"';?>>
                <ul>
                    <li>
                        <a href="download.php?plugin=<?php echo $entry_plugin->uid;?>&uid=<?php echo $this->info->UID;?>&name=<?php echo $alias_dl;?>.csv&cmd=export">
                            <img border="0" src="img/download.png"/>
                            <br/>Export into CSV
                        </a>
                    </li>
        <?php
        if ($uid_archive)
        {
            ?>
                <li>
                    <a href="download.php?plugin=<?php echo $uid_archive;?>&uid=<?php echo $this->info->UID;?>&name=<?php echo $alias_dl;?>.zip&cmd=archive">
                        <img border="0" src="img/download.png"/>
                        <br/>Download
                    </a>
                </li>
            <?php
        }
        ?>
                    <li class="separator"></li>
                    <li>
                        <a onclick="load_main_frame('page=repositories_summary&main=cts_input_form&mode=update&entry=<?php echo $this->info->UID;?>&produced_by=<?php echo $this->info->plugin_uid;?>&command=update'); return false">
                            <img border="0" src="img/add.png"/>
                            <br/>Update data
                        </a>
                    </li>
                    <li>
                        <a onclick="display_form('.change_alias'); return false;"><img src="img/edit.png"/><br/>Change alias</a>
                    </li>
                    <li>
                        <a onclick="display_form('.move_repository'); return false;"><img src="img/move.png"/><br/>Move to repository</a>
                    </li>
                    <li>
                        <a href="?page=<?php echo $_GET['page'];?>&main=delete&type=entry&entry=<?php echo $this->info->UID;?>" onclick="if (confirm('Are you sure?')) load_main_frame(this, 'page=repositories_summary&main=query&search_query=*&repository_type=<?php echo $repository_id;?>'); return false;">
                            <img border="0" src="img/delete.png"/>
                            <br/>Delete data
                        </a>
                    </li>
                </ul>
            </div>
        <?php
        
        ?>
            <div class="block full-view change_alias" style="margin: 5px 0 0 0; width: 250px;">
                <?php cts_form_change_alias($this->info->UID, CTR_ENTRY_DATA);?>
            </div>
            <div class="block full-view move_repository" style="margin: 5px 0 0 0; width: 250px;">
                <table><?php cts_form_move_to_repository($this->info->UID, $this->info->repository_type);?></table>
            </div>
        <?php
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the CTS actions link
     */
    public function actions()
    {
        $first = true;
        $produced_by_name = Plugin::get_child($this->info->plugin_uid);
        if($produced_by_name)
        {
            $produced_by_name .= 'Plugin';
            $DIRECTORY = $GLOBALS['DIRECTORY'];
            require_once($DIRECTORY['PLUGINS'] . $produced_by_name . '.php');
            $produced_by_plugin = new $produced_by_name();
            foreach($produced_by_plugin->get_actions() as $cmd_name => $cmd_params)
            {
                if ($first)
                {
                ?>
                <div id="full-view-command" class="full-view-widget full-view-widget-right">
                    <div class="block">
                        <ul>
                <?php
                    $first = false;
                }
                ?>
                            <li>
                                <a href="<?php echo $cmd_params['url'];?>&entry=<?php echo $this->info->UID;?>&plugin=<?php echo $produced_by_plugin->uid;?>&cmd=<?php echo $cmd_name;?>" onclick="load_main_frame(this); return false;">
                                    <?php echo $cmd_params['name'];?>
                                </a>
                            </li>
                <?php
            }
            
            if (!$first)
            {
            ?>
                        </ul>
                    </div>
                </div>
            <?php
            }
        }
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the header (title and repository)
     */
    public function header()
    {
        ?>
            <h1 style="border-bottom: 0;">
                <?php
                    echo htmlspecialchars(
                            cts_create_uid_visualization($this->info->UID, CTR_ENTRY_DATA)
                        );
                ?>
            </h1>
            <h2>Repository =
                <?php
                    echo cts_create_visualization_type(
                            META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID,
                            $this->info->repository,
                            $this->info->repository_type
                        );
                ?>
            </h2>
        <?php
    }

    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the output file
     */
    public function output_file()
    {
        //Output file
        if (isset($this->info->output_default))
        {
            $this->id_table = $this->echo_tables(
                $this->info->output_file,
                $this->id_table,
                $this->info->UID,
                'output'
            );
        }
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the file content (empty div refresh by Javascript)
     */
    public function file_content()
    {
        ?>
            <div id="file-content"></div>
        <?php
    }
    
    //---------------------------------------------------------------------------
    
     /**
     *
     * @brief Print the HTML code for the note plugin
     */
    public function note()
    {
        $note_plugin = new NotePlugin();
        ?>
            <div>
                <h2>Notes</h2>
                <div class="block" style="width: 75%;">
                    <table>
                        <?php
                            $this->info_note = $note_plugin->get_data('get', array('entry' => $this->info->UID));
                            $count = 0;
                            
                            foreach($this->info_note as $note)
                            {
                            ?>
                                <tr>
                                    <td>
                                        <?php echo $note->date;?>, <?php echo $note->username;?>: <?php echo $note->txt;?>
                                    </td>
                                    <td>
                                        <form method="POST" action="?page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID;?>" onsubmit="load_main_frame('page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID;?>','',false,'POST', 'note_cmd=rm&entry=<?php echo $this->info->UID;?>&note_number=<?php echo $count;?>');return false;">
                                            <input type="hidden" name="note_cmd" value="rm"/>
                                            <input type="hidden" name="entry" value="<?php echo $this->info->UID;?>"/>
                                            <input type="hidden" name="note_number" value="<?php echo $count;?>"/>
                                            <input style="border:none" src="img/close_button.png" type="image"/>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                                $count ++;
                              }
                            ?>
                                <tr>
                                    <td>
                                    <form method="POST" action="?page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID;?>" onsubmit="load_main_frame('page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID;?>','',false,'POST', 'note_cmd=add&entry=<?php echo $this->info->UID;?>&note='+$('input[name=note]').val());return false;">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="note_cmd" value="add"/>
                                                        <input type="hidden" name="entry" value="<?php echo $this->info->UID;?>"/>
                                                        <input type="text" name="note"/>
                                                    </td>
                                                    <td>
                                                        <input type="submit" value="Add note"/>
                                                    </td>
                                                </tr>
                                            </table>
                                    </form>
                                   </td>
                                   <td></td>
                                </tr>
                    </table>
                </div>
            </div>
        <?php
    }
    
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the additional files
     */
    public function additional_files()
    {
        $file_plugin = new FilePlugin();
        $entry_plugin = new EntryPlugin();
        
        ?>
            <div>
                <h2>Import files</h2>
                <div class="block" style="width: 75%;">
                    <table>
                        <?php
                            $this->additional_files = $file_plugin->get_data('get_additional_file', array('entry' => $this->info->UID));
                            $count = 0;
                            foreach($this->additional_files as $additional_file)
                            {
                                ?>
                                <tr>
                                    <td>
                                        <a href="download.php?plugin=<?php echo $entry_plugin->uid;?>&uid=<?php echo $this->info->UID;?>&name=<?php echo $additional_file;?>.zip&filename=<?php echo $additional_file;?>&cmd=archive_file" target="_blank"><?php echo $additional_file; ?></a>
                                    </td>
                                    <td>
                                        <form method="POST" action="?page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID; ?>" onsubmit="load_main_frame('page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID;?>', '', false, 'POST', 'additional_file_cmd=rm_additional_file&entry=<?php echo $this->info->UID;?>&file_number=<?php echo $count;?>'); return false;">
                                            <input type="hidden" name="additional_file_cmd" value="rm_additional_file"/>
                                            <input type="hidden" name="entry" value="<?php echo $this->info->UID;?>"/>
                                            <input type="hidden" name="file_number" value="<?php echo $count;?>"/>
                                            <input style="border:none" src="img/close_button.png" type="image"/>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                                $count ++;
                            }
                        ?>
                        <tr>
                            <td>
                            <form method="POST" action="?page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID; ?>" enctype="multipart/form-data">
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="additional_file_cmd" value="add_additional_file"/>
                                                <input type="hidden" name="entry" value="<?php echo $this->info->UID;?>"/>
                                                <input type="file" name="file"/>
                                            </td>
                                            <td>
                                                <input type="submit" value="Add a file"/>
                                            </td>
                                        </tr>
                                    </table>
                              </form>
                            </td>
                            <td></td>
                        </tr>
                </table>
                </div>
            </div>
        <?php
    }
    
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the tag plugin
     */
    public function tag()
    {
        $tag_plugin = new TagPlugin();
        
        ?>
            <div>
                <h2>Tags</h2>
                <div class="block" style="width: 75%;">
                    <i><small>Click on a tag to access to the corresponding entries.</small></i>
                    <table>
                        <?php
                            $this->info_tag = $tag_plugin->get_data('get', array('entry' => $this->info->UID));
                            $count = 0;
                            
                            foreach($this->info_tag as $tag)
                            {
                        ?>
                                <tr>
                                    <td><a href="?page=repositories_summary&main=query&repository_type=all&produced_by=all&tag_s=<?php echo $tag;?>&search_query=" onclick="load_main_frame('page=repositories_summary&main=query&repository_type=all&produced_by=all&tag_s=<?php echo $tag;?>&search_query='); return false"><?php echo $tag;?></a></td>
                                    <td>
                                    <form method="POST" action="?page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID; ?>" onsubmit="load_main_frame('page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID; ?>','', false, 'POST', 'tag_cmd=rm&entry=<?php echo $this->info->UID;?>&tag_number=<?php echo $count;?>');return false;">
                                            <input type="hidden" name="tag_cmd" value="rm"/>
                                            <input type="hidden" name="entry" value="<?php echo $this->info->UID;?>"/>
                                            <input type="hidden" name="tag_number" value="<?php echo $count;?>"/>
                                            <input style="border:none" src="img/close_button.png" type="image"/>
                                        </form>
                                    </td>
                                </tr>
                        <?php
                                $count ++;
                            }
                        ?>
                                <tr>
                                    <td>
                                    <form method="POST" action="?page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID; ?>" onsubmit="load_main_frame('page=repositories_summary&main=view_data&data_uid=<?php echo $this->info->UID; ?>','',false, 'POST', 'tag_cmd=add&entry=<?php echo $this->info->UID;?>&tag='+$('input[name=tag]').val());return false;">
                                            <table border="0">
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="tag_cmd" value="add"/>
                                                        <input type="hidden" name="entry" value="<?php echo $this->info->UID;?>"/>
                                                        <input type="text" name="tag"/>
                                                    </td>
                                                    <td>
                                                        <input type="submit" value="Add tag"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>
                                    <td></td>
                                </tr>
                        </table>
                    </div>
                </div>
        <?php
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for the information
     */
    public function information()
    {
        ?>
            <div id="full-view-property" class="full-view-widget full-view-widget-right">
                <div class="block">
        <?php
        
        $table_info_data = array();
        $table_info_data[] = array('Property', 'Value');
        $table_info_data[] = array(
            array('value' => 'Plugin', 'type' => 'TEXT'),
            array('value' => cts_create_visualization_type(
                META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID,
                $this->info->plugin_uid
            ), 'type' => 'TEXT')
        );
        $table_info_data[] = array(
            array('value' => 'Exit code', 'type' => 'TEXT'),
            array('value' => htmlspecialchars($this->info->exit_code), 'type' => 'TEXT')
        );
        $table_info_data[] = array(
            array('value' => 'Start date', 'type' => 'TEXT'),
            array('value' => htmlspecialchars($this->info->date_start), 'type' => 'TEXT')
        );
        $table_info_data[] = array(
            array('value' => 'End date', 'type' => 'TEXT'),
            array('value' => htmlspecialchars($this->info->date_end), 'type' => 'TEXT')
        );
        $table_info_data[] = array(
            array('value' => 'Author', 'type' => 'TEXT'),
            array('value' => cts_create_visualization_type(
                    META_CONTENT_ATTRIBUTE_TYPE_DATA_UID,
                    $this->info->user_uid
            ), 'type' => 'TEXT')
        );
        
        cts_table_create(
            $this->id_table,
            $table_info_data
        );
        
        $this->id_table = $this->id_table + 1;
        ?>
                </div>
            </div>
        <?php
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Print the HTML code for all the page
     */
    public function html()
    {
        $this->script();
        $this->toolbar();
        $this->actions();
        $this->header();
        $this->output_file();
        $this->file_content();
        $this->additional_files();
        $this->note();
        $this->tag();
        $this->information();
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Returns the variable part of the viewer class name corresponding to the uid
     * @param uid: the uid of the plugin.
     */
    public static function get_child($uid)
    {
        if(array_key_exists($uid, Viewer::$plugin_uids))
        {
            return Viewer::$plugin_uids[$uid];
        }
        return null;
    }
}
