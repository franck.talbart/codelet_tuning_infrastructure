<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['PLUGINS'].'UpdatePlugin.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');


/**
 * 
 * @brief Displays the update daemon command using the given parameters
 * @param params: the array of parameters
 */
function update_daemon_display($params)
{
    global $DIRECTORY;
    
    //Update confirmation
    if (!isset($params['run']))
    {
        //Asking the admin to confirm update.
        ?>
            <div class="block">
                <div align="center">
                    <img border="0" src="img/warning.png" width="100"/>
                    <br/>
                    <strong>WARNING: you are going to update CTI. <br/>You should not lost any of your work but please be sure to backup your repositories before doing this.
                    </strong><br/><br/>
                    <a href="?page=update&run=1">&gt; Update CTI to the last stable release &lt;</a><br/><br/>
                </div>
                <br/>
            </div>
        <?php 
    }
    else
    {
        //Run the update.
        $update_plugin = new UpdatePlugin();
        $info = $update_plugin->get_data('daemon');
        
        if(@$info->CTI_PLUGIN_CALL_ERROR)
        {
            //Abort contents loading.
            return;
        }
        
        if (isset($info->entry))
        {
            ?>
                <div class="block">
                    <div align="center">
                        <div id="update_status"></div><br/>
                        <textarea cols="90" name="txt" id="txt" rows="35" disabled>
                        </textarea>
                    </div>
                    <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>ajax/view.js"></script>
                    <script type="text/javascript">get_update_status('<?php echo $info->entry;?>', 1000);</script>
                    <br/>
                </div>
            <?php 
        }
        else
        {
            echo "Entry can't be created. UID is None." . $info->output;
        }
    }
}
?>