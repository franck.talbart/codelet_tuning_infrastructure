<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['PLUGINS'].'MovePlugin.php');

/**
 * 
 * @brief Displays the move information using the given parameters
 * @param params: the array of parameters
 */
function html_page_move_display($params)
{
    $command = 'local';
    if($params['repository'] == CTR_REP_COMMON)
    {
        $command = 'common';
    }
    else if ($params['repository'] == CTR_REP_TEMP)
    {
        $command = 'temp';
    }
    
    $move_plugin = new MovePlugin();
    $info = $move_plugin->get_data($command, $params);
    
    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }
    
    ?>
        <div align="center">
            Entry moved to the <?php echo $command; ?> repository.
            <br/><br/><br/>
    <?php 
    
    foreach(@$info->moved as $uid)
    {
        echo cts_create_visualization_type(
            $info->type,
            $uid
        );
        
        ?>
             - Moved<br/> 
        <?php 
    }
    
    ?><br/><br/><?php
    
    foreach(@$info->skipped as $uid)
    {
        echo cts_create_visualization_type(
            $info->type, 
            $uid
        );
        
        ?> 
            - Skipped (This data/plugin already exists in this repository).<br/>
        <?php 
    }
    
    ?>
        </div>
    <?php 
}

?>
