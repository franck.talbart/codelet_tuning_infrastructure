<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************i***************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['INCLUDE'].'query.inc.php');
require_once($DIRECTORY['PLUGINS'].'QueryPlugin.php');

/**
*
* @brief displays the full page for query results
* @param params: the array of parameters
* @param id_table: the next free table index
* @return the next free table index.
*/
function query_display($params, $id_table)
{
    
    if (isset($params['search_query']))
    {
        $table_params = $params;
        $table_params['id_table'] = $id_table;
        
        $added_columns = array();
        
        if (isset($table_params['fields']))
        {
            #Adding additional entry UID column if needed, to enable the delete buttons
            if(!in_array('entry_info.entry_uid', $table_params['fields']) || !in_array('entry_uid', $table_params['fields']))
            {
                $added_columns[] = 'entry_info.entry_uid';
                $table_params['fields'][] = 'entry_info.entry_uid';
            }
            #If there is already an entry_uid and an alias is present, turn the alias column into an additional column 
            else if (in_array('entry_info.alias', $table_params['fields']))
            {
                $added_columns[] = 'entry_info.alias';
                array_splice($table_params['fields'], array_search('entry_info.alias', $table_params['fields']), 1);
                $table_params['fields'][] = 'entry_info.alias';
            }
            else if (in_array('alias', $table_params['fields']))
            {
                $added_columns[] = 'entry_info.alias';
                array_splice($table_params['fields'], array_search('alias', $table_params['fields']), 1);
                $table_params['fields'][] = 'entry_info.alias';
            }
        }
        else
        {
            #If the default columns are used, do not display the alias column.
            $added_columns[] = 'entry_info.alias';
        }
        
        $query_plugin = new QueryPlugin();
        
        $info = $query_plugin->get_data('select', prepare_query($table_params));
        if(@$info->CTI_PLUGIN_CALL_ERROR)
        {
            //Abort contents loading.
            return;
        }
        
        foreach($added_columns as $index => $col)
        {
            array_splice($info->header, array_search($col, $info->fields), 1);
        }
        
        $url_query = http_build_query($params);
        ?>
            <h1>List of data</h1>
            <div class="block">
                <?php query_display_table_only($info, $id_table, False, $added_columns); ?>
                <div style="position:absolute; right:1em;">
                    <div align="center">
                    <a href="javascript:save_query('<?php echo $url_query; ?>')">
                            <img src="img/save.png" border="0"/>
                            <br/>
                            <small>
                                <small>
                                    Save query
                                </small>
                            </small>
                        </a>
                    </div>
                </div>
                <br/> 
                <div align="center">
                    <small>
                        <small>
                            Number of results: <?php echo $info->total;?> Time: <?php echo number_format($info->time, 3);?> s
                            <br/><br/>
                        </small>
                    </small>
                </div>
            </div>
       <?php 
    }
    else
    {
        ?>
            <div>The query is empty!</div>
        <?php 
    }
    return $id_table+1;
}
//---------------------------------------------------------------------------

/**
*
* @brief Displays the table itself from the query
* @param info: json result from query converted to object
* @param id_table: the next free table index
* @param widget_mode: true to indicate that the table is displayed in a widget
*/
function query_display_table_only($info, $id_table, $widget_mode=False, $ignore_columns=array())
{
    $table_contents = array(@$info->header);
    
    $plugins_visualization = array();
    $aliases = False;
    if(in_array('entry_info.alias', $ignore_columns))
    {
        $aliases = True;
    }
    
    if (@$info->data)
    {
        foreach ($info->data as $line)
        {
            $delete = cts_form_delete_button($line->{'entry_info.entry_uid'}, 'entry');
            $to_add = array();
            
            foreach (@$info->fields as $attr)
            {
                if(in_array($attr, $ignore_columns))
                {
                    continue;
                }
                
                if($attr === 'entry_info.plugin_uid' || $attr === 'plugin_uid')
                {
                    //Optimizing speed by storing the plugins visualizations.
                    if(!isset($plugins_visualization[$line->$attr]))
                    {
                        $plugins_visualization[$line->$attr] =
                        cts_create_visualization_type(
                                META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID,
                                $line->$attr
                        );
                    }
                    $value = $plugins_visualization[$line->$attr];
                }
                else if ($aliases && ($attr === 'entry_info.entry_uid' || $attr === 'entry_uid'))
                {
                    $value = cts_create_visualization_type(
                                META_CONTENT_ATTRIBUTE_TYPE_DATA_UID,
                                $line->$attr,
                                $line->{'entry_info.alias'}
                        );
                }
                else
                {
                    $value = $line->$attr;
                     
                    if (cts_is_UID($value))
                    {
                        $value = cts_create_visualization_type(
                                META_CONTENT_ATTRIBUTE_TYPE_DATA_UID,
                                $value
                        );
                    }
                    else if (is_array($value))
                    {
                        $tab = $value;
                        $value = '';
                        foreach ($tab as $element)
                        {
                            if (cts_is_UID($element))
                            {
                                $element = cts_create_visualization_type(
                                        META_CONTENT_ATTRIBUTE_TYPE_DATA_UID,
                                        $element
                                );
                            }
                            
                            $value.= ' '.$element;
                        }
                    }
                }
                
                $to_add[] = array('value' => $value, 'type' => 'TEXT');
            }
            
            $to_add[] = array('value' => $delete, 'type' => 'TEXT');
            $table_contents[] = $to_add;
        }
    }
    
    $config_table = array(
        'size' => @$info->config_table->size,
        'sorted' => @$info->config_table->sorted,
        'current' => @$info->config_table->current,
        'pages' =>(int) ( ceil( @$info->total / @$info->config_table->size) ),
        'total' => @$info->total,
    );
    
    cts_table_create(
        $id_table,
        $table_contents,
        DEFAULT_TABLE_SIZE,
        $config_table,
        $widget_mode
    );
}

?>
