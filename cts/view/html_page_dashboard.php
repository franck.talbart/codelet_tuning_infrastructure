<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Florent Hemmi, Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['VIEW'].'Dashboard.php');

function dashboard_display()
{
    if (!isset($_SESSION['dashboard']))
    {
        $dashboard = new Dashboard($_SESSION['login_uid']);
        $_SESSION['dashboard'] = serialize($dashboard);
    }
    else
    {
        $dashboard = unserialize($_SESSION['dashboard']);
    }
    
    echo $dashboard->html();
    
    
    $_SESSION['dashboard'] = serialize($dashboard);
}
?>
