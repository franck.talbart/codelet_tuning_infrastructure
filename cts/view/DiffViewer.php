<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once('Viewer.php');
require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['PLUGINS'].'DiffPlugin.php');

class DiffViewer extends Viewer
{
    static $loop_plugin_uid = "06e2e012-5b61-11e3-9170-d3f36bd3c5f9";
    
    /**
     *
     * @brief Override parent constructor. Initialises all the parameters.
     * @param params: the array of parameters
     * @param id_table: the next free table index
     * @param info: view plugin data
     */
    function __construct($params, $id_table, $info)
    {
        parent::__construct($params, $id_table, $info);
        $diff = new DiffPlugin();
        $this->uid = $diff->uid;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Override. Page functions, for better readability
     * @param file: file
     * @param id_table: the next free table index
     * @param uid_data: data uid
     * @param type_file: file type
     * @return the next free table index
     */
    public function echo_tables($file, $id_table, $uid_data, $type_file)
    {
        foreach ($file as $cmd => $content)
        {
            ?>
                <h2>
                    Plugin command =
                    <i><?php echo htmlspecialchars($cmd);?></i>
            <?php
            
            if (isset($content->attributes->{META_ATTRIBUTE_DESC}))
            {
                ?>
                    ; Description =
                    <i><?php echo $content->attributes->{META_ATTRIBUTE_DESC};?></i>
                <?php
            }
            ?>
                </h2>
            <?php
            
            $contents_common = $content->params;
            
            // get the entry type and indexes of percentage parameters
            $type_query = "none";
            $perc_nb_entries = array();
            $perc_coverage_entries = array();
            foreach($contents_common as $index => $param)
            {
                $param_name = $param->{META_ATTRIBUTE_NAME};
                if($param_name == "type_query")
                {
                    $type_query = $param->{META_ATTRIBUTE_VALUE};
                }
                else if($param_name == "perc_nb_entries_A_strict_match" ||
                        $param_name == "perc_nb_entries_B_strict_match" ||
                        $param_name == "perc_nb_entries_A_not_match" ||
                        $param_name == "perc_nb_entries_B_not_match")
                {
                    $perc_nb_entries[] = $index;
                }
                else if($param_name == "perc_coverage_A_strict_match" ||
                        $param_name == "perc_coverage_B_strict_match" ||
                        $param_name == "perc_coverage_A_not_match" ||
                        $param_name == "perc_coverage_B_not_match")
                {
                    $perc_coverage_entries[] = $index;
                }
            }
            // delete percentage depending on the entry type
            if($type_query == DiffViewer::$loop_plugin_uid)
            {
                foreach($perc_nb_entries as $index)
                {
                    unset($contents_common[$index]);
                }
            }
            else
            {
                foreach($perc_coverage_entries as $index)
                {
                    unset($contents_common[$index]);
                }
            }
            
            $table_common = $this->generate_table_data($contents_common, $uid_data);
            
            ?>
                <div class="full-view-space"></div>
                <div class="block" style="width: 75%;">
                    <?php cts_table_create($this->id_table, $table_common);?>
                </div>
            <?php
            $this->id_table += 1;
        }
        return $this->id_table;
    }
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Override. Print the HTML code for all the page
     */
    public function html()
    {
        $this->script();
        $this->toolbar();
        $this->actions();
        $this->header();
        $this->output_file();
        $this->file_content();
        $this->additional_files();
        $this->note();
        $this->tag();
        $this->information();
    }
}
