<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['PLUGINS'].'ListPlugin.php');
require_once($DIRECTORY['PLUGINS'].'AliasPlugin.php');
require_once($DIRECTORY['PLUGINS'].'RepositoryPlugin.php');



/**
*
* @brief Displays the local repositories list using the given parameters
* @param params: the array of parameters
* @param id_table: the next free table index
* @return the next free table index.
*/
function list_local_repositories_display_contents($param, $id_table)
{
    global $DIRECTORY;
    //Case of page refresh using the alias update button.
    if(array_key_exists('alias', $_POST))
    {
        $alias_plugin = new AliasPlugin();
        $alias_plugin->get_data('repository', $_POST);
    }

    $list_plugin = new ListPlugin();
    $cmd_result = $list_plugin->get_data('local_repositories', $param);
    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }

    //Setting header
    $table_list_ctr = array(array('Alias', 'Path', 'Last use', 'Browser', '', ''));

    //Looping through plugin-genrated data
    foreach ($cmd_result as $ctr)
    {
        $delete = cts_form_delete_button($ctr->uid, 'repository');

        //Compute the actual alias if it is set.
        $alias_r = $ctr->alias;
        if ($alias_r == 'null')
        {
            $alias_r = '';
        }

        //Alias display required extra attention, as there is an update button
        $alias = '<form method="POST" action="?' . $_SERVER['QUERY_STRING'] . '">'.
            '<input type="hidden" name="uid" value="' . $ctr->uid . '"/>' .
            '<input type="text" name="alias" value="'  . htmlspecialchars($alias_r) . '"/>'.
            '<input type="submit" value="Update"/>'.
            '</form>';

        //Creating and addign the row to the table.
        if (!$ctr->last_use)
        {
            $ctr->last_use = '<font color="red" >Repository entry not found</font>';
        }
        $repository_plugin = new RepositoryPlugin();

        $plugins_button = '<a href="?page=repositories_summary&main=list_plugin&repository_type='. $ctr->uid .
                                  '" onclick="load_main_frame(this); return false">' .
                              '<button>Plugins</button>'.
                          '</a>';
        $data_button = '<a href="?page=repositories_summary&main=query&repository_type='. $ctr->uid . '&search_query=*"'.
                               ' onclick="load_main_frame(this); return false">' .
                           '<button>Data</button>'.
                       '</a>';
        $download_button = '<a target="_blank" href="download.php?plugin=' . $repository_plugin->uid . '&uid=' . $ctr->uid. '">' .
                               '<img border="0" src="'. $DIRECTORY['IMG'] . 'download_button.png" title="Download" alt="Download"/>'.
                           '</a>';

        $contents_buttons = '<table><tr>' .
                                '<td>'.$plugins_button.'</td>' .
                                '<td>'.$data_button.'</td>' .
                                '<td>'.$download_button.'</td>' .
                            '</tr></table>';

        $table_list_ctr[] = array(
            array('value' => $alias, 'type' => 'TEXT'),
            array('value' => $ctr->path, 'type' => 'TEXT'),
            array('value' => $ctr->last_use, 'type' => 'TEXT'),
            array('value' => $contents_buttons, 'type' => 'TEXT'),
            array('value' => $delete, 'type' => 'TEXT')
        );

    }

    ?>
        <div class="block">
    <?php

    //Now all the table data is ready, call the cts table creator function.
    cts_table_create($id_table, $table_list_ctr);

    ?>
            <br/>
        </div>
    <?php

    return $id_table+1;
}
//---------------------------------------------------------------------------

/**
*
* @brief Displays the local repositories list as a standalone page using the given parameters
* @param params: the array of parameters
* @return the next free table index.
*/
function list_local_repositories_display($params)
{
    ?>
        <h1>Local Repositories</h1>
    <?php

    list_local_repositories_display_contents($params, 0);

}

?>
