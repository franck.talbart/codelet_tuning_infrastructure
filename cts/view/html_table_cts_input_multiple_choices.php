<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_input.inc.php');
require_once($DIRECTORY['PLUGINS'].'AliasPlugin.php');


/**
*
* @brief Displays the plugins multiple choices using the given parameters
* @param params: the array of parameters
* @param id_table: the next free table index
* @return the next free table index.
*/
function cts_input_multiple_choices_display($params, $id_table)
{
    $info = cts_input_multiple_choices_get_data($params);
    
    $field_name = '';
    $is_multiple = '';
    $produced_by = '';
    if (array_key_exists('field', $params))
    {
        $field_name = $params['field'];
    }
    if (array_key_exists('is_mult', $params))
    {
        $is_multiple = $params['is_mult'];
    }
    if (array_key_exists('produced_by', $params))
    {
        $produced_by = $params['produced_by'];
    }
    
    ?>
        <strong>Produced by: "<?php echo $info->plugin_id;?>"</strong><br/>
    <?php 
    
    $table_list_plugin = array(array("Entry"));
    
    foreach(@$info->array_uid as $uid)
    {
        if (cts_is_UID($uid))
        {
            $alias = cts_create_uid_visualization(
                    $uid,
                    CTR_ENTRY_DATA
                );
            
            $line = "<a href=\"#\" onClick=\"" .
                    "window.opener.document.getElementById('" . 
                    $field_name;
            if ($is_multiple == '0')
            {
                $line .= "').value='" . $alias;
            }
            else
            {
                $line .= "').value+='" . $alias . "\\n";
            }
            $line .= "' ; window.opener.document.getElementsByClassName('checkbox_" . $field_name . "')[0].checked=false ";
            $line .= ";self.close();\">" .
                  $alias . "</a>";
                
            $table_list_plugin[] = array(array('value' => $line, 'type' => 'TEXT'));
        }
    }
    ?>
        <a href="index.php?main=cts_input_form&mode=add&command=init&produced_by=<?php echo $produced_by;?>" target="_blank">
            Add an entry
        </a>
    <?php 
    
    cts_table_create(
        $id_table,
        $table_list_plugin
    );
    
    return $id_table+1;
}
//---------------------------------------------------------------------------

/**
*
* @brief Displays the plugins multiple choices using the given parameters
* @param params: the array of parameters
*/
function cts_input_multiple_choices_popup_display($params)
{
    global $DIRECTORY;
    global $CFG;
    ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <head>
            <link href="<?php echo $DIRECTORY['THEMES'].$CFG['theme'];?>/theme.css" media="screen" rel="stylesheet" type="text/css" />
            <link href="<?php echo $DIRECTORY['THEMES'].$CFG['theme'];?>/colorPicker.css" media="screen" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>jquery-1.5.min.js" ></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>jquery.history.js" ></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>jquery-ui-1.8.17.custom.min.js" ></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>jquery-ui-timepicker-addon.js" ></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>cts_input.js" ></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>general.js" ></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>search_box.js" ></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>left_menu.js" ></script>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS'];?>ajax/load_frame.js" ></script>
        </head>
        <body>
            <?php 
            cts_input_multiple_choices_display($params, 0);
            ?>
        </body>
    </html>
    <?php 
}

?>
