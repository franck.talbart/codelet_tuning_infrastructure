<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['PLUGINS'].'ListPlugin.php');


/**
*
* @brief Displays the plugins list using the given parameters
* @param params: the array of parameters
* @param id_table: the next free table index
* @return the next free table index.
*/
function list_plugins_display($params, $id_table)
{
    $list_plugin = new ListPlugin();
    
    $cmd_result = $list_plugin->get_data('plugins', $params);
    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }
    
    //Setting header
    $table_list_ctr = array(array('Plugin','Category','Description','Data','Delete'));
    
    //Looping through plugin-genrated data
    foreach($cmd_result as $uid_plugin => $plugin_data)
    {
        $alias = htmlspecialchars($plugin_data->category_alias);
        $desc = htmlspecialchars($plugin_data->desc);
        
        $plugin = cts_create_visualization_type(
            META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID,
            $uid_plugin,
            $plugin_data->alias
        );
        
        $add_data = cts_form_add_button($uid_plugin, 'init');
        $delete = cts_form_delete_button($uid_plugin, 'entry');
        
        $table_list_ctr[] = array(
            array('value' => $plugin, 'type' => 'TEXT'),
            array('value' => cts_create_visualization_type(
                META_CONTENT_ATTRIBUTE_TYPE_TEXT,
                $plugin_data->category_uid,
                $alias
            ), 'type' => 'TEXT'),
            array('value' => $desc, 'type' => 'TEXT'),
            array('value' => $add_data, 'type' => 'TEXT'),
            array('value' => $delete, 'type' => 'TEXT')
        );
    }
    
    ?>
        <h1>List of plugins</h1>
        <div class="block">
    <?php
    //Now all the table data is ready, call the cts table creator function.
    cts_table_create($id_table, $table_list_ctr);
    ?>
            <br/>
        </div>
    <?php 
    
    return $id_table+1;
}
?>
