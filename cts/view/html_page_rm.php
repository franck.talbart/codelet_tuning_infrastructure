<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['PLUGINS'].'RmPlugin.php');


/**
 * 
 * @brief Displays the remove information using the given parameters
 * @param params: the array of parameters
 */
function html_page_rm_display($params)
{
    
    $remove_plugin = new RmPlugin();
    $info = $remove_plugin->get_data($params['type'], $params);
    
    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }
    ?>
        <div align="center">
    <?php 
    foreach(@$info->output as $line)
    {
        echo $line;
        ?>
            <br/>
        <?php 
    }
    ?>
        </div>
    <?php 
}

?>
