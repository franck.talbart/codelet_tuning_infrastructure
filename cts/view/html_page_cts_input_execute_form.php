<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'plugins.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_table.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_form.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_input.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['VIEW'].'html_page_view_data.php');
require_once($DIRECTORY['VIEW'].'html_page_view_plugin.php');


/**
 *
 * @brief Displays the view of a created entry using the given parameters
 * @param params: the array of parameters
 */
function cts_input_execute_form_display($params)
{
    $get = $params[0];
    $post = $params[1];

    global $DIRECTORY;

    $AVOID_PARAMETER = array('repository_type', 'added_params', 'process_mode');

    // none is set
    if (isset($post['none']))
        foreach($post['none'] as $none)
            $post[$none] = NULL;
    // Delete a single file
    if (isset($post['delete']))
        foreach($post['delete'] as $delete)
            $post[$delete] = NULL;
    // Delete a set of files from a list
    if (isset($post['delete_files']))
    {
        foreach($post['delete_files'] as $delete_file)
        {
            $delete_file = explode("______", $delete_file);
            if (array_key_exists($delete_file[1], $post))
            {
                $post[$delete_file[1]] = $post[$delete_file[1]]."\r\n".'-'.$delete_file[0]; 
            }
            else
            {
                $post[$delete_file[1]] = '-'.$delete_file[0]; 
            }
        }
    }

    $command_name = $get['command'];
    $plugin = $get['plugin'];
    $repository_type = @$post['repository_type'];

    $doc = array();
    $doc[$command_name]['attributes'] = array
        (
            'repository'=>$repository_type,
            'name'=>$command_name
        );

    $added_params = unserialize(urldecode(@$post['added_params']));

    while (list($key, $value) = each($post))
    {
        if (!(in_array($key, $AVOID_PARAMETER)))
        {
            // List
            if (@$added_params[$key])
            {
                if ($value == '')
                {
                    $value = array();
                }
                else
                {
                    $value = explode("\r\n", trim($value, "\r\n"));
                }
            }

            $doc[$command_name]['params'][$key] = $value;
        }
    }

    // Files
    while (list($key, $value) = each($_FILES))
    {
        if (file_exists($value['tmp_name']))
        {
            rename($value['tmp_name'], '/tmp/'.$value['name']);
            $key_array = explode("/", $key);
            $tmp_file_value = '/tmp/'.$value['name'];
            // List
            if (@$added_params[$key])
                $tmp_file_value = array($tmp_file_value);
            
            if (isset($doc[$command_name]['params'][$key_array[0]]))
                if (is_array($doc[$command_name]['params'][$key_array[0]]))
                    $tmp_file_value = array_merge($doc[$command_name]['params'][$key_array[0]], $tmp_file_value);

            $doc[$command_name]['params'][$key_array[0]] = $tmp_file_value;
        }
    }

    // Run the plugin
    $param = array('input' => $doc, 'uid' => $plugin);
    $command_data = cts_input_form_parse($param);

    if(isset($post['daemon_mode']) || isset($post['auto_run']))
        $command_data->cts_daemon_mode = true;
    
    if(isset($post['process_mode']))
        $command_data->cts_process_mode = unserialize(urldecode($post['process_mode']));

    $info = cts_input_launch_command($command_data);
    $result = view_entry($info);
    if ($result !== 1)
        echo $result;
}

?>
