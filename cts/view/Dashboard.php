<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['VIEW'].'Widget.php');
require_once($DIRECTORY['VIEW'].'html_page_advanced_search.php');
require_once($DIRECTORY['INCLUDE'].'Profil.php');

/**
 *
 * @brief This class manages the dashboard and all the widgets.
 */
class Dashboard
{
    private $login_uid; // User UID
    private $widgets = array(); // Widgets array

    private $profil;
    private static $dashboard_fname = 'dashboard'; // Dashboard filename
    private static $default_dashboard_fname = 'dashboard_default';

    /**
     *
     * @brief Constructor. Initialize the dashboard.
     * Create the user data directory if not created with chmod 775 and
     * load the dashboard file.
     * If not found, a default dashboard is loaded.
     * @param login_uid: user UID used to read the right directory
     */
    function __construct($login_uid)
    {
        $this->login_uid = $login_uid;

        $this->load();

        // Loading default widget if there is no widgets.
        if (empty($this->widgets))
        {
            $this->load(true);
            $this->save();
        }
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Add a widget to the dashboard and save it.
     * @param params: the widget parameters (generally $_GET or $_POST)
     * @return the new widget ID (useful to get the widget HTML code)
     */
    public function add_widget($params)
    {
        $id = 0;
        if (count($this->widgets) > 0)
        {
            end($this->widgets);
            $id = key($this->widgets)+1;
            reset($this->widgets);
        }

        $w = new Widget($id);
        $w->set($params);

        $this->widgets[$id] = $w;
        $this->save();

        return $id;
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Delete a widget from the dashboard and save it.
     * @param id: the ID of the widget
     * @return true if the dashboard has been saved correctly, false otherwise
     */
    public function delete_widget($id)
    {
        unset($this->widgets[$id]);
        return $this->save();
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Load the dashboard and fill the widget array.
     * @param default_widget: set to true to load the default widget
     * @return true if the dashboard has been loaded, false otherwise
     */
    public function load($default_widget=false)
    {
        global $DIRECTORY;

        if ($default_widget)
        {
            $fname = self::$dashboard_fname;
        }
        else
        {
            $fname = self::$default_dashboard_fname;
        }

        $this->profil = new Profil($this->login_uid, $fname);

        $handle = $profil->load_filename();
        if (!$handle) return false;

        $size = filesize($profil->get_filename());
        $data = fread($handle, $size);
        fclose($handle);

        $data = object_to_array(json_decode($data));

        foreach ($data as $id => $cdata)
        {
            $cid = $cdata['id'];

            $w = new Widget($cid);
            $w->set($cdata);

            $this->widgets[$cid] = $w;
        }
        return true;
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Save the dashboard.
     * The dashboard use the JSON format to store the widgets.
     * @return true if the dashboard has been saved, false otherwise
     */
    public function save()
    {
        $fname = self::$dashboard_fname;
        $profil = $this->profil;
        $handle = $profil->load_filename('w');
        if (!$handle) return false;
        $json = json_encode($this->get_widgets_to_array());
        fwrite($handle, $json);
        fclose($handle);
        return true;
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Generate the HTML code for the dashboard display.
     * This function calls the HTML method of all the widgets.
     */
    public function html()
    {
        ?>
            <div id="dashboard">
                <?php
                     foreach ($this->widgets as $id => $w)
                    {
                        $info = $w->execute_query();
                        if (Widget::$types[$w->get_type()] == 'chart')
                        {
                            $w->add_data($info->total);
                        }
                        $w->html();
                    }
                ?>
            </div>
        <?php
        $this->save();
    }
    //---------------------------------------------------------------------------

    // Setters
    /**
     *
     * @brief Save the widget in the widgets array.
     * This method is useful for updating a widget.
     * @param id: the widget ID
     * @param widget: the new widget
     */
    public function set_widget($id, $widget)
    {
        $this->widgets[$id] = $widget;
    }
    //---------------------------------------------------------------------------

    // Getters
    /**
     *
     * @brief Convert the widgets array to a "standard" array
     * Useful for storing the widget in JSON format.
     * @return the array
     */
    private function get_widgets_to_array()
    {
        $widgets = array();
        foreach ($this->widgets as $id => $w)
            $widgets[] = $w->to_array();
        return $widgets;
    }

    //---------------------------------------------------------------------------

    /**
     *
     * @return all the widgets in the array
     */
    public function get_widgets()
    {
        return $this->widgets;
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @param id: the widget ID
     * @return the widget corresponding or null if not found
     */
    public function get_widget($id)
    {
        $w = null;
        if (isset($this->widgets[$id]))
            $w = $this->widgets[$id];
        return $w;
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Create the HTML code needed for the create widget form.
     * This function is using the advanced search form.
     */
    public function form_create_widget()
    {
        global $DIRECTORY;
        ?>
            <script type="text/javascript" src="<?php echo $DIRECTORY['JS']; ?>ajax/advanced_search.js"></script>

            <div id="form_create_widget">
                <h1>Add a widget</h1>
                <div id="block">
                    <form method="POST" action="ajax/dashboard.php">
                        <input type="hidden" name="action" value="add" />
                        <input type="hidden" name="dashboard" value="0" />

                        <table>
                            <tr>
                                <td>Widget title</td>
                                <td><input type="text" name="title" /></td>
                            </tr>
                            <tr>
                                <td>Widget type</td>
                                <td>
                                    <input type="radio" name="type" value="0" checked="checked" />Text<br />
                                    <input type="radio" name="type" value="1" />Chart<br />
                                    <input type="radio" name="type" value="2" />Number
                                </td>
                            </tr>
                            <tr class="chart_options">
                                <td colspan="2" style="height: 20px;"></td>
                            </tr>
                            <tr class="chart_options">
                                <td>Time before next record</td>
                                <td>
                                    <select name="time_limit_o">
                                        <option value="0">Immediately</option>
                                        <option value="5">5 seconds</option>
                                        <option value="<?php echo 60*60; ?>">1 hour</option>
                                        <option value="<?php echo 24*3600; ?>">1 day</option>
                                        <option value="<?php echo 24*7*3600; ?>">1 week</option>
                                        <option value="<?php echo 30*24*7*3600; ?>">1 month</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="chart_options">
                                <td>Number of data to record</td>
                                <td>
                                    <select name="record_limit_o">
                                        <?php
                                        for ($i = 10; $i <= 100; $i += 10)
                                        {
                                            ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr class="text_options">
                                <td colspan="2" style="height: 20px;"></td>
                            </tr>
                            <tr class="text_options">
                                <td>Search since</td>
                                <td>
                                    <input type="radio" name="search_since_o" value="begin" checked="checked" />Begin<br />
                                    <input type="radio" name="search_since_o" value="end" />End
                                </td>
                            </tr>
                            <tr class="text_options">
                                <td>Number of results</td>
                                <td><input type="text" name="number_results_o" value="0" /></td>
                            </tr>
                            <tr class="text_options">
                                <td>Column to sort</td>
                                <td><input type="text" name="n_column_sort_o" value="0" /></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 20px;"></td>
                            </tr>
                            <?php advanced_search_display(); ?>
                        </table>
                        <br />
                        <a href="?page=dashboard" onclick="cts_dashboard.show(); return false">
                            <input type="button" value="Cancel" />
                        </a>
                        <input type="submit" value="Add new widget" />
                    </form>
                </div>
            </div>
        <?php
    }
}
?>
