<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Florent Hemmi, Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['PLUGINS'].'QueryPlugin.php');
require_once($DIRECTORY['VIEW'].'html_table_query.php');
require_once($DIRECTORY['INCLUDE'].'query.inc.php');

/**
 * 
 * @brief Widget class which contains all the widget parameters.
 */
class Widget
{
    private $id;
    private $title;
    private $top, $left;
    private $width, $height;
    private $type;
    private $color;
    private $query;
    private $data = array();
    private $params = array();
    
    private $info = null;
    
    public static $types = array('text', 'chart', 'number');
    
    
    /**
     * 
     * @brief Initialize a widget
     * @param $id the ID
     * @param $title the title
     * @param $top the distance from the top of the main frame
     * @param $left the distance from the left of the main frame
     * @param $type the widget type (text, chart or number)
     * @param $color the widget color (chart and number)
     * @param $query the query (an array which correspond usually to the $_GET array)
     */
    function __construct($id=0, $title='', $top=0, $left=0, $width=400, $height=200, $type=0, $color='#000000', $query=array())
    {
        $this->id = $id;
        $this->title = $title;
        $this->top = $top;
        $this->left = $left;
        $this->width = $width;
        $this->height = $height;
        $this->type = $type;
        $this->color = $color;
        $this->query = $query;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @brief This function save the data to the current widget.
     * The data is only stored depending on the last query time.
     * If the number of maximum data is reached, the first data
     * is deleted, the array is re-organised and the new data
     * is inserted at the end.
     * This function is useful only for generating chart.
     * @param $data the data to add
     * @return true if the data has been added, false otherwise
     */
    public function add_data($data)
    {
        $internal_params = $this->data;
        $time = time();
        
        if (isset($internal_params['query_time']))
        {
            if ($time - $internal_params['query_time'] < $this->params['time_limit'])
                return false;
        }
        $internal_params['query_time'] = $time;
        
        $data_to_add = array(
            'time' => $time,
            'value' => $data
        );
        if (count($internal_params) > 0 && count($internal_params)-1 >= $this->params['record_limit'])
        {
            $internal_params_time = $internal_params['query_time'];
            unset($internal_params['query_time']);
            unset($internal_params[0]);
            $internal_params = array_values($internal_params);
            $internal_params[$this->params['record_limit']-1] = $data_to_add;
            $internal_params['query_time'] = $internal_params_time;
        }
        else
        {
            $internal_params[] = $data_to_add;
        }
        
        $this->data = $internal_params;
        return true;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @brief Execute the query with the query plugin.
     * @return the array by query plugin or null if the query fail
     */
    public function execute_query()
    {
        $query_data = null;
        $query_plugin = new QueryPlugin();
        
        $params = $this->query;
        $params['id_table'] = $this->id;
        
        if (self::$types[$this->type] == 'text' && $this->params['search_since'] == 'begin' && $this->params['number_results'] != 0)
        {
            $params['size_'.$this->id] = $this->params['number_results'];
        }
        else 
        {
            $params['size_'.$this->id] = -1;
        }
        $params['sorted_'.$this->id] = $this->params['n_column_sort'];
        
        $info = $query_plugin->get_data('select', prepare_query($params));
        if(!@$info->CTI_PLUGIN_CALL_ERROR)
            $query_data = $info;
        
        $this->info = $query_data;
        return $query_data;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @brief Parse the result of the query and
     * provide a different display depending on the widget type
     */
    private function parse_result($info)
    {
        $info->config_table->sorted = $this->params['n_column_sort'];
        
        if (self::$types[$this->type] == 'text')
        {
            if ($info->total > 1 && $this->params['number_results'] != 0 && $this->params['search_since'] == 'end')
            {
                $data = array();
                for ($i = ($info->total-1); $i > ($info->total - $this->params['number_results'] -1) && $i >= 0; $i--)
                {
                    $data[] = $info->data[$i];
                }
                
                $info->data = $data;
            }
            if($info->total > $this->params['number_results'])
            {
                $info->total = $this->params['number_results'];
            }
        }
        
        switch(self::$types[$this->type])
        {
            case 'text':
                query_display_table_only($info, $this->id, true);
                break;
            
            case 'chart':
                ?>
                    <img border="0" alt="Chart <?php echo $this->id; ?>" src="ajax/dashboard.php?action=view&amp;widget=<?php echo $this->id; ?>" />
                <?php
                break;
            
            case 'number':
                ?>
                    <h2 style="color: <?php echo $this->color; ?>;"><?php echo $info->total; ?></h2>
                <?php
                break;
        }
    }
    //---------------------------------------------------------------------------
    
    public function html()
    {
        if ($this->info == null)
        {
            $info = $this->execute_query();
            if (self::$types[$this->get_type()] == 'chart')
                $this->add_data($info->total);
        }
        ?>
        <div id="widget_<?php echo $this->id; ?>" class="widget">
            <div class="close">
                <form method="post" action="ajax/dashboard.php" onsubmit="cts_dashboard.delete_widget(this); return false;">
                    <input type="hidden" name="dashboard" value="0" />
                    <input type="hidden" name="action" value="delete" />
                    <input type="hidden" name="widget" value="<?php echo $this->id; ?>" />
                    <input type="image" src="img/close_button.png" />
                </form>
            </div>
            <div class="title"><?php echo $this->title; ?></div>
                <?php
                if (self::$types[$this->type] != 'text')
                {
                    ?>
                        <div class="color">
                            <form>
                                <input name="color" type="text" value="<?php echo $this->color; ?>" />
                            </form>
                        </div>
                    <?php
                }
                ?>
                <div class="content">
                <?php $this->parse_result($this->info); ?>
                </div>
            </div>
        <?php
    }
    //---------------------------------------------------------------------------
    
    // Setters
    /**
     * 
     * @param params the array with the private attributes to replace with
     */
    public function set($params)
    {
        foreach ($params as $key => $value)
        {
            if (!isset($this->$key))
                continue;
            
            $this->$key = $value;
        }
    }
    //---------------------------------------------------------------------------
    
    // Getters
    /**
     * 
     * @brief Convert the widget to an array
     * @return the converted array
     */
    public function to_array()
    {
        $result = array();
        foreach ($this as $key => $value)
        {
            if ($key != 'info')
                $result[$key] = $value;
        }
        return $result;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return the widget id
     */
    public function get_id()
    {
        return $this->id;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return the widget title
     */
    public function get_title()
    {
        return $this->title;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return an array with the widget position (top and left)
     */
    public function get_positions()
    {
        return array(
            'top' => $this->top,
            'left' => $this->left
        );
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return an array with the widget size (width and height)
     */
    public function get_size()
    {
        return array(
            'width' => $this->width,
            'height' => $this->height
        );
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return the widget type
     */
    public function get_type()
    {
        return $this->type;
    }
    
    public function get_color()
    {
        return $this->color;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return the widget query (array)
     */
    public function get_query()
    {
        return $this->query;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @brief Use this function to get the data.
     * It delete the query time value and return just the data.
     * Useful to get the data for generating the chart.
     * @return the widget data
     */
    public function get_data()
    {
        $data = $this->data;
        unset($data['query_time']);
        return $data;
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return the widget parameters (array)
     */
    public function get_params()
    {
        return $this->params;
    }
}
?>
