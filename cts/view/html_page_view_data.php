<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['PLUGINS'].'Plugin.php');
require_once($DIRECTORY['VIEW'].'Viewer.php');


/**
 * 
 * @brief Displays the data view using the given parameters
 * @param params: the array of parameters
 * @param id_table: the next free table index
 * @return the next free table index.
 */
function view_data_display($params, $id_table)
{
    global $DIRECTORY;

    $view_plugin = new ViewPlugin();
    $info = $view_plugin->get_data('data', $params);
    
    if(@$info->CTI_PLUGIN_CALL_ERROR)
    {
        //Abort contents loading.
        return;
    }
    //Call the Viewer class.
    $viewer = Viewer::get_child(@$info->plugin_uid);
    if ($viewer)
    {
        require_once($DIRECTORY['VIEW'].$viewer.'Viewer.php');
    }
    $viewer .= 'Viewer';
    $viewer = new $viewer($params, $id_table, $info);
    
    $viewer->html();
    ?>
        <div class="full-view-space"></div>
    <?php 
    
    return $viewer->get_id_table();
}
?>
