<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'Profil.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');

/**
 *
 * @brief This class manages the SavedQueries
 */
class SavedQueries
{
    private static $saved_queries_fname = 'saved_queries';
    private $profil;
    private $queries = array(); // queries array

    /**
     *
     * @brief Constructor. Initialize the savedqueries.
     * @param login_uid: user UID used to read the right directory
     */
    function __construct($login_uid)
    {
        $this->profil = new Profil($login_uid, self::$saved_queries_fname);
        $this->load();
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Add a query to the saved queries and save it.
     * @param $label: the label of the query
     * @param $query: the full URL query
     * @return the new query ID
     */
    public function add_saved_queries($label, $query)
    {
        $id = 0;
        if (count($this->queries) > 0)
        {
            end($this->queries);
            $id = key($this->queries)+1;
            reset($this->queries);
        }

        $this->queries[$id] = array("label" => $label, "query" => $query);
        $this->save();

        return $id;
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Delete a query from the saved queries and save it.
     * @param id: the ID of the query
     * @return true if the query has been saved correctly, false otherwise
     */
    public function delete_query($id)
    {
        unset($this->queries[$id]);
        return $this->save();
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Load the queries and fill the queries array.
     * @return true if the queries have been loaded, false otherwise
     */
    private function load()
    {
        $profil = $this->profil;
        $handle = $profil->load_filename();
        if (!$handle) return false;

        $size = filesize($profil->get_filename());
        if ($size > 0)
        {
            $data = fread($handle, $size);
            fclose($handle);

            $data = object_to_array(json_decode($data));

            foreach ($data as $id => $cdata)
                $this->queries[$id] = $cdata;
        }

        return true;
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Save the queries
     * The savedqueries use the JSON format to store the queries.
     * @return true if the queries have been saved, false otherwise
     */
    private function save()
    {
        $profil = $this->profil;
        $handle = $profil->load_filename('w');
        if (!$handle) return false;
        $json = json_encode($this->queries);
        fwrite($handle, $json);
        fclose($handle);
        return true;
    }
    //---------------------------------------------------------------------------

    /**
     *
     * @brief Generate the HTML code for the savedqueries display.
     * This function calls the HTML method of all the queries.
     */
    public function html()
    {
        $this->load();
        global $DIRECTORY;
        ?>
            <div class="saved_queries">
                <table>
                    <thead>
                        <th>
                            <div align="center"><strong>Saved Queries</strong></div>
                        </th>
                    </thead>
                </table>
                <table class="content">
                <?php
                    foreach ($this->queries as $id=>$query)
                    {
                ?>
                    <tr>
                        <td>
                            <a href="?<?php echo $query['query']; ?>" onclick='load_main_frame(this); return false;'><?php echo $query['label']; ?></a>
                        </td>
                        <td width="10%">
                        <a href="" onclick="if (confirm('Are you sure?')) delete_query(<?php echo $id; ?>); return false;">
                                <img border="0" src="<?php echo $DIRECTORY['IMG']; ?>delete_button.png" title="Delete" alt="Delete"/>
                            </a>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </table>
            </div>
        <?php
    }
}
?>
