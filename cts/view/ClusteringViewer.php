<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once('Viewer.php');
require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'DataSource.php');
require_once($DIRECTORY['PLUGINS'].'ClusteringPlugin.php');
require_once($DIRECTORY['PLUGINS'].'ViewPlugin.php');

class ClusteringViewer extends Viewer
{
    /**
     *
     * @brief Override parent constructor. Initialises all the parameters.
     * @param params: the array of parameters
     * @param id_table: the next free table index
     * @param info: view plugin data
     */
    function __construct($params, $id_table, $info)
    {
        parent::__construct($params, $id_table, $info);
        $clustering = new ClusteringPlugin();
        $this->uid = $clustering->uid;
    }
    
    //---------------------------------------------------------------------------
    
    public function button_bar($current_tab)
    {
        $url_tab = '?page=repositories_summary&main=view_data&data_uid='.$this->info->UID;
        ?>
            <ul id="tabs">
                <li <?php if ($current_tab=='output') echo 'class="active"';?>><?php echo cts_create_link(' Output ', $url_tab, False, 'onclick="load_main_frame(this); return false;"');?></li>
                <li <?php if ($current_tab=='clusters') echo 'class="active"';?>><?php echo cts_create_link(' Clusters ', "$url_tab&current_tab=clusters", False, 'onclick="load_main_frame(this); return false;"');?></li>
            </ul>
        <?php
    }
    
    //---------------------------------------------------------------------------
    
    public function output()
    {
        $this->output_file();
        $this->file_content();
        $this->additional_files();
        $this->note();
        $this->tag();
    }
    
    //---------------------------------------------------------------------------
    
    private function load_file($filename)
    {
        $view_plugin = new ViewPlugin();
        $info = $view_plugin->get_data('file', array('filename' => $filename, 'data_uid' => $this->info->UID));
        $csv_parse = new File_CSV_DataSource;
        $tmpfname = tempnam("/tmp", "FOO");
        $handle = fopen($tmpfname, "w");
        fwrite($handle, $info->content);
        fclose($handle);
        $csv_parse->load($tmpfname);
        unlink($tmpfname);
        return $csv_parse;
    }
    
    //---------------------------------------------------------------------------
    
    public function clusters()
    {
        //Displaying clustering results is pertinent only if the experience has been run.
        //If not, we print a warning message instead of the tables.
        $view_plugin = new ViewPlugin();
        $clustering_results = $view_plugin->get_data('data', array('data_uid' => $this->info->UID));
        $results = array();
        foreach($clustering_results->output_file->init->params as $param)
        {
            if($param->{META_ATTRIBUTE_NAME} == 'result')
            {
                $results = $param->{META_ATTRIBUTE_VALUE};
            }
        }
        
        //If the experience has not been run.
        if(sizeof($results) == 0)
        {
            ?>
                <p>To see the results of this clustering, please run it first using CTS or typing in a terminal:
                <strong>cti clustering run <?php echo $this->info->UID ?></strong>.</p>
            <?php
        }

        //The experience has been run and we can display the tables.
        else
        {
        
            ?>
            
                <!-- show/shide buttons scripts. -->
                <script type="text/javascript">
                    var clustering_toggle_loops = function(cluster_name)
                    {
                        $('tr[id='+cluster_name+']').toggle(100);
                        button_name = $('#button_'+cluster_name).html();
                        if (button_name == 'show')
                        {
                            button_name = 'hide';
                        }
                        else
                        {
                            button_name = 'show';
                        }
                        $('#button_'+cluster_name).html(button_name);
                        
                    };
                    
                    $(document).ready (function()
                    {
                        $('tr[id*=cluster_]').hide();
                    });
                   
                    // Shows or hides all clusters.
                    var clustering_toggle_all_loops = function()
                    {
                        var tr_tags = document.getElementsByTagName('tr');
                        var cluster_names = [];
                        var i;
                        var tag;

                        // Retrieving all cluster names.
                        for (i = 0; i < tr_tags.length; i++)
                        {
                            tag = tr_tags[i];

                            // If the tag refers to a cluster...
                            if (tag.id && (tag.id.indexOf('cluster_') == 0)
                                && (cluster_names.indexOf(tag.id) == -1))
                            {
                                // ... we store the cluster name.
                                cluster_names.push(tag.id);
                            }
                        }

                        // For each cluster name, toggling the tables.
                        for(i = 0; i < cluster_names.length; i++)
                        {
                            $('tr[id='+cluster_names[i]+']').toggle(100);
                        }
                        
                        button_name = $('#button_all_clusters').html();
                        if (button_name == 'show all')
                        {
                            button_name = 'hide all';
                        }
                        else
                        {
                            button_name = 'show all';
                        }
                        $('#button_all_clusters').html(button_name);
                        
                    };
                    
                    $(document).ready (function()
                    {
                        $('tr[id*=cluster_]').hide();
                    });
                </script>

                <table class="cts_table">
                    <thead>
                        <tr><td colspan=6>Legend</td></tr>
                    </thead>
                    <tr>
                        <td class="cts_clustering_color_lowest">Between 100% and 67% of negative deviation</td>
                        <td class="cts_clustering_color_lower">Between 67% and 33% of negative deviation</td>
                        <td class="cts_clustering_color_low">Between 33% and 0% of negative deviation</td>
                        <td class="cts_clustering_color_high">Between 0% and 33% of positive deviation</td>
                        <td class="cts_clustering_color_higher">Between 33% and 67% of positive deviation</td>
                        <td class="cts_clustering_color_highest">Between 67% and 100% of positive deviation</td>
                    </tr>
                    <tr><td colspan=6>Every percent is relative to the maximum positive and negative deviations observed in every codelets.</td></tr>
                </table>
                <br/><br/>
            <?php 
            
            $table_contents = array();
            $table_head = array();
            
            $contents_by_cluster = array();
            
            //Opening the data
            $median_info = $this->load_file('median_cluster.csv');
            $median_global = $this->load_file('median_global.csv');
            $output_info = $this->load_file('OUTPUT-features.csv');
            $clustering_info = $this->load_file('clustering.csv');
            $deviation_info = $this->load_file('deviation.csv');
            $far_percent = 0.33;
            $further_percent = 0.67;

            //The headers of the CSV files read by the clustering viewer are
            //formatted in a specific way, to be compatible with R.
            //This format is quite unaesthetic, that's why we set up a map wherein
            //we associate the files headers to those which sould be displayed in
            //CTS.

            //Getting the headers from the lib_counting_results and maqao_cqa_results plugins.
            $lib_counting_results = $view_plugin->get_data('plugin', array('plugin_uid' => 'lib_counting_results'));
            $maqao_results = $view_plugin->get_data('plugin', array('plugin_uid' => 'maqao_cqa_results'));
            $lib_counting_headers = array();
            $maqao_headers = array();
        
            //Storing lib_counting headers.
            foreach($lib_counting_results->output_file->init->params as $param)
            {
                if($param->{META_ATTRIBUTE_NAME} == 'metrics')
                {
                    //Associating to each CSV file header its corresponding description.
                    foreach(array_combine($param->{META_ATTRIBUTE_MATRIX_COLUMN_NAMES}, $param->{META_ATTRIBUTE_MATRIX_COLUMN_DESCS})
                    as $header_name => $header_desc)
                    {
                        //Formatting the header name to match with the CSV file header.
                        //See the format_header_R method from the clustering plugin.

                        //Replacing all non alphanumeric characters by dots.
                        $header_name = preg_replace('#([^a-zA-Z0-9])#', '.', $header_name);
                        //Uppercasing the first character.
                        $header_name = ucfirst(strtolower($header_name));
                        //If the first character is a dot, add an X to the beginning of the string.
                        if($header_name[0] == '.')
                        {
                            $header_name = 'X' + $header_name;
                        }

                        $lib_counting_headers[$header_name] = $header_desc;
                    }
                }
            }
            
            //Storing maqao headers, same mechanism.
            foreach($maqao_results->output_file->init->params as $param)
            {
                if($param->{META_ATTRIBUTE_NAME} == 'matrix_loop_results')
                {
                    foreach(array_combine($param->{META_ATTRIBUTE_MATRIX_COLUMN_NAMES}, $param->{META_ATTRIBUTE_MATRIX_COLUMN_DESCS})
                    as $header_name => $header_desc)
                    {
                        $header_name = preg_replace('#([^a-zA-Z0-9])#', '.', $header_name);
                        $header_name = ucfirst(strtolower($header_name));
                        if($header_name[0] == '.')
                        {
                            $header_name = 'X' + $header_name;
                        }

                        $maqao_headers[$header_name] = $header_desc;
                    }
                }
            }

            //Getting info on clusters and setting base for info
            foreach($median_info->connect() as $index => $line)
            {
                //Filling the header
                if(count($table_head) == 0)
                {
                    $table_head[] = 'Name';
                    foreach($line as $head => $value)
                    {
                        if($head != 'Coverage MAQAO' && $head != 'Cluster')
                        {
                            $table_head[] = $head;
                        }
                    }
                    $table_head[] = 'Coverage MAQAO';
                }
                
                //Filling the contents
                $current_line = array();
                foreach($table_head as $head)
                {
                    if($head === 'Name')
                    {
                        $current_line[] = 'MEDIAN';
                    }
                    else
                    {
                        $current_line[] = $line[$head];
                    }
                }
                $contents_by_cluster[$line['Cluster']] = array('cluster_id' => $line['Cluster'], 'cluster_info' => $current_line, 'loop_info' => array());
            }
            
            //Setting a loop => cluster map
            $loop_cluster_map = array();
            foreach($clustering_info->connect() as $index => $line)
            {
                $loop_cluster_map[$line['CodeletName']] = $line['Cluster'];
            }

            $max_global = array();
            $min_global = array();

            //Setting up an array containing the uids of all the codelets figuring in the output CSV file.
            //Used to generate the links in the first column of each cluster table.
            $codelets_names = array();
            $codelets_uids = array();
            
            foreach($output_info->connect() as $line)
            {
                $codelets_names[] = $line['CodeletName'];
            }

            //Getting all the uids in one shot for performance reasons.
            $codelets_uids = cts_get_data_uid($codelets_names);

            //Getting loop info
            foreach($output_info->connect() as $index => $line)
            {
                $loop_info = array();
                foreach($table_head as $head)
                {
                    if ($head === 'Name')
                    {
                        $loop_info[] = cts_create_visualization_type(CTR_ENTRY_DATA, $codelets_uids[$index], $alias=$line['CodeletName'], $new_window=True, $append='&current_tab=source');
                    }
                    else if ($head === '-')
                    {
                        $loop_info[] = '-'; 
                    }
                    else
                    {
                        $loop_info[] = $line[$head];
                        if (!array_key_exists($head, $max_global))
                        {
                            $max_global[$head] = $line[$head];
                            $min_global[$head] = $line[$head];
                        }
                        else
                        {
                           if($min_global[$head] > $line[$head])
                           {
                                $min_global[$head] = $line[$head];
                           }
                           if($max_global[$head] < $line[$head])
                           {
                                $max_global[$head] = $line[$head];
                           }
                        }
                    }
                }
                $current_cluster = $loop_cluster_map[$line['CodeletName']];
                $contents_by_cluster[$current_cluster]['loop_info'][$line['CodeletName']] = array('data' => $loop_info);
            }

            $median_global = $median_global->connect();
            $intervals = array();
            $intervals[] = array();
            foreach($median_global[0] as $index => $val)
            {
                $min = $min_global[$index];
                $max = $max_global[$index];
                $med = $val;
                $neg_dev = $med - $min;
                $pos_dev = $max - $med;
                        
                $intervals[] = array(
                       'max' => $max,
                       'higher' => $med + ($pos_dev * $further_percent),
                       'high' => $med + ($pos_dev * $far_percent),
                       'med' => $med,
                       'low' => $med - ($neg_dev * $far_percent),
                       'lower' => $med - ($neg_dev * $further_percent),
                       'min' => $min
                        );
            }

            $intervals[] = array();
     
            //Getting deviation info
            foreach($deviation_info->connect() as $index => $line)
            {
                $loop_dev = array();
                foreach($table_head as $head)
                {
                    if ($head === 'Name' || $head === 'Coverage MAQAO' || $head === '-' || $head === 'Invocations' || $head === 'Cycles')
                    {
                        $loop_dev[] = 'DO NOT DISPLAY';
                    }
                    else
                    {
                        $loop_dev[] = $line[$head];
                    }
                }
                $contents_by_cluster[$loop_cluster_map[$line['CodeletName']]]['loop_info'][$line['CodeletName']]['deviation'] = $loop_dev;
            }

            //echo '<pre>'; print_r($contents_by_cluster); echo '</pre>';

            //Sorting the clusters by their MAQAO coverage, from the most to the least.
            foreach($contents_by_cluster as $cluster_id => $cluster_contents)
            {
                $cluster_info_last_index = sizeof($cluster_contents['cluster_info']) - 1;
                $maqao_coverages[$cluster_id] = $cluster_contents['cluster_info'][$cluster_info_last_index];
            }
            array_multisort($maqao_coverages, SORT_DESC, $contents_by_cluster);
            //echo '<pre>'; print_r($contents_by_cluster); echo '</pre>';
                        
            //Displaying a button to show/hide all tables.
            echo "<p><center><button id=\"button_all_clusters\" onclick=\"clustering_toggle_all_loops()\">show all</button></center></p>";
            
            //Making tables
            foreach($contents_by_cluster as $cluster_contents)
            {
                ?>
                    <table class="cts_table">
                        <thead>
                            <tr>
                <?php
                
                $cluster_id =  $cluster_contents['cluster_id'];

                //Header

                $invocations_column_index = 0;
                $cycles_column_index = 0;
                $coverage_column_index = 0;

                foreach($table_head as $index => $head)
                {
                    $current = $head;
                    if ($head == 'Name')
                    {
                        $current = 'Cluster '.$cluster_id;
                    }
                    else if ($head == 'Invocations')
                    {
                        $invocations_column_index = $index;
                    }
                    else if ($head == 'Cycles')
                    {
                        $cycles_column_index = $index;
                    }
                    else if ($head == 'Coverage MAQAO')
                    {
                        $coverage_column_index = $index;
                    }

                    //Setting the "fancy" headers retrieved earlier insted of using the CSV file headers.
                    if (array_key_exists($current, $lib_counting_headers))
                    {
                        $current = $lib_counting_headers[$current];
                    }
                    else if (array_key_exists($current, $maqao_headers))
                    {
                        $current = $maqao_headers[$current];
                    }
                        
                    ?>
                        <th><?php echo $current ?></th>
                    <?php 
                }
                ?>
                        </tr>
                    </thead>
                <?php 
                
                //Loop contents
               foreach($cluster_contents['loop_info'] as $index => $loop_infos)
                {
                    ?>
                        <tr id="cluster_<?php echo $cluster_id ?>">
                    <?php
                    //Reading the clustering tables is difficult due to the large
                    //amount of data. Moreover, when scrolling, we can't see the
                    //table headers anymore.
                    //Thus, we add a tooltip to each cell which reminds its
                    //corresponding headers to facilitate the navigation.
                    $matches = array();
                    preg_match('#loop[0-9]+#', $loop_infos['data'][0], $matches);
                    $loop_id = $matches[0];
                     
                    //Computing deviance intervals
                   
                    foreach($loop_infos['data'] as $charac => $loop_info)
                    {
                        $color_class = 'cts_clustering_color_null';
                        $deviation_text = '';
                        $dev = $loop_infos['deviation'][$charac];

                        //Retrieving the header for the tooltip.
                        $tooltip = '';
                        if(array_key_exists($table_head[$charac], $lib_counting_headers))
                        {
                            $tooltip = $loop_id . ': ' . $lib_counting_headers[$table_head[$charac]];
                        }
                        else if(array_key_exists($table_head[$charac], $maqao_headers))
                        {
                            $tooltip = $loop_id . ': ' . $maqao_headers[$table_head[$charac]];
                        }
                        else
                        {
                            $tooltip = $loop_id . ': ' . $table_head[$charac];
                        }

                        if($loop_info === '' || $loop_info === 'None')
                        {
                            $loop_info = 'None';
                        }
                        //The Invocations, Cycles and Coverage MAQAO columns does not contain float values.
                        else if($charac == $coverage_column_index)
                        {
                            $loop_info = number_format($loop_info, 2);
                        }
                        else if($charac != 0 && $charac != $invocations_column_index && $charac != $cycles_column_index && $loop_info !== '-')
                        {
                            $interval = $intervals[$charac];
                            $loop_info = (float) $loop_info;
                            if ($loop_info > $interval['med']) 
                            {
                                if($loop_info <= $interval['high'])
                                {
                                    $color_class = 'cts_clustering_color_high';
                                }
                                else if ($loop_info <= $interval['higher'])
                                {
                                    $color_class = 'cts_clustering_color_higher';
                                }
                                else if ($loop_info <= $interval['max'])
                                {
                                    $color_class = 'cts_clustering_color_highest';
                                }
                            }
                            else
                            {
                                if($loop_info >= $interval['low'])
                                {
                                    $color_class = 'cts_clustering_color_low';
                                }
                                else if ($loop_info >= $interval['lower'])
                                {
                                    $color_class = 'cts_clustering_color_lower';
                                }
                                else if ($loop_info >= $interval['min'])
                                {
                                    $color_class = 'cts_clustering_color_lowest';
                                }
                            }
                            
                            $loop_info = number_format($loop_info, 2);
                        }
                        
                        if($dev !== 'DO NOT DISPLAY')
                        {
                            if($dev === '' || $dev === null)
                            {
                                $deviation_text = ' (UNKNOWN)';
                            }
                            else
                            {
                                $deviation_text = ' (' . number_format($dev, 2) . ')';
                            }
                        }
                        else
                        {
                            $color_class = "cts_clustering_no_color_change";
                        }
                        ?>
                            <td title="<?php echo $tooltip; ?>" class="<?php echo $color_class; ?>"><?php echo $loop_info . $deviation_text; ?></td>
                        <?php 
                    }
                    
                    
                    ?>
                        </tr>
                    <?php
                }
                
                //MEDIAN contents
                ?>
                    <tr>
                <?php 
                foreach($cluster_contents['cluster_info'] as $col => $cluster_info)
                {
                    $cluster_info_last_index = sizeof($cluster_contents['cluster_info']) - 1;
                    
                    //Total MAQAO coverage cell.
                    if ($col == $cluster_info_last_index)
                    {
                        $cluster_info = 'TOTAL: '.number_format($cluster_info, 2);
                    }
                    else if($col != 0 && $cluster_info !== 'None' && is_numeric($cluster_info))
                    {
                        $cluster_info = number_format($cluster_info, 2);
                    }
                    ?>
                        <td><?php echo $cluster_info; ?></td>
                    <?php 
                }
                
                ?>
                        </tr>
                        <tr>
                            <td>
                                <?php
                        echo "<button id=\"button_cluster_$cluster_id\" onclick=\"clustering_toggle_loops('cluster_$cluster_id')\">show</button>";
                                ?>
                            </td>
                        </tr>
                    </table>
                <?php 
            }
        }
    }
    
    //---------------------------------------------------------------------------
    
    /**
     *
     * @brief Override. Print the HTML code for all the page
     */
    public function html()
    {
        $current_tab = 'output'; 
        if (isset($_GET['current_tab']))
            $current_tab = $_GET['current_tab'];
        
        $this->script();
        $this->toolbar();
        $this->actions();
        $this->header();
        $this->button_bar($current_tab);
        
        switch ($current_tab)
        {
            case "output":
                $this->output();
                break;
            case "clusters":
                $this->clusters();
                break;
        }
        $this->information();
    }
}
