#************************************************************************
# Codelet Tuning Infrastructure
# Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

BUILD_DIR=$(CTI_ROOT)/build.tmp

CMD_USER=--user=
CMD_NO_SESSION=--no-session

CTI_FE_NAME=cti
CTI_ENVIRONMENT_SCRIPT=set_environment.sh
CTI_SERVICES_SCRIPT=set_services.sh

CTS_CFG=config.inc.php
CTS_CFG_DIR=cfg
CTS_CTI_RUN_SCRIPT=run_cti.sh
CTS_INCLUDE_DIR=include
CTS_PLUGINS_DIR=plugins
CTS_VIEW_DIR=view
CTS_LIGHTTPD_DIR=lighttpd
CTS_LIGHTTPD_PORT=3000
CTS_PAGES_DIR=pages
CTS_SHELL=bash
CTS_THIRD_PARTY_DIR=third-party
CTS_WWW_DIR=www
CTS_USER_DIR=user-data
CTS_TEST_DIR=tests

DESTINATION_CTS=${CTI_ROOT}/cts
DESTINATION_COMMON=${CTI_ROOT}/ctr-common
DESTINATION_TEMP=${CTI_ROOT}/ctr-temp

FULL_SHELL=/bin/bash

LINK_COMPANY=http://www.exascale-computing.eu
LINK_QUERY_DOC=https://github.com/franck-talbart/codelet_tuning_infrastructure/wiki/GeneralUsageQueries
LINK_TICKETS=https://github.com/franck-talbart/codelet_tuning_infrastructure/issues
LINK_WIKI=https://github.com/franck-talbart/codelet_tuning_infrastructure/wiki

