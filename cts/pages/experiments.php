<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['PLUGINS'].'CompilePlugin.php');
require_once($DIRECTORY['PLUGINS'].'CompilerPlugin.php');
require_once($DIRECTORY['PLUGINS'].'MaqaoCqaPlugin.php');
require_once($DIRECTORY['PLUGINS'].'MaqaoPerfPlugin.php');
require_once($DIRECTORY['PLUGINS'].'DecanPlugin.php');
require_once($DIRECTORY['PLUGINS'].'ProcessPlugin.php');
require_once($DIRECTORY['VIEW'].'SavedQueries.php');

$compile_plugin = new CompilePlugin();
$compiler_plugin = new CompilerPlugin();
$maqao_cqa_plugin = new MaqaoCqaPlugin();
$maqao_perf_plugin = new MaqaoPerfPlugin();
$decan_plugin = new DecanPlugin();
$process_plugin = new ProcessPlugin();

$current = '';
if(isset($_GET['main']))
{
    $current = $_GET['main'];
}

?>

<div id="left_menu">
    <ul class="navigation">
        <li class="toggleSubMenu"><span>Extraction</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $maqao_perf_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="MAQAO PERF" href="?page=experiments&main=cts_input_form&mode=add&produced_by=<?php echo $maqao_perf_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">MAQAO PERF</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $maqao_perf_plugin->uid || @$_GET['produced_by'] == 'maqao_perf')){ echo 'class="highlit"';}?> title="List of MAQAO PERF instances" href="?page=experiments&main=query&amp;search_query=*&produced_by=maqao_perf" onclick="load_main_frame(this); return false;">List of MAQAO PERF instances</a></li>
            </ul>
        </li>
        <li class="toggleSubMenu"><span>Compile</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $compile_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Compile" href="?page=experiments&main=cts_input_form&mode=add&produced_by=<?php echo $compile_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">Compile</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $compile_plugin->uid || @$_GET['produced_by'] == 'compile')){ echo 'class="highlit"';}?> title="List of compile instances" href="?page=experiments&main=query&amp;search_query=*&produced_by=compile" onclick="load_main_frame(this); return false;">List of compile instances</a></li>
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $compiler_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Compiler" href="?page=experiments&main=cts_input_form&mode=add&produced_by=<?php echo $compiler_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">Compiler</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $compiler_plugin->uid || @$_GET['produced_by'] == 'compiler')){ echo 'class="highlit"';}?> title="List of compilers" href="?page=experiments&main=query&amp;search_query=*&produced_by=compiler" onclick="load_main_frame(this); return false;">List of compilers</a></li>
            </ul>
        </li>
        <li class="toggleSubMenu"><span>Analysis</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $maqao_cqa_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="MAQAO" href="?page=experiments&main=cts_input_form&mode=add&produced_by=<?php echo $maqao_cqa_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">MAQAO CQA</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $maqao_cqa_plugin->uid || @$_GET['produced_by'] == 'maqao_cqa')){ echo 'class="highlit"';}?> title="List of MAQAO CQA instances" href="?page=experiments&main=query&amp;search_query=*&produced_by=maqao_cqa" onclick="load_main_frame(this); return false;">List of MAQAO CQA instances</a></li>
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $decan_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="DECAN" href="?page=experiments&main=cts_input_form&mode=add&produced_by=<?php echo $decan_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">DECAN</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $decan_plugin->uid || @$_GET['produced_by'] == 'decan')){ echo 'class="highlit"';}?> title="List of DECAN instances" href="?page=experiments&main=query&amp;search_query=*&produced_by=decan" onclick="load_main_frame(this); return false;">List of DECAN instances</a></li>
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $process->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Process" href="?page=experiments&main=cts_input_form&mode=add&produced_by=<?php echo $process_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">Process</a></li>
 
            </ul>
        </li>
    </ul>
    <?php
        $login_uid = $_SESSION['login_uid'];
        $saved_queries = new SavedQueries($login_uid);
        $saved_queries->html();
    ?>
</div>

<div id="main_frame">

<?php

if(!isset($_GET['main']))
{
    $_GET['main'] = default_include('experiments');
}

if (security_include($_GET['main']))
{
    require_once($DIRECTORY['PAGES']. $_GET['main'] . '.php');
}
else
{
    echo 'This page is not allowed!';
}

?>
</div>
