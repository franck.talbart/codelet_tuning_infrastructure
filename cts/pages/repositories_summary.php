<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['PLUGINS'].'RepositoryPlugin.php');
require_once($DIRECTORY['VIEW'].'SavedQueries.php');

$repository_plugin = new RepositoryPlugin();

$current = '';
if(isset($_GET['main']))
{
    $current = $_GET['main'];
}

?>
<div id="left_menu">
    <ul class="navigation">
        <li><a title="Local repositories" href="?page=repositories_summary&main=list_local_repositories" onclick="load_main_frame(this); return false;">Local repositories</a></li>
        <li class="toggleSubMenu"><span>Common repository</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'list_plugin' && strstr(@$_GET['repository_type'], 'common')){ echo 'class="highlit"';}?> title="Plugins" href="?page=repositories_summary&main=list_plugin&repository_type=common" onclick="load_main_frame(this); return false;">Plugins</a></li>
                <li><a <?php if($current == 'query' && strstr(@$_GET['repository_type'], 'CTR_REP_COMMON')){ echo 'class="highlit"';}?> title="Data" href="?page=repositories_summary&main=query&repository_type=<?php echo CTR_REP_COMMON;?>&produced_by=&date_start=&date_end=&user=&search_query=*" onclick="load_main_frame(this); return false;">Data</a></li>
                <li><a title="Download" href="download.php?plugin=<?php echo $repository_plugin->uid;?>&uid=common&name=Common.zip" target="_blank">Download</a></li>
            </ul>
        </li>
        <li class="toggleSubMenu"><span>All</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'list_plugin' && strstr(@$_GET['repository_type'], 'all')){ echo 'class="highlit"';}?> title="Plugins" href="?page=repositories_summary&main=list_plugin&repository_type=all" onclick="load_main_frame(this); return false;">Plugins</a></li>
                <li><a <?php if($current == 'query' && strstr(@$_GET['repository_type'], 'all')){ echo 'class="highlit"';}?> title="Data" href="?page=repositories_summary&main=query&repository_type=all&search_query=*" onclick="load_main_frame(this); return false;">Data</a></li>
            </ul>
        </li>
    </ul>
    <?php
        $login_uid = $_SESSION['login_uid'];
        $saved_queries = new SavedQueries($login_uid);
        $saved_queries->html();
    ?>
</div>

<div id="main_frame">

<?php

if(!isset($_GET['main']))
{
    $_GET['main'] = default_include('repositories_summary');
}

if (security_include($_GET['main']))
{
    require_once($DIRECTORY['PAGES']. $_GET['main'] . '.php');
}
else
{
    echo 'This page is not allowed!';
}

?>

</div>
