<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/ 

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['VIEW'].'html_page_view_data.php');
require_once($DIRECTORY['PLUGINS'].'FilePlugin.php');


//Case of page refresh using the alias update button.
if(array_key_exists('alias', $_POST))
{
    $alias_plugin = new AliasPlugin();
    $alias_plugin->get_data('data', array('alias' => $_POST['alias'], 'uid' => $_GET['data_uid']));
}
    
//Case of page refresh with note.
if(array_key_exists('note_cmd', $_POST))
{
    $note_plugin = new NotePlugin();
    $note_plugin->get_data($_POST['note_cmd'], $_POST);
}
    
//Case of page refresh with tag.
if(array_key_exists('tag_cmd', $_POST))
{
    $tag_plugin = new TagPlugin();
    $tag_plugin->get_data($_POST['tag_cmd'], $_POST);
}

//Case of page refresh with additional files
if(array_key_exists('additional_file_cmd', $_POST))
{
    $import_files_plugin = new FilePlugin();

    if ($_POST['additional_file_cmd'] == 'add_additional_file')
    {
        rename($_FILES['file']['tmp_name'], '/tmp/'.$_FILES['file']['name']);
        $_POST['file'] = '/tmp/'.$_FILES['file']['name'];
    }

    $import_files_plugin->get_data($_POST['additional_file_cmd'], $_POST);
}

view_data_display($_GET, 0);

?>
