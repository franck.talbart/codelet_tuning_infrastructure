<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['PLUGINS'].'ApplicationPlugin.php');
require_once($DIRECTORY['PLUGINS'].'ApplicationGroupPlugin.php');
require_once($DIRECTORY['PLUGINS'].'LoopPlugin.php');
require_once($DIRECTORY['PLUGINS'].'BinaryPlugin.php');
require_once($DIRECTORY['PLUGINS'].'LoopGroupPlugin.php');
require_once($DIRECTORY['PLUGINS'].'MaqaoPerfPlugin.php');
require_once($DIRECTORY['VIEW'].'SavedQueries.php');

$application_plugin = new ApplicationPlugin();
$application_group_plugin = new ApplicationGroupPlugin();
$loop_plugin = new LoopPlugin();
$binary_plugin = new BinaryPlugin();
$loop_group_plugin = new LoopGroupPlugin();
$maqao_perf_plugin = new MaqaoPerfPlugin();


$current = '';
if(isset($_GET['main']))
{
    $current = $_GET['main'];
}

?>
<div id="left_menu">
    <ul class="navigation">
       <li class="toggleSubMenu"><span>Application</span>
            <ul class="subMenu">
               <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $application_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Add an application" href="?page=codelets&main=cts_input_form&mode=add&amp;produced_by=<?php echo $application_plugin->uid;?>&amp;command=init" onclick="load_main_frame(this); return false;">Add an application</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $application_plugin->uid || @$_GET['produced_by'] == 'application')){ echo 'class="highlit"';}?> title="List of applications" href="?page=codelets&main=query&amp;search_query=*&produced_by=application" onclick="load_main_frame(this); return false;">List of applications</a></li>
                <li><a <?php if($current == 'cts_input_add' && $_GET['produced_by'] == $application_group_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Add an application group" href="?page=codelets&main=cts_input_form&mode=add&produced_by=<?php echo $application_group_plugin->uid;?>&amp;command=init" onclick="load_main_frame(this); return false;">Add an application group</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $application_group_plugin->uid || @$_GET['produced_by'] == 'application_group')){ echo 'class="highlit"';}?> title="List of application groups" href="?page=codelets&main=query&amp;search_query=*&produced_by=application_group" onclick="load_main_frame(this); return false;">List of application groups</a></li>
            </ul>
        </li>
        <li class="toggleSubMenu"><span>Binary</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $binary_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Add a binary" href="?page=codelets&main=cts_input_form&mode=add&amp;produced_by=<?php echo $binary_plugin->uid;?>&amp;command=init" onclick="load_main_frame(this); return false;">Add a binary</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $binary_plugin->uid || @$_GET['produced_by'] == 'binary')){ echo 'class="highlit"';}?> title="List of binaries" href="?page=codelets&main=query&amp;search_query=*&produced_by=binary" onclick="load_main_frame(this); return false;">List of binaries</a></li>
            </ul>
        </li>
        <li class="toggleSubMenu"><span>Loop</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $loop_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Add a loop" href="?page=codelets&main=cts_input_form&mode=add&amp;produced_by=<?php echo $loop_plugin->uid;?>&amp;command=init" onclick="load_main_frame(this); return false;">Add a loop</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $loop_plugin->uid || @$_GET['produced_by'] == 'loop')){ echo 'class="highlit"';}?> title="List of loops" href="?page=codelets&main=query&amp;search_query=*&produced_by=loop" onclick="load_main_frame(this); return false;">List of loops</a></li>
            </ul>
        </li>
        <li class="toggleSubMenu"><span>Group of loops</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $loop_group_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Add a set of loops" href="?page=codelets&main=cts_input_form&mode=add&produced_by=<?php echo $loop_group_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">Add a set of loops</a></li>
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $loop_group_plugin->uid || @$_GET['produced_by'] == 'loop_group')){ echo 'class="highlit"';}?> title="List of loop groups" href="?page=codelets&main=query&amp;search_query=*&produced_by=loop_group" onclick="load_main_frame(this); return false;">List of loop groups</a></li>
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $maqao_perf_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Create loops from an application" href="?page=codelets&main=cts_input_form&mode=add&amp;produced_by=<?php echo $maqao_perf_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">Create loops from an application</a></li>
            </ul>
        </li>
    </ul>
    <?php
        $login_uid = $_SESSION['login_uid'];
        $saved_queries = new SavedQueries($login_uid);
        $saved_queries->html();
    ?>

</div>

<div id="main_frame">

<?php

if(!isset($_GET['main']))
{
    $_GET['main'] = default_include('codelets');
}

if (security_include($_GET['main']))
{
    require_once($DIRECTORY['PAGES']. $_GET['main'] . '.php');
}
else
{
    echo 'This page is not allowed!';
}

?>

</div>
