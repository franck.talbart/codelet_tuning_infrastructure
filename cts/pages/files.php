<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_utils.inc.php');
require_once($DIRECTORY['PLUGINS'].'ImportCsvPlugin.php');
require_once($DIRECTORY['PLUGINS'].'FilePlugin.php');
require_once($DIRECTORY['PLUGINS'].'DatabasePlugin.php');
require_once($DIRECTORY['VIEW'].'SavedQueries.php');


$import_csv_plugin = new ImportCsvPlugin();
$file_plugin = new FilePlugin();
$database_plugin = new DatabasePlugin();

$current = '';
if(isset($_GET['main']))
{
    $current = $_GET['main'];
}

?>

<div id="left_menu">
    <ul class="navigation">
        <li class="toggleSubMenu"><span>Add</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $import_csv_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Import a CSV file" href="?page=files&main=cts_input_form&mode=add&amp;produced_by=<?php echo $import_csv_plugin->uid;?>&amp;command=init" onclick="load_main_frame(this); return false;">Import a CSV file</a>
                </li>
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $file_plugin->uid && $_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Import a file" href="?page=files&main=cts_input_form&mode=add&amp;produced_by=<?php echo $file_plugin->uid;?>&amp;command=init" onclick="load_main_frame(this); return false;">Import a file</a>
                </li>
                <li><a <?php if($current == 'cts_input_form' && $_GET['produced_by'] == $database_plugin->uid && $_GET['command'] == 'from_csv'){ echo 'class="highlit"';}?> title="Create a schema from CSV" href="?page=files&main=cts_input_form&mode=add&amp;produced_by=<?php echo $database_plugin->uid;?>&amp;command=from_csv" onclick="load_main_frame(this); return false;">Create a schema from CSV</a>
                </li>
            </ul>
        </li>
        <li class="toggleSubMenu"><span>List</span>
            <ul class="subMenu">
                <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $file_plugin->uid || @$_GET['produced_by'] == 'file')){ echo 'class="highlit"';}?> title="List of files" href="?page=files&main=query&amp;search_query=*&produced_by=file" onclick="load_main_frame(this); return false;">List of files</a></li>
            </ul>
        </li>
    </ul>
    <?php
        $login_uid = $_SESSION['login_uid'];
        $saved_queries = new SavedQueries($login_uid);
        $saved_queries->html();
    ?>

</div>

<div id="main_frame">

<?php

if(!isset($_GET['main']))
{
    $_GET['main'] = default_include('files');
}

if (security_include($_GET['main']))
{
    require_once($DIRECTORY['PAGES']. $_GET['main'] . '.php');
}
else
{
    echo 'This page is not allowed!';
}

?>

</div>
