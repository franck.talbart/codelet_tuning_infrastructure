<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'globals.inc.php');
require_once($DIRECTORY['PLUGINS'].'UserPlugin.php');
require_once($DIRECTORY['PLUGINS'].'CategoryPlugin.php');
require_once($DIRECTORY['PLUGINS'].'OrganizationPlugin.php');

$user_plugin = new UserPlugin();
$category_plugin = new CategoryPlugin();
$organization_plugin = new OrganizationPlugin();

$current = '';
if(isset($_GET['main']))
{
    $current = $_GET['main'];
}

?>


<div id="left_menu">
<ul class="navigation">
    <li class="toggleSubMenu"><span>System</span>
        <ul class="subMenu">
            <li><a <?php if($current == 'update'){ echo 'class="highlit"';}?> title="Update CTI" href="?page=administration&main=update" onclick="load_main_frame(this); return false;">Update CTI</a></li>
        </ul>
    </li>
    <li class="toggleSubMenu"><span>User</span>
        <ul class="subMenu">
            <li><a <?php if($current == 'cts_input_form' && @$_GET['produced_by'] == $user_plugin->uid && @$_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Add a user" href="?page=administration&main=cts_input_form&mode=add&produced_by=<?php echo $user_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">Add a user</a></li>
            <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $user_plugin->uid || @$_GET['produced_by'] == 'user')){ echo 'class="highlit"';}?> title="List of users" href="?page=administration&main=query&search_query=*&produced_by=user" onclick="load_main_frame(this); return false;">List of users</a></li>
        </ul>
    </li>
    <li class="toggleSubMenu"><span>Category</span>
        <ul class="subMenu">
            <li><a <?php if($current == 'cts_input_form' && @$_GET['produced_by'] == $category_plugin->uid && @$_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Add a category" href="?page=administration&main=cts_input_form&mode=add&produced_by=<?php echo $category_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">Add a category</a></li>
            <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $category_plugin->uid || @$_GET['produced_by'] == 'category')){ echo 'class="highlit"';}?> title="List of categories" href="?page=administration&main=query&search_query=*&produced_by=category" onclick="load_main_frame(this); return false;">List of categories</a></li>
        </ul>
    </li>
    <li class="toggleSubMenu"><span>Organization</span>
        <ul class="subMenu">
            <li><a <?php if($current == 'cts_input_form' && @$_GET['produced_by'] == $organization_plugin->uid && @$_GET['command'] == 'init'){ echo 'class="highlit"';}?> title="Add an organization" href="?page=administration&main=cts_input_form&mode=add&produced_by=<?php echo $organization_plugin->uid;?>&command=init" onclick="load_main_frame(this); return false;">Add an organization</a></li>
            <li><a <?php if($current == 'query' && (@$_GET['produced_by'] == $organization_plugin->uid || @$_GET['produced_by'] == 'organization')){ echo 'class="highlit"';}?> title="List of organizations" href="?page=administration&main=query&search_query=*&produced_by=organization" onclick="load_main_frame(this); return false;">List of organization</a></li>
        </ul>
    </li>
</ul>
</div>

<div id="main_frame">

<?php
if(!isset($_GET['main']))
{
    $_GET['main'] = default_include('administration');
}

if (security_include($_GET['main']))
{
    require_once($DIRECTORY['PAGES'] . $_GET['main']. '.php');
}
else
{
    echo 'This page is not allowed!';
}
?>

</div>

