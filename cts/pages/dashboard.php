<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Florent Hemmi, Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['VIEW'].'html_page_dashboard.php');

$current = '';
if(isset($_GET['main']))
{
    $current = $_GET['main'];
}

?>
<script type="text/javascript" src="<?php echo $DIRECTORY['JS']; ?>dashboard.js"></script>
<script type="text/javascript" src="<?php echo $DIRECTORY['JS']; ?>jquery.colorPicker.min.js"></script>

<div id="left_menu">
<ul class="navigation">
    <li><a title="New widget" href="?page=dashboard&action=form" onclick="cts_dashboard.form_create_widget(); return false;">New widget</a></li>
    <li><a title="Return to dashboard" href="?page=dashboard" onclick="cts_dashboard.show(); return false;">Return to dashboard</a></li>
</ul>
</div>

<div id="main_frame">
<?php
if (isset($_GET['action']))
{
    $_POST['action'] = $_GET['action'];
    unset($_GET['action']);
    
    require_once('ajax/dashboard.php');
}
else
{
    dashboard_display();
}
?>
</div>
