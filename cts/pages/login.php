<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'query.inc.php');
require_once($DIRECTORY['INCLUDE'].'security.inc.php');

?>
    <br/>
<?php 

$registration = 0;
if (isset($_GET['registration']))
    $registration = $_GET['registration'];

if (isset($_POST['username']) && login_user($_POST['username'], md5($_POST['password'])))
{
    $_SESSION['username'] = $_POST['username'];
    $_SESSION['pwd'] = md5($_POST['password']);
    $_SESSION['login_uid'] = get_user_uid($_POST['username']); 

    if (isset($_SESSION['previous_page']) && $_SESSION['previous_page'] != '')
    {
        $link = $_SESSION['previous_page'];
        $_SESSION['previous_page'] = '';
    }
    else
    {
        $link = '?';
    }
    
    ?>
    <br/>
    <div align="center">
        You are now connected!<br/>
        <a href=" <?php echo $link;?>">Go to CTS</a><br/><br/>
        You will be redirected in a few seconds.
    </div><br/>
    <meta http-equiv="refresh" content="3; url=<?php echo $link;?>"/>
    <?php 
}
else if(isset($_POST['username']))
{
        ?>
        <div align='center'>
            <b><font color="#E80000">Wrong username or password!</font></b>
        </div>
        <?php
        display_form();
}
else if ($registration == 1)
{
    ?>
    <br/><div align="center">
            <div id="login">
            <form action="?registration=2" method="POST">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" >
                                <div align="center">
                                    <strong>
                                        Registration form - Welcome to CTI!
                                    </strong>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Username
                            </td>
                            <td>
                                <input type="text" name="reg_username" value=""/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Full name
                            </td>
                            <td>
                                <input type="text" name="reg_full_name" value=""/>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                Email
                            </td>
                            <td>
                                <input type="text" name="reg_email" value=""/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div align="center">
                    <input type="submit" value="Send">
                </div>
            </form>
        <div align="center"><small>
            An email will be sent to the administrator.
            </small>
        </div>
        </div>
        <br/>
    </div> 
    <?php
}
else if ($registration == 2)
{
    $email = $CFG['admin_email']; 

    $subject = '[CTI] Account request';
    $message = 'The following user asks an account:';
    $message .= "\nUsername: ".$_POST['reg_username'];
    $message .= "\nFull name: ".$_POST['reg_full_name'];
    $message .= "\nEmail: ".$_POST['reg_email'];
    $receiver = $email;
    $headers = "From: \"CTI\"<$receiver>\n";
    $headers .= "Reply-To: $receiver\n";
    $headers .= "Content-Type: text/plain; charset=\"iso-8859-1\"";
    if(mail($receiver,$subject,$message,$headers))
    {
        ?>
        <div align="center">
            An email has been sent to the administrator. He will create the account.
        </div>
        <?php 
    }
    else
    {
        ?>
        <div align="center">
            Error while sending the email! Please contact the administrator.
        </div>
        <?php 
    }
}
else
{
    display_form();
}

function display_form()
{
    $username = '';
    
    if (isset($_POST['username']))
    {
        $username = $_POST['username'];
    }
    ?>
    <br/>
          <div align="center">
            <img border="0" src="img/logo_ecr.png"/>
          </div>
        
         <br/>
          <br/>
     <div align="center"><div id="login">
            <form action="?page=login" method="POST">
                <table width="300">
                    <thead>
                        <tr>
                            <th colspan="2" >
                                <div align="center"><strong>Login form - Welcome to CTI!</strong></div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Username 
                            </td>
                            <td>
                                <input type="text" name="username" value="<?php echo $username;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Password
                            </td>
                            <td>
                                <input type="password" name="password"/>
                            </td>
                        </tr>
                        <tr>
                        <td>
                            <input type="checkbox" checked="checked" name="remember"> Remember
                        </td>
                            <td>
                                <div align="right">
                                    <input type="submit" value="Sign In"/>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        <div align="center">
            <a href="?registration=1">Create an account</a>
        </div>
       </div>
      </div>
      <br/>
      <?php 
}

?>
