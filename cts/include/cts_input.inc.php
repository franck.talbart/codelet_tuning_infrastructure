<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit


require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['INCLUDE'].'plugins.inc.php');
require_once($DIRECTORY['INCLUDE'].'cts_create.inc.php');
require_once($DIRECTORY['INCLUDE'].'query.inc.php');
require_once($DIRECTORY['PLUGINS'].'ViewPlugin.php');
require_once($DIRECTORY['PLUGINS'].'AliasPlugin.php');
require_once($DIRECTORY['PLUGINS'].'ListPlugin.php');
require_once($DIRECTORY['PLUGINS'].'QueryPlugin.php');
require_once($DIRECTORY['PLUGINS'].'DatabasePlugin.php');
require_once($DIRECTORY['PLUGINS'].'DaemonPlugin.php');
require_once($DIRECTORY['PLUGINS'].'ProcessPlugin.php');
require_once($DIRECTORY['PLUGINS'].'Plugin.php');

/**
 * @brief Fetches and returns information to build a create form from a command.
 * @param params: an array containing the required basic information on what is to be added
 *             Will basically contain the plugin and command that will generate the data, along with possible default values
 *             that will pre-fill the future form.
 * @return an object containing the data used by the form.
 */
function cts_input_get_form_data($params)
{
    //Processing parameters
    $result = array();
    $expand = False;
    if (array_key_exists('expand', $params))
        $expand = $params['expand'];
    if(array_key_exists('produced_by', $params))
    {
        $result['plugin_uid'] = $params['produced_by'];
    }
    if(array_key_exists('command', $params))
    {
        $result['command'] = $params['command'];
    }
    if(array_key_exists('default', $params))
    {
        $result['default_values'] = $params['default'];
    }
    else
    {
        $result['default_values'] = array();
    }
 
    if(array_key_exists('entry', $params) && isset($result['command']) && $result['command'] == 'update')
    {
        $result['default_values']['entry'] = $params['entry'];
        
        //Preparing default values for the update command.
        
        $view_plugin = new ViewPlugin();
        $view_info = $view_plugin->get_data('data', array('data_uid' => $params['entry']));
        foreach($view_info->output_file->init->params as $param_contents)
        {
            $result['default_values'][$param_contents->{META_ATTRIBUTE_NAME}] = $param_contents->{META_ATTRIBUTE_VALUE};
        }
    }
    
    //Getting plugin input info.
    $view_plugin = new ViewPlugin();
    $info = $view_plugin->get_data('plugin', array('plugin_uid' => $result['plugin_uid']));
    
    //Failing gracefully on error.
    if(isset($info->CTI_PLUGIN_CALL_ERROR))
    {
        echo 'Error calling View on plugin '.$result['plugin_uid'];
        
        //Returning an error pointer to the calling env.
        return array('error' => $info);
    }
    
    $work_params = $info->input_file;
    
    //Removing hardcoded values (already present in the input_default of the plugin)
    foreach($work_params->$result['command']->params as $param_index => $param_value)
    {
        //Removing the format attribute
        if($param_value->{META_ATTRIBUTE_NAME} == 'format')
        {
            unset($work_params->$result['command']->params[$param_index]);
            continue;
        }
        
        // Franck: Here, isset is evil since it returns False
        // if value is NULL! AHAHAHAHAHAHAHAH
        if (property_exists($param_value, META_ATTRIBUTE_VALUE) && !$expand)
        {
            $work_params->$result['command']->params[$param_index]->hidden = True;
        }
    }
    
    //Assigning default values
    foreach($work_params->$result['command']->params as $index => $dict)
    {
        if(array_key_exists($dict->{META_ATTRIBUTE_NAME}, $result['default_values']))
        {
            $default = $result['default_values'][$dict->{META_ATTRIBUTE_NAME}];
            if(cts_is_UID($default))
            {
                $alias_plugin = new AliasPlugin();
                if($dict->type == 'PLUGIN_UID')
                {
                    $alias_info = $alias_plugin->get_data('get_plugin_alias', array('uid' => $default), true);
                    if(!isset($alias_info->CTI_PLUGIN_CALL_ERROR) && isset($alias_info->alias))
                    {
                        $default = $alias_info->alias;
                    }
                }
                else if($dict->type == 'DATA_UID')
                {
                    $alias_info = $alias_plugin->get_data('get_data_alias', array('uid' => $default), true);
                    if(!isset($alias_info->CTI_PLUGIN_CALL_ERROR) && isset($alias_info->alias))
                    {
                        $default = $alias_info->alias;
                    }
                }
            }
            $work_params->$result['command']->params[$index]->default_values = $default;
        }
    }
    
    $result['params'] = $work_params;
    
    //Setting repository type
    $result['repository_type'] = '';
    
    if(isset($work_params->$result['command']->attributes->{META_ATTRIBUTE_REP}))
    {
        $result['repository_type'] = $work_params->$result['command']->attributes->{META_ATTRIBUTE_REP};
    }
    
    
    //Getting list of local repositories
    $list_plugin = new ListPlugin();
    $repos_info = $list_plugin->get_data('local_repositories');
    //Failing gracefully on error.
    if(isset($repos_info->CTI_PLUGIN_CALL_ERROR))
    {
        echo 'Error calling list local_repositories.';
        
        //Returning an error pointer to the calling env.
        return array('error' => $repos_info);
    }
    
    //Creating repository list with Common and Temp
    $repos_list = array(
        (object) array(
            'type' => 2,
            'name' => 'Temp',
            'alias' => 'Temp'
        ),
        (object) array(
            'type' => 1,
            'name' => 'Common',
            'alias' => 'Common'
        )
    );
    
    //Adding local repositories and finding the one that will be selected by default
    $default_repos = (object) array('path' => '', 'last_use' => '');
    foreach($repos_info as $repos)
    {
        $repos_list[] = (object) array(
            'name' => $repos->uid,
            'type' => 0,
            'path' => $repos->path,
            'alias' => $repos->alias
        );
        //Find the last accessed local and mark it for default
        if(strcmp($default_repos->last_use, $repos->last_use)<0)
        {
            $default_repos = $repos;
        }
    }
    
    $result['list_repositories'] = $repos_list;
    if($result['repository_type'] == CTR_REP_LOCAL)
    {
        $result['local_repository_path'] = $default_repos->path;
    }
    else
    {
        $result['local_repository_path'] = NULL;
    }
    
    $result['produce_data'] = False;
    if($work_params->$result['command']->attributes->produce_data)
    {
        $result['produce_data'] = True;
    }
    
    return (object) $result;
}
//---------------------------------------------------------------------------

/**
 *
 * Displays a form that asks user to enter data.
 * @param param: The current data to transform into an input.
 * @param force_type: replaces the type of the input for this parameter.
 * @param hidden: hide the input field
 */
function cts_form_enter_data($param, $data, $force_type=false, $hidden=false)
{
    $hidden_html = '';
    if ($hidden)
        $hidden_html = 'style="display:none"';
    ?>
        <tr <?php echo $hidden_html;?>>
            <td>
    <?php
    $islist = false;
    $dropbox_displayed = false;
    if (isset($param->{META_ATTRIBUTE_DESC}))
    {
        echo $param->{META_ATTRIBUTE_DESC} . ': '. "\n";
    }
    else
    {
        echo $param->{META_ATTRIBUTE_NAME} . ': '. "\n";
    }
    ?>
        </td>
    <?php
    $name = $param->{META_ATTRIBUTE_NAME};
    $value = '';
    
    if (isset($param->default_values))
    {
        $param->{META_ATTRIBUTE_VALUE} = $param->default_values;
    }
    if (isset($param->{META_ATTRIBUTE_VALUE}) && !@$param->{META_ATTRIBUTE_PASSWORD})
    {
        $value = $param->{META_ATTRIBUTE_VALUE};
    }
    $directory_value = false;
    if (isset($param->{META_ATTRIBUTE_DIRECTORY}) && @$param->{META_ATTRIBUTE_DIRECTORY})
    {
        $directory_value = $param->{META_ATTRIBUTE_DIRECTORY};
    }
    if($force_type)
    {
        $param->{META_ATTRIBUTE_TYPE} = $force_type;
    }
    if (@$param->{META_ATTRIBUTE_LIST})
    {
        $islist = true;
        ?>
            <td>
                <?php
                    if ($param->{META_ATTRIBUTE_TYPE} == META_CONTENT_ATTRIBUTE_TYPE_FILE)
                    {
                        ?>
                        <table>
                            <?php
                                foreach ($value as $v)
                                {
                                    ?>
                                    <tr>
                                        <td>
                                            <small><strong><?php echo $v; ?></strong></small>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="checkbox_delete_files_<?php echo $name; ?>" name="delete_files[]" value="<?php echo "${v}______${name}";?>"/> <small>Delete</small>
                                        </td>
                                    </tr>
                                <?php
                                }
                            ?>
                        </table>
                            <input type="file"
                                name="<?php echo $name;?>"
                                id="<?php echo $name;?>"
                                value=""
                            />
                    <?php
                    }
                    else
                    {
                        $value = '';
                        if (isset($param->{META_ATTRIBUTE_VALUE}) && is_array($param->{META_ATTRIBUTE_VALUE}))
                        {
                            foreach ($param->{META_ATTRIBUTE_VALUE} as $element)
                            {
                                $value = $value . $element . "\n";
                            }
                        }
 
                        ?>
                        <textarea cols="26" name="<?php echo $name;?>" id="<?php echo $name;?>"><?php echo $value; ?></textarea>
                        <?php
                    }
                ?>
            </td>
            <td>
        <?php
        if (isset($param->{META_ATTRIBUTE_PRODUCED_BY}))
        {
            ?>
                    <input type="button" onClick="open_popup('<?php echo $param->{META_ATTRIBUTE_PRODUCED_BY};?>', '<?php echo $name;?>', '1')" value="Find"/>
            <?php
        }?>
        </td>
        <td>
        </td>
        <?php
    }
    else
    {
        if (@$param->{META_ATTRIBUTE_TYPE})
        {
            if ($param->{META_ATTRIBUTE_TYPE} == META_CONTENT_ATTRIBUTE_TYPE_FILE)
            {
                $type = 'file';
            }
            else if ($param->{META_ATTRIBUTE_TYPE} == META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID)
            {
                cts_input_repository_dropbox($data, $name, true);
                $dropbox_displayed = true;
            }
            else if ($param->{META_ATTRIBUTE_TYPE} == META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN)
            {
                $type = 'checkbox';
            }
            else if (@$param->{META_ATTRIBUTE_PASSWORD})
            {
                $type = 'password';
            }
        }
        //Default type
        if (! isset($type))
        {
            $type = 'text';
        }
        
        if(!$dropbox_displayed)
        {
            ?>
                <td>
                   <?php 

                        if ($type == 'file')
                        { 
                        ?>
                            <table>
                            <?php
                                if ($value != '')
                                {
                                    ?>
                                    <tr>
                                        <td>
                                            <small><strong><?php echo $value; ?></strong></small>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="checkbox_delete_<?php echo $name; ?>" name="delete[]" value="<?php echo "$name";?>"/> <small>Delete</small>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            ?>
                            </table>
                        <?php
                        }
                        if (is_object($value))
                        {
                            $tmp_value = '';
                            // COMPLEX type
                            foreach ($value as $v)
                            {
                                $tmp_value .= $v;
                            }
                            $tmp_value = trim($tmp_value);
                            if ($tmp_value == '')
                                $tmp_value = '{}';

                            $value = $tmp_value;
                        }
                        if ($directory_value)
                        {
                            $type = 'text';
                        }
                        // If param is a directory and user is updating an entry
                        // the directory value could NOT be displayed as the directory does not necessary exist
                        // on the current machine. Setting the value to None does not resolve the issue because
                        // the parameter would be erased...
                        // The problem does not occur is param is a file because if no file is provided in the
                        // input field, the param does not appear in the $_POST variable... (Thanks HTML 4!!!!)
                        if(!($directory_value && $value != ''))
                        { 
                   ?>
                    <input type="<?php echo $type;?>"
                       name="<?php echo $name;?>"
                       id="<?php echo $name;?>"
                       value="<?php if ($type == 'checkbox'){echo '1';}elseif($value === '' && $type != 'password' && $type != 'file'){ echo 'NONE'; }elseif($type != 'file'){echo $value; }?>"
                       <?php
                            if($type === 'checkbox' && is_bool($value) && $value == True)
                            {
                                ?>checked="True"<?php
                            }
                            if($type !== 'checkbox' && $type !== 'file')
                            {
                                ?>
                                  onFocus="if (this.value == 'NONE') this.value=''; document.getElementsByClassName('checkbox_<?php echo $name; ?>')[0].checked=false; "
                                <?php
                            }
                            if ($type == "file")
                            {
                                ?>
                                onChange="if (this.value == '') document.getElementsByClassName('checkbox_<?php echo $name; ?>')[0].checked=true;"
                                <?php
                            }?>/>
                </td>
            <?php
                }
        }
        
        if (isset($param->{META_ATTRIBUTE_PRODUCED_BY}))
        {
            ?>
                <td>
                    <input type="button" onClick="open_popup('<?php echo $param->{META_ATTRIBUTE_PRODUCED_BY};?>', '<?php echo $name;?>', '0') " value="Find"/>
                </td>
            <?php
        }
        else
        {
            ?>
                <td></td>
            <?php
        }
        ?>
        <td>
            <?php
           if ($param->{META_ATTRIBUTE_TYPE} != META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN)
            {
                ?>
                    <input type="checkbox" class="checkbox_<?php echo $name; ?>" name="none[]"  <?php if  ($value === '')  echo 'checked'; ?> 
                value="<?php echo $name; ?>" onChange="if (this.checked){document.getElementById('<?php echo $name; ?>').value = 'NONE';}else{document.getElementById('<?php echo $name; ?>').value = '';}" <?php if ($type == 'file'){?> style="opacity:0;"/><?php } else { ?>/> None <?php }}?>
        </td>
    <?php
    }
    ?>
    </tr>
    <?php
    return $islist;
}
//---------------------------------------------------------------------------

/**
 *
 * Echoes the repository dropbox to the output.
 * @param data: the data from cts_input plugin.
 * @param local: if true, will only display local repositories. Will add common and temp otherwise.
 */
function cts_input_repository_dropbox($data, $select_id='repository_type', $local=false)
{
    ?>
        <td>
            <select name="<?php echo $select_id;?>">
    <?php
    $default_repos_path  = '';
    if (@$data->local_repository_path)
    {
        $default_repos_path = $data->local_repository_path;
    }
    
    $first = 1;
    foreach ($data->list_repositories as $repos)
    {
        $select = '';
        $value = $repos->type;
        //If it's a local repository
        if ($repos->type == CTR_REP_LOCAL)
        {
            $value = $repos->name;
            if (!$repos->alias)
            {
                $repos->alias = 'No Alias';
            }
        }
        else if ($local)
        {
            continue;
        }
        if($data->repository_type == $repos->type)
        {
            if ($repos->type == CTR_REP_LOCAL)
            {
                //If we don't know exactly what is the default local repository,
                //we select the first one
                if(!$default_repos_path && $first == 1)
                {
                    $select = 'selected ';
                    $first = 0;
                }
                else if ($default_repos_path == $repos->path)
                {
                    $select = 'selected ';
                }
            }
            //If repository is common or temp
            else
            {
                $select = 'selected ';
            }
        }
        ?>
            <option <?php echo $select;?>value="<?php echo $value;?>"><?php echo $repos->alias;?></option>
        <?php
    }
    ?>
                </select>
            </td>
    <?php
}
//---------------------------------------------------------------------------

/**
 *
 * Generates a form for for adding/updating CTS-related entries.
 * @param parameters: the get parameters from the page calling for the form
 * @param data: the data from cts_input plugin
*/
function cts_input_form_generate($parameters, $data)
{
    global $DIRECTORY;
    
    $command = $data->command;
    $displayed_mode = ucfirst($command);
    $expand = False;
    if (array_key_exists('expand', $parameters))
        $expand = $parameters['expand'];
 
    ?>
        <h1><?php echo $displayed_mode;?> data for "<?php echo cts_create_uid_visualization($data->plugin_uid, CTR_ENTRY_PLUGIN);?>"</h1>
        <div class="block">
    <?php
    
    $data_uid_url = '';
    if(@$data->data_uid)
    {
        ?>
            <input type="hidden" name="entry" value="<?php echo $data->data_uid->uid;?>"/>
        <?php
    }
    
    if($parameters['mode'] == 'update')
    {
        if (!@$data->data_id)
        {
            $data->data_id = $data->default_values['entry'];
        }
        
        ?>
        <h2> Current data: <?php echo cts_create_uid_visualization($data->data_id, CTR_ENTRY_DATA);?></h2>
        <?php
    }
    
    ?>
    <form method="POST" action="?page=<?php echo $_GET['page'];
                              ?>&main=cts_input_execute_form&command=<?php echo $command;
                              ?>&plugin=<?php echo $data->plugin_uid;
                              ?>" enctype="multipart/form-data" >
                <div align="center">< 
                <?php
                    if (!$expand)
                    {
                ?>
                <a href="?<?php echo http_build_query($parameters);?>&expand=1" title="More parameters" onclick="load_main_frame(this); return false;">More parameters</a>
                <?php
                    }
                    else
                    {
                    ?>
                <a href="?<?php echo http_build_query($parameters);?>&expand=0" title="Less parameters" onclick="load_main_frame(this); return false;">Less parameters</a>
                    <?
                    }
                ?>
                ></div>
        <table>
            <?php
            
            //Checking for process
            $plugin = Plugin::get_child($data->plugin_uid) . 'Plugin';
            require_once($DIRECTORY['PLUGINS']. $plugin .'.php');
            $plugin = new $plugin();
            
            if(in_array($command, $plugin->daemon_commands))
            {
                ?><input type="hidden" name="daemon_mode" value="true"/><?php
            }
            
            //Not displaying repository list when updating data.
            if($data->produce_data)
            {
                ?>
                    <tr>
                        <td>Repository type:</td>
                <?php
                cts_input_repository_dropbox((object) array('list_repositories' => $data->list_repositories, 'local_repository_path' => $data->local_repository_path, 'repository_type' => $data->repository_type));
                ?>
                    <td></td>
                    <td></td>
                    </tr>
                <?php
            }
            
            //Adding the name input if needed
            $db_plugin = new DatabasePlugin();
            if(array_key_exists($data->plugin_uid, array($db_plugin->uid)))
            {
            ?>
            <tr>
                <td>Entry name:</td>
                <td>
                    <?php
                    
                    $name = 'entry_name';
                    $type = 'text';
                    $value = '';
                    if (@$data->data_uid->uid)
                    {
                        if (@$data->data_uid->value)
                        {
                            $value = $data->data_uid->value;
                        }
                        $value = htmlspecialchars($value);
                    }
                    ?>
                    <input type="<?php echo $type;?>"
                           name="<?php echo $name;?>"
                           id="<?php echo $name;?>"
                           value="<?php echo $value;?>"/>
                </td>
            </tr>
            <?php
            }
    
            $added_params = array();
            $hidden_params = array();
            if (isset($parameters['process_mode']))
            {
                $process_mode_params = unserialize(urldecode($parameters['process_mode']));
            }
    
            foreach ($data->params->$command->params as $param)
            {
                //Hiding process-related parameter
                if(isset($process_mode_params) && $process_mode_params['uid_parameter'] == $param->{META_ATTRIBUTE_NAME} )
                {
                    ?>
                        <input type="hidden" name="<?php echo $param->{META_ATTRIBUTE_NAME};?>" value="&lt;UID&gt;"/>
                    <?php
                }
                //Adding the entry to the parameters without displaying it.
                else if($parameters['mode'] == 'update' && $param->{META_ATTRIBUTE_NAME} == 'entry')
                {
                    $added_params[$param->{META_ATTRIBUTE_NAME}] = false;
                    if (isset($param->default_values))
                    {
                        $param->{META_ATTRIBUTE_VALUE} = $param->default_values;
                    }
                    ?>
                    <input type="hidden"
                           name="<?php echo $param->{META_ATTRIBUTE_NAME};?>"
                           id="<?php echo $param->{META_ATTRIBUTE_NAME};?>"
                           value="<?php echo $param->{META_ATTRIBUTE_VALUE};?>"/>
                    <?php
                }
                else
                {
                    $hidden = false;
                    if (isset($param->hidden))
                        $hidden = $param->hidden;
                    if (!$hidden)
                    {
                        $added_params[$param->{META_ATTRIBUTE_NAME}] = cts_form_enter_data($param, (object)array('list_repositories' => $data->list_repositories, 'local_repository_path' => $data->local_repository_path, 'repository_type' => $data->repository_type), False);
                    }
                    else
                    {
                        array_push($hidden_params, $param);
                    }
                }
            }
            ?>
        </table>
        <table>
        <?php
            //We need to display the default values as hidden fields
            // otherwhise, the checkboxes are not taken into account
            foreach ($hidden_params as $param)
            {
                $added_params[$param->{META_ATTRIBUTE_NAME}] = cts_form_enter_data($param, (object)array('list_repositories' => $data->list_repositories, 'local_repository_path' => $data->local_repository_path, 'repository_type' => $data->repository_type), False,True);
            }
        ?>
        </table>
        <input type="hidden" name="added_params" value="<?php echo urlencode(serialize($added_params));?>"/>
        <?php
            if(isset($parameters['process_mode']))
            {
                ?><input type="hidden" name="process_mode" value="<?php echo urlencode(serialize($parameters['process_mode']));?>"/><?php
            }
        ?>
        <br/>
        <input type="submit" value="Add"/>
    </form>
</div>
    <?php
}
//---------------------------------------------------------------------------

/**
 * @brief Parses the result from a cts_input generated form.
 * @param params: an array filled with the form contents.
 * @return an object with the results from the form.
 */
function cts_input_form_parse($params)
{
    global $DIRECTORY;
    //Processing parameters
    $result = array();
    if(array_key_exists('input', $params))
    {
        $result['input'] = $params['input'];
    }
    if(array_key_exists('uid', $params))
    {
        $result['plugin_uid'] = $params['uid'];
    }
    
    //Veryfiying there is only one command in the input.
    $command = array_keys($result['input']);
    if(count($command)==1)
    {
        $result['command'] = $command[0];
    }
    else
    {
        $msg = 'no command was';
        if(count($command)>1)
        {
            $msg = 'too many commands were';
        }
        ?>
            <br/>Error: <?php echo $msg;?> specified to create data in the following input :<br/><br/>
            <textarea><pre><?php echo $result['input'];?></pre></textarea>
        <?php
        return null;
    }
    
    return (object)$result;
}
//---------------------------------------------------------------------------

/**
 * @brief Wraps a cts_input command inside a daemon.
 * @param command_data: the cts_input formatted command and its parameters.
 * @return A new command data that describes the old command wrapped inside a daemon.
 */
function cts_input_wrap_daemon($command_data, $plugin, $cts_daemon_produced_by=NULL)
{
    $command = $command_data->command;
    $command_data->input[$command]['params']['format'] = 'txt';
    $json_array = call_user_func(array($plugin, $command . '_parse'), $command_data->input[$command]['params']);
    $wrapped_string = cmd_line_from_array($plugin->uid, $json_array);
    
    $produced = @$_SESSION['username'];
    if (array_key_exists($command, $plugin->daemon_produced_by))
        $produced = $command_data->input[$command]['params'][$plugin->daemon_produced_by[$command]];
        
    if (!is_null($cts_daemon_produced_by))
        $produced = $command_data->input[$command]['params'][$cts_daemon_produced_by];
    $command_data->command = 'init';
    unset($command_data->input[$command]);
    $command_data->input[$command_data->command] = array('params' => array(
            'repository_attr' => 'temp',
            'command' => $wrapped_string,
            'produced_by' => $produced,
            'auto_run' => True
    ));
    
    return $command_data;
}
//---------------------------------------------------------------------------

/**
 * @brief Wraps a cts_input command inside a process.
 * @param command_data: the cts_input formatted command and its parameters.
 * @return A new command data that describes the old command wrapped inside a daemon.
 */
function cts_input_wrap_process($command_data, $plugin)
{
    $command = $command_data->command;
    $command_data->input[$command]['params'][$command_data->cts_process_mode['uid_parameter']] = '<UID>';
    
    $json_array = call_user_func(array($plugin, $command . '_parse'), $command_data->input[$command]['params']);
    $wrapped_string = cmd_line_from_array($plugin->uid, $json_array);

    $command_data->command = 'run_command';
    unset($command_data->input[$command]);
    $command_data->input[$command_data->command] = array('params' => array(
            'repository_attr' => 'temp',
            'command' => join(',' ,$wrapped_string),
            'query' => @$command_data->cts_process_mode['query'],
            'type_query' => @$command_data->cts_process_mode['type'],
            'repository_query' => @$command_data->cts_process_mode['repository_query'],
            'list_uids' => @$command_data->cts_process_mode['uids']
    ));
    
    return $command_data;
}

//---------------------------------------------------------------------------

/**
 * @brief Launches the command to create a new data entry using the given parameters.
 * @param command_data: an object containing the plugin, command and parameters to launch.
 * @return the result of the command
 */
function cts_input_launch_command($command_data)
{
    global $DIRECTORY;
    //Finding the plugin that was requested.
    $plugin = Plugin::get_child($command_data->plugin_uid) . 'Plugin';
    require_once($DIRECTORY['PLUGINS'].$plugin.'.php');
    $plugin = new $plugin();
    
    $command = $command_data->command;
    if(isset($command_data->input[$command]['attributes']['repository']))
    {
        $command_data->input[$command]['params']['repository_attr'] = $command_data->input[$command]['attributes']['repository'];
    }
    
    //Wrapping the command inside a process if needed.
    if(isset($command_data->cts_process_mode))
    {
        $command_data->cts_process_mode = unserialize(urldecode($command_data->cts_process_mode));
        $command_data = cts_input_wrap_process($command_data, $plugin);
        $command = $command_data->command;
        $plugin = new ProcessPlugin();
        $command_data->cts_daemon_mode = True;
    }
    
    //Wrapping the command inside a daemon if needed.
    if(@$command_data->cts_daemon_mode)
    {
        $cts_daemon_produced_by = NULL;
        if(@$command_data->cts_daemon_produced_by)
        {
            $cts_daemon_produced_by = $command_data->cts_daemon_produced_by;
        }
        $command_data = cts_input_wrap_daemon($command_data, $plugin, $cts_daemon_produced_by);
        $command = $command_data->command;
        $plugin = new DaemonPlugin();
    }
    
    //Launching the requested command.
    @$output = $plugin->get_data($command_data->command, $command_data->input[$command]['params']);
    //Failing gracefully on error.
    if(isset($output->CTI_PLUGIN_CALL_ERROR))
    {
        echo 'Error calling the command '. $command_data->command . ' of plugin '. Plugin::get_child($command_data->plugin_uid) . '.';
        //Returning the error anyway.
        return $output;
    }
    
    if(!isset($output->type))
    {
        $output->type = META_CONTENT_ATTRIBUTE_TYPE_DATA_UID;
    }
    else if($output->type === CTR_ENTRY_DATA)
    {
        $output->type = META_CONTENT_ATTRIBUTE_TYPE_DATA_UID;
    }
    else if($output->type === CTR_ENTRY_PLUGIN)
    {
        $output->type = META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID;
    }
    
    return $output;
}
//---------------------------------------------------------------------------

/**
 * @brief Returns the data needed to generate the multiple choices popup
 * @param params: an array containing the basic information to call query
 * @return an object containing the date needed for the popup
 */
function cts_input_multiple_choices_get_data($params)
{
    //Processing parameters
    if(array_key_exists('produced_by', $params))
    {
        $result['plugin_id'] = cts_create_uid_visualization($params['produced_by'], CTR_ENTRY_PLUGIN);
    }
    
    //Searching for entries produced by the given plugin
    $query_plugin = new QueryPlugin();
    $query_results = $query_plugin->get_data('select',
                                             prepare_query(
                                                 array('produced_by' => $params['produced_by'],
                                                       'search_query' => '*'
                                                 )
                                              )
                                            );
    
    //Processing results
    $result['array_uid'] = array();
    foreach($query_results->data as $row)
    {
        $result['array_uid'][] = $row->{'entry_info.entry_uid'};
    }
    
    $result['array_uid'] = (object) $result['array_uid'];
    return (object) $result;
    
}
?>
