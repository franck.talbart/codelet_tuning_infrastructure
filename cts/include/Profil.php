<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');

/**
 * 
 * @brief This class manages the CTS profil
 */
class Profil
{
    private $login_uid; // User UID
    private $profil_filename; // Profil filename
    
    /**
     * 
     * @brief Constructor. Initialize the profil.
     * Create the user data directory if not created with chmod 775 and
     * load the profil file.
     * @param login_uid: user UID used to read the right directory
     * @param profil_filename: the profil filename (ex: dashboard)
     */
    function __construct($login_uid, $profil_filename)
    {
        $this->login_uid = $login_uid;
        $this->profil_filename = $profil_filename;

        // Creating the config directory if needed:
        // cts/user-data/{uid}/
        $dname = $this->get_user_data_directory();
        if (!file_exists($dname))
            mkdir($dname, 0775, true);

        // Creating the profil file if needed:
        // cts/user-data/{uid}/profil
        $fname = $this->get_filename();
        if (!file_exists($fname))
        {
            $fp = fopen($fname,"w");
            fclose($fp);
        };
    }

    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return the user data directory
     */
    private function get_user_data_directory()
    {
        global $DIRECTORY;
        return $DIRECTORY['USER_DATA'] . $this->login_uid . '/';
    }
    //---------------------------------------------------------------------------
    
    /**
     * 
     * @return the profil filename
     */
    public function get_filename()
    {
        return $this->get_user_data_directory() . $this->profil_filename;
    }
    
    //---------------------------------------------------------------------------
    
    /**
     * @return the content of the profil file
     */
    public function load_filename($mode = 'r')
    {
        $fname = $this->get_filename();
        if (!file_exists($fname)) return false;
        
        $handle = fopen($fname, $mode);
        if (!$handle) return false;
        
        return $handle;
    }
}
?>
