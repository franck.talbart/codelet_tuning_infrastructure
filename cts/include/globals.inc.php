<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

//From CTI constants

//Types
define('META_CONTENT_ATTRIBUTE_TYPE_DATA_UID', 'DATA_UID');
define('META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID', 'PLUGIN_UID');
define('META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID', 'REPOSITORY_UID');
define('META_CONTENT_ATTRIBUTE_TYPE_TEXT', 'TEXT');
define('META_CONTENT_ATTRIBUTE_TYPE_FILE', 'FILE');
define('META_CONTENT_ATTRIBUTE_TYPE_URL', 'URL');
define('META_CONTENT_ATTRIBUTE_TYPE_COMPLEX', 'COMPLEX');
define('META_CONTENT_ATTRIBUTE_TYPE_EMAIL', 'EMAIL');
define('META_CONTENT_ATTRIBUTE_TYPE_MATRIX', 'MATRIX');
define('META_CONTENT_ATTRIBUTE_TYPE_BOOLEAN', 'BOOLEAN');

//Tags
define('META_ATTRIBUTE_DESC', 'desc');
define('META_ATTRIBUTE_DIRECTORY', 'directory');
define('META_ATTRIBUTE_LONG_DESC', 'long_desc');
define('META_ATTRIBUTE_VALUE', 'value');
define('META_ATTRIBUTE_TYPE', 'type');
define('META_ATTRIBUTE_LIST', 'list');
define('META_ATTRIBUTE_REP', 'repository');
define('META_ATTRIBUTE_PRODUCED_BY', 'produced_by');
define('META_ATTRIBUTE_NAME', 'name');
define('META_ATTRIBUTE_PASSWORD', 'password');
define('META_ATTRIBUTE_MATRIX_COLUMN_NAMES', 'column_names');
define('META_ATTRIBUTE_MATRIX_COLUMN_TYPES', 'column_types');
define('META_ATTRIBUTE_MATRIX_COLUMN_DESCS', 'column_descs');
define('META_ATTRIBUTE_MATRIX_COLUMN_LONG_DESCS', 'column_long_descs');

//UID
define ('UID_NUMBER_OF_MINUS', 4);
define ('UID_NUMBER_OF_SYMBOL', 36);

//enum CTR_DATA_T
define('CTR_ENTRY_DATA',0);
define('CTR_ENTRY_PLUGIN',1);
define('CTR_ENTRY_REPOSITORY',2);

//enum CTR_REP_T
define('CTR_REP_LOCAL',0);
define('CTR_REP_COMMON',1);
define('CTR_REP_TEMP',2);


//Critical error range
define('CTI_PLUGIN_CRITICAL_ERROR_BEGIN', 1);
define('CTI_PLUGIN_CRITICAL_ERROR_END', 13);

//enum CtiPluginCriticalError
define('CTI_PLUGIN_ERROR_SYNTAX',1);
define('CTI_PLUGIN_ERROR_EXCEPT',2);
define('CTI_PLUGIN_ERROR_UNEXPECTED', 3);
define('CTI_PLUGIN_ERROR_CRITICAL_IO', 4);
define('CTI_PLUGIN_ERROR_RUNSCRIPT', 5);
define('CTI_PLUGIN_ERROR_CANT_CREATE_DATA_ENTRY', 6);
define('CTI_PLUGIN_ERROR_UNKNOWN_ENTRY', 7);
define('CTI_PLUGIN_ERROR_INCOMPATIBLE_ENTRY', 8);
define('CTI_PLUGIN_ERROR_INIT_NOT_FOUND', 9);
define('CTI_PLUGIN_ERROR_DEFAULT_REPOSITORY', 10);
define('CTI_PLUGIN_ERROR_DOT_CTR', 11);
define('CTI_PLUGIN_ERROR_COMMAND_NOT_FOUND', 12);
define('CTI_PLUGIN_ERROR_LOCAL_REP_DOESNT_EXISTS', 13);

//Non-critical error range
define('CTI_PLUGIN_ERROR_BEGIN', 247);
define('CTI_PLUGIN_ERROR_END', 255);

//enum CtiPluginError
define('CTI_PLUGIN_ERROR_UNKNOWN_FORMAT', 255);
define('CTI_PLUGIN_ERROR_IO', 254);
define('CTI_PLUGIN_ERROR_INVALID_ARGUMENTS', 253);
define('CTI_PLUGIN_ERROR_WRONG_PASSWORD', 252);
define('CTI_PLUGIN_ERROR_SESSION_EXISTS', 251);
define('CTI_PLUGIN_ERROR_USER_INTERRUPT', 250);
define('CTI_PLUGIN_ERROR_RIGHT', 249);
define('CTI_PLUGIN_ERROR_IMPORT', 248);
define('CTI_PLUGIN_QUERY_NO_RESULT', 247);

//CTS constants
define('DEFAULT_TABLE_SIZE', 20);

?>