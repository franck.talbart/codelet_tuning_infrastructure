<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once("security.inc.php");
require_once("globals.inc.php");
require_once('cts_utils.inc.php');


/**
 * @brief Gets the current CTI version.
 * @return a string containing the current version of cti.
 */
function get_cti_version()
{
    global $CFG;
    global $LINK;
    
    if (isset($_SESSION['username']) && $_SESSION['username'] != '')
    {
        $user = $_SESSION['username'];
        $password = $_SESSION['pwd'];
        $cmd_user = $CFG['cmd_user'].$user.' '.$password;
    }
    else
    {
        $cmd_user = $CFG['no_session'];
    }
    
    $param_cti = " $cmd_user --version";
    $cmd = $CFG['shell'].' '.$CFG['cts_root'].'/'.$CFG['cti_run_script'].$param_cti;
    
    // We check if there is not malicious code inside the URL
    if (check_malicious_code($cmd) == 1)
        return 1;
    
    $return_execute = execute_command_line($cmd);
    display_cts_error($return_execute, '--version');
    //Checking for non-critical errors:
    if ($return_execute['return_code'] != 0)
    {
        ?>
            <div class="block" align=center>
                The error message was:<br/>
                <?php echo $return_execute['error_message'];?><br/>
                The return code was: <?php echo $return_execute['return_code'];?>
            </div>
        <?php 
    }
    return $return_execute['result'];
}
//---------------------------------------------------------------------------

/**
 * @brief Makes an array command from an json array that contains the information about the command.
 * @param plugin: the plugin to call in the command.
 * @param json_array: the array containing the command and its parameter, in the same format as json inputs.
 */
function cmd_line_from_array($plugin, $json_array)
{
    global $CFG;
    
    $cmd_name = array_keys($json_array);
    $cmd_name = $cmd_name[0];
    
    $result = array($CFG['shell'],$CFG['cts_root'].'/'.$CFG['cti_run_script']);
    
    if (isset($_SESSION['username']) && $_SESSION['username'] != '')
    {
        $user = $_SESSION['username'];
        $password = $_SESSION['pwd'];
        $result = array_merge($result, array($CFG['cmd_user'].$user, $password));
    }
    else
    {
        $result[] = $CFG['no_session'];
    }
    
    $result = array_merge($result, array($plugin,$cmd_name));
    foreach($json_array[$cmd_name]['params'] as $param)
    {
        if (gettype($param['value']) === "boolean")
        {
            $param['value'] = ($param['value']) ? 'true' : 'false';
        }
        if (is_array($param['value']))
            $param['value'] = join(",", $param['value']);
        $result[] = '--'.$param['name'].'="'.preg_replace(array('/"/', '/([^\\\\]|^)([<>])/', '/([^\\\\]|^)([()])/', '/([^\\\\]|^) /'), array('\\"', '$1\\\\$2', '$1\\\\\\\\\\\\$2' , '$1\\ '), $param['value']).'"';
    }
    
    if(@$json_array[$cmd_name]['attributes']['repository'])
    {
        $result[] = '--repository_produce='. $json_array[$cmd_name]['attributes']['repository'];
    }

    return $result;
}
//---------------------------------------------------------------------------

/**
 * @brief Execute a plugin with an input file
 * @param plugin: the plugin UID / Alias of the plugin to run
 * @param json_array: the content of the input file in array format
 * @param format: the format of the requested plugin output
 * @return the returned value of the plugin
 */
function execute_plugin_by_file($plugin, $json_array)
{
    global $CFG;
    
    //Encoding the parameter array
    $json = json_encode($json_array);
    
    if (isset($_SESSION['username']) && $_SESSION['username'] != '')
    {
        $user = $_SESSION['username'];
        $password = $_SESSION['pwd'];
        $cmd_user = $CFG['cmd_user'].$user.' '.$password;
    }
    else
    {
        $cmd_user = $CFG['no_session'];
    }
    
    $tmpfname = tempnam("/tmp", "");
    $handle = fopen($tmpfname, "w");
    fwrite($handle, $json);
    fclose($handle);
    
    $param_cti = " $cmd_user $plugin @$tmpfname";
    $cmd = $CFG['shell'].' '.$CFG['cts_root'].'/'.$CFG['cti_run_script'].$param_cti;
    
    // We check if there is not malicious code inside the URL
    if (check_malicious_code($cmd) == 1)
        return 1;
    
    $return_execute = execute_command_line($cmd);
    display_cts_error($return_execute, $plugin, $json_array);
    unlink($tmpfname);
    
    return $return_execute;
}
//---------------------------------------------------------------------------

/**
 * @brief Execute a command
 * @param cmd: the command
 * @param format: the format of the requested plugin output
 * @return an array containing the returned value and the error message
 */
function execute_command_line($cmd)
{
    $descriptorspec = array
    (
       0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
       1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
       2 => array("pipe", "w")  // stderr
    );

    $cmd="export TERM=linux;$cmd";
    $process = proc_open($cmd, $descriptorspec, $pipes, NULL, NULL);
    
    if (is_resource($process))
    {
        // $pipes now looks like this:
        // 0 => writeable handle connected to child stdin
        // 1 => readable handle connected to child stdout
        // 2 => error
        
        fclose($pipes[0]);
        
        $result = stream_get_contents($pipes[1]);
        $error_msg = stream_get_contents($pipes[2]);
        
        fclose($pipes[1]);
        fclose($pipes[2]);
        
        $return_var = proc_close($process);
    }
    
    return array(
        "return_code" => $return_var, 
        "error_message" => $error_msg, 
        "process_id" => $process["pid"], 
        "result"=>$result
    );
}
//---------------------------------------------------------------------------

/**
 * @brief Display a CTS error
 * @param return_execute: an array containing what the plugin returns
 * @param plugin: the plugin
 * @param json_array: the json input file contents if it exists. 
 */
function display_cts_error($return_execute, $plugin, $json_array=false)
{
    global $LINK;
    global $CFG;
    global $DEBUG_PROD;
    if (CTI_PLUGIN_CRITICAL_ERROR_BEGIN <= $return_execute['return_code'] && $return_execute['return_code'] <= CTI_PLUGIN_CRITICAL_ERROR_END)
    {
        ?>
            <div class="block">
                <font color="#FE0101">
                    <p>
                        <strong>
                            CTI "<?php echo $plugin;?>" behaved unexpectedly. Please report this situation 
                            <a href="<?php echo $LINK['redmine'];?>">here</a>.
                        </strong>
                    </p>
                </font>
            </div>
            <div class="block">
        <?php 
        
        if (!$DEBUG_PROD)
        {
            ?>
                <p>
                    CTS Output:<br/>
                    <div align="left">
                        <textarea rows="5" cols="130"><?php echo $return_execute['result'];?></textarea>
                    </div>
                    <br/><hr/>
                </p>
                <p>
                    CTS error:<br/>
                    <div align="left">
                        <textarea rows="10" cols="130"><?php echo $return_execute['error_message'];?></textarea>
                    </div>
                    <br/><hr/>
                </p>
                <p>
                    <p>
                        The returned value of the plugin is 
            <?php 
            echo $return_execute['return_code'];
        }
        
        if($json_array)
        {
            ?>
                <p>
                    Call parameters: <br/>
                    <div align="center">
                        <textarea rows="15" cols="130">
                            <?php print_r($json_array);?>
                        </textarea>
                    </div><br/>
            <?php 
        }
        else
        {
            ?>
            <h1>WARNING: THERE WERE NO CALL PARAMETERS!</h1>
            <?php 
        }
        ?>
                    </p>
                    <div align="center">
                        <textarea rows="15" cols="130">
                            <?php 
                                $log = get_log_file();
                                echo $log;
                            ?>
                        </textarea>
                    </div>
                    <br/>
                </p>
            </div>
        <?php
    }
}

?>
