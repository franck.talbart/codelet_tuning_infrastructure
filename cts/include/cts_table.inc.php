<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once('plugins.inc.php');
require_once('globals.inc.php');
require_once('cts_create.inc.php');
require_once('cts_utils.inc.php');

/**
 * 
 * @brief Parse the parameter string into an array
 * @param id: the id of the table we want to get parameters of
 * @param count_lines: the number of lines of the table, header excluded
 * @param page_size: the number of lines for each page
 * @return the array
 */
function cts_table_parameter_to_array($id, $count_lines, $page_size)
{
    $pagenum = 1;
    $size = $page_size;
    $sorted = 0;
    
    if (array_key_exists('size_'.$id, $_GET))
    {
        $size = $_GET['size_'.$id];
    }
    //Preventing user mis-input.
    if ($size <1){
        $pages = 1;
    }
    else
    {
        //Calculating number of pages
        $pages = (int) ( ceil( $count_lines / $size) );
        //In the case of an empty table, avoiding null current page value:
        if($pages<1)
        {
            $pages = 1;
        }
    }
    if (array_key_exists('current_'.$id, $_GET))
    {
        $pagenum = $_GET['current_'.$id];
        //Current page should not be lower than 1 or higher than the total.
        if($pagenum < 1)
        {
            $pagenum = 1;
        }
        else if($pagenum > $pages)
        {
            $pagenum = $pages;
        }
    }
    if (array_key_exists('sorted_'.$id, $_GET))
    {
        $sorted = $_GET['sorted_'.$id];
    }
    
    return array(
        'current' => $pagenum,
        'size' => $size,
        'sorted' => $sorted,
        'pages' => $pages,
        'total' => $count_lines,
    );
}
//---------------------------------------------------------------------------

/**
 * 
 * @brief Convert an array to a parameter string
 * @param changes: the array of new/changed parameters
 * @param id: the id of the table
 * @return the parameter string
 */
function cts_table_array_to_parameter($changes, $id)
{
    $result = '';
    $base_array = $_GET;
    
    $get_vars = $changes;
    unset($get_vars['pages']);//We don't need to display the number of pages.
    
    //Updating the copy of GET args
    foreach($get_vars as $key => $val)
    {
        $base_array[$key.'_'.$id] = $val;
    }
    
    //Preparing the string containing all arguments.
    foreach($base_array as $key => $val)
    {
        if(is_array($val))
        {
            foreach($val as $elem)
            {
                $result .= '&' . $key . '[]=' . $elem;
            }
        }
        else
        {
            $result .= '&' . $key . '=' . $val;
        }
    }
    
    
    return '?' . trim($result, '&');
}
//---------------------------------------------------------------------------

/**
 * 
 * @brief Sort the table
 * @param content_table: content of the table
 * @param id_field: The id of the field to sort
 * @param ascending: sort by ascending order. Defaults to true, false means descending.
 * @return the sorted table
 */
function cts_table_sort_table($content_table, $id_field)
{
    $ascending = True;
    if ($id_field < 0)
    {
        $ascending = False;
        $id_field = abs($id_field);
    }
    $actual_index = $id_field - 1;
    $result = $content_table;
    //If there is at least 2 lines to sort (without the header).
    if(count($result) > 2)
    {
        array_shift($result); //avoiding header.
        $code = "return -1;";
        switch($result[0][$actual_index]['type'])
        {
        case 'INTEGER':
        case 'FLOAT':
            $code = "if (\$a['$actual_index']['value'] < \$b['$actual_index']['value']) return -1;
                     if (\$a['$actual_index']['value'] > \$b['$actual_index']['value']) return 1;
                     return 0;";
            break;
        case 'FILE':
            $code = "return strnatcmp(getTextBetweenTags(\$a['$actual_index']['value'], 'a'), getTextBetweenTags(\$b['$actual_index']['value'], 'a'));";
            break;
        case 'URL':
        case 'DATE':
        case 'EMAIL':
        case 'TEXT':
        case 'DATA_UID':
        case 'PLUGIN_UID':
        case 'REPOSITORY_UID':
            $code = "return strnatcmp(\$a['$actual_index']['value'], \$b['$actual_index']['value']);";
            break;
        case 'COMPLEX':
        default:
            $code = "return -1;";
            break;
        }
        
        usort($result, create_function('$a,$b', $code));
        
        if (!$ascending)
        {
            $result = array_reverse($result);
        }
        array_unshift($result, $content_table[0]); //Adding header back
    }
    return $result;
}//---------------------------------------------------------------------------

/**
 * 
 * @brief Display a table in HTML format
 * @param id_table: The id table (integer) on the webpage
 * @param content_table: the content of the table (an array of arrays), 
 *               including the header at the first position in the array.
 * @param page_size: The size of a table when created
 * @param config_table: 
 * @param $widget_mode: to display a different header in widget mode
 */
function cts_table_create($id_table, $content_table, $page_size=DEFAULT_TABLE_SIZE, $config_table=False, $widget_mode=False)
{
    ?>
        <div id="table_<?php echo $id_table;?>">
    <?php 
    if (!$config_table)
    {
        $config_table = cts_table_parameter_to_array($id_table, count($content_table)-1, $page_size);
        
        //Sort the table as asked.
        if ($config_table['sorted'] != 0)
        {
            $content_table = cts_table_sort_table($content_table, $config_table['sorted']);
        }
        
        if($config_table['size'] < 0)
        {
            $first_line = 1;
            $last_line = $config_table['total']+1;
        }
        else
        {
            $first_line = $config_table['size'] * ($config_table['current']-1) + 1; // +1 avoid the header
            $last_line = $first_line + $config_table['size'];
            $last_line = min($last_line, ($config_table['total']+1));
        }
    }
    else
    {
        $first_line = 1; //avoid the header at 0
        $last_line = $first_line + $config_table['size'];
        $last_line = min($last_line, ($config_table['total']+1)- ($config_table['size'] * ($config_table['current']-1)));
    }
    
    if ($first_line > ($config_table['total']+1) || $first_line < 0 || $last_line < 0 || $last_line < $first_line || $last_line > ($config_table['total']+1))
    {
        echo 'Size error in CTS table';
    }
    
    ?>
        <div align="center">
            <?php buttons_table($config_table, $id_table, $last_line);?>
        </div>
        <table class="cts_table">
            <thead>
                <tr>
    <?php 
    $cpt = 1;
    foreach ($content_table[0] as $element)
    {
        $temp_config_table = $config_table;
        $tmp = $temp_config_table['sorted'];
        $temp_config_table['sorted'] = $cpt;
        $sorted_arrow = '';
        if (abs($tmp) == $cpt)
        {
            $temp_config_table['sorted'] = -$tmp;
            if ($tmp > 0)
            {
                $sorted_arrow = '&nbsp;&uarr;';
            }
            else
            {
                $sorted_arrow = '&nbsp;&darr;';
                $temp_config_table['sorted'] = 0;
            }
        }
        ?>
            <th>
                <b>
                    <?php
                        if ($widget_mode)
                        {
                            echo cts_create_link(
                                    $element, 
                                    'ajax/dashboard.php?widget=' . $id_table . '&sorted=' . $temp_config_table['sorted'],
                                    False,
                                    'onclick="cts_dashboard.sort_widget('.$id_table.', '.$temp_config_table['sorted'].'); return false;"'
                                ) . $sorted_arrow;
                        }
                        else
                        {
                            echo cts_create_link(
                                    $element, 
                                    cts_table_array_to_parameter($temp_config_table, $id_table),
                                    False,
                                    'onclick="load_main_frame(this); return false;"'
                                ) . $sorted_arrow;
                        }
                    ?>
                </b>
            </th>
        <?php
        $temp_config_table[$id_table]['sorted'] = $tmp;
        $cpt += 1;
    }
    
    ?>
            </tr>
        </thead>
        <tbody>
    <?php 
    
    //Rows display
    if ($config_table['total'] == 0) //if the table is empty (only the header is present)
    {
        ?>
            <tr>
                <td colspan="<?php echo count($content_table[0]);?>">No data</td>
            </tr>
        <?php 
    }
    //Starting to display rows
    for ($index=$first_line; $index<$last_line; $index++)
    {
        ?>
            <tr>
        <?php 
        foreach ($content_table[$index] as $elt)
        {
            $display_val = '';
            switch($elt['type'])
            {
                case 'DATA_UID':
                case 'PLUGIN_UID':
                case 'REPOSITORY_UID':
                        $display_val = cts_create_visualization_type(
                                        $elt['type'],
                                        $elt['value']
                                    );
                    break;
                default:
                    if (is_null($elt['value']))
                        $elt['value'] = 'Null';
                    $display_val = utf8_encode((string)($elt['value']));
                    break;
            }
            
            
            ?>
                <td><?php echo $display_val;?></td>
            <?php 
        }
        ?>
            </tr>
        <?php 
    }
    
    ?>
                </tbody>
            </table>
            <div align="center">
                <?php buttons_table($config_table, $id_table, $last_line);?>
            </div>
        </div>
    <?php 
}
//---------------------------------------------------------------------------

function buttons_table($config_table, $id_table, $last_line)
{
    //Displaying previous pages links.
    if ( $config_table['current'] > 1 )
    {
        $temp = $config_table['current'];
        $config_table['current'] = 1;
        echo cts_create_link_img('img/begin.png', cts_table_array_to_parameter($config_table, $id_table),
                'onclick="load_main_frame(this); return false;"');
        $config_table['current'] = $temp - 1;
        echo cts_create_link_img('img/previous.png', cts_table_array_to_parameter($config_table, $id_table),
                'onclick="load_main_frame(this); return false;"');
        $config_table["current"] = $temp;
    }
    
    //Displaying page count.
    if ($config_table['pages'] > 1)
    {
        ?>
            <small>
                <?php echo $config_table['current'];?>/<?php echo $config_table['pages'];?>
            </small>
        <?php 
    }
    
    //Displaying next pages links.
    if ($config_table['current'] < $config_table['pages'])
    {
        $config_table['current'] += 1;
        echo cts_create_link_img('img/next.png', cts_table_array_to_parameter($config_table, $id_table),
                'onclick="load_main_frame(this); return false;"');
        $config_table['current'] = $config_table['pages'];
        echo cts_create_link_img('img/end.png', cts_table_array_to_parameter($config_table, $id_table),
                'onclick="load_main_frame(this); return false;"');
    }
}

?>
