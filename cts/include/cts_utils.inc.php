<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once('plugins.inc.php');
require_once('globals.inc.php');
require_once('query.inc.php');
require_once('security.inc.php');
require_once($DIRECTORY['PLUGINS'].'AliasPlugin.php');


/**
 * @brief Returns the value of the given html tag in a string
 * @param string: the string containing the html tag
 * @param tagname: the name of the tag which value should be returned
 * @return the value of the given html tag in the given string.
 */
function getTextBetweenTags($string, $tagname)
{
    $pattern = "#<$tagname".'[^>]*>(.*)<\/'."$tagname>#";
    preg_match($pattern, $string, $matches);
    return $matches[1];
}
//---------------------------------------------------------------------------

/**
 * @brief Return the content of the log file
 * @return log file
 */
function get_log_file()
{
    $line = '';
    global $CFG;

    $file = $CFG['cti_root'].'/cfg/cti.log';
    $handle = fopen($file, 'r');
    fseek($handle, -10240, SEEK_END);
    fgets($handle);// we go to the next full line
    $data = fgets($handle);
    $result = '';
    while($data)
    {
        $result .= $data;
        $data = fgets($handle);
    }
    return $result;
}
//---------------------------------------------------------------------------

/**
 * @brief Manage the cookies
 */
function manage_cookies()
{

    if (isset($_COOKIE['username']) && trim($_COOKIE['username']) == '')
        destroy_cookies();

    if (isset($_COOKIE['username']) && isset($_COOKIE['pwd']) && $_COOKIE['username']!= '')
    {
        $_SESSION['username'] = $_COOKIE['username'];
        $_SESSION['pwd'] = $_COOKIE['pwd'];
        $_SESSION['login_uid'] = @$_COOKIE['login_uid'];
    }

    if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['remember']) && login_user($_POST['username'], md5($_POST['password'])))
    {
        setcookie('username', $_POST['username'], time()+60*60*10);
        setcookie('pwd', md5($_POST['password']), time()+60*60*10);

        $user_uid = get_user_uid($_POST['username'], $_POST['username'], md5($_POST['password']));
        setcookie('login_uid', $user_uid, time()+60*60*10);
    }

    if (isset($_GET['destroy_cookie']))
    {
        destroy_cookies();
    }
}
//---------------------------------------------------------------------------

/**
 * @brief Destroy the cookies
 */
function destroy_cookies()
{
    //Emptying cookie values and setting them for instant expiration.
    setcookie('username', '', 0);
    setcookie('pwd', '', 0);
    setcookie('login_uid', '', 0);
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Check that a given string is an array of 36th characters.
 * @param uid: the uid
 * @return true if it uses the good format, false otherwise
 */
function cts_is_UID($uid)
{
    if (!is_string($uid))
        return false;

    if ($uid == NULL)
        return false;

    if(strlen($uid) != UID_NUMBER_OF_SYMBOL)
        return false;

    // Check if "-"  at the following positions: 9 14 19 24
    foreach (array(8,13,18,23) as $val)
    {
        if ($uid[$val] != '-')
            return false;
    }
    return true;
}

//---------------------------------------------------------------------------

/**
 * Searches and returns the uids corresponding to a given list of data aliases.
 * @param alias_list: the data aliases list
 * @return the uids if they are all found, null else
 */ 
function cts_get_data_uid($aliases_list)
{
    $alias_plugin = new AliasPlugin();
    $info = $alias_plugin->get_data('get_data_uid', array('uid_list' => $aliases_list));
    if(!property_exists($info, 'CTI_PLUGIN_CALL_ERROR'))
    {
        return $info->uid_list;
    }
    return null;
}

//---------------------------------------------------------------------------

/**
 *
 * @brief Returns the variable part of an url from a given array.
 * @param array: the array containing the parameters of the url to make
 * @return the url param string
 */
function cts_url_from_array($params_array)
{
    $result = '';
    foreach($params_array as $param => $content)
    {
        $result .= '&' . $param . '=' . $content;
    }
    $result = ltrim($result, '&');
    $result = '?' . $result;
    return $result;
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Joins given paths in parameters.
 * @param strings: an array of strings to join in a path.
 * @return The strings joined by a '/', forming a path.
 */
function cts_make_path($strings) {
    $paths = array_filter($strings);
    //If there is nothing left, the arguments were invalid.
    if(!$paths)
    {
        return 'Invalid Path';
    }
    $paths = array_map(create_function('$p', 'return trim($p, "/");'), $paths);
    return '/' . join('/', $paths);
}
//---------------------------------------------------------------------------

/**
 * @brief add the attributes to the link
 * @param link: the link to modify
 * @param attributes: attributes to add
 * @return the new link
 */
function add_attributes_to_link($link, $attributes)
{
    return preg_replace('#(<a[^>]*href=\"*\"[^>]*)>#', '\1 '.$attributes.'>', $link);
}
//---------------------------------------------------------------------------

/**
 * @brief Gives the default include for the main category page, and arranges $_GET accordingly
 * @param page: main category page
 * @return the name of the main page
 */
function default_include($page='')
{
    global $CFG, $DIRECTORY;

    $main_page = '';

    switch ($page)
    {
        case 'administration':
            require_once($DIRECTORY['PLUGINS'].'UserPlugin.php');
            $plugin = new UserPlugin();
            $main_page = 'query';
            $_GET['search_query'] = '*';
            $_GET['produced_by'] = $plugin->uid;
            break;

        case 'repositories_summary':
            $main_page = 'list_local_repositories';
            break;

        case 'codelets':
            require_once($DIRECTORY['PLUGINS'].'ApplicationPlugin.php');
            $plugin = new ApplicationPlugin();
            $_GET['produced_by'] = $plugin->uid;
            $_GET['command'] = 'init';
            $_GET['mode'] = 'add';
            $main_page = 'cts_input_form';
            break;

        case 'architecture':
            require_once($DIRECTORY['PLUGINS'].'PlatformPlugin.php');
            $plugin = new PlatformPlugin();
            $_GET['produced_by'] = $plugin->uid;
            $_GET['command'] = 'init';
            $_GET['mode'] = 'add';
            $main_page = 'cts_input_form';
            break;

        case 'files':
            require_once($DIRECTORY['PLUGINS'].'FilePlugin.php');
            $plugin = new FilePlugin();
            $_GET['produced_by'] = $plugin->uid;
            $_GET['command'] = 'init';
            $_GET['mode'] = 'add';
            $main_page = 'cts_input_form';
            break;

        case 'experiments':
            require_once($DIRECTORY['PLUGINS'].'MaqaoPerfPlugin.php');
            $plugin = new MaqaoPerfPlugin();
            $_GET['produced_by'] = $plugin->uid;
            $_GET['command'] = 'init';
            $_GET['mode'] = 'add';
            $main_page = 'cts_input_form';
            break;

        default:
            break;
    }
    return $main_page;
}

//---------------------------------------------------------------------------

/**
 * @brief Convert an object to an array
 * @param object: the object to convert
 * @return the array
 */
function object_to_array($object)
{
    if (is_object($object))
    {
        $object = get_object_vars($object);
    }

    if (is_array($object))
    {
        return array_map(__FUNCTION__, $object);
    }
    else
    {
        return $object;
    }
}

//---------------------------------------------------------------------------

/**
 * @brief Display the corresponding viewer of a CTI entry
 * @param info: set of results returned by a plugin
 * @return 1 if successn error otherwhise
 */
function view_entry($info)
{
    $_GET = array();
    $_POST = array();
    global $DIRECTORY;
    if(@$info->new_uid)
    {
        switch ($info->type)
        {
            case META_CONTENT_ATTRIBUTE_TYPE_DATA_UID:
                $_GET['data_uid'] = $info->new_uid;
                require_once($DIRECTORY['PAGES']. 'view_data.php');
                break;
            case META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID:
                $_GET['plugin_uid'] = $info->new_uid;
                require_once($DIRECTORY['PAGES']. 'view_plugin.php');
                break;
        }
    }
    else if(@$info->daemon_uid)
    {
        $_GET['data_uid'] = $info->daemon_uid;
        require_once($DIRECTORY['PAGES']. 'view_data.php');
    }
    else
    {
        return 'Entry could not be created. UID is None.' .
            "\n" . @$info->output;
    }
    
    return 1;
}

?>
