<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once("plugins.inc.php");
require_once("globals.inc.php");
require_once("cts_utils.inc.php");
require_once($DIRECTORY['PLUGINS'].'AliasPlugin.php');


/**
 *
 * @brief Create a link in HTML
 * @param name name: of the link
 * @param value value: of the link
 * @param new_window: if true, the target is _blank
 * @param attributes attributes: of the link
 * @return the HTML code
 */
function cts_create_link($name, $value, $new_window=False, $attributes="")
{
    $result = '<a title="' . htmlspecialchars($name) . '" href="' . htmlspecialchars($value);
    if ($new_window)
    {
        $result .= '" target="_blank';
    }
    $result .= '"';
    if (!empty($attributes))
    {
        $result .= ' ' . strip_tags($attributes);
    }
    $result .= '>' . htmlspecialchars($name) . '</a>';
    return $result;
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Create a link in HTML with a picture
 * @param img: filename of the picture
 * @param value: value of the link
 * @param attributes: attributes of the link
 * @return the HTML code
 */
function cts_create_link_img($img, $value, $attributes="")
{
    $result = '<a href="' . htmlspecialchars($value) . '"';
    if (!empty($attributes))
    {
        $result .= ' ' . strip_tags($attributes);
    }
    $result .= '><img border="0" src="' .
               htmlspecialchars($img) . '"/></a>';
    return $result;
}
//---------------------------------------------------------------------------

/**
 *
 * Returns "missing-link" style colored text.
 * @param name: the text to be colored
 * @return the colored text
 */
function cts_create_missing_link($name)
{
    return '<font color="#FE0101">' .
        htmlspecialchars($name) .
        '</font>';
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Returns user friendly format for a given UID
 * @param uid: the uid to visualize
 * @param data_type: data or plugin
 * @param alias: the alias if we already have it, will be searched if not provided
 * @return a string representing the name that should be displayed
 */
function cts_create_uid_visualization($uid, $data_type, $alias=NULL)
{
    if(!($data_type === CTR_ENTRY_REPOSITORY || $data_type === CTR_ENTRY_DATA ||$data_type === CTR_ENTRY_PLUGIN))
    {
        return 'Invalid Entry Type';
    }
    if(!$alias)
    {
        if($data_type == CTR_ENTRY_REPOSITORY)
        {
            //Requesting name of given plugin uid, using the name
            $alias_plugin = new AliasPlugin();
            $result = $alias_plugin->get_data('get_repository_alias', array('uid' => $uid), true);
            if(@$result->CTI_PLUGIN_CALL_ERROR)
            {
                if($result->CTI_PLUGIN_CALL_ERROR->return_code == CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                {
                    return 'WARNING: UID DOES NOT EXIST';
                }
            }
            else
            {
                $alias = $result->alias;
            }
        }
        else if ($data_type == CTR_ENTRY_DATA)
        {
            //Requesting alias of given data uid
            $alias_plugin = new AliasPlugin();
            $result = $alias_plugin->get_data('get_data_alias', array('uid' => $uid), true);
            if(@$result->CTI_PLUGIN_CALL_ERROR)
            {
                if($result->CTI_PLUGIN_CALL_ERROR->return_code == CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                {
                    return 'WARNING: UID DOES NOT EXIST';
                }
            }
            else
            {
                $alias = $result->alias;
            }
        }
        else if ($data_type == CTR_ENTRY_PLUGIN)
        {
            //Requesting alias of given data uid
            $alias_plugin = new AliasPlugin();
            $result = $alias_plugin->get_data('get_plugin_alias', array('uid' => $uid), true);
            if(@$result->CTI_PLUGIN_CALL_ERROR)
            {
                if($result->CTI_PLUGIN_CALL_ERROR->return_code == CTI_PLUGIN_ERROR_INVALID_ARGUMENTS)
                {
                    return 'WARNING: UID DOES NOT EXIST';
                }
            }
            else
            {
                $alias = $result->alias;
            }
        }
    }
    //Once we've done our research, we select the output.
    $result = 'No Alias';
    if ($alias)
    {
        $result = $alias;
    }

    return $result;
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Return a link to the viewer depending of the type
 * @param type: type of the entry
 * @param entry_uid: the UID of the entry
 * @param alias: the alias of the entry, or the text that will contain the link
 * @param new_window: if true, the target of the link is _blank
 * @return the HTML code
 */
function cts_create_visualization_type($type, $entry_uid, $alias='', $new_window=False, $concat='')
{
    if (is_null($entry_uid))
            return 'Null';
    
    if ($type == META_CONTENT_ATTRIBUTE_TYPE_DATA_UID)
    {
        if (!cts_is_UID($entry_uid))
        {
            return cts_create_missing_link('Invalid UID');
        }
        if ($alias=='')
        {
            $alias = cts_create_uid_visualization($entry_uid, CTR_ENTRY_DATA);
            
            if ($alias == 'WARNING: UID DOES NOT EXIST')
            {
                return cts_create_missing_link('Missing entry');
            }
            
            $alias = htmlspecialchars($alias);
        }
        return cts_create_link($alias, '?page=repositories_summary&main=view_data&data_uid='.$entry_uid.$concat,
                $new_window, 'onclick="load_main_frame(this); return false;"');
    }
    else if ($type == META_CONTENT_ATTRIBUTE_TYPE_PLUGIN_UID)
    {
        if (!cts_is_UID($entry_uid))
        {
            return cts_create_missing_link('Invalid UID');
        }
        if($alias=='')
        {
            $alias = cts_create_uid_visualization($entry_uid, CTR_ENTRY_PLUGIN);
            if ($alias == 'WARNING: UID DOES NOT EXIST')
            {
                return cts_create_missing_link('Missing entry');
            }
        }
        return cts_create_link($alias, '?page=repositories_summary&main=view_plugin&plugin_uid='.$entry_uid.$concat,
                $new_window, 'onclick="load_main_frame(this); return false;"');
    }
    else if ($type == META_CONTENT_ATTRIBUTE_TYPE_REPOSITORY_UID)
    {
        //Non-local repository
        if(in_array($alias, array(CTR_REP_COMMON, CTR_REP_TEMP)))
        {
            return cts_create_link($entry_uid, '?page=repositories_summary&main=query&search_query=*&repository_type='.$alias.$concat,
                    False, 'onclick="load_main_frame(this); return false;"');
        }
        //Local repository
        else
        {
            if (!cts_is_UID($entry_uid))
            {
                return cts_create_missing_link('Invalid UID');
            }
            return cts_create_link(
                cts_create_uid_visualization($entry_uid, CTR_ENTRY_REPOSITORY),
                '?page=repositories_summary&main=query&search_query=*&repository_type='.$entry_uid.$concat,
                $new_window,
                'onclick="load_main_frame(this); return false;"'
            );
        }
    }
    else if ($type == META_CONTENT_ATTRIBUTE_TYPE_TEXT)
    {
        if ($alias != '')
            return htmlspecialchars($alias);
        return htmlspecialchars($entry_uid);
    }
    else if ($type == META_CONTENT_ATTRIBUTE_TYPE_FILE)
    {
        return cts_create_link($alias, '?page=files&main=view_file&data_uid='.$entry_uid.'&filename='.$alias.$concat);
    }
    else if ($type == META_CONTENT_ATTRIBUTE_TYPE_URL)
    {
        return cts_create_link($alias, $entry_uid, $new_window);
    }
    else if ($type == META_CONTENT_ATTRIBUTE_TYPE_EMAIL)
    {
        return cts_create_link($alias, 'mailto:'.$entry_uid, $new_window);
    }
    return htmlspecialchars($entry_uid);
}

?>
