<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once('cts_utils.inc.php');
require_once('cts_create.inc.php');
require_once($DIRECTORY['PLUGINS'].'ListPlugin.php');
require_once($DIRECTORY['PLUGINS'].'ViewPlugin.php');
require_once($DIRECTORY['PLUGINS'].'AliasPlugin.php');

/**
 *
* @brief return the HTML code for a delete button
* @param uid_to_delete: the entry / repository to delete
* @param type: "entry" or "repository"
* @return the HTML code
*/
function cts_form_delete_button($uid_to_delete, $type)
{
    global $DIRECTORY;
    $_GET['page'] = htmlspecialchars(@$_GET['page']);
    $uid_to_delete = htmlspecialchars($uid_to_delete);
    $type = htmlspecialchars($type);

    $params = 'page='.$_GET['page'] .
        '&main=delete' .
        '&type='.$type;
    if ($type == 'entry')
        $params .='&entry='.$uid_to_delete;
    if ($type == 'repository')
        $params .='&repository='.$uid_to_delete;

    $delete = '<a href="?'. $params .'" onclick="if (confirm(\'Are you sure?\')) ' .
                    'load_main_frame(\'' . $params . '\', \'' . http_build_query($_GET) . '\'); return false;">' .
                  '<img border="0" src="'. $DIRECTORY['IMG'] .'delete_button.png" title="Delete" alt="Delete"/>'.
              '</a>';

    return $delete;
}
//---------------------------------------------------------------------------

/**
 *
 * @brief check if the add button is disabled or not
 * @param uid_plugin: the plugin UID
 * @param command: the command used to generate the form
 * @return true if the add button is disabled, false otherwise.
 */
function cts_form_check_add_button_disabled($uid_plugin, $command='init')
{
    $_GET['page'] = htmlspecialchars(@$_GET['page']);
    $uid_plugin = htmlspecialchars($uid_plugin);
    $command = htmlspecialchars($command);

    //Using the view plugin to get the possible inputs
    $view_plugin = new ViewPlugin();
    $json = $view_plugin->get_data('plugin', array('plugin_uid' => $uid_plugin));

    if (@$json->CTI_PLUGIN_CALL_ERROR)
    {
        echo 'Could not verify that command '. $command . ' existed.' . "\n";
    }

    //Disabling button if the command doesn't exist.
    if (!property_exists($json->output_file, $command))
    {
        return true;
    }
    else
    {
        return false;
    }
}
//---------------------------------------------------------------------------

/**
 *
 * @brief return the HTML code for a add button
 * @param uid_plugin: the plugin UID
 * @param command: the command used to generate the form
 * @return the HTML code
 */
function cts_form_add_button($uid_plugin, $command='init')
{
    if (cts_form_check_add_button_disabled($uid_plugin, $command))
    {
        $dis_attr = '" disabled="disabled';
    }
    else
    {
        $dis_attr = '';
    }

    $params = 'page='.$_GET['page'] .
        '&main=cts_input_form&mode=add' .
        '&produced_by='.$uid_plugin .
        '&command='.$command;

    $add_data= '<form method="GET" onsubmit="load_main_frame(\'' . $params . '\'); return false;">' . "\n" .
        '<input type="hidden" name="page" value="'. $_GET['page'] . '"/>' . "\n" .
        '<input type="hidden" name="main" value="cts_input_form"/>' . "\n" .
        '<input type="hidden" name="mode" value="add"/>' . "\n" .
        '<input type="hidden" name="produced_by" value="' . $uid_plugin . '"/>' . "\n" .
        '<input type="hidden" name="command" value="'. $command .'"/>' . "\n" .
        '<input type="submit" value="Add data' . $dis_attr . '"/></form>' . "\n";
    return $add_data;
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Echoes the entry move form in html.
 * @param uid: the uid of the entry
 * @param repository_type: the type of repository that already contains the entry
 * @param data_type: the type of data the entry is
 */
function cts_form_move_to_repository($uid, $repository_type)
{
    $_GET['page'] = htmlspecialchars(@$_GET['page']);
    $uid = htmlspecialchars($uid);
    $repository_type = htmlspecialchars($repository_type);

    $list_plugin = new ListPlugin();
    ?>
        <tr>
            <td>
                <form method="GET" onsubmit="load_main_frame('page=<?php echo $_GET['page'];?>&main=move&repository='+$('select[name=repository]').val()+'&entry=<?php echo $uid;?>', '<?php echo http_build_query($_GET);?>'); return false;">
                    <input type="hidden" name="page" value="<?php echo $_GET['page'];?>"/>
                    <input type="hidden" name="main" value="move"/>
            </td>
            <td>
                <select name="repository">
    <?php

    if ($repository_type != CTR_REP_COMMON)
    {
        ?>
            <option value="<?php echo CTR_REP_COMMON;?>">Common</option>
        <?php
    }
    if ($repository_type != CTR_REP_TEMP)
    {
        ?>
            <option value="<?php echo CTR_REP_TEMP;?>">Temp</option>
        <?php
    }

    //Calling the list of local repositories
    $cmd_result = $list_plugin->get_data('local_repositories');
    if(!@$cmd_result->CTI_PLUGIN_CALL_ERROR)
    {
        foreach (@$cmd_result as $repos)
        {
            if ($repository_type != $repos->path)
            {
                ?>
                    <option value="<?php echo $repos->uid;?>">
                <?php
                $alias_rep = $repos->alias;

                if (!$alias_rep)
                {
                    $alias_rep = $repos->uid;
                }
                echo htmlspecialchars($alias_rep);
                ?>
                    </option>
                <?php
            }
        }
    }
    ?>
                    </select>
                </td>
                <input type="hidden" name="entry" value="<?php echo $uid;?>"/>
                <td>
                    <input type="submit" value="Move"/>
                </td>
            </tr>
                </form>
    <?php
}
//---------------------------------------------------------------------------

/**
 *
 * @brief Echoes the html form for changing the alias of an entry
 * @param uid: the entry uid
 * @param data_type: the entry data type
 */
function cts_form_change_alias($uid, $data_type)
{
    //Invalid data type case
    if(!in_array($data_type, array(CTR_ENTRY_DATA, CTR_ENTRY_PLUGIN)))
    {
        echo 'Invalid data type';
        return;
    }
    $alias_plugin = new AliasPlugin();

    //Initialising the variable in case no result is given.
    $alias = '';

    if ($data_type == CTR_ENTRY_DATA) //Data case
    {
        $result = $alias_plugin->get_data('get_data_alias', array('uid' => $uid));
        if(!@$result->CTI_PLUGIN_CALL_ERROR)
        {
            $alias = @$result->alias;
        }
    }
    else if($data_type == CTR_ENTRY_PLUGIN) //Plugin case
    {
        $result = $alias_plugin->get_data('get_plugin_alias', array('uid' => $uid));
        if(!@$result->CTI_PLUGIN_CALL_ERROR)
        {
            $alias = @$result->alias;
        }
    }

    if (!$alias)
    {
        $alias = '';
    }
    ?>
    <?php
        if($data_type == CTR_ENTRY_PLUGIN)
        {
    ?>
        <form method="POST" action="?page=repositories_summary&main=view_plugin&plugin_uid=<?php echo $uid;?>" onsubmit="load_main_frame('page=repositories_summary&main=view_plugin&plugin_uid=<?php echo $uid;?>', '', false, 'POST', 'alias='+$('input[name=alias]').val()); return false;">
    <?php
         }
        else
        {
    ?>
        <form method="POST" action="?page=repositories_summary&main=view_data&data_uid=<?php echo $uid;?>" onsubmit="load_main_frame('page=repositories_summary&main=view_data&data_uid=<?php echo $uid;?>', '', false, 'POST', 'alias='+$('input[name=alias]').val()); return false;">
    <?php
        }
    ?>
        <table>
            <tr>
                <td>
                    <input type="text" name="alias" value="<?php echo htmlspecialchars($alias);?>"/>
                </td>
                <td>
                    <input type="submit" value="Update"/>
                </td>
            </tr>
        </table>
    </form>
    <?php
}

//---------------------------------------------------------------------------

/**
 *
 * Echoes a set of <option> tags for all the repositories.
 * @param name: changes the name attribute of the select tag.
 */
function cts_form_list_repositories($name='repository_type')
{
    //Hardcoded Repositories
    ?>
        <select name="<?php echo $name;?>">
            <option selected value="all">All</option>
            <option value="<?php echo CTR_REP_COMMON;?>">Common</option>
            <option value="<?php echo CTR_REP_TEMP;?>">Temp</option>
    <?php

    //Local Repositories
    $list_plugin = new ListPlugin();
    $cmd_result = $list_plugin->get_data('local_repositories');
    if(!@$cmd_result->CTI_PLUGIN_CALL_ERROR)
    {
        foreach (@$cmd_result as $repos)
        {
            $display = $repos->uid;
            if (@$repos->alias)
            {
                $display = $repos->alias;
            }
            ?>
                <option value="<?php echo $repos->uid;?>"><?php echo $display;?></option>
            <?php
        }
    }
    ?>
        </select>
    <?php
}

?>
