<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit


require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['PLUGINS'].'QueryPlugin.php');

/**
 * @brief Get UIDs result from a query 
 * @param query: the query
 * @param type: the entry type
 * @param user: specify a user different from the session's
 * @param password: specify a password different from the session's
 * @return an array of uids
 */
function user_query($query, $type_q, $user='', $password='')
{
    //Saving old ident data to restore it.
    $temp_ident = array();
    if(isset($_SESSION['username']))
    {
        $temp_ident['u'] = $_SESSION['username'];
        $temp_ident['p'] = $_SESSION['pwd'];
    }
    
    if($user != '')
    {
        $_SESSION['username'] = $user;
        $_SESSION['pwd'] = $password;
    }
    
    $query_plugin = new QueryPlugin();
    $result = $query_plugin->get_data('select', 
                                      prepare_query(
                                          array(
                                                'search_query' => $query,
                                                'produced_by' => $type_q
                                               )
                                        )
                                     );
    
    //Restoring old ident data
    if(isset($tempident['u']))
    {
        $_SESSION['username'] = $temp_ident['u'];
        $_SESSION['pwd'] = $temp_ident['p'];
    }
    else
    {
        unset($_SESSION['username']);
        unset($_SESSION['pwd']);
    }
    
    //Processing result
    if(@$result->CTI_PLUGIN_CALL_ERROR)
    {
        return array();
    }
    
    $ret = array();
    foreach($result->data as $data)
    {
        $ret[] = $data->{'entry_info.entry_uid'};
    }
    return $ret;
}
//---------------------------------------------------------------------------

/**
 * @brief get the user uid form the username
 * @param username: the username to search for
 * @param cti_user: the user to run the query with
 * @param password: the password of the cti_user
 * @return the user uid
 */
function get_user_uid($username, $cti_user='', $password='')
{
    $user_uid = user_query('username:'.$username, 'user', $cti_user, $password);
    if (count($user_uid) > 0)
    {
        $user_uid = $user_uid[0];
    }
    return $user_uid;
}

//---------------------------------------------------------------------------
/**
 * @brief prepare the query parameters
 * @param params: the parameters
 * @return the new parameters
 */
function prepare_query($params)
{
    $result = array();
    
    $repository_query = 'all';
    if (isset($params['repository_query']))
        $repository_query = $params['repository_query'];
    $result['repository'] = $repository_query;
    
    if (isset($params['repository_type']))
    {
        switch ($params['repository_type'])
        {
            case 'all':
                $repository_query = 'all';
                break;
            case (string) CTR_REP_COMMON:
                $repository_query = 'common';
                break;
            case (string) CTR_REP_TEMP:
                $repository_query = 'temp';
                break;
            default:
                $repository_query = $params['repository_type'];
        }
        $result['repository'] = $repository_query;
    }
    
    $query = $params['search_query'];
    
    // Advanced search box
    // alias_s could contain ":" so we need to put " in order to avoir parse errors
    if (isset($params['alias_s']) && $params['alias_s'] != '')
        $query.= ' alias:"'.$params['alias_s'].'"';
    
    if (isset($params['user']) && $params['user'] != '')
    {
        $p_user = $params['user'];
        if (cts_is_UID($p_user))
        {
            $query.= ' user_uid:'.$p_user;
        }
        else
        {
            $query.= ' user_uid:{'.$p_user.'}';
        }
    }
    
    if (isset($params['tag_s']) && $params['tag_s'] != '')
        $query.= ' tag:'.$params['tag_s'];
    
    if (isset($params['file_s']) && $params['file_s'] != '')
        $query.= ' additional_files:'.$params['file_s'];
    
    if (isset($params['note']) && $params['note'] != '')
        $query.= ' note:*'.$params['note'].'*';
    
    if (isset($params['exit']) && $params['exit'] != '')
        $query.= ' plugin_exit_code:'.$params['exit'];
    
    $query = ltrim($query);
    $result['query'] = $query;
    
    if (isset($params['date_start']))
        $result['date_start'] = $params['date_start'];
    if (isset($params['date_end']))
        $result['date_end'] = $params['date_end'];
        
    if (isset($params['produced_by']) && $params['produced_by'] != '')
    {
        if ($params['produced_by'] == 'all')
            $params['produced_by'] = Null;
        $result['type'] = $params['produced_by'];
    }
    
    if (isset($params['fields']))
    {
        // Fields is an array when it's produced by the advanced search page
        // it's a string when it's produced by the database table itself (example: sort by field)
        if (!is_array($params['fields']))
            $params['fields'] = explode(',', $params['fields']);
        $result['fields'] = $params['fields'];
    }
    
    if (isset($params['sorted_'.@$params['id_table']]))
    {
        $result['sorted'] = $params['sorted_'.@$params['id_table']];
        
        //Adding one if fields wasn't set, since the entry UID and alias fields are fused by default 
        if (!isset($params['fields']) && $result['sorted'] != 0)
        {
            if ($result['sorted'] > 0)
            {
                $result['sorted'] = $result['sorted'] + 1;
            }
            else
            {
                $result['sorted'] = $result['sorted'] - 1;
            }
        }
    }
    
    if (isset($params['size_'.@$params['id_table']]))
        $result['size'] = $params['size_'.@$params['id_table']];
    
    if (isset($params['current_'.@$params['id_table']]))
        $result['page'] = $params['current_'.@$params['id_table']];
        
    return $result;
}

?>
