<?php
/************************************************************************
 Codelet Tuning Infrastructure
 Copyright (C) 2010-2015 Intel Corporation, CEA, GENCI, and UVSQ

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

// Authors: Franck Talbart, Mathieu Bordet, Nicolas Petit

require_once($_SERVER['DOCUMENT_ROOT'].'../cfg/config.inc.php');
require_once($DIRECTORY['PLUGINS'].'UserPlugin.php');

/**
 * @brief Check if a webpage can be included or not
 * @param page: the page to check
 * @return true if the page is allowed, false otherwise
 */
function security_include($page)
{
        global $CFG;
        
        if(in_array($page, $CFG['allowed_pages']))
        {
            return true;
        }
        else return false;
}
//---------------------------------------------------------------------------

/**
* @brief Check if the username and password are right
* @param user_login: the user UID
* @param pwd: the password
* @return True if the identification succeed, False otherwise 
*/
function login_user($user_login, $pwd)
{
    if ($user_login == '' || $pwd == '')
        return 0;
    
    $user_plugin = new UserPlugin();
    
    $result = $user_plugin->get_data('check_password', array('username'=>$user_login, 'password'=>$pwd));
    if (@$result->CTI_PLUGIN_CALL_ERROR)
    {
        //Invalid username/password combination.
        return False;
    }
    
    return @$result->result;
}
//---------------------------------------------------------------------------

/**
* @brief Check if a command is a malicious code
* @param cmd: the command to check
* @return 0 if the command is safe, 1 otherwise
*/
function check_malicious_code($cmd)
{
    global $LINK;
    $security = preg_match("|[^-._a-zA-Z0-9éèàç/\=,\"'\:/\\\\/+ #{}@]|", $cmd); // /\\\\/ = one back slash!!
    if ($security == 1)
    {
        ?>
            <div class="block">
                <font color="#FE0101">Malicious code detected.</font>
                Please report this situation 
                <a href="<?php echo $LINK['redmine'];?>">here</a>.
            </div>
        <?php 
        return 1;
    }
    return 0;
}

?>
